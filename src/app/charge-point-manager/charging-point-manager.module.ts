﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClientModule,  HttpClient, HttpClientJsonpModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { ChargingPointManagerRoutingModule } from './charging-point-manager-routing.module';
import { MapsModule } from '../google-maps/maps.module';
import { DatepickerModule } from '../common/datepicker/datepicker.module';
import { PeriodSelectorModule } from '../common/period-selector/period-selector.module';
import { TimePickerModule } from '../common/timepicker/timepicker.module';
import { ChargingPointConnectorsSharedModule } from './connectors-shared/charge-point-connector.module';

// SERVICES
import { ChargingPointManagerService } from './charging-point-manager.service';
import { DiagnosticsService } from './edit/diagnostics/diagnostics.service';
import { ChargePointNotificationService } from './notifications/charge-point-notification.service';
import { UnlockDialogService } from './edit/details/unlock-dialog/unlock-dialog.service';
import { TranslateStateService } from '../core/translateStore.service';

// COMPONENTS
import { ChargingPointsManagerComponent } from './view/charging-points-manager.component';
import { DetailsComponent } from './edit/details/details.component';
import { FirmwareUpdateDialogComponent } from './edit/details/firmware-update/firmware-update.component';
import { UnlockDialogComponent } from './edit/details/unlock-dialog/unlock-dialog.component';
import { DiagnosticsComponent } from './edit/diagnostics/diagnostics.component';
import { TransactionsComponent } from './edit/transactions/transactions.component';
import { OcppComponent } from './edit/ocpp/ocpp.component';
import { OcppDetailsDialogComponent } from './edit/ocpp/details/details.component';
import { AssetGroupsModule } from '../common/asset-groups/asset-groups.module';

// PIPES
import { MeterValueDateFilterPipe } from './edit/transactions/meter-values-date-filter.pipe';
import { MeasurandTranslationPipe } from './edit/transactions/measurand-translation.pipe';
import { UnitOfMeasureTranslationPipe } from './edit/transactions/unit-of-measure-translation.pipe';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/charge-point-manager/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        HttpClientJsonpModule,
        HttpClientModule,
        SharedModule,
        ChargingPointManagerRoutingModule,
        MapsModule,
        DatepickerModule,
        PeriodSelectorModule,
        TimePickerModule,
        ChargingPointConnectorsSharedModule,
        AssetGroupsModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ChargingPointsManagerComponent,
        DetailsComponent,
        TransactionsComponent,
        OcppComponent,
        FirmwareUpdateDialogComponent,
        UnlockDialogComponent,
        OcppDetailsDialogComponent,
        DiagnosticsComponent,
        // transaction meter values pipes
        MeterValueDateFilterPipe,
        MeasurandTranslationPipe,
        UnitOfMeasureTranslationPipe
    ],
    providers: [
        DiagnosticsService,
        ChargingPointManagerService,
        ChargePointNotificationService,
        UnlockDialogService
    ],
    entryComponents: [
        FirmwareUpdateDialogComponent,
        OcppDetailsDialogComponent,
        DiagnosticsComponent,
        UnlockDialogComponent
    ]
})
export class ChargingPointManagerModule { }
