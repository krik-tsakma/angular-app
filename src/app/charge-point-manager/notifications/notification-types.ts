export interface CpStatusChange {
    id: number;
}

export interface CpDiagnosticsChange {
    id: number;
}
