// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Subject } from 'rxjs';

// SIGNALR
import * as signalR from '@aspnet/signalr';

// SERVICES
import { Config } from '../../config/config';

// TYPES
import { CpStatusChange, CpDiagnosticsChange } from './notification-types';

@Injectable()
export class ChargePointNotificationService {
    public stateNotificationReceived: Subject<CpStatusChange> = new Subject<CpStatusChange>();
    public diangosticsNotificationReceived: Subject<CpDiagnosticsChange> = new Subject<CpDiagnosticsChange>();

    private hubConnection: signalR.HubConnection = null;
    private hubEndPoint: string = this.config.get('chargePointsHub');
   
    constructor(private config: Config) {
        // foo
    }
        
    public Start(): void {
        if (this.hubConnection !== null) {
            return;
        }
        this.hubConnection = new signalR.HubConnectionBuilder()
                            .withUrl(this.hubEndPoint)
                            .build();
        
        this.hubConnection
            .start()
            .then(() => console.log('Connection with Charge Point Hub started!'))
            .catch(() => console.log('Error while establishing connection with Export Files Hub'));

        this.hubConnection.on('OnStateChange', (res: CpStatusChange) => {
            // console.log('state change notification');
            if (!res || !res.id) {
                return;
            }
            this.stateNotificationReceived.next(res);
        });
        
        this.hubConnection.on('OnDiagnosticsChange', (res: CpDiagnosticsChange) => {
            // console.log('dignostics state change notification');
            if (!res || !res.id) {
                return;
            }
            this.diangosticsNotificationReceived.next(res);
        });
    }

    public Stop(): void {
        if (this.hubConnection === null) {
            return;
        }
        this.hubConnection
            .stop()
            .then(() => {
                this.hubConnection = null;
            })
            .catch(() => console.log('Error while terminating connection with Charge Point Hub'));
    }
} 
