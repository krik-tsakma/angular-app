﻿// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

// RXJS
import { Subscription } from 'rxjs';

// COMPONENTS
import { MoreOptionsEditingOptionsDialogComponent } from '../../shared/data-table/more-options-editing-options-dialog/more-options-edit-options-dialog.component';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { ChargingPointManagerService } from '../charging-point-manager.service';
import { ChargePointNotificationService } from '../notifications/charge-point-notification.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';
import { Config } from '../../config/config';

// TYPES
import {
    EvChargingPointsResults, EvChargingPointResultItem,
    EvChargingPointsRequest, ChargePointStatusOptions,
    EvChargingPointStatus, ChargePointManagerViewOptions,
    ChargePointManagerViewOptionsDialogCloseResult
} from '../charging-point-manager-types';
import {
    TableColumnSetting,
    TableActionItem,
    TableActionOptions,
    TableCellFormatOptions,
    TableColumnSortOrderOptions,
    TableColumnSortOptions
} from '../../shared/data-table/data-table.types';
import { Pager } from '../../common/pager';
import { KeyName } from '../../common/key-name';
import { SearchType } from '../../shared/search-mechanism/search-mechanism-types';
import { MapModeOptions } from '../../google-maps/types/map.types';
import { ChargePointMarker, ChargePointMarkerCreator } from '../../google-maps/types/charge-point.types';
import { BaseCustomMarker } from '../../google-maps/types/custom-marker.types';
import { CpStatusChange } from '../notifications/notification-types';
import { AssetGroupTypes } from './../../admin/asset-groups/asset-groups.types';
import { StoreItems } from './../../shared/store-manager/store-items';

// PIPES
import { DateFormatPipe, DateFormatterDisplayOptions } from '../../shared/pipes/dateFormat.pipe';
import { ChargePointConnectorFormatPipe, ChargePointConnectorFormatter } from '../connectors-shared/charge-point-connector-format.pipe';

@Component({
    selector: 'charging-points-manager',
    templateUrl: './charging-points-manager.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./charging-points-manager.less'],
    providers: [DateFormatPipe]
})

export class ChargingPointsManagerComponent implements OnInit, OnDestroy {
    @ViewChild(MoreOptionsEditingOptionsDialogComponent, { static: true }) public editingOptionsDialog: MoreOptionsEditingOptionsDialogComponent;

    public loading: boolean;
    public message: string;
    public mapInitializing: boolean;
    public searchTypes: any = SearchType;
    public groupTypes: any = AssetGroupTypes;
    public request: EvChargingPointsRequest = {} as EvChargingPointsRequest;
    public pager: Pager = new Pager();
    public evChargingPointsData: EvChargingPointResultItem[] = [] as EvChargingPointResultItem[];
    public tableSettings: TableColumnSetting[];
    public chargePointStatuses: KeyName[];
    public mapModes: any = MapModeOptions;
    public cpMarkers: Array<BaseCustomMarker<ChargePointMarker>> = [];

    private subscriber: Subscription;

    constructor(
        private service: ChargingPointManagerService,
        private translate: CustomTranslateService,
        private router: Router,
        private unsubscribe: UnsubscribeService,
        private dateTimeFormatPipe: DateFormatPipe,
        private userOptions: UserOptionsService,
        private cpNotificationService: ChargePointNotificationService,
        private configService: Config,
        private storeManager: StoreManagerService,
        private connectorFormatPipe: ChargePointConnectorFormatPipe) {

            this.tableSettings = this.getTableSettings();
            this.chargePointStatuses = this.getChargePointStatuses();
            this.request.showMap = false;

    }

    // =========================
    // LIFECYCLE
    // =========================
    public ngOnInit() {
        const savedData = this.storeManager.getOption(StoreItems.cpm) as EvChargingPointsRequest;
        if (savedData) {
            this.request = savedData;
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.get(true);

        this.cpNotificationService.Start();
        this.cpNotificationService.stateNotificationReceived.subscribe((res: CpStatusChange) => {

            const cp = this.evChargingPointsData.find((c) => c.id === Number(res.id));
            if (!cp) {
                return;
            }

            this.service
                .getChargingPointLastUpdate(res.id)
                .subscribe((response: EvChargingPointResultItem) => {
                    // update table data, do it this way to invoke angular's change detection
                    const currentData: EvChargingPointResultItem[] = [];
                    this.evChargingPointsData.forEach((c) => {
                        if (c.id !== Number(res.id)) {
                            currentData.push(c);
                        } else {
                            currentData.push(response);
                        }
                    });
                    this.evChargingPointsData = currentData;

                    // update the map markers
                    this.cpMarkers = this.evChargingPointsData.map((chp) => {
                        const m = new ChargePointMarkerCreator(chp.id, chp.name, chp.chargePointStatus, chp.latitude, chp.longitude);
                        return m.marker;
                    });
                });
        });

        this.editingOptionsDialog.afterClosed().subscribe((res: ChargePointManagerViewOptionsDialogCloseResult) => {
            const cpName = { cpName: res.element.name };

            switch (res.action) {
                case ChargePointManagerViewOptions.details:
                    this.router.navigate(['charge-points-manager', 'details', res.element.id], { skipLocationChange: !this.configService.get('router') });
                    break;
                case ChargePointManagerViewOptions.diagnostics:
                    this.router.navigate(['charge-points-manager', 'diagnostics', res.element.id], { queryParams: cpName, skipLocationChange: !this.configService.get('router') });
                    break;
                case ChargePointManagerViewOptions.transactions:
                    this.router.navigate(['charge-points-manager', 'transactions', res.element.id], { queryParams: cpName, skipLocationChange: !this.configService.get('router') });
                    break;
                case ChargePointManagerViewOptions.ocpp:
                    this.router.navigate(['charge-points-manager', 'ocpp', res.element.id], { queryParams: cpName, skipLocationChange: !this.configService.get('router') });
                    break;
                default:
                    break;
            }
        });
    }


    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.subscriber);
        this.cpNotificationService.Stop();
    }

    // =========================
    // SHOW MAP
    // =========================

    public onShowMap() {
        this.storeManager.saveOption(StoreItems.cpm, this.request);
    }



    // =========================
    // GROUPS
    // =========================

    public onChangeGroup(group?: KeyName) {
        this.request.groupID = group ? group.id : null;
        this.get(true);
    }


    // =========================
    // TABLE
    // =========================

    // fetch data
    public loadMore() {
        if (this.pager.pageNumber < this.pager.totalPages && this.loading === false) {
            this.pager.addPage();
            this.get(false);
        }
    }

    // When the user selected a column to sort (from list view)
    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }


    // When the user click the '3 dots'
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const record = item.record as EvChargingPointResultItem;

        switch (item.action) {
            case TableActionOptions.moreOptions:
                const options = this.getOptions();
                this.editingOptionsDialog.open(record, options, { x: item.e.x, y: item.e.y });
                break;
            default:
                break;

        }
    }


    // Fetch ev charging points
    public get(resetPageNumber: boolean) {
        if (this.loading) {
            return;
        }
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.pager.pageSize = 20;
            this.evChargingPointsData = new Array<EvChargingPointResultItem>();
        }

        this.loading = true;

        this.request.pageSize = this.pager.pageSize;
        this.request.pageNumber = this.pager.pageNumber;

        this.storeManager.saveOption(StoreItems.cpm, this.request);

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.subscriber);

        // then start the new request
        this.subscriber = this.service
            .get(this.request)
            .subscribe((response: EvChargingPointsResults) => {
                this.loading = false;
                if (resetPageNumber === true) {
                    this.evChargingPointsData = response && response.results
                        ? response.results
                        : [];
                } else {
                    this.evChargingPointsData = this.evChargingPointsData.concat(response.results);
                }

                // set the map markers
                this.cpMarkers = this.evChargingPointsData.map((cp) => {
                    const m = new ChargePointMarkerCreator(cp.id, cp.name, cp.chargePointStatus, cp.latitude, cp.longitude);
                    return m.marker;
                });

                // Get the paging info
                this.pager.pageNumber = response.pageNumber;
                this.pager.pageSize = response.pageSize;
                this.pager.totalPages = response.totalPages;
                this.pager.totalRecords = response.totalRecords;
            },
            (err) => {
                this.loading = false;
                this.evChargingPointsData = [];
                this.message = 'data.error';
            });
        return;
    }


    // =========================
    // PRIVATE
    // =========================

    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                primaryKey: 'name',
                header: 'ev_charging_points_manager.name',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'status',
                header: 'ev_charging_points_manager.status',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvChargingPointResultItem) => {
                    if (item.chargePointStatus !== null || item.chargePointStatus !== undefined) {
                        return EvChargingPointStatus.formatStatus(Number(item.chargePointStatus), this.translate);
                    }
                    return 'N/A';
                },
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'connectors',
                header: 'ev_charging_points_manager.connectors_status',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvChargingPointResultItem) => {
                    const args = { hideZeroConnector: true } as ChargePointConnectorFormatter;
                    let connHTML = '';
                    item.connectors.forEach((connector) => {
                        connHTML += this.connectorFormatPipe.transform(connector, args);
                    });
                    return connHTML;
                }
            },
            {
                primaryKey: 'lastUpdate',
                header: 'ev_charging_points_manager.last_update',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvChargingPointResultItem) => {
                    if (!item.lastUpdate) {
                        return 'N/A';
                    }
                    const startT = this.dateTimeFormatPipe.transform(item.lastUpdate, {
                        display: DateFormatterDisplayOptions.both,
                        dateFormat: this.userOptions.localOptions.short_date,
                        showSeconds: true
                    });
                    return startT;
                },
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'vehicleLabel',
                header: 'ev_charging_points_manager.vehicle',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'soC',
                header: 'ev_charging_points_manager.soc',
                format: TableCellFormatOptions.percentage,
            },
            {
                primaryKey: 'model',
                header: 'ev_charging_points_manager.model',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'serialNumber',
                header: 'ev_charging_points_manager.meter_serial',
            },
            {
                primaryKey: 'firmwareVersion',
                header: 'ev_charging_points_manager.firmware',
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.moreOptions],
            }
        ];
    }

    private getChargePointStatuses(): KeyName[] {
        const statuses: KeyName[] =  [
            {
                id: null,
                name: 'All',
                term: 'form.actions.all',
                enabled: true,
            },
            {
                id: ChargePointStatusOptions.Available,
                name: 'Available',
                term: 'ev_charging_points_manager.statuses.available',
                enabled: true
            },
            {
                id: ChargePointStatusOptions.Unavailable,
                name: 'Unavailable',
                term: 'ev_charging_points_manager.statuses.unavailable',
                enabled: true
            },
            {
                id: ChargePointStatusOptions.Faulted,
                name: 'Faulted',
                term: 'ev_charging_points_manager.statuses.faulted',
                enabled: true
            }
        ];

        statuses.forEach((s) => {
            this.translate
                .get(s.term)
                .subscribe((t) => {
                    s.name = t;
                });
        });
        return statuses;
    }

    private getOptions(): KeyName[] {
        return [
            {
                id: ChargePointManagerViewOptions.details,
                term: 'ev_charging_points_manager.details',
                name: 'details',
                enabled: true,
            },
            {
                id: ChargePointManagerViewOptions.diagnostics,
                term: 'ev_charging_points_manager.diagnostics',
                name: 'diagnostics',
                enabled: true,
            },
            {
                id: ChargePointManagerViewOptions.transactions,
                term: 'ev_charging_points_manager.transactions',
                name: 'transactions',
                enabled: true,
            },
            {
                id: ChargePointManagerViewOptions.ocpp,
                term: 'ev_charging_points_manager.ocpp',
                name: 'ocpp',
                enabled: true,
            }
        ];
    }
}
