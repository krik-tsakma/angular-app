// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
import { ChargePointConnector, ConnectorStatusOptions, ConnectorErrorCodes } from './charge-point-connector-types';

/*
 * This pipe provides number variable replacement
 * in a decimal separator and rounding if asked
 * Usage:
 *   value | numberFormat:arguments
 * Example:
 *   {{ 4.50123 | numberFormat:[',', 2] }}
 *   formats to:  4,50
*/

export interface ChargePointConnectorFormatter {
    hideZeroConnector: boolean;
}
@Pipe({name: 'myChargePointConnectorFormat'})
export class ChargePointConnectorFormatPipe implements PipeTransform {

    constructor(private translateService: CustomTranslateService) {

    }

    public transform(c: ChargePointConnector, args: ChargePointConnectorFormatter, contrl?: boolean): string {
        let title = '';
        if (args.hideZeroConnector && c.id === 0) {
            return;
        }

        let className = '';
        switch (c.status) {
            case ConnectorStatusOptions.Available:
                className = 'available';
                title = 'ev_charging_points_manager.statuses.available';
                break;
            case ConnectorStatusOptions.Faulted:
                className = 'faulted';
                title = 'ev_charging_points_manager.statuses.faulted';
                break;
            case ConnectorStatusOptions.Unavailable:
                className = 'unavailable';
                title = 'ev_charging_points_manager.statuses.unavailable';
                break;
            case ConnectorStatusOptions.Charging:
                className = 'prepare-charging-finishing';
                title = 'ev_charging_points_manager.statuses.charging';
                break;
            case ConnectorStatusOptions.SuspendedEV:
                className = 'unavailable';
                title = 'ev_charging_points_manager.statuses.suspendedEV';
                break;
            case ConnectorStatusOptions.SuspendedEVSE:
                className = 'unavailable';
                title = 'ev_charging_points_manager.statuses.suspendedEVSE';
                break;
            case ConnectorStatusOptions.Finishing:
                className = 'prepare-charging-finishing';
                title = 'ev_charging_points_manager.statuses.finishing';
                break;
            case ConnectorStatusOptions.Preparing:
                className = 'prepare-charging-finishing';
                title = 'ev_charging_points_manager.statuses.preparing';
                break;
            case ConnectorStatusOptions.Reserved:
                className = 'unavailable';
                title = 'ev_charging_points_manager.statuses.reserved';
                break;
            case ConnectorStatusOptions.Unknown:
                className = 'unknown';
                title = 'ev_charging_points_manager.statuses.unknown';
                break;
            default:
                break;
        }
        title = this.translateService.instant(title);
        const val = contrl
            ? `${c.id}: ${title}`
            : `<span class="charging-status ${className}">${c.id}: ${title}</span>`;

        return val;
    }
}
