// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// SERVICES
import { CustomTranslateService } from './../../shared/custom-translate.service';

// TYPES
import { ChargePointConnector, ConnectorErrorCodes } from './charge-point-connector-types';

/*
 * This pipe provides number variable replacement
 * in a decimal separator and rounding if asked
 * Usage:
 *   value | numberFormat:arguments
 * Example:
 *   {{ 4.50123 | numberFormat:[',', 2] }}
 *   formats to:  4,50
*/

@Pipe({name: 'myChargePointConnectorErrorFormat'})
export class ChargePointConnectorErrorFormatPipe implements PipeTransform {
    
    constructor(private translateService: CustomTranslateService) {

    }

    public transform(c: ChargePointConnector, args: any): string {
        const lastReceivedError = this.translateService.instant('ev_charging_points_manager.statuses.last_received_error');
        const val = (c.errorCode !== null && c.errorCode !== undefined && c.errorCode !== ConnectorErrorCodes.NoError ? `<span class="cp-connector-error">` + 
                        `<span class="ico"></span>` + 
                        `${ lastReceivedError }: ${ this.getErrorCodeTerm(c.errorCode) } ${c.errorDescription}` 
                    : ``);
        return val;
        
    }

    private getErrorCodeTerm(code: ConnectorErrorCodes): string {
        let errorTerm: string = '';
        switch (code) {
            case ConnectorErrorCodes.ConnectorLockFailure:
                errorTerm = 'ev_charging_points_manager.connector_errors.connector_lock_failure';
                break;
            case ConnectorErrorCodes.EVCommunicationError:
                errorTerm = 'ev_charging_points_manager.connector_errors.EV_communication_error';
                break;
            case ConnectorErrorCodes.GroundFailure:
                errorTerm = 'ev_charging_points_manager.connector_errors.ground_failure';
                break;
            case ConnectorErrorCodes.HighTemperature:
                errorTerm = 'ev_charging_points_manager.connector_errors.high_temperature';
                break;
            case ConnectorErrorCodes.InternalError:
                errorTerm = 'ev_charging_points_manager.connector_errors.internal_error';
                break;
            case ConnectorErrorCodes.LocalListConflict:
                errorTerm = 'ev_charging_points_manager.connector_errors.local_list_conflict';
                break;
            case ConnectorErrorCodes.NoError:
                break;
            case ConnectorErrorCodes.OtherError:
                errorTerm = 'ev_charging_points_manager.connector_errors.other_error';
                break;
            case ConnectorErrorCodes.OverCurrentFailure:
                errorTerm = 'ev_charging_points_manager.connector_errors.over_current_failure';
                break;
            case ConnectorErrorCodes.OverVoltage:
                errorTerm = 'ev_charging_points_manager.connector_errors.over_voltage';
                break;
            case ConnectorErrorCodes.PowerMeterFailure:
                errorTerm = 'ev_charging_points_manager.connector_errors.power_meter_failure';
                break;
            case ConnectorErrorCodes.PowerSwitchFailure:
                errorTerm = 'ev_charging_points_manager.connector_errors.power_switch_failure';
                break;
            case ConnectorErrorCodes.ReaderFailure:
                errorTerm = 'ev_charging_points_manager.connector_errors.reader_failure';
                break;
            case ConnectorErrorCodes.ResetFailure:
                errorTerm = 'ev_charging_points_manager.connector_errors.reset_failure';
                break;
            case ConnectorErrorCodes.UnderVoltage:
                errorTerm = 'ev_charging_points_manager.connector_errors.under_voltage';
                break;
            case ConnectorErrorCodes.WeakSignal:
                errorTerm = 'ev_charging_points_manager.connector_errors.weak_signal';
                break;
        }
        return errorTerm ? this.translateService.instant(errorTerm) : '';
    }

}
