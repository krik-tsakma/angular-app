export interface ChargePointConnector {
    id: number;
    status: ConnectorStatusOptions;
    errorCode?: ConnectorErrorCodes;
    errorDescription: string;
}

export enum ConnectorStatusOptions {
    Available = 0,
    Preparing = 1,
    Charging = 2,
    SuspendedEVSE = 3,
    SuspendedEV = 4,
    Finishing = 5,
    Reserved = 6,
    Unavailable = 7,
    Faulted = 8,
    Unknown = 99
}

export enum ConnectorErrorCodes {
    ConnectorLockFailure = 0,
    EVCommunicationError = 1,
    GroundFailure = 2,
    HighTemperature = 3,
    InternalError = 4,
    LocalListConflict = 5,
    NoError = 6,
    OtherError = 7,
    OverCurrentFailure = 8,
    OverVoltage = 9,
    PowerMeterFailure = 10,
    PowerSwitchFailure = 11,
    ReaderFailure = 12,
    ResetFailure = 13,
    UnderVoltage = 14,
    WeakSignal = 15,
}
