// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from './../../shared/shared.module';

// PIPES
import { ChargePointConnectorFormatPipe } from './charge-point-connector-format.pipe';
import { ChargePointConnectorErrorFormatPipe } from './charge-point-connector-error-format.pipe';

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        ChargePointConnectorFormatPipe,
        ChargePointConnectorErrorFormatPipe
    ],
    exports: [
        ChargePointConnectorFormatPipe,
        ChargePointConnectorErrorFormatPipe
    ],
    providers: [
        ChargePointConnectorFormatPipe
    ]
})
export class ChargingPointConnectorsSharedModule { }
