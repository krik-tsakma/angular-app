﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../config/config';
import { AuthHttp } from '../auth/authHttp';

// TYPES
import {
    EvChargingPointsResults, 
    EvChargingPointsRequest, 
    EvChargePointDetails,
    ChargePointResetCodeOptions,
    ChargePointOperationStatusCodeOptions,
    ChargePointOperationStatusRequest,
    ChargePointResetRequest,
    EvChargingPointResultItem,
    UnlockConnectorRequest,
    ChargePointsHistoryActionsRequest,
    ChargePointsHistoryActionsResults
} from './charging-point-manager-types';
import { 
    EvChargePointManagerTransactionsRequest, 
    EvChargingPointTransactionsResults, 
    EvChargePointManagerTransactionDetails,
    TransactionMeterValue,
    TransactionVehiclesResult,
    TransactionVehiclesResultItem,
    TransactionVehiclesRequest,
} from './edit/transactions/transactions-types';
import { EvChargePointManagerOcppRequest, EvChargePointOcppResults } from './edit/ocpp/ocpp-types';
import { FirmwareUpdateRequest, FirmwareUpdateCodeOptions } from './edit/details/firmware-update/firmware-update-types';
import { OcppDetailsRequest } from './edit/ocpp/details/details-types';

@Injectable()
export class ChargingPointManagerService {
    private baseApiURL: string;

    constructor(
        private authHttp: AuthHttp,
        private configService: Config) {
            this.baseApiURL = this.configService.get('apiUrl') + '/api/EvChargePointManager';
    }

    // ====================
    // CHARGE POINTS
    // ====================

    public get(req: EvChargingPointsRequest): Observable<EvChargingPointsResults> {

        const obj = this.authHttp.removeObjNullKeys({            
            chargePointStatus: req.chargePointStatus ? req.chargePointStatus.toString() : null,
            groupID: req.groupID ? req.groupID.toString() : null,
            pageNumber: req.pageNumber,
            pageSize: req.pageSize,
            sorting: req.sorting ? req.sorting.toString() : null
        });

        const params = new HttpParams({
            fromObject: obj
        });


        return this.authHttp
            .get(this.baseApiURL, { params });
    }

    public getChargingPointLastUpdate(id: number): Observable<EvChargingPointResultItem> {
        return this.authHttp
            .get(this.baseApiURL + '/LastUpdateRefresh/' + id);
    }

    public getChargingPoint(id: number): Observable<EvChargePointDetails> {
        return this.authHttp
            .get(this.baseApiURL + '/' + id);
    }

    // ====================
    // TRANSACTIONS
    // ====================

    public getTransactions(request: EvChargePointManagerTransactionsRequest): Observable<EvChargingPointTransactionsResults> {

        const obj = this.authHttp.removeObjNullKeys({
            ChargePointID: request.chargePointID,
            VehicleID: request.vehicleID,
            VehicleChargingID: request.vehicleChargingID,
            PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
            PageSize: request.pageSize ? request.pageSize.toString() : '20',
            Since: request.periodSince,
            Till: request.periodTill,
            Sorting: request.sorting,
        });

        const params = new HttpParams({
            fromObject: obj
        });       

        return this.authHttp
            .get(this.baseApiURL + '/GetTransactions', { params });
    }


    public getTransactionByID(id: number): Observable<EvChargePointManagerTransactionDetails> {
        return this.authHttp
            .get(this.baseApiURL + '/GetTransaction/' + id);
    }

    public getMeterValuesForTransaction(id: number): Observable<TransactionMeterValue[]>  {
        return this.authHttp
        .get(this.baseApiURL + '/GetMeterValues/' + id);
    }

    public getLastTransaction(chargePointID: number): Observable<EvChargePointManagerTransactionDetails> {
        return this.authHttp
            .get(this.baseApiURL + '/GetLastTransaction/' + chargePointID);
    }

    public stopTransaction(ocppid: string): Observable<number>  {
        return this.authHttp
        .get(this.baseApiURL + '/StopTransaction/' + ocppid, { observe: 'response' })
        .pipe(
            map((res) => res.status)               
        );
    }

    public getTransactionVehicles(request: TransactionVehiclesRequest): Observable<TransactionVehiclesResultItem[]> {

        const obj = this.authHttp.removeObjNullKeys({
            SearchTerm: request.searchTerm,
            ChargePointID: request.chargePointID,
            PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
            PageSize: request.pageSize ? request.pageSize.toString() : '20',
            Sorting: request.sorting,
        });
        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.baseApiURL + '/GetTransactionVehicles', { params })
            .pipe(
                map((res: TransactionVehiclesResult) => {
                    return res.results;
                })
            );
    }

    // ====================
    // OCPP
    // ====================

    public getOcpp(request: EvChargePointManagerOcppRequest): Observable<EvChargePointOcppResults> {

        const obj = this.authHttp.removeObjNullKeys({
            PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
            PageSize: request.pageSize ? request.pageSize.toString() : '20',
            Date: request.date,
            ShowErrors: request.showErrors,
            Sorting: request.sorting,
            ChargePointID: request.chargePointID,
            HideHeartbeats: request.hideHeartbeats
        });

        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.baseApiURL + '/OCPP', { params });
    }

    public ocppDetails(request: OcppDetailsRequest): Observable<string> {
        const params = new HttpParams({
            fromObject:  {
                Timestamp: request.timestamp,
                MessageType: request.messageType.toString(),
                OCPPID: request.OCPPID
            }
        });

        return this.authHttp
            .get(this.baseApiURL + '/OCPPDetails', { params });
    }


    // ====================
    // ACTIONS
    // ====================

    public setOperationStatus(entity: ChargePointOperationStatusRequest): Observable<ChargePointOperationStatusCodeOptions>  {
        return this.authHttp
            .post(this.baseApiURL + '/SetOperationStatus', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ChargePointOperationStatusCodeOptions;
                    }
                })          
            );
    }

    public reset(entity: ChargePointResetRequest): Observable<ChargePointResetCodeOptions>  {
        return this.authHttp
            .post(this.baseApiURL + '/Reset', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ChargePointResetCodeOptions;
                    }
                })          
            );
    }

    public clearCache(ocppid): Observable<ChargePointResetCodeOptions>  {
        return this.authHttp
            .post(this.baseApiURL + '/ClearCache', ocppid, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ChargePointResetCodeOptions;
                    }
                })          
            );
    }

    public unlock(req: UnlockConnectorRequest): Observable<ChargePointResetCodeOptions>  {
        return this.authHttp
            .post(this.baseApiURL + '/Unlock', req);
    }

    public firmwareUpdate(entity: FirmwareUpdateRequest): Observable<FirmwareUpdateCodeOptions>  {
        return this.authHttp
            .post(this.baseApiURL + '/UpdateFirmware', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as FirmwareUpdateCodeOptions;
                    }
                })          
            );
    }


    // ====================
    // DETAILS HISTORY
    // ====================

    public getCPMHistory(request: ChargePointsHistoryActionsRequest): Observable<ChargePointsHistoryActionsResults> {

        const obj = this.authHttp.removeObjNullKeys({
            PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
            PageSize: request.pageSize ? request.pageSize.toString() : '20',    
            Sorting: request.sorting,
            ChargePointID: request.chargePointID,
            Since: request.periodSince,
            Till: request.periodTill,  
        });

        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.baseApiURL + '/History', { params });
    }

}
