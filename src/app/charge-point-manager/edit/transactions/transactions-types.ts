import { PagerRequest, PagerResults, SearchTermRequest } from '../../../common/pager';
import { PeriodTypes } from '../../../common/period-selector/period-selector-types';

// ====================
// TRANSACTIONS
// ====================

export interface EvChargePointManagerTransactionsRequest extends PagerRequest {
    chargePointID: number;
    vehicleID?: number;
    vehicleChargingID: string;
    vehicleLabel: string;
    periodType: PeriodTypes;
    periodSince: string;
    periodTill: string;
}

export interface EvChargingPointTransactionsResults extends PagerResults {
    results: EvChargePointManagerTransactionItem[];
}

export interface EvChargePointManagerTransactionItem {
    id: number;
    startTimestamp: string;
    stopTimestamp?: string;
    duration?: number;
    vehicleLabel: string;
    whTransferred?: number;
}

export interface EvChargePointManagerTransactionDetails {
    id: number;
    ocppid: string;
    startTimestamp: string;
    stopTimestamp?: string;
    duration?: number;
    vehicleLabel: string;
    connectorID: number;
    meterStart: number;
    meterStop?: number;
}

export interface TransactionMeterValue {
    timestamp: string;    
    measurand: MeasurandOptions;
    value: string;
    unitOfMeasure: UnitOfMeasureOptions;
}

export enum MeasurandOptions {
    CurrentExportMeasurand = 1,
    CurrentImportMeasurand = 2,
    CurrentOfferedMeasurand = 3,
    EnergyActiveExportRegister = 4,
    EnergyActiveImportRegister = 5,
    EnergyReactiveExportRegister = 6,
    EnergyReactiveImportRegister = 7,
    EnerygActiveExportInterval = 8,
    EnergyActiveImportInterval = 9,
    EnergyReactiveExportInterval = 10,
    EnergyReactiveImportInterval = 11,
    Frequency = 12,
    PowerActiveExport = 13,
    PowerActiveImport = 14,
    PowerFactor = 15,
    PowerOffered = 16,
    PowerReactiveExport = 17,
    PowerReactiveImport = 18,
    RPM = 19,
    SoC = 20,
    Temperature = 21,
    Voltage = 22
}

export enum UnitOfMeasureOptions {
    Wh = 1,
    kWh = 2,
    varh = 3,
    kvarh = 4,
    W = 5,
    kW = 6,
    VA = 7,
    kVA = 8,
    var = 9,
    kvar = 10,
    A = 11,
    V = 12,
    Celsius = 13,
    Fahrenheit = 14,
    K = 15,
    Percent = 16
}

// ======================
// TRANSACTIONS VEHICLES
// ======================
export interface TransactionVehiclesRequest extends SearchTermRequest {
    chargePointID: number;
}
export interface TransactionVehiclesResult extends PagerResults {
    results: TransactionVehiclesResultItem[];
}

export interface TransactionVehiclesResultItem {
    vehicleID?: number;
    vehicleLabel: string;
    vehicleChargingID: string;
}
