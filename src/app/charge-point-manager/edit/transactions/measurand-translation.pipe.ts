﻿// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';
import { TransactionMeterValue, MeasurandOptions } from './transactions-types';
import { CustomTranslateService } from '../../../shared/custom-translate.service';

@Pipe({
    name: 'myMeasurandTranslation',
    pure: false
})

export class MeasurandTranslationPipe implements PipeTransform {
    constructor(
        private translateService: CustomTranslateService
    ) { 
        // foo
    }

    public transform(option: MeasurandOptions): any {

        let term = MeasurandOptions[option];

        switch (option) {
            case MeasurandOptions.CurrentExportMeasurand:
                term = 'ev_charging_points_manager.transactions.measurands.current_export_measurand';
                break;
            case MeasurandOptions.CurrentImportMeasurand:
                term = 'ev_charging_points_manager.transactions.measurands.current_import_measurand';
                break;
            case MeasurandOptions.CurrentOfferedMeasurand:
                term = 'ev_charging_points_manager.transactions.measurands.current_offered_measurand';
                break;
            case MeasurandOptions.EnergyActiveExportRegister:
                term = 'ev_charging_points_manager.transactions.measurands.energy_active_export_register';
                break;
            case MeasurandOptions.EnergyActiveImportRegister:
                term = 'ev_charging_points_manager.transactions.measurands.energy_active_import_register';
                break;
            case MeasurandOptions.EnergyReactiveExportRegister:
                term = 'ev_charging_points_manager.transactions.measurands.energy_reactive_export_register';
                break;
            case MeasurandOptions.EnergyReactiveImportRegister:
                term = 'ev_charging_points_manager.transactions.measurands.energy_reactive_import_register';
                break;
            case MeasurandOptions.EnerygActiveExportInterval:
                term = 'ev_charging_points_manager.transactions.measurands.energy_active_export_interval';
                break;
            case MeasurandOptions.EnergyActiveImportInterval:
                term = 'ev_charging_points_manager.transactions.measurands.energy_active_import_interval';
                break;
            case MeasurandOptions.EnergyReactiveExportInterval:
                term = 'ev_charging_points_manager.transactions.measurands.energy_reactive_export_interval';
                break;
            case MeasurandOptions.EnergyReactiveImportInterval:
                term = 'ev_charging_points_manager.transactions.measurands.energy_reactive_import_interval';
                break;
            case MeasurandOptions.Frequency:
                term = 'ev_charging_points_manager.transactions.measurands.Frequency';
                break;
            case MeasurandOptions.PowerActiveExport:
                term = 'ev_charging_points_manager.transactions.measurands.power_active_export';
                break;
            case MeasurandOptions.PowerActiveImport:
                term = 'ev_charging_points_manager.transactions.measurands.power_active_import';
                break;
            case MeasurandOptions.PowerFactor:
                term = 'ev_charging_points_manager.transactions.measurands.power_factor';
                break;
            case MeasurandOptions.PowerOffered:
                term = 'ev_charging_points_manager.transactions.measurands.power_offered';
                break;
            case MeasurandOptions.PowerReactiveExport:
                term = 'ev_charging_points_manager.transactions.measurands.power_reactive_export';
                break;
            case MeasurandOptions.PowerReactiveImport:
                term = 'ev_charging_points_manager.transactions.measurands.power_reactive_import';
                break;
            case MeasurandOptions.RPM:
                term = 'ev_charging_points_manager.transactions.measurands.rpm';
                break;
            case MeasurandOptions.SoC:
                term = 'ev_charging_points_manager.transactions.measurands.soc';
                break;
            case MeasurandOptions.Temperature:
                term = 'ev_charging_points_manager.transactions.measurands.temperature';
                break;
            case MeasurandOptions.Voltage:
                term = 'ev_charging_points_manager.transactions.measurands.voltage';
                break;
        }
       
        return this.translateService.instant(term);
    }
}
