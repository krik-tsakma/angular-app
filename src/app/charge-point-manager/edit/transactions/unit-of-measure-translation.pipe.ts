﻿// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// TYPES
import { UnitOfMeasureOptions } from './transactions-types';

@Pipe({
    name: 'myUnitOfMeasureTranslation',
    pure: false
})

export class UnitOfMeasureTranslationPipe implements PipeTransform {
    public transform(option: UnitOfMeasureOptions): any {

        let term = UnitOfMeasureOptions[option];

        switch (option) {
            case UnitOfMeasureOptions.Wh: // Watt-hours (energy)
                term = 'Wh';
                break;
            case UnitOfMeasureOptions.kWh: // kiloWatt-hours (energy)
                term = 'kWh';
                break;
            case UnitOfMeasureOptions.varh: // Var-hours (reactive energy)
                term = 'VARh';
                break;
            case UnitOfMeasureOptions.kvarh:  // kilovar-hours (reactive energy)
                term = 'kVARh';
                break;
            case UnitOfMeasureOptions.W: // Watts (power)
                term = 'W';
                break;
            case UnitOfMeasureOptions.kW: // kilowatts (power)
                term = 'kW';
                break;
            case UnitOfMeasureOptions.VA: // VoltAmpere (apparent power)
                term = 'VA';
                break;
            case UnitOfMeasureOptions.kVA: // kiloVolt Ampere (apparent power)
                term = 'kVA';
                break;
            case UnitOfMeasureOptions.var: // Vars (reactive power)
                term = 'VAR';
                break;
            case UnitOfMeasureOptions.kvar: // kilovars (reactive power)
                term = 'kVAR';
                break;
            case UnitOfMeasureOptions.A: // Amperes (current)
                term = 'A';
                break;
            case UnitOfMeasureOptions.V: // Voltage (r.m.s. AC)
                term = 'V';
                break;
            case UnitOfMeasureOptions.Celsius: // Degrees (temperature)
                term = '°C';
                break;
            case UnitOfMeasureOptions.Fahrenheit:  // Degrees (temperature)
                term = '°F';
                break;
            case UnitOfMeasureOptions.K:  // Degrees Kelvon (temperature)
                term = 'K';
                break;
            case UnitOfMeasureOptions.Percent:
                term = '%';
                break;
        }
       
        return term;
    }
}
