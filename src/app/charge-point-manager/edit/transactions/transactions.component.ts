﻿// FRAMEWORK
import {
    Component, OnInit, OnDestroy, ViewEncapsulation,
    ViewChild, ElementRef, Renderer
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// RXJS
import { Observable, Subscription, Subject, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

// COMPONENTS
import { PeriodSelectorComponent } from '../../../common/period-selector/period-selector.component';

// SERVICES
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { ChargingPointManagerService } from '../../charging-point-manager.service';
import { ChargePointNotificationService } from '../../notifications/charge-point-notification.service';
import { StoreManagerService } from '../../../shared/store-manager/store-manager.service';
import { Config } from '../../../config/config';

// TYPES
import {
    EvChargePointManagerTransactionsRequest,
    EvChargePointManagerTransactionItem,
    EvChargingPointTransactionsResults,
    EvChargePointManagerTransactionDetails,
    TransactionMeterValue,
    TransactionVehiclesResultItem,
    TransactionVehiclesRequest,
    MeasurandOptions
} from './transactions-types';
import {
    TableColumnSetting,
    TableColumnSortOptions,
    TableColumnSortOrderOptions,
    TableCellFormatOptions,
    TableActionOptions,
    TableActionItem
} from '../../../shared/data-table/data-table.types';
import { Pager } from '../../../common/pager';
import { PeriodTypes, PeriodTypeOption } from '../../../common/period-selector/period-selector-types';
import { KeyName } from '../../../common/key-name';
import { MeasurandTranslationPipe } from './measurand-translation.pipe';
import { AmChartTypes } from '../../../amcharts/amchart-types';
import { CpStatusChange } from '../../notifications/notification-types';
import { StoreItems } from '../../../shared/store-manager/store-items';

// MOMENT
import moment from 'moment';

// PIPES
import { DateFormatterDisplayOptions, DateFormatPipe } from '../../../shared/pipes/dateFormat.pipe';
import { TimeFormatPipe } from '../../../shared/pipes/timeFormat.pipe';

@Component({
    selector: 'charge-point-transactions',
    templateUrl: './transactions.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./transactions.less'],
    providers: [MeasurandTranslationPipe, DateFormatPipe, TimeFormatPipe]
})

export class TransactionsComponent implements OnInit, OnDestroy {
    @ViewChild(PeriodSelectorComponent, { static: false })
    public periodSelectorComponent: PeriodSelectorComponent;
    @ViewChild('term', { static: true })
    public fileInput: ElementRef;

    public loadingSingle: boolean;
    public loading: boolean;
    public loadingSearchVehicles: boolean;
    public message: string;
    public cpHeaderTitle: string;

    // grid
    public request: EvChargePointManagerTransactionsRequest = {} as EvChargePointManagerTransactionsRequest;
    public pager: Pager = new Pager();
    public tableSettings: TableColumnSetting[];
    public data: EvChargePointManagerTransactionItem[] = [] as EvChargePointManagerTransactionItem[];
    public periodSelectorOptions: PeriodTypes[] = [
        PeriodTypes.LastWeek,
        PeriodTypes.ThisWeek,
        PeriodTypes.Today,
        PeriodTypes.Custom
    ];

    // last transaction
    public dateDisplayOptions: any = DateFormatterDisplayOptions;
    public selectedTransaction: EvChargePointManagerTransactionDetails;
    public meterValues: TransactionMeterValue[];
    public meterValuesLast: TransactionMeterValue[] = [];
    public measurands: KeyName[] = [];
    public chartType = AmChartTypes.Serial;
    public selectedFirstMeasurand: number;
    public selectedSecondMeasurand: number;
    public measurandChartOptions: any;
    public searchVehicleStream: Subject<string> = new Subject();

    public items: Observable<TransactionVehiclesResultItem[]> = this.searchVehicleStream
        .pipe(
            debounceTime(500),
            distinctUntilChanged(),
            switchMap((term) => {
                // use this to empty result set in case of change
                if (term === null) {
                    this.loadingSearchVehicles = false;
                    return of([]);
                } else {
                    this.loadingSearchVehicles = false;
                    return this.service.getTransactionVehicles({
                        chargePointID: this.request.chargePointID,
                        searchTerm: term
                    } as TransactionVehiclesRequest);
                }
            })
        );

    private subscriber: Subscription;
    private stopTransactionSubscriber: Subscription;
    private stateNotificationSubscriber: Subscription;

    constructor(
        public userOptions: UserOptionsService,
        private renderer: Renderer,
        private service: ChargingPointManagerService,
        private activatedRoute: ActivatedRoute,
        private unsubscribe: UnsubscribeService,
        private previousRouteService: PreviousRouteService,
        private router: Router,
        private configService: Config,
        private snackBar: SnackBarService,
        private measurandTraslationPipe: MeasurandTranslationPipe,
        private dateTimeFormatPipe: DateFormatPipe,
        private timeFormatPipe: TimeFormatPipe,
        private cpNotificationService: ChargePointNotificationService,
        private storeManager: StoreManagerService) {
            this.tableSettings = this.getTableSettings();
    }


    // =========================
    // LIFECYCLE
    // =========================

    public ngOnInit() {
        const savedData = this.storeManager.getOption(StoreItems.transactions) as EvChargePointManagerTransactionsRequest;
        if (savedData) {
            this.request = savedData;
            if (savedData.periodType === PeriodTypes.Custom) {
                this.periodSelectorComponent.setCustomDates(
                    moment(savedData.periodSince),
                    moment(savedData.periodTill)
                );
            }
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
            this.request.periodType = PeriodTypes.Today;
        }

        this.activatedRoute
            .queryParams
            .subscribe((params: { cpName: string }) => {
                if (params.cpName) {
                    this.cpHeaderTitle = params.cpName;
                }
            });

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => {
                if (params.id) {
                    this.request.chargePointID = Number(params.id);
                    setTimeout(() => {
                        this.get(true);
                        this.getLastTransaction();

                        this.cpNotificationService.Start();
                        this.stateNotificationSubscriber = this.cpNotificationService
                            .stateNotificationReceived
                            .subscribe((res: CpStatusChange) => {
                                if (res.id !== this.request.chargePointID) {
                                    return;
                                }
                                this.service
                                    .getLastTransaction(this.request.chargePointID)
                                    .subscribe((response: EvChargePointManagerTransactionDetails) => {
                                        if (this.selectedTransaction.id !== response.id) {
                                            return;
                                        }
                                        this.selectedTransaction = response;
                                        this.getMeterValuesForTransaction(
                                            response.id, this.selectedFirstMeasurand, this.selectedSecondMeasurand
                                        );
                                    });

                            });
                    }, 1000);
                }
            });
    }

    public ngOnDestroy() {
        this.unsubscribe.removeSubscription([
            this.subscriber,
            this.stateNotificationSubscriber
        ]);
        this.cpNotificationService.Stop();
    }

    public toDateTime(timestamp): string {
        return moment(timestamp)
            .locale(this.userOptions.localOptions.moment_locale)
            .format('HH:mm:ss');
    }

    // =========================
    // EVENT HANDLERS
    // =========================

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }

    public drawMeasurandChart() {
        this.measurandChartOptions = this.makeMeasurandChart(this.selectedFirstMeasurand, this.selectedSecondMeasurand);
        window['AmCharts'].makeChart('measurand_chart', this.measurandChartOptions);
    }

    // Button that triggers the end of last transaction of the pole
    public stopTransaction() {
        if (this.selectedTransaction) {
            this.unsubscribe.removeSubscription(this.stopTransactionSubscriber);
            this.stopTransactionSubscriber = this.service
                .stopTransaction(this.selectedTransaction.ocppid)
                .subscribe((res: number) => {
                    this.loading = false;
                    this.snackBar.open('ev_charging_points_manager.action.success', 'form.actions.close');
                    this.getTransaction(this.selectedTransaction.id);
                },
                (err) => {
                    this.loading = false;
                    let message = 'form.errors.unknown';
                    if (err.status === 503) {
                        message = 'ev_charging_points_manager.action.error';
                    }
                    this.snackBar.open(message, 'form.actions.close');
                });
        }
    }


    // =========================
    // TABLE / TRANSACTIONS
    // =========================

    // fetch data
    public loadMore() {
        if (this.pager.pageNumber < this.pager.totalPages && this.loading === false) {
            this.pager.addPage();
            this.get(false);
        }
    }

    // When the user selected a column to sort (from list view)
    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }

    public onTransactionDetails(item: TableActionItem) {
        if (!item) {
            return;
        }

        const record = item.record as EvChargePointManagerTransactionItem;
        switch (item.action) {
            case TableActionOptions.details:
                this.getTransaction(record.id);
                break;
            default:
                break;
        }
    }

    public getLastTransaction() {
        this.selectedTransaction = null;
        this.loadingSingle = true;
        this.service
            .getLastTransaction(this.request.chargePointID)
            .subscribe((res: EvChargePointManagerTransactionDetails) => {
                this.selectedTransaction = res;
                this.loadingSingle = false;
                this.getMeterValuesForTransaction(res.id, null, null);
            },
            (err) => {
                this.loadingSingle = false;
                this.data = [];
                this.message = 'data.empty_records';
            });
    }

    // ===========================
    // VEHICLE SEARCH AUTOCOMPLETE
    // ===========================

    public onVehicleSearch(term: any) {
        this.loadingSearchVehicles = true;
        if (term && term.length > 1) {
            this.searchVehicleStream.next(term);
        } else {
            this.loadingSearchVehicles = false;
        }
    }

    public onVehicleSelect(item?: TransactionVehiclesResultItem) {
        if (!item) {
            this.request.vehicleID = null;
            this.request.vehicleChargingID = null;
            this.request.vehicleLabel = null;
            this.get(true);

            return;
        }
        if (item.vehicleID) {
            this.request.vehicleChargingID = null;
            this.request.vehicleID = item.vehicleID;
            this.request.vehicleLabel = item.vehicleLabel;
        } else {
            this.request.vehicleChargingID = item.vehicleChargingID;
            this.request.vehicleID = null;
            this.request.vehicleLabel = item.vehicleChargingID;
        }

        this.get(true);
    }

    public clearInput(e?: Event) {
        // do not bubble event otherwise focus is on the textbox
        if (e) {
            e.stopPropagation();
        }

        // clear selection
        this.onVehicleSelect(null);

        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.fileInput.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }


    // =========================
    // PERIOD SELECTOR CALLBACKS
    // =========================

    // On period select
    public onPeriodSelect(pto: PeriodTypeOption): void {
        this.request.periodType = pto.id;
        this.request.periodSince = pto.datetimeSince.format('YYYY-MM-DD HH:mm');
        this.request.periodTill = pto.datetimeTill.format('YYYY-MM-DD HH:mm');
        this.get(true);
    }

    // =========================
    // PRIVATE
    // =========================

    // Fetch ev charging points
    private get(resetPageNumber: boolean) {
        if (this.loading) {
            return;
        }

        this.loading = true;

        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.pager.pageSize = 20;
            this.data = new Array<EvChargePointManagerTransactionItem>();
        }

        if (!this.request.periodSince) {
            this.request.periodSince = this.periodSelectorComponent.selectedOption.datetimeSince.format('YYYY-MM-DD HH:mm');
            this.request.periodTill = this.periodSelectorComponent.selectedOption.datetimeTill.format('YYYY-MM-DD HH:mm');
        }

        this.request.pageSize = this.pager.pageSize;
        this.request.pageNumber = this.pager.pageNumber;

        this.storeManager.saveOption(StoreItems.transactions, this.request);

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.subscriber);

        // then start the new request
        this.subscriber = this.service
            .getTransactions(this.request)
            .subscribe((response: EvChargingPointTransactionsResults) => {
                this.loading = false;
                if (resetPageNumber === true) {
                    this.data = response && response.results
                        ? response.results
                        : [];
                } else {
                    this.data = this.data.concat(response.results);
                }

                // Get the paging info
                this.pager.pageNumber = response.pageNumber;
                this.pager.pageSize = response.pageSize;
                this.pager.totalPages = response.totalPages;
                this.pager.totalRecords = response.totalRecords;
            },
            (err) => {
                this.loading = false;
                this.data = [];
                this.message = 'data.error';
            });
        return;
    }

    private getTransaction(id: number) {
        this.loadingSingle = true;
        this.service
            .getTransactionByID(id)
            .subscribe((res) => {
                this.selectedTransaction = res;
                this.loadingSingle = false;
                this.getMeterValuesForTransaction(res.id, this.selectedFirstMeasurand, this.selectedSecondMeasurand);
            },
            (err) => {
                this.loadingSingle = false;
                this.data = [];
                this.message = 'data.error';
            });
    }

    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                primaryKey: 'startTimestamp',
                header: 'ev_charging_points_manager.transactions.start',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvChargePointManagerTransactionItem) => {
                    const startT = this.dateTimeFormatPipe.transform(item.startTimestamp, {
                        display: this.dateDisplayOptions.both,
                        dateFormat: this.userOptions.localOptions.short_date,
                        showSeconds: true
                    });
                    return startT;
                },
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'stopTimestamp',
                header: 'ev_charging_points_manager.transactions.stop',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvChargePointManagerTransactionItem) => {
                    if (!item.stopTimestamp) {
                        return 'N/A';
                    }
                    const stopT = this.dateTimeFormatPipe.transform(item.stopTimestamp, {
                        display: this.dateDisplayOptions.both,
                        dateFormat: this.userOptions.localOptions.short_date,
                        showSeconds: true
                    });
                    return stopT;
                },
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'duration',
                header: 'ev_charging_points_manager.transactions.duration',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvChargePointManagerTransactionItem) => {
                    if (!item.duration) {
                        return 'N/A';
                    }
                    const dur = this.timeFormatPipe.transform(item.duration, {
                        format: 'HH:mm:ss',
                        includeDays: true
                    });
                    return dur;
                },
            },
            {
                primaryKey: 'vehicleLabel',
                header: 'ev_charging_points_manager.transactions.vehicle_label',
            },
            {
                primaryKey: 'whTransferred',
                header: 'ev_charging_points_manager.transactions.wh_transferred',
            },
            {
                primaryKey: 'id',
                header: 'ev_charging_points_manager.transactions.transaction_id',
                format: TableCellFormatOptions.string,
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.details],
            }
        ];
    }

    // =========================
    // MEASURANDS
    // =========================

    private getMeterValuesForTransaction(id: number, selectedFirstMeasurand: number, selectedSecondMeasurand: number ) {
        this.loadingSingle = true;
        this.measurands = [];
        this.meterValuesLast = [];
        this.service
            .getMeterValuesForTransaction(id)
            .subscribe((res: TransactionMeterValue[]) => {
                this.loadingSingle = false;

                // meter values come with timestamp sort ascending
                this.meterValues = res;
                this.meterValues.forEach((mv) => {
                    if (!this.measurands.find((x) => x.id === mv.measurand)) {
                        this.measurands.push({
                            id: mv.measurand,
                            name:  this.measurandTraslationPipe.transform(mv.measurand),
                            enabled: true
                        } as KeyName);
                    }
                });

                // sort timestamp descending, to get the last updated values
                const meterValuesSortDescending = Object.create(res, {}).sort((a, b) => {
                    return new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime();
                });
                meterValuesSortDescending.forEach((mv) => {
                    if (!this.meterValuesLast.find((x) => x.measurand === mv.measurand)) {
                        this.meterValuesLast.push(mv);
                    }
                });

                if (this.measurands.length > 0) {
                    const defaultFirstMeasurand = this.measurands.find((measurand) => measurand.id === MeasurandOptions.PowerActiveImport);
                    const defaultLastMeasurand = this.measurands.find((measurand) => measurand.id === MeasurandOptions.SoC);
                    this.selectedFirstMeasurand = selectedFirstMeasurand
                        ? selectedFirstMeasurand
                        : defaultFirstMeasurand
                            ? defaultFirstMeasurand.id
                            : this.measurands[0].id ;
                    this.selectedSecondMeasurand = selectedSecondMeasurand
                        ? selectedSecondMeasurand
                        : defaultLastMeasurand
                            ? defaultLastMeasurand.id
                            : this.measurands[0].id;

                    this.drawMeasurandChart();
                }
            },
            (err) => {
                this.loadingSingle = false;
                this.data = [];
                this.message = 'data.error';
            });
    }

    private  makeMeasurandChart(firstMeasurand: number, secondMeasurand: number): any {
        const firstItems = this.meterValues.filter((x) => x.measurand === firstMeasurand);
        const secondItems = this.meterValues.filter((x) => x.measurand === secondMeasurand);

        const items = [];
        firstItems.forEach((fi) => {
            const si = secondItems.find((sc) => sc.timestamp === fi.timestamp);
            if (si) {
                items.push({
                    timestamp: si.timestamp,
                    firstMeasurand: fi.measurand,
                    secondMeasurand: si.measurand,
                    firstValue: fi.value,
                    secondValue: si.value,
                    firstUnitOfMeasure: fi.unitOfMeasure,
                    secondUnitOfMeasure: si.unitOfMeasure
                });
            }
        });

        return {
            dataProvider: items,
            type: 'serial',
            legend: {
                useGraphSettings: true,
                divId: 'legend',
            },
            categoryField: 'timestamp',
            categoryAxis: {
                categoryFunction: (category: any, dataItem: TransactionMeterValue, categoryAxis: any) => {
                    return this.toDateTime(dataItem.timestamp);
                },
                gridPosition: 'start',
                labelRotation: 10
            },
            valueAxes: [
                {
                    id: 'leftAxis',
                    axisAlpha: 0,
                    gridAlpha: 0,
                    position: 'left',
                    title: this.measurands.find(((x) => x.id === firstMeasurand)).name,
                },
                {
                    id: 'rightAxis',
                    axisAlpha: 0,
                    gridAlpha: 0,
                    position: 'right',
                    title: this.measurands.find(((x) => x.id === secondMeasurand)).name,
                }
            ],
            theme: 'default',
            graphs: [
                {
                    balloonText: '[[category]]<br><b><span style=\'font-size:14px;\'>[[value]]</span></b>',
                    bullet: 'round',
                    bulletSize: 8,
                    lineColor: '#637bb6',
                    lineThickness: 2,
                    negativeLineColor: '#d1655d',
                    legendValueText: '[[value]]',
                    valueField: 'firstValue',
                    valueAxis: 'leftAxis',
                    labelPosition: 'left',
                    title: this.measurands.find(((x) => x.id === firstMeasurand)).name,
                },
                {
                    balloonText: '[[category]]<br><b><span style=\'font-size:14px;\'>[[value]]</span></b>',
                    bullet: 'square',
                    bulletSize: 6,
                    lineColor: '#63b67e',
                    lineThickness: 1,
                    negativeLineColor: '#b55dd1',
                    legendValueText: '[[value]]',
                    valueField: 'secondValue',
                    valueAxis: 'rightAxis',
                    labelPosition: 'right',
                    title: this.measurands.find(((x) => x.id === secondMeasurand)).name,
                }
            ],
            responsive: {
                enabled: true
            }
        };
    }
}
