﻿// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';
import { TransactionMeterValue } from './transactions-types';

@Pipe({
    name: 'myMeterValueDateFilter',
    pure: false
})

export class MeterValueDateFilterPipe implements PipeTransform {
    public transform(meterValues: TransactionMeterValue[], filter: string): any {

        if (!meterValues || !filter) {
            return meterValues;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return meterValues.filter((item) => {
            return item.timestamp === filter;
        });
    }
}
