﻿// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

// RxJS
import { Subscription } from 'rxjs';

// SERVICES
import { SnackBarService } from '../../../../shared/snackbar.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { ChargingPointManagerService } from '../../../charging-point-manager.service';

// TYPES
import { FirmwareUpdateRequest, FirmwareUpdateCodeOptions } from './firmware-update-types';

// PIPES
import { StringFormatPipe } from '../../../../shared/pipes/stringFormat.pipe';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'firmware-update',
    templateUrl: 'firmware-update.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['firmware-update.less'],
})

export class FirmwareUpdateDialogComponent {
    public loading: boolean;
    public data: FirmwareUpdateRequest = {} as FirmwareUpdateRequest;
    public retrieveDatePicker: any;
    public retrieveDateHours: number  = 0;
    public retrieveDateMinutes: number  = 0;
    private serviceSubscriber: Subscription;

    constructor(private dialogRef: MatDialogRef<FirmwareUpdateDialogComponent>,
                private snackBar: SnackBarService,
                private service: ChargingPointManagerService,
                private translator: CustomTranslateService,
                private stringFormatPipe: StringFormatPipe,
                @Inject(MAT_DIALOG_DATA) private ocppid: string) {
        this.data.OCPPID = ocppid;
        this.retrieveDatePicker = moment();
    }

    public closeDialog() {
        this.dialogRef.close();
    }

    public onSubmit({ value, valid }: { value: FirmwareUpdateRequest, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        if (this.serviceSubscriber) {
            this.serviceSubscriber.unsubscribe();
        }

        this.data.retrieveDate = this.retrieveDatePicker
                                    .startOf('day')
                                    .add(this.retrieveDateHours, 'hours')
                                    .add(this.retrieveDateMinutes, 'minutes');

        this.serviceSubscriber = this.service
            .firmwareUpdate(this.data)
            .subscribe((res: FirmwareUpdateCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('ev_charging_points_manager.action.success', 'form.actions.close');
                    this.closeDialog();
                } else {
                    const message = this.getUpdateResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                let message = 'form.errors.unknown';
                if (err.status === 503) {
                    message = 'ev_charging_points_manager.action.error';
                }
                this.snackBar.open(message, 'form.actions.close');
            });
    }

    // =========================
    // RESULTS
    // =========================

    private getUpdateResult(code: FirmwareUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case FirmwareUpdateCodeOptions.FileLocationInvalid:
                message = 'ev_charging_points_manager.perform.invalid_uri';
                break;
            case FirmwareUpdateCodeOptions.FileLocationRequired:
                const required = this.translator.instant('form.errors.required');
                const name = this.translator.instant('company.name');
                message = this.stringFormatPipe.transform(required, [name]);
                break;
            case FirmwareUpdateCodeOptions.ChargePointUrlInvalid:
            case FirmwareUpdateCodeOptions.ChargePointUrlRequired:
            case FirmwareUpdateCodeOptions.OCPPIDRequired:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
