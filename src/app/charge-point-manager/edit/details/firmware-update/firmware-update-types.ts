export interface FirmwareUpdateRequest {
    location: string;
    retrieveDate: string;
    OCPPID: string;
}

export enum FirmwareUpdateCodeOptions {
    Ok = 0,
    ChargePointUrlRequired = 1,
    ChargePointUrlInvalid = 2,
    OCPPIDRequired = 3,
    FileLocationRequired = 4,
    FileLocationInvalid = 5
}
