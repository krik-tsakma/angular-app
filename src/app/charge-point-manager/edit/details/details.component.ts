// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { ChargingPointManagerService } from '../../charging-point-manager.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { UnlockDialogService } from './unlock-dialog/unlock-dialog.service';
import { ChargePointNotificationService } from '../../notifications/charge-point-notification.service';
import { UserOptionsService } from './../../../shared/user-options/user-options.service';
import { StoreManagerService } from './../../../shared/store-manager/store-manager.service';
import { Config } from '../../../config/config';

// COMPONENTS
import { FirmwareUpdateDialogComponent } from './firmware-update/firmware-update.component';
import { PeriodSelectorComponent } from '../../../common/period-selector/period-selector.component';

// TYPES
import {
    EvChargePointDetails,
    ChargePointStatusOptions,
    CpActions,
    ChargePointOperationStatusRequest,
    ChargePointOperationStatusCodeOptions,
    ChargePointResetCodeOptions,
    ChargePointResetRequest,
    UnlockConnectorRequest,
    ChargePointsHistoryActionsRequest,
    ChargePointsHistoryActionsItem,
    ChargePointsHistoryActionsResults
} from '../../charging-point-manager-types';
import {
    TableColumnSetting,
    TableCellFormatOptions,
    TableColumnSortOrderOptions,
    TableColumnSortOptions
} from '../../../shared/data-table/data-table.types';
import { UnlockDialogParams } from './unlock-dialog/unlock-dialog-types';
import { ConnectorStatusOptions, ConnectorErrorCodes, ChargePointConnector } from '../../connectors-shared/charge-point-connector-types';
import { ConfirmationDialogParams } from '../../../shared/confirm-dialog/confirm-dialog-types';
import { PeriodTypes, PeriodTypeOption } from '../../../common/period-selector/period-selector-types';
import { Pager } from '../../../common/pager';
import { StoreItems } from '../../../shared/store-manager/store-items';

// PIPES
import { DateFormatterDisplayOptions } from '../../../shared/pipes/dateFormat.pipe';
import { CpStatusChange } from '../../notifications/notification-types';
import { ChargePointConnectorFormatPipe } from '../../connectors-shared/charge-point-connector-format.pipe';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'charge-point-details',
    templateUrl: './details.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./details.less'],
    providers: [ChargePointConnectorFormatPipe]
})

export class DetailsComponent implements OnInit, OnDestroy {
    @ViewChild(PeriodSelectorComponent, { static: true })
    public periodSelectorComponent: PeriodSelectorComponent;

    // GENERAL PUBLIC
    public skipLocChange = !this.configService.get('router');
    public loading: boolean;
    public historyLoading: boolean;
    public buttonLoading: boolean;
    public message: string;
    public dateDisplayOptions: any = DateFormatterDisplayOptions;
    public periodSelectorOptions: PeriodTypes[] = [
        PeriodTypes.Today,
        PeriodTypes.LastWeek,
        PeriodTypes.ThisWeek,
        PeriodTypes.LastMonth,
        PeriodTypes.ThisMonth,
        PeriodTypes.Custom
    ];

    // Details
    public evChargePointDetails: EvChargePointDetails = {} as EvChargePointDetails;
    public evChargePointHistory: ChargePointsHistoryActionsItem[];
    public tableSettings: TableColumnSetting[];
    public historyRequest: ChargePointsHistoryActionsRequest = {} as ChargePointsHistoryActionsRequest;
    public pager: Pager = new Pager();
    public chargePointStatuses = ChargePointStatusOptions;
    public connectorStatusOptions: any = ConnectorStatusOptions;
    public connectorErrorCodes: any = ConnectorErrorCodes;
    public cpActions: any = CpActions;

    // Private attributes
    private chargePointSubscriber: Subscription;
    private actionsHistorySubscriber: Subscription;
    private resetSubscriber: Subscription;
    private diagnosticsSubscriber: Subscription;

    constructor(
        private activatedRoute: ActivatedRoute,
        private service: ChargingPointManagerService,
        private translate: CustomTranslateService,
        private previousRouteService: PreviousRouteService,
        private unsubscribe: UnsubscribeService,
        private snackBar: SnackBarService,
        private configService: Config,
        private confirmDialogService: ConfirmDialogService,
        private unlockDialogService: UnlockDialogService,
        private router: Router,
        private dialog: MatDialog,
        private cpNotificationService: ChargePointNotificationService,
        private storeManager: StoreManagerService,
        private userOptions: UserOptionsService) {

            this.tableSettings = this.getTableSettings();
            this.historyRequest.periodType = PeriodTypes.Today;
    }


    // =========================
    // LIFECYCLE
    // =========================

    public ngOnInit() {
        const savedData = this.storeManager.getOption(StoreItems.cpActionsHistory) as ChargePointsHistoryActionsRequest;
        if (savedData) {
            this.historyRequest = savedData;
            if (savedData.periodType === PeriodTypes.Custom) {
                this.periodSelectorComponent.setCustomDates(
                    moment(savedData.periodSince),
                    moment(savedData.periodTill)
                );
            }
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.historyRequest.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
            this.historyRequest.periodType = PeriodTypes.Today;
        }

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => {
                if (params.id) {
                    this.get(params.id);

                    this.cpNotificationService.Start();
                    this.cpNotificationService.stateNotificationReceived.subscribe((res: CpStatusChange) => {
                        // console.log('should update cp row', res.id);
                        if (res.id !== this.evChargePointDetails.id) {
                            return;
                        }
                        this.service
                            .getChargingPoint(res.id)
                            .subscribe((response: EvChargePointDetails) => {
                                this.loading = false;
                                this.evChargePointDetails = response;
                            });
                    });
                }
            });
    }

    public ngOnDestroy() {
        this.unsubscribe.removeSubscription([
            this.chargePointSubscriber,
            this.actionsHistorySubscriber,
            this.resetSubscriber,
            this.diagnosticsSubscriber
        ]);
        this.cpNotificationService.Stop();
    }

    // =========================
    // CONNECTORS
    // =========================

    public getZeroConnectorStatus() {
        const connector0 = this.evChargePointDetails.connectors.find((c) => c.id === 0);
        if (connector0) {
            return connector0.status;
        }
        return ConnectorStatusOptions.Faulted;
    }

    // =========================
    // EVENT HANDLERS
    // =========================

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: this.skipLocChange });
    }

    public openFirmwareDialog() {
        this.dialog.open(FirmwareUpdateDialogComponent, {
          data: this.evChargePointDetails.ocppid
        });
    }

    public performAction(action: CpActions, connectors?: ChargePointConnector[]) {
        let title = '';
        if (action === CpActions.UnlockConnector) {
            this.unlockDialogService
                .confirm({
                    connectors: connectors
                } as UnlockDialogParams)
                .subscribe((result: string) => {
                    if (result !== 'no') {
                        this.unlock(result);
                    }
                });
            return;
        }

        switch (action) {
            case CpActions.SoftReset:
                title = 'ev_charging_points_manager.perform.soft_reset';
                break;
            case CpActions.HardReset:
                title = 'ev_charging_points_manager.perform.hard_reset';
                break;
            case CpActions.ClearCache:
                title = 'ev_charging_points_manager.perform.clear_cache';
                break;

            case CpActions.SetOperative:
            case CpActions.SetInoperative:
                title = 'ev_charging_points_manager.perform.status_change';
                break;

        }

        this.confirmDialogService
            .confirm({
                title: title,
                message: 'form.warnings.are_you_sure',
                submitButtonTitle: 'form.actions.ok'
            } as ConfirmationDialogParams)
            .subscribe((result) => {
                if (result === 'yes') {
                    switch (action) {
                        case CpActions.SoftReset:
                            this.reset(false);
                            break;
                        case CpActions.HardReset:
                            this.reset(true);
                            break;
                        case CpActions.ClearCache:
                            this.clearCache();
                            break;
                        case CpActions.SetOperative:
                            this.setOperationStatus(true);
                            break;
                        case CpActions.SetInoperative:
                            this.setOperationStatus(false);
                            break;
                    }
                }
            });
    }

    public loadMore() {
        if (this.pager.pageNumber < this.pager.totalPages && this.historyLoading === false) {
            this.pager.addPage();
            this.getHistory(false);
        }
    }


    // When the user selected a column to sort (from list view)
    public onSort(column: TableColumnSetting) {
        this.historyRequest.sorting = column.sorting.stringFormat(column.primaryKey);
        this.getHistory(true);
    }



    // =========================
    // PERIOD SELECTOR CALLBACKS
    // =========================

    // On period select
    public onPeriodSelect(pto: PeriodTypeOption): void {
        this.historyRequest.periodType = pto.id;
        this.historyRequest.periodSince = pto.datetimeSince.format('YYYY-MM-DD HH:mm');
        this.historyRequest.periodTill = pto.datetimeTill.format('YYYY-MM-DD HH:mm');
        if (this.historyRequest.chargePointID) {
            this.getHistory(true);
        }
    }


    // =========================
    // PRIVATE
    // =========================

    // Fetch ev charging points
    private get(id: number) {
        if (this.loading) {
            return;
        }

        this.loading = true;

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.chargePointSubscriber);

        // then start the new request
        this.chargePointSubscriber = this.service
            .getChargingPoint(id)
            .subscribe((response: EvChargePointDetails) => {
                this.loading = false;
                this.evChargePointDetails = response;
                this.getHistory(true);
            },
            () => {
                this.loading = false;
                this.evChargePointDetails = null;
                this.message = 'data.error';
            });
    }

    private getHistory(resetPageNumber: boolean) {
        if (this.historyLoading) {
            return;
        }
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.pager.pageSize = 20;
            this.evChargePointHistory = new Array<ChargePointsHistoryActionsItem>();
        }

        this.historyLoading = true;

        if (!this.historyRequest.periodSince) {
            this.historyRequest.periodSince = this.periodSelectorComponent.selectedOption.datetimeSince.format('YYYY-MM-DD HH:mm');
            this.historyRequest.periodTill = this.periodSelectorComponent.selectedOption.datetimeTill.format('YYYY-MM-DD HH:mm');
        }

        this.historyRequest.chargePointID = this.evChargePointDetails.id;
        this.historyRequest.pageSize = this.pager.pageSize;
        this.historyRequest.pageNumber = this.pager.pageNumber;

        this.storeManager.saveOption(StoreItems.cpActionsHistory, this.historyRequest);

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.actionsHistorySubscriber);

        // then start the new request
        this.actionsHistorySubscriber = this.service
            .getCPMHistory(this.historyRequest)
            .subscribe((response: ChargePointsHistoryActionsResults) => {
                this.historyLoading = false;
                if (resetPageNumber === true) {
                    this.evChargePointHistory = response && response.results
                        ? response.results
                        : [];
                } else {
                    this.evChargePointHistory = this.evChargePointHistory.concat(response.results);
                }

                // Get the paging info
                this.pager.pageNumber = response.pageNumber;
                this.pager.pageSize = response.pageSize;
                this.pager.totalPages = response.totalPages;
                this.pager.totalRecords = response.totalRecords;
            },
            (err) => {
                this.historyLoading = false;
                this.evChargePointHistory = [];
                this.message = 'data.error';
            });
    }

    private setOperationStatus(setOperative: boolean) {
        this.buttonLoading = true;
        this.unsubscribe.removeSubscription(this.diagnosticsSubscriber);
        this.diagnosticsSubscriber = this.service
            .setOperationStatus({
                chargePointID: this.evChargePointDetails.id,
                setOperative: setOperative
            } as ChargePointOperationStatusRequest)
            .subscribe((res: ChargePointOperationStatusCodeOptions) => {
                this.buttonLoading = false;
                if (res === null) {
                    this.snackBar.open('ev_charging_points_manager.action.success', 'form.actions.close');
                    this.getHistory(true);
                } else {
                    const message = this.getOperationStatusResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.buttonLoading = false;
                let message = 'form.errors.unknown';
                if (err.status === 503) {
                    message = 'ev_charging_points_manager.action.error';
                }
                this.snackBar.open(message, 'form.actions.close');
            });
    }


    private reset(hardReset: boolean) {
        this.buttonLoading = true;
        this.unsubscribe.removeSubscription(this.diagnosticsSubscriber);
        this.diagnosticsSubscriber = this.service
            .reset({
                chargePointID: this.evChargePointDetails.id,
                hardReset: hardReset
            } as ChargePointResetRequest)
            .subscribe((res: ChargePointResetCodeOptions) => {
                this.buttonLoading = false;
                if (res === null) {
                    this.snackBar.open('ev_charging_points_manager.action.success', 'form.actions.close');
                    this.getHistory(true);
                } else {
                    const message = this.getResetResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.buttonLoading = false;
                let message = 'form.errors.unknown';
                if (err.status === 503) {
                    message = 'ev_charging_points_manager.action.error';
                }
                this.snackBar.open(message, 'form.actions.close');
            });
    }



    private clearCache() {
        this.buttonLoading = true;
        this.unsubscribe.removeSubscription(this.diagnosticsSubscriber);
        this.diagnosticsSubscriber = this.service
            .clearCache({ chargePointID: this.evChargePointDetails.id })
            .subscribe((res: ChargePointResetCodeOptions) => {
                this.buttonLoading = false;
                if (res === null) {
                    this.snackBar.open('ev_charging_points_manager.action.success', 'form.actions.close');
                    this.getHistory(true);
                } else {
                    const message = this.getResetResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.buttonLoading = false;
                let message = 'form.errors.unknown';
                if (err.status === 503) {
                    message = 'ev_charging_points_manager.action.error';
                }
                this.snackBar.open(message, 'form.actions.close');
            });
    }



    private unlock(connector: string) {
        this.service
            .unlock(
                {
                    chargePointID: this.evChargePointDetails.id,
                    connectorID: connector
                } as UnlockConnectorRequest
            )
            .subscribe((res) => {
                console.log('unlock response', res);
                this.getHistory(true);
            });
    }
    // =========================
    // RESULTS
    // =========================

    private getOperationStatusResult(code: ChargePointOperationStatusCodeOptions): string {
        let message = '';
        switch (code) {
            case ChargePointOperationStatusCodeOptions.ChargePointUrlInvalid:
            case ChargePointOperationStatusCodeOptions.ChargePointUrlRequired:
            case ChargePointOperationStatusCodeOptions.OCPPIDRequired:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getResetResult(code: ChargePointResetCodeOptions): string {
        let message = '';
        switch (code) {
            case ChargePointResetCodeOptions.ChargePointUrlInvalid:
            case ChargePointResetCodeOptions.ChargePointUrlRequired:
            case ChargePointResetCodeOptions.OCPPIDRequired:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                primaryKey: 'logTimestamp',
                header: 'ev_charging_points_manager.details.history.timestamp',
                format: TableCellFormatOptions.dateTime,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.desc, true, null)
            },
            {
                primaryKey: 'userLabel',
                header: 'ev_charging_points_manager.details.history.user',
            },
            {
                primaryKey: 'action',
                header: 'ev_charging_points_manager.details.history.action',
                format: TableCellFormatOptions.custom,
                    formatFunction: (record: ChargePointsHistoryActionsItem) => {
                        switch(record.action) {
                            case CpActions.SoftReset: {
                                return this.translate.instant('ev_charging_points_manager.perform.soft_reset');
                            }
                            case CpActions.HardReset: {
                                return this.translate.instant('ev_charging_points_manager.perform.hard_reset');
                            }
                            case CpActions.SetOperative: {
                                return this.translate.instant('ev_charging_points_manager.perform.set_operative');
                            }
                            case CpActions.SetInoperative: {
                                return this.translate.instant('ev_charging_points_manager.perform.set_inoperative');
                            }
                            case CpActions.ClearCache: {
                                return this.translate.instant('ev_charging_points_manager.perform.clear_cache');
                            }
                            case CpActions.UnlockConnector: {
                                return this.translate.instant('ev_charging_points_manager.perform.unlock');
                            }
                            case CpActions.UpdateFirmware: {
                                return this.translate.instant('ev_charging_points_manager.perform.firmware_update');
                            }

                        }
                    }
            }
        ];
    }
}
