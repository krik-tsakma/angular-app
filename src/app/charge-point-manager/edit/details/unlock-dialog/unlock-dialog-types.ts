﻿import { ChargePointConnector } from '../../../connectors-shared/charge-point-connector-types';

export interface UnlockDialogParams {    
    connectors: ChargePointConnector [];
}
