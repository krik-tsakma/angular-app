﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { UnlockDialogComponent } from './unlock-dialog.component';

// TYPES
import { UnlockDialogParams } from './unlock-dialog-types';


@Injectable()
export class UnlockDialogService {

    constructor(private dialog: MatDialog) { }

    public confirm(params: UnlockDialogParams): Observable<string> {
        const dialogRef: MatDialogRef<UnlockDialogComponent> = this.dialog.open(UnlockDialogComponent);
        dialogRef.componentInstance.connectors = params.connectors;

        return dialogRef.afterClosed();
    }
}
