﻿// FRAMEWORK
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';

// TYPES
import { KeyName } from '../../../../common/key-name';
import { ChargePointConnector } from '../../../connectors-shared/charge-point-connector-types';

// PIPES
import { ChargePointConnectorFormatPipe } from '../../../connectors-shared/charge-point-connector-format.pipe';

@Component({
    selector: 'unlock-dialog',
    templateUrl: './unlock-dialog.html',
    providers: [ ChargePointConnectorFormatPipe  ]
})

export class UnlockDialogComponent implements OnInit {
    public connectors: ChargePointConnector[] = [];  
    public showOptions: KeyName[] = [];  
    public selectedConnector: KeyName;


    constructor(
        public dialogRef: MatDialogRef<UnlockDialogComponent>,
        public formatConnecetor: ChargePointConnectorFormatPipe,
        private translator: CustomTranslateService) { }

    public ngOnInit() {
        if (this.connectors.length > 0) {
            this.showOptions = this.connectors
                .filter((con) => {
                    return con.id !== 0;
                })
                .map((connector) => {                        
                    return {
                        id: connector.id,
                        term: this.formatConnecetor.transform(connector, { hideZeroConnector: false }, true),
                        name: connector.status.toString(),
                        enabled: true
                    };                    
                });
                
            this.showOptions.unshift({
                id: null,
                name: this.translator.instant('form.actions.select') + '...',
                enabled: false,
            });            
        }
    }
    public changeConnector(event: KeyName) {
        this.selectedConnector = this.showOptions.find((opt) => opt.id === event);
    }

}
