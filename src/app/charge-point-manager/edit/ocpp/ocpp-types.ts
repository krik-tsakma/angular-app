import { PagerResults, PagerRequest } from '../../../common/pager';

export interface EvChargePointManagerOcppRequest extends PagerRequest {
    chargePointID: number;
    date: string;
    showErrors: boolean;
    hideHeartbeats: boolean;
}

export interface EvChargePointOcppResults extends PagerResults {
    results: EvChargePointManagerOcppItem[];
}

export interface EvChargePointManagerOcppItem {
    ocppid: string;
    timestamp: string;
    messageType: ChargePointMessageType;
    errorCode?: string;
    errorDescription?: string;
}

export enum ChargePointMessageType {
    BootNotificationReq = 0,
    BootNotificationConf = 1,
    HeartbeatReq = 2,
    HeartbeatConf = 3,
    MeterValuesReq = 4,
    MeterValuesConf = 5,
    RemoteStartTransactionReq = 6,
    RemoteStartTransactionConf = 7,
    RemoteStopTransactionReq = 8,
    RemoteStopTransactionConf = 9,
    StatusNotificationReq = 10,
    StatusNotificationConf = 11,
    StartTransactionReq = 12,
    StartTransactionConf = 13,
    StopTransactionReq = 14,
    StopTransactionConf = 15,
    ResetReq = 16,
    ResetConf = 17,
    GetDiagnosticsReq = 18,
    GetDiagnosticsConf = 19,
    ChangeAvailabilityReq = 20,
    ChangeAvailabilityConf = 21,
    UpdateFirmwareReq = 22,
    UpdateFirmwareConf = 23
}

export enum ChargePointMessageErrorCodes {
    NotImplemented = 0,
    NotSupported = 1,
    InternalError = 2,
    ProtocolError = 3,
    SecurityError = 4,
    FormationViolation = 5,
    PropertyConstraintViolation = 6,
    OccurenceConstraintViolation = 7,
    TypeConstraintViolation = 8,
    GenericError = 9
}
