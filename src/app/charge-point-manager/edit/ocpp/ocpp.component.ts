﻿// FRAMEWORK
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';

// RXJS
import { Subscription } from 'rxjs';

// MOMENT
import moment from 'moment';

// SERVICES
import { ChargingPointManagerService } from '../../charging-point-manager.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { ChargePointNotificationService } from '../../notifications/charge-point-notification.service';
import { StoreManagerService } from '../../../shared/store-manager/store-manager.service';
import { Config } from '../../../config/config';

// COMPONENTS
import { OcppDetailsDialogComponent } from './details/details.component';

// TYPES
import {
    TableColumnSetting,
    TableColumnSortOptions,
    TableColumnSortOrderOptions,
    TableActionOptions,
    TableCellFormatOptions,
    TableActionItem
} from '../../../shared/data-table/data-table.types';
import { Pager } from '../../../common/pager';
import {
    EvChargePointManagerOcppRequest,
    EvChargePointManagerOcppItem,
    ChargePointMessageType,
    EvChargePointOcppResults,
    ChargePointMessageErrorCodes
} from './ocpp-types';
import { KeyName } from '../../../common/key-name';
import { DateFormatterDisplayOptions, DateFormatPipe } from '../../../shared/pipes/dateFormat.pipe';
import { CpStatusChange } from '../../notifications/notification-types';
import { StoreItems } from '../../../shared/store-manager/store-items';

@Component({
    selector: 'charge-point-ocpp',
    templateUrl: './ocpp.html',
    providers: [DateFormatPipe]
})

export class OcppComponent implements OnInit, OnDestroy {
    public loading: boolean;
    public message: string;
    public cpHeaderTitle: string;

    // grid
    public request: EvChargePointManagerOcppRequest = {} as EvChargePointManagerOcppRequest;
    public pager: Pager = new Pager();
    public tableSettings: TableColumnSetting[];
    public data: EvChargePointManagerOcppItem[] = [] as EvChargePointManagerOcppItem[];

    public ocppDate: moment.Moment = moment().startOf('day');

    private subscriber: Subscription;

    constructor(
        public userOptions: UserOptionsService,
        private service: ChargingPointManagerService,
        private unsubscribe: UnsubscribeService,
        private activatedRoute: ActivatedRoute,
        private previousRouteService: PreviousRouteService,
        private router: Router,
        private dialog: MatDialog,
        private configService: Config,
        private dateTimeFormatPipe: DateFormatPipe,
        private cpNotificationService: ChargePointNotificationService,
        private storeManager: StoreManagerService) {
            this.tableSettings = this.getTableSettings();
    }

    // =========================
    // LIFECYCLE
    // =========================

    public ngOnInit() {
        const savedData = this.storeManager.getOption(StoreItems.ocpp) as EvChargePointManagerOcppRequest;
        if (savedData) {
            this.request = savedData;
            this.ocppDate = moment(this.request.date);
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.hideHeartbeats = true;
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.activatedRoute
            .queryParams
            .subscribe((params: { cpName: string }) => {
                if (params.cpName) {
                    this.cpHeaderTitle = params.cpName;
                }
            });

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => {
                if (params.id) {
                    this.request.chargePointID = params.id;
                    this.get(true);

                    this.cpNotificationService.Start();
                    this.cpNotificationService.stateNotificationReceived.subscribe((res: CpStatusChange) => {
                        const today: moment.Moment = moment();
                        if (today.diff(this.ocppDate, 'days') === 0 && res.id === this.request.chargePointID) {
                            this.get(true);
                        }
                    });
                }
            });
    }


    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.subscriber);
        this.cpNotificationService.Stop();
    }


    // =========================
    // EVENT HANDLERS
    // =========================

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }

    public fetchData() {
        this.get(true);
    }

    // =========================
    // TABLE
    // =========================

    public onOCPPDetails(item: TableActionItem) {
        if (!item) {
            return;
        }

        const record = item.record as EvChargePointManagerOcppItem;
        switch (item.action) {
            case TableActionOptions.details:
                this.dialog.open(OcppDetailsDialogComponent, {
                    data: record
                });
                break;
            default:
                break;
        }
    }

    // fetch data in OCPP list table
    public loadMore() {
        if (this.pager.pageNumber < this.pager.totalPages && this.loading === false) {
            this.pager.addPage();
            this.get(false);
        }
    }


    // Shorting OCPP results
    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }


    // =========================
    // PRIVATE
    // =========================

    // Get OCPPs List
    private get(resetPageNumber: boolean) {
        if (this.loading) {
            return;
        }
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.pager.pageSize = 20;
            this.data = [] as EvChargePointManagerOcppItem[];
        }

        this.message = '';
        this.loading = true;

        this.request.pageSize = this.pager.pageSize;
        this.request.pageNumber = this.pager.pageNumber;
        this.request.date = this.ocppDate.format('YYYY-MM-DD HH:mm');

        this.storeManager.saveOption(StoreItems.ocpp, this.request);
        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.subscriber);

        this.subscriber = this.service
            .getOcpp(this.request)
            .subscribe((response: EvChargePointOcppResults) => {
                this.loading = false;
                if (resetPageNumber === true) {
                    this.data = response && response.results
                        ? response.results
                        : [];
                } else {
                    this.data = this.data.concat(response.results);
                }

                // Get the paging info
                this.pager.pageNumber = response.pageNumber;
                this.pager.pageSize = response.pageSize;
                this.pager.totalPages = response.totalPages;
                this.pager.totalRecords = response.totalRecords;
            },
            () => {
                    this.loading = false;
                    this.data = [];
                    this.message = 'data.error';
                });
        return;
    }

    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                primaryKey: 'timestamp',
                header: 'ev_charging_points_manager.ocpp.time_received_sent',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvChargePointManagerOcppItem) => {
                    if (!item.timestamp) {
                        return 'N/A';
                    }
                    const startT = this.dateTimeFormatPipe.transform(item.timestamp, {
                        display: DateFormatterDisplayOptions.both,
                        dateFormat: this.userOptions.localOptions.short_date,
                        showSeconds: true
                    });
                    return startT;
                },
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'messageType',
                header: 'ev_charging_points_manager.ocpp.message',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvChargePointManagerOcppItem) => {
                    const message = this.ocppRequests().find((x) => x.id === Number(item.messageType));
                    return message !== undefined
                        ? message.name
                        : 'N/A';
                },
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'errorCode',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvChargePointManagerOcppItem) => {
                    if (item.errorCode === null || item.errorCode === undefined) {
                        return 'N/A';
                    }

                    const code = this.ocppErrorCodes().find((x) => x.id === Number(item.errorCode));
                    return code !== undefined
                        ? code.name
                        : 'N/A';
                },
                header: 'ev_charging_points_manager.ocpp.error_code',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'errorDescription',
                header: 'ev_charging_points_manager.ocpp.error_message',
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.details],
            }
        ];
    }

     private ocppErrorCodes(): KeyName[] {
        return [
            {
                id: ChargePointMessageErrorCodes.NotImplemented,
                name: 'Not implemented'
            },
            {
                id: ChargePointMessageErrorCodes.NotSupported,
                name: 'Not supported'
            },
            {
                id: ChargePointMessageErrorCodes.InternalError,
                name: 'Internal error'
            },
            {
                id: ChargePointMessageErrorCodes.ProtocolError,
                name: 'Protocol error'
            },
            {
                id: ChargePointMessageErrorCodes.SecurityError,
                name: 'Security error'
            },
            {
                id: ChargePointMessageErrorCodes.FormationViolation,
                name: 'Formation violation'
            },
            {
                id: ChargePointMessageErrorCodes.PropertyConstraintViolation,
                name: 'Property constraint violation'
            },
            {
                id: ChargePointMessageErrorCodes.OccurenceConstraintViolation,
                name: 'Occurence constraint violation'
            },
            {
                id: ChargePointMessageErrorCodes.TypeConstraintViolation,
                name: 'Type constraint violation'
            },
            {
                id: ChargePointMessageErrorCodes.GenericError,
                name: 'Generic error'
            },
        ];
    }

    private ocppRequests(): KeyName[] {
        return [
            {
                id: ChargePointMessageType.BootNotificationReq,
                name: 'BootNotification.req'
            },
            {
                id: ChargePointMessageType.BootNotificationConf,
                name: 'BootNotification.conf'
            },
            {
                id: ChargePointMessageType.HeartbeatReq,
                name: 'Heartbeat.req'
            },
            {
                id: ChargePointMessageType.HeartbeatConf,
                name: 'Heartbeat.conf'
            },
            {
                id: ChargePointMessageType.MeterValuesReq,
                name: 'MeterValues.req'
            },
            {
                id: ChargePointMessageType.MeterValuesConf,
                name: 'MeterValues.conf'
            },
            {
                id: ChargePointMessageType.RemoteStartTransactionReq,
                name: 'RemoteStartTransaction.req'
            },
            {
                id: ChargePointMessageType.RemoteStartTransactionConf,
                name: 'RemoteStartTransaction.conf'
            },
            {
                id: ChargePointMessageType.RemoteStopTransactionReq,
                name: 'RemoteStopTransaction.req'
            },
            {
                id: ChargePointMessageType.RemoteStopTransactionConf,
                name: 'RemoteStopTransaction.conf'
            },
            {
                id: ChargePointMessageType.StatusNotificationReq,
                name: 'StatusNotification.req'
            },
            {
                id: ChargePointMessageType.StatusNotificationConf,
                name: 'StatusNotification.conf'
            },
            {
                id: ChargePointMessageType.StartTransactionReq,
                name: 'StartTransaction.req'
            },
            {
                id: ChargePointMessageType.StartTransactionConf,
                name: 'StartTransaction.conf'
            },
            {
                id: ChargePointMessageType.StopTransactionReq,
                name: 'StopTransaction.req'
            },
            {
                id: ChargePointMessageType.StopTransactionConf,
                name: 'StopTransaction.conf'
            },
            {
                id: ChargePointMessageType.ResetReq,
                name: 'Reset.req'
            },
            {
                id: ChargePointMessageType.ResetConf,
                name: 'Reset.conf'
            },
            {
                id: ChargePointMessageType.GetDiagnosticsReq,
                name: 'GetDiagnostic.req'
            },
            {
                id: ChargePointMessageType.GetDiagnosticsConf,
                name: 'GetDiagnostics.conf'
            },
            {
                id: ChargePointMessageType.ChangeAvailabilityReq,
                name: 'ChangeAvailability.req'
            },
            {
                id: ChargePointMessageType.ChangeAvailabilityConf,
                name: 'ChangeAvailability.conf'
            },
            {
                id: ChargePointMessageType.UpdateFirmwareReq,
                name: 'UpdateFirmware.req'
            },
            {
                id: ChargePointMessageType.UpdateFirmwareConf,
                name: 'UpdateFirmware.conf'
            },
        ];
    }
}
