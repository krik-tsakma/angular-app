import { ChargePointMessageType } from '../ocpp-types';

export interface OcppDetailsRequest {
    timestamp: string;
    OCPPID: string;
    messageType: ChargePointMessageType;
}
