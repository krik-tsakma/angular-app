﻿// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

// RxJS
import { Subscription } from 'rxjs/Subscription';

// SERVICES
import { SnackBarService } from '../../../../shared/snackbar.service';
import { ChargingPointManagerService } from '../../../charging-point-manager.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';

// TYPES
import { OcppDetailsRequest } from './details-types';
import { EvChargePointManagerOcppItem } from '../ocpp-types';

@Component({
    selector: 'ocpp-details',
    templateUrl: 'details.html',
    encapsulation: ViewEncapsulation.None
})

export class OcppDetailsDialogComponent implements OnInit {
    public loading: boolean;
    public request: OcppDetailsRequest = {} as OcppDetailsRequest;
    public jsonData: string;
    private serviceSubscriber: Subscription;

    constructor(private dialogRef: MatDialogRef<OcppDetailsDialogComponent>,
                private snackBar: SnackBarService,
                private service: ChargingPointManagerService,
                private translator: CustomTranslateService,
                @Inject(MAT_DIALOG_DATA) private req: any) {
        this.request = {
            timestamp: req.timestamp,
            OCPPID: req.ocppid,
            messageType: req.messageType
        } as OcppDetailsRequest;
    }

    public ngOnInit(): void {
        this.get();
    }

    public closeDialog() {
        this.dialogRef.close();
    }

    public get() {
        this.loading = true;
        if (this.serviceSubscriber) {
            this.serviceSubscriber.unsubscribe();
        }

        this.serviceSubscriber = this.service
            .ocppDetails(this.request)
            .subscribe((res: string) => {
                this.loading = false;
                this.jsonData = res;
            },
            (err) => {
                this.loading = false;
                let message = 'form.errors.unknown';
                if (err.status === 503) {
                    message = 'ev_charging_points_manager.action.error';
                }
                this.snackBar.open(message, 'form.actions.close');
            });
    }
}
