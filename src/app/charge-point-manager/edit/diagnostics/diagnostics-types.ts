import { PagerRequest, PagerResults } from '../../../common/pager';

// ====================
// CREATE DIAGNOSTIC
// ====================

export interface CreateChargePointDiagnosticRequest {
    SinceDateTime: string;
    TillDateTime: string;
    OCPPID: number;
}

export enum CreateChargePointDiagnosticCodeOptions {
    Ok = 0,  
    DateTimeError = 1,
    OCPPIDRequired = 2
}



// ====================
// GET DIAGNOSTICS
// ====================

export interface ChargePointDiagnosticItem {
    id: number;
    requestTimestamp: string;
    sinceTimestamp: string;
    tillTimestamp: string;
    userFullname?: string;
    fileName?: string;
    fileSize?: number;
    status: ChargePointDiagnosticsLogStatus;
}

export interface ChargePointDiagnosticsResult extends PagerResults {
    results: ChargePointDiagnosticItem[];
}

export interface ChargePointDiagnosticsLogEntriesRequest extends PagerRequest {
    id: number;
    periodType: number;
    since: string;
    till: string;
    status?: ChargePointDiagnosticsLogStatus;
}

export enum ChargePointDiagnosticsLogStatus {
       Requested = 0, /// request to CP has been answered with a filename for the pending upload
       Confirmed = 1, /// GetDiagnostics.conf has been received
       Uploading = 2, /// file upload has started but is not done
       Uploaded = 3,  /// file upload finished
       Failed = 4 /// file upload failed
}

