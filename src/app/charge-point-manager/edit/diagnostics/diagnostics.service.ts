// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../../../config/config';
import { AuthHttp } from '../../../auth/authHttp';

// TYPES
import { 
    CreateChargePointDiagnosticRequest, 
    CreateChargePointDiagnosticCodeOptions, 
    ChargePointDiagnosticsLogEntriesRequest,
    ChargePointDiagnosticsResult,
    ChargePointDiagnosticItem
 } from './diagnostics-types';


@Injectable()
export class DiagnosticsService {
    private baseApiURL: string;

    constructor(
        private authHttp: AuthHttp,
        private configService: Config) {
            this.baseApiURL = this.configService.get('apiUrl') + '/api/EvChargePointManager';
    }


    // CREATE DIAGNOSTIC
    public createDignostic(entity: CreateChargePointDiagnosticRequest): Observable<CreateChargePointDiagnosticCodeOptions>  {
        return this.authHttp
            .post(this.baseApiURL + '/GetDiagnosticsLog', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as CreateChargePointDiagnosticCodeOptions;
                    }
                })          
            );
    }



    // GET ALL DIAGNOSTICS
    public getDignostics(req: ChargePointDiagnosticsLogEntriesRequest): Observable<ChargePointDiagnosticsResult>  {
        const obj = this.authHttp.removeObjNullKeys({                        
            id: req.id,    
            since: req.since,
            till: req.till,
            status: (req.status !== null && req.status !== undefined) ? req.status.toString() : null,
            pageNumber: req.pageNumber,
            pageSize: req.pageSize,
            sorting: req.sorting ? req.sorting.toString() : null
        });

        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.baseApiURL + '/GetDiagnosticsLogEntries', { params });
                   
    }



	// GET SPECIFIC DIAGNOSTIC
    public getDiagnosticsLogEntry(id: number): Observable<ChargePointDiagnosticItem>  {       
        return this.authHttp
            .get(this.baseApiURL + '/GetDiagnosticsLogEntry/' + id);
                   
    }



	// DOWNLOAD DIAGNOSTIC LOG 
    public downloadDiagnosticsLog(id: number) {
        return this.authHttp
            .post(this.baseApiURL + '/DownloadDiagnosticsLog', { id: id },  { observe: 'response', responseType: 'blob' })
            .pipe(
                map((res) => {                    
                    return res.body;                    
                })
            );
    }


}
