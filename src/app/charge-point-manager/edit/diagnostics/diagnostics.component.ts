// FRAMEWORK
import {
    Component, OnInit, OnDestroy,
    ViewChild, ViewEncapsulation
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// RxJS
import { Subscription } from 'rxjs';

// COMPONENTS
import { PeriodSelectorComponent } from '../../../common/period-selector/period-selector.component';

// SERVICES
import { SnackBarService } from '../../../shared/snackbar.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { ChargePointNotificationService } from '../../notifications/charge-point-notification.service';
import { DiagnosticsService } from './diagnostics.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { StoreManagerService } from '../../../shared/store-manager/store-manager.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES
import {
    ChargePointDiagnosticsLogStatus,
    ChargePointDiagnosticItem,
    ChargePointDiagnosticsLogEntriesRequest,
    ChargePointDiagnosticsResult,
    CreateChargePointDiagnosticCodeOptions,
    CreateChargePointDiagnosticRequest
} from './diagnostics-types';
import {
    TableColumnSetting,
    TableColumnSortOptions,
    TableColumnSortOrderOptions,
    TableCellFormatOptions,
    TableActionOptions,
    TableActionItem,
} from '../../../shared/data-table/data-table.types';
import { PeriodTypes, PeriodTypeOption } from '../../../common/period-selector/period-selector-types';
import { Pager } from '../../../common/pager';
import { KeyName } from '../../../common/key-name';
import { CpDiagnosticsChange } from '../../notifications/notification-types';
import { StoreItems } from '../../../shared/store-manager/store-items';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'diagnostics',
    templateUrl: 'diagnostics.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./diagnostics.less'],
})

export class DiagnosticsComponent implements OnInit, OnDestroy {
    @ViewChild(PeriodSelectorComponent, { static: true }) public periodSelectorComponent: PeriodSelectorComponent;

    public loading: boolean;
    public cpHeaderTitle: string;
    public loadingNewDiagnostic: boolean;
    public readyForTableRefresh: boolean;
    public downloading: boolean;
    public message: string;
    public sinceDate: moment.Moment = moment().subtract(1, 'days');
    public sinceDateHours: number  = Number(moment().subtract(1, 'days').format('HH'));
    public sinceDateMinutes: number  = Number(moment().subtract(1, 'days').format('mm'));
    public tillDate: moment.Moment = moment();
    public tillDateHours: number = Number(moment().format('HH'));
    public tillDateMinutes: number  = Number(moment().format('mm'));
    public tableSettings: TableColumnSetting[];
    public periodSelectorOptions: PeriodTypes[] = [
        PeriodTypes.LastWeek,
        PeriodTypes.ThisWeek,
        PeriodTypes.Today,
        PeriodTypes.Custom,
        PeriodTypes.LastMonth,
        PeriodTypes.ThisMonth,
    ];
    public statusOptions: KeyName[] = [];

    public request: ChargePointDiagnosticsLogEntriesRequest = {} as ChargePointDiagnosticsLogEntriesRequest;
    public pager: Pager = new Pager();
    public diagnosticsData: ChargePointDiagnosticItem[] = [] as ChargePointDiagnosticItem[];
    private serviceSubscriber: Subscription;
    private diagnosticsSubscriber: Subscription;
    private downloadDiagnosticSubsciber: Subscription;
    private getDiagnosticsLogEntrySubsciber: Subscription;

    constructor(private router: Router,
                private previousRouteService: PreviousRouteService,
                private translate: CustomTranslateService,
                private configService: Config,
                private activatedRoute: ActivatedRoute,
                private snackBar: SnackBarService,
                private service: DiagnosticsService,
                private storeManager: StoreManagerService,
                private unsubscribe: UnsubscribeService,
                private cpNotificationService: ChargePointNotificationService) {
        this.tableSettings = this.getTableSettings();
        this.statusOptions = this.getStatusOptions();

    }

    public ngOnInit() {
        const savedData = this.storeManager.getOption(StoreItems.diagnostics) as ChargePointDiagnosticsLogEntriesRequest;
        if (savedData) {
            this.request = savedData;
            if (savedData.periodType === PeriodTypes.Custom) {
                this.periodSelectorComponent.setCustomDates(
                    moment(savedData.since),
                    moment(savedData.till)
                );
            }
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
            this.request.periodType = PeriodTypes.Today;
        }

        this.activatedRoute
            .queryParams
            .subscribe((params: { cpName: string }) => {
                if (params.cpName) {
                    this.cpHeaderTitle = params.cpName;
                }
            });

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => {
                if (!params.id) {
                    return;
                }
                this.request.id = Number(params.id);
                setTimeout(() => {
                    this.getDiagnostics(true);
                    this.cpNotificationService.Start();
                    this.cpNotificationService.diangosticsNotificationReceived.subscribe((res: CpDiagnosticsChange) => {

                        if (this.getDiagnosticsLogEntrySubsciber) {
                            this.getDiagnosticsLogEntrySubsciber.unsubscribe();
                        }

                        const log = this.diagnosticsData.find((c) => c.id === Number(res.id));

                        this.getDiagnosticsLogEntrySubsciber = this.service
                            .getDiagnosticsLogEntry(res.id)
                            .subscribe((response: ChargePointDiagnosticItem) => {
                                // update table data, do it this way to invoke angular's change detection
                                if (!log) {
                                    this.readyForTableRefresh = true;
                                    return;
                                }
                                this.diagnosticsData = this.diagnosticsData.map((diagnostic) => {
                                    return diagnostic.id !== res.id ? diagnostic : response;
                            });
                    });
                });
            }, 1000);
        });

    }

    public ngOnDestroy() {
        this.unsubscribe.removeSubscription([
            this.diagnosticsSubscriber,
            this.downloadDiagnosticSubsciber,
            this.getDiagnosticsLogEntrySubsciber
        ]);
        this.cpNotificationService.Stop();
    }



    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }



    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.getDiagnostics(true);
    }



    // When new log file is ready, you can click the button to refresh data table
    public refreshTable() {
        this.request.sorting = 'requestTimestamp desc';
        this.getDiagnostics(true);
        this.readyForTableRefresh = false;
    }



    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }
        this.pager.addPage();
        this.getDiagnostics(false);
    }


    public onStatusSelect(id?: number) {
        this.getDiagnostics(true);
    }



    public onDownloadDiagnostic(item: TableActionItem) {
        if (this.downloadDiagnosticSubsciber) {
            this.downloadDiagnosticSubsciber.unsubscribe();
        }
        this.downloading = true;
        const record = item.record as ChargePointDiagnosticItem;
        this.downloadDiagnosticSubsciber = this.service.downloadDiagnosticsLog(record.id)
            .subscribe((res: any) => {
                const link = document.createElement('a');
                link.href = window.URL.createObjectURL(res);
                link.download = record.fileName;
                link.click();

                this.downloading = false;

            }, (err) => {
                this.downloading = false;
                if (err.status === 500) {
                    this.snackBar.open('ev_charging_points_manager.diagnostics.error.file_not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            });
    }



    // On period select
    public onPeriodSelect(pto: PeriodTypeOption): void {
        this.request.periodType = pto.id;
        this.request.since = pto.datetimeSince.format('YYYY-MM-DD HH:mm');
        this.request.till = pto.datetimeTill.format('YYYY-MM-DD HH:mm');
        this.getDiagnostics(true);
    }



    public createDiagnostic() {
        this.loadingNewDiagnostic = true;
        if (this.serviceSubscriber) {
            this.serviceSubscriber.unsubscribe();
        }

        const selectedSinceDate = this.sinceDate.startOf('day')
                                        .add(this.sinceDateHours, 'hours')
                                        .add(this.sinceDateMinutes, 'minutes')
                                        .format('YYYY-MM-DD HH:mm');
        const selectedTillDate = this.tillDate.startOf('day')
                                        .add(this.tillDateHours, 'hours')
                                        .add(this.tillDateMinutes, 'minutes')
                                        .format('YYYY-MM-DD HH:mm');

        const createDiagnosticLog: CreateChargePointDiagnosticRequest = {
            OCPPID: this.request.id,
            SinceDateTime: moment().diff(selectedSinceDate) > 0
                ? selectedSinceDate
                : moment().format('YYYY-MM-DD HH:mm'),
            TillDateTime: moment().diff(selectedTillDate) > 0
            ? selectedTillDate
            : moment().format('YYYY-MM-DD HH:mm'),
        };

        this.serviceSubscriber = this.service
            .createDignostic(createDiagnosticLog)
            .subscribe((res: CreateChargePointDiagnosticCodeOptions) => {
                this.loadingNewDiagnostic = false;
                if (res === null) {
                    this.snackBar.open('ev_charging_points_manager.action.success', 'form.actions.close');
                } else {
                    const message = this.createDiagnosticResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loadingNewDiagnostic = false;
                let message = 'form.errors.unknown';
                if (err.status === 503) {
                    message = 'ev_charging_points_manager.action.error';
                }
                this.snackBar.open(message, 'form.actions.close');
            });
    }



    private getDiagnostics(resetPageNumber: boolean) {
        if (this.loading || !this.request.since || !this.request.till) {
            return;
        }

        this.loading = true;

        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.pager.pageSize = 20;
            this.diagnosticsData = new Array<ChargePointDiagnosticItem>();
        }

        this.request.pageSize = this.pager.pageSize;
        this.request.pageNumber = this.pager.pageNumber;

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.diagnostics, this.request);

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.diagnosticsSubscriber);

        // then start the new request
        this.diagnosticsSubscriber = this.service
            .getDignostics(this.request)
            .subscribe((response: ChargePointDiagnosticsResult) => {
                this.loading = false;
                if (resetPageNumber === true) {
                    this.diagnosticsData = response && response.results
                        ? response.results
                        : [];
                } else {
                    this.diagnosticsData = this.diagnosticsData.concat(response.results);
                }

                // Get the paging info
                this.pager.pageNumber = response.pageNumber;
                this.pager.pageSize = response.pageSize;
                this.pager.totalPages = response.totalPages;
                this.pager.totalRecords = response.totalRecords;
            },
            () => {
                this.loading = false;
                this.diagnosticsData = [];
                this.message = 'data.error';
            });
    }



    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                primaryKey: 'requestTimestamp',
                header: 'ev_charging_points_manager.diagnostics.request_timestamp',
                format: TableCellFormatOptions.dateTime,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'sinceTimestamp',
                header: 'ev_charging_points_manager.diagnostics.since',
                format: TableCellFormatOptions.dateTime
            },
            {
                primaryKey: 'tillTimestamp',
                header: 'ev_charging_points_manager.diagnostics.till',
                format: TableCellFormatOptions.dateTime
            },
            {
                primaryKey: 'userFullname',
                header: 'ev_charging_points_manager.diagnostics.user',
            },
            {
                primaryKey: 'fileName',
                header: 'ev_charging_points_manager.diagnostics.file_name',
            },
            {
                primaryKey: 'fileSize',
                header: 'ev_charging_points_manager.diagnostics.file_size',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: ChargePointDiagnosticItem) => {

                    if (!item.fileSize) {
                        return 'N/A';
                    }

                    return item.fileSize <= 1024
                        ? item.fileSize + ' KB'
                        : (item.fileSize / 1024).toFixed(1) + ' MB';
                }
            },
            {
                primaryKey: 'status',
                header: 'ev_charging_points_manager.diagnostics.req_status',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: ChargePointDiagnosticItem ) => {
                    let message = '';
                    switch (item.status) {
                        case ChargePointDiagnosticsLogStatus.Uploading:
                            message = 'ev_charging_points_manager.diagnostics.statuses.uploading';
                            break;
                        case ChargePointDiagnosticsLogStatus.Uploaded:
                            message = 'ev_charging_points_manager.diagnostics.statuses.uploaded';
                            break;
                        case ChargePointDiagnosticsLogStatus.Confirmed:
                            message = 'ev_charging_points_manager.diagnostics.statuses.confirmed';
                            break;
                        case ChargePointDiagnosticsLogStatus.Failed:
                            message = 'ev_charging_points_manager.diagnostics.statuses.failed';
                            break;
                        default:
                            message = 'ev_charging_points_manager.diagnostics.statuses.requested';
                            break;
                    }

                    return this.translate.instant(message);
                }
            },
            {
                primaryKey: ' ',
                header: '',
                actions: [TableActionOptions.download],
                actionsVisibilityFunction: (action: TableActionOptions, item: ChargePointDiagnosticItem ): boolean => {
                    if (!item || item.status !== ChargePointDiagnosticsLogStatus.Uploaded) {
                        return false;
                    }
                    return true;
                }
            }
        ];
    }

    private createDiagnosticResult(code: CreateChargePointDiagnosticCodeOptions): string {
        let message = '';
        switch (code) {
            case CreateChargePointDiagnosticCodeOptions.DateTimeError:
                message = 'ev_charging_points_manager.perform.invalid_uri';
                break;
            case CreateChargePointDiagnosticCodeOptions.OCPPIDRequired:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getStatusOptions(): KeyName[] {
        return [
            {
                id: null,
                name: this.translate.instant('form.actions.all'),
                enabled: true
            },
            {
                id: ChargePointDiagnosticsLogStatus.Requested,
                name: this.translate.instant('ev_charging_points_manager.diagnostics.statuses.requested'),
                enabled: true
            },
            {
                id: ChargePointDiagnosticsLogStatus.Confirmed,
                name: this.translate.instant('ev_charging_points_manager.diagnostics.statuses.confirmed'),
                enabled: true
            },
            {
                id: ChargePointDiagnosticsLogStatus.Uploading,
                name: this.translate.instant('ev_charging_points_manager.diagnostics.statuses.uploading'),
                enabled: true
            },
            {
                id: ChargePointDiagnosticsLogStatus.Uploaded,
                name: this.translate.instant('ev_charging_points_manager.diagnostics.statuses.uploaded'),
                enabled: true
            },
            {
                id: ChargePointDiagnosticsLogStatus.Failed,
                name: this.translate.instant('ev_charging_points_manager.diagnostics.statuses.failed'),
                enabled: true
            },
        ];
    }

}
