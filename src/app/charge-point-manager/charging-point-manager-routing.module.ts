﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/authGuard';

// COMPONENTS
import { ChargingPointsManagerComponent } from './view/charging-points-manager.component';
import { DetailsComponent } from './edit/details/details.component';
import { DiagnosticsComponent } from './edit/diagnostics/diagnostics.component';
import { TransactionsComponent } from './edit/transactions/transactions.component';
import { OcppComponent } from './edit/ocpp/ocpp.component';

const ChargingPointManagerRoutes: Routes = [
    {
        path: '',
        component: ChargingPointsManagerComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'details/:id',
        component: DetailsComponent,
        canActivate: [AuthGuard],
        // data: {
        //     breadcrumb: 'ev_charging_points_manager.details.title'
        // }
    },
    {
        path: 'diagnostics/:id',
        component: DiagnosticsComponent,
        canActivate: [AuthGuard],      
    },
    {
        path: 'transactions/:id',
        component: TransactionsComponent,
        canActivate: [AuthGuard],        
    },
    {
        path: 'ocpp/:id',
        component: OcppComponent,
        canActivate: [AuthGuard],        
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ChargingPointManagerRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class ChargingPointManagerRoutingModule { }
