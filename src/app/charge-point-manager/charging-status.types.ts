import { CustomTranslateService } from '../shared/custom-translate.service';
import { KeyName } from '../common/key-name';

export enum EvChargingStatuses {
    NotConnected = 0,
    NotCharging = 1,
    Charging = 2,
    Interrupted = 3,
    Finished = 4,
    OtherVehicleSpecific = 5,
    OtherChargerSpecific = 6
}

export let ChargingStatuses: KeyName[] = [
    {
        id: EvChargingStatuses.NotConnected,
        name: 'not connected',
        term: 'ev_charging_monitor.status.not_connected',
    },
    {
        id: EvChargingStatuses.NotCharging,
        name: 'not charging',
        term: 'ev_charging_monitor.status.not_charging',
    },
    {
        id: EvChargingStatuses.Interrupted,
        name: 'interrupted',
        term: 'ev_charging_monitor.status.interrupted',
    },
    {
        id: EvChargingStatuses.OtherVehicleSpecific,
        name: 'pantograph error',
        term: 'ev_charging_monitor.status.pantographError',
    },
    {
        id: EvChargingStatuses.Charging,
        name: 'charging',
        term: 'ev_charging_monitor.status.charging',
    },
    {
        id: EvChargingStatuses.Finished,
        name: 'finished',
        term: 'ev_charging_monitor.status.finished',
    },
    {
        id: EvChargingStatuses.OtherChargerSpecific,
        name: 'charger error',
        term: 'ev_charging_monitor.status.chargerError',
    }
];

export class EvChargingStatus {

    public static formatChargingStatus(chargingStatus: EvChargingStatuses, translate: CustomTranslateService) {
        let className = '';               
        switch (chargingStatus) {
            case EvChargingStatuses.NotConnected:
            case EvChargingStatuses.NotCharging:
            case EvChargingStatuses.OtherVehicleSpecific:
            case EvChargingStatuses.OtherChargerSpecific:
            case EvChargingStatuses.Interrupted:
                className = 'error';
                break;
            default:
                break;
        }        
        const chargingStatusItem = ChargingStatuses.find((status) => status.id === chargingStatus);
        let chargingStatusLabel = 'N/A';
        if (chargingStatusItem) {
            chargingStatusLabel = translate.instant(chargingStatusItem.term);
        }
        return `<span class="charging-status ${className}">${chargingStatusLabel}</span>`;
    }
}
