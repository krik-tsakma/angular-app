﻿import { PagerRequest, PagerResults } from '../common/pager';
import { PeriodTypes } from '../common/period-selector/period-selector-types';
import { CustomTranslateService } from '../shared/custom-translate.service';
import { ChargePointConnector } from './connectors-shared/charge-point-connector-types';


// ====================
// COMMON
// ====================

export enum ChargePointStatusOptions {
    Available = 0,
    Preparing = 1,
    Charging = 2,
    SuspendedEVSE = 3,
    SuspendedEV = 4,
    Finishing = 5,
    Reserved = 6,
    Unavailable = 7,
    Faulted = 8,
    Unknown = 99
}

interface EvChargePointManagerCommon {
    id: number;
    name: string;
    serialNumber: string;
    model: string;
    firmwareVersion: string;
    connectors: ChargePointConnector[];
}



// ====================
// GRID VIEW
// ====================

export interface EvChargingPointsRequest extends PagerRequest {
    showMap: boolean;
    chargePointStatus?: ChargePointStatusOptions;    
    groupID?: number;
}

export interface EvChargingPointsResults extends PagerResults {
    results: EvChargingPointResultItem[];
}

export interface EvChargingPointResultItem extends EvChargePointManagerCommon {        
    chargePointStatus: ChargePointStatusOptions;
    vehicleLabel: string;
    soC?: number;       
    latitude: number;
    longitude: number;
    lastUpdate: string;
}

export enum ChargePointManagerViewOptions {
    details = 1,
    transactions = 2,
    diagnostics = 3,
    ocpp = 4 
}

export interface ChargePointManagerViewOptionsDialogCloseResult {
    action: ChargePointManagerViewOptions;
    element: EvChargingPointResultItem;
}

export class EvChargingPointStatus {

    public static formatStatus(status: ChargePointStatusOptions, translate: CustomTranslateService) {
        let className = '';
        let title = '';
        switch (status) {
            case ChargePointStatusOptions.Available:
                className = 'available';
                title = 'ev_charging_points_manager.statuses.available';
                break;
            case ChargePointStatusOptions.Faulted:
                className = 'faulted';
                title = 'ev_charging_points_manager.statuses.faulted';
                break;
            case ChargePointStatusOptions.Unavailable:
                className = 'unavailable';
                title = 'ev_charging_points_manager.statuses.unavailable';
                break;
            case ChargePointStatusOptions.Charging:
                className = 'unavailable';
                title = 'ev_charging_points_manager.statuses.charging';
                break;
            case ChargePointStatusOptions.Finishing:
                className = 'unavailable';
                title = 'ev_charging_points_manager.statuses.finishing';
                break;
            case ChargePointStatusOptions.Preparing:
                className = 'unavailable';
                title = 'ev_charging_points_manager.statuses.preparing';
                break;
            case ChargePointStatusOptions.Reserved:
                className = 'unavailable';
                title = 'ev_charging_points_manager.statuses.reserved';
                break;
            case ChargePointStatusOptions.SuspendedEV:  
                className = 'unavailable';
                title = 'ev_charging_points_manager.statuses.suspendedEV';
                break;
            case ChargePointStatusOptions.SuspendedEVSE:
                className = 'unavailable';
                title = 'ev_charging_points_manager.statuses.suspendedEVSE';
                break;
            case ChargePointStatusOptions.Unknown:
                className = 'unknown';
                title = 'ev_charging_points_manager.statuses.unknown';
                break;
            default:
                break;
        }       

        title = translate.instant(title);
        return `<span class="charging-status ${className}">${ title }</span>`;
    }
}



// ====================
// DETAILS
// ====================

export interface EvChargePointDetails extends EvChargePointManagerCommon { 
    ocppid: string;      
    vendor: string;
    meterType: string;
    meterSerialNumber: string;
    imsi?: string;
    iccid?: string;
    ipAddress: string;
    lastHeartbeat: string;
    bootTime: string;
}


// ====================
// RESET
// ====================

export enum CpActions {
    SoftReset = 1,
    HardReset = 2,
    SetOperative = 3,
    SetInoperative = 4,
    ClearCache = 5,
    UnlockConnector = 6,
    UpdateFirmware = 7
}

export interface ChargePointResetRequest {
    chargePointID: number;
    hardReset: boolean;
}

export enum ChargePointResetCodeOptions {
    Ok = 0,
    ChargePointUrlRequired = 1,
    ChargePointUrlInvalid = 2,
    OCPPIDRequired = 3
}


// ====================
// UNLOCK
// ====================
export interface UnlockConnectorRequest {
    chargePointID: number;
    connectorID: string;
}


// ====================
// OPERATION STATUS
// ====================

export interface ChargePointOperationStatusRequest {
    chargePointID: number;
    setOperative: boolean;
}

export enum ChargePointOperationStatusCodeOptions {
    Ok = 0,
    ChargePointUrlRequired = 1,
    ChargePointUrlInvalid = 2,
    OCPPIDRequired = 3
}



// ====================
// CPM HISTORY
// ====================

export interface ChargePointsHistoryActionsRequest extends PagerRequest {
    chargePointID: number;
    periodType: PeriodTypes;
    periodSince: string;
    periodTill: string;
}

export interface ChargePointsHistoryActionsItem {    
    userLabel: string;
    logtimestamp: string;    
    action: CpActions;
}

export interface ChargePointsHistoryActionsResults extends PagerResults {
    results: ChargePointsHistoryActionsItem[];
}
