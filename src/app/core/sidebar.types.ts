import { TripReviewPropertyOptions } from './../auth/acl.types';
import { ModuleOptions, EVOperationsPropertyOptions } from '../auth/acl.types';
import { AuthService } from '../auth/authService';

export interface SideBar {
    routeParam: string;
    icon: string;
    title: string;
    showNav?: boolean;
    parent?: string;
    children?: SideBar[];
    opened?: boolean;
    hasAccess: boolean;
}

export class SidebarOptions {
    private as: AuthService;

    constructor(authService: AuthService) {
        this.as = authService;
    }

    public Get(): SideBar[] {
        return [
            {
                title: 'sidebar.dashboard',
                routeParam: 'dashboard',
                icon: 'dashboard',
                hasAccess: this.as.userHasAccess(ModuleOptions.Dashboard),
                showNav: true
            },
            {
                title: 'sidebar.trip_review',
                routeParam: 'trip-review',
                icon: 'trip-review',
                hasAccess: this.as.userHasAccess(ModuleOptions.TripReview, TripReviewPropertyOptions.ViewTrips),
                showNav: true
            },
            {
                title: 'sidebar.follow_up',
                routeParam: 'follow-up',
                icon: 'follow-up',
                hasAccess: this.as.userHasAccess(ModuleOptions.FollowUp),
                showNav: true
            },
            {
                title: 'sidebar.events',
                routeParam: 'events',
                icon: 'events',
                hasAccess: this.as.userHasAccess(ModuleOptions.Events),
                showNav: true
            },
            {
                title: 'sidebar.ev_operations',
                routeParam: 'ev-operations',
                icon: 'ev-operations',
                showNav: true,
                opened: false,
                hasAccess: this.as.userHasAccess(ModuleOptions.EVOperations),
                children: [
                    {
                        title: 'sidebar.ev_operations.vehicles',
                        routeParam: 'ev-operations/vehicles',
                        icon: 'ev-vehicles',
                        showNav: true,
                        hasAccess: this.as.userHasAccess(ModuleOptions.EVOperations, EVOperationsPropertyOptions.Vehicles),
                        parent: 'ev-operations'
                    },
                    {
                       title: 'sidebar.ev_operations.charging_monitor',
                       routeParam: 'ev-operations/charging-monitor',
                       icon: 'ev-charging-monitor',
                       showNav: true,
                       hasAccess: this.as.userHasAccess(ModuleOptions.EVOperations, EVOperationsPropertyOptions.ChargingMonitor),
                       parent: 'ev-operations'
                    }
                ]
            },
            {
                title: 'sidebar.ev_operations.charge_points_manager',
                routeParam: 'charge-points-manager',
                icon: 'ev-charge-points-manager',
                showNav: true,
                hasAccess: this.as.userHasAccess(ModuleOptions.ChargePointManager),
            },
            {
                title: 'sidebar.book_n_go',
                routeParam: 'bookings',
                icon: 'bookings',
                hasAccess: this.as.userHasAccess(ModuleOptions.Bookings),
                showNav: true
            },
            {
                title: 'sidebar.reporting',
                routeParam: 'reporting',
                icon: 'reporting',
                showNav: true,
                opened: false,
                hasAccess: this.as.userHasAccess(ModuleOptions.Reports),
                children: [
                    {
                        title: 'sidebar.reporting.reports',
                        routeParam: 'reports',
                        icon: 'reports',
                        showNav: true,
                        hasAccess: this.as.userHasAccess(ModuleOptions.Reports),
                        parent: 'reporting'
                    },
                    {
                        title: 'sidebar.reporting.subscriptions',
                        routeParam: 'subscriptions',
                        icon: 'subscriptions',
                        showNav: true,
                        hasAccess: this.as.userHasAccess(ModuleOptions.Reports),
                        parent: 'reporting'
                    }
                ]
            },
            {
                title: 'sidebar.fiscal_trips',
                routeParam: 'fiscal-trips',
                icon: 'fiscal-trips',
                hasAccess: this.as.userHasAccess(ModuleOptions.FiscalTripReview),
                showNav: true
            },
            {
                title: 'sidebar.driver_take_over',
                routeParam: 'driver-take-over',
                icon: 'driver-take-over',
                hasAccess: this.as.userHasAccess(ModuleOptions.DriverTakeOver),
                showNav: true
            },
            {
                title: 'sidebar.admin',
                routeParam: 'admin',
                icon: 'admin',
                hasAccess: this.as.userHasAccess(ModuleOptions.Admin),
                showNav: true
            }
        ];
    }
}
