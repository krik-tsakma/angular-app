﻿// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICE
import { AuthService } from '../../../auth/authService';

@Component({
    selector: 'access-denied',
    templateUrl: 'access-denied.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['../page-error.less']
})

export class AccessDeniedComponent {

    constructor(
        public authService: AuthService,
        public router: Router) {

            this.authService.isLoggedIn();
    }

}
