﻿// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'unexpected-error',
    templateUrl: 'unexpected-error.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['../page-error.less']
})

export class UnexpectedErrorComponent {
    constructor(public router: Router) {
            // foo
    }

}
