﻿// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICE
import { AuthService } from '../../../auth/authService';

@Component({
    selector: 'not-found',
    templateUrl: '404error.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['../page-error.less']
})

export class PageNotFoundComponent {

    constructor(
        public authService: AuthService,
        public router: Router) {
            
            this.authService.isLoggedIn();
    }

}
