// FRAMEWORK
import { Injectable } from '@angular/core';

@Injectable()
export class TranslateStateService {
    public cachedArray: string[] = [];
    // only setters and getters can access
    private trans: any = {};

    constructor() { }

    get translations() {
        return this.trans;
    }

    set translations(val) {
        const key = Object.keys(val)[0];
        if (Object.keys(this.trans).length === 0 ||
            Object.keys(this.trans).find((tran) => tran === key) === undefined) {
            this.trans = Object.assign(this.trans, val);
        } else {
            this.trans[key] = Object.assign(this.trans[key], val[key]);
        }
    }

    public removeTranslations() {
        this.trans = {};
    }

}
