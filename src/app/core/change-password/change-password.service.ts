// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';
import { ChangePasswordRequest, ChangePasswordResponseCond } from './change-password.types';

@Injectable()
export class ChangePasswordService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) { 
            this.baseURL = config.get('apiUrl') + '/api/User';
    }

    public change(req: ChangePasswordRequest): Observable<{}>  {
        return this.authHttp
            .put(this.baseURL + '/ChangePassword', req, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server                    
                    if (res.status === 200 && !res.body) {
                        return ChangePasswordResponseCond.PasswordChanged;
                    } else {
                        return res.body;
                    }
                })  
            );
    }

    public shouldCaptcha() {
        return this.authHttp.get(this.baseURL + '/ShouldCaptcha', { observe: 'response' });            
    }

    public logFailedPasswordChange() {
        return this.authHttp.post(this.baseURL + '/LogFailedPasswordChange', null, { observe: 'response' })
        .pipe(
            map((res) => {
                // this is means empty Ok response from the server
                if (res.status === 200 && !res.body) {
                    return null;
                } else  {
                    return res.body;
                }
            })
        );  
    }
}
