export interface ChangePasswordRequest {
    oldPassword: string;
    newPassword: string;
    confirmNewPassword: string;
    recaptchaReactive?: string;
}

export enum ChangePasswordResponseCond {
    PasswordChanged = 0,
    PasswordEmpty = -1,
    PasswordsDoNotMatch = -2,
    UserDoesNotExist = -3,
    OldPasswordInvalid = -4,
}
