// FRAMEWORK
import { Component, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// COMPONENTS
import { RecaptchaComponent } from 'ng-recaptcha';  

// SERVICES
import { ChangePasswordService } from './change-password.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { AuthService } from '../../auth/authService';
import { UnsubscribeService } from '../../shared/unsubscribe.service';

// PIPES
import { FormValidationMessagePipe } from '../../shared/pipes/form-validation-message.pipe';
import { TranslatePipe } from '@ngx-translate/core';
import { StringFormatPipe } from '../../shared/pipes/stringFormat.pipe';
import { ChangePasswordRequest, ChangePasswordResponseCond } from './change-password.types';
import { PasswordRulesFromOptions } from '../../shared/password-rules/password-rules-types';

@Component({
    selector: 'change-password',
    templateUrl: 'change-password.html',
    encapsulation: ViewEncapsulation.None,
    providers: [ChangePasswordService, FormValidationMessagePipe, StringFormatPipe, TranslatePipe]
})

export class ChangePasswordDialogComponent implements OnDestroy {
    @ViewChild('captcha', { static: false }) public captcha: RecaptchaComponent;
    public loading: boolean;
    public showRecaptcha: boolean = false;
    public request: ChangePasswordRequest = {} as ChangePasswordRequest;
    public passwordRulesOptions: any = PasswordRulesFromOptions;
    public isPasswordValid: boolean;

    private serviceSubscriber: any;

    constructor(private authService: AuthService,
                private dialogRef: MatDialogRef<ChangePasswordDialogComponent>,
                private service: ChangePasswordService,
                private snackBar: SnackBarService,
                private unsubscribe: UnsubscribeService) {
        // foo
    }

    // Clean forgetSubscribe to avoid memory leak
    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }

    public closeDialog() {
        this.dialogRef.close();
    }

    // =========================
    // EVENT HANDLERS
    // =========================

    public onCaptchaResolved(captchaResponse: string) {
        this.request.recaptchaReactive = captchaResponse;
    }

    public onPasswordValidation(isValid: boolean): void {
        this.isPasswordValid = isValid;
    }


    public onChangePassword({ value, valid }: { value: string, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }

        this.loading = true;

        const changePass = () => {
            this.unsubscribe.removeSubscription(this.serviceSubscriber);
            this.serviceSubscriber = this.service
                .change(this.request)
                .subscribe((res) => {
                    this.loading = false;
                    switch (res) {
                        case ChangePasswordResponseCond.PasswordChanged:
                            this.snackBar.open('reset_password.success', 'form.actions.ok');
                            this.closeDialog();
                            break;
                        case ChangePasswordResponseCond.PasswordEmpty:
                            this.service.logFailedPasswordChange().subscribe();                             
                            this.snackBar.open('reset_password.password_empty', 'form.actions.ok');                           
                            break;
                        case ChangePasswordResponseCond.PasswordsDoNotMatch:
                            this.snackBar.open('reset_password.confirmation_mismatch', 'form.actions.ok');                          
                            break;
                        case ChangePasswordResponseCond.UserDoesNotExist:
                            this.service.logFailedPasswordChange().subscribe();                            
                            this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                            break;
                        case ChangePasswordResponseCond.OldPasswordInvalid:
                            this.service.logFailedPasswordChange().subscribe();
                            this.snackBar.open('reset_password.old_password_invalid', 'form.actions.ok');
                            break;
                        default:
                            this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }

                    if (this.captcha) {
                        this.captcha.reset(); 
                    }
                    
                },
                (err) => {
                    this.loading = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                });
        };

        this.service
            .shouldCaptcha()
            .subscribe((res: any) => {                
                if (res.status !== 200) {
                    return;
                }    

                if (!this.showRecaptcha && res.body) {
                    this.showRecaptcha = true;                    
                    this.loading = false;                    
                    return;                   
                }

                if (this.request.recaptchaReactive) {
                    this.authService
                        .performCaptchaCheck({ captchaResponse: this.request.recaptchaReactive })
                        .subscribe(
                            (respond: any) => {
                                if (!respond) {
                                    changePass();
                                } else {
                                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                                }
                            },
                            (err) => {
                                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                            }
                        );  
                } else {
                    changePass();
                }                                          
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    
    }
}
