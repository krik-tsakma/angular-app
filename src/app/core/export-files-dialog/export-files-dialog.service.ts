// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

// COMPONENTS
import { ExportFilesDialogComponent } from './export-files-dialog.component';

// TYPES
import { ExportFileView } from '../../admin/import-export/import-export.types';

@Injectable()
export class ExportFilesDialogService {
    public returnArray: Subject<ExportFileView[]> = new Subject<ExportFileView[]>();
    private dialogRef: MatDialogRef<ExportFilesDialogComponent>;
    private isDialogOpen: boolean = false;

    constructor(private dialog: MatDialog) { }       

    public openDialog(params: ExportFileView[]) {   
       /* Check if there a dialog open
        * if there is no dialog open, open it, otherwise only append the new data
        */            
        if (this.isDialogOpen === false) {           
            this.dialogRef = this.dialog.open(ExportFilesDialogComponent);
            this.isDialogOpen = true;
        }

        this.dialogRef.componentInstance.request = params;                        
        this.dialogRef
            .afterClosed()
            .subscribe((res: any) => {
                this.isDialogOpen = false;
                if (res && res.remove) {                               
                    this.returnArray.next(res.newArray);
                }
            });
    }

} 
