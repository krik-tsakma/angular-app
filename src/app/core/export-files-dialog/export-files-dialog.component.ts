// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// TYPES
import { KeyName } from '../../common/key-name';
import { DateFormatterDisplayOptions } from '../../shared/pipes/dateFormat.pipe';
import { ExportFileView, ExportFileCodeOptions, ImportFileCodeOptions, CodeType } from '../../admin/import-export/import-export.types';

@Component({
    selector: 'export-files-dialog',
    templateUrl: './export-files-dialog.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./export-files-dialog.less']
})

export class ExportFilesDialogComponent implements OnInit {
    public loading: boolean;
    public dateDisplayOptions: any = DateFormatterDisplayOptions;
    public codeType: any = CodeType;
    public importFileCodeOptions: any = ImportFileCodeOptions;
    public request: ExportFileView[] = [];    
    public exportFileCodes: KeyName[] = [
        {
            id: ExportFileCodeOptions.Exported,
            name: 'export_files.export.messages.exported',
            term: 'success',
            enabled: true
        },
        {
            id: ExportFileCodeOptions.ErrorRetrievingDataForExport,
            name: 'export_files.messages.export.error_retrieving_data_for_export',
            term: 'error',
            enabled: false
        },
        {
            id: ExportFileCodeOptions.ErrorWritingToFile,
            name: 'export_files.export.messages.error_writing_to_file',
            term: 'error',
            enabled: false
        },
        {
            id: ExportFileCodeOptions.ErrorUploadingFile,
            name: 'export_files.export.messages.error_uploading_file',
            term: 'error',
            enabled: false
        },
        {
            id: ExportFileCodeOptions.UnknownError,
            name: 'export_files.export.messages.unknown_error',
            term: 'error',
            enabled: false
        },        
    ];     
    public importFileCodes: KeyName[] = [
        {
            id: ImportFileCodeOptions.Imported,
            name: 'export_files.import.messages.imported',
            term: 'success',
            enabled: false
        },
        {
            id: ImportFileCodeOptions.ErrorReadingFromFile,
            name: 'export_files.import.messages.error_reading_from_file',
            term: 'error',
            enabled: false
        },
        {
            id: ImportFileCodeOptions.ErrorWritingToErrorsFile,
            name: 'export_files.import.messages.error_writing_to_errors_file',
            term: 'error',
            enabled: false
        },
        {
            id: ImportFileCodeOptions.ErrorUploadingErrorsFile,
            name: 'export_files.import.messages.error_uploading_errors_file',
            term: 'error',
            enabled: false
        },
        {
            id: ImportFileCodeOptions.ErrorDownloadingImportFile,
            name: 'export_files.import.messages.error_downloading_import_file',
            term: 'error',
            enabled: false
        },  
        {
            id: ImportFileCodeOptions.ImportCompletedWithErrors,
            name: 'export_files.import.messages.import_completed_with_errors',
            term: 'error',
            enabled: true
        },    
        {
            id: ImportFileCodeOptions.UnknownError,
            name: 'export_files.import.messages.unknown_error',
            term: 'error',
            enabled: false
        },          
    ];

    constructor(public dialogRef: MatDialogRef<ExportFilesDialogComponent>) { }


    public ngOnInit() { 
        if (this.request && this.request.length > 0) {
            this.request.forEach((req) => {             
                const founded = req.codeType === CodeType.export 
                    ? this.exportFileCodes.find((code) => code.id === req.code)
                    : this.importFileCodes.find((code) => code.id === req.code);           
                req.message = founded 
                    ? founded.name
                    : '';
                req.error = founded && founded.term === 'error'
                    ? true
                    : false;
                req.download = founded.enabled;
            });
        }
    }

    public removeFromList(file?: ExportFileView) {        
        if (this.request && this.request.length > 0) {
            this.loading = true;            
            setInterval(() => { 
                this.request = file 
                    ? this.request.filter((req) => req.timestamp !== file.timestamp)
                    : [];           
                this.loading = false;
                this.dialogRef.close({remove: true, newArray: this.request});
            }, 2000);
            
        }

    }    

}
