// FRAMEWORK
import { Component, OnInit, OnDestroy, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

// COMPONENTS
import { RecaptchaComponent } from 'ng-recaptcha';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { AuthService } from '../../auth/authService';
import { ResetPasswordService } from './reset-password.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { Config } from '../../config/config';

// TYPES
import { ResetPasswordRequest, ResetPasswordValidationResult } from './reset-password.types';
import { PasswordRulesFromOptions } from '../../shared/password-rules/password-rules-types';


@Component({
    selector: 'reset-password',
    templateUrl: './reset-password.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./reset-password.less']
})

export class ResetPasswordComponent implements OnInit, OnDestroy {
    @ViewChild('captcha', { static: false }) public captcha: RecaptchaComponent;
    public request: ResetPasswordRequest = {} as ResetPasswordRequest;
    public user: ResetPasswordValidationResult;
    public loading: boolean;
    public showRecaptcha: boolean = false;
    public passwordRulesOptions: any = PasswordRulesFromOptions;
    public isPasswordValid: boolean;

    private serviceSubscriber: Subscription;

    constructor(private authService: AuthService,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private snackBar: SnackBarService,
                private service: ResetPasswordService,
                private userOptionsService: UserOptionsService,
                private unsubscribe: UnsubscribeService,
                private configService: Config) {

                    this.userOptionsService.setCulture('en-GB');
                }

    // =========================
    // LIFECYCLE
    // =========================

    public ngOnInit(): void {
        this.activatedRoute
            .params
            .subscribe((params) => {
                this.request.code = params['key'];
                this.validateKey(this.request.code);
            });
        }


    // Clean changePassSubscribe to avoid memory leak
    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }

    // =========================
    // EVENT HANDLERS
    // =========================

    public onPasswordValidation(isValid: boolean): void {
        this.isPasswordValid = isValid;
    }

    public goToLoginPage() {
        this.router.navigate(['login'], { skipLocationChange: !this.configService.get('router')});
    }

    // =========================
    // REQUESTS
    // =========================

    public validateKey(key: string) {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.service
            .validateKey(key)
            .subscribe((result: ResetPasswordValidationResult) => {
                this.loading = false;
                if (result) {
                    this.user = result;
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    this.router.navigateByUrl('/', { skipLocationChange: !this.configService.get('router') });
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
                this.router.navigateByUrl('/', { skipLocationChange: !this.configService.get('router') });
            });
    }

    public onCaptchaResolved(captchaResponse: string) {
        this.request.recaptchaReactive = captchaResponse;
    }

    public onResetPassword({ value, valid }: { value: ResetPasswordRequest, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        const resetPass = () => {
            this.unsubscribe.removeSubscription(this.serviceSubscriber);
            this.serviceSubscriber = this.service
                .reset(this.request)
                .subscribe((result: number) => {
                    this.loading = false;
                    if (result === 200) {
                        this.goToLoginPage();
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                        this.service.logFailedPasswordReset({ passwordRecoveryCode: this.request.code }).subscribe();
                        if (this.captcha) {
                            this.captcha.reset();
                        }
                    }
                },
                (err) => {
                    this.loading = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    this.service.logFailedPasswordReset({ passwordRecoveryCode: this.request.code }).subscribe();
                    if (this.captcha) {
                        this.captcha.reset();
                    }
                });
        };

        this.service
            .shouldCaptchaReset({ passwordRecoveryCode: this.request.code })
            .subscribe((res: any) => {
                if (res.status !== 200) {
                    return;
                }

                if (!this.showRecaptcha && res.body) {
                    this.showRecaptcha = true;
                    this.loading = false;
                    return;
                }

                if (this.request.recaptchaReactive) {
                    this.authService
                        .performCaptchaCheck({ captchaResponse: this.request.recaptchaReactive })
                        .subscribe(
                            (respond: any) => {
                                if (!respond) {
                                    resetPass();
                                } else {
                                    this.loading = false;
                                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                                }
                            },
                            (err) => {
                                this.loading = false;
                                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                            }
                        );
                } else {
                    resetPass();
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });

    }
}
