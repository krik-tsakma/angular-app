import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResetPasswordComponent } from './reset-password.component';

const ResetPasswordRoutes: Routes = [
    {
        path: ':key',
        component: ResetPasswordComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ResetPasswordRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})

export class ResetPasswordRoutingModule { }
