export interface ResetPasswordRequest {   
    code: string; 
    newPassword: string;
    confirmNewPassword: string;
    recaptchaReactive?: string;
}

export interface ResetPasswordValidationResult {
    username: string;
    fullname: string;
}

export interface ShouldCaptchaResetRequest {
    passwordRecoveryCode: string;
}

