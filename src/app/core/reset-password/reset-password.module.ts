// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { ResetPasswordRoutingModule } from './reset-password-routing.module';

// COMPONENTS
import { ResetPasswordComponent } from './reset-password.component';

// SERVICES
import { ResetPasswordService } from './reset-password.service';

@NgModule({
    imports: [
        SharedModule,
        ResetPasswordRoutingModule
    ],
    declarations: [
        ResetPasswordComponent,
    ],
    providers: [
        ResetPasswordService
    ]
})

export class ResetPasswordModule {

}
