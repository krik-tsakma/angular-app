// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import { ResetPasswordRequest, ResetPasswordValidationResult, ShouldCaptchaResetRequest } from './reset-password.types';

@Injectable()
export class ResetPasswordService {
    private baseURL: string;
    constructor(
        // use auth to resolve the s{id}-apps.sycada.com in prd
        private authHttp: AuthHttp,
        private http: HttpClient,
        private config: Config) { 
            this.baseURL = config.get('apiUrl') + '/api/Account';
        }



    public reset(req: ResetPasswordRequest): Observable<number>  {
        return this.authHttp
            .put(this.baseURL + '/ResetPassword', req, { observe: 'response' })
            .pipe(
                map((res) => res.status)
            );
    }



    public validateKey(key: string): Observable<ResetPasswordValidationResult> {

        const params = new HttpParams({
            fromObject: {
                key
            }
        });

        return this.authHttp
            .get(this.baseURL + '/ResetPasswordValidateKey/', { params });
    }



    public shouldCaptchaReset(check: ShouldCaptchaResetRequest) {
        return this.http.post(this.baseURL + '/ShouldCaptchaReset', check, { observe: 'response' });            
    }



    public logFailedPasswordReset(log: ShouldCaptchaResetRequest) {
        return this.http.post(this.baseURL + '/LogFailedPasswordReset', log, { observe: 'response' })
        .pipe(
            map((res) => {
                // this is means empty Ok response from the server
                if (res.status === 200 && !res.body) {
                    return null;
                } else  {
                    return res.body;
                }
            })
        );  
    }
}
