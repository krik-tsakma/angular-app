export interface DriverResult {
    id: number;
    fullname: string;
    email: string;
    codeValidationError?: DriverCodeConfirmationErrors;
}

export class UserAccountCreationRequest {
    public code: string;
    public email: string;
    public username: string;
    public password: string;
    public confirmPassword: string;
    public doNotUseEmailAsUserName: boolean;

    constructor(code: string, email: string) {
        this.code = code;
        this.email = email;
        this.username = this.password = this.confirmPassword = null;
        this.doNotUseEmailAsUserName = false;
    }
}

export enum DriverCodeConfirmationErrors {
    Found = 0,
    NoHumanResourceForThatCode = 1,
    FoundButAccountCreationNotAllowed = 2,
    FoundButAccountAlreadyCreated = 3, 
    BadlyFormattedCode = 4,
    PrefixDoesNotCorrespondToACompanyAccount = 5
}


export enum DriverCreationErrors {
    AccountCreated = 0,
    NoSuchHRExists = 1,
    AccountAlreadyExists = 2,
    FirstLastNameCombinationExists = 3,
    UsernameAlreadyExists = 4,
    PasswordsDoNotMatch = 5,
    EMailAddressInvalid = 6,
    UserNameInvalid = 7,
    EMailAddressAlreadyExists = 8,
    PasswordInvalid = 9,
    UnknownError = 20,
}
