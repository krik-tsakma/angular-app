﻿// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import { 
    DriverResult, 
    UserAccountCreationRequest, 
    DriverCreationErrors
} from './create-account.types';

@Injectable()
export class CreateAccountService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) { 
            this.baseURL = this.config.get('apiUrl') + '/api/Account';
        }

    public getByCode(code: string): Observable<DriverResult> {
        return this.authHttp
            .get(this.baseURL + '/' + code);
    }
    
    public create(account: UserAccountCreationRequest): Observable<DriverCreationErrors>  {
        return this.authHttp
            .post(this.baseURL, account, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else {
                        return res.body as DriverCreationErrors;
                    }
                })
            );
    }
    
}
