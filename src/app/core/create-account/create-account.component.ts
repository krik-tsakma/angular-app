// FRAMEWORK
import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// SERVICES
import { CreateAccountService } from './create-account.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';

// PIPES
import { FormValidationMessagePipe } from '../../shared/pipes/form-validation-message.pipe';
import { TranslatePipe } from '@ngx-translate/core';
import { StringFormatPipe } from '../../shared/pipes/stringFormat.pipe';

// TYPES
import { 
    DriverResult, DriverCreationErrors,
    DriverCodeConfirmationErrors, UserAccountCreationRequest 
} from './create-account.types';
import { PasswordRulesFromOptions } from '../../shared/password-rules/password-rules-types';

@Component({
    selector: 'create-account',
    templateUrl: 'create-account.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['create-account.less'],
    providers: [CreateAccountService, FormValidationMessagePipe, StringFormatPipe, TranslatePipe]
})

export class CreateAccountDialogComponent implements OnDestroy {
    public driver: DriverResult;
    public codeEntered: string;
    public account: UserAccountCreationRequest;
    public loading: boolean;
    public showUsernameField: boolean = false;
    public passwordRulesOptions: any = PasswordRulesFromOptions;
    public isPasswordValid: boolean;

    private serviceSubscriber: any;

    constructor(
        private dialogRef: MatDialogRef<CreateAccountDialogComponent>,
        private service: CreateAccountService,
        private snackBar: SnackBarService,
        private translator: CustomTranslateService,
        private stringFormatPipe: StringFormatPipe,
        private unsubscribe: UnsubscribeService) {
            // foo
        }


    // Clean forgetSubscribe to avoid memory leak
    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }

    public closeDialog() {
        this.dialogRef.close();
    }

    // =========================
    // EVENT HANDLERS
    // =========================

    public onPasswordValidation(isValid: boolean): void {
        this.isPasswordValid = isValid;
    }

    
    // =========================
    // CODE CONFIRMATION
    // =========================

    public onConfirmCode({ value, valid }: { value: string, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }

        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.service
            .getByCode(this.codeEntered)
            .subscribe((result: DriverResult) => {
                this.loading = false;
                if (result.codeValidationError === null) {
                    this.driver = result;
                    this.account = new UserAccountCreationRequest(this.codeEntered, result.email);
                } else {
                    const message = this.getCodeValidationResult(result.codeValidationError);
                    this.snackBar.open(message, 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }

    public onAccountCreation({ value, valid }: { value: UserAccountCreationRequest, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }

        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.service
            .create(this.account)
            .subscribe((errorCode: DriverCreationErrors) => {
                this.loading = false;
                if (errorCode === null) {
                    this.snackBar.open('create_account.success', 'form.actions.ok');
                    this.closeDialog();
                } else {
                    const message = this.getDriverCreationResult(errorCode);
                    this.snackBar.open(message, 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }

   
    // =========================
    // RESULTS
    // =========================

    private getCodeValidationResult(code: DriverCodeConfirmationErrors): string {
        let message = 'form.errors.unknown';
        switch (code) {
            case DriverCodeConfirmationErrors.FoundButAccountAlreadyCreated:
                message = 'create_account.code.errors.already_used';
                break;
            case DriverCodeConfirmationErrors.NoHumanResourceForThatCode:
                message = 'create_account.code.errors.no_driver';
                break;
            case DriverCodeConfirmationErrors.BadlyFormattedCode:
                message = 'create_account.code.errors.invalid_format';
                break;
            case DriverCodeConfirmationErrors.PrefixDoesNotCorrespondToACompanyAccount:
                message = 'create_account.code.errors.invalid_prefix';
                break;
            case DriverCodeConfirmationErrors.FoundButAccountCreationNotAllowed:
                message = 'create_account.code.errors.not_allowed';
                break;
        }

        return message;
    }

    private getDriverCreationResult(code: DriverCreationErrors): string {
        let message = 'form.errors.unknown';
        switch (code) {
            case DriverCreationErrors.NoSuchHRExists:
                message = 'form.errors.not_found';
                break;
            case DriverCreationErrors.AccountAlreadyExists:
                message = 'create_account.errors.already_exist';
                break;
            case DriverCreationErrors.FirstLastNameCombinationExists:
                message = 'create_account.code.errors.fullname_exists';
                break;
            case DriverCreationErrors.EMailAddressAlreadyExists:
                message = 'create_account.errors.email_exists';
                break;
            case DriverCreationErrors.UsernameAlreadyExists:
                message = 'create_account.errors.username_exists';
                break;
            case DriverCreationErrors.EMailAddressInvalid:
                message = 'form.errors.email_invalid';
                break;
            case DriverCreationErrors.UserNameInvalid:
                const required = this.translator.instant('form.errors.required');
                const username = this.translator.instant('create_account.username');
                message = this.stringFormatPipe.transform(required, [username]);
                break;
            case DriverCreationErrors.PasswordsDoNotMatch:
                message = 'create_account.confirmation_mismatch';
                break;
            case DriverCreationErrors.UnknownError:
            default:
                message = 'form.errors.unknown';
                break;
        }

        return message;
    }
}
