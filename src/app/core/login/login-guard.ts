// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

// RXJS
import { catchError } from 'rxjs/operators';

// SERVICES
import { AuthService } from '../../auth/authService';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';

// TYPES
import { TakeOverRequest } from './login-types';
import { StoreItems } from '../../shared/store-manager/store-items';
import { OAuthResult, AuthResponseCodeOptions } from '../../auth/authResponse';
import { Config } from '../../config/config';
import { TakeOverResult, AuthCode } from '../../driver-take-over/driver-take-over-types';

@Injectable()
export class LoginGuard implements CanActivate {
    private takeOverURL: string = this.configService.get('apiUrl') + '/api/DefaultAdminTakeOver/';
    private takeOverRequestParams: TakeOverRequest;

    constructor(private authService: AuthService,
                private storeManagerService: StoreManagerService,
                private router: Router,
                private http: HttpClient,
                private configService: Config) {
            // foo
        }

    public canActivate(route: ActivatedRouteSnapshot, innerState: RouterStateSnapshot) {
        // IF THIS IS A TAKE OVER REQUEST
        const queryParams = route.queryParams as TakeOverRequest;

        if (queryParams.auid && queryParams.touid) {
            this.takeOverRequestParams = queryParams;
            // if we are already logged in, loggout first
            if (this.authService.isLoggedIn()) {
                this.authService.logOut(true).then(() => {
                    this.takeOverAccount(queryParams);
                });
            } else {
                this.takeOverAccount(queryParams);
            }
            return false;
        } else {
            // NORMAL LOGIN REQUEST
            return true;
        }
    }

    private takeOverAccount(queryParams: TakeOverRequest): void {
        this.http
            .post(this.takeOverURL, queryParams)
            .pipe(
                catchError((err) => {
                    this.router.navigate(['/access-denied'], { skipLocationChange: !this.configService.get('router') });
                    return err;
                })
            )
            .subscribe((takeOver: TakeOverResult) => {

                if (takeOver.code === AuthCode.error) {
                    this.router.navigate(['/access-denied'], { skipLocationChange: !this.configService.get('router') });
                }

                const takeOverAccount: OAuthResult = JSON.parse(takeOver.response);

                this.authService.afterAuthentication(takeOverAccount).subscribe((code: AuthResponseCodeOptions) => {
                    if (code === AuthResponseCodeOptions.authenticated) {
                        this.storeManagerService.saveOption(StoreItems.superAdminGUID, this.takeOverRequestParams);
                        const defaultRedirectURL = this.authService.defaultHomePage();
                        this.router.navigate([defaultRedirectURL], { skipLocationChange: !this.configService.get('router') });
                    } else {
                        this.router.navigate(['/access-denied'], { skipLocationChange: !this.configService.get('router') });
                    }
                });
            }, (err) => {
                this.router.navigate(['/access-denied'], { skipLocationChange: !this.configService.get('router') });
            });
    }

}
