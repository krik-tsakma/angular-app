// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { CountrySelectorModule } from '../../common/country-selector/country-selector.module';
import { LoginRoutingModule } from './login-routing.module';

// COMPONENTS
import { LoginComponent, BrowsersVersionDialogComponent } from './login.component';


@NgModule({
    imports: [
        SharedModule,
        LoginRoutingModule,
        CountrySelectorModule
    ],
    declarations: [
        LoginComponent,
        BrowsersVersionDialogComponent
    ],
    exports: [
        LoginComponent,
        BrowsersVersionDialogComponent
    ],
    entryComponents: [
        BrowsersVersionDialogComponent
    ]
})
export class LoginModule {}
