﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login.component';
import { LoginGuard } from './login-guard';

const LoginRoutes: Routes = [
    {
        path: '',
        component: LoginComponent,
        canActivate: [LoginGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(LoginRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        LoginGuard
    ]
})

export class LoginRoutingModule { }
