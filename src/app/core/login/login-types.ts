﻿export class UserLogin {
    public username: string;
    public password: string;
    public recaptchaReactive?: string;
}

export interface TakeOverRequest {
    touid: number;
    auid: number;
}
