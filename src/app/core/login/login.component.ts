// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { timer, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

// COMPONENT
import { ForgotPasswordDialogComponent } from '../forgot-password/forgot-password.component';
import { CreateAccountDialogComponent } from '../create-account/create-account.component';

// SERVICES
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../auth/authService';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { AnnouncementsBottomSheetService } from '../../shared/announcements-bottom-sheet/announcements-bottom-sheet.service';
import { AnnouncementsService } from '../../announcements/announcements.service';
import { Config } from '../../config/config';

// TYPES
import { AuthResponseCodeOptions } from '../../auth/authResponse';
import { Announcement } from '../../shared/announcements-bottom-sheet/announcements-bottom-sheet-types';
import { UserLanguageOptions } from '../../auth/user';
import { CountryOption } from '../../common/country-selector/country-selector-types';
import { StoreItems } from '../../shared/store-manager/store-items';
import { UserLogin } from './login-types';

// LODASH-ES
import groupBy from 'lodash-es/groupBy';

@Component({
    selector: 'login',
    templateUrl: './login.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./login.less'],
})
export class LoginComponent implements OnInit, OnDestroy {
    public loginRedirect: boolean = false;
    public message: string;
    public showRecaptcha: boolean = false;
    public loading: boolean = false;
    public langOptions: CountryOption[] = UserLanguageOptions;
    public selectedLanguage: CountryOption;
    public loginForm: FormGroup;
    private formSubscriber: Subscription;
    private announcementsSubscriber: Subscription;
    private user: UserLogin = new UserLogin();
    private forgetPassDialogRef: MatDialogRef<ForgotPasswordDialogComponent>;
    private createAccountDialogRef: MatDialogRef<CreateAccountDialogComponent>;
    private browsersVersionDialogRef: MatDialogRef<BrowsersVersionDialogComponent>;

    constructor(
        private translate: TranslateService,
        private authService: AuthService,
        private router: Router,
        private fb: FormBuilder,
        private dialog: MatDialog,
        private storeManager: StoreManagerService,
        private userOptionsService: UserOptionsService,
        private unsubscribe: UnsubscribeService,
        private announBottomSheet: AnnouncementsBottomSheetService,
        private announcementsService: AnnouncementsService,
        private configService: Config) {
        // Check if user is logged in, and navigate to dashboard
        if (this.authService.isLoggedIn()) {
            this.loginRedirect = true;
            this.redirectToURL();
        } else {
            // came here for the first time, get culture settings
            const langSelectionSaved = storeManager.getOption(
                StoreItems.culture
            ) as string;
            const langSelectionByUser = storeManager.getOption(
                StoreItems.cultureSetByUser
            ) as boolean;

            if (langSelectionSaved) {
                if (langSelectionByUser) {
                    this.selectedLanguage = UserLanguageOptions.find(
                        (x) => x.code === langSelectionSaved
                    );
                }
            }


            if (this.configService.get('announcementEnabled')) {
                this.announcementsSubscriber = timer(0, 60000)
                    .pipe(
                        switchMap((_) => {
                            return this.announcementsService.get(storeManager.getOption(StoreItems.culture));
                        })
                    )
                    .subscribe((result: Announcement[]) => {
                        if (result && result.length > 0) {
                            const announcements = this.announBottomSheet.storageChecker(result);

                            const filteredAnnouns: Announcement[] = [];

                            // group all announcements by same id.
                            const grouped = groupBy(announcements, (announ: Announcement) => {
                                return announ.id;
                            });

                            /*
                            * if there are annoucements with same id but with different cultures, then
                            * a) check if one of these annoucements has same culture with user's culture
                            * and filter it. Else
                            * b) filter only the announcement with culture === 'default'
                            */
                            for (const key in grouped) {
                                if (grouped.hasOwnProperty(key)) {
                                    if (langSelectionSaved &&
                                        grouped[key].find((announ) => announ.culture === langSelectionSaved) !== undefined) {
                                        filteredAnnouns.push(grouped[key].find((announ) => announ.culture === langSelectionSaved));
                                    } else {
                                        filteredAnnouns.push(grouped[key].find((announ) => announ.culture === 'default'));
                                    }
                                }
                            }


                            if (filteredAnnouns && filteredAnnouns.length > 0) {
                                this.announBottomSheet.announs(filteredAnnouns);
                            }
                        }
                    });
            }
        }
    }

    // On Init call buildForm method
    public ngOnInit(): void {
        this.buildForm();
    }

    public ngOnDestroy(): void {
        this.unsubscribe.removeSubscription([
            this.formSubscriber,
            this.announcementsSubscriber
        ]);
    }

    // Show forgot password dialog
    public forgotPassword() {
        this.forgetPassDialogRef = this.dialog.open(
            ForgotPasswordDialogComponent
        );
    }

    // Show create account dialog
    public createAccount() {
        this.createAccountDialogRef = this.dialog.open(
            CreateAccountDialogComponent
        );
    }

    public selectDisplayLanguage(lang: CountryOption) {
        // if user selected a lang, store it and change display settings
        const currentLang = this.translate.currentLang;
        if (currentLang !== this.translate.getDefaultLang() && (lang && lang.code !== currentLang)) {
            this.translate.resetLang(currentLang);
            this.translate.setDefaultLang('en-GB');
        }

        if (lang) {
            this.selectedLanguage = lang;
            this.storeManager.saveOption(StoreItems.culture, lang.code);
            this.storeManager.saveOption(StoreItems.cultureSetByUser, true);
            this.userOptionsService.setCulture(lang.code);
        } else {
            // the user selected the default option
            // remove stored values and set default display settings
            this.selectedLanguage = null;

            this.storeManager.deleteOption(StoreItems.culture);
            this.storeManager.deleteOption(StoreItems.cultureSetByUser);

            const defaultLang = UserLanguageOptions.find(
                (x) => x.default === true
            ).code;
            this.userOptionsService.setCulture(defaultLang);
        }
    }

    // after clicking login button, login method is called, which calls the authService service
    // and its login promise method
    public login() {
        this.message = '';
        this.user = this.loginForm.value;
        this.loading = true;

        const authLoggin = () => {
            this.authService
                .logIn(this.user.username, this.user.password)
                .subscribe(
                    (code: AuthResponseCodeOptions) => {
                        this.loading = false;
                        if (code === AuthResponseCodeOptions.authenticated) {
                            this.redirectToURL();
                        } else {
                            if (this.loginForm.get('recaptchaReactive')) {
                                this.loginForm.get('recaptchaReactive').setValue(null);
                            }

                            if (code === AuthResponseCodeOptions.invalid_request) {
                                this.message = 'auth_response.wrong_credentials';
                                this.authService.logFailedLogin({ username: this.user.username })
                                    .subscribe();
                            } else if (code === AuthResponseCodeOptions.access_denied) {
                                this.message = 'auth_response.locked_out';
                                this.authService.logFailedLogin({ username: this.user.username })
                                    .subscribe();
                            } else {
                                this.message = 'data.unknown-error';
                            }
                        }
                    },
                    (err) => {
                        this.loading = false;
                        this.message = 'data.unknown-error';
                    }
                );
        };

        this.authService
            .shouldCaptcha({username: this.user.username})
            .subscribe((res: any) => {
                if (res.status !== 200) {
                    return;
                }

                if (!this.showRecaptcha && res.body) {
                    this.showRecaptcha = true;
                    this.loginForm.addControl('recaptchaReactive', this.fb.control([null, Validators.required]));
                    this.loginForm.controls['recaptchaReactive'].reset();
                    this.loading = false;
                    return;
                }

                if (this.user.recaptchaReactive) {
                    this.authService
                        .performCaptchaCheck({ captchaResponse: this.user.recaptchaReactive })
                        .subscribe((respond: any) => {
                            if (!respond) {
                                authLoggin();
                            } else {
                                this.message = 'data.unknown-error';
                            }
                        },
                        (err) => {
                            this.message = 'data.unknown-error';
                        });
                } else {
                    authLoggin();
                }

            },
            (err) => {
                this.loading = false;
                this.message = 'data.unknown-error';
            });
    }

    public resolved(captchaResponse: string) {
        this.user.recaptchaReactive = captchaResponse;
    }

    public browserVersion() {
        this.browsersVersionDialogRef = this.dialog.open(
            BrowsersVersionDialogComponent
        );
    }


    // Initialize loginForm and subscribe in valueChange stream
    // which calls onValueChanged method
    private buildForm(): void {
        this.loginForm = this.fb.group({
            username: [this.user.username, Validators.required],
            password: [this.user.password, Validators.required],
        });

        this.formSubscriber = this.loginForm.valueChanges.subscribe((data) => {
            this.message = '';
        });
    }

    private redirectToURL() {
        const defaultRedirectURL = this.authService.defaultHomePage();
        this.router.navigate([defaultRedirectURL], {
            skipLocationChange: !this.configService.get('router')
        });
    }
}

// 'Browsers-Version' dialog
@Component({
    selector: 'browsers-version',
    templateUrl: './browsers-version.html',
})
export class BrowsersVersionDialogComponent {
    constructor(
        public dialogRef: MatDialogRef<BrowsersVersionDialogComponent>
    ) {}
}
