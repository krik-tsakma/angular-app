import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Router, NavigationEnd } from '@angular/router';

// --- use a declare to allow the compiler find the gtag function
declare let gtag: Function;

// notes you may need to do this as well: npm install --save-dev @types/google.analytics
@Injectable({
    providedIn: 'root'
})

export class GoogleAnalyticsService {
  private subscription: Subscription;

  constructor(private router: Router) {  }


  public subscribe() {
    const hostName = location.hostname === 'gfm3.sycada.com' ? '1' : '2';

    if (!this.subscription) {
      // subscribe to any router navigation and once it ends, write out the google script notices
      this.subscription = this.router.events.subscribe( e => {
        if (e instanceof NavigationEnd) {
          // this will find & use the ga function pulled via the google scripts
          try {
            gtag('config', `UA-137497398-${hostName}`, { page_path: e.urlAfterRedirects });
          } catch {
             console.log('tracking not found - make sure you installed the scripts');
          }
        }
      });
    }
  }

  public unsubscribe() {
    if (this.subscription) {
      // --- clear our observable subscription
      this.subscription.unsubscribe();
    }
  }

}
