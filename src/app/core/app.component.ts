// FRAMEWORK
import { Component, OnInit, ViewChild, ViewEncapsulation, HostListener, OnDestroy } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Router, ActivatedRoute, RouterEvent, NavigationStart, NavigationEnd } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { TranslateService } from '@ngx-translate/core';
import { UserOptionsService } from '../shared/user-options/user-options.service';
import { UnsubscribeService } from '../shared/unsubscribe.service';
import { StoreManagerService } from '../shared/store-manager/store-manager.service';
import { PreviousRouteService } from './previous-route/previous-route.service';
import { GoogleAnalyticsService } from './google-analytics.service';
import { Config } from '../config/config';

// TYPES
import { AuthService } from '../auth/authService';
import { AuthGuard } from '../auth/authGuard';
import { SideBar, SidebarOptions } from './sidebar.types';
import { StoreItems } from '../shared/store-manager/store-items';

// DEFAULT LOCALE
import defaultLanguage from '../../assets/i18n/en-GB.json';

// MOMENT
import 'moment/locale/en-gb';
import 'moment/locale/nl';
import 'moment/locale/de';


@Component({
    selector: 'my-app',
    templateUrl: './app.html',
    providers: [AuthGuard],
    encapsulation: ViewEncapsulation.None,
    styleUrls: [
        './material-app-theme.scss',
        './core.less',
        './sidebar.less',
        './toolbar.less'
    ],
    animations: [
        trigger(
            'openClose',
            [
                state('collapsed', style({ display: 'none', opacity: '0', height: '0px' })),
                state('expanded', style({ display: 'block', opacity: '1', height: '*' })),
                transition('collapsed <=> expanded', animate('500ms ease-out'))
            ]
        )
    ]
})

export class AppComponent implements OnInit, OnDestroy {
    @ViewChild('start', { static: false }) public sidenav: MatSidenav;
    public skipLocChange = !this.configService.get('router');
    public active: boolean;
    public menu: boolean = false;
    public opening: boolean;
    public mode: string;
    public sidebarOptions: SideBar[];
    public newVersion: MatSnackBarRef<{}>;

    public bgColor: boolean = false;
    public showLoggedIn: boolean = true;
    public checkIfLoggedIn: boolean;

    private routerSubscription: Subscription;

    constructor(
        public router: Router,
        public authService: AuthService,
        public dialog: MatDialog,
        public translate: TranslateService,
        public activatedRoute: ActivatedRoute,
        public userOptions: UserOptionsService,
        public snackBar: MatSnackBar,
        public previousRoute: PreviousRouteService,
        private googleAnalyticsService: GoogleAnalyticsService,
        private storeManager: StoreManagerService,
        private unsubscribe: UnsubscribeService,
        private configService: Config) {

            this.authService.logInSuccessfull.subscribe((success: boolean) => {
                this.routerSubscription = this.router.events.subscribe((event: RouterEvent) => {
                    if (event instanceof NavigationStart) {
                        this.applyAppLoggedInSettings();
                    }
                    if (event instanceof NavigationEnd) {
                        this.enableLoggedInSettings();
                    }
                });
            });

            // Global Promise for app's new version.
            if (window['isUpdateAvailable']) {
                window['isUpdateAvailable']
                    .then((isAvailable: boolean) => {
                        if (isAvailable) {
                            setTimeout(() => {
                                if (!this.userOptions || !this.userOptions.getUser()) {
                                    location.reload();
                                    return;
                                }

                                location.reload();

                            }, 3500);
                        }
                    });
            }
    }

    // checks if starting window width has changed and if it's
    // less than 720px it hides sidebar (usually this is functional in
    // large mobiles and tablets while rotating the screen.
    @HostListener('window:resize') public onResize() {
        if (window.innerWidth > 880) {
            this.opening = true;
            this.mode = 'side';
            this.bgColor = true;
        } else {
            this.opening = false;
            this.mode = 'over';
            this.bgColor = false;
        }
    }

    // On init check window size for sidebar
    public ngOnInit() {
        this.setUpLangOptions();
        this.googleAnalyticsService.subscribe();

        // on page refresh theme class is lost, so reset it here
        if (this.userOptions && this.userOptions.getUser()) {
            this.applyAppLoggedInSettings();
            this.enableLoggedInSettings();
        }
        this.setOpening(window.innerWidth);
    }


    // On Destroy unsubscribe from analytics tracking
    public ngOnDestroy() {
        this.googleAnalyticsService.unsubscribe();
    }


    // Check if window size is > than 880 and keep sidebar open.
    // If it is less-equal then sidebar must be closed
    public setOpening(windowWith: number) {
        if (windowWith > 880) {
            this.opening = true;
            this.mode = 'side';
            return;
        }
        this.opening = false;
        this.mode = 'over';
    }

    // Option for larger menu with more information
    public largerMenu() {
        this.menu = !this.menu;
    }

    // If window size is less than 880px, then the sidebar is hidden. By pushing
    // hamburger button, we use this method for hidding-showing the sidebar
    public openClose() {
        if (this.mode === 'over') {
            setTimeout(() => {
                this.sidenav.toggle();
            }, this.sidenav.opened ? 600 : 0);
        }
    }

    public showChildMenu(menuId) {
        this.sidebarOptions.forEach((menu) => {
            if (menu.routeParam === menuId) {
                menu.opened = !menu.opened;
            }
        });
    }

    // ======================
    //  PRIVATE
    // ======================

    private setUpLangOptions() {
        const langAvailable = ['en-GB', 'nl-NL', 'de-DE'];

        // Initiallize all available translations
        this.translate.addLangs(langAvailable);

        // this language will be used as a fallback when a translation isn't found in the current language
        this.translate.setDefaultLang('en-GB');
        this.translate.setTranslation('en-GB', defaultLanguage);
        // get the default languange
        const browserLang = langAvailable.find((lang) => {
            return lang.includes(this.translate.getBrowserCultureLang());
        });

        // Set Browser lang as Default lang (if exists), otherwise use english
        const userLang = browserLang ? browserLang : 'en-GB';

        if (!this.userOptions.getUser()) {
            const storedCulture = this.storeManager.getOption(StoreItems.culture);
            this.userOptions.setCulture(storedCulture
                ? storedCulture
                : userLang
            );
        }

    }

    private applyAppLoggedInSettings() {
        this.sidebarOptions = new SidebarOptions(this.authService).Get();
        this.userOptions.setTheme();
        this.userOptions.setCulture(null);
        this.userOptions.setMeasurementSystem(null);
    }

    private enableLoggedInSettings() {
        this.checkIfLoggedIn = true;
        this.unsubscribe.removeSubscription(this.routerSubscription);
    }
}
