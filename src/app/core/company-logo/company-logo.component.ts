// FRAMEWORK
import { Component, ViewEncapsulation, ViewChild } from '@angular/core';

// SERVICES
import { AuthService } from '../../auth/authService';
import { UserOptionsService } from '../../shared/user-options/user-options.service';

// TYPES
import { Config } from '../../config/config';

@Component({
    selector: 'company-logo',
    templateUrl: 'company-logo.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['company-logo.less']
})

export class CompanyLogoComponent {
    @ViewChild('logoImage', { static: false }) public logoImage;
    public loading: boolean;
    private baseLogoUrl: string;
    private imageDefault: string = this.config.get('companyLogoUrl') + '/SycadaLogo.png';

    constructor(
        private config: Config,
        private auth: AuthService,
        private userOptions: UserOptionsService
    ) {
        this.baseLogoUrl = config.get('companyLogoUrl');
    }
    
    /**
     * show the default sycada image if none was found in the server
     */
    public imgErrorHandler(event) {
        event.target.src = this.imageDefault;
    }
   
    /**
     * show the company's logo or the default if one is not defined
     */
    public get(): string {
        if (this.auth.isLoggedIn()) {
            return this.userOptions.getUser().appearance.logoURL;
        } 
        return this.imageDefault;
   }
}
