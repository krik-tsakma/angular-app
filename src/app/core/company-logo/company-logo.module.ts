// FRAMEWORK
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// COMPONENTS
import { CompanyLogoComponent } from './company-logo.component';

@NgModule({
    imports: [
        CommonModule                
    ],
    declarations: [
        CompanyLogoComponent
    ],
    exports: [
        CompanyLogoComponent
    ]
})
export class CompanyLogoModule {}
