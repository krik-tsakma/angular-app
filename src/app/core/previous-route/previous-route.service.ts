// FRAMEWORK
import { Injectable } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';

// SERVICES
import { Config } from '../../config/config';

interface RoutePath {
  depth: number;
  url: string;
  root: string;
}

@Injectable({providedIn: 'root'})
export class PreviousRouteService {
    public routePath: RoutePath[] = [{
        depth: 1,
        url: '/',
        root: null
    }];
    private previousUrl: RoutePath;
    private currentUrl: RoutePath;

    constructor(
        private router: Router,
        private location: Location,
        private configService: Config) {
            // initialize current url before subscribe in router's events
            this.currentUrl = {
            depth: this.router.url.split('').filter((word) => word === '/').length,
            url: this.router.url,
            root: null
            };


            this.router.events.subscribe((event) => {
                /*
                *
                * Router's mechanism that clears URIs from Browser's address bar
                *
                */
                if (!this.configService.get('router')) {
                    if (event instanceof NavigationStart) {
                        if (event.navigationTrigger !== 'imperative') {
                            this.router.navigateByUrl('/', { skipLocationChange: !this.configService.get('router') });
                        }

                        if (!this.location.isCurrentPathEqualTo('/')) {
                            this.location.replaceState('/');
                        }
                    }
                }




                /*
                *
                * 'backTo' functionallity
                *
                */
                if (event instanceof NavigationEnd) {
                    this.previousUrl = Object.assign({}, this.currentUrl);

                    if (event.url !== '/') {
                    /* Find current url depth by how many '/' are in the url string.
                    * ex:
                    * '/dsds' -> depth 1.
                    * '/dsds/dsds' -> depth 2.
                    * '/wwew/dssds/dsdsd' -> depth 3.
                    *
                    */
                    const eventUrlDepth = event.url.split('').filter((word) => word === '/').length;

                    /*
                    * Find the root of the url
                    * ex:
                    * '/admin' -> root 'admin'
                    * '/admin/drivers/edit/32343' -> root 'admin'
                    */

                    const root = event.url.split('/'); // Ex: /admin/drivers -> root = ['','admin', 'driver'];
                    root.shift(); // Ex: root = ['admin', 'driver'];

                    this.currentUrl = {
                        depth: eventUrlDepth, // ex: 2
                        url: event.url, // ex: '/admin/driver'
                        root: root[0] // ex: 'admin'
                    };


                    // Check if current url is one step forward in depth
                    const finded = this.routePath.find((path) => path.depth === this.currentUrl.depth);

                    // Same level entry finded in route path array
                    if (finded !== undefined) {

                        if (this.previousUrl.depth === this.currentUrl.depth) {
                        if (this.previousUrl.root !== this.currentUrl.root) {
                            this.routePath.pop();
                            this.routePath.push(this.previousUrl);
                        }
                        } else {
                        if (this.previousUrl.root !== this.currentUrl.root && this.currentUrl.depth > this.previousUrl.depth) {
                            this.routePath.pop();
                        }
                        }
                    } else {
                        // Current is step forward from the previous
                        if (this.currentUrl.depth > this.previousUrl.depth) {
                        this.routePath.push(this.previousUrl);
                        }
                    }

                    // If currentUrl is in first route depth, keep only routes with depth === 1 in routesPath array
                    if (this.currentUrl.depth === 1) {
                        this.routePath = this.routePath.filter((path) => path.depth === 1 && path.url !== this.currentUrl.url);
                    } else {
                        // else keep paths in routePath array that their depth is less that currentUrl's depth
                        this.routePath = this.routePath.filter((path) => path.depth < this.currentUrl.depth);
                    }


                    // if for any reason the routePath is empty, add the default 'dashboard' route.
                    if (this.routePath.length === 0) {
                        this.routePath.push({
                        depth: 1,
                        url: '/dashboard',
                        root: 'dashboard'
                        });
                    }

                    }
                }
            });
        }



    public getPreviousUrl() {
        if (this.routePath.length > 0) {
        const obj = this.routePath.pop();
        return obj.url;
        }
        return this.previousUrl.url;
    }
}
