export enum ForgotPasswordCodeOptions {
    GeneratedAndSent = 0,
    EMailAddressNotFound = 1,
    EMailAddressInvalid = 2
}
