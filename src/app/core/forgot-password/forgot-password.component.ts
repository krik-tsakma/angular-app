// FRAMEWORK
import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { ForgotPasswordService } from './forgot-password.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';

// PIPES
import { FormValidationMessagePipe } from '../../shared/pipes/form-validation-message.pipe';
import { TranslatePipe } from '@ngx-translate/core';
import { StringFormatPipe } from '../../shared/pipes/stringFormat.pipe';

// TYPES
import { ForgotPasswordCodeOptions } from './forgot-password.types';

@Component({
    selector: 'forgot-password',
    templateUrl: 'forgot-password.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['forgot-password.less'],
    providers: [ForgotPasswordService, FormValidationMessagePipe, StringFormatPipe, TranslatePipe]
})

export class ForgotPasswordDialogComponent implements OnDestroy {
    public forgotPasswordEmail: string;
    public loading: boolean;
    public emailSent: boolean;
    private serviceSubscriber: Subscription;

    constructor(private dialogRef: MatDialogRef<ForgotPasswordDialogComponent>,
                private service: ForgotPasswordService,
                private snackBar: SnackBarService,
                private unsubscribe: UnsubscribeService) {
        // foo
    }

    // Clean forgetSubscribe to avoid memory leak
    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }

    public closeDialog() {
        this.dialogRef.close();
    }

    public onForgotPassword({ value, valid }: { value: string, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }

        this.emailSent = false;
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.service
            .sendEmail(this.forgotPasswordEmail)
            .subscribe((res: ForgotPasswordCodeOptions) => {
                this.loading = false;
                // this is means empty Ok response from the server
                // we ignore email not found to avoid security implications (prevent leaking account existance)
                if (res === null || res === ForgotPasswordCodeOptions.EMailAddressNotFound) {
                    this.emailSent = true;
                } else {
                    const message = this.getCodeValidationResult(res);
                    this.snackBar.open(message, 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }

      // =========================
    // RESULTS
    // =========================

    private getCodeValidationResult(code: ForgotPasswordCodeOptions): string {
        let message = 'form.errors.unknown';
        switch (code) {
            case ForgotPasswordCodeOptions.EMailAddressInvalid:
                message = 'form.errors.email_invalid';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
