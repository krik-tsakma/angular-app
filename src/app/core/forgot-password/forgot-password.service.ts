// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import { ForgotPasswordCodeOptions } from './forgot-password.types';

@Injectable()
export class ForgotPasswordService {
    private baseURL: string;
    constructor(
        // must use AuthHttp to resolve properly the s{id}-apps.sycada.com in prd
        private authHttp: AuthHttp,
        private config: Config) { 
            this.baseURL = config.get('apiUrl') + '/api/Account';
        }
   
        public sendEmail(email: string): Observable<ForgotPasswordCodeOptions> {
            const req = {
                Email : email
            };

            return this.authHttp
                .post(this.baseURL + '/ForgotPassword/', req, { observe: 'response' })
                .pipe(
                    map((res) => {
                        if (res.status === 200 && !res.body) {
                            return null;
                        } else {
                            return res.body as ForgotPasswordCodeOptions;
                        }
                    })
                );
        }
}
