// FRAMEWORK
import { Component, Input, HostListener, OnInit } from '@angular/core';

// SERVICES
import { Config } from '../../config/config';
import { AuthService } from '../../auth/authService';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';
import { DriverTakeOverService } from '../../driver-take-over/driver-take-over.service';
import { SnackBarService } from '../../shared/snackbar.service';

// TYPES
import { StoreItems } from '../../shared/store-manager/store-items';
import { TakeOverRequest } from '../login/login-types';
import { AuthCode, TakeOverResult } from '../../driver-take-over/driver-take-over-types';
import { OAuthResult, AuthResponseCodeOptions } from '../../auth/authResponse';

@Component({
    selector: 'back-to-admin',
    templateUrl: 'back-to-admin.html',
    styleUrls: ['./back-to-admin.less']
})

export class BackToAdminComponent implements OnInit { 
    
    @Input() public guid: TakeOverRequest;
    public backToManager: string;
    public enabled: boolean = false;
    public desktopWidth: boolean;
    private baseSuperAdminURL: string;
    
    constructor(private config: Config,
                private snackBar: SnackBarService,
                private authService: AuthService,
                private storeManager: StoreManagerService,
                private driverTakeOverService: DriverTakeOverService ) {
        this.baseSuperAdminURL = this.config.get('superAdminUrl');        
        this.desktopWidth = window.innerWidth > 840;
    }
    

    @HostListener('window:resize', ['$event']) public onResize(event) {
        this.desktopWidth = window.innerWidth > 840;        
    }

    public ngOnInit() {
        // Super Admin 
        this.guid = this.storeManager.getOption('guid');
        // Company's manager
        this.backToManager = this.storeManager.getOption(StoreItems.BackToAdminButton); 
        // show button or not
        this.enabled = this.guid || this.backToManager ? true : false;  
    }

    /* Back to Super Admin.
     *
     * When button is pressed, we logout the super admin without a redirection to the homePage. 
     * If the Logout is succeed, then we redirect user to the superAdmin page. 
     */
    public redirectToAdmin() {
        const queryParams = `touid=${this.guid.auid}&auid=${this.guid.touid}`;
        this.authService.logOut(false, true)
            .then((res) => {
                if (res) {
                    window.location.href = this.baseSuperAdminURL + '?' + queryParams;
                }
            })
            .catch((err) => {
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }


    /* Back to company manager account from driver's profile.
     *
     * When button is pressed we request for company's manager auth result. 
     * If the request is succeed, then we logout the current driver without a redirection to the homePage. 
     * After that we pass authorized manager to afterAuthentication function to receive all the necessary
     * settings. If the result is authenticated, then we redirect(refresh) the new user(manager) to the homePage. 
     */
    public redirectToManager() {
        this.driverTakeOverService.backToManager()
            .subscribe((backToManagerRes: TakeOverResult) => {

                if (backToManagerRes.code === AuthCode.error) {
                    const errorRes = JSON.parse(backToManagerRes.response).error;
                    const errorCode: AuthResponseCodeOptions = AuthResponseCodeOptions[`${errorRes}`];
                    switch (errorCode) {
                        case AuthResponseCodeOptions.access_denied: {
                            this.snackBar.open('auth_response.locked_out', 'form.actions.ok');
                            break;
                        } 
                        default: {
                            this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                            break;
                        }
                    }
                    return;
                }

                const backToManager: OAuthResult = JSON.parse(backToManagerRes.response); 
                this.authService.logOut(false, true)
                        .then((res) => {
                            if (res) { 
                                this.authService.afterAuthentication(backToManager)
                                    .subscribe((result: AuthResponseCodeOptions) => {
                                        if (result === AuthResponseCodeOptions.authenticated) {
                                            window.location.href = '/';
                                        }
                                    },
                                    (err) => {
                                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                                        window.setTimeout(() => {
                                            window.location.href = '/';
                                        }, 2500);
                                    });
                            }
                        })
                        .catch((err) => {
                            this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                        });
            },
            (err) => {
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }

}
