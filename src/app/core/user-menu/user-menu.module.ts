// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from './../../shared/shared.module';

// COMPONENTS
import { UserMenuComponent } from './user-menu.component';
import { BackToAdminComponent } from './../back-to-admin/back-to-admin.component';
import { ExportFilesDialogComponent } from '../export-files-dialog/export-files-dialog.component';
import { ChangePasswordDialogComponent } from '../change-password/change-password.component';

// SERVICES
import { FileExportNotificationService } from './../../admin/import-export/file-export-notification.service';
import { ExportFilesDialogService } from './../export-files-dialog/export-files-dialog.service';
import { DriverTakeOverService } from '../../driver-take-over/driver-take-over.service';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        UserMenuComponent,
        ChangePasswordDialogComponent,
        ExportFilesDialogComponent,
        BackToAdminComponent,
    ],
    entryComponents: [
        ChangePasswordDialogComponent,
        ExportFilesDialogComponent
    ],
    exports: [UserMenuComponent],
    providers: [
        FileExportNotificationService,
        ExportFilesDialogService,
        DriverTakeOverService
    ]
})

export class UserMenuModule { }
