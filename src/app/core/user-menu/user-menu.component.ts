// FRAMEWORK
import { Component, OnDestroy, ViewEncapsulation, OnInit, Input, AfterViewInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { Router, RoutesRecognized } from '@angular/router';

// RxJS
import { timer, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';


// COMPONENTS
import { ChangePasswordDialogComponent } from './../change-password/change-password.component';

// SERVICES
import { AuthService } from './../../auth/authService';
import { FileExportNotificationService } from './../../admin/import-export/file-export-notification.service';
import { ExportFilesDialogService } from './../export-files-dialog/export-files-dialog.service';
import { StoreManagerService } from './../../shared/store-manager/store-manager.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { UserOptionsService } from './../../shared/user-options/user-options.service';
import { Config } from '../../config/config';
import { AnnouncementsBottomSheetService } from './../../shared/announcements-bottom-sheet/announcements-bottom-sheet.service';
import { AnnouncementsService } from '../../announcements/announcements.service';

// TYPES
import { ExportFileView, ImportExportTypes, ExportTypeOptions, CodeType, ExportFileNotification, ImportFileNotification } from './../../admin/import-export/import-export.types';
import { KeyName } from './../../common/key-name';
import { StoreItems } from './../../shared/store-manager/store-items';
import { ModuleOptions, TripReviewPropertyOptions } from './../../auth/acl.types';
import { Announcement } from './../../shared/announcements-bottom-sheet/announcements-bottom-sheet-types';

// LODASH-ES
import groupBy from 'lodash-es/groupBy';


@Component({
    selector: 'user-menu',
    templateUrl: 'user-menu.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['user-menu.less'],
})

export class UserMenuComponent implements OnInit, OnDestroy {
    @Input() public showFullname: boolean;
    public importExportTypes: KeyName[] = ImportExportTypes;
    public filesForDownload: ExportFileView[] = [];
    public changePasswordDialogRef: MatDialogRef<ChangePasswordDialogComponent>;

    public announcements: Announcement[] = [];

    private announcementsSubscriber: Subscription;
    private exportFilesDialogSubscriber: Subscription;
    private importNotificationSubscriber: Subscription;
    private exportNotificationSubscriber: Subscription;

    constructor(private userOptions: UserOptionsService,
                private announBottomSheet: AnnouncementsBottomSheetService,
                private announcementsService: AnnouncementsService,
                private unsubscribe: UnsubscribeService,
                private authService: AuthService,
                private router: Router,
                private exportFileNotificationService: FileExportNotificationService,
                private exportFilesDialogService: ExportFilesDialogService,
                private dialog: MatDialog,
                private storeManager: StoreManagerService,
                private configService: Config) {

        // IMPORT / EXPORT NOTIFICATIONS
        this.filesForDownload = this.storeManager.getOption(StoreItems.exportFiles)
            ? this.storeManager.getOption(StoreItems.exportFiles)
            : [];


        this.exportNotificationSubscriber = this.exportFileNotificationService
            .exportNotificationReceived
            .subscribe((res: ExportFileNotification) => {
                // console.log('export', res, this.userOptions.getUser().uid);
                if (res.uid !== this.userOptions.getUser().uid) {
                    return;
                }

                const exportView: ExportFileView = {
                    name: res.fileName,
                    typeName: this.importExportTypes.find((type) => type.id === res.type).name,
                    fileURL: res.fileURL,
                    timestamp: res.timestamp,
                    code: res.code, // TO DO : translate
                    codeType: CodeType.export,
                };

                if (res.exportType !== ExportTypeOptions.Errors) {
                    exportView.exportType = res.exportType === ExportTypeOptions.Export
                        ? 'export_files.export.options.company_data'
                        : 'export_files.export.options.template';
                }

                this.filesForDownload.push(exportView);
                this.filesForDownload = this.filesForDownload.filter(function(elem, index, self) {
                    return index === self.indexOf(elem);
                });
                this.storeManager.saveOption(StoreItems.exportFiles, this.filesForDownload);
                this.exportFilesDialogService.openDialog(this.filesForDownload);
            });

        this.importNotificationSubscriber = this.exportFileNotificationService
            .importNotificationReceived
            .subscribe((res: ImportFileNotification) => {
                // console.log('import', res, this.userOptions.getUser().uid);
                if (res.uid !== this.userOptions.getUser().uid) {
                    return;
                }

                const importView: ExportFileView = {
                    name: 'Import',
                    typeName: this.importExportTypes.find((type) => type.id === res.type).name,
                    fileURL: res.errorFileURL,
                    timestamp: res.timestamp,
                    code: res.code, // TO DO : translate,
                    codeType: CodeType.import
                };

                this.filesForDownload.push(importView);
                this.filesForDownload = this.filesForDownload.filter(function(elem, index, self) {
                    return index === self.indexOf(elem);
                });

                this.storeManager.saveOption(StoreItems.exportFiles, this.filesForDownload);
                this.exportFilesDialogService.openDialog(this.filesForDownload);
        });

        this.exportFilesDialogSubscriber = this.exportFilesDialogService
            .returnArray
            .subscribe((res: ExportFileView[]) => {
                this.filesForDownload = res;
                this.storeManager.saveOption(StoreItems.exportFiles, this.filesForDownload);
            });

        const culture = this.storeManager.getOption(StoreItems.culture);
        if (this.configService.get('announcementEnabled')) {
            this.announcementsSubscriber = timer(0, 60000)
                .pipe(
                    switchMap((_) => {
                        return this.announcementsService.get(culture);
                    })
                )
                .subscribe((result: Announcement[]) => {
                    if (result && result.length > 0) {
                        const filteredAnnouns: Announcement[] = [];

                        // group all announcements by same id.
                        const grouped = groupBy(result, (announ: Announcement) => {
                            return announ.id;
                        });

                        /*
                        * if there are annoucements with same id but with different cultures, then
                        * a) check if one of these annoucements has same culture with user's culture
                        * and filter it. Else
                        * b) filter only the announcement with culture === 'default'
                        */
                        for (const key in grouped) {
                            if (grouped.hasOwnProperty(key)) {
                                if (culture && grouped[key].find((announ) => announ.culture === culture) !== undefined) {
                                    filteredAnnouns.push(grouped[key].find((announ) => announ.culture === culture));
                                } else {
                                    filteredAnnouns.push(grouped[key].find((announ) => announ.culture === 'default'));
                                }
                            }
                        }

                        this.announcements = filteredAnnouns;
                    }
                    // if we had announcements and after a new check there are no annoucement,
                    // updatethe the array.
                    if (this.announcements.length > 0 && (result && result.length === 0)) {
                        this.announcements = [];
                    }
                });
        }
    }


    // ===================
    // LIFECYCLE
    // ===================

    public ngOnInit() {
        // this.router.events
        //     .pipe(
        //         filter((event) => event instanceof RoutesRecognized)
        //     )
        //     .subscribe((event: RoutesRecognized) => {
        //         if (this.announcementsArray) {
        //             this.announcements = this.announcementsArray.filter((res: Announcement) => {
        //                 if (this.userOptions.getUser() && this.userOptions.getUser().culture) {
        //                     return res.culture === this.userOptions.getUser().culture &&
        //                         (event.url.includes(res.showOnPage) || !res.showOnPage);
        //                 } else {
        //                     if (event.state.root.firstChild.params && event.state.root.firstChild.params['lang']) {
        //                         return res.culture === event.state.root.firstChild.params['lang'] &&
        //                             (event.url.includes(res.showOnPage) || !res.showOnPage);
        //                     } else {
        //                         return res.culture === 'en-GB' &&
        //                             (event.url.includes(res.showOnPage) || !res.showOnPage);
        //                     }
        //                 }
        //             });

        //         }
        //     });
    }

    // Clean forgetSubscribe to avoid memory leak
    public ngOnDestroy() {
        this.exportFileNotificationService.Stop();
        this.unsubscribe.removeSubscription([
            this.exportNotificationSubscriber,
            this.importNotificationSubscriber,
            this.exportFilesDialogSubscriber,
            this.announcementsSubscriber
        ]);
    }




    public getFullname(): string {
        if (this.userOptions.getUser()) {
            return this.userOptions.getUser().fullname;
        }
        return;
    }

    // ===================
    // ANNOUNCEMENTS
    // ===================

    public announcementsBottomSheet() {
        this.announBottomSheet
            .announs(this.announcements);
    }


    // ===================
    // EXPORT FILES
    // ===================

    public exportFiles() {
        this.exportFilesDialogService.openDialog(this.filesForDownload);
    }

    // ===================
    // CHANGE PASSWORD
    // ===================

    // Show change password dialog
    public changePassword() {
        this.changePasswordDialogRef = this.dialog.open(ChangePasswordDialogComponent);
    }


    // ===================
    // AUDIT LOG
    // ===================

    public auditLog() {
        this.router.navigate(['/audit-log'], { skipLocationChange: !this.configService.get('router') });
    }


    // ===================
    // LOG OUT
    // ===================

    // Loggout method tha uses the authService service and its logOut method
    public logout() {
        this.authService.logOut(false);
    }


    // ===================
    // COUNT SUM OF IMPORTS/EXPORTS & ANNOUNCEMENTS
    // ===================
    get counterNumber(): number {
        return [
            this.announcements.length,
            this.filesForDownload.length
        ].reduce((accumulator, currentValue) =>  accumulator + currentValue);
    }
}
