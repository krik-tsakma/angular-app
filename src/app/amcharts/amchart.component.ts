// FRAMEWORK
import { Component, Input, ElementRef, AfterViewInit, OnDestroy, ViewEncapsulation } from '@angular/core';

// SERVICES
import { ScriptLoaderService } from '../common/script-loader.service';

// TYPES
import { AmChartTypes } from './amchart-types';

declare const AmCharts;

@Component({
    selector: 'amchart',
    templateUrl: 'amchart.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['amchart.less'],
    providers: [ScriptLoaderService]
})

export class AmChartComponent implements AfterViewInit, OnDestroy {
    @Input() public htmlID: string;
    @Input() public options: any;
    @Input() public type: AmChartTypes;
    @Input() public customLegend: boolean;

    private chart: any;
    private filesToLoad: string[] = [];
    private loadCompleteInterval: any;

    constructor(private scriptLoader: ScriptLoaderService) {
        // common for all charts
       this.filesToLoad.push('/assets/js/charts/amcharts.js');
       this.filesToLoad.push('/assets/js/charts/plugins/export/export.js');
       this.filesToLoad.push('/assets/js/charts/plugins/responsive/responsive.js');
    }

    public ngAfterViewInit() {
        let typeMainFunc;
        switch (this.type) {
            case AmChartTypes.Serial:
                typeMainFunc = 'AmSerialChart';
                this.filesToLoad.push('/assets/js/charts/serial.js');
                break;
            case AmChartTypes.Pie:
                typeMainFunc =  'AmPieChart';
                this.filesToLoad.push('/assets/js/charts/pie.js');
                break;
            default:
                break;
        }

        // load chart libraries.
        // After libraries finished loading create the chart
        const that = this;
        this.scriptLoader.load(this.filesToLoad, () => {
                                that.createChart(typeMainFunc);
                            });
    }

    public ngOnDestroy() {
        if (this.chart) {
            this.chart.removeLegend();
            this.chart.clear();
            this.chart = null;
        }
    }

    private createChart(typeMainFunc) {
        // On first load there is a possibility that the script hasn't fully loaded, so refresh until it does before we show anything
        if (window['AmCharts'] === undefined || window['AmCharts'][typeMainFunc] === undefined) {
            this.loadCompleteInterval = setInterval(() => {
                clearInterval(this.loadCompleteInterval);
                this.createChart(typeMainFunc);
            }, 500);
        } else {
            this.chart = AmCharts.makeChart(this.htmlID, this.options);
            setTimeout(() => {
                if (!this.chart) {
                    return;
                }
                this.chart.invalidateSize();
            }, 1000);
        }
    }
}
