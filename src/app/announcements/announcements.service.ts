﻿// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../config/config';
import { AuthHttp } from '../auth/authHttp';

// TYPES
import { Announcement } from '../shared/announcements-bottom-sheet/announcements-bottom-sheet-types';
import { 
    AnnouncementResult, AnnouncementViewItem, AnnouncementCreateRequest, 
    AnnouncementUpdateRequest, AnnouncementCreateCode, AnnouncementUpdateCode,
    Term, TermRequest, TermCreateCode, TermUpdateCode
} from './announcements-types';

@Injectable()
export class AnnouncementsService {
    private baseApiURL: string;
    private baseApiAdminURL: string;

    constructor(private _authHttp: AuthHttp,
                private configService: Config) {
        this.baseApiURL = this.configService.get('apiUrl') + '/api/Announcements';
        this.baseApiAdminURL = this.configService.get('apiUrl') + '/api/AdminAnnouncements';
    }

    // Get all the announcements for bottom-sheet shared module
    public get(locale: string): Observable<Announcement[]> {
        return this._authHttp
            .get(this.baseApiURL + '/' + locale);
    }



    // Get all the announcements for admin page
    public getAdmin(): Observable<AnnouncementViewItem[]> {
        return this._authHttp
            .get(this.baseApiAdminURL);
    }



    // Get announcement by id for admin page
    public getByID(id: number): Observable<AnnouncementResult> {
        return this._authHttp
            .get(this.baseApiAdminURL + '/' + id);
    }



    // Create announcement (admin page)
    public create(newItem: AnnouncementCreateRequest): Observable<AnnouncementCreateCode> {
        return this._authHttp
            .post(this.baseApiAdminURL, newItem, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as AnnouncementCreateCode;
                    }
                })
            );
    }



    // Update announcement (admin page)
    public update(updateItem: AnnouncementUpdateRequest): Observable<AnnouncementUpdateCode> {
        return this._authHttp
            .put(this.baseApiAdminURL, updateItem, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 200) {
                        return null;
                    } else  {
                        return res.body as AnnouncementUpdateCode;
                    }
                })
            );
            
    }



    // Delete announcement (admin page)
    public delete(id: number): Observable<number> {
        return this._authHttp
            .delete(this.baseApiAdminURL + '/' + id, { observe: 'response' })
            .pipe(
                map((res) => res.status)
            ); 
    }



    // Get term(culture) for announcement (admin page)
    public getTermByID(id: number): Observable<Term> {
        return this._authHttp
            .get(this.baseApiAdminURL + '/GetTerm/' + id);
    }



    // Create term(culture) for announcement (admin page)
    public createTerm(newTerm: TermRequest): Observable<TermCreateCode> {
        return this._authHttp
            .post(this.baseApiAdminURL  + '/CreateTerm', newTerm, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as TermCreateCode;
                    }
                })
            );
    }



    // Update term(culture) for announcement (admin page)
    public updateTerm(updateTerm: TermRequest): Observable<TermUpdateCode> {
        return this._authHttp
            .put(this.baseApiAdminURL + '/UpdateTerm', updateTerm, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 200) {
                        return null;
                    } else  {
                        return res.body as TermUpdateCode;
                    }
                })
            );         
    }



    // Delete term(culture) for announcement (admin page)
    public deleteTerm(id: number): Observable<number> {
        return this._authHttp
            .delete(this.baseApiAdminURL + '/DeleteTerm/' + id, { observe: 'response' })
            .pipe(
                map((res) => res.status)
            ); 
    }
}
