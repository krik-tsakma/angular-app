// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../shared/shared.module';
import { AnnouncementsRoutingModule } from './announcements-routing.module';
import { PeriodSelectorModule } from '../common/period-selector/period-selector.module';
import { DatepickerModule } from '../common/datepicker/datepicker.module';
import { TimePickerModule } from '../common/timepicker/timepicker.module';

// COMPONENTS
import { ViewAnnouncementsComponent } from './view/view-announcements.component';
import { EditAnnouncementsComponent } from './edit/edit-announcements.component';
import { EditCultureComponent } from './edit-culture/edit-culture.component';

// SERVICES
import { AnnouncementsService } from './announcements.service';
import { AdminLabelService } from './../admin/admin-label.service';
import { TranslateStateService } from '../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService ) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/announcements/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        AnnouncementsRoutingModule,
        PeriodSelectorModule,
        DatepickerModule,
        TimePickerModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        }),
    ],
    declarations: [
        ViewAnnouncementsComponent,
        EditAnnouncementsComponent,
        EditCultureComponent
    ],
    providers: [
        AnnouncementsService,
        AdminLabelService
    ],
    entryComponents: [
        EditCultureComponent
    ]
})
export class AnnouncementsModule { }
