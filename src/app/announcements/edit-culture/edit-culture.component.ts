// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../admin/admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { AdminLabelService } from '../../admin/admin-label.service';
import { AnnouncementsService } from '../announcements.service';
import { Config } from '../../config/config';

// TYPES
import { TermRequest, TermCreateCode, TermUpdateCode, CultureOptions } from '../announcements-types';
import { KeyName } from '../../common/key-name';

// DIALOG
@Component({
    selector: 'edit-culture',
    templateUrl: 'edit-culture.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-culture.less']
})

export class EditCultureComponent extends AdminBaseComponent implements OnInit, OnDestroy {
    public editCultureForm: FormGroup;
    public loading: boolean;
    public action: string;
    public usedLocales: string[];
    public record: TermRequest = {} as TermRequest;
    public cultureOptions: KeyName[];
    private formSubscriber: any; 

    constructor(        
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected router: Router,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected configService: Config,        
        private service: AnnouncementsService,
        public dialogRef: MatDialogRef<EditCultureComponent>,
        public fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        this.record = Object.assign({}, data.term); // immutable term
        this.usedLocales = [...data.locales]; // immutable locales
        this.cultureOptions = [...CultureOptions]; // immutable CultureOption
        
        if (this.usedLocales.length > 0) {
            if (!this.record.id) {
                this.cultureOptions.forEach((culture) => {
                    culture.enabled = this.usedLocales.find((usedLocale) => usedLocale === culture.id ) !== undefined
                        ? false
                        : true;                    
                });

            } else {
                this.cultureOptions.forEach((culture) => {
                    culture.enabled = 
                        this.usedLocales.find((usedLocale) => usedLocale === culture.id ) !== undefined && 
                        this.record.culture !== culture.id
                            ? false
                            : true;              
                });  
            }   
                                   
        }

        this.cultureOptions.unshift({
            id: null,
            name: this.translator.instant('form.actions.select') + '...',
            enabled: false,
        });     
        
    }



    // Super init and initialize form
    public ngOnInit(): void {
        super.ngOnInit();
        this.buildForm();

    }



    // Unsubscribe all subscriptions
    public ngOnDestroy(): void {
        this.unsubscriber.removeSubscription([
            this.formSubscriber
        ]);
    }



    // Grab and change the culture change from drop down
    public changeCulture(event) {
        this.editCultureForm.controls['culture'].setValue(event);
    }



    // close dialog when cancel or X is pressed
    public closeDialog(opt: boolean) {
        this.dialogRef.close(opt);
    }



    // Submit form
    public onSubmit() {
        
        this.loading = true;
        this.unsubscribeService();
        this.record = {
            ...this.record,
            ...this.editCultureForm.value
        };          

        if (this.record.id) {
            // Update term
            this.service
                .updateTerm(this.record)
                .subscribe((res: TermUpdateCode) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.updated_success', 'form.actions.ok');
                        this.closeDialog(true);
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
            
        } else {
            // Create term
            this.service
                .createTerm(this.record)
                .subscribe((res: TermCreateCode) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.ok');
                        this.closeDialog(true);
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        }
    }
    


    // =========================
    // PRIVATE
    // =========================

    // form building function
    private buildForm(): void {

        this.editCultureForm = this.fb.group({
            title: [this.record.title, Validators.required],
            description: [this.record.description, Validators.required], 
            culture: [this.record.culture, Validators.required],              
        });

        if (!this.record.id && !this.record.culture) {
            this.editCultureForm.controls['culture'].reset(); 
        }
        this.formSubscriber = this.editCultureForm.valueChanges
            .subscribe((data: any) => {                
                this.message = '';
            });
    }


}
