import { KeyName } from '../common/key-name';

export interface AnnouncementsResult {            
    items: AnnouncementViewItem[];
}



export enum AnnouncementTypes {
    Information = 1,
    Warning = 2
}



export enum AnnouncementCreateCode {
    Created = 0,
    TitleRequired = 1,
    DescriptionRequired = 2,
}



export enum TermCreateCode {
    Created = 0,
    TitleRequired = 1,
    DescriptionRequired = 2,
    CultureRequired = 3,
    AnnouncementNotFound = 4,
    CultureExists = 5
}



export enum TermUpdateCode {
    Updated = 0,
    NotFound = 1,
    CultureExists = 2
}



export enum AnnouncementUpdateCode {
    Updated = 0,
    NotFound = 1,
    KeyCulturePairExists = 2
}



export interface AnnouncementItem {
    id: number;
    type: AnnouncementTypes;
    startDate?: string;
    endDate?: string;    
    showOnPage: string;
    defaultTerm: Term;   
    title?: string;
    description?: string;
}



export interface AnnouncementViewItem extends AnnouncementItem  {    
    terms: Term[];
    defaultTermId: number;
}



export interface Term {
    id: number;
    culture: string;
    title: string;
    description: string;
}



export interface TermRequest {
    id?: number;
    announcementID?: number;
    culture: string;
    title: string;
    description: string;
}



export interface AnnouncementRequest {
    type: AnnouncementTypes;
    showOnPage: string;
    startDate?: string;
    endDate?: string; 
}



export interface AnnouncementCreateRequest extends AnnouncementRequest  {   
    title: string;
    description: string;    
}



export interface AnnouncementUpdateRequest extends AnnouncementRequest  {
    id: number;    
}



export interface AnnouncementResult {
    code: number;
    item: AnnouncementItem;
}



export const CultureOptions: KeyName[] = [
    {
        id: 'en-GB',
        term: 'culture.options.english',
        name: 'en-GB',
        enabled: true
    },
    {
        id: 'nl-NL',
        term: 'culture.options.dutch',
        name: 'nl-NL',
        enabled: true
    },
    {
        id: 'de-DE',
        term: 'culture.options.german',
        name: 'de-DE',
        enabled: true
    }
];



export const AnnouncementOptions: KeyName[] = [
    {
        id: AnnouncementTypes.Information,
        term: 'announcements_admin.edit.information',
        name: 'Information',
        enabled: true
    },
    {
        id: AnnouncementTypes.Warning,
        term: 'announcements_admin.edit.warning',
        name: 'Warning',
        enabled: true
    },
];
