// FRAMEWORK
import { 
    Component, OnInit, OnDestroy, AfterViewInit, 
    ViewEncapsulation, ViewChildren, QueryList
} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material';

// RXJS
import { Subscription } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../admin/admin-base.component';
import { EditCultureComponent } from '../edit-culture/edit-culture.component';
import { TableLayoutComponent } from '../../shared/data-table/data-table.component';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { AdminLabelService } from '../../admin/admin-label.service';
import { ConfirmDialogService } from '../../shared/confirm-dialog/confirm-dialog.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { Config } from '../../config/config';
import { AnnouncementsService } from '../announcements.service';

// TYPES
import { AnnouncementViewItem, AnnouncementTypes, Term, TermRequest, CultureOptions } from '../announcements-types';
import { TableColumnSetting, TableActionOptions, TableCellFormatOptions, TableActionItem } from '../../shared/data-table/data-table.types';
 
@Component({
    selector: 'view-announcements',
    templateUrl: './view-announcements.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['view-announcements.less']
})

export class ViewAnnouncementsComponent extends AdminBaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChildren(TableLayoutComponent) public tableLayouts: QueryList<TableLayoutComponent>;
    public loading: boolean;
    public announcements: AnnouncementViewItem[];
    public serviceSubscriber: Subscription;
    public announcementTypes = AnnouncementTypes;
    public tableSettings: TableColumnSetting[];
    public editCultureDialogRef: MatDialogRef<EditCultureComponent>;  
    public skipLocChange = !this.configService.get('router');

    constructor(
        protected translator: CustomTranslateService,
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        protected snackBar: SnackBarService,                
        protected confirmDialogService: ConfirmDialogService,   
        private service: AnnouncementsService,
        private dialog: MatDialog
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar, null, confirmDialogService);
        this.tableSettings = this.getTableSettings();       
    }   



    // On init get all announcements
    public ngOnInit(): void {       
       this.get();
    }



    // Unsubscribe all subscriptions
    public ngOnDestroy(): void {
        this.unsubscriber.removeSubscription(this.serviceSubscriber);
    }


    // After view init, fix table header width of Terms(cultures) table for every announcement
    public ngAfterViewInit() {       
        this.tableLayouts.changes.subscribe(() => {
            this.tableLayouts.toArray().forEach((table) => {
                table.resizeTableHeader();                
            });
        });
       
    }    



    // ----------------
    // ANNOUNCEMENT
    // ----------------

    // Add new announcement
    public add(): void {
        this.router.navigate([this.router.url , 'add'], { skipLocationChange: this.skipLocChange });  
    }



    // Edit announcement
    public editAnnouncement(id: number, defaultTermId: number): void {
        this.router.navigate([this.router.url , 'edit', `${id}_${defaultTermId}`], { skipLocationChange: this.skipLocChange });           
    }


    // press Delete announcement button
    public deleteAnnouncement(id: number) {
        this.openDialog()
            .subscribe((response) => {
                if (response) {
                    this.delete(id);                                
                }
            });
    }



    // ----------------
    // TERM
    // ----------------

    // Return's term with default culture
    public getDefault(ann: AnnouncementViewItem, value: string ) {
        return ann.terms.find((term) => term.culture === 'default')[value];
    }



    // Add new Term(locale)
    public newTerm(id: number) {
        this.openEditDialog({} as Term, id); 
    }



    // Manage iterm (edit or delete)
    public onManage(item: TableActionItem, id: number) {
        if (!item) {
            return;
        }

        const record = item.record as Term;
        switch (item.action) {
            case TableActionOptions.edit: 
                this.openEditDialog(record, id); 
                break;
            case TableActionOptions.delete: 
                this.openDialog()
                    .subscribe((response) => {
                        if (response) {
                            this.deleteTerm(record.id);                                
                        }
                    });
                break;
            default: 
                break;

        }
    }

    

    // Disable add button if announcement already has terms in all
    // available cultures.  
    public disableAddCulture(terms: Term[]): boolean {
        const reducer = (acc, cur) => {
            if (CultureOptions.find((opt) => opt.id === cur.culture)) {
                return acc + 1;
            }
        };
        return terms.reduce(reducer, 0) === 3 ? true : false;
    }
    

    
    // ----------------
    // PRIVATE
    // ----------------

    // Table settings for each announcment term's table
    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                primaryKey: 'culture',
                header: 'announcements_admin.culture',
                format: TableCellFormatOptions.string
            },
            {
                primaryKey: 'title',
                header: 'announcements_admin.title',
                format: TableCellFormatOptions.string
            },
            {
                primaryKey: 'description',
                header: 'announcements_admin.description',
                format: TableCellFormatOptions.string
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete]
            }
        ];
    }



    // Edit a term of an announcement
    private openEditDialog(item: Term, announId: number) {
        const announcement = this.announcements.find((announ) => announ.id === announId);

        if (announcement === undefined) {
            this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            return;
        }

        const itemRequest: TermRequest = {
            announcementID: announId,
            ...item
        };
        
        this.editCultureDialogRef = this.dialog.open(EditCultureComponent, {
            data: {
                term: itemRequest,
                locales: announcement.terms.map((term) => {
                    if (term.culture !== item.culture) {
                        return term.culture;
                    }                    
                })                    
            }
        });
    
        this.editCultureDialogRef.afterClosed().subscribe((res) => {
            this.editCultureDialogRef = null;     
            if (res) {
                this.get();
            }       
        });
    }
    


    // Fetch announcements
    private get() {                
        this.loading = true;
        // first stop currently executing requests
        this.unsubscriber.removeSubscription(this.serviceSubscriber);

        // then start the new request
        this.serviceSubscriber = this.service
            .getAdmin()
            .subscribe(
                (res) => {
                    this.loading = false;
                    this.announcements = res;
                    this.announcements.forEach((announ) => {
                        const foundedAnnouncement = announ.terms.find((term) => term.culture === 'default');
                        if (foundedAnnouncement) {
                            announ.title = foundedAnnouncement.title;
                            announ.description = foundedAnnouncement.description;
                            announ.defaultTermId = foundedAnnouncement.id;
                        }
                        announ.terms = announ.terms.filter((term) => {
                            return term.culture !== 'default';
                        });     


                    });
                },
                (err) => {
                    this.loading = false;                    
                }
            );
    }



    // Delete announcement
    private delete(id: number) {
        this.loading = true;
        this.unsubscriber.removeSubscription(this.serviceSubscriber);
        
        this.serviceSubscriber = this.service
            .delete(id)
            .subscribe((res) => {
                if (res !== 200) {                                        
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                    return;      
                }
                this.snackBar.open('form.delete_success', 'form.actions.ok');
                this.get();
               
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }



    // Delete term
    private deleteTerm(id: number) {
        this.loading = true;
        this.unsubscriber.removeSubscription(this.serviceSubscriber);
        
        this.serviceSubscriber = this.service
            .deleteTerm(id)
            .subscribe((res) => {
                if (res !== 200) {                                        
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                    return;      
                }
                this.snackBar.open('form.delete_success', 'form.actions.ok');
                this.get();
               
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }
}
