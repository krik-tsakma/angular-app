// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// RXJS
import { Subscription, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AnnouncementsService } from '../announcements.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { Config } from '../../config/config';

// TYPES
import { KeyName } from '../../common/key-name';
import {
    AnnouncementItem, AnnouncementResult, AnnouncementCreateRequest,
    AnnouncementCreateCode, AnnouncementUpdateCode, AnnouncementOptions,
    AnnouncementTypes, TermUpdateCode
 } from '../announcements-types';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'edit-announcements',
    templateUrl: 'edit-announcements.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-announcements.less']
})

export class EditAnnouncementsComponent implements OnInit, OnDestroy {
    public skipLocChange = !this.configService.get('router');
    public loading: boolean;
    public checked: boolean;
    public action: string;
    public announcementOptions: KeyName[] = [ ...AnnouncementOptions ];
    public defaultTermId: number;
    public sinceDate: moment.Moment = moment();
    public sinceDateHours: number  = 0;
    public sinceDateMinutes: number  = 0;
    public tillDate: moment.Moment = moment();
    public tillDateHours: number = 1;
    public tillDateMinutes: number  = 0;

    public announcement: AnnouncementItem = {} as AnnouncementItem;
    private serviceSubscriber: Subscription;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private configService: Config,
        private previousRouteService: PreviousRouteService,
        private service: AnnouncementsService,
        private snackBar: SnackBarService,
        private unsubscriber: UnsubscribeService,
    ) {
        this.announcement.type = AnnouncementTypes.Information;
    }

    public ngOnInit(): void {
        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: string;
            }) => {
                this.action = params.editMode === 'edit'
                            ? 'form.actions.edit'
                            : 'form.actions.create';

                if (params.id) {
                    const ids = params.id.split('_');
                    this.defaultTermId = Number(ids[1]);
                    this.get(Number(ids[0]));
                } else {
                    this.dateTimeToggle({ checked: false });
                }
            });
    }


    public ngOnDestroy(): void {
        this.unsubscribe();
    }



    // Enable/disable date-time pickers for announcement
    public dateTimeToggle(event) {
        if (!event.checked) {
            this.announcement.startDate = null;
            this.announcement.endDate = null;
            this.checked = event.checked;
            return;
        }

        if (this.announcement.startDate && this.announcement.endDate) {
            this.updateFormElements(false);
        } else {
            this.announcement.startDate = moment().toISOString();
            this.announcement.endDate = moment().toISOString();
            this.updateFormElements(true);
        }
        this.checked = event.checked;
    }



    // check and update 'sinceDate' depends on 'tillDate'
    public updateSinceDate() {
        if (this.tillDate < this.sinceDate) {
            this.tillDate = moment(this.sinceDate);
        }
    }



    // Get announcement
    public get(id: number) {
        this.loading = true;
        this.unsubscribe();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((res: AnnouncementResult) => {
                this.loading = false;
                if (res.code === 0) {
                    this.announcement = res.item;
                    this.dateTimeToggle({
                        checked: !this.announcement.startDate && !this.announcement.endDate
                            ? false
                            : true
                    });
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            });
    }



    // Submit announcement (edit or add)
    public onSubmit({ value, valid }: { value: AnnouncementItem, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        if (this.sinceDate && this.tillDate && this.announcement.startDate && this.announcement.endDate) {
            this.announcement.startDate = this.sinceDate.startOf('day').add(this.sinceDateHours, 'hours').add(this.sinceDateMinutes, 'minutes').format('YYYY-MM-DD HH:mm');
            this.announcement.endDate = this.tillDate.startOf('day').add(this.tillDateHours, 'hours').add(this.tillDateMinutes, 'minutes').format('YYYY-MM-DD HH:mm');
        }
        this.loading = true;
        this.unsubscribe();

        // add new one
        if (typeof(this.announcement.id) === 'undefined' || this.announcement.id === 0) {

            this.serviceSubscriber = this.service
                .create({
                    title: this.announcement.title,
                    description: this.announcement.description,
                    type: this.announcement.type,
                    showOnPage: this.announcement.showOnPage,
                    startDate: this.announcement.startDate,
                    endDate: this.announcement.endDate,
                })
                .subscribe((res: AnnouncementCreateCode) => {
                    if (!res) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                        return;
                    }
                    switch (res) {
                        case AnnouncementCreateCode.TitleRequired:
                            this.snackBar.open('', 'form.actions.close');
                            break;
                        case AnnouncementCreateCode.DescriptionRequired:
                            this.snackBar.open('', 'form.actions.close');
                            break;
                        default:
                            this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                            break;
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else {
            // This is an update
            this.serviceSubscriber = forkJoin(
                this.service
                    .update({
                        id: this.announcement.id,
                        type: this.announcement.type,
                        showOnPage: this.announcement.showOnPage,
                        startDate: this.announcement.startDate,
                        endDate: this.announcement.endDate,
                    })
                    .pipe(
                        map((val: AnnouncementUpdateCode) => ({ type: 'announ', value: val }))
                    ),
                this.service
                    .updateTerm({
                        id: this.defaultTermId,
                        culture: 'default',
                        title: this.announcement.title,
                        description: this.announcement.description
                    })
                    .pipe(
                        map((val: TermUpdateCode) => ({ type: 'term', value: val }))
                    )
                )
                .subscribe((results) => {
                    this.loading = false;
                    for (const res of results) {
                        if (res.type === 'announ') {
                            if (res.value) {
                                switch (res.value) {
                                    case AnnouncementUpdateCode.NotFound:
                                        this.snackBar.open('', 'form.actions.close');
                                        break;
                                    case AnnouncementUpdateCode.KeyCulturePairExists:
                                        this.snackBar.open('', 'form.actions.close');
                                        break;
                                    default:
                                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                                        break;
                                }
                                return;
                            }
                        } else {
                            if (res.value) {
                                switch (res.value) {
                                    case TermUpdateCode.NotFound:
                                        this.snackBar.open('', 'form.actions.close');
                                        break;
                                    case TermUpdateCode.CultureExists:
                                        this.snackBar.open('', 'form.actions.close');
                                        break;
                                    default:
                                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                                        break;
                                }
                                return;
                            }
                        }
                    }

                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                },
                (err) => {
                    this.loading = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                });
        }
    }



    // Back to previous page
    public back() {
        this.router
            .navigate(this.previousRouteService.getPreviousUrl().split('/'), { skipLocationChange: this.skipLocChange });
    }



    // =========================
    // PRIVATE
    // =========================

    private updateFormElements(empty: boolean) {
        this.sinceDate = moment(this.announcement.startDate);
        this.sinceDateHours = this.sinceDate.get('hour');
        this.sinceDateMinutes = this.sinceDate.get('minute');
        this.tillDate = moment(this.announcement.endDate);
        this.tillDateHours = empty
            ? this.tillDate.add(1, 'hour').get('hour')
            : this.tillDate.get('hour');
        this.tillDateMinutes = this.tillDate.get('minute');
    }

    private unsubscribe(): void {
        this.unsubscriber.removeSubscription(this.serviceSubscriber);
    }

}
