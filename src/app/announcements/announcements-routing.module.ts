// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../auth/authGuard';

// COMPONENTS
import { ViewAnnouncementsComponent } from './view/view-announcements.component';
import { EditAnnouncementsComponent } from './edit/edit-announcements.component';


const AnnouncementsRoutes: Routes = [
    {
        path: '',
        component: ViewAnnouncementsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: ':editMode/:id',
        component: EditAnnouncementsComponent,
        canActivate: [AuthGuard],
    },
    {
        path: ':editMode',
        component: EditAnnouncementsComponent,
        canActivate: [AuthGuard]
    }  
];

@NgModule({
    imports: [
        RouterModule.forChild(AnnouncementsRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AnnouncementsRoutingModule { }
