import { PeriodTypeOption, PeriodTypeValues } from '../../common/period-selector/period-selector-types';

// MOMENT
import moment from 'moment';

export enum BookingsPeriodTypes {
    Today = 0 ,
    Tomorrow = 1,
    ThisWeek = 2,
    NextWeek = 3,
    ThisMonth = 4,
    NextMonth = 5,
    Custom = 6,
}

export class BookingsPeriodTypesConverter {
    public convert(type: BookingsPeriodTypes): PeriodTypeValues {
        const dt = {
            datetimeSince: moment(),
            datetimeTill: moment()
        } as PeriodTypeValues;

        switch (type) {
            case BookingsPeriodTypes.Today:
                dt.datetimeSince = moment().startOf('day');
                dt.datetimeTill = moment().endOf('day');
                break;
            case BookingsPeriodTypes.Tomorrow:
                dt.datetimeSince = moment().add(1, 'day').startOf('day');
                dt.datetimeTill = moment().add(1, 'day').endOf('day');
                break;
            case BookingsPeriodTypes.ThisWeek:
                dt.datetimeSince = moment().startOf('isoWeek').subtract(1, 'day');
                dt.datetimeTill = moment().startOf('isoWeek').endOf('week').subtract(1, 'day');
                break;
            case BookingsPeriodTypes.NextWeek:
                dt.datetimeSince = moment().add(1, 'week').startOf('isoWeek').subtract(1, 'day');
                dt.datetimeTill = moment().add(1, 'week').startOf('isoWeek').endOf('week').subtract(1, 'day');
                break;
            case BookingsPeriodTypes.ThisMonth:
                dt.datetimeSince = moment().startOf('month');
                dt.datetimeTill = moment().endOf('month');
                break;
            case BookingsPeriodTypes.NextMonth:
                dt.datetimeSince = moment().add(1, 'month').startOf('month');
                dt.datetimeTill = moment().add(1, 'month').endOf('month');
                break;
            default:
                break;
        }

        return dt;
    }
}


export let BookingsPeriodTypeOptionsDefaults: PeriodTypeOption[] = [
    {
        term: 'period_selector.today',
        id: BookingsPeriodTypes.Today,
        enabled: true,
        name: 'Today',
    },
    {
        term: 'period_selector.tomorrow',
        id: BookingsPeriodTypes.Tomorrow,
        enabled: true,
        name: 'Tomorrow',
    },
    {
        term: 'period_selector.this_week',
        id: BookingsPeriodTypes.ThisWeek,
        enabled: true,
        name: 'This week',
    },
    {
        term: 'period_selector.next_week',
        id: BookingsPeriodTypes.NextWeek,
        enabled: true,
        name: 'Next week',
    },
    {
        term: 'period_selector.this_month',
        id: BookingsPeriodTypes.ThisMonth,
        enabled: true,
        name: 'This month',
    },
    {
        term: 'period_selector.next_month',
        id: BookingsPeriodTypes.NextMonth,
        enabled: true,
        name: 'Last month',
    },
    {
        term: 'period_selector.custom',
        id: BookingsPeriodTypes.Custom,
        enabled: true,
        name: 'Custom',
    }
];
