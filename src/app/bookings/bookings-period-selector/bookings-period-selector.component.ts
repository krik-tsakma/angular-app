
// FRAMEWORK
import {
    Component, Input, Output, OnChanges,
    SimpleChanges, EventEmitter, ViewEncapsulation
} from '@angular/core';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { SnackBarService } from '../../shared/snackbar.service';

// TYPES
import { PeriodTypeOption } from '../../common/period-selector/period-selector-types';
import { BookingsPeriodTypesConverter, BookingsPeriodTypeOptionsDefaults, BookingsPeriodTypes } from './bookings-period-selector-types';

// MOMENT
import moment from 'moment';


@Component({
    selector: 'bookings-period-selector',
    templateUrl: 'bookings-period-selector.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['bookings-period-selector.less']
})

export class BookingsPeriodSelectorComponent implements OnChanges {
    @Input() public htmlID: string;
    @Input() public modelData: any;
    @Output() public modelDataChange: EventEmitter<PeriodTypeOption> = new EventEmitter<PeriodTypeOption>();
    public matSinceDatePicker: any;
    public matTillDatePicker: any;
    public selectedOption: PeriodTypeOption;
    public showDatepicker: boolean = false;
    public periodOptions: PeriodTypeOption[] = [];
    public touchOption: boolean;
    private customOptionValue: number;
    private periodTypeConverter: BookingsPeriodTypesConverter;


    constructor(
        public userOptions: UserOptionsService,
        public snackBar: SnackBarService,
        private translate: CustomTranslateService) {
            this.periodTypeConverter = new BookingsPeriodTypesConverter();

            this.customOptionValue = BookingsPeriodTypeOptionsDefaults
                                    .find((x) => x.id === BookingsPeriodTypes.Custom).id;

            BookingsPeriodTypeOptionsDefaults.forEach((option) => {
                this.translate.get(option.term).subscribe((t) => {
                    option.name = t;
                });
            });
            this.periodOptions = BookingsPeriodTypeOptionsDefaults;
    }


    public ngOnChanges(changes: SimpleChanges) {
        // on every change of the model data
        const modelDataChange = changes['modelData'];
        if (modelDataChange && modelDataChange.currentValue !== null && modelDataChange.currentValue !== undefined) {
            this.selectOption(modelDataChange.currentValue);
        }
    }


    public setCustomDates(datetimeSince: moment.Moment, datetimeTill: moment.Moment, submit: boolean = true) {

        this.matSinceDatePicker = moment(datetimeSince);
        this.matTillDatePicker = moment(datetimeTill);

        if (submit === true) {
            this.datepickerSubmit();
        }
    }



    public datepickerSubmit() {
        this.onChangeOption(this.customOptionValue, true);
    }


    public async onChangeOption(id: any, submitDates?: boolean) {
        let result: PeriodTypeOption;
        try {
            result = await this.selectOption(id);
            if (Number(result.id) !== this.customOptionValue || submitDates === true) {
                this.modelData = result.id;
                this.modelDataChange.emit(result);
            }
        } catch (e) {
            // could be on initialization where periodOptions are not yes populated
        }
    }


    // public onSelectSinceDate(dateSelected: moment.Moment) {
    //     const clonedSince = this.matSinceDatePicker.clone();
    //     const sincePlus30Days = clonedSince.add(30, 'days').endOf('day');
    //     if (this.matTillDatePicker > sincePlus30Days ) {
    //         this.matTillDatePicker = clonedSince;
    //     }
    // }



    public onSelectSinceDate(dateSelected: moment.Moment) {
        const clonedSince = this.matSinceDatePicker.clone();
        const sincePlus30Days = clonedSince.add(30, 'days').endOf('day');

        if (this.matTillDatePicker > sincePlus30Days ) {
            this.matTillDatePicker = clonedSince;
        }

        if (this.matTillDatePicker < this.matSinceDatePicker) {
            this.matTillDatePicker = this.matSinceDatePicker.clone().endOf('day');
        }
    }

    public onSelectTillDate(dateSelected: moment.Moment) {
        const clonedSince = this.matSinceDatePicker.clone();
        const sincePlus30Days = clonedSince.add(30, 'days').endOf('day');

        if (sincePlus30Days < this.matTillDatePicker) {
            const cloneTill = this.matTillDatePicker.clone();
            const tillSubtract30Days = cloneTill.subtract(30, 'days').startOf('day');

            this.matSinceDatePicker = tillSubtract30Days.clone();

        }

        if (this.matTillDatePicker < this.matSinceDatePicker) {
            this.matSinceDatePicker = this.matTillDatePicker.clone().subtract(30, 'days').startOf('day');
        }
    }





    private selectOption(optionValue: number): Promise<PeriodTypeOption> {
        return new Promise((resolve, reject) => {
            const option = this.periodOptions.find((item) => item.id === Number(optionValue));
            if (!option) {
                // could be on initialization where periodOptions are not yes populated
                reject('option not found');
                return;
            }
            this.showDatepicker = false;

            const dt = this.periodTypeConverter.convert(option.id);

            // Start of week is Sunday (like bq)
            switch (option.id) {
                case BookingsPeriodTypes.Custom:
                    this.showDatepicker = true;
                    option.datetimeSince = this.matSinceDatePicker.startOf('day');
                    option.datetimeTill = this.matTillDatePicker.endOf('day');
                    break;
                default:
                    option.datetimeSince = dt.datetimeSince;
                    option.datetimeTill = dt.datetimeTill;
                    break;
            }

            // assign values to datepickers
            this.setCustomDates(option.datetimeSince, option.datetimeTill, false);

            this.selectedOption = option;
            resolve(this.selectedOption);
        });
    }
}
