// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../auth/authGuard';

// COMPONENTS
import { ViewBookingsComponent } from './view/view-bookings.component';
import { EditBookingComponent } from './edit/edit-booking.component';

const BookingRoutes: Routes = [
    {
        path: '',
        component: ViewBookingsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: ':editMode/:id',
        component: EditBookingComponent,
        canActivate: [AuthGuard],
    },
    {
        path: ':editMode',
        component: EditBookingComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(BookingRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class BookingsRoutingModule { }
