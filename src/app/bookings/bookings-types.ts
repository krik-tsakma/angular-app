import { BookingsPeriodTypes } from './bookings-period-selector/bookings-period-selector-types';
import { PagerRequest } from '../common/pager';
import { BookNGoResults } from '../admin/book-n-go/book-n-go.types';

export interface BookingsRequest extends PagerRequest {
    searchTerm: string;
    periodType: BookingsPeriodTypes;
    periodSince: string;
    periodTill: string;
}

export interface Booking {
    id: number;
    name: string;
    referenceID: string;
    isBlockBooking: boolean;
    pudopID: number;
    pudopLabel: string;
    vehicleLabel: string;
    vehicleID: string;
    vehicleGroupID: string;
    driverLabel: string;
    since: string;
    till: string;
    blockBookingVehicles: BlockBookingVehicle[];
}

export interface BlockBookingVehicle {
    id: number;
    name: string;
    isGroup: boolean;
    vehicleType: string;
}
