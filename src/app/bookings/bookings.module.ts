// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../shared/shared.module';
import { BookingsRoutingModule } from './bookings-routing.module';
import { PeriodSelectorModule } from '../common/period-selector/period-selector.module';
import { DatepickerModule } from '../common/datepicker/datepicker.module';
import { TimePickerModule } from '../common/timepicker/timepicker.module';

// COMPONENTS
import { ViewBookingsComponent } from './view/view-bookings.component';
import { EditBookingComponent } from './edit/edit-booking.component';
import { BookingsPeriodSelectorComponent } from './bookings-period-selector/bookings-period-selector.component';

// SERVICES
import { BookingsService } from './bookings.service';
import { AdminLabelService } from '../admin/admin-label.service';
import { TranslateStateService } from '../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/bookings/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        HttpClientJsonpModule,
        SharedModule,
        PeriodSelectorModule,
        DatepickerModule,
        TimePickerModule,
        BookingsRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewBookingsComponent,
        EditBookingComponent,
        BookingsPeriodSelectorComponent
    ],
    providers: [
        BookingsService,
        AdminLabelService
    ]
})

export class BookingsModule {

}
