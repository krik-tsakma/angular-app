// FRAMEWORK
import { Component, ViewChild, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../admin/admin-base.component';
import { BookingsPeriodSelectorComponent } from '../bookings-period-selector/bookings-period-selector.component';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { BookingsService } from '../bookings.service';
import { ConfirmDialogService } from '../../shared/confirm-dialog/confirm-dialog.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { AdminLabelService } from '../../admin/admin-label.service';
import { Config } from '../../config/config';

// TYPES
import { Pager } from '../../common/pager';
import { Booking, BookingsRequest } from '../bookings-types';
import { StoreItems } from '../../shared/store-manager/store-items';
import { BookingsPeriodTypes } from '../bookings-period-selector/bookings-period-selector-types';
import { PeriodTypeOption } from '../../common/period-selector/period-selector-types';
import {
    TableColumnSetting, TableActionItem, TableActionOptions,
    TableCellFormatOptions, TableColumnSortOrderOptions, TableColumnSortOptions
} from '../../shared/data-table/data-table.types';
import {
     BookNGoDeleteRequest, BookNGoCrudCodeOptions,
     BookNGoCrudResult, BookNGoResults
} from '../../admin/book-n-go/book-n-go.types';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'view-bookings',
    templateUrl: './view-bookings.html'
})

export class ViewBookingsComponent extends AdminBaseComponent implements OnInit, AfterViewInit {
    @ViewChild(BookingsPeriodSelectorComponent, { static: true }) public periodSelectorComponent: BookingsPeriodSelectorComponent;
    public searchTermControl = new FormControl();
    public request: BookingsRequest = {} as BookingsRequest;
    public bookings: Booking[] = [];
    public pager: Pager = new Pager();
    public loading: boolean = false;
    public tableSettings: TableColumnSetting[];

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,
        protected storeManager: StoreManagerService,
        protected configService: Config,
        private userOptions: UserOptionsService,
        private service: BookingsService,
        private cdr: ChangeDetectorRef
    ) {
        super(
            translator, previousRouteService, router, unsubscriber, labelService,
            configService, snackBar, storeManager, confirmDialogService);

        this.tableSettings =  [
            {
                primaryKey: 'referenceID',
                header: 'book_n_go.bookings.referenceID',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'pudopLabel',
                header: 'book_n_go.bookings.pudop',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'PickupPoint')
            },
            {
                primaryKey: 'name',
                header: 'book_n_go.bookings.name',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'vehicleLabel',
                header: 'book_n_go.bookings.vehicle',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'driverLabel',
                header: 'book_n_go.bookings.driver',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'since',
                header: 'book_n_go.bookings.since',
                format: TableCellFormatOptions.dateTime,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, 'Start')
            },
            {
                primaryKey: 'till',
                header: 'book_n_go.bookings.till',
                format: TableCellFormatOptions.dateTime,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'Stop')
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete],
                actionsVisibilityFunction: this.showUDButtons
            }
        ];
    }

    public ngOnInit(): void {
        super.ngOnInit();

        const savedData =  this.storeManager.getOption(StoreItems.bookings) as BookingsRequest;
        if (savedData) {
            this.request = savedData;
            if (savedData.periodType === BookingsPeriodTypes.Custom) {
                this.periodSelectorComponent.setCustomDates(
                    moment(savedData.periodSince),
                    moment(savedData.periodTill)
                );
            }
        } else {
            // set default values, for sorting and period
            this.request.periodType = BookingsPeriodTypes.Today;
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchTermControl.valueChanges
            .pipe(
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    this.request.searchTerm = term;
                    this.get(true);
                    return of([]);
                })
            )
            .subscribe();


    }

    public ngAfterViewInit() {
        this.get(true);
        this.cdr.detectChanges();
    }


    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.searchTermControl.setValue(null);
    }


    // On period select
    public onPeriodSelect(pto: PeriodTypeOption): void {
        this.request.periodType = pto.id;
        this.request.periodSince = pto.datetimeSince.format('YYYY-MM-DD HH:mm');
        this.request.periodTill = pto.datetimeTill.format('YYYY-MM-DD HH:mm');
        this.get(true);
    }


    // ----------------
    // TABLE CALLBACKS
    // ----------------

    // Navigate to edit-quick-filters page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.edit:
                this.manage(id);
                break;
            case TableActionOptions.delete:
                this.openDialog().subscribe((response) => {
                    if (response === true) {
                        this.delete(id);
                    }
                });
                break;
            default:
                break;

        }
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }


    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public showUDButtons(action: TableActionOptions, record: Booking): boolean {
        if (!record) {
            return false;
        }
        if (record.blockBookingVehicles && record.blockBookingVehicles.length > 0) {
            return true;
        }

        return false;
    }


    // ----------------
    // PRIVATE
    // ----------------

    // Fetch events data
    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.bookings = new Array<Booking>();
        }

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        if (!this.request.periodSince) {
            this.request.periodSince = this.periodSelectorComponent.selectedOption.datetimeSince.format('YYYY-MM-DD HH:mm');
            this.request.periodTill = this.periodSelectorComponent.selectedOption.datetimeTill.format('YYYY-MM-DD HH:mm');
        }

        // first stop currently executing requests
        this.unsubscribeService();
        // the save the request's data for future use
        this.storeManager.saveOption(StoreItems.bookings, this.request);

        // then start the new request
        this.serviceSubscriber = this.service
            .get(this.request)
            .subscribe(
                (response: BookNGoResults<Booking>) => {
                    this.loading = false;

                    this.bookings = this.bookings.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;

                    if (!this.bookings || this.bookings.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }


    // ----------------
    // DELETE
    // ----------------
    private delete(id: number) {
        this.loading = true;
        this.unsubscribeService();

        const req = { id: id, culture: this.userOptions.getCulture() } as BookNGoDeleteRequest;
        this.serviceSubscriber = this.service
            .delete(req)
            .subscribe((res: BookNGoCrudResult) => {
                if (res.code !== BookNGoCrudCodeOptions.None) {
                    switch (res.code) {
                        case BookNGoCrudCodeOptions.NotFound:
                            this.snackBar.open('form.errors.not_found', 'form.actions.close');
                            break;
                        case BookNGoCrudCodeOptions.Unknown:
                            this.snackBar.open('form.errors.unknown', 'form.actions.close');
                            break;
                        default:
                            this.snackBar.open(res.localizedErrorMessage, 'form.actions.close');
                            break;
                    }
                } else {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get(true);
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }
}
