// FRAMEWORK
import {
    Component, ViewChild, OnInit,
    ViewEncapsulation, Renderer, ElementRef
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

// RXJS
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, map } from 'rxjs/operators';

// SERVICES
import { AdminBaseComponent } from '../../admin/admin-base.component';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { BookingsService } from '../bookings.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { AdminLabelService } from '../../admin/admin-label.service';
import { Config } from '../../config/config';

// TYPES
import { Booking, BlockBookingVehicle } from '../bookings-types';
import { BookNGoCrudResult, BookNGoUpsertRequest, BookNGoCrudCodeOptions } from '../../admin/book-n-go/book-n-go.types';
import { SearchType } from '../../shared/search-mechanism/search-mechanism-types';
import { KeyName } from '../../common/key-name';
import {
    TableColumnSetting, TableCellFormatOptions,
    TableActionOptions, TableActionItem
} from '../../shared/data-table/data-table.types';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'edit-booking',
    templateUrl: 'edit-booking.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-booking.less']
})

export class EditBookingComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('vehicleTerm', { static: true }) public fileInput: ElementRef;
    public action: string;
    public loading: boolean;
    public editForm: FormGroup;
    public sinceDate: moment.Moment = moment();
    public sinceDateHours: number  = 0;
    public sinceDateMinutes: number  = 0;
    public tillDate: moment.Moment = moment();
    public tillDateHours: number = 1;
    public tillDateMinutes: number  = 0;
    public searchTypes: any = SearchType;
    public vehicleTermControl = new FormControl();
    public blockBookingVehicleLoading: boolean;
    public blockBookingVehicleItems: Observable<BlockBookingVehicle[]>;
    public blockBookingVehicleTableSettings: TableColumnSetting[];

    public booking: Booking = {} as Booking;

    private variousTermTranslation: string;
    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected router: Router,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: BookingsService,
        private userOptions: UserOptionsService,
        private renderer: Renderer,
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar);

        translator.get('book_n_go.bookings.block_booking.is_group').subscribe((t) => {
            this.variousTermTranslation = t;
        });

        this.blockBookingVehicleTableSettings =  [
            {
                primaryKey: 'name',
                header: 'book_n_go.bookings.vehicles',
            },
            {
                primaryKey: 'vehicleType',
                header: 'book_n_go.bookings.block_booking.vehicle_type',
                format: TableCellFormatOptions.custom,
                formatFunction: this.formatVehicleType
            } ,
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.delete]
            }
        ];
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: string;
            }) => {
                this.action = params.editMode === 'edit'
                            ? 'form.actions.edit'
                            : 'form.actions.create';

                if (params.id) {
                    this.get(Number(params.id));
                }
            });

        this.blockBookingVehicleItems = this.vehicleTermControl.valueChanges
            .pipe(
            // Debounce 500 waits for 500ms until user stops pushing
                debounceTime(500),
                // use this to handle scenarios like hitting backspace and retyping the same letter
                distinctUntilChanged(),
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    if (!term || term.length < 2) {
                        return of([]);
                    }
                    this.blockBookingVehicleLoading = true;
                    const res = this.service
                        .searchBlockBookingVehicles(term)
                        .pipe(
                            map((data) => {
                                this.blockBookingVehicleLoading = false;
                                return data;
                            })
                        );
                    return res;
                })
            );

    }

    public get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((data) => {
                this.loading = false;
                this.booking = data;
                this.updateFormElements();
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            });
    }


    public onSubmit({ value, valid }: { value: Booking, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.booking.since = this.sinceDate.startOf('day').add(this.sinceDateHours, 'hours').add(this.sinceDateMinutes, 'minutes').format('YYYY-MM-DD HH:mm');
        this.booking.till = this.tillDate.startOf('day').add(this.tillDateHours, 'hours').add(this.tillDateMinutes, 'minutes').format('YYYY-MM-DD HH:mm');

        this.loading = true;
        this.unsubscribeService();

        // This is create
        const req = { entity: this.booking, culture: this.userOptions.getCulture() } as BookNGoUpsertRequest<Booking>;
        if (typeof(this.booking.id) === 'undefined' || this.booking.id === 0) {
            this.serviceSubscriber = this.service
                .create(req)
                .subscribe((res: BookNGoCrudResult) => {
                    this.loading = false;
                    if (res.code !== BookNGoCrudCodeOptions.None) {
                        const errorMessage = this.getCrudResult(res.code, res.localizedErrorMessage);
                        this.snackBar.open(errorMessage, 'form.actions.close');
                    } else {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else {
            // This is an update
            this.serviceSubscriber = this.service
                .update(req)
                .subscribe((res: BookNGoCrudResult) => {
                    this.loading = false;
                    if (res.code !== BookNGoCrudCodeOptions.None) {
                        const errorMessage = this.getCrudResult(res.code, res.localizedErrorMessage);
                        this.snackBar.open(errorMessage, 'form.actions.close');
                    } else {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    }
                },
                (err) => {
                    this.loading = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                });
        }
    }

    // =========================
    // LOCATION SEARCH
    // =========================

    public onLocationSelect(item?: KeyName) {
        this.booking.pudopID = item ? item.id : null;
        this.booking.pudopLabel = item ? item.name : null;
    }


    // =========================
    // VEHICLES SEARCH
    // =========================

    public onVehicleSelect(item: BlockBookingVehicle) {
        if (!this.booking.blockBookingVehicles) {
            this.booking.blockBookingVehicles = [];
        }
        // append the vehicle to the list only if it is not already in it
        if (this.booking.blockBookingVehicles.find((x) => x.id === item.id) === undefined ) {
            this.booking.blockBookingVehicles.push(item);
            this.clearVehicleSearchInput(null);
        }
    }

    public clearVehicleSearchInput(e: Event) {
        if (e) {
            // do not bubble event otherwise focus is on the textbox
            e.stopPropagation();
        }
        // clear selection
        this.vehicleTermControl.setValue(null);

        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.fileInput.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }

    public formatVehicleType(item: BlockBookingVehicle): string {
        if (!item || item.vehicleType === undefined || item.vehicleType === null) {
            return this.variousTermTranslation;
        }
        return item.vehicleType;
    }

    public onDeleteBlockBookingVehicle(item: TableActionItem) {
        if (!item) {
            return;
        }

        this.booking.blockBookingVehicles = this.booking.blockBookingVehicles.filter((x) => {
            return x.id !== item.record.id;
        });
    }

    // =========================
    // RESULTS
    // =========================

    private getCrudResult(code: BookNGoCrudCodeOptions, message: string): string {
        switch (code) {
            case BookNGoCrudCodeOptions.NotFound:
                message = 'form.errors.not_found';
                break;
            case BookNGoCrudCodeOptions.Unknown:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }


    // =========================
    // PRIVATE FORM
    // =========================

    private updateFormElements() {
        this.sinceDate = moment(this.booking.since);
        this.sinceDateHours = this.sinceDate.get('hour');
        this.sinceDateMinutes = this.sinceDate.get('minute');
        this.tillDate = moment(this.booking.till);
        this.tillDateHours = this.tillDate.get('hour');
        this.tillDateMinutes = this.tillDate.get('minute');
    }

}
