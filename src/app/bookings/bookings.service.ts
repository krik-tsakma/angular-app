// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../auth/authHttp';
import { Config } from '../config/config';

// TYPES
import { 
    Booking, BookingsRequest, BlockBookingVehicle } from './bookings-types';
import { 
    BookNGoDeleteRequest, BookNGoUpsertRequest,
    BookNGoResults, BookNGoCrudResult, BookNGoCrudCodeOptions 
} from '../admin/book-n-go/book-n-go.types';

@Injectable()
export class BookingsService {
    private baseApiURL: string;
    constructor(private authHttp: AuthHttp,
                private config: Config) { 
        
        this.baseApiURL = config.get('apiUrl') + '/api/Bookings';
    }


    // Get 
    public get(request: BookingsRequest): Observable<BookNGoResults<Booking>> {

        const obj = this.authHttp.removeObjNullKeys({
            Since: request.periodSince,
            Till: request.periodTill,
            SearchTerm: request.searchTerm ? request.searchTerm.toString() : null,
            PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
            PageSize: request.pageSize ? request.pageSize.toString() : '20',
            Sorting: request.sorting
        });

        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.baseApiURL, { params });
    }

    public getByID(id: number): Observable<Booking> {
        return this.authHttp
            .get(this.baseApiURL + '/' + id);
    }


    public create(booking: BookNGoUpsertRequest<Booking>): Observable<BookNGoCrudResult>  {
        return this.authHttp
            .post(this.baseApiURL, booking, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        const createdEntity = res.body as BookNGoCrudResult;
                        return { id: createdEntity.id, code: BookNGoCrudCodeOptions.None } as BookNGoCrudResult;
                    } else  {
                        return res.body as BookNGoCrudResult;
                    }
                })
            );
    }

    public update(booking: BookNGoUpsertRequest<Booking>): Observable<BookNGoCrudResult>  {
        return this.authHttp
            .put(this.baseApiURL, booking, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return { code: BookNGoCrudCodeOptions.None } as BookNGoCrudResult;
                    } else  {
                        return res.body as BookNGoCrudResult;
                    }
                })
            );            
    }

    // Delete method 
    public delete(request: BookNGoDeleteRequest): Observable<BookNGoCrudResult>  {

        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                Culture: request.culture
            }
        });

        return this.authHttp
            .delete(this.baseApiURL, { params, observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 200 && !res.body) {
                        return { code: BookNGoCrudCodeOptions.None } as BookNGoCrudResult;
                    } else  {
                        return res.body as BookNGoCrudResult;
                    }
                })
            );
    }

    // Get block booking vehicles
    public searchBlockBookingVehicles(searchTerm: string): Observable<BlockBookingVehicle[]> {


        const params = new HttpParams({
            fromObject: {
                searchTerm
            }
        });

        return this.authHttp
            .get(this.baseApiURL + '/SearchBlockBookingVehicles/', { params });
    }
}
