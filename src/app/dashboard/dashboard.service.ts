﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../auth/authHttp';

// TYPES
import { WidgetCreateRequest } from './edit/edit-widget-types';
import {
    WidgetTypes, BaseWidget, WidgetsResults,
    WidgetZoneUpsertResult
} from './widgets/widget-types';
import { Config } from '../config/config';

@Injectable()
export class WidgetsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/DashboardWidgets';
        }

    // Get all the widgets for Dashboard
    public get(roleID?: number): Observable<WidgetsResults> {
        return this.authHttp
            .get(this.baseURL + '/' + roleID);
    }

    // Get specific widget by id number
    public getWidget(id: number, roleID?: number): Observable<BaseWidget> {
        const obj = this.authHttp.removeObjNullKeys({
            ID: id.toString(),
            RoleID: roleID ? roleID.toString() : null
        });
        const params = new HttpParams({
            fromObject: obj
        });        


        return this.authHttp
            .get(this.baseURL + '/GetSingle', { params });
    }

    // Create new widget - Not in use at the moment
    public create(widget: BaseWidget, type: WidgetTypes, roleID?: number): Observable<number> {
        const req =  {
            widget: widget,
            roleID: roleID
        } as WidgetCreateRequest;
        
        return this.authHttp
            .post(this.baseURL + '/' + type, req, { observe: 'response' })
            .pipe(
                map((res) => res.status)               
            );
    }

    // Update method for widget in edit page
    public update(widget: BaseWidget, type: WidgetTypes, roleID?: number): Observable<number> {
        const req =  {
            widget: widget,
            roleID: roleID
        } as WidgetCreateRequest;
        
        return this.authHttp
            .put(this.baseURL + '/' + type, req, { observe: 'response' })
            .pipe(
                map((res) => res.status)
            );

    }

     // Update method for widget orders
    public updateMany(widgets: BaseWidget[]): Observable<number> {
        return this.authHttp
            .put(this.baseURL + '/ChangeOrder', widgets, { observe: 'response' })
            .pipe(
                map((res) => res.status)
            );
          
    }

    // Update method for widget zone layout 
    public upsertZones(layout: string): Observable<WidgetZoneUpsertResult> {
        const zl = {
            layout: layout
        };
        return this.authHttp
            .post(this.baseURL + '/SetZones', zl);
    }

    // Delete method for widget in edit page
    public delete(id: number, roleID?: number): Observable<number> {

        const obj = this.authHttp.removeObjNullKeys({
            ID: id.toString(),
            RoleID: roleID ? roleID.toString() : null
        });
        const params = new HttpParams({
            fromObject: obj
        }); 
     
        return this.authHttp
            .delete(this.baseURL, { params, observe: 'response' })
            .pipe(
                map((res) => res.status)
            );
    }

    // Get all the widgets for Dashboard
    public deleteAll(): Observable<number> {
        return this.authHttp
            .delete(this.baseURL + '/Reset', { observe: 'response' })
            .pipe(
                map((res) => res.status)
            );
    }
    
    public getAllowedWidgetTypesToAdd(): Observable<number[]> {   
        return this.authHttp
            .get(this.baseURL + '/AllowedWidgetTypesToAdd');
    }

    // Get all the widgets for Dashboard
    public getDriver(id: number): Observable<string> {
        return this.authHttp
            .get(this.baseURL + '/GetDriver/' + id,  { observe: 'response', responseType: 'text' })
            .pipe(
                map((res) => {
                    return res.body as string;
                })
            );
    }
}
