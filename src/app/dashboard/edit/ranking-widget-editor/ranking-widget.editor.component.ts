// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EditWidgetBaseComponent } from '../edit-widget-base.component';

// MOMENT
import moment from 'moment';

// SERVICES
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { WidgetsService } from '../../dashboard.service';
import { PreviewWidgetService } from '../preview-widget.service';

// TYPES
import { RankingWidget, WidgetTypes } from '../../widgets/widget-types';
import { PeriodTypes, PeriodTypeOption } from '../../../common/period-selector/period-selector-types';
import { DashboardAssetTypes, DashboardSelectionItem } from './../asset-selection/asset-selection.types';
import { KeyName } from './../../../common/key-name';
import { SortDirection } from './../../widgets/ranking-widget/ranking-types';

@Component({
    selector: 'ranking-widget-editor',
    templateUrl: 'ranking-widget-editor.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: [],
})

export class RankingWidetEditorComponent extends EditWidgetBaseComponent implements OnInit, OnDestroy {
    public periodSelectorOptions = [
        PeriodTypes.FiscalYearToDate,
        PeriodTypes.Last12Months,
        PeriodTypes.ThisMonth,
        PeriodTypes.ThisWeek,
        PeriodTypes.YearToDate
    ];

    public sortDirections: KeyName[];

    constructor(
        protected translator: CustomTranslateService,
        protected previousRouteService: PreviousRouteService,
        protected unsubscriber: UnsubscribeService,
        protected widgetsService: WidgetsService,
        protected previewWidgetService: PreviewWidgetService,
        protected userOptions: UserOptionsService,
        protected activatedRoute: ActivatedRoute,
        protected snackBar: SnackBarService,
        protected router: Router
    ) {
        super(translator, previousRouteService, unsubscriber, widgetsService,
            previewWidgetService, userOptions, activatedRoute,
            snackBar, router);

        this.sortDirections = this.getSortOptions();
    }


    // =========================
    // ANGULAR LIFECYCLE
    // =========================

    public ngOnInit(): void {

        this.widgetType = WidgetTypes.Ranking;
        this.widget = new RankingWidget('Ranking ' + moment().format(this.userOptions.localOptions.short_date),
                                    1,
                                    1,
                                    0,
                                    PeriodTypes.YearToDate,
                                    10,
                                    null,
                                    null);

        this.setEntityTypeOptionsForWidget();

        super.ngOnInit();
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }


    // =========================
    // PERIOD TYPE CALLBACKS
    // =========================

    // On change period, initialiaze view options and update the preview of the widget
    public onChangePeriod(newPeriod: PeriodTypeOption): void {
        this.widget.periodType = newPeriod.id;
        this.previewWidget();
    }



    // ===============================
    // ASSET TYPE SELECTION CALLBACKS
    // ===============================

    public onRankingAssetSelection(item: DashboardSelectionItem): void {
        this.onAssetSelection(item, true);
    }


    // =========================
    // ELEMENTS VISIBILITY
    // =========================

    public showNumberOfRows(): boolean {
        if (!this.assetSelected || this.assetSelected.type === DashboardAssetTypes.Me) {
            return false;
        }

        return true;
    }



    // =========================
    // ENTITY OPTIONS
    // =========================

    private setEntityTypeOptionsForWidget() {
        if (this.user.isDriver || this.isDriverRole) {
            this.assetTypeOptions = [
                DashboardAssetTypes.Me,
                DashboardAssetTypes.DriverGroupDimensions
            ];
            return;
        }

        this.assetTypeOptions = [
            DashboardAssetTypes.DriverGroups,
            DashboardAssetTypes.DriverGroupDimensions,
            DashboardAssetTypes.DriverUsers
        ];
    }

    private getSortOptions(): KeyName[] {
        return [
            {
                id: SortDirection.ASC,
                term: 'dashboard.ranking_widget.asc',
                enabled: true,
                name: ''
            },
            {
                id: SortDirection.DESC,
                term: 'dashboard.ranking_widget.desc',
                enabled: true,
                name: ''
            }
        ];
    }
}
