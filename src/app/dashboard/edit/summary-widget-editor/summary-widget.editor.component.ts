
// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EditWidgetBaseComponent } from '../edit-widget-base.component';

// MOMENT
import moment from 'moment';

// SERVICES
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { WidgetsService } from '../../dashboard.service';
import { PreviewWidgetService } from '../preview-widget.service';

// TYPES
import { PeriodTypes, PeriodTypeOption } from '../../../common/period-selector/period-selector-types';
import { AssetTypes } from '../../../common/entities/entity-types';
import { SummaryTypes } from '../../widgets/summary-widget/summary-types';
import { SummaryWidget, SummaryWidgetValueTypes, WidgetTypes } from '../../widgets/widget-types';
import { DistributionTypes } from '../../../common/distribution-types/distribution-types';
import { DashboardAssetTypes, DashboardSelectionItem } from './../asset-selection/asset-selection.types';
import { KeyName } from './../../../common/key-name';

@Component({
    selector: 'summary-widget-editor',
    templateUrl: 'summary-widget-editor.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: [],
})

export class SummaryWidgetEditorComponent extends EditWidgetBaseComponent implements OnInit, OnDestroy {
    public loadingEventProperties: boolean;
    public periodSelectorOptions = [
        PeriodTypes.FiscalYearToDate,
        PeriodTypes.Last12Months,
        PeriodTypes.Last13Months,
        PeriodTypes.LastMonth,
        PeriodTypes.LastWeek,
        PeriodTypes.PreviousFiscalYear,
        PeriodTypes.ThisMonth,
        PeriodTypes.ThisWeek,
        PeriodTypes.YearToDate
    ];


    public summaryOptions: KeyName[];
    public valueTypes: KeyName[];

    constructor(
        protected translator: CustomTranslateService,
        protected previousRouteService: PreviousRouteService,
        protected unsubscriber: UnsubscribeService,
        protected widgetsService: WidgetsService,
        protected previewWidgetService: PreviewWidgetService,
        protected userOptions: UserOptionsService,
        protected activatedRoute: ActivatedRoute,
        protected snackBar: SnackBarService,
        protected router: Router
    ) {
        super(translator, previousRouteService, unsubscriber, widgetsService,
            previewWidgetService, userOptions, activatedRoute,
            snackBar, router);

        this.summaryOptions = this.getSummaryTypeOptions();
        this.valueTypes = this.getValueTypes();
    }


    // =========================
    // ANGULAR LIFECYCLE
    // =========================

    public ngOnInit(): void {
        this.widgetType = WidgetTypes.Summary;
        this.widget = new SummaryWidget('Total distance ' + moment().format(this.userOptions.localOptions.short_date),
                            1,
                            1,
                            AssetTypes.Vehicle,
                            SummaryTypes.TotalDistance,
                            PeriodTypes.YearToDate,
                            SummaryWidgetValueTypes.Volume,
                            null,
                            null,
                            null,
                            null,
                            0);
        this.setEntityTypeOptionsForWidget();

        super.ngOnInit();
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // On change period, initialiaze view options and update the preview of the widget
    public onChangePeriod(newPeriod: PeriodTypeOption): void {
        this.widget.periodType = newPeriod.id;
        this.previewWidget();
    }

    // =========================
    // WIDGET TYPE CALLBACKS
    // =========================

    // On change widget type
    public onChangeType(): void {
        this.widget.targetValue = null;
        this.widget.valueType = SummaryWidgetValueTypes.Volume;
        this.widget.energyTypeID = null;
        switch (this.widget.type) {
            case SummaryTypes.Score:
            case SummaryTypes.ScoreDriven:
                this.widget.scoreID = 0;
                break;
            default:
                this.widget.scoreID = null;
                break;
        }

        this.previewWidget();
    }


   // ===============================
    // ASSET TYPE SELECTION CALLBACKS
    // ===============================

    public onSummaryAssetSelection(item: DashboardSelectionItem): void {
        this.onAssetSelection(item, true);
    }


    // =============================
    // DISTRIBUTION TYPES CALLBACK
    // =============================
    public onChangeDistributionType(newType: DistributionTypes): void {
        this.widget.distributeBy = Number(newType);
        this.previewWidget();
    }


    // =========================
    // ELEMENTS VISIBILITY
    // =========================

    public showOptions(): boolean {
        return this.showValueTypeSelect() ||
                this.showEnergyTypeInput() ||
                this.showTargetInput() ||
                this.showScoresInput()
        ;
    }


    public showScoreThresholdInput(): boolean {
        return this.widget.type === SummaryTypes.ScoreDriven;
    }

    public showScoresInput(): boolean {
        switch (this.widget.type) {
            case SummaryTypes.Score:
            case SummaryTypes.ScoreDriven:
                return true;
            default:
                return false;
        }
    }

    public showDistributionOptionsInput(): boolean {
        return this.widget.type === SummaryTypes.ScoreDriven;
    }

    public showTargetInput(): boolean {
        switch (this.widget.type) {
            case SummaryTypes.AccountedEnergy:
            case SummaryTypes.UnaccountedEnergy:
            case SummaryTypes.CO2Emission:
            case SummaryTypes.ScoreDriven:
                return true;
            default:
                return false;
        }
    }

    public showEnergyTypeInput(): boolean {
        switch (this.widget.type) {
            case SummaryTypes.AccountedEnergy:
            case SummaryTypes.UnaccountedEnergy:
            case SummaryTypes.EnergySavings:
            case SummaryTypes.EnergySavingsPotential:
            case SummaryTypes.EnergyUsed:
            case SummaryTypes.EnergyEfficiency:
                return true;
            default:
                return false;
        }
   }


    public showValueTypeSelect(): boolean {
        switch (this.widget.type) {
            case SummaryTypes.EnergySavings:
            case SummaryTypes.EnergySavingsPotential:
                return true;
            default:
                return false;
        }
    }

    // =========================
    // ENTITY OPTIONS
    // =========================

    private setEntityTypeOptionsForWidget() {
        if (this.user.isDriver || this.isDriverRole) {
            this.assetTypeOptions = [
                DashboardAssetTypes.Me,
                DashboardAssetTypes.DriverGroupDimensions
            ];

            if (this.user.isManager) {
                this.assetTypeOptions.push(DashboardAssetTypes.VehicleGroups);
                this.assetTypeOptions.push(DashboardAssetTypes.DriverGroups);
                this.assetTypeOptions.push(DashboardAssetTypes.VehicleTypes);
                this.assetTypeOptions.push(DashboardAssetTypes.DriverUsers);
            }
        } else {
            this.assetTypeOptions = [
                DashboardAssetTypes.VehicleGroups,
                DashboardAssetTypes.DriverGroups,
                DashboardAssetTypes.VehicleTypes,
                DashboardAssetTypes.DriverUsers
            ];
        }
    }

    private getSummaryTypeOptions(): KeyName[] {
        return [
            {
                id: SummaryTypes.TotalDistance,
                term: 'dashboard.summary_widget.type.total_distance',
                enabled: true,
                name: ''
            },
            {
                id: SummaryTypes.TotalDuration,
                term: 'dashboard.summary_widget.type.total_duration',
                enabled: true,
                name: ''
            },
            {
                id: SummaryTypes.EnergyUsed,
                term: 'dashboard.summary_widget.type.energy_used',
                enabled: true,
                name: ''
            },
            {
                id: SummaryTypes.EnergyEfficiency,
                term: 'dashboard.summary_widget.type.energy_efficiency',
                enabled: true,
                name: ''
            },
            {
                id: SummaryTypes.EnergySavings,
                term: 'dashboard.summary_widget.type.energy_savings',
                enabled: true,
                name: ''
            },
            {
                id: SummaryTypes.EnergySavingsPotential,
                term: 'dashboard.summary_widget.type.energy_savings_potential',
                enabled: true,
                name: ''
            },
            {
                id: SummaryTypes.CO2Emission,
                term: 'dashboard.summary_widget.type.CO2_Emission',
                enabled: true,
                name: ''
            },
            {
                id: SummaryTypes.AccountedEnergy,
                term: 'dashboard.summary_widget.type.accounted_energy',
                enabled: true,
                name: ''
            },
            {
                id: SummaryTypes.UnaccountedEnergy,
                term: 'dashboard.summary_widget.type.unaccounted_energy',
                enabled: true,
                name: ''
            },
            {
                id: SummaryTypes.ScoreDriven,
                term: 'dashboard.summary_widget.type.score_driven',
                enabled: true,
                name: ''
            },
            {
                id: SummaryTypes.Score,
                term: 'dashboard.summary_widget.type.score',
                enabled: true,
                name: ''
            }
        ];
    }

    private getValueTypes() {
        return [
            {
                id: SummaryWidgetValueTypes.Volume,
                term: 'dashboard.summary_widget.volume',
                enabled: true,
                name: ''
            },
            {
                id: SummaryWidgetValueTypes.Percentage,
                term: 'dashboard.summary_widget.percentage',
                enabled: true,
                name: ''
            }
        ];
    }
}
