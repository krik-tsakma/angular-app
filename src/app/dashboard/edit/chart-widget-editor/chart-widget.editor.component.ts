// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// MOMENT
import moment from 'moment';

// SERVICES
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { WidgetsService } from '../../dashboard.service';
import { PreviewWidgetService } from '../preview-widget.service';

// TYPES
import { EditWidgetBaseComponent } from '../edit-widget-base.component';
import { PeriodTypes, PeriodTypeOption } from '../../../common/period-selector/period-selector-types';
import { AssetTypes } from '../../../common/entities/entity-types';
import { AssetGroupItem } from '../../../admin/asset-groups/asset-groups.types';
import { ChartTypes } from '../../widgets/chart-widget/chart-types';
import { ViewGranularityOptions } from '../../dashboard-types';
import { ChartWidget, WidgetTypes } from '../../widgets/widget-types';
import { DistributionTypes } from '../../../common/distribution-types/distribution-types';
import { AssetGroupTypes } from './../../../admin/asset-groups/asset-groups.types';
import { KeyName } from './../../../common/key-name';
import { DashboardSelectionItem, DashboardAssetTypes } from './../asset-selection/asset-selection.types';

@Component({
    selector: 'chart-widget-editor',
    templateUrl: 'chart-widget-editor.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: [],
    providers: []
})

export class ChartWidgetEditorComponent extends EditWidgetBaseComponent implements OnInit, OnDestroy {
    public periodSelectorOptions = [
        PeriodTypes.FiscalYearToDate,
        PeriodTypes.Last12Months,
        PeriodTypes.Last13Months,
        PeriodTypes.LastMonth,
        PeriodTypes.LastWeek,
        PeriodTypes.PreviousFiscalYear,
        PeriodTypes.ThisMonth,
        PeriodTypes.ThisWeek,
        PeriodTypes.YearToDate
    ];

    public chartOptions: KeyName[];
    public groupTypes = AssetGroupTypes;
    public granularityOptions = ViewGranularityOptions;
    public referenceAssetSelected: DashboardSelectionItem;

    constructor(
        protected translator: CustomTranslateService,
        protected previousRouteService: PreviousRouteService,
        protected unsubscriber: UnsubscribeService,
        protected widgetsService: WidgetsService,
        protected previewWidgetService: PreviewWidgetService,
        protected userOptions: UserOptionsService,
        protected activatedRoute: ActivatedRoute,
        protected snackBar: SnackBarService,
        protected router: Router
    ) {
        super(translator, previousRouteService, unsubscriber, widgetsService,
            previewWidgetService, userOptions, activatedRoute,
            snackBar, router);
        // foo
    }


    // =========================
    // LIFECYCLE
    // =========================

    public ngOnInit(): void {
        this.chartOptions = this.getChartTypeOptions();
        this.widgetType = WidgetTypes.Chart;
        this.widget = new ChartWidget('Energy savings ' + moment().format(this.userOptions.localOptions.short_date),
                                    2,
                                    1,
                                    AssetTypes.Vehicle,
                                    ChartTypes.EnergySavings,
                                    PeriodTypes.YearToDate,
                                    ViewGranularityOptions.Monthly,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    -1);

        this.setEntityTypeOptionsForWidget();
        this.resetReferenceAssetSelection();

        super.ngOnInit();

        this.referenceAssetSelected = {
            id: this.widget.referenceID,
            type: this.assetSelected.type
        };
    }


    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }


    // =========================
    // PERIOD TYPES CALLBACKS
    // =========================

    // On change period, initialiaze view options and update the preview of the widget
    public onChangePeriod(newPeriod: PeriodTypeOption): void {
        if (this.widget.periodType !== newPeriod.id) {
            const newGranularityValue = this.getDefaultGranularity(newPeriod.id);
            this.widget.viewGranularity = newGranularityValue;
        }
        this.widget.periodType = newPeriod.id;
        this.previewWidget();
    }


    // =========================
    // WIDGET TYPE CALLBACKS
    // =========================

    // On change widget type
    public onChangeChartType(type: ChartTypes): void {
        this.setEntityTypeOptionsForWidget();
        if (this.widget.type === ChartTypes.ScoreEvolution || this.widget.type === ChartTypes.ScoreDistribution) {
            this.widget.scoreID = 0;
        } else {
            this.widget.scoreID = null;
        }

        this.resetReferenceAssetSelection();
        this.previewWidget();
    }


    // ===============================
    // ASSET TYPE SELECTION CALLBACKS
    // ===============================

    public onDashboardAssetSelection(item: DashboardSelectionItem): void {
        const widgetBeforeChange = Object.assign({}, this.widget);
        this.onAssetSelection(item, false);

        if (widgetBeforeChange.assetType !== this.widget.assetType) {
            this.resetReferenceAssetSelection();
        } else if (this.showReferenceAssetsOptions()) {
            this.widget.referenceID = widgetBeforeChange.referenceID;
            this.referenceAssetSelected = {
                id: this.widget.referenceID,
                type: this.assetSelected.type
            };
        }

        this.previewWidget();
    }

    public onReferenceAssetSelection(item: DashboardSelectionItem): void {
        if (!item) {
            return;
        }
        this.widget.referenceID = item.id;
        this.previewWidget();
    }


    // =============================
    // DISTRIBUTION TYPES CALLBACK
    // =============================

    public onChangeDistributionType(newType: DistributionTypes): void {
        this.widget.distributeBy = Number(newType);
        this.previewWidget();
    }


    // =============================
    // DEVICE TYPE GROUPS CALLBACK
    // =============================

    // When the user selects a device type group from the DDL
    public onChangeDeviceTypeGroup(newDeviceGroup?: AssetGroupItem): void {
        this.widget.deviceTypeGroupID = newDeviceGroup.id;
        if (!this.widget.scoreID) {
            this.widget.scoreID = 0;
        }

        this.previewWidget();
    }


    // =========================
    // ELEMENTS VISIBILITY
    // =========================

    public showReferenceAssetsOptions(): boolean {
        if (this.assetSelected.type === DashboardAssetTypes.Me
            || this.assetSelected.type === DashboardAssetTypes.DriverGroupDimensions
            || this.widget.type === ChartTypes.EnergySavings
        ) {
            return false;
        }

        return true;
    }

    public showScoresInput(): boolean {
        switch (this.widget.type) {
            case ChartTypes.EnergySavings:
            case ChartTypes.AccelerometerEvents:
                return false;
            default:
                return true;
        }
    }

    public showTargetInput(): boolean {
        switch (this.widget.type) {
            case ChartTypes.ScoreDistribution:
                return true;
            default:
                return false;
        }
    }

    public showEnergyTypeInput(): boolean {
        switch (this.widget.type) {
            case ChartTypes.EnergySavings:
                return true;
            default:
                return false;
        }
   }

    public showEnergyScoreOption(): boolean {
        switch (this.widget.type) {
            case ChartTypes.ScoreBuildUp:
                return false;
            default:
                return true;
        }
    }

    public showViewGranularityInput(): boolean {
        switch (this.widget.type) {
            case ChartTypes.ScoreBuildUp:
            case ChartTypes.AccelerometerEvents:
                return false;
            default:
                return true;
        }
    }

    public showDeviceTypeGroupsInput(): boolean {
        switch (this.widget.type) {
            case ChartTypes.ScoreBuildUp :
                return true;
            default:
                return false;
        }
    }

    public showDistributionOptionsInput(): boolean {
        switch (this.widget.type) {
            case ChartTypes.ScoreDistribution :
                return true;
            default:
                return false;
        }
    }

    public getGranularityOptionAvailability(option: ViewGranularityOptions): boolean {
        let isDisabled: boolean;
        switch (this.widget.periodType) {
            case PeriodTypes.YearToDate:
            case PeriodTypes.FiscalYearToDate:
            case PeriodTypes.Last12Months:
            case PeriodTypes.Last13Months:
            case PeriodTypes.PreviousFiscalYear:
                if (option === ViewGranularityOptions.Daily) {
                    isDisabled = true;
                }
                break;

            case PeriodTypes.LastMonth:
            case PeriodTypes.ThisMonth:
                if (option === ViewGranularityOptions.Monthly) {
                    isDisabled = true;
                }
                break;

            case PeriodTypes.LastWeek:
            case PeriodTypes.ThisWeek:
                if (option === ViewGranularityOptions.Weekly
                    || option === ViewGranularityOptions.Monthly) {
                    isDisabled = true;
                }
                break;

            default:
                isDisabled = false;
                break;
        }
        return isDisabled;
    }


    // =========================
    // PRIVATE
    // =========================

    private resetReferenceAssetSelection(): void {
        if (!this.showReferenceAssetsOptions()) {
            this.referenceAssetSelected = { } as DashboardSelectionItem;
            return;
        }

        // -1 means NONE,
        // NULL means ALL
        this.referenceAssetSelected = {
            id: -1,
            type: this.assetSelected.type
        } as DashboardSelectionItem;
    }

    private getDefaultGranularity(option: PeriodTypes): number {
        let granularity: number;
        switch (option) {
            case PeriodTypes.YearToDate:
            case PeriodTypes.FiscalYearToDate:
            case PeriodTypes.Last12Months:
            case PeriodTypes.Last13Months:
            case PeriodTypes.PreviousFiscalYear:
                granularity = ViewGranularityOptions.Monthly;
                break;

            case PeriodTypes.LastMonth:
            case PeriodTypes.ThisMonth:
                granularity = ViewGranularityOptions.Weekly;
                break;

            case PeriodTypes.LastWeek:
            case PeriodTypes.ThisWeek:
                granularity = ViewGranularityOptions.Daily;
                break;

            default:
                granularity = ViewGranularityOptions.Daily;
                break;
        }
        return granularity;
    }


    private setEntityTypeOptionsForWidget() {
        if (this.widget.type === ChartTypes.AccelerometerEvents) {
            this.assetTypeOptions = [DashboardAssetTypes.Me];
            return;
        }
        if (this.user.isDriver || this.isDriverRole) {
            this.assetTypeOptions = [
                DashboardAssetTypes.Me,
                DashboardAssetTypes.DriverGroupDimensions
            ];

            if (this.user.isManager && this.isDriverRole === false) {
                this.assetTypeOptions.push(DashboardAssetTypes.VehicleGroups);
                this.assetTypeOptions.push(DashboardAssetTypes.DriverGroups);
                this.assetTypeOptions.push(DashboardAssetTypes.VehicleTypes);
                this.assetTypeOptions.push(DashboardAssetTypes.DriverUsers);
            }
        } else {
            this.assetTypeOptions = [
                DashboardAssetTypes.VehicleGroups,
                DashboardAssetTypes.DriverGroups,
                DashboardAssetTypes.VehicleTypes,
                DashboardAssetTypes.DriverUsers
            ];
        }

        super.setAssetSelection();
    }

    private getChartTypeOptions(): KeyName[] {
        return [
            {
                id: ChartTypes.EnergySavings,
                term: 'dashboard.chart_widget.type.energy_savings',
                enabled: true,
                name: 'Energy savings'
            },
            {
                id: ChartTypes.ScoreEvolution,
                term: 'dashboard.chart_widget.type.score_evolution',
                enabled: true,
                name: 'Score evolution'
            },
            {
                id: ChartTypes.ScoreDistribution,
                term: 'dashboard.chart_widget.type.score_distribution',
                enabled: true,
                name: 'Score distribution'
            },
            {
                id: ChartTypes.ScoreBuildUp,
                term: 'dashboard.chart_widget.type.score_build_up',
                enabled: true,
                name: 'Score build up'
            },
            {
                id: ChartTypes.AccelerometerEvents,
                term: 'dashboard.chart_widget.type.accelerometer_events',
                enabled: this.user.isDriver,
                name: 'Accelerometer events'
            }];
    }
}
