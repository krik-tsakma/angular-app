// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { PeriodSelectorModule } from '../../common/period-selector/period-selector.module';
import { AssetGroupsModule } from '../../common/asset-groups/asset-groups.module';
import { DistributionTypesModule } from '../../common/distribution-types/distribution-types.module';
import { ScoresListModule } from '../../common/scores-list/scores-list.module';
import { AssetGroupDimensionsModule } from '../../common/asset-group-dimensions/asset-group-dimensions.module';
import { VehicleTypesListModule } from '../../common/vehicle-types-list/vehicle-types-list.module';
import { EnergyTypesListModule } from '../../common/energy-types/energy-types-list.module';
import { EvenstClassesListModule } from '../../common/events-classes-list/events-classes-list.module';
import { EventsPrioritiesListModule } from '../../common/events-priorities-list/events-priorities-list.module';

// COMPONENTS
import { EventsWidetEditorComponent } from './events-widget-editor/events-widget.editor.component';
import { RankingWidetEditorComponent } from './ranking-widget-editor/ranking-widget.editor.component';
import { SummaryWidgetEditorComponent } from './summary-widget-editor/summary-widget.editor.component';
import { ChartWidgetEditorComponent } from './chart-widget-editor/chart-widget.editor.component';
import { DashboardAssetSelectionComponent } from './asset-selection/asset-selection.component';

// SERVICES
import { WidgetsService } from '../dashboard.service';
import { PreviewWidgetService } from './preview-widget.service';


@NgModule({
    imports: [
        SharedModule,
        PeriodSelectorModule,
        AssetGroupsModule,
        DistributionTypesModule,
        ScoresListModule,
        AssetGroupDimensionsModule,
        VehicleTypesListModule,
        EnergyTypesListModule,
        EventsPrioritiesListModule,
        EvenstClassesListModule,
    ],
    declarations: [
        ChartWidgetEditorComponent,
        SummaryWidgetEditorComponent,
        EventsWidetEditorComponent,
        RankingWidetEditorComponent,
        DashboardAssetSelectionComponent
    ],
    exports: [
        ChartWidgetEditorComponent,
        SummaryWidgetEditorComponent,
        EventsWidetEditorComponent,
        RankingWidetEditorComponent,
        DashboardAssetSelectionComponent
    ],
    providers: [
        PreviewWidgetService,
        WidgetsService
    ]
})
export class EditDashboardWidgetsModule { }
