// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// SERVICES
import { PreviewWidgetService } from './preview-widget.service';
import { ConfirmDialogService } from '../../shared/confirm-dialog/confirm-dialog.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { WidgetsService } from '../dashboard.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { Config } from '../../config/config';

// COMPONENTS
import { DeleteWidgetBaseComponent } from '../delete-widget-base.component';

// TYPES
import { BaseWidget, WidgetTypes } from '../widgets/widget-types';

@Component({
    selector: 'edit-widget',
    templateUrl: './edit-widget.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./edit-widget.less'],
    providers: [PreviewWidgetService]
})

export class EditWidgetComponent extends DeleteWidgetBaseComponent implements OnInit, OnDestroy {
    public action: string;
    public widget: any = {} as BaseWidget;
    public widgetType: WidgetTypes;
    public widgetTypes = WidgetTypes;
    public widgetID: number;

    constructor(protected service: WidgetsService,
                protected snackBar: SnackBarService,
                protected confirmDialogService: ConfirmDialogService,
                protected unsubscriber: UnsubscribeService,
                private activatedRoute: ActivatedRoute,
                private previewWidget: PreviewWidgetService,
                private previousRouteService: PreviousRouteService,
                private router: Router,
                private configService: Config) { 
        
                super(unsubscriber, service, confirmDialogService , snackBar);

                // subscribe to widget changes
                this.previewWidget
                    .changedWidget$
                    .subscribe((item: any) => {
                        if (item != null) {
                            this.widget =  Object.assign({}, item);
                        }
                    });
     }

    public ngOnInit(): void {
        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                type: WidgetTypes,
                id: number;
            }) => { 
            this.widgetType = params.type;
            this.widgetID = params.id;
            this.action = params.editMode === 'edit' 
                            ? 'dashboard.edit_widget.title'
                            : 'dashboard.add_widget';
        });
    }

    public ngOnDestroy() {
        super.ngOnDestroy();
    }

    public delete() {
        this.openDeleteDialog(this.widgetID)
            .then(() => {
                this.back();
            });
    }

    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router')});
    }
}
