﻿import { BaseWidget } from '../widgets/widget-types';

export interface WidgetCreateRequest {
    widget: BaseWidget;
    roleID?: number;
}
