import { DashboardAssetTypes, DashboardSelectionItem } from './../asset-selection/asset-selection.types';
// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EditWidgetBaseComponent } from '../edit-widget-base.component';

// RXJS
import { Subscription } from 'rxjs';

// MOMENT
import moment from 'moment';

// SERVICES
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { WidgetsService } from '../../dashboard.service';
import { PreviewWidgetService } from '../preview-widget.service';
import { EventsService } from '../../../events/events.service';

import { EventsWidget } from '../../widgets/events-widget/events-widget.types';
import { PeriodTypes, PeriodTypeOption } from '../../../common/period-selector/period-selector-types';
import { AssetTypes } from '../../../common/entities/entity-types';
import { WidgetTypes } from '../../widgets/widget-types';

@Component({
    selector: 'events-widget-editor',
    templateUrl: 'events-widget-editor.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: [],
    providers: [EventsService]
})

export class EventsWidetEditorComponent extends EditWidgetBaseComponent implements OnInit, OnDestroy {
    public periodSelectorOptions = [
        PeriodTypes.FiscalYearToDate,
        PeriodTypes.Last12Months,
        PeriodTypes.Last13Months,
        PeriodTypes.LastMonth,
        PeriodTypes.LastWeek,
        PeriodTypes.PreviousFiscalYear,
        PeriodTypes.ThisMonth,
        PeriodTypes.ThisWeek,
        PeriodTypes.YearToDate
    ];

    public propertiesSubscriber: Subscription;

    constructor(
        protected translator: CustomTranslateService,
        protected previousRouteService: PreviousRouteService,
        protected unsubscriber: UnsubscribeService,
        protected widgetsService: WidgetsService,
        protected previewWidgetService: PreviewWidgetService,
        protected userOptions: UserOptionsService,
        protected activatedRoute: ActivatedRoute,
        protected snackBar: SnackBarService,
        protected router: Router,
    ) {
        super(translator, previousRouteService, unsubscriber, widgetsService,
            previewWidgetService, userOptions, activatedRoute,
            snackBar, router);
        // foo
    }

    // =========================
    // ANGULAR LIFECYCLE
    // =========================

    public ngOnInit(): void {
        this.widgetType = WidgetTypes.Events;
        this.widget = new EventsWidget('Events ' + moment().format(this.userOptions.localOptions.short_date),
                                        1,
                                        1,
                                        AssetTypes.Vehicle,
                                        PeriodTypes.YearToDate,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null);

        this.setEntityTypeOptionsForWidget();

        super.ngOnInit();
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }


    // =========================
    // PERIOD TYPE CALLBACKS
    // =========================

    // On change period, initialiaze view options and update the preview of the widget
    public onChangePeriod(newPeriod: PeriodTypeOption): void {
        this.widget.periodType = newPeriod.id;
        this.previewWidget();
    }


    // ===============================
    // ASSET TYPE SELECTION CALLBACKS
    // ===============================

    public onEventsAssetSelection(item: DashboardSelectionItem): void {
        this.onAssetSelection(item, true);
    }


    // =========================
    // ENTITY OPTIONS
    // =========================

    private setEntityTypeOptionsForWidget() {
        if (this.user.isDriver || this.isDriverRole) {
            this.assetTypeOptions = [
                DashboardAssetTypes.Me,
                DashboardAssetTypes.DriverGroupDimensions
            ];

            if (this.user.isManager) {
                this.assetTypeOptions.push(DashboardAssetTypes.VehicleGroups);
                this.assetTypeOptions.push(DashboardAssetTypes.DriverGroups);
                this.assetTypeOptions.push(DashboardAssetTypes.VehicleTypes);
                this.assetTypeOptions.push(DashboardAssetTypes.DriverUsers);
            }
        } else {
            this.assetTypeOptions = [
                DashboardAssetTypes.VehicleGroups,
                DashboardAssetTypes.DriverGroups,
                DashboardAssetTypes.VehicleTypes,
                DashboardAssetTypes.DriverUsers
            ];
        }
    }
}
