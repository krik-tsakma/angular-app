﻿// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Subject } from 'rxjs';

@Injectable()
export class PreviewWidgetService {

    // Observable string sources
    public changedWidgetSource = new Subject();

    // Observable string streams
    public changedWidget$ = this.changedWidgetSource.asObservable();

    // Service message commands
    public changePreviewWidget(item: any) {
        this.changedWidgetSource.next(item);
    }
}
