
// FRAMEWORK
import { OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// RXJS
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { WidgetsService } from '../dashboard.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { PreviewWidgetService } from './preview-widget.service';

// TYPES
import { WidgetTypes } from '../widgets/widget-types';
import { User } from '../../auth/user';
import { AssetTypes } from '../../common/entities/entity-types';
import { DashboardSelectionItem, DashboardAssetTypes } from './asset-selection/asset-selection.types';

export class EditWidgetBaseComponent implements OnInit, OnDestroy {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    @Input() public roleID?: number;
    @Input() public isDriverRole?: boolean;
    @Input() public widgetID: number;
    @Output() public onComplete: EventEmitter<void> = new EventEmitter();

    public widgetType: WidgetTypes;
    public loading: boolean;
    public serviceSubscriber: Subscription;
    public formSubscriber: Subscription;
    public message: string;
    public widget: any;

    // asset selection
    public assetSelected: DashboardSelectionItem;
    public assetTypeOptions: DashboardAssetTypes[];

    // current user
    public user: User;

    constructor(        
        protected translator: CustomTranslateService,
        protected previousRouteService: PreviousRouteService,
        protected unsubscriber: UnsubscribeService,
        protected widgetsService: WidgetsService,
        protected previewWidgetService: PreviewWidgetService,
        protected userOptions: UserOptionsService,
        protected activatedRoute: ActivatedRoute,
        protected snackBar: SnackBarService,
        protected router: Router
    ) {
        this.user = userOptions.getUser();
    }


    // =========================
    // ANGULAR LIFECYCLE
    // =========================

    public ngOnInit(): void {
        // this is for widget creation
        this.setAssetSelection();
        if (this.user.isDriver || this.user.isManager || this.isDriverRole === true) {
            this.widget.assetID = this.isDriverRole ? -1 : this.user.driverId;
            this.widget.assetType = AssetTypes.HR;
            this.assetSelected = {
                id: this.widget.assetID,
                type: DashboardAssetTypes.Me
            };
        }

        // for widget editing
        if (this.widgetID) {
            this.get(this.widgetID);
        } 

        // form changes subscription
        this.formSubscriber = this.editForm
            .valueChanges
            .pipe(
                debounceTime(500) // Debounce 500 waits for 500ms until user stops pushing   
            ) 
            .subscribe((changes: any) => {
                this.previewWidget();
            });
    }

    public ngOnDestroy(): void {
        this.unsubscriber.removeSubscription(this.formSubscriber);
        this.unsubscribeService();
    }


    // =========================
    // COMMON 
    // =========================
    // Click back arrow
    public done() {
        this.onComplete.emit();
    }

    public unsubscribeService(): void {
        this.unsubscriber.removeSubscription(this.serviceSubscriber);        
    }

    public onAssetSelection(item: DashboardSelectionItem, preview: boolean): void {
        if (!item) {
            return;
        }

        this.widget.assetID = null;
        this.widget.groupID = null; 
        this.widget.groupDimensionID = null;
        this.widget.vehicleTypeID = null;
        this.widget.referenceID = -1; 
        switch (item.type) {
            case DashboardAssetTypes.VehicleGroups:
                this.widget.groupID = item.id;
                this.widget.assetType = AssetTypes.Vehicle;
                break;
            case DashboardAssetTypes.DriverGroups:
                this.widget.groupID = item.id;
                this.widget.assetType = AssetTypes.HR;
                break;
           
            case DashboardAssetTypes.VehicleTypes:
                this.widget.vehicleTypeID = item.id;
                this.widget.assetType = AssetTypes.Vehicle;
                break;
            case DashboardAssetTypes.Me:
                this.widget.assetID = this.isDriverRole ? -1 : this.user.driverId;
                this.widget.assetType = AssetTypes.HR;
                break;
                case DashboardAssetTypes.DriverGroupDimensions:
                this.widget.assetID = this.isDriverRole ? -1 : this.user.driverId;
                this.widget.groupDimensionID = item.id;
                this.widget.assetType = AssetTypes.HR;
                break;
            case DashboardAssetTypes.DriverUsers:
                this.widget.assetID = item.id;
                this.widget.assetType = AssetTypes.HR;
                break;
        }
        if (preview) {
            this.previewWidget();
        }
    }


    // =========================
    // FORM 
    // =========================

    public previewWidget(): void {
        if (this.editForm.invalid) {
            return;
        }
        this.previewWidgetService.changePreviewWidget(this.widget);
    }    

    public onSubmit({ value, valid }: { value: any, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        
        // This is create
        if (typeof(this.widget.id) === 'undefined' || this.widget.id === 0) {
           this.create();
        } else { 
           this.update();
        }
    }

    public setAssetSelection(): DashboardSelectionItem {
        if (!this.widget) {
            return;
        }
        const isForDriver = this.isDriverRole || this.user.isDriver;
        if (isForDriver && this.widget.assetID && !this.widget.groupDimensionID) {
            this.assetSelected = {
                id: this.widget.assetID,
                type: DashboardAssetTypes.Me
            };
        } else if (this.widget.groupDimensionID) {
            this.assetSelected = {
                id: this.widget.groupDimensionID,
                type: DashboardAssetTypes.DriverGroupDimensions
            };
        } else {
            switch (this.widget.assetType) {
                case AssetTypes.HR: {
                    if (this.widget.assetID) {
                        this.assetSelected = {
                            id: this.widget.assetID,
                            type: DashboardAssetTypes.DriverUsers
                        };
                    } else {
                        this.assetSelected = {
                            id: this.widget.groupID,
                            type: DashboardAssetTypes.DriverGroups
                        };
                    }
                    break;
                }
                case AssetTypes.Vehicle: {
                    if (this.widget.vehicleTypeID) {
                        this.assetSelected = {
                            id: this.widget.vehicleTypeID,
                            type: DashboardAssetTypes.VehicleTypes
                        };
                    } else {
                        this.assetSelected = {
                            id: this.widget.groupID,
                            type: DashboardAssetTypes.VehicleGroups
                        };
                    }
                    break;
                }
            }
            // console.log('widget:', this.widget);
            // console.log('assetSelected', this.assetSelected);
        }
    }




    // =========================
    // DATA 
    // =========================
    
    private create() {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.widgetsService
            .create(this.widget, this.widgetType, this.roleID)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 201) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.done();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
        },
        (err) => {
            this.loading = false;
            this.snackBar.open('form.errors.unknown', 'form.actions.ok');
        });
    }

    private update() {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.widgetsService
            .update(this.widget, this.widgetType, this.roleID)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 200) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.done();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
        },
        (err) => {
            this.loading = false;
            if (err.status === 404) {
                this.snackBar.open('form.errors.not_found', 'form.actions.close');
            } else {
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            }
        });
    }

    private get(id: number): void {
        this.loading = true;
        this.unsubscriber.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.widgetsService
            .getWidget(id, this.roleID)
            .subscribe((widget) => {                                                                
                this.loading = false;
                this.widget = widget;
                this.setAssetSelection();
               
                this.previewWidget();
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
            });
    }
}
