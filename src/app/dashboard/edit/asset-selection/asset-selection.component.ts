// FRAMEWORK
import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges
} from '@angular/core';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UserOptionsService } from './../../../shared/user-options/user-options.service';
import { WidgetsService } from './../../dashboard.service';
// TYPES
import { KeyName } from '../../../common/key-name';
import { DashboardSelectionItem, DashboardAssetTypes } from './asset-selection.types';
import { SearchType } from './../../../shared/search-mechanism/search-mechanism-types';
import { AssetGroupTypes, AssetGroupItem } from './../../../admin/asset-groups/asset-groups.types';
import { User } from './../../../auth/user';

@Component({
    selector: 'dashboard-asset-selection',
    templateUrl: 'asset-selection.html',
    styleUrls: ['asset-selection.less']
})

export class DashboardAssetSelectionComponent implements OnChanges {
    @Input() public disabled?: boolean;
    @Input() public showNoneOption?: boolean;
    @Input() public options: DashboardAssetTypes[];
    @Input() public modelData?: DashboardSelectionItem = {} as DashboardSelectionItem;
    @Output() public modelDataChange: EventEmitter<DashboardSelectionItem> =
        new EventEmitter<DashboardSelectionItem>();

    public assetOptions: KeyName[];
    public assetTypes = DashboardAssetTypes;
    public groupTypes = AssetGroupTypes;
    public assetSearchTypes: any = SearchType;
    public driverName: string;
    public loading: boolean;

    // current user
    private user: User;

    constructor(private translate: CustomTranslateService,
                private userOptions: UserOptionsService,
                private widgetsService: WidgetsService) {

        this.user = userOptions.getUser();

        this.assetOptions = this.getAssetOptions();
    }

    // =====================
    //  LIFECYCLE
    // =====================

    public ngOnChanges(changes: SimpleChanges) {
        // if this is a driver selection
        const modelDataChange = changes['modelData'];
        if (modelDataChange &&
            modelDataChange.currentValue &&
            modelDataChange.currentValue.id > 0 &&
            modelDataChange.currentValue.type === DashboardAssetTypes.DriverUsers) {
           this.getDriverName(modelDataChange.currentValue.id);
        }

        const changeType = changes['options'];
        if (!changeType) {
            return;
        }

        const optionsEnabled = changeType.currentValue as DashboardAssetTypes[];


         // check whether or not me option should be available
        const meOption = {
            id: DashboardAssetTypes.Me,
            term: 'assets.me',
            name: 'Me',
            enabled: false
        };

        const meInRequested = optionsEnabled.findIndex((x) => x ===  DashboardAssetTypes.Me);
        const meInExisting = this.assetOptions.findIndex((x) => x.id ===  DashboardAssetTypes.Me);
        if (meInRequested > -1 && meInExisting === -1) {
            this.assetOptions.unshift(meOption);
        } else if (meInExisting > -1) {
            this.assetOptions.slice(meInExisting, 1);
        }

        this.assetOptions.forEach((o) => {
            o.enabled = false;
        });
        if (changeType.currentValue) {
            this.assetOptions.forEach((o) => {
                if (optionsEnabled.find((e) => e === o.id) !== undefined) {
                    o.enabled = true;
                }
            });
        }
    }

    // =====================
    //  CALLBACKS
    // =====================

    public onSelectAssetType(type: DashboardAssetTypes) {
        this.modelData.id = null;
        this.modelDataChange.emit(this.modelData);
    }

    public onChangeGroup(group: AssetGroupItem) {
        this.modelData.id = group ? group.id : null;
        this.modelDataChange.emit(this.modelData);
    }

    public onDriverUserSelect(driver?: KeyName) {
        this.modelData.id = driver ? driver.id : null;
        this.modelDataChange.emit(this.modelData);
    }

    public onVehicleTypeSelect(vehicleType?: KeyName) {
        this.modelData.id = vehicleType ? vehicleType.id : null;
        this.modelDataChange.emit(this.modelData);    }

    public onDriverGroupDimensionSelect() {
        this.modelDataChange.emit(this.modelData);
    }


    // =====================
    //  PRIVATE
    // =====================

    private getAssetOptions(): KeyName[] {
        const options = [
            {
                id: DashboardAssetTypes.VehicleGroups,
                term: 'assets.groups.vehicle',
                name: 'Vehicle groups',
                enabled: false
            },
            {
                id: DashboardAssetTypes.DriverGroups,
                term: 'assets.groups.driver',
                name: 'Driver groups',
                enabled: true
            },
            {
                id: DashboardAssetTypes.VehicleTypes,
                term: 'assets.vehicle_types',
                name: 'Vehicle types',
                enabled: false
            },
            {
                id: DashboardAssetTypes.DriverGroupDimensions,
                term: 'assets.groups.driver.dimensions',
                name: 'Driver group dimensions',
                enabled: false
            },
            {
                id: DashboardAssetTypes.DriverUsers,
                term: 'assets.drivers',
                name: 'Drivers',
                enabled: false
            },
        ];

        return options;
    }

    private getDriverName(assetID: number): void {
        this.widgetsService
            .getDriver(assetID)
            .subscribe((driverName: string) => {
                this.driverName = driverName;
            });
    }
}
