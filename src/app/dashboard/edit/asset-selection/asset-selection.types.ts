export enum DashboardAssetTypes {
    None = -1,
    Me = 1,
    VehicleGroups = 2,
    DriverGroups = 3,
    VehicleTypes = 4,
    DriverGroupDimensions = 5,
    DriverUsers = 6
}

export interface DashboardSelectionItem {
    type: DashboardAssetTypes;
    id?: number;
}
