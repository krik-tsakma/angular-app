import { ViewGranularityOptions } from '../dashboard-types';
import { AssetTypes } from '../../common/entities/entity-types';
import { ChartTypes } from './chart-widget/chart-types';
import { SortDirection } from './ranking-widget/ranking-types';
import { SummaryTypes } from './summary-widget/summary-types';
import { PeriodTypes } from '../../common/period-selector/period-selector-types';
import { DistributionTypes } from '../../common/distribution-types/distribution-types';

export class BaseWidget {
    public title: string;
    public id: number;
    public cols: number;
    public order: number;
    public rows: number;
    public allowUserToRemove: boolean;

    constructor() {
        this.title = null;
        this.cols = 1;
        this.order = 1;
        this.rows = 1;
        this.allowUserToRemove = true;
    }
}

/** These are the keys used to define the widget types. */
export enum WidgetTypes {
    Chart = 'ChartWidget' as any,
    Summary = 'SummaryWidget' as any,
    Ranking = 'RankingWidget' as any,
    Events = 'EventsWidget' as any
}
/* Maps a widget with its acl value  */
export class WidgetTypesAclMapping {
    public static aclMappingTable: { [key: string]: number } = {
        [WidgetTypes.Chart]:  0x01,
        [WidgetTypes.Summary]: 0x02,
        [WidgetTypes.Ranking]: 0x04,
        [WidgetTypes.Events]: 0x08,
    };

    public static GetForWidgetType(type: WidgetTypes): number {
        return WidgetTypesAclMapping.aclMappingTable[type];
    }
}


export class ChartWidget extends BaseWidget {
    public type: ChartTypes;
    public viewGranularity: ViewGranularityOptions;
    public periodType: PeriodTypes;
    public assetType: AssetTypes;
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public vehicleTypeID?: number;
    public energyTypeID?: number;
    public scoreID?: number;
    public deviceTypeGroupID?: number;
    public distributeBy: DistributionTypes;
    public targetValue?: number;
    public referenceID?: number;

    constructor(title: string, 
                cols: number,
                order: number,
                assetType: AssetTypes, 
                chartType: ChartTypes, 
                periodType: PeriodTypes, 
                viewGranularity: ViewGranularityOptions, 
                energyTypeID?: number,
                vehicleTypeID?: number,
                assetID?: number,
                groupID?: number,
                groupDimensionID?: number,
                scoreID?: number,
                deviceTypeGroupID?: number,
                distributeBy?: DistributionTypes,
                targetValue?: number,
                referenceID?: number) {
        super();
        this.title = title;
        this.cols = cols;
        this.order = order;
        this.assetType = assetType;
        this.periodType = periodType;
        this.type = chartType;
        this.viewGranularity = viewGranularity;
        this.energyTypeID = energyTypeID;
        this.vehicleTypeID = vehicleTypeID;
        this.assetID = assetID;
        this.groupID = groupID;
        this.groupDimensionID = groupDimensionID;
        this.scoreID = scoreID;
        this.deviceTypeGroupID = deviceTypeGroupID;
        this.distributeBy = distributeBy ? distributeBy : DistributionTypes.NumOfTrips;
        this.targetValue = targetValue;
        this.referenceID = referenceID;
    }
}

export enum SummaryWidgetValueTypes {
    Volume = 0,
    Percentage = 1,
}

export class SummaryWidget extends BaseWidget {
    public type: SummaryTypes;
    public valueType: SummaryWidgetValueTypes;
    public groupID?: number;
    public assetID?: number;
    public vehicleTypeID?: number;
    public groupDimensionID?: number;
    public scoreID?: number;
    public periodType: PeriodTypes;
    public assetType: AssetTypes;
    public energyTypeID?: number;
    public targetValue?: number;
    public scoreThreshold?: number;
    public distributeBy?: number;

    constructor(title: string, 
                cols: number,
                order: number,
                assetType: AssetTypes, 
                summaryType: SummaryTypes, 
                periodType: PeriodTypes, 
                summaryValueType: SummaryWidgetValueTypes, 
                energyTypeID?: number, 
                assetID?: number,
                vehicleTypeID?: number,
                target?: number,
                scoreThreshold?: number,
                scoreID?: number) {
        super();
        this.title = title;
        this.cols = cols;
        this.order = order;
        this.assetType = assetType;
        this.periodType = periodType;
        this.type = summaryType;
        this.energyTypeID = energyTypeID;
        this.assetID = assetID;
        this.vehicleTypeID = vehicleTypeID;
        this.targetValue = target;
        this.scoreThreshold = scoreThreshold;
        this.scoreID = scoreID;
        this.valueType = summaryValueType;
    }
}


export class RankingWidget extends BaseWidget {
    public scoreID: number;
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public periodType: PeriodTypes;
    public sortDirection: SortDirection;
    public numberOfRows: number;
    public assetType: AssetTypes;
    
    constructor(title: string, 
                cols: number,
                order: number,
                scoreID: number,
                periodType: PeriodTypes,
                numberOfRows: number,
                assetID?: number,
                groupDimensionID?: number) {
        super();
        this.title = title;
        this.cols = cols;
        this.order = order;
        this.periodType = periodType;
        this.scoreID = scoreID;
        this.sortDirection = SortDirection.ASC;
        this.numberOfRows = numberOfRows;
        this.assetType = AssetTypes.HR;
        this.assetID = assetID;
        this.groupDimensionID = groupDimensionID;
    }
}



export class WidgetLayout {
    public summary: WidgetLayoutPositions;
    public charts: WidgetLayoutPositions;

    constructor() {
        this.summary = WidgetLayoutPositions.left;
        this.charts = WidgetLayoutPositions.top;
    }

    public set(layout: string): void {
        const zones = layout.split('$');
        zones.forEach((z) => {
            const items = z.split('#');
            if (items[0] === '1c') {
                this.summary = items[1] === '0' ? WidgetLayoutPositions.left : WidgetLayoutPositions.right;
            } else if (items[0] === '2c1r') {
                this.charts = items[1] === '0' ? WidgetLayoutPositions.top : WidgetLayoutPositions.bottom;
            }
        });
    }

     public get(): string {
        const oneColValue = this.summary ===  WidgetLayoutPositions.left ? 0 : 1;
        const twoColFirstRowValue = this.charts ===  WidgetLayoutPositions.top ? 0 : 1;
        return '1c#' + oneColValue + '$2c1r#' + twoColFirstRowValue;
    }
}

export enum WidgetLayoutPositions {
    top,
    bottom,
    left,
    right
}

// CRUD

export class WidgetZoneUpsertResult {
    public code: WidgetZoneUpsertResultCodeOptions;
}

export enum WidgetZoneUpsertResultCodeOptions {
    Ok = 0,
    UknownError = 1
}

// ViewModel
export class WidgetsResults {
    public items: Widget[];
    public zoneLayout?: string;
}

export class Widget {
    public instance: BaseWidget;
    public type: WidgetTypes;
}

