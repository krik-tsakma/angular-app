// FRAMEWORK
import {
    Component, EventEmitter, OnChanges, SimpleChanges,
    OnDestroy, Input, Output, ViewEncapsulation, ElementRef
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

// COMPONENTS
import { DeleteWidgetBaseComponent } from '../../delete-widget-base.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { SummaryService } from './summary-widget.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { WidgetsService } from '../../dashboard.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { EnergyTypesService } from '../../../common/energy-types/energy-types.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { Config } from '../../../config/config';

// TYPES
import { 
    SummaryRequest, SummaryResult, SummaryTypes, 
    DriveEnergyScoreRequest, ScoreDrivenRequest 
} from './summary-types';
import { SummaryWidgetValueTypes, BaseWidget, WidgetTypes, SummaryWidget } from '../widget-types';
import { Score, ScoreFormatter } from '../../../common/score';
import { AssetTypes } from '../../../common/entities/entity-types';
import { EnergyUnitOptions } from '../../../common/energy-types/energy-types';
import { LocalOptions } from '../../../shared/user-options/user-options-types';

@Component({
    selector: 'summary-widget',
    templateUrl: './summary-widget.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./summary-widget.less']
})

export class SummaryWidgetComponent extends DeleteWidgetBaseComponent implements OnChanges, OnDestroy {
    @Input() public editing: boolean;
    @Input() public widget: SummaryWidget = {} as SummaryWidget;
    @Output() public onDeleted = new EventEmitter<number>();
    public summaryTypes: any = SummaryTypes;
    public summaryWidgetValueTypes: any = SummaryWidgetValueTypes;
    public scoreFormatter = ScoreFormatter;
    public result: SummaryResult;
    public scoreResult: Score;
    public localOptions: LocalOptions;
    
    public loading: boolean;
    public message: string;
    public metricLabel: string;    
    public deviationPercValue: number;
    public headerStyle: string;
    public headerTooltip: string;

    private serviceSubscriber: any;
    private deleteWidgetSubscriber: any;

    constructor(
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,   
        protected confirmDialogService: ConfirmDialogService,
        protected widgetService: WidgetsService,
        public elementRef: ElementRef,
        public iconRegistry: MatIconRegistry,
        public sanitizer: DomSanitizer,
        public userOptions: UserOptionsService,
        private configService: Config,
        private summaryService: SummaryService,
        private translate: CustomTranslateService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private energyTypesService: EnergyTypesService) {
            super(unsubscriber, widgetService, confirmDialogService , snackBar);
            iconRegistry
                    .addSvgIcon('percentage_icon',
                        sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/widgets/percentage_icon.svg'))
                    .addSvgIcon('target_icon',
                        sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/widgets/target_icon.svg'));

            this.metricLabel = '';
            this.localOptions = this.userOptions.localOptions;
    }


    // =========================
    // ANGULAR LIFECYCLE
    // =========================

    public ngOnChanges(changes: SimpleChanges) {
        const widgetChange = changes['widget'];
        if (widgetChange) {
            const current = widgetChange.currentValue as SummaryWidget;
            const previous = widgetChange.previousValue as SummaryWidget;
            // it makes sense to fetch data for all elements change apart from title
            if (current && previous && previous.title && current.title.toString() !== previous.title.toString()) {
                return;
            }
            this.get();
        }
    }
    

    // Clean subscribed observables to avoid memory leak
    public ngOnDestroy() {
        super.ngOnDestroy();
        this.unsubscriber.removeSubscription([
            this.serviceSubscriber,
            this.deleteWidgetSubscriber
        ]);      
    }

    public edit(id): void {
        this.router.navigate(['/dashboard/edit', WidgetTypes.Summary, id], { skipLocationChange: !this.configService.get('router') });
    }

    // this option is available only to users on dashboard
    public openDialog(widget: BaseWidget) {
        this.openDeleteDialog(widget.id)
            .then((deleted: boolean) => {
                if (deleted === true) {
                    this.onDeleted.emit(widget.id);
                }
            });
    }

    private get(): void {
        if (this.widget === undefined || Object.keys(this.widget).length === 0) {
            return;
        }
        this.message = '';
        this.loading = true;
      
        this.unsubscriber.removeSubscription(this.serviceSubscriber);
        if (this.widget.assetID === -1) {
            this.widget.assetID = this.userOptions.getUser().driverId;
            this.widget.assetType = AssetTypes.HR;
        }
        const type = Number(this.widget.type);
        switch (type) { 
            case SummaryTypes.Score: { 
                const params = new DriveEnergyScoreRequest(this.widget.periodType, 
                                                        this.widget.assetType,
                                                        this.widget.scoreID,
                                                        this.widget.groupID, 
                                                        this.widget.assetID,
                                                        this.widget.groupDimensionID,
                                                        this.widget.vehicleTypeID);
                                        
                this.serviceSubscriber = this.summaryService
                    .getDriveEnergyScore(params)
                    .subscribe(
                        (result) => {
                            this.loading = false;
                        
                            if (result == null || result.score == null || result.score.normalized == null) {
                                this.message = 'dashboard.edit_widget.score_not_applied';
                            } else {
                            this.scoreResult = result.score;
                            }
                        },
                        (err) => {
                            this.loading = false;
                            this.message = 'data.error';
                        }
                    );
                break; 
            } 
            case SummaryTypes.ScoreDriven: { 
                if (!this.widget.scoreThreshold) {
                    this.widget.scoreThreshold = 0;
                }
                const params = new ScoreDrivenRequest(this.widget.periodType,
                                                    this.widget.assetType,
                                                    this.widget.scoreID,
                                                    this.widget.groupID, 
                                                    this.widget.assetID,
                                                    this.widget.groupDimensionID,
                                                    this.widget.vehicleTypeID,
                                                    this.widget.distributeBy,
                                                    this.widget.scoreThreshold);

                this.serviceSubscriber = this.summaryService
                    .getScoreDriven(params)
                    .subscribe(
                        (result) => {
                            this.loading = false;
                            if (result) {
                                this.result = result as SummaryResult;
                                this.result.secondaryValue =  Math.round(result.secondaryValue * 100) / 100;
                                this.setHeadeStyle();
                            }
                        },
                        (err) => {
                            this.loading = false;
                            this.message = 'data.error';
                        }
                    );
                break; 
            } 
            default: { 
                const params = new SummaryRequest(this.widget.type,
                                                this.widget.assetType,
                                                this.widget.periodType,
                                                this.widget.groupID,
                                                this.widget.assetID,
                                                this.widget.groupDimensionID,
                                                this.widget.vehicleTypeID,
                                                this.widget.energyTypeID);

                this.serviceSubscriber = this.summaryService
                    .getSummary(params)
                    .subscribe(
                        (result) => {
                            this.loading = false;
                            this.result = result;

                            this.result.secondaryValue =  Math.round(result.secondaryValue * 100) / 100;
                            this.result.primaryValue = result.primaryValue;

                            this.result.type = this.widget.type;
                              
                            this.getLabel(this.widget.type, this.widget.energyTypeID).then((res: string) => {
                                this.metricLabel = res;
                            });
                            this.setHeadeStyle();
                        },
                        (err) => {
                            this.loading = false;
                            this.message = 'data.error';
                        }
                    );
                break; 
            } 
        }
    }  

    private getLabel(type: number, energyTypeID?: any): Promise<string>  {
        return new Promise((resolve, reject) => {
            const nType = Number(type);
            switch (nType) {
                case SummaryTypes.TotalDistance: 
                    resolve( 1000 + this.userOptions.metrics.distance); 
                    break;
                case SummaryTypes.CO2Emission: 
                    resolve(1000 + this.userOptions.metrics.weight); 
                    break;
                case SummaryTypes.AccountedEnergy: 
                case SummaryTypes.UnaccountedEnergy: 
                    resolve('%'); 
                    break;
                case SummaryTypes.EnergyEfficiency:
                    this.getEnergyType(energyTypeID).then((unit) => {
                        resolve(this.userOptions.getCalculatedUnit('energyGJ', 100, unit));
                    });
                    break;
                default: 
                    this.getEnergyType(energyTypeID).then((unit) => {
                        resolve(this.userOptions.getCalculatedUnit('energyGJ', null, unit));
                    });
                    break;
            }
        });
    }

    private getEnergyType(energyTypeID?: number): Promise<EnergyUnitOptions>  {
        return new Promise((resolve, reject) => {
            const energyUnit = null;
            if (energyTypeID) {
                this.energyTypesService
                    .getEnergyUnit(energyTypeID)
                    .subscribe((res: EnergyUnitOptions) => {
                        resolve(res);
                    });
            } else {
                resolve(null);
            }
        });
    }

    private setHeadeStyle(): void {
        this.headerStyle = '';
        this.headerTooltip = '';
        if (!this.widget.targetValue) {
            return;
        }
        const type = Number(this.widget.type);

        // positive meaning
        if (type === SummaryTypes.ScoreDriven) {
            const diffPerc = this.calcTargetDeviation(type, this.result.secondaryValue);
            this.deviationPercValue = diffPerc * 100;
            if (this.deviationPercValue >= -5) {
                this.headerStyle = 'good-on-target';
                this.headerTooltip = this.translate.instant('score_build_up.good_on_target');
            } else if (this.deviationPercValue > -20) {
                this.headerStyle = 'high-on-target';
                this.headerTooltip = this.translate.instant('score_build_up.high_on_target');
            } else {
                this.headerStyle = 'low-on-target';
                this.headerTooltip = this.translate.instant('score_build_up.low_on_target');
            }
        } else {
            // negative meaning
            const diffPerc = this.calcTargetDeviation(type, this.result.primaryValue);
            //  console.log("primaryValue: ", this.result.primaryValue, 
            // "targetValue: ", this.widget.targetValue, 
            // " goalValue: ", this.deviationPercValue)
            this.deviationPercValue = diffPerc * 100;
        
            if (this.deviationPercValue <= 5) {
                this.headerStyle = 'good-on-target';
                this.headerTooltip = this.translate.instant('score_build_up.good_on_target');
            } else if (this.deviationPercValue <= 20) {
                this.headerStyle = 'high-on-target';
                this.headerTooltip = this.translate.instant('score_build_up.high_on_target');
            } else {
                this.headerStyle = 'low-on-target';
                this.headerTooltip = this.translate.instant('score_build_up.low_on_target');
            }
        }
    }

    private calcTargetDeviation(type: number, rawValue: number) {
        const diff = (rawValue - this.widget.targetValue);
        let comp = this.widget.targetValue;
    
  
        if (type === SummaryTypes.AccountedEnergy 
            || type === SummaryTypes.UnaccountedEnergy 
            || type === SummaryTypes.ScoreDriven) {
            comp = 100;
        }
       // console.log("diff: ", diff, " comp: ", comp)
        const diffPerc = diff / comp;

        return diffPerc;
    }
}
