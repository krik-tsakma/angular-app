import { AssetTypes } from '../../../common/entities/entity-types';
import { PeriodTypes } from '../../../common/period-selector/period-selector-types';
import { Score, ScoreFormatter } from '../../../common/score';
import { DistributionTypes } from '../../../common/distribution-types/distribution-types';

export enum SummaryTypes {
    TotalDistance = 0,
    EnergyUsed = 1,
    EnergySavings = 2,
    EnergySavingsPotential = 3,
    CO2Emission = 4,
    AccountedEnergy = 5,
    UnaccountedEnergy = 6,
    TotalDuration = 7,
    EnergyEfficiency = 8,
    Score = 100,
    ScoreDriven = 101
}

export class SummaryRequest {
    public type: SummaryTypes;
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public vehicleTypeID?: number;
    public periodType: PeriodTypes;
    public assetType: AssetTypes;
    public energyTypeID?: number;

    constructor(type: SummaryTypes, 
                assetType: AssetTypes, 
                periodType: PeriodTypes, 
                groupID?: number, 
                assetID?: number, 
                groupDimensionID?: number,
                vehicleTypeID?: number,
                energyTypeID?: number) {
        this.type = type;
        this.assetType = assetType;
        this.periodType = periodType;
        this.assetID = assetID;
        this.groupID = groupID;
        this.groupDimensionID = groupDimensionID;
        this.vehicleTypeID = vehicleTypeID;
        this.energyTypeID = energyTypeID;
    }
}

export class SummaryResult {
    public primaryValue: number;
    public secondaryValue?: number;
    public type: number;
    public score?: Score;
}

// DRIVE-ENERGY SCORE
export class DriveEnergyScoreRequest {
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public vehicleTypeID?: number;
    public periodType: PeriodTypes;
    public scoreID: number;
    public assetType: AssetTypes;

    constructor(periodType: PeriodTypes, 
                assetType: AssetTypes, 
                scoreID: number,
                groupID?: number, 
                assetID?: number,
                groupDimensionID?: number,
                vehicleTypeID?: number) {
        this.periodType = periodType;
        this.assetType = assetType;
        this.scoreID = scoreID;
        this.groupID = groupID;
        this.assetID = assetID;
        this.groupDimensionID = groupDimensionID;
        this.vehicleTypeID = vehicleTypeID;
    }
}

export class DriveEnergyScoreResult {
    public score: Score;
}

// DRIVE-ENERGY SCORE
export class ScoreDrivenRequest {
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public vehicleTypeID?: number;
    public periodType: PeriodTypes;
    public scoreID: number;
    public assetType: AssetTypes;
    public distributeBy: DistributionTypes;
    public scoreThreshold: number;

    constructor(periodType: PeriodTypes, 
                assetType: AssetTypes, 
                scoreID: number,
                groupID?: number, 
                assetID?: number,
                groupDimensionID?: number,
                vehicleTypeID?: number,
                distributeBy?: DistributionTypes,
                scoreThreshold?: number) {
        this.periodType = periodType;
        this.assetType = assetType;
        this.scoreID = scoreID;
        this.groupID = groupID;
        this.assetID = assetID;
        this.groupDimensionID = groupDimensionID;
        this.distributeBy = distributeBy ? distributeBy : DistributionTypes.NumOfTrips;
        this.scoreThreshold = scoreThreshold ? scoreThreshold : 0;
    }
}

export class ScoreDrivenResult {
    public primaryValue: number;
    public secondaryValue?: number;
}
