// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from '../../../config/config';
import { AuthHttp , ObjectURLSearchParams } from '../../../auth/authHttp';

// TYPES
import {
    SummaryRequest, SummaryResult, DriveEnergyScoreRequest,
    DriveEnergyScoreResult, ScoreDrivenRequest, ScoreDrivenResult
} from './summary-types';

@Injectable()
export class SummaryService {
    public baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = config.get('apiUrl') + '/api/Summary';
        }

    public getSummary(req: SummaryRequest): Observable<SummaryResult> {
        
        const params = new ObjectURLSearchParams(req);

        return this.authHttp
            .get(this.baseURL, { params });
    }

    public getDriveEnergyScore(req: DriveEnergyScoreRequest): Observable<DriveEnergyScoreResult> {

        const params = new ObjectURLSearchParams(req);

        return this.authHttp
            .get(this.config.get('apiUrl') + '/api/DriveEnergyScore', { params });
    }

    public getScoreDriven(req: ScoreDrivenRequest): Observable<ScoreDrivenResult> {

        const params = new ObjectURLSearchParams(req);

        return this.authHttp
            .get(this.config.get('apiUrl') + '/api/ScoreDriven', { params });
    }
}

