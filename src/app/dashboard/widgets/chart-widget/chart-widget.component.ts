// FRAMEWORK
import {
    Component, EventEmitter, OnChanges, SimpleChanges,
    OnDestroy, Input, Output, ViewEncapsulation, ElementRef
} from '@angular/core';
import { Router } from '@angular/router';


// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { ChartsService } from './chart-widget.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { WidgetsService } from '../../dashboard.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { EnergyTypesService } from '../../../common/energy-types/energy-types.service';
import { Config } from '../../../config/config';

// COMPONENTS
import { DeleteWidgetBaseComponent } from '../../delete-widget-base.component';

// TYPES
import { AmChartTypes } from '../../../amcharts/amchart-types';
import { AssetTypes } from '../../../common/entities/entity-types';
import { LocalOptions, Metrics } from '../../../shared/user-options/user-options-types';
import { BaseWidget, ChartWidget, Widget, WidgetTypes } from '../widget-types';
import { AccelerometerEventsResult, AccelerometerEventsChart } from '../../../common/accelerometer-events';
import { EnergyUnitOptions } from '../../../common/energy-types/energy-types';
import {
    ChartTypes, EnergySavingsRequest, EnergySavingsResult,
    ScoreBuildUpRequest, AccelerometerEventsRequest,
    ScoreDistributionRequest, ScoreDistributionResult, ScoreDistributionChartItem,
    ScoreEvolutionResult, ScoreEvolutionRequest, ScoreEvolutionChartItem, ScoreBuildUpResult
 } from './chart-types';
import {
    ScoreColorMap, Score, ScoreFormatter, ScoreBuildUpChartItem
} from '../../../common/score';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'chart-widget',
    templateUrl: './chart-widget.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./chart-widget.less']
})

export class ChartWidgetComponent extends DeleteWidgetBaseComponent implements OnChanges, OnDestroy {
    @Input() public widget: ChartWidget = {} as ChartWidget;
    @Input() public editing: boolean;
    @Output() public onDeleted = new EventEmitter<number>();

    public metrics: Metrics;
    public localOptions: LocalOptions;
    public chartOptions: any;
    public message: string;
    public loading: boolean;
    public widgetTypes: any = ChartTypes;
    public chartType = AmChartTypes.Serial;
    public score: Score;
    public scoreFormatter: any = ScoreFormatter;

    private chartTitle: string;
    private savingsTitle: string;
    private savingsPotentialTitle: string;
    private baseLineTitle: string;
    private targetTitle: string;
    private actualTitle: string;
    private targetDeviationTitle: string;
    private scoreBuildUpTranslations: any;
    private referenceAssetsTitle: string;

    private serviceSubscriber: any;
    private deleteWidgetSubscriber: any;

    constructor(
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected confirmDialogService: ConfirmDialogService,
        protected widgetService: WidgetsService,
        public elementRef: ElementRef,
        private userOptions: UserOptionsService,
        private chartService: ChartsService,
        private translate: CustomTranslateService,
        private router: Router,
        private energyTypesService: EnergyTypesService,
        private configService: Config) {
            super(unsubscriber, widgetService, confirmDialogService , snackBar);

            this.localOptions = this.userOptions.localOptions;
            this.metrics = this.userOptions.metrics;

            this.translate.get(['dashboard.chart_widget.actual',
                            'dashboard.chart_widget.baseline',
                            'dashboard.chart_widget.energy_efficiency',
                            'dashboard.chart_widget.target_deviation',
                            'dashboard.chart_widget.savings',
                            'dashboard.chart_widget.savings_potential',
                            'dashboard.chart_widget.target',
                            'score_build_up.good_on_target',
                            'score_build_up.high_on_target',
                            'score_build_up.low_on_target',
                            'dashboard.edit_widget.reference_assets']).subscribe((t: any) => {
                this.actualTitle = t['dashboard.chart_widget.actual'];
                this.baseLineTitle = t['dashboard.chart_widget.baseline'];
                this.chartTitle = t['dashboard.chart_widget.energy_efficiency'];
                this.targetDeviationTitle = t['dashboard.chart_widget.target_deviation'];
                this.savingsTitle = t['dashboard.chart_widget.savings'];
                this.savingsPotentialTitle = t['dashboard.chart_widget.savings_potential'];
                this.targetTitle = t['dashboard.chart_widget.target'];
                this.scoreBuildUpTranslations = {
                    goodOnTarget: t['score_build_up.good_on_target'],
                    highOnTarget: t['score_build_up.high_on_target'],
                    lowOnTarget: t['score_build_up.low_on_target']
                };
                this.referenceAssetsTitle = t['dashboard.edit_widget.reference_assets'];
            });
    }

     // =========================
    // ANGULAR LIFECYCLE
    // =========================

    public ngOnChanges(changes: SimpleChanges) {
        const widgetChange = changes['widget'];
        if (widgetChange) {
            const current = widgetChange.currentValue as ChartWidget;
            const previous = widgetChange.previousValue as ChartWidget;
            // it makes sense to fetch data for all elements change apart from title
            if (current && previous && previous.title && current.title.toString() !== previous.title.toString()) {
                return;
            }
            this.get();
        }
    }


    // Clean subscribed observables to avoid memory leak
    public ngOnDestroy() {
        super.ngOnDestroy();
        this.unsubscriber.removeSubscription([
            this.serviceSubscriber,
            this.deleteWidgetSubscriber
        ]);
    }

    // Edit method that routes you to the edit page
    public edit(id): void {
        this.router.navigate(['/dashboard/edit', WidgetTypes.Chart, id], { skipLocationChange: !this.configService.get('router') });
    }


    public openDialog(widget: BaseWidget) {
        this.openDeleteDialog(widget.id)
            .then((deleted: boolean) => {
                if (deleted === true) {
                    this.onDeleted.emit(widget.id);
                }
            });
    }

    // Get the chart widget
    private get(): void {
        if (this.widget === undefined || Object.keys(this.widget).length === 0) {
            return;
        }
        this.message = '';
        this.loading = true;

        if (this.widget.assetID === -1) {
            this.widget.assetID = this.userOptions.getUser().driverId;
            this.widget.assetType = AssetTypes.HR;
        }

        this.unsubscriber.removeSubscription(this.serviceSubscriber);

        switch (this.widget.type) {
            case ChartTypes.EnergySavings: {
                const params = new EnergySavingsRequest(this.widget.assetType,
                                                        this.widget.periodType,
                                                        this.widget.viewGranularity,
                                                        this.widget.groupID,
                                                        this.widget.assetID,
                                                        this.widget.groupDimensionID,
                                                        this.widget.vehicleTypeID,
                                                        this.widget.energyTypeID);
                this.serviceSubscriber = this.chartService
                    .getEnergySavings(params)
                    .subscribe((result) => {
                            this.loading = false;
                            if (result && result.items && result.items.length > 0) {
                                this.getEnergySavingsTitle(this.widget.energyTypeID)
                                    .then((chartTitle) => {
                                        this.chartOptions = this.makeEnergySavingsChart(result,
                                            this.localOptions,
                                            chartTitle);
                                    });
                            } else {
                                this.message = 'data.empty_records';
                            }
                        },
                        (err) => {
                            this.loading = false;
                            this.message = 'data.error';
                        }
                    );
                break;
            }
            case ChartTypes.ScoreEvolution: {
                const params = new ScoreEvolutionRequest(this.widget.assetType,
                                                        this.widget.periodType,
                                                        this.widget.viewGranularity,
                                                        this.widget.scoreID,
                                                        this.widget.groupID,
                                                        this.widget.assetID,
                                                        this.widget.groupDimensionID,
                                                        this.widget.vehicleTypeID,
                                                        this.widget.referenceID);
                this.serviceSubscriber = this.chartService
                    .getScoreEvolution(params)
                    .subscribe((result: ScoreEvolutionResult) => {
                            this.loading = false;
                            if (result && result.items && result.items.length > 0) {
                                const chartTitle = this.targetDeviationTitle + ' %';
                                this.chartOptions = this.makeScoreEvolutionChart(result,
                                                                    this.referenceAssetsSelected(),
                                                                    this.localOptions,
                                                                    this.widget.scoreID, chartTitle);
                            } else {
                                this.message = 'data.empty_records';
                            }
                        },
                        (err) => {
                            this.loading = false;
                            this.message = 'data.error';
                        }
                    );
                break;
            }
            case ChartTypes.ScoreBuildUp: {
                const scoreID = Number(this.widget.scoreID);
                const devGroupID = Number(this.widget.deviceTypeGroupID);
                if (!scoreID || scoreID === 0 || isNaN(devGroupID) || !devGroupID || devGroupID === 0)  {
                    this.loading = false;
                    this.message = 'data.error';
                    return;
                }
                const params = new ScoreBuildUpRequest(this.widget.assetType,
                                                        this.widget.periodType,
                                                        this.widget.scoreID,
                                                        this.widget.deviceTypeGroupID,
                                                        this.widget.assetID,
                                                        this.widget.groupID,
                                                        this.widget.groupDimensionID,
                                                        this.widget.vehicleTypeID,
                                                        this.widget.referenceID);

                this.serviceSubscriber = this.chartService
                    .getScoreBuildUp(params)
                    .subscribe((result: ScoreBuildUpResult) => {
                            this.loading = false;
                            if (result && result.items && result.items.length > 0) {
                                const htmlID = 'esc' + this.widget.id;
                                const sbc = new window['ScoreBuildUpGraph'](
                                    htmlID,
                                    this.localOptions.decimal_separator,
                                    this.localOptions.thousands_separator,
                                    this.targetDeviationTitle,
                                    this.scoreBuildUpTranslations.goodOnTarget,
                                    this.scoreBuildUpTranslations.highOnTarget,
                                    this.scoreBuildUpTranslations.lowOnTarget,
                                    this.referenceAssetsTitle);

                                this.chartOptions = sbc.get(result);
                                this.score = result.score;
                            } else {
                                this.message = 'data.empty_records';
                            }
                        },
                        (err) => {
                            this.loading = false;
                            this.message = 'data.error';
                        }
                    );
                break;
            }
            case ChartTypes.ScoreDistribution: {
                const params = new ScoreDistributionRequest(this.widget.assetType,
                                                        this.widget.periodType,
                                                        this.widget.scoreID,
                                                        this.widget.viewGranularity,
                                                        this.widget.distributeBy,
                                                        this.widget.groupID,
                                                        this.widget.assetID,
                                                        this.widget.groupDimensionID,
                                                        this.widget.vehicleTypeID,
                                                        this.widget.referenceID);

                this.serviceSubscriber = this.chartService
                    .getScoreDistribution(params)
                    .subscribe((result) => {
                            this.loading = false;

                            if (result && result.items && result.items.length > 0) {
                                const htmlID = 'esc' + this.widget.id;
                                this.chartOptions = this.makeScoreDistributionChart(htmlID,
                                        result,
                                        this.referenceAssetsSelected(),
                                        this.localOptions);
                            } else {
                                this.message = 'data.empty_records';
                            }
                        },
                        (err) => {
                            this.loading = false;
                            this.message = 'data.error';
                        }
                    );
                break;
            }
            case ChartTypes.AccelerometerEvents: {
                const params = new AccelerometerEventsRequest(this.widget.assetType,
                                                        this.widget.periodType,
                                                        this.widget.scoreID,
                                                        this.widget.assetID,
                                                        this.widget.groupID,
                                                        this.widget.groupDimensionID,
                                                        this.widget.vehicleTypeID,
                                                        this.widget.referenceID);

                this.serviceSubscriber = this.chartService
                    .getAccelerationEvents(params)
                    .subscribe((result) => {
                            this.loading = false;
                            if (result && result.items && result.items.length > 0) {
                                 this.chartOptions = this.makeAccelerationEventsChart(result,
                                                                     this.localOptions,
                                                                     this.metrics.distance);
                            } else {
                                this.message = 'data.empty_records';
                            }
                        },
                        (err) => {
                            this.loading = false;
                            this.message = 'data.error';
                        }
                    );
                break;
            }
            default:
                this.loading = false;
                break;
        }
    }

    private makeEnergySavingsChart(dataProvider: EnergySavingsResult, localOptions: LocalOptions, chartTitle: string): any {
        const items = dataProvider.items;
        return {
            dataProvider: items,
            type: 'serial',
            categoryField: 'timestamp',
            startDuration: 0,
            theme: 'default',
            categoryAxis: {
                // TODO(GT): duplicate code
                categoryFunction: (category: any, dataItem: any, categoryAxis: any) => {
                    if (localOptions) {
                        if (categoryAxis.chart.dataProvider.length === 12) {
                            return moment(dataItem.timestamp)
                                .locale(localOptions.moment_locale)
                                .format('MM/YYYY');
                        }
                        return moment(dataItem.timestamp)
                            .locale(localOptions.moment_locale)
                            .format(localOptions.short_date);
                    }
                    return ' ';
                },
                gridPosition: 'start',
                // small angle to labels on x axis to avoid label overlapping
                labelRotation: 15
            },
            numberFormatter: {
                decimalSeparator: localOptions.decimal_separator,
                thousandsSeparator: localOptions.thousands_separator
            },
            chartCursor: {
                enabled: true
            },
            trendLines: [],
            graphs: [
                // SAVINGS
                {
                    animationPlayed: true,
                    closeField: 'baseline',
                    color: '#FFFFFF',
                    fillAlphas: 0.85,
                    fillColors: '#4BBA42',
                    id: 'FuelSaving',
                    labelText: ' ',
                    labelFunction: (item) => {
                        const diff = item.dataContext.baseline - item.dataContext.actual;
                        const perc = (diff / item.dataContext.baseline) * 100;
                        return Math.round(perc * 10) / 10 + '%';
                    },
                    showAllValueLabels: true,
                    lineColor: '#4BBA42',
                    openField: 'actual',
                    title: this.savingsTitle,
                    type: 'column',
                    valueAxis: 'va1'
                },
                // SAVINGS POTENTIAL
                {
                    animationPlayed: true,
                    closeField: 'target',
                    fillAlphas: 0.85,
                    fillColors: '#FEBA00',
                    id: 'SavingsPotential',
                    labelText: ' ',
                    labelFunction: (item) => {
                        const diff = item.dataContext.actual - item.dataContext.target;
                        const perc = (diff / item.dataContext.actual) * 100 ;
                        if (perc > 0) {
                            return Math.round(perc * 10) / 10 + '%';
                        } else {
                            return '';
                        }
                    },
                    showAllValueLabels: true,
                    lineColor: '#FEBA00',
                    openField: 'actual',
                    title: this.savingsPotentialTitle,
                    ype: 'column',
                    valueAxis: 'va1'
                },
                // BASELINE
                {
                    animationPlayed: true,
                    id: 'Baseline',
                    includeInMinMax: false,
                    lineColor: '#E00001',
                    lineThickness: 3,
                    stackable: false,
                    title: this.baseLineTitle,
                    type: 'smoothedLine',
                    valueAxis: 'va1',
                    valueField: 'baseline'
                },
                // TARGET
                {
                    animationPlayed: true,
                    id: 'Target',
                    includeInMinMax: false,
                    lineColor: '#00A74E',
                    lineThickness: 3,
                    stackable: false,
                    title: this.targetTitle,
                    type: 'smoothedLine',
                    valueAxis: 'va1',
                    valueField: 'target'
                },
                // ACTUAL
                {
                    animationPlayed: true,
                    id: 'Actual',
                    includeInMinMax: false,
                    lineColor: '#000000',
                    lineThickness: 3,
                    title: this.actualTitle,
                    type: 'smoothedLine',
                    valueAxis: 'va1',
                    valueField: 'actual'
                }
            ],
            guides: [],
            valueAxes: [
                {
                    id: 'va1',
                    stackType: 'regular',
                    titleFontSize: 10,
                    title: chartTitle
                }
            ],
            allLabels: [],
            balloon: {
                animationDuration: 0,
                borderThickness: 10,
                fadeOutDuration: 0
            },
            legend: {
                useGraphSettings: true,
                position: 'bottom',
                align: 'center',
                switchable: false,
            },
            responsive: {
                enabled: true
            }
        };
    }


    private makeScoreEvolutionChart(
        dataProvider: ScoreEvolutionResult,
        referenceAssetsSelected: boolean,
        localOptions: LocalOptions,
        scoreID: number,
        chartTitle: string
    ): any {
        const colorMap = new ScoreColorMap();
        const finalItems: ScoreEvolutionChartItem[] = [];
        dataProvider.items.forEach((item) => {
            const finalItem: ScoreEvolutionChartItem = {
                timestamp: item.timestamp,
                rawScore: item.score.normalized,
                letterScore: item.score.letter,
                colorScore: colorMap.getForLetter(item.score.letter)
            } as ScoreEvolutionChartItem;
            if (dataProvider.referenceItems && dataProvider.referenceItems.length > 0) {
                const referenceItem = dataProvider.referenceItems.filter((x) => x.timestamp === item.timestamp);
                if (referenceItem && referenceItem.length > 0) {
                    finalItem.referenceRawScore = referenceItem[0].score.normalized;
                    finalItem.referenceColorScore = colorMap.getForLetter(referenceItem[0].score.letter);
                    finalItem.referenceLetterScore = referenceItem[0].score.letter;
                }
            }
            finalItems.push(finalItem);
        });
        const cfg = {
            dataProvider: finalItems,
            type: 'serial',
            categoryField: 'timestamp',
            startDuration: 0,
            theme: 'default',
            categoryAxis: {
                categoryFunction:
                // TODO(GT): duplicate code
                (category: any, dataItem: any, categoryAxis: any) => {
                    if (localOptions) {
                        if (categoryAxis.chart.dataProvider.length === 12) {
                            return moment(dataItem.timestamp)
                                .locale(localOptions.moment_locale)
                                .format('MM/YYYY');
                        }
                        return moment(dataItem.timestamp)
                            .locale(localOptions.moment_locale)
                            .format(localOptions.short_date);
                    }
                    return ' ';
                },
                gridPosition: 'start',
                // small angle to labels on x axis to avoid label overlapping
                labelRotation: 15
            },
            numberFormatter: {
                decimalSeparator: localOptions.decimal_separator,
                thousandsSeparator: localOptions.thousands_separator
            },
            trendLines: [],
            graphs: [
                {
                    balloonText: '[[value]]' + (scoreID !== 0 ? '%' : ''),
                    fillAlphas: 1,
                    id: 'RawScoreCol',
                    labelPosition: 'middle',
                    title: '',
                    type: 'column',
                    valueAxis: 'va1',
                    valueField: 'rawScore',
                    lineThickness: 0,
                    labelText: ' ',
                    labelFunction: (item) => {
                        return item.dataContext.letterScore;
                    },
                    showAllValueLabels: true,
                    fontSize: 18,
                    colorField: 'colorScore'
                }
            ],
            guides: [],
            valueAxes: [
                {
                    id: 'va1',
                    totalText: '',
                    unit: '',
                    title: Number(scoreID) !== 0
                        ? '%'
                        : chartTitle,
                }
            ],
            allLabels: [],
            balloon: {},
            titles: []
        };

        // when we have reference items, then add a new axis and attach the items
        if (referenceAssetsSelected === true) {
            const graphs = cfg.graphs as any[];
            graphs.push({
                balloonText: '<b>' + this.referenceAssetsTitle + '</b><br/>[[value]]' + (scoreID !== 0 ? '%' : ''),
                fillAlphas: 0.5,
                id: 'ReferenceRawScoreCol',
                labelPosition: 'middle',
                title: '',
                type: 'column',
                valueAxis: 'va1',
                valueField: 'referenceRawScore',
                lineThickness: 1,
                lineColorField: 'referenceColorScore',
                labelText: ' ',
                labelFunction: (item) => {
                    return item.dataContext.referenceLetterScore;
                },
                showAllValueLabels: true,
                fontSize: 18,
                colorField: 'referenceColorScore'
            });
        }

        if (Number(scoreID) !== 0) {
            cfg.valueAxes[0]['minimum'] = 0;
            cfg.valueAxes[0]['maximum'] = 100;
        }
        return cfg;
    }

    private makeScoreDistributionChart(htmlID: string,
                                       dataProvider: ScoreDistributionResult,
                                       referenceAssetsSelected: boolean,
                                       localOptions: LocalOptions): any {
        const h = new window['ScoreDistributionGraph'](htmlID,
                        localOptions.decimal_separator,
                        localOptions.thousands_separator,
                        localOptions.short_date,
                        this.targetTitle,
                        this.widget.targetValue,
                        referenceAssetsSelected);

        const cfg = h.get(dataProvider);

        return cfg;
    }

    private makeAccelerationEventsChart(dataProvider: AccelerometerEventsResult, localOptions: LocalOptions, distanceMetric: string): any {
        const aec = new AccelerometerEventsChart();
        return aec.makeChart(dataProvider,
                            localOptions.decimal_separator,
                            localOptions.thousands_separator,
                            distanceMetric);
    }

    private getEnergySavingsTitle(energyTypeID?: any): Promise<string>  {
        return new Promise((resolve, reject) => {
            const energyUnit = null;
            if (energyTypeID) {
                this.energyTypesService
                    .getEnergyUnit(energyTypeID)
                    .subscribe((res: EnergyUnitOptions) => {
                        resolve(this.chartTitle + ' ' + this.userOptions.getCalculatedUnit('energyMJ', 100, res));
                    });
            } else {
                resolve(this.chartTitle + ' ' + this.userOptions.getCalculatedUnit('energyMJ', 100, null));
            }
        });
    }

    private referenceAssetsSelected(): boolean {
        if (!this.widget) {
            return false;
        }
        return this.widget.referenceID !== -1 && (this.widget.groupID > 0 || this.widget.vehicleTypeID > 0);
    }
}
