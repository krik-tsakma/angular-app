import { ViewGranularityOptions } from '../../dashboard-types';
import { PeriodTypes } from '../../../common/period-selector/period-selector-types';
import { AssetTypes } from '../../../common/entities/entity-types';
import { Score } from '../../../common/score';

import { DistributionTypes } from '../../../common/distribution-types/distribution-types';

export enum ChartTypes {
    EnergySavings = 0,
    ScoreEvolution = 1,
    ScoreBuildUp = 2,
    ScoreDistribution = 3,
    AccelerometerEvents = 4
}

// ENERGY SAVINGS 

export class EnergySavingsRequest {
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public vehicleTypeID?: number;
    public viewGranularity: ViewGranularityOptions;
    public periodType: PeriodTypes;
    public assetType: AssetTypes;
    public energyTypeID?: number;

    constructor(assetType: AssetTypes, 
                periodType: PeriodTypes, 
                viewGranularity: ViewGranularityOptions, 
                groupID?: number,
                assetID?: number,
                groupDimensionID?: number,
                vehicleTypeID?: number,
                energyTypeID?: number) {
        this.assetType = assetType;
        this.periodType = periodType;
        this.viewGranularity = viewGranularity;
        this.groupID = groupID;
        this.assetID = assetID;
        this.groupDimensionID = groupDimensionID;
        this.vehicleTypeID = vehicleTypeID;
        this.energyTypeID = energyTypeID;        
    }
}

export class EnergySavingsResult {
    public items: EnergySavingsResultItem[];
}

export class EnergySavingsResultItem {
    public timestamp: Date;
    public actual: number;
    public target: number;
    public baseline: number;
    public referenceItemActual: number;
}

// SCORE EVOLUTION 

export class ScoreEvolutionRequest {
    public assetType: AssetTypes;
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public vehicleTypeID?: number;
    public viewGranularity: ViewGranularityOptions;
    public periodType: PeriodTypes;
    public scoreID: number;
    public referenceID?: number;

    constructor(assetType: AssetTypes,
                periodType: PeriodTypes, 
                viewGranularity: ViewGranularityOptions, 
                scoreID: number,
                groupID?: number,
                assetID?: number,
                groupDimensionID?: number,
                vehicleTypeID?: number,
                referenceID?: number) {
        this.assetType = assetType;
        this.periodType = periodType;
        this.viewGranularity = viewGranularity;
        this.groupID = groupID;
        this.assetID = assetID;
        this.groupDimensionID = groupDimensionID;
        this.vehicleTypeID = vehicleTypeID;
        this.scoreID = scoreID;
        this.referenceID = referenceID;
    }
}


export class ScoreEvolutionResult {
    public items: ScoreEvolutionResultItem[];
    public referenceItems: ScoreEvolutionResultItem[];
}

export class ScoreEvolutionResultItem {
    public timestamp: Date;
    public score: Score;
}

export class ScoreEvolutionChartItem {
    public timestamp: Date;
    public rawScore: number;
    public letterScore: string;
    public colorScore: string;
    public referenceRawScore: number;
    public referenceColorScore: string;
    public referenceLetterScore: string;
}

// SCORE BUILD UP 

export class ScoreBuildUpRequest {
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public vehicleTypeID?: number;
    public deviceTypeGroupID: number;
    public scoreID: number;
    public periodType: PeriodTypes;
    public assetType: AssetTypes;
    public referenceID?: number;

    constructor(assetType: AssetTypes, 
                periodType: PeriodTypes, 
                scoreID: number,
                deviceTypeID: number,
                assetID?: number,
                groupID?: number,
                groupDimensionID?: number,
                vehicleTypeID?: number,
                referenceID?: number) {
        this.assetType = assetType;
        this.periodType = periodType;
        this.groupID = groupID;
        this.assetID = assetID;
        this.groupDimensionID = groupDimensionID;
        this.vehicleTypeID = vehicleTypeID;
        this.deviceTypeGroupID = deviceTypeID;
        this.scoreID = scoreID;
        this.referenceID = referenceID;
    }
}

export class ScoreBuildUpResult {
    public items: ScoreBuildUpResultItem[];
    public referenceItems: ScoreBuildUpResultItem[];
    public score: Score;
}

export class ScoreBuildUpResultItem {
    public datapointID: number;
    public datapointName: string;
    public targetDeviation?: number;
    public weight?: number;
    public letter?: string;
}


// SCORE DISTRIBUTION

export class ScoreDistributionRequest {
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public vehicleTypeID?: number;
    public viewGranularity: ViewGranularityOptions;
    public periodType: PeriodTypes;
    public assetType: AssetTypes;
    public scoreID: number;
    public distributeBy: DistributionTypes;
    public referenceID?: number;

    constructor(assetType: AssetTypes, 
                periodType: PeriodTypes, 
                scoreID: number,
                viewGranularity: ViewGranularityOptions, 
                distributeBy?: DistributionTypes, 
                groupID?: number,
                assetID?: number,
                groupDimensionID?: number,
                vehicleTypeID?: number,
                referenceID?: number) {
        this.assetType = assetType;
        this.periodType = periodType;
        this.viewGranularity = viewGranularity;
        this.groupID = groupID;
        this.assetID = assetID;
        this.groupDimensionID = groupDimensionID;
        this.vehicleTypeID = vehicleTypeID;
        this.scoreID = scoreID;
        this.distributeBy = distributeBy ? distributeBy : DistributionTypes.NumOfTrips;
        this.referenceID = referenceID;
    }
}

export class ScoreDistributionResult {
    public items: ScoreDistributionResults[];
    public referenceItems: ScoreDistributionResults[];
}

export class ScoreDistributionResults {
    public timestamp: Date;
    public items: ScoreDistributionItem[];
}
export class ScoreDistributionItem {
    public letter: string;
    public perc: number;
}
export class ScoreDistributionChartItem extends ScoreDistributionItem {
    public timestamp: Date;
}

// ACCELEROMETER EVENTS 

export class AccelerometerEventsRequest {
    public assetType: AssetTypes;
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public vehicleTypeID?: number;
    public periodType: PeriodTypes;
    public scoreID: number;
    public referenceID?: number;

    constructor(assetType: AssetTypes,
                periodType: PeriodTypes, 
                scoreID: number,
                assetID?: number,
                groupID?: number,
                groupDimensionID?: number,
                vehicleTypeID?: number,
                referenceID?: number) {
        this.assetType = assetType;
        this.periodType = periodType;
        this.assetID = assetID;
        this.scoreID = scoreID;
        this.groupID = groupID;
        this.groupDimensionID = groupDimensionID;
        this.vehicleTypeID = vehicleTypeID;
        this.referenceID = referenceID;
    }
}
