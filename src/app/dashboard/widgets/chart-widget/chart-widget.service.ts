// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from '../../../config/config';
import { AuthHttp , ObjectURLSearchParams } from '../../../auth/authHttp';

// TYPES
import { 
    EnergySavingsRequest, EnergySavingsResult, 
    ScoreEvolutionResult, ScoreEvolutionRequest, 
    ScoreBuildUpResult, ScoreBuildUpRequest, 
    ScoreDistributionResult, ScoreDistributionRequest, 
    AccelerometerEventsRequest 
} from './chart-types';
import { AccelerometerEventsResult } from '../../../common/accelerometer-events';

@Injectable()
export class ChartsService {

    private config: any;

    constructor(
        private _authHttp: AuthHttp,
        private _config: Config) {}

    public getEnergySavings(req: EnergySavingsRequest): Observable<EnergySavingsResult> {
        
        const params = new ObjectURLSearchParams(req);

        return this._authHttp
            .get(this._config.get('apiUrl') + '/api/EnergySavings', { params });
    }

    public getScoreEvolution(req: ScoreEvolutionRequest): Observable<ScoreEvolutionResult> {
       
        const params = new ObjectURLSearchParams(req);

        return this._authHttp
            .get(this._config.get('apiUrl') + '/api/ScoreEvolution', { params });
    }
    
    public getScoreBuildUp(req: ScoreBuildUpRequest): Observable<ScoreBuildUpResult> {

        const params = new ObjectURLSearchParams(req);

        return this._authHttp
            .get(this._config.get('apiUrl') + '/api/ScoreBuildUp', { params });
    }

    public getScoreDistribution(req: ScoreDistributionRequest): Observable<ScoreDistributionResult> {

        const params = new ObjectURLSearchParams(req);

        return this._authHttp
            .get(this._config.get('apiUrl') + '/api/ScoreDistribution', { params });
    }

    public getAccelerationEvents(req: AccelerometerEventsRequest): Observable<AccelerometerEventsResult> {

        const params = new ObjectURLSearchParams(req);

        return this._authHttp
            .get(this._config.get('apiUrl') + '/api/AccelerometerEvents', { params });
    }
}
