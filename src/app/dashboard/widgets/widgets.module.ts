// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';

// COMPONENTS
import { ChartWidgetComponent } from './chart-widget/chart-widget.component';
import { SummaryWidgetComponent } from './summary-widget/summary-widget.component';
import { RankingWidgetComponent } from './ranking-widget/ranking-widget.component';
import { EventsWidgetComponent } from './events-widget/events-widget.component';

// SERVICES
import { WidgetsService } from '../dashboard.service';
import { EventsWidgetService } from './events-widget/events-widget.service';
import { RankingService } from './ranking-widget/ranking-widget.service';
import { SummaryService } from './summary-widget/summary-widget.service';
import { ChartsService } from './chart-widget/chart-widget.service';

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        ChartWidgetComponent,
        SummaryWidgetComponent,
        RankingWidgetComponent,
        EventsWidgetComponent
    ],
    exports: [
        ChartWidgetComponent,
        SummaryWidgetComponent,
        RankingWidgetComponent,
        EventsWidgetComponent
    ],
    providers: [
        WidgetsService,
        SummaryService,
        ChartsService,
        RankingService,
        EventsWidgetService
    ]
})
export class DashboardWidgetsModule { }
