import { AssetTypes } from '../../../common/entities/entity-types';
import { PeriodTypes } from '../../../common/period-selector/period-selector-types';
import { Score } from '../../../common/score';


export class RankingRequest {
    public scoreID: number;
    public periodType: PeriodTypes;
    public sortDirection: SortDirection;
    public numberOfRows: number;
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;

    constructor(periodType: PeriodTypes, 
                scoreID: number,
                sortDirection: SortDirection,
                numberOfRows: number,
                groupID?: number,
                assetID?: number,
                groupDimensionID?: number) {
        this.periodType = periodType;
        this.scoreID = scoreID;
        this.sortDirection = sortDirection;
        this.numberOfRows = numberOfRows;
        this.groupID = groupID;
        this.assetID = assetID;
        this.groupDimensionID = groupDimensionID;
    }
}

export class RankingResult {
    public items: RankingResultItem[];
}

export class RankingResultItem {
    public firstname: string;
    public lastname: string;
    public score: Score;
    public rank: number;
    public changeTrend: ChangeTrends;
    public previousRank?: number;
}

export enum SortDirection {
    ASC,
    DESC
}

export enum ChangeTrends {
    Upward,
    Downside,
    Steady,
    Unavailable
}
