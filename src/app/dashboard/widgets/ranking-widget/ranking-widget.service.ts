// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from '../../../config/config';
import { AuthHttp , ObjectURLSearchParams } from '../../../auth/authHttp';

// TYPES
import { RankingRequest, RankingResult } from './ranking-types';

@Injectable()
export class RankingService {
    public baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = config.get('apiUrl') + '/api/Ranking';
        }

    // Get ranking type wiget
    public get(req: RankingRequest): Observable<RankingResult> {

        const params = new ObjectURLSearchParams(req);
        

        return this.authHttp
            .get(this.baseURL, { params });
    }
}

