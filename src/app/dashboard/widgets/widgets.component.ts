// FRAMEWORK
import { Component, AfterViewInit, OnDestroy, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// SERVICES
import { WidgetsService } from '../dashboard.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { MaximizeService } from '../../shared/maximize/maximize.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { Config } from '../../config/config';

// TYPES
import {
    Widget, WidgetTypes, WidgetLayout, WidgetLayoutPositions,
    WidgetZoneUpsertResultCodeOptions, WidgetTypesAclMapping
} from './widget-types';
import { Resizer } from '../../shared/maximize/maximize-types';
import {
    MoreOptionsMenuOption, MoreOptionsMenuOptions, MoreOptionsMenuTypes
} from '../../shared/more-options-menu/more-options-menu.types';


@Component({
    selector: 'widgets',
    templateUrl: 'widgets.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['widgets.less']
})

export class WidgetsComponent implements AfterViewInit, OnDestroy  {
    public pageOptions: MoreOptionsMenuOptions[] = [
        {
            term: 'customize_layout.title',
            options: [
                {
                    id: 'customize',
                    term: 'customize_layout.customize',
                    type: MoreOptionsMenuTypes.svgIcon,
                    enabled: true,
                    svgIcon: 'customize_icon',
                    svgIconClass: 'custom-svg-icon',
                    svgUrl: 'images/widgets/customize_icon.svg'
                },
                {
                    id: 'restore',
                    term: 'dashboard.restore_options',
                    type: MoreOptionsMenuTypes.svgIcon,
                    enabled: true,
                    svgIcon: 'restore_icon',
                    svgIconClass: 'custom-svg-icon',
                    svgUrl: 'images/widgets/restore_icon.svg'
                }
            ]
        }
    ];

    public message: string;
    public isDriver: boolean;
    public isManager: boolean;
    public widgetTypes = WidgetTypes;
    public fullWidth: boolean;
    public summaryWidgets: Widget[] = new Array<Widget>();
    public chartWidgets: Widget[] = new Array<Widget>();
    public otherWidgets: Widget[] = new Array<Widget>();
    public layout: WidgetLayout = new WidgetLayout();
    public layoutPositions = WidgetLayoutPositions;
    public maximizer: Resizer = null;
    public loading: boolean = true;
    public editMode: boolean = false;

    private serviceSubscriber: any;

    constructor(
        public router: Router,
        public activatedRoute: ActivatedRoute,
        public userOptions: UserOptionsService,
        private widgetService: WidgetsService,
        private maximizeService: MaximizeService,
        private unsubscribe: UnsubscribeService,
        private configService: Config,
        private cdr: ChangeDetectorRef ) {
            const u = userOptions.getUser();
            this.isDriver = u.isDriver;
            this.isManager = u.isManager;

            this.maximizeService.resizer.subscribe((resizer: Resizer) => {
                this.maximizer = resizer;
            });

            // set the correct widgets height
            this.setWidthWidget();
            window.onresize = (e) => {
                 this.setWidthWidget();
            };

            const widgetMenuOptions = this.widgetPageOptions();

            const userAllowedToAddWidgets = this.activatedRoute.snapshot.data.allowedToAddWidgets;

            widgetMenuOptions.options.forEach((w) => {
                const flagValue = WidgetTypesAclMapping.GetForWidgetType(w.id);
                if (userAllowedToAddWidgets && userAllowedToAddWidgets.find((x) => x === flagValue)) {
                    w.enabled = true;
                }
            });

            // show widget add section if there is at least one widget type allowed
            if (!(widgetMenuOptions.options.filter((w) => w.enabled === false).length === widgetMenuOptions.options.length)) {
                this.pageOptions.unshift(widgetMenuOptions);
            }

        }

    // On Init get widgets from Back-end
    public ngAfterViewInit(): void {
        this.get();
    }

    // Clean widgetSubscribe to avoid memory leak
    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }


    public onDeleted(id: number): void {
        let widgets = this.getAllWidgets();
        widgets = widgets.filter((widget) => {
            return widget.instance.id !== id;
        });
        this.zoneBasedWidgetSeparation(widgets);
    }

    // ----------------
    // EVENT HANDLERS
    // ----------------
    public onSelectPageOption(item: MoreOptionsMenuOption): void {
        if (item.id === 'customize') {
            this.customizeLayout();
        }  else if (item.id === 'restore') {
            this.restoreDefaultWidgets();
        } else {
            this.addWidget(item.id);
        }
    }

    // navigate to create widgets page
    public addWidget(item: WidgetTypes): void {
        this.router.navigate([this.router.url, 'add', item], { skipLocationChange: !this.configService.get('router') });
    }

    // enable edit layout mode
    public customizeLayout(): void {
        const widgetsAll = this.getAllWidgets();
        if (this.editMode === true ||
            (!widgetsAll || widgetsAll.length === 0)) {
            return;
        }

        this.editMode = true;
    }

    // cancel layout changes
    public onCancelEdit() {
        this.editMode = false;
        this.get();
    }

    // save layout changes
     public onSaveEdit() {
        this.loading = true;
        this.message = '';
        const widgets = this.getAllWidgets().map((w) => w.instance);
        this.serviceSubscriber = this.widgetService
            .updateMany(widgets)
            .subscribe((status: number) => {
                this.loading = false;
                this.editMode = false;
                if (status !== 200) {
                    this.message = 'data.error';
                    return;
                }
                this.saveZoneLayout();
            },
            (err) => {
                this.loading = false;
                this.editMode = false;
                this.message = 'data.error';
            });
    }

    public saveZoneLayout() {
        const zoneLayout = this.layout.get();
        if (!zoneLayout) {
            return;
        }
        this.loading = true;
        this.message = '';

        this.serviceSubscriber = this.widgetService
            .upsertZones(zoneLayout)
            .subscribe((res: any) => {
                this.loading = false;
                if (res.code !== WidgetZoneUpsertResultCodeOptions.Ok) {
                    this.message = 'data.error';
                    return;
                }
                this.get();
            },
            (err) => {
                if (err.ok === false || (err.status && err.status !== 200)) {
                    this.loading = false;
                    this.message = 'data.error';
                }
            });
    }

    public onChangeLayout(type: string, position: WidgetLayoutPositions) {
        if (type === 'summary') {
            this.layout.summary = position;
        }

        if (type === 'charts') {
            this.layout.charts = position;
        }
    }

    public onChangeWidgetOrderUp(widget: any) {
        let widgets = null;
        switch (widget.type) {
            case WidgetTypes.Summary: {
                widgets = this.summaryWidgets;
                break;
            }
            case WidgetTypes.Chart: {
                widgets = this.chartWidgets;
                break;
            }
            default: {
                widgets = this.otherWidgets;
                break;
            }
        }
        const currentIndex = widgets.findIndex((w) => w.instance.id === widget.instance.id);
        if (currentIndex === 0) {
            return;
        }

        const prevWidget = widgets[currentIndex - 1];
        if (prevWidget) {
            // clone current order num
            const current = JSON.parse(JSON.stringify(widget.instance.order));
            const prev = JSON.parse(JSON.stringify(prevWidget.instance.order));
            widget.instance.order = prev;
            prevWidget.instance.order = current;
        }

        this.sortAscendingOrder(widgets);
    }

    public onChangeWidgetOrderDown(widget: any) {
        let widgets = null;
        switch (widget.type) {
            case WidgetTypes.Summary: {
                widgets = this.summaryWidgets;
                break;
            }
            case WidgetTypes.Chart: {
                widgets = this.chartWidgets;
                break;
            }
            default: {
                widgets = this.otherWidgets;
                break;
            }
        }

        const currentIndex = widgets.findIndex((w) => w.instance.id === widget.instance.id);

        const widgetsCount = widgets ? widgets.length : 0;
        if (widgetsCount === 0 || widgetsCount === 1
            ||  currentIndex === (widgetsCount - 1)) {
            return;
        }

        const nextWidget = widgets[currentIndex + 1];
        if (nextWidget) {
            // clone current order num
            const current = JSON.parse(JSON.stringify(widget.instance.order));
            const next = JSON.parse(JSON.stringify(nextWidget.instance.order));
            widget.instance.order = next;
            nextWidget.instance.order = current;
        }

        this.sortAscendingOrder(widgets);
    }

     // RESIZE CALLBACKS
    public hideElement(id) {
        return this.maximizer !== null && this.maximizer.maximized === true && this.maximizer.htmlID !== id;
    }

    // ----------------
    // PRIVATE
    // ----------------
    private sortAscendingOrder(widgets: Widget[]): Widget[] {
        return widgets.sort((x1, x2) => x1.instance.order  - x2.instance.order);
    }

    // Get method for widgets
    private get(): void {
        this.loading = true;
        this.message = '';

        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.widgetService
            .get()
            .subscribe((widgets) => {
                this.loading = false;
                if (!widgets || widgets.items.length === 0) {
                    this.message = 'data.empty_records';
                }
                this.zoneBasedWidgetSeparation(widgets.items);

                if (widgets.zoneLayout) {
                    this.layout.set(widgets.zoneLayout);
                }
            },
            (err) => {
                if (err.status !== 200 || err.ok === false) {
                    this.loading = false;
                    this.message = 'data.error';
                }
            });
    }


    private restoreDefaultWidgets(): void {
        this.loading = true;
        this.message = '';

        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.widgetService
            .deleteAll()
            .subscribe((res) => {
                this.loading = false;
                this.get();
            },
            (err) => {
                if (err.status !== 200 || err.ok === false) {
                    this.loading = false;
                    this.message = 'data.error';
                }
            });

    }


    private setWidthWidget() {
        if (window.innerWidth < 720) {
            this.fullWidth = true;
        } else {
            this.fullWidth = false;
        }
    }

    private zoneBasedWidgetSeparation(widgets: Widget[]) {
        this.summaryWidgets = widgets.filter((x) => x.type === WidgetTypes.Summary);
        this.chartWidgets = widgets.filter((x) => x.type === WidgetTypes.Chart);
        this.otherWidgets = widgets.filter((x) => x.type === WidgetTypes.Events || x.type === WidgetTypes.Ranking);
    }

    private getAllWidgets(): Widget[] {
        const widgetsAll = this.summaryWidgets.concat(this.chartWidgets).concat(this.otherWidgets);
        return widgetsAll;
    }


    private widgetPageOptions(): MoreOptionsMenuOptions {
       return {
            term: 'dashboard.add_widget',
            options: [
                {
                    id: WidgetTypes.Chart,
                    term: 'dashboard.edit_widget.chart',
                    type: MoreOptionsMenuTypes.svgIcon,
                    enabled: false,
                    svgIcon: 'graph_icon',
                    svgIconClass: 'custom-svg-icon',
                    svgUrl: 'images/widgets/graph_icon.svg'
                },
                {
                    id: WidgetTypes.Summary,
                    term: 'dashboard.edit_widget.summary',
                    type: MoreOptionsMenuTypes.svgIcon,
                    enabled: false,
                    svgIcon: 'summary_icon',
                    svgIconClass: 'custom-svg-icon',
                    svgUrl: 'images/widgets/summary_icon.svg'
                },
                {
                    id: WidgetTypes.Ranking,
                    term: 'dashboard.edit_widget.ranking',
                    type: MoreOptionsMenuTypes.svgIcon,
                    enabled: false,
                    svgIcon: 'ranking_icon',
                    svgIconClass: 'custom-svg-icon',
                    svgUrl: 'images/widgets/ranking_icon.svg'
                },
                {
                    id: WidgetTypes.Events,
                    term: 'dashboard.edit_widget.events',
                    type: MoreOptionsMenuTypes.svgIcon,
                    enabled: false,
                    svgIcon: 'events_icon',
                    svgIconClass: 'custom-svg-icon',
                    svgUrl: 'images/widgets/events_icon.svg'
                }
            ]
        };
    }
}
