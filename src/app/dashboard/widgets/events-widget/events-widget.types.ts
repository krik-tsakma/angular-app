import { BaseWidget } from '../widget-types';
import { AssetTypes } from '../../../common/entities/entity-types';
import { PeriodTypes } from '../../../common/period-selector/period-selector-types';
import { KeyName } from '../../../common/key-name';

export class EventsWidget extends BaseWidget implements IEventsParams {
    public periodType: PeriodTypes;
    public assetType: AssetTypes;
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public vehicleTypeID?: number;
    public classIdx?: number;
    public priorityIdx?: number;

    constructor(title: string, 
                cols: number,
                order: number,
                assetType: AssetTypes,
                periodType: PeriodTypes,
                groupID?: number,
                assetID?: number,
                groupDimensionID?: number,
                vehicleTypeID?: number,
                classIdx?: number,
                priorityIdx?: number) {
        super();
        this.title = title;
        this.cols = cols;
        this.order = order;
        this.periodType = periodType;
        this.assetType = AssetTypes.HR;
        this.groupID = groupID;
        this.assetID = assetID;
        this.groupDimensionID = groupDimensionID;
        this.vehicleTypeID = vehicleTypeID;
        this.classIdx = classIdx;
        this.priorityIdx = priorityIdx;
    }
}

export interface IEventsParams {
    periodType: PeriodTypes;
    assetType: AssetTypes;
    groupID?: number;
    assetID?: number;
    groupDimension?: number;
    vehicleTypeID?: number;
    classIdx?: number;
    priorityIdx?: number;
}

export class EventsRequest implements IEventsParams {
    public periodType: PeriodTypes;
    public assetType: AssetTypes;
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public classIdx?: number;
    public priorityIdx?: number;
    public vehicleTypeID?: number;

    constructor(periodType: PeriodTypes, 
                assetType: AssetTypes,
                groupID?: number,
                assetID?: number, 
                groupDimensionID?: number,
                vehicleTypeID?: number,
                classIdx?: number,
                priorityIdx?: number) {
        this.periodType = periodType;
        this.assetType = assetType;
        this.classIdx = classIdx;
        this.priorityIdx = priorityIdx;
        this.vehicleTypeID = vehicleTypeID;
        this.groupID = groupID;
        this.assetID = assetID;
        this.groupDimensionID = groupDimensionID;
    }
}

export class EventsResult {
    public items: EventResultItem[];
}

export class EventResultItem {
    public ruleID: number;
    public ruleName: string;
    public numOfEvents: number;
}

/* tslint:disable */
export interface EventProperty extends KeyName {

}
/* tslint:enable */
