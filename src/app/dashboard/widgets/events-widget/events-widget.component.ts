// FRAMEWORK
import {
    Component, EventEmitter, OnChanges, SimpleChanges,
    OnDestroy, Input, Output, ViewEncapsulation, ElementRef
} from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { WidgetsService } from '../../dashboard.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { EventsWidgetService } from './events-widget.service';
import { Config } from '../../../config/config';

// COMPONENTS
import { DeleteWidgetBaseComponent } from '../../delete-widget-base.component';

// TYPES
import { BaseWidget, WidgetTypes } from '../widget-types';
import { EventsRequest, EventsResult, EventsWidget } from './events-widget.types';
import { ScoreFormatter } from '../../../common/score';
import { AssetTypes } from '../../../common/entities/entity-types';

@Component({
    selector: 'events-widget',
    templateUrl: './events-widget.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./events-widget.less'],
    providers: []
})

export class EventsWidgetComponent extends DeleteWidgetBaseComponent implements OnChanges, OnDestroy {
    @Input() public editing: boolean;
    @Input() public widget: EventsWidget = {} as EventsWidget;
    @Output() public onDeleted = new EventEmitter<number>();
    public scoreFormatter = ScoreFormatter;
    public result: EventsResult;

    public loading: boolean;
    public message: string;

    private serviceSubscriber: any;
    private deleteWidgetSubscriber: any;

    constructor(
        public elementRef: ElementRef,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,   
        protected confirmDialogService: ConfirmDialogService,
        protected widgetService: WidgetsService,
        public userOptions: UserOptionsService,
        private service: EventsWidgetService,
        private translate: CustomTranslateService,
        private router: Router,
        private configService: Config) {
            super(unsubscriber, widgetService, confirmDialogService , snackBar);
    }

    // =========================
    // ANGULAR LIFECYCLE
    // =========================

    public ngOnChanges(changes: SimpleChanges) {
        const widgetChange = changes['widget'];
        if (widgetChange) {
            const current = widgetChange.currentValue as EventsWidget;
            const previous = widgetChange.previousValue as EventsWidget;
            // it makes sense to fetch data for all elements change apart from title
            if (current && previous && previous.title && current.title.toString() !== previous.title.toString()) {
                return;
            }

            this.get();
        }
    }


    // Clean subscribed observables to avoid memory leak
    public ngOnDestroy() {
        super.ngOnDestroy();
        this.unsubscriber.removeSubscription([
            this.serviceSubscriber,
            this.deleteWidgetSubscriber
        ]);        
    }

    public edit(id): void {
        this.router.navigate(['/dashboard/edit', WidgetTypes.Events, id], { skipLocationChange: !this.configService.get('router') });
    }

  
    public openDialog(widget: BaseWidget) {
        this.openDeleteDialog(widget.id)
            .then((deleted: boolean) => {
                if (deleted === true) {
                    this.onDeleted.emit(widget.id);
                }
            });
    }


    private get(): void {
        if (this.widget === undefined || Object.keys(this.widget).length === 0) {
            return;
        }
        this.message = '';
        this.loading = true;
        if (this.widget.assetID === -1) {
            this.widget.assetID = this.userOptions.getUser().driverId;
            this.widget.assetType = AssetTypes.HR;
        }
        const params = new EventsRequest(this.widget.periodType,
                                        this.widget.assetType, 
                                        this.widget.groupID, 
                                        this.widget.assetID,
                                        this.widget.groupDimensionID,
                                        this.widget.vehicleTypeID,
                                        this.widget.classIdx,
                                        this.widget.priorityIdx);
        this.unsubscriber.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.service
            .get(params)
            .subscribe(
                (result) => {
                    this.loading = false;
                    this.result = result;

                    if (!result || result.items.length === 0) {
                         this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }     
}
