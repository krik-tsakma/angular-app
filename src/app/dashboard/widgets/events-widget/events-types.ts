import { AssetTypes } from '../../../common/entities/entity-types';
import { PeriodTypes } from '../../../common/period-selector/period-selector-types';
import { KeyName } from '../../../common/key-name';

export interface IEventsParams {
    periodType: PeriodTypes;
    assetType: AssetTypes;
    groupID?: number;
    assetID?: number;
    groupDimension?: number;
    vehicleTypeID?: number;
    classIdx?: number;
    priorityIdx?: number;
}

export class EventsRequest implements IEventsParams {
    public periodType: PeriodTypes;
    public assetType: AssetTypes;
    public groupID?: number;
    public assetID?: number;
    public groupDimensionID?: number;
    public classIdx?: number;
    public priorityIdx?: number;
    public vehicleTypeID?: number;

    constructor(periodType: PeriodTypes, 
                assetType: AssetTypes,
                groupID?: number,
                assetID?: number, 
                groupDimensionID?: number,
                vehicleTypeID?: number,
                classIdx?: number,
                priorityIdx?: number) {
        this.periodType = periodType;
        this.assetType = assetType;
        this.classIdx = classIdx;
        this.priorityIdx = priorityIdx;
        this.vehicleTypeID = vehicleTypeID;
        this.groupID = groupID;
        this.assetID = assetID;
        this.groupDimensionID = groupDimensionID;
    }
}

export class EventsResult {
    public items: EventResultItem[];
}

export class EventResultItem {
    public ruleID: number;
    public ruleName: string;
    public numOfEvents: number;
}

export class EventPropertiesResult {
    public classes: EventProperty[];
    public priorities: EventProperty[];
}

/* tslint:disable */
export interface EventProperty extends KeyName {

}
/* tslint:enable */
