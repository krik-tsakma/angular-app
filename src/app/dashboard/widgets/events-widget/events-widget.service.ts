// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from '../../../config/config';
import { AuthHttp , ObjectURLSearchParams } from '../../../auth/authHttp';

// TYPES
import { EventsResult, EventsRequest } from './events-widget.types';

@Injectable()
export class EventsWidgetService {
    private baseURL: string;

    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/Events';
        }

    // Get events type wiget
    public get(req: EventsRequest): Observable<EventsResult> {

        const params = new ObjectURLSearchParams(req);

        return this.authHttp
            .get(this.baseURL, { params });
    }
}
