// FRAMEWORK
import { Injectable } from '@angular/core';
import { Router, Resolve } from '@angular/router';

// SERVICES
import { WidgetsService } from '../dashboard.service';
import { Config } from '../../config/config';


@Injectable()
export class AllowedWidgetTypesResolver implements Resolve<number[]> {
    constructor(
        private configService: Config,
        private service: WidgetsService,
        private router: Router) { }

    public resolve(): Promise<number[]> {            
        return this.service
            .getAllowedWidgetTypesToAdd()
            .toPromise()
            .then((res: number[]) => {
                if (res) {                    
                    return res;
                } 
                return null;
            })
            .catch((err) => {            
                return null;
            });
    }
}
