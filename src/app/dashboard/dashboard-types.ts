export enum ViewGranularityOptions {
    Daily = 0,
    Weekly = 1,
    Monthly = 2
}

export enum TargetDirection  {
    Upward = 0,
    Downward = 1
}
