﻿// FRAMEWORD
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../auth/authGuard';

// COMPONENTS
import { WidgetsComponent } from './widgets/widgets.component';
import { EditWidgetComponent } from './edit/edit-widget.component';

import { AllowedWidgetTypesResolver } from './widgets/allowed-widget-types-resolver.service';

const DashboardRoutes: Routes = [
    {
        path: '',
        component: WidgetsComponent,
        resolve: {
            allowedToAddWidgets: AllowedWidgetTypesResolver
        },
        canActivate: [AuthGuard]
    },
    {
        path: ':editMode/:type/:id',
        component: EditWidgetComponent,
        canActivate: [AuthGuard]
    },
    {
        path: ':editMode/:type',
        component: EditWidgetComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(DashboardRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class DashboardRoutingModule { }
