﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// COMPONENTS
import { WidgetsComponent } from './widgets/widgets.component';
import { EditWidgetComponent } from './edit/edit-widget.component';

// MODULES
import { SharedModule } from '../shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardWidgetsModule } from './widgets/widgets.module';
import { EditDashboardWidgetsModule } from './edit/edit-widget.module';

// SERVICES
import { TranslateStateService } from '../core/translateStore.service';

// RESOLVER
import { AllowedWidgetTypesResolver } from './widgets/allowed-widget-types-resolver.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/dashboard/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        DashboardRoutingModule,
        DashboardWidgetsModule,
        EditDashboardWidgetsModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        }),
    ],
    declarations: [
        WidgetsComponent,
        EditWidgetComponent
    ],
    providers: [
        AllowedWidgetTypesResolver
    ]
})
export class DashboardModule { }
