
// FRAMEWORK
import { OnDestroy } from '@angular/core';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { SnackBarService } from '../shared/snackbar.service';
import { UnsubscribeService } from '../shared/unsubscribe.service';
import { WidgetsService } from './dashboard.service';
import { ConfirmDialogService } from '../shared/confirm-dialog/confirm-dialog.service';
import { ConfirmationDialogParams } from '../shared/confirm-dialog/confirm-dialog-types';

export class DeleteWidgetBaseComponent implements OnDestroy {
    public deleteLoading: boolean;
    private subscriber: Subscription;

    constructor(        
        protected unsubscriber: UnsubscribeService,
        protected widgetService: WidgetsService,
        protected confirmDialogService: ConfirmDialogService,
        protected snackBar: SnackBarService
    ) {
       // foo
    }
    

    public ngOnDestroy(): void {
        this.unsubscriber.removeSubscription(this.subscriber);
    }

   
    public openDeleteDialog(id: number): Promise<boolean> {
        return new Promise((resolve, reject) => { 
            const confirmReq = {
                title: 'form.actions.delete',
                message: 'form.warnings.are_you_sure',
                submitButtonTitle: 'form.actions.delete'
            } as ConfirmationDialogParams;
            
            this.confirmDialogService
                .confirm(confirmReq)
                .subscribe((result) => {
                    if (result === 'yes') {
                        this.deleteWidget(id)
                            .then(() => {
                                resolve(true);
                            }).catch(() => {
                                resolve(false);
                            });
                    } else {
                        resolve(false);
                    }
                });
        });
    }

    private deleteWidget(id: number, ) {
        return new Promise((resolve, reject) => { 
            this.deleteLoading = true;
            this.widgetService
                .delete(id, null)
                .subscribe((status: any) => {
                    this.deleteLoading = false;
                    if (status === 200) {
                        this.snackBar.open('form.delete_success', 'form.actions.ok');
                        resolve();
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.close');
                        reject();
                    }
                },
                (error) => {
                    this.deleteLoading = false;
                    if (error.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                    reject();
                }
            );
        });
    }
}
