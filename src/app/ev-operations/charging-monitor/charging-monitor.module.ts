// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { SharedModule } from '../../shared/shared.module';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { ScrollToTopModule } from '../../common/scroll-to-top/scroll-to-top.module';
import { ChargingMonitorRoutingModule } from './charging-monitor-routing.module';

// SERVICES
import { ChargingMonitorService } from './charging-monitor.service';
import { TranslateStateService } from '../../core/translateStore.service';

// COMPONENTS
import { ChargingMonitorComponent } from './charging-monitor.component';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/ev-operations/ev-charging-monitor/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        HttpClientJsonpModule,
        HttpClientModule,
        SharedModule,
        ScrollToTopModule,
        ChargingMonitorRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ChargingMonitorComponent
    ],
    providers: [
        ChargingMonitorService
    ]
})
export class ChargingMonitorModule { }
