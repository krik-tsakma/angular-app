﻿import { Pager, PagerResults, SearchTermRequest } from '../../common/pager';
import { KeyName } from '../../common/key-name';
import { EvChargingStatuses } from '../../charge-point-manager/charging-status.types';

export interface EvChargingRequest extends SearchTermRequest {
    chargeLocationID?: number;
    chargeLocationName?: string;
    errorsOnly: boolean;
}

export interface EvChargingResults extends PagerResults {
    results: EvChargingResultItem[];
}

export interface EvChargingResultItem {
    vehicleID?: number;
    vehicleLabel?: string;
    range?: number;
    soc?: number;
    chargeLocation: string;
    chargePointName: string;
    chargingStatus: EvChargingStatuses;
}

