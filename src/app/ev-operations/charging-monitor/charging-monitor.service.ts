﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import { EvChargingRequest, EvChargingResults } from './charging-monitor-types';
import { KeyName } from '../../common/key-name';

@Injectable()
export class ChargingMonitorService {
    private baseApiURL: string;

    constructor(
        private authHttp: AuthHttp,
        private configService: Config) {
        this.baseApiURL = this.configService.get('apiUrl') + '/api/EvChargingMonitor';
    }

    public get(req: EvChargingRequest): Observable<EvChargingResults> {

        const obj = this.authHttp.removeObjNullKeys({            
            ChargeLocationID: req.chargeLocationID ? req.chargeLocationID.toString() : null,
            ErrorsOnly: req.errorsOnly,
            PageNumber: req.pageNumber.toString(),
            PageSize: req.pageSize.toString(),
            Sorting: req.sorting,
            SearchTerm: req.searchTerm,           
        });

        const params = new HttpParams({
            fromObject: obj
        });


        return this.authHttp
            .get(this.baseApiURL, { params });
    }

}
