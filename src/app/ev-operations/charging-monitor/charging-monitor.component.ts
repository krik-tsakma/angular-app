﻿// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Subscription, of } from 'rxjs';

// SERVICES
import { ChargingMonitorService } from './charging-monitor.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';

// TYPES
import { StoreItems } from '../../shared/store-manager/store-items';
import { Pager } from '../../common/pager';
import { KeyName } from '../../common/key-name';
import {
    EvChargingRequest,
    EvChargingResults, 
    EvChargingResultItem
} from './charging-monitor-types';
import {
    TableColumnSetting, 
    TableCellFormatOptions, 
    TableColumnSortOrderOptions,
    TableColumnSortOptions
} from '../../shared/data-table/data-table.types';
import { SearchType } from '../../shared/search-mechanism/search-mechanism-types';
import { MoreOptionsMenuOption, MoreOptionsMenuTypes, MoreOptionsMenuOptions } from '../../shared/more-options-menu/more-options-menu.types';
import { EvChargingStatus } from '../../charge-point-manager/charging-status.types';

@Component({
    selector: 'charging-monitor',
    templateUrl: './charging-monitor.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['charging-monitor.less']
})

export class ChargingMonitorComponent implements OnInit, OnDestroy {
    public loading: boolean;
    public evChargingData: EvChargingResultItem[] = [] as EvChargingResultItem[];
    public tableSettings: TableColumnSetting[];
    public request: EvChargingRequest = {} as EvChargingRequest;
    public searchTypes: any = SearchType;
    public termControl = new FormControl();   
    public pager: Pager = new Pager();
    public message: string;
    public pageOptions: MoreOptionsMenuOptions[];

    private subscriber: Subscription;
    private refreshIntervalHandler: any;

    constructor(
        public userOptions: UserOptionsService,
        private translateService: CustomTranslateService,
        private service: ChargingMonitorService,
        private storeManager: StoreManagerService,
        private unsubscribe: UnsubscribeService) {
           
            this.setTableSettings(translateService);
            this.setPageOptions();

            this.termControl
                .valueChanges
                .pipe(
                    debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing             
                    distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                    // switchMap operator automatically unsubscribes from previous subscriptions,
                    // as soon as the outer Observable emits new values
                    // so we will always search with last term
                    switchMap((term) => {
                        this.request.searchTerm = term;
                        this.get(true);
                        return of([]);
                    })
                )
                .subscribe();
    }

    public ngOnInit() {        
        // savedData contains all recquest's options
        const savedData = this.storeManager.getOption(StoreItems.evChargingMonitor) as EvChargingRequest;
        if (savedData) {
            this.request = savedData;
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.get(true);
    }

    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.subscriber);
        this.setAutoRefresh(false);
    }


    // =========================
    // EVENT HANDLERS
    // =========================

    public onSelectPageOption(item: MoreOptionsMenuOption): void {
        if (item.id === 'autorefresh') {
            this.setAutoRefresh(item.checkboxValue);
        }
    }

    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.termControl.setValue(null);
    }

    public onLocationSelect(location?: KeyName) {
        this.request.chargeLocationID = location ? location.id : null;
        this.request.chargeLocationName = location ? location.name : null;
        this.get(true);
    }

    public onChangeShowErrorsOnly() {
        this.get(true);
    }

    // =========================
    // TABLE
    // =========================

    // fetch data
    public loadMore() {
        if (this.pager.pageNumber < this.pager.totalPages && this.loading === false) {
            this.pager.addPage();
            this.get(false);
        }
    }

    // When the user selected a column to sort (from list view)
    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }

    // =========================
    // PRIVATE
    // =========================

    // Fetch driver summary data
    private get(resetPageNumber: boolean) {
        if (this.loading) {
            return;
        }
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.pager.pageSize = 20;
            this.evChargingData = new Array<EvChargingResultItem>();
        } 

        this.message = '';
        this.loading = true;

        this.request.pageSize = this.pager.pageSize;
        this.request.pageNumber = this.pager.pageNumber;

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.subscriber);

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.evChargingMonitor, this.request);

        // then start the new request
        this.subscriber = this.service
            .get(this.request)
            .subscribe((response: EvChargingResults) => {
                this.loading = false;
                if (resetPageNumber === true) {
                    this.evChargingData = response && response.results
                        ? response.results
                        : [];
                } else {
                    this.evChargingData = this.evChargingData.concat(response.results);
                }
                
                if (!this.evChargingData || this.evChargingData.length === 0) {
                    this.message = 'data.empty_records';
                    return;
                }

                // Get the paging info
                this.pager.pageNumber = response.pageNumber;
                this.pager.pageSize = response.pageSize;
                this.pager.totalPages = response.totalPages;
                this.pager.totalRecords = response.totalRecords;
            },
            (err) => {
                this.loading = false;
                this.evChargingData = [];
                this.message = 'data.error';
            });
        return;
    }


    private setTableSettings(translateService: CustomTranslateService) {
        this.tableSettings = [
            {
                primaryKey: 'vehicleLabel',
                header: 'ev_charging_monitor.list_options.vehicle',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'range',
                header: 'ev_charging_monitor.list_options.range',
                format: TableCellFormatOptions.distance,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'soC',
                header: 'ev_charging_monitor.list_options.soc',
                format: TableCellFormatOptions.percentage,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'chargeLocation',
                header: 'ev_charging_monitor.list_options.charge_location',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
               primaryKey: 'chargePointName',
               header: 'ev_charging_monitor.list_options.charger_id',
               sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'chargingStatus',
                header: 'ev_charging_monitor.list_options.charging_status',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvChargingResultItem) => {
                    return EvChargingStatus.formatChargingStatus(item.chargingStatus, translateService);
                },
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            }
        ];
    }

    private setPageOptions(): void {
        this.pageOptions = [
            {
                term: 'data.autorefresh',
                options: [
                    {
                        id: 'autorefresh',
                        term: 'Autorefresh',
                        enabled: true,
                        type: MoreOptionsMenuTypes.checkbox,
                        checkboxValue: true,
                    }
                ]
            }
        ];
        this.setAutoRefresh(true);
    }

    private setAutoRefresh(autoRefreshEnabled: boolean) {
        if (autoRefreshEnabled === true) {
            // Datatable refreshes every RefreshInterval minute
            // if period type is today to fetch the new events
            const user = this.userOptions.getUser();
            this.refreshIntervalHandler = setInterval(() => { 
                this.get(true);
        }, (user.refreshIntervalSecs * 1000));
        } else {
            if (this.refreshIntervalHandler) {
                clearInterval(this.refreshIntervalHandler);
            }
        }
    }
}
