﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../../auth/authGuard';
import { ChargingMonitorComponent } from './charging-monitor.component';

const ChargingMonitorRoutes: Routes = [
    {
        path: '',
        component: ChargingMonitorComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'maximized',
        component: ChargingMonitorComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ChargingMonitorRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class ChargingMonitorRoutingModule { }
