import { style } from '@angular/animations';
// FRAMEWORK
import {
    AfterViewInit, Component, OnInit, OnDestroy,
    ViewEncapsulation, ViewChildren, QueryList, ElementRef
} from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Subscription, of } from 'rxjs';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { EvVehiclesService } from './ev-vehicles.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { ResizeService } from '../../shared/resize.service';

// TYPES
import { StoreItems } from '../../shared/store-manager/store-items';
import { Pager } from '../../common/pager';
import { KeyName } from '../../common/key-name';
import {
    TableColumnSetting, TableActionItem, TableActionOptions,
    TableCellFormatOptions, TableColumnSortOrderOptions, TableColumnSortOptions, TableClickRowItem
} from '../../shared/data-table/data-table.types';
import { EvVehiclesResultItem, EvVehiclesRequest, EvVehiclesResult } from './ev-vehicles.types';
import { AssetGroupTypes, AssetGroupItem } from '../../admin/asset-groups/asset-groups.types';
import { TripEvStatuses } from '../../common/entities/status-types';
import { MapCustomOptionsGroups } from '../../google-maps/custom-options/custom-options.types';
import { AssetExtended, AssetTypes } from '../../common/entities/entity-types';
import { MapModeOptions } from '../../google-maps/types/map.types';
import { BatteryLevel } from '../../common/ev-battery-status';
import {
    MoreOptionsMenuTypes,
    MoreOptionsMenuOptions,
    MoreOptionsMenuOption
} from '../../shared/more-options-menu/more-options-menu.types';
import { EvChargingStatus } from '../../charge-point-manager/charging-status.types';


@Component({
    selector: 'ev-vehicles',
    templateUrl: 'ev-vehicles.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['ev-vehicles.less']
})

export class EvVehiclesComponent implements AfterViewInit, OnInit, OnDestroy {
    @ViewChildren('myList') public myList: QueryList<ElementRef>;
    public loading: boolean;
    public data: EvVehiclesResultItem[] = [] as EvVehiclesResultItem[];
    public tableSettings: TableColumnSetting[];
    public request: EvVehiclesRequest = {} as EvVehiclesRequest;
    public termControl = new FormControl();
    public pager: Pager = new Pager();
    public message: string;
    public listViewShow: boolean = false;
    public groupTypes: any = AssetGroupTypes;
    public tripEvStatusesArray: KeyName[];
    public selectedAssets: AssetExtended[] = [];
    public tableColumnSortOrderOptions = TableColumnSortOrderOptions;

    public mapModes: any = MapModeOptions;
    public customOptionsGroups = MapCustomOptionsGroups;
    public pageOptions: MoreOptionsMenuOptions[];

    private subscriber: Subscription;
    private myListSubscriber: Subscription;
    private refreshIntervalHandler: any;

    constructor(
        public userOptions: UserOptionsService,
        private translateService: CustomTranslateService,
        private service: EvVehiclesService,
        private storeManager: StoreManagerService,
        private router: Router,
        private unsubscribe: UnsubscribeService,
        private snackBar: SnackBarService,
        private resizeService: ResizeService,
        private iconRegistry: MatIconRegistry,
        private sanitizer: DomSanitizer) {
            // register svg icons used in this page
            iconRegistry.addSvgIcon('deselect_icon', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/deselect.svg'));

            this.setPageOptions();

            this.setTableSettings(translateService);

            this.termControl
                .valueChanges
                .pipe(
                    debounceTime(500),
                    distinctUntilChanged(),
                    switchMap((term) => {
                        this.request.searchTerm = term;
                        this.get(true);
                        return of([]);
                    })
                )
                .subscribe();
    }

    public ngOnInit() {
        // savedData contains all recquest's options
        const savedData = this.storeManager.getOption(StoreItems.evVehicles) as EvVehiclesRequest;
        if (savedData) {
            this.request = savedData;
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }
        this.setStatusDDL();
        this.get(true);
    }

    public ngAfterViewInit() {

        this.myListSubscriber = this.myList.changes.subscribe((res) => {
            if (this.pager.pageNumber < this.pager.totalPages) {
                const parentULHeight = res.last.nativeElement.offsetHeight;
                const childrenLIHeight = res.last.nativeElement.children.length > 0
                    ? res.last.nativeElement.children[0].clientHeight * res.last.nativeElement.children.length
                    : 0;

                if (childrenLIHeight === 0) {
                    return;
                }
                res.last.nativeElement.style.height = childrenLIHeight <= parentULHeight
                    ? childrenLIHeight - (childrenLIHeight * 0.19) + 'px'
                    : '';
            }
        });
    }

    public ngOnDestroy() {
        this.unsubscribe.removeSubscription([
            this.subscriber,
            this.myListSubscriber
        ]);
        this.setAutoRefresh(false);
    }

    // =========================
    // EVENT HANDLERS
    // =========================

    public onSelectPageOption(item: MoreOptionsMenuOption): void {
        if (item.id === 'autorefresh') {
            this.setAutoRefresh(item.checkboxValue);
        }
    }


    public onViewSelection() {
        setTimeout(() => {
            this.resizeService.trigger();
        }, 1000);
    }

    // =========================
    // SEARCH TERM
    // =========================

    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.termControl.setValue(null);
    }


    // =========================
    // VEHICLE GROUPS
    // =========================

    public onChangeGroup(group: AssetGroupItem) {
        this.request.vehicleGroupID = group ? group.id : null;
        this.get(true);
    }

    // =========================
    // TRIP/EV STATUSES
    // =========================

    public onSelectTripEvStatus(id?: number) {
        this.get(true);
    }

    // =========================
    // TABLE
    // =========================

    // fetch data
    public loadMore() {
        if (this.pager.pageNumber < this.pager.totalPages && this.loading === false) {
            this.pager.addPage();
            this.get(false);
        }
    }

    // When the user selected a column to sort (from list view)
    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }

    public getChargingStatus(item: EvVehiclesResultItem) {
        // should show only when vehicle status is Charging
        let label = '';
        if (item.chargeLocation) {
            label = EvChargingStatus.formatChargingStatus(item.chargingStatus, this.translateService);
        }
        return label;
    }

    public getChargeLocation(item: EvVehiclesResultItem) {
        return item.chargeLocation;
    }


    // =========================
    // MAP
    // =========================

    public onAssetSelect(e: Event, record: EvVehiclesResultItem) {
        // if (e.srcElement.nodeName === 'MAT-ICON' || e.srcElement.nodeName === 'BUTTON' ) {
        //     return;
        // }

        console.log('event', e, record);

        if (record.vehicleStatus === TripEvStatuses.NoStatus) {
            // show warning that cannot show on map
            this.snackBar.open('map.cannot_display_warning', 'form.actions.ok');
            return;
        }

        // clone table
        // WE NEED THIS TO CAUSE THE CALL OF ngOnChanges
        // Angular change detection only checks object identity, not object content.
        // Inserts or removals are therefore not detected.
        let currentTable = this.selectedAssets.map((x) => Object.assign({}, x));
        this.selectedAssets = [];
        // if the user selected the same event, then he wants to clear previous selection
        if (currentTable.find((x) => x.vehicleID === record.vehicleID)) {
            currentTable = currentTable.filter((x) => {
                return x.vehicleID !== record.vehicleID;
            });
        } else {
            currentTable.push(record as AssetExtended);
        }

        this.selectedAssets = currentTable;
    }

    public deselectAllAssets() {
        this.selectedAssets = [];
    }

    public isAssetSelected(event: EvVehiclesResultItem) {
        return this.selectedAssets.find((x) => x.vehicleID === event.vehicleID) ? true : false;
    }

    public toggleDetails(vehicleID: number) {
        const detailsID = 'details_' + vehicleID;
        const elm = document.getElementById(detailsID);
        elm.style.display =  (!elm.style.display || elm.style.display === 'block') ? 'none' : 'block';
    }


     // When the user selected a column to sort (from map view)
     public onMapSortSelection(column: TableColumnSetting) {
        this.tableSettings.forEach((c) => {
            if (c.sorting) {
                c.sorting.selected = false;
            }
        });
        this.tableSettings.find((x) => x.header === column.header).sorting.selected = true;
            // change the sort order
        if (column.sorting.order === TableColumnSortOrderOptions.asc) {
            column.sorting.order = TableColumnSortOrderOptions.desc;
        } else {
            column.sorting.order = TableColumnSortOrderOptions.asc;
        }
        // select the current column
        column.sorting.selected = true;
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }

    public formatIconStatus(item: EvVehiclesResultItem) {
        let className = '';
        switch (item.vehicleStatus) {
            case TripEvStatuses.NoStatus:
                className = 'unknown';
                break;
            case TripEvStatuses.Driving:
                className = 'driving';
                break;
            case TripEvStatuses.Parked:
                className = 'parked';
                break;
            case TripEvStatuses.Charging:
                className = 'charging';
                break;
            case TripEvStatuses.Offline:
            default:
                className = 'offline';
                break;
        }
        className += ' ' + BatteryLevel.map(item.batteryLevel);

        return `<i class="ev-icon ev-icon-${className}"></i>`;
    }

    public formatChargeStatus(item: EvVehiclesResultItem) {
        let className = '';
        switch (item.vehicleStatus) {
            case TripEvStatuses.NoStatus:
                className = 'unknown';
                break;
            case TripEvStatuses.Driving:
                className = 'driving';
                break;
            case TripEvStatuses.Parked:
                className = 'parked';
                break;
            case TripEvStatuses.Charging:
                className = 'charging';
                break;
            case TripEvStatuses.Offline:
            default:
                className = 'offline';
                break;
        }
        className += ' ' + BatteryLevel.map(item.batteryLevel);

        return `<i class="ev-icon ev-icon-${className}"></i>`;
    }

    // =========================
    // PRIVATE
    // =========================

    // Fetch driver summary data
    private get(resetPageNumber: boolean) {
        if (this.loading) {
            return;
        }
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.pager.pageSize = 20;
            this.data = new Array<EvVehiclesResultItem>();
        }

        this.message = '';
        this.loading = true;

        this.request.pageSize = this.pager.pageSize;
        this.request.pageNumber = this.pager.pageNumber;

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.subscriber);

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.evVehicles, this.request);

        // then start the new request
        this.subscriber = this.service
            .get(this.request)
            .subscribe((response: EvVehiclesResult) => {
                this.loading = false;
                if (resetPageNumber === true) {
                    this.data = response && response.results
                        ? response.results
                        : [];
                } else {
                    this.data = this.data.concat(response.results);
                }

                // refresh the selected list
                this.data.forEach((e) => {
                    e.isEv = true;
                    e.assetType = AssetTypes.Vehicle;
                    // if (refresh === true && vehIDs.find((vid) => vid === e.vehicleID)) {
                    //     this.selectedAssets.push(e);
                    // }
                });

                if (!this.data || this.data.length === 0) {
                    this.message = 'data.empty_records';
                    return;
                }

                // Get the paging info
                this.pager.pageNumber = response.pageNumber;
                this.pager.pageSize =  response.pageSize;
                this.pager.totalPages = response.totalPages;
                this.pager.totalRecords = response.totalRecords;

                this.resizeService.trigger();
            },
            (err) => {
                this.loading = false;
                this.data = [];
                this.message = 'data.error';
            });
    }

    private setTableSettings(translateService: CustomTranslateService) {
        this.tableSettings = [
            {
                primaryKey: 'status',
                header: 'ev_operations.list_options.status',
                format: TableCellFormatOptions.custom,
                formatFunction: this.formatIconStatus,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'vehicleLabel',
                header: 'ev_charging_monitor.list_options.vehicle',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'range',
                header: 'ev_charging_monitor.list_options.range',
                format: TableCellFormatOptions.distance,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'soC',
                header: 'ev_charging_monitor.list_options.soc',
                format: TableCellFormatOptions.percentage,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'chargeLocation',
                header: 'ev_charging_monitor.list_options.charge_location',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null),
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvVehiclesResultItem) => {
                    return this.getChargeLocation(item);
                },
            },
            // {
            //    primaryKey: 'chargerID',
            //    header: 'ev_charging_monitor.list_options.charger_id',
            //    sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            // },
            {
                primaryKey: 'chargingStatus',
                header: 'ev_charging_monitor.list_options.charging_status',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: EvVehiclesResultItem) => {
                    return this.getChargingStatus(item);
                },
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            }
        ];
    }

    private setStatusDDL() {
        this.tripEvStatusesArray = [
            {
                id: null,
                name: 'All',
                term: 'assets.groups.all',
                enabled: true
            },
            // {
            //     id: TripEvStatuses.Offline,
            //     name: this.translateService.instant('ev_operations.tripEvStatuses.offline'),
            //     enabled: true
            // },
            // {
            //     id: TripEvStatuses.Private,
            //     name: this.translateService.instant('ev_operations.tripEvStatuses.private'),
            //     enabled: true
            // },
            {
                id: TripEvStatuses.Driving,
                name: 'Driving',
                term: 'ev_operations.tripEvStatuses.driving',
                enabled: true
            },
            {
                id: TripEvStatuses.Parked,
                name: 'Parked',
                term: 'ev_operations.tripEvStatuses.parked',
                enabled: true
            },
            {
                id: TripEvStatuses.NoStatus,
                name: 'No status',
                term: 'ev_operations.tripEvStatuses.no_status',
                enabled: true
            },
            {
                id: TripEvStatuses.Charging,
                name: 'Charging',
                term: 'ev_operations.tripEvStatuses.charging',
                enabled: true
            },
            {
                id: TripEvStatuses.BatteryLow,
                name: 'Battery low',
                term: 'ev_operations.tripEvStatuses.battery_low',
                enabled: true
            },
            {
                id: TripEvStatuses.BatteryMedium,
                name: 'Battery medium',
                term: 'ev_operations.tripEvStatuses.battery_medium',
                enabled: true
            },
            {
                id: TripEvStatuses.BatteryFull,
                name: 'Battery full',
                term: 'ev_operations.tripEvStatuses.battery_full',
                enabled: true
            }
        ];

        this.tripEvStatusesArray.forEach((te) => {
            this.translateService
                .get(te.term)
                .subscribe((t) => {
                    te.name = t;
                });
        });
    }


    private setPageOptions(): void {
        this.pageOptions = [
            {
                term: 'data.autorefresh',
                options: [
                    {
                        id: 'autorefresh',
                        term: 'Autorefresh',
                        enabled: true,
                        type: MoreOptionsMenuTypes.checkbox,
                        checkboxValue: true,
                    }
                ]
            }
        ];
        this.setAutoRefresh(true);
    }

    private setAutoRefresh(autoRefreshEnabled: boolean) {
        if (autoRefreshEnabled === true) {
            // Datatable refreshes every RefreshInterval minute
            // if period type is today to fetch the new events
            const user = this.userOptions.getUser();
            this.refreshIntervalHandler = setInterval(() => {
                this.get(true);
        }, (user.refreshIntervalSecs * 1000));
        } else {
            if (this.refreshIntervalHandler) {
                clearInterval(this.refreshIntervalHandler);
            }
        }
    }
}
