﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import { EvVehiclesRequest, EvVehiclesResult } from './ev-vehicles.types';

@Injectable()
export class EvVehiclesService {
    private baseApiURL: string;

    constructor(
        private authHttp: AuthHttp,
        private configService: Config) {
            this.baseApiURL = configService.get('apiUrl') + '/api/EvVehicles';
        }

    public get(req: EvVehiclesRequest): Observable<EvVehiclesResult> {
        const obj = this.authHttp.removeObjNullKeys({
            VehicleGroupID: req.vehicleGroupID ? req.vehicleGroupID.toString() : null,
            PageNumber: req.pageNumber.toString(),
            PageSize: req.pageSize.toString(),
            Sorting: req.sorting,
            SearchTerm: req.searchTerm,
            Status: req.status ? req.status.toString() : null
        });

        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.baseApiURL, { params });            
    }
}
