// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { SharedModule } from '../../shared/shared.module';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { AssetGroupsModule } from '../../common/asset-groups/asset-groups.module';
import { EvVehiclesRoutingModule } from './ev-vehicles-routing.module';
import { MapsModule } from '../../google-maps/maps.module';

// COMPONENTS
import { EvVehiclesComponent } from './ev-vehicles.component';

// SERVICES
import { EvVehiclesService } from './ev-vehicles.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/ev-operations/ev-vehicles/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        HttpClientJsonpModule,
        HttpClientModule,
        SharedModule,
        MapsModule,
        AssetGroupsModule,
        EvVehiclesRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        EvVehiclesComponent
    ],
    providers: [
        EvVehiclesService
    ]
})
export class EvVehiclesModule { }
