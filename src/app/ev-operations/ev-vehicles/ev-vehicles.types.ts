import { PagerRequest, PagerResults } from '../../common/pager';
import { AssetExtended } from '../../common/entities/entity-types';
import { TripEvStatuses } from '../../common/entities/status-types';
import { EvChargingStatuses } from '../../charge-point-manager/charging-status.types';

export interface EvVehiclesRequest extends PagerRequest {
    vehicleGroupID?: number;
    searchTerm: string;
    status?: TripEvStatuses;
}


export interface EvVehiclesResultItem extends AssetExtended {
    // lineID: string;
    chargeLocation: string;
    chargerID: string;
    chargingStatus: EvChargingStatuses;
    soC?: number;
    range?: number;
}

export interface EvVehiclesResult extends PagerResults {
    results: EvVehiclesResultItem[];
}


export interface EvVehiclesExtraInfo {
    flexWidth?: number;
    listView: boolean;
}
