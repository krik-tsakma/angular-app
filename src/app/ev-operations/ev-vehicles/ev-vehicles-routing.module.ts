﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { EvVehiclesComponent } from './ev-vehicles.component';

const EvOperationsRoutes: Routes = [
    {
        path: '',
        component: EvVehiclesComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(EvOperationsRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class EvVehiclesRoutingModule { }
