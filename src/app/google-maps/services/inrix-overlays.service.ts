// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

// RXJHS
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { EnhancedMapsOptions } from '../../auth/user';
import { MapCustomOptionsService } from '../custom-options/custom-options.service';
import { MapsLoaderService } from './maps-loader.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { ProgressBarService } from './progress-bar.service';
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import { MapCustomOption, MapCustomOptions } from '../custom-options/custom-options.types';
import { xml2json } from '../../common/xml2json';
import {
     MapCustomOverlay, TrafficLayerTypes, TrafficHtmlTemplates,
     TrafficCamera, TrafficIncident, TrafficIncidentTerms
} from '../types/custom-map.types';
import { GoogleMap } from '../types/map.types';

// MOMENT
import moment from 'moment';

declare const MarkerClusterer;

@Injectable()
export class InrixOverlaysService {
    private inrixProxyURL: string;
    private map: google.maps.Map;
    private settings: EnhancedMapsOptions;
    private trafficControl: HTMLElement;
    private xml2jsonParser = new xml2json();
    private infowindow: any;
    private speedLabel: string;
    private weatherLabel: string;
    private constructionLabel: string;
    private congestionLabel: string;
    private accidentLabel: string;
    private incidentTerms: TrafficIncidentTerms;
    private get dateFormat(): string {
        return this.userOptions.localOptions.short_date + ' HH:mm';
    }
    private incidentsSubscription: any;
    private weatherIncidentsSubscription: any;
    private camerasSubscription: any;

    private markers: { [id: number]: google.maps.Marker[] } = {
        [TrafficLayerTypes.camera]: [],
        [TrafficLayerTypes.construction]: [],
        [TrafficLayerTypes.congestion]: [],
        [TrafficLayerTypes.accident]: [],
        [TrafficLayerTypes.weather]: []
    };

    private markerClusterers: { [id: number]: any } = {
        [TrafficLayerTypes.camera]: null,
        [TrafficLayerTypes.construction]: null,
        [TrafficLayerTypes.congestion]: null,
        [TrafficLayerTypes.accident]: null,
        [TrafficLayerTypes.weather]: null
    };

    private eventListeners: { [id: number]: google.maps.MapsEventListener[] } = {
        [TrafficLayerTypes.camera]: [],
        [TrafficLayerTypes.construction]: [],
        [TrafficLayerTypes.congestion]: [],
        [TrafficLayerTypes.accident]: [],
        [TrafficLayerTypes.weather]: []
    };

    constructor(private userOptions: UserOptionsService,
                private mapsLoaderService: MapsLoaderService,
                private customOptionsService: MapCustomOptionsService,
                private progressBarService: ProgressBarService,
                private translate: CustomTranslateService,
                private configService: Config,
                private authHttp: AuthHttp,
                private httpClient: HttpClient) {

        this.inrixProxyURL = configService.get('apiUrl') + '/api/InrixProxy';
        this.settings = userOptions.getUser().enhancedMapsOptions;

        if (!this.settings) {
            return;
        }

        translate.get(['map_tooltip.information.speed',
                        'map.custom_options.accident',
                        'map.custom_options.construction',
                        'map.custom_options.congestion',
                        'map.custom_options.weather',
                        'map_tooltip.incident.location',
                        'map_tooltip.incident.when',
                        'map_tooltip.incident.till',
                        'map_tooltip.incident.length',
                        'map_tooltip.incident.delay_from_free_flow',
                        'map_tooltip.incident.delay_from_typical_flow',
                        'map_tooltip.incident.description',
                    ]).subscribe((t: any) => {
            this.speedLabel = t['map_tooltip.information.speed'];
            this.congestionLabel = t['map.custom_options.congestion'];
            this.constructionLabel = t['map.custom_options.construction'];
            this.weatherLabel = t['map.custom_options.weather'];
            this.accidentLabel = t['map.custom_options.accident'];

            this.incidentTerms = {
                location: t['map_tooltip.incident.location'],
                when: t['map_tooltip.incident.when'],
                till:  t['map_tooltip.incident.till'],
                length: t['map_tooltip.incident.length'],
                delayFromFreeFlow: t['map_tooltip.incident.delay_from_free_flow'],
                delayFromTypicalFlow: t['map_tooltip.incident.delay_from_typical_flow'],
                description: t['map_tooltip.incident.description'],
            } as TrafficIncidentTerms;
        });

        // when map finished loading
        mapsLoaderService.map.subscribe((mapInstace: GoogleMap) => {
            this.map = mapInstace.map;
            this.infowindow = new google.maps.InfoWindow();
            google.maps.event.addListener(mapInstace.map, 'dragend', () => { // idle
               // if traffic data layers exist, refetch them
                setTimeout(() => {
                    this.getIncidentsInRegion();
                    this.getWeatherIncidentsInRegion();
                    this.getCamerasInRegion();
                }, 1000);
            });

            // add the traffic control
            this.trafficControl = document.createElement('div');
            this.trafficControl.classList.add('InrixLegend');
            this.trafficControl.style.display = 'none';
            const buckets = userOptions.getUser().enhancedMapsOptions.speedBuckets;
            this.trafficControl.innerHTML =  TrafficHtmlTemplates.speedLegend(buckets, this.speedLabel, userOptions.metrics.speed);
            this.map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(this.trafficControl);
        });

        // when map finished zooming
        mapsLoaderService.zoomChange.subscribe(() => {
            this.getIncidentsInRegion();
            this.getWeatherIncidentsInRegion();
            this.getCamerasInRegion();
        });

        // when a user changed a custom option
        customOptionsService.optionChanged.subscribe((option: MapCustomOption) => {
            this.onChangeCustomOption(option);
        });
    }


   /**
    * Callback for when the user has selected an option to show/hide
    * @param option: the option used
    */
    private onChangeCustomOption(option: MapCustomOption) {
        switch (option.id) {
            case MapCustomOptions.traffic: {
                this.removeSpeedOverlay();
                if (option.checked === true) {
                    this.addSpeedOverlay();
                }
                break;
            }
            case MapCustomOptions.congestion:
            case MapCustomOptions.accident:
            case MapCustomOptions.construction: {
                this.getIncidentsInRegion();
                break;
            }
            case MapCustomOptions.roadWeather: {
                this.getWeatherIncidentsInRegion();
                break;
            }
            case MapCustomOptions.camera: {
                this.getCamerasInRegion();
                break;
            }
        }
    }


    // ======================
    // TRAFFIC OVERLAY
    // ======================

    // https://developers.google.com/maps/documentation/javascript/examples/maptype-overlay
    private addSpeedOverlay() {
        const overlay = new MapCustomOverlay(this.map.getProjection(), this.settings.authToken, this.settings.mapsBaseURL);
        this.map.overlayMapTypes.insertAt(0, overlay);
        this.trafficControl.style.display = 'block';
    }


    private removeSpeedOverlay() {
        this.trafficControl.style.display = 'none';
        const om = this.map.overlayMapTypes;
        om.forEach((o, index) => {
            om.removeAt(index);
        });
    }


    // ======================
    // INCIDENTS
    // ======================

   /**
    * Get all traffic cameras for the region defined by map's northEastCorner and southWestCorner coordinates
    * @param ne: the north east corner of the map
    * @param sw: the south east corner of the map
    * http://devzonedocs.inrix.com/v3/docs/index.php/cs/incidentelement
    */
    private getIncidentsInRegion() {
        if (this.hasTokenExpired() === true) {
            console.log('token refresh');
            return;
        }

        if (this.map.getZoom() < 9) {
            this.clearTrafficMarkers();
            return;
        }

        // 1=Construction, 2=Event, 3=Flow, 4=Incident, 5=RoadWeather, 6 = Police
        const types = [];
        if (this.customOptionsService.getOptionStatus(MapCustomOptions.construction) === true) {
            types.push('Construction');
        }
        if (this.customOptionsService.getOptionStatus(MapCustomOptions.accident) === true) {
            types.push('Incidents');
        }
        if (this.customOptionsService.getOptionStatus(MapCustomOptions.congestion) === true) {
            types.push('Flow');
        }

        if (types.length === 0) {
            this.clearTrafficMarkers();
            return;
        }

        const bounds = this.map.getBounds();
        const ne = bounds.getNorthEast();
        const sw = bounds.getSouthWest();
        const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/xml'});
        const params = new HttpParams({
            fromObject: {
                action: 'GetIncidentsInBox',
                token: this.settings.authToken,
                corner1: this.getCorner(ne),
                corner2: this.getCorner(sw),
                incidentType: types.join(','),
                locrefmethod: 'XD,TMC',
                incidentoutputfields: 'all',
                locale: this.userOptions.getUser().culture,
                units: this.userOptions.getUser().measurementSystem === 'metric' ? '1' : '0'
            }
        });

        this.unsubscribeService(this.incidentsSubscription);
        this.progressBarService.startLoading();

        this.incidentsSubscription = this.authHttp
            .get(this.inrixProxyURL, { headers, params, responseType: 'text' })
            .pipe(
                map((res) => JSON.parse(this.xml2jsonParser.get(res, '  '))),
                catchError((error) => {
                    this.progressBarService.stopLoading();
                    return throwError(error);
                })
            )
            .subscribe((jsonData) => {
                this.progressBarService.stopLoading();

                const incidents: TrafficIncident[] = new Array<TrafficIncident>();
                if (jsonData.Inrix && jsonData.Inrix.Incidents && jsonData.Inrix.Incidents.Incident) {
                    let incidentData = [];
                    if (jsonData.Inrix.Incidents.Incident.length > 0) {
                        incidentData = jsonData.Inrix.Incidents.Incident;
                    } else {
                        incidentData.push(jsonData.Inrix.Incidents.Incident);
                    }

                    incidentData.forEach((inc) => {
                        if (inc['@status'] !== 'ACTIVE') {
                            return;
                        }

                        // console.log(inc)
                        const tt = this.getType(inc['@type']);
                        const incident = {
                            id: inc['@id'],
                            status: inc['@status'],
                            latitude: inc['@latitude'],
                            longitude: inc['@longitude'],
                            severity: inc['@severity'],
                            startTime: moment(inc['@startTime']).format(this.dateFormat),
                            endTime: moment(inc['@endTime']).format(this.dateFormat),
                            type: tt.type,
                            title: tt.title,
                            location: this.getIncidentLocationDescription(inc.ParameterizedDescription),
                            distance: inc.DelayImpact['@distance'] + this.userOptions.metrics.distance,
                            description: inc.FullDesc['#text'],
                            freeFlowDelay: this.formatIncidentDuration(inc.DelayImpact['@fromFreeFlowMinutes']),
                            typicalFlowDelay: this.formatIncidentDuration(inc.DelayImpact['@fromTypicalFlowMinutes']),
                        } as TrafficIncident;
                        incidents.push(incident);
                    });
                }

                this.drawIncidents(incidents);
            });
    }

   /**
    * Show incident markers on the map
    * @param cameras: the results from INRIX api
    */
    private drawIncidents(incidents: TrafficIncident[]) {
        this.clearTrafficMarkers();

        if (!incidents) {
            return;
        }
        incidents.forEach((inc) => {
            const marker = this.getMarker(inc.type, inc.latitude, inc.longitude);
            const clickListener = marker.addListener('click', () => {
                const txt = TrafficHtmlTemplates.incident(inc, this.incidentTerms);
                this.infowindow.setContent(txt);
                this.infowindow.open(this.map, marker);
            });

            this.markers[inc.type].push(marker);
            this.eventListeners[inc.type].push(clickListener);

        });
        this.setupMarkerClusterer(TrafficLayerTypes.construction);
        this.setupMarkerClusterer(TrafficLayerTypes.congestion);
        this.setupMarkerClusterer(TrafficLayerTypes.accident);
    }

    private getIncidentLocationDescription(location) {
        if (!location) {
            return '';
        }

        let loc = location.Direction + ' ' + location.RoadName;
        if (location.Position1) {
            loc += ' ' + location.Position1;
        }
        if (location.Crossroad1) {
            loc += ' ' + (location.Crossroad1['#text'] ? location.Crossroad1['#text'] : location.Crossroad1);
        }

        if (location.Position2) {
            loc += ' ' + location.Position2;
        }
        if (location.Crossroad2) {
            loc += ' ' + (location.Crossroad2['#text'] ? location.Crossroad2['#text'] : location.Crossroad2);
        }

        return loc;

    }

    private formatIncidentDuration(minutes) {
        const timeFormatted = moment.utc(moment.duration(minutes, 'minutes')
                                    .asMilliseconds())
                                    .format('HH:mm:ss');
        return timeFormatted;
    }

    private getType(inrixType: string) {
        // 1=Construction, 2=Event, 3=Flow/Congestion, 4=Incident/Accident, 5=Road Weather, 6 = Police
        let type = TrafficLayerTypes.congestion;
        let title = '';
        switch (inrixType) {
            case '4':
                type = TrafficLayerTypes.accident;
                title = this.accidentLabel;
                break;
            case '1':
                type = TrafficLayerTypes.construction;
                title = this.constructionLabel;
                break;
            case '3':
                type = TrafficLayerTypes.congestion;
                title = this.congestionLabel;
                break;
            case '5':
                type = TrafficLayerTypes.weather;
                title = this.weatherLabel;
                break;
        }

        return { type: type, title: title };
    }

    private clearTrafficMarkers() {
        this.clearMarkers(TrafficLayerTypes.congestion);
        this.clearMarkers(TrafficLayerTypes.accident);
        this.clearMarkers(TrafficLayerTypes.construction);
    }

    // ======================
    // WEATHER INCIDENTS
    // ======================

   /**
    * Get all traffic cameras for the region defined by map's northEastCorner and southWestCorner coordinates
    * @param ne: the north east corner of the map
    * @param sw: the south east corner of the map
    * http://devzonedocs.inrix.com/v3/docs/index.php/cs/incidentelement
    */
    private getWeatherIncidentsInRegion() {
        if (this.hasTokenExpired() === true) {
            console.log('token refresh');
            return;
        }

        if (this.map.getZoom() < 7) {
            this.clearMarkers(TrafficLayerTypes.weather);
            return;
        }

        if (this.customOptionsService.getOptionStatus(MapCustomOptions.roadWeather) === false) {
            this.clearMarkers(TrafficLayerTypes.weather);
            return;
        }

        const bounds = this.map.getBounds();
        const ne = bounds.getNorthEast();
        const sw = bounds.getSouthWest();

        const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/xml' });
        const params = new HttpParams({
            fromObject: {
                action: 'GetXDIncidentsInBox',
                token: this.settings.authToken,
                corner1: this.getCorner(ne),
                corner2: this.getCorner(sw),
                incidentType: 'RoadWeather',
                locrefmethod: 'XD',
                incidentoutputfields: 'all',
                locale: this.userOptions.getUser().culture,
                units: this.userOptions.getUser().measurementSystem === 'metric' ? '1' : '0'
            }
        });


        this.unsubscribeService(this.weatherIncidentsSubscription);
        this.progressBarService.startLoading();
        this.weatherIncidentsSubscription = this.authHttp
            .get(this.inrixProxyURL, { headers, params, responseType: 'text' })
            .pipe(
                map((res) => JSON.parse(this.xml2jsonParser.get(res, '  '))),
                catchError((error) => {
                    this.progressBarService.stopLoading();
                    return throwError(error);
                })
            )
            .subscribe((jsonData) => {
                this.progressBarService.stopLoading();

                const incidents: TrafficIncident[] = new Array<TrafficIncident>();
                if (jsonData.Inrix && jsonData.Inrix.XDIncidents && jsonData.Inrix.XDIncidents.XDIncident) {
                    const responseData = jsonData.Inrix.XDIncidents.XDIncident;
                    let incidentData = [];
                    if (responseData.length > 0) {
                        incidentData = responseData;
                    } else {
                        incidentData.push(responseData);
                    }

                    incidentData.forEach((inc) => {
                        if (inc['@status'] !== 'active') {
                            return;
                        }
                        const tt = this.getType(inc['@type']);
                        const descriptions = inc.Descriptions.Desc as string[];
                        const longDescription = descriptions.find((d) => d['@type'] === 'long');
                        let description = '';
                        if (longDescription) {
                            description = longDescription['#text'];
                        }
                        const incident = {
                            id: inc['@id'],
                            status: inc['@status'],
                            latitude: inc['@latitude'],
                            longitude: inc['@longitude'],
                            severity: inc['@severity'],
                            startTime: moment(inc.Schedule.OccurrenceStartTime).format(this.dateFormat),
                            endTime: moment(inc.Schedule.OccurrenceEndTime).format(this.dateFormat),
                            type: tt.type,
                            title: tt.title,
                            location: this.getIncidentLocationDescription(inc.Descriptions.ParameterizedDescription),
                            distance: inc.DelayImpact['@distance'] + this.userOptions.metrics.distance,
                            description,
                            freeFlowDelay: this.formatIncidentDuration(inc.DelayImpact['@fromFreeFlowMinutes']),
                            typicalFlowDelay: this.formatIncidentDuration(inc.DelayImpact['@fromTypicalFlowMinutes']),
                        } as TrafficIncident;
                        incidents.push(incident);
                    });
                }

                this.drawWeatherIncidents(incidents);
            });
    }


   /**
    * Show incident markers on the map
    * @param cameras: the results from INRIX api
    */
    private drawWeatherIncidents(incidents: TrafficIncident[]) {
        this.clearMarkers(TrafficLayerTypes.weather);

        if (!incidents) {
            return;
        }
        incidents.forEach((inc) => {
            const marker = this.getMarker(inc.type, inc.latitude, inc.longitude);
            const clickListener = marker.addListener('click', () => {
                const txt = TrafficHtmlTemplates.incident(inc, this.incidentTerms);
                this.infowindow.setContent(txt);
                this.infowindow.open(this.map, marker);
            });

            this.markers[inc.type].push(marker);
            this.eventListeners[inc.type].push(clickListener);

        });
        this.setupMarkerClusterer(TrafficLayerTypes.weather);
    }

    // ======================
    // TRAFFIC CAMERAS
    // ======================

   /**
    * Get all traffic cameras for the region defined by map's northEastCorner and southWestCorner coordinates
    * @param ne: the north east corner of the map
    * @param sw: the south east corner of the map
    */
    private getCamerasInRegion() {
        if (this.hasTokenExpired() === true) {
            console.log('token refresh');
            return;
        }

        if (this.map.getZoom() < 9) {
            this.clearMarkers(TrafficLayerTypes.camera);
            return;
        }

        if (this.customOptionsService.getOptionStatus(MapCustomOptions.camera) === false) {
            this.clearMarkers(TrafficLayerTypes.camera);
            return;
        }

        const bounds = this.map.getBounds();
        const ne = bounds.getNorthEast();
        const sw = bounds.getSouthWest();

        // http://devzonedocs.inrix.com/v3/docs/index.php/cs/cameraelement/
        // "http://eu.api.inrix.com/Traffic/Inrix.ashx?Action=GetTrafficCamerasInBox&
        // token=gMdUWvtntCi3atMR*KLBtT4HCD8dFqWeHtiibBZz4gY|&Corner1=47.63905031375326
        // |-122.2366032595215&Corner2=47.43905031375326|-122.0366032595215&Count=40&Locale=en-us&Sort=1"


        const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/xml' });
        const params = new HttpParams({
            fromObject: {
                action: 'GetTrafficCamerasInBox',
                token: this.settings.authToken,
                corner1: this.getCorner(ne),
                corner2: this.getCorner(sw)
            }
        });

        this.unsubscribeService(this.camerasSubscription);
        this.progressBarService.startLoading();
        this.camerasSubscription = this.authHttp
            .get(this.inrixProxyURL, { headers, params, responseType: 'text' })
            .pipe(
                map((res) => JSON.parse(this.xml2jsonParser.get(res, '  '))),
                catchError((error) => {
                    this.progressBarService.stopLoading();
                    return throwError(error);
                })
            )
            .subscribe((jsonData) => {
                this.progressBarService.stopLoading();

                const cameras: TrafficCamera[] = new Array<TrafficCamera>();
                if (jsonData.Inrix && jsonData.Inrix.Cameras && jsonData.Inrix.Cameras.Camera) {
                    let camData = [];
                    if (jsonData.Inrix.Cameras.Camera.length > 0) {
                        camData = jsonData.Inrix.Cameras.Camera;
                    } else {
                        camData.push(jsonData.Inrix.Cameras.Camera);
                    }
                    camData.forEach((cam) => {
                        const trafficCamera = {
                            id: cam['@id'],
                            name: cam.Name,
                            outOfService: cam.Status['@outOfService'],
                            latitude: cam.Point['@latitude'],
                            longitude: cam.Point['@longitude'],
                            copyright: cam.License.CopyrightNotice
                        } as TrafficCamera;
                        cameras.push(trafficCamera);
                    });
                }

                this.drawCameras(cameras);
            });
    }

   /**
    * Show camera markers on the map
    * @param cameras: the results from INRIX api
    */
    private drawCameras(cameras: TrafficCamera[]) {
        this.clearMarkers(TrafficLayerTypes.camera);

        if (!cameras) {
            return;
        }
        cameras.forEach((cam) => {
            if (cam.outOfService === true) {
                 return;
            }
            const marker = this.getMarker(TrafficLayerTypes.camera, cam.latitude, cam.longitude);
            const clickListener = marker.addListener('click', () => {
                this.fetchCameraSingle(cam, marker);
            });
            this.markers[TrafficLayerTypes.camera].push(marker);
            this.eventListeners[TrafficLayerTypes.camera].push(clickListener);
        });
        this.setupMarkerClusterer(TrafficLayerTypes.camera);
    }

   /**
    * Show tooltip with camera image for a single camera marker
    * @param camera: the camera object
    * @param marker: the camera marker
    */
    private fetchCameraSingle(camera: TrafficCamera, marker: google.maps.Marker) {
        const that = this;
        this.infowindow.close();

        const params = new HttpParams({
            fromObject: {
                action: 'GetTrafficCameraImage',
                token: this.settings.authToken,
                CameraID: camera.id,
                DesiredHeight: '220',
                DesiredWidth: '235'
            }
        });

        this.progressBarService.startLoading();
        this.httpClient
            .get(this.settings.serviceBaseURL, { params, responseType: 'blob' })
            .pipe(
                catchError((error) => {
                    this.progressBarService.stopLoading();
                    return throwError(error);
                })
            )
            .subscribe((image) => {
                this.progressBarService.stopLoading();
                const reader = new FileReader();
                if (image) {
                    reader.readAsDataURL(image);
                }
                reader.addEventListener('load', () => {
                    const imageToShow = reader.result;
                    const img = TrafficHtmlTemplates.trafficCamera(camera.name, camera.copyright, imageToShow);
                    that.infowindow.setContent(img);
                    that.infowindow.open(this.map, marker);
                }, false);

            });
    }

    // ======================
    // MARKERS
    // ======================

   /**
    * Creates a marker clusterer for the specific type
    * @param type: the TrafficLayerTypes type
    */
    private setupMarkerClusterer(type: TrafficLayerTypes) {
        const clusterStyles =
        [{
            url: 'assets/marker-icons/traffic/' + TrafficLayerTypes[type] + '-cluster.svg',
            width: 51,
            height: 54,
            textColor: '#000',
            fontWeight: 'bold',
            anchorText:  [-16, 13]  // yoffset, xoffset
        }];


        this.markerClusterers[type] = new MarkerClusterer(
            this.map,
            null,
            { averageCenter: true, zoomOnClick: true, styles: clusterStyles }
        );
        this.markerClusterers[type].addMarkers(this.markers[type]);
    }

   /**
    * Creates a marker for the specific type
    * @param type: the TrafficLayerTypes type
    * @param latitude: the marker's latitude
    * @param longitude: the marker's longitude
    */
    private getMarker(type: number, latitude: number, longitude: number): google.maps.Marker {
        const svgMarker = {
            url: 'assets/marker-icons/traffic/' + TrafficLayerTypes[type] + '.svg',
            size: new google.maps.Size(42, 48)
        };
        return new google.maps.Marker({
            position: new google.maps.LatLng(latitude, longitude),
            draggable: false,
            icon: svgMarker
        });
    }

   /**
    * clear the markers from the map for a specific traffic layer type
    * @param type: the TrafficLayerTypes type
    */
    private clearMarkers(type: number) {
        if (this.markers[type]) {
            // remove markers
            this.markers[type].forEach((m) => {
                m.setMap(null);
            });
            this.markers[type] = [];

            // cleanup events
            this.eventListeners[type].forEach((e) => {
                google.maps.event.removeListener(e);
            });
            this.eventListeners[type] = [];
        }
        // destroy cluster
        if (this.markerClusterers[type]) {
            this.markerClusterers[type].clearMarkers();
            this.markerClusterers[type].setMap(null);
        }
    }

    // ======================
    // HELPERS
    // ======================

   /**
    * whether the INRIX authentication token has expired
    */
    private hasTokenExpired(): boolean {
        const expiration = new Date(Number(this.settings.expiryDate));
        return (expiration < new Date());
    }


   /**
    * corner of the region for which to get data
    */
    private getCorner(coordinates) {
        return coordinates.lat() + '|' + coordinates.lng();
    }

    private unsubscribeService(subscribe): void {
        if (subscribe) {
            subscribe.unsubscribe();
       }
   }
}
