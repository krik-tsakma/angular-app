
// FRAMEWORK
import { Injectable } from '@angular/core';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { MapsLoaderService } from './maps-loader.service';
import { TooltipService } from './tooltip.service';
import { ProgressBarService } from './progress-bar.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
import { AssetTooltipRequest, AssetTooltipResult, AssetTooltipTerms } from '../types/tooltip.types';
import { MarkerTypes, MarkerExtended } from '../types/marker.types';
import { GoogleMap } from '../types/map.types';

// MOMENT
import moment from 'moment';


@Injectable()
export class InfoWindowService {
    private get dateFormat(): string {
        return this.userOptions.localOptions.short_date + ' HH:mm';
    }
    private iw: google.maps.InfoWindow;
    private map: google.maps.Map;
    private locals: AssetTooltipTerms;

    constructor(private userOptions: UserOptionsService,
                private translate: CustomTranslateService,
                private mapsLoaderService: MapsLoaderService,
                private tooltipService: TooltipService,
                private progressBarService: ProgressBarService) {

        mapsLoaderService.map.subscribe((mapInstace: GoogleMap) => {
            this.map = mapInstace.map;
            this.iw = new google.maps.InfoWindow();
        });

        translate.get(['map_tooltip.information',
                        'map_tooltip.events',
                        'map_tooltip.information.timestamp',
                        'map_tooltip.driver',
                        'map_tooltip.vehicle',
                        'map_tooltip.information.speed',
                        'map_tooltip.event.date',
                        'map_tooltip.event.address',
                        'map_tooltip.event.message',
                        'map_tooltip.ev.soc',
                        'map_tooltip.ev.range']).subscribe((t: any) => {
                this.locals = new AssetTooltipTerms();
                this.locals.information = t['map_tooltip.information'];
                this.locals.events = t['map_tooltip.events'];
                this.locals.driver = t['map_tooltip.driver'];
                this.locals.vehicle = t['map_tooltip.vehicle'];
                this.locals.timestamp = t['map_tooltip.information.timestamp'];
                this.locals.speed = t['map_tooltip.information.speed'];
                this.locals.eventAddress = t['map_tooltip.event.address'];
                this.locals.eventMessage = t['map_tooltip.event.message'];
                this.locals.eventDate = t['map_tooltip.event.date'];
                this.locals.soc = t['map_tooltip.ev.soc'];
                this.locals.range = t['map_tooltip.ev.range'];
            });
    }

    public open(marker: MarkerExtended) {
        this.close();
        const request = {
            vehicleID: marker.point.vehicleID,
            // where marker is asset we want to fetch the last known status,
            // otherwise we clicked on a trace or event (so fetch historic data)
            timestamp: marker.point.type === MarkerTypes.Asset ? null : marker.point.timestamp
        } as AssetTooltipRequest;

        this.progressBarService.startLoading();
        this.tooltipService
            .get(request)
            .subscribe(
                (response: AssetTooltipResult) => {
                    this.progressBarService.stopLoading();

                    if (!response) {
                        return;
                    }
                  //  console.log(response)

                    const innerHtml = this.getTooltipContents(marker.point.type, response);
                    this.iw.setContent(innerHtml);
                    this.iw.open(this.map, marker);

                    // add event handlers
                    document.getElementById('tooltip-info-ckbx').onclick = () => {
                        document.getElementById('tooltip-info').classList.remove('hidden');
                        document.getElementById('tooltip-events').classList.add('hidden');
                    };

                    document.getElementById('tooltip-event-ckbx').onclick = () => {
                        document.getElementById('tooltip-info').classList.add('hidden');
                        document.getElementById('tooltip-events').classList.remove('hidden');
                    };
                },
                (err) => {
                    this.progressBarService.stopLoading();
                }
            );
    }

    public close(): void {
        this.iw.close();
    }

    private getTooltipContents(type: MarkerTypes, response: AssetTooltipResult): string {
        let contents: string = '';
        contents = `<div class="map-tooltip">
                        <div class="tooltip-header">
                            <div class="toggle-btn tab-like horizontal">
                                <input type="radio" id="tooltip-info-ckbx" name="tooltip-header" value="info-ck" checked>
                                <label for="tooltip-info-ckbx">${ this.locals.information }</label>
                            </div>
                            <div class="toggle-btn tab-like horizontal">
                                <input type="radio" id="tooltip-event-ckbx" name="tooltip-header"  value="events-ck">
                                <label for="tooltip-event-ckbx">${ this.locals.events }</label>
                            </div>
                        </div>
                        <table id="tooltip-info" class="tooltip-contents">
                            <tr>
                                <td>${ this.locals.timestamp}</td>
                                <td>${ moment(response.timestamp).format(this.dateFormat)}</td>
                            </tr>
                            <tr>
                                <td>${ this.locals.vehicle }</td>
                                <td>${ response.vehicleLabel }</td>
                            </tr>
                            <tr>
                                <td>${ this.locals.driver }</td>
                                <td>${ (response.driverLabel ? response.driverLabel : '&nbsp') }</td>
                            </tr>
                            <tr>
                                <td>${ this.locals.speed }</td>
                                <td>${ response.speed } ${ this.userOptions.metrics.speed }</td>
                            </tr>`;

        // EV Properties
        if (response.isEV && type === MarkerTypes.Asset) {
            const soc = response.soC ? (response.soC.toString() + '%') : 'N/A';
            contents += `<tr>
                            <td>${ this.locals.soc }</td>
                            <td>${ soc }</td>
                        </tr>`;
            const range = response.range ? (response.range.toString() + this.userOptions.metrics.distance) : 'N/A';
            contents += `<tr>
                            <td>${ this.locals.range }</td>
                            <td>${ range }</td>
                        </tr>`;
        }
        contents += '</table>';

        contents += '<table id="tooltip-events" class="tooltip-contents hidden">';
        if (response.events && response.events.length > 0) {
            contents += `<thead>
                            <tr>
                                <th>${ this.locals.eventDate }</th>
                                <th>${ this.locals.eventMessage }</th>
                                <th>${ this.locals.eventAddress }</th>
                            </tr>
                         </thead>
                        <tbody>`;
            response.events.forEach((event) => {
                contents += `<tr>
                                <td>${ moment(event.timestamp).format(this.dateFormat) }</td>
                                <td>${ event.description }</td>
                                <td>${ event.address }</td>
                            </tr>`;
            });
        }
        contents += `</tbody>
                    </table>
                </div>`;


        return contents;
    }
}
