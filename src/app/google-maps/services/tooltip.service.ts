// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { retryWhen, delay, timeout } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { AssetTooltipRequest, AssetTooltipResult } from '../types/tooltip.types';

@Injectable()
export class TooltipService {

    constructor(
        private authHttp: AuthHttp,
        private config: Config) {}

    public get(req: AssetTooltipRequest): Observable<AssetTooltipResult> {

        const obj = this.authHttp.removeObjNullKeys({
            VehicleID: req.vehicleID.toString(),
            Timestamp: req.timestamp
        });

        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.config.get('apiUrl') + '/api/AssetTooltip', { params })
            .pipe(
                retryWhen((error) => error.pipe(delay(2000))),
                timeout(10000)
            );
    }

}
