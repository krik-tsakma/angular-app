// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Subject } from 'rxjs';

// SERVICES
import { ScriptLoaderService } from '../../common/script-loader.service';

// TYPES
import { GoogleMap, MapZoomChange } from '../types/map.types';
/**
 * Sets up a new map
 */
@Injectable()
export class MapsLoaderService {
    public map: Subject<GoogleMap> = new Subject<GoogleMap>();
    public boundsChange: Subject<void> = new Subject<void>();
    public zoomChange: Subject<MapZoomChange> = new Subject<MapZoomChange>();

    private boundsChangedInterval: any;

    constructor(private scriptLoader: ScriptLoaderService) {
        // foo
    }

    public load(mapElm: any) {
        this.onLoadMapsLibrary(mapElm).then((map) => {
            // trigger map change to let all the other services know that map has finished loading
            this.map.next({
                map: map,
                mapID: mapElm.id
            } as GoogleMap);
            map['enableKeyDragZoom']({
                key: 'shift'
            });
            this.addZoomChangedListener(map);
            this.addBoundsChangedLister(map);

        });
    }

    private addZoomChangedListener(map) {
        const that = this;
        google.maps.event.addListener(map, 'zoom_changed', () => {
            that.zoomChange.next({ center: map.getCenter(), zoomLevel: map.getZoom() });
        });
    }

    private addBoundsChangedLister(map) {
        const that = this;
        google.maps.event.addListener(map, 'bounds_changed', () => {
            clearTimeout(that.boundsChangedInterval);
            that.boundsChangedInterval = setTimeout(() => {
                that.boundsChange.next();
            }, 1000);
         });
    }

    private onLoadMapsLibrary(mapElm: any): Promise<google.maps.Map> {
        return new Promise((resolve, reject) => {
            // init the map control
            mapElm.style.display = 'block';
            const map = new google.maps.Map(mapElm,
                {
                    mapTypeControl: true,
                    overviewMapControl: true,
                    mapTypeControlOptions: {
                        position: google.maps.ControlPosition.TOP_RIGHT,
                        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    },
                    zoomControl: true,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.RIGHT_BOTTOM
                    },
                    fullscreenControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    minZoom: 3,
                    streetViewControl: true,
                    scaleControl: true,

            });
            google.maps.event.addListenerOnce(map, 'idle', () => {
                // console.log('idle')
                // do something only the first time the map is loaded
                this.scriptLoader.load([
                    '/assets/js/map/markerwithlabel.js',
                    '/assets/js/map/markerclusterer.js',
                    '/assets/js/map/keydragzoom.js'], () => {
                        resolve(map);
                    });
            });

        });
    }
}
