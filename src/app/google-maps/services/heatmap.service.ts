// FRAMEWORK
import { Injectable } from '@angular/core';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { MapsLoaderService } from './maps-loader.service';
import { ProgressBarService } from './progress-bar.service';

// TYPES
import { Asset } from '../../common/entities/entity-types';
import { GoogleMap } from '../types/map.types';

@Injectable()
export class HeatmapService {
    private map: google.maps.Map;
    private heatmap: google.maps.visualization.HeatmapLayer;
    private bounds: google.maps.LatLngBounds;

    constructor(private userOptions: UserOptionsService,
                private mapsLoaderService: MapsLoaderService,
                private progressBarService: ProgressBarService) {

        // when map finished loading
        mapsLoaderService.map.subscribe((mapInstance: GoogleMap) => {
            this.map = mapInstance.map;
        });
    }

    public set(assets: Asset[]): void {
        this.clear();
        if (!assets || assets.length === 0) {
            return;
        }

        this.bounds = new google.maps.LatLngBounds();
        const heatmapData: google.maps.LatLng[] = [];
        assets.forEach((event, index) => {
            const point = new google.maps.LatLng(event.latitude, event.longitude);
            heatmapData.push(point);
            this.bounds.extend(point);
        });

        this.heatmap = new google.maps.visualization.HeatmapLayer({
            data: heatmapData,
            map: this.map
        });

        this.center();
    }


    public center() {
        if (!this.bounds || this.bounds.isEmpty()) {
            return;
        }
        this.map.fitBounds(this.bounds);       // auto-zoom
        this.map.panToBounds(this.bounds);     // auto-center    
    }


    private clear() {
        if (!this.heatmap) {
            return;
        }
        this.heatmap.setMap(null);
    }
}
