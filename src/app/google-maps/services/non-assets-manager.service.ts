// FRAMEWORK
import { Injectable } from '@angular/core';
import { EventManager } from '@angular/platform-browser';

// SERVICES
import { MapsLoaderService } from './maps-loader.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { MapCustomOptionsService } from '../custom-options/custom-options.service';
import { SinglePointBoundsService } from './single-point-bounds.service';
import { InfoWindowService } from './info-window.service';
import { MarkerCreatorService } from './marker-creator.service';
import { SnapToRoadsService } from './snap-to-roads.service';

// TYPES
import { CustomMarker, MarkerExtended } from '../types/marker.types';
import { GoogleMap, MapZoomChange } from '../types/map.types';
import { MapCustomOption, MapCustomOptions } from '../custom-options/custom-options.types';
import { TraceEvent, Trace, AssetIndexObject } from '../../common/entities/entity-types';


// import { } from '@types/googlemaps';
declare const MarkerWithLabel;

@Injectable()
export class NonAssetsMarkerManagerService {
    private map: google.maps.Map;
    private eventListeners: google.maps.MapsEventListener[] = [];
    private markers: MarkerExtended[];
    private freezedMapData: MapZoomChange;
    private bounds: google.maps.LatLngBounds;
    private connectTheDotsPolylines: google.maps.Polyline[];
    private snapToRoadsPolylines: google.maps.Polyline[];

    constructor(private snackBar: SnackBarService,
                private mapsLoaderService: MapsLoaderService,
                private customOptionsService: MapCustomOptionsService,
                private singlePointBounds: SinglePointBoundsService,
                private eventManager: EventManager,
                private markerCreator: MarkerCreatorService,
                private infoWindowMananger: InfoWindowService,
                private snapToRoadsService: SnapToRoadsService) {
        
        // when map finished loading
        mapsLoaderService.map.subscribe((mapInstance: GoogleMap) => {
            this.map = mapInstance.map;
        });
        
        // when map changed zoom
        mapsLoaderService.zoomChange.subscribe((zoomChange: MapZoomChange) => {
            this.freezedMapData = zoomChange;
        });

        // when a user changed an option
        customOptionsService.optionChanged.subscribe((option: MapCustomOption) => {
            this.onChangeCustomOption(option);
        });
    }

    /*
    * Find the markers, and centers around them
    * @param points: the latitude of the marker
    */
    public set(traces: Trace[], events: TraceEvent[], selected: AssetIndexObject[]) {
        const hideLbl = this.customOptionsService.getOptionStatus(MapCustomOptions.hideLabels);
        this.clear();

        if (!traces && !events) {
            return;
        }   
        
        this.markers = [];
        const that = this;
        Promise.all(
            [
                this.markerCreator.toTraceMarkers(traces, hideLbl),
                this.markerCreator.toEventMarkers(events, hideLbl),
            ]
        ).then((res) => {
            const traceMarkers = res[0];
            if (traceMarkers) {
                this.markers = traceMarkers;
            }
              
            const eventMarkers = res[1];
            if (eventMarkers) {
                this.markers = this.markers.concat(eventMarkers);
            }
              
            // add click event to open info window
            this.markers.forEach((marker) => {
                // add marker click event
                const listener = marker.addListener('click', () => {
                    that.infoWindowMananger.close();
                    that.infoWindowMananger.open(marker);
                });
                this.eventListeners.push(listener);
            });

            this.bounds = new google.maps.LatLngBounds();
            if (!selected || selected.length === 0 || 
                (selected.length === 1 && !selected[0])) {
                this.markers.forEach((marker) => {
                    marker.setVisible(true);
                    marker.setMap(this.map);
                    this.bounds.extend(marker.getPosition());
                });
                    
            } else {

                this.markers.forEach((m) => {
                    m.setVisible(false);
                });

                selected.forEach((p) => {
                    const marker = this.markers.find((e) => { 
                        return e.point.timestamp === p.timestamp && e.point.vehicleID === p.vehicleID;
                    });
                    if (marker) {
                        // add to map bounds only the visible markers
                        marker.setVisible(true);
                        marker.setMap(this.map);
                        this.bounds.extend(marker.getPosition());
                    }
                });
            }

            const allVisibleMarkers = this.markers.filter((x) => x.getVisible() === true);
            if (allVisibleMarkers.length === 1) {
                this.bounds = this.singlePointBounds.calculate(allVisibleMarkers[0].getPosition());
            }

            this.center();
            this.drawPolylines();
        });
    }

    
    public clear() {
        this.clearMarkers();
        this.clearPolylines();
    }


    /*
    * Either keep the current zoom level if it selected, or fit map to markers
    */
    public center() {
        if (!this.markers) {
            return;
        }

        if (this.customOptionsService.getOptionStatus(MapCustomOptions.freezeMap) === true) {
            this.map.setZoom(this.freezedMapData.zoomLevel);
            this.map.setCenter(this.freezedMapData.center);
        } else {
            if (!this.bounds || this.bounds.isEmpty()) {
                return;
            }

            this.map.fitBounds(this.bounds);       // auto-zoom
            this.map.panToBounds(this.bounds);     // auto-center  
        }
    }


    // ------------
    // PRIVATE
    // ------------
    
    private clearMarkers() {
        if (this.markers) {
            this.markers.forEach((m) => {
                m.setMap(null);
            });
            this.markers = [];
            
            // remove listeners
            this.removeListeners(this.eventListeners);
            this.eventListeners = [];
        }
    }

    private onChangeCustomOption(option: MapCustomOption) {
        switch (option.id) {
            case MapCustomOptions.hideLabels: 
                this.setLabelVisibility(!option.checked);
                break;
            case MapCustomOptions.freezeMap: 
                // no need to take action, here
                // will happen on the next reload of markers
                break;
            case MapCustomOptions.clusterAssets: 
                // no clustering here
                break;
            case MapCustomOptions.connectTheDots: 
                this.drawPolylines();
                break;
            case MapCustomOptions.snapToRoads:
                this.drawPolylines();
                break;
        }
    }
   
    /*
    * Hide/Show labels from all markers 
    */
    private setLabelVisibility(visible: boolean) {
        if (!this.markers) {
            return;
        }
        this.markers.forEach((m: any) => {
            m.labelVisible = visible;
            m.label.setVisible(visible);
        });
    }

    /*
    * Removes google map event listeners 
    * @param eventHandlersArray: a list of event listeners
    */
    private removeListeners(eventHandlersArray) {
        if (eventHandlersArray) {
            for (const i of eventHandlersArray) {
                google.maps.event.removeListener(i);
            }
        }
    }

    // --------------------
    // POLYLINES DRAWING
    // --------------------

    private drawPolylines() {
        if (this.customOptionsService.getOptionStatus(MapCustomOptions.connectTheDots) === true ) {
           // Draw a straight line between each trace of a trip
            const paths = this.getPaths();
            this.drawSinglePolyline('red', paths, false);
        } else {
            this.clearSimplePolylines();
        }
         
        if (this.customOptionsService.getOptionStatus(MapCustomOptions.snapToRoads) === true ) {
            const paths1 = this.getPaths();
            this.snapToRoadsService.getSnappedPaths(paths1).then((snappedPaths) => {
                this.drawSinglePolyline('blue', snappedPaths, true);
            });
        } else {
            this.clearSnappedPolylines();
        }
        
    }

    private getPaths(): google.maps.LatLng[]  {
        const paths: google.maps.LatLng[] = new Array<google.maps.LatLng>();
        this.markers.forEach((m) => {
            const path = new google.maps.LatLng(m.point.latitude, m.point.longitude);
            paths.push(path);
        });
        return paths;
    }

    /*
    * Draws a polyline on the map  
    * @param lineColor: the color used to draw the line
    */
    private drawSinglePolyline(lineColor: string, paths: google.maps.LatLng[], isSnapped) {
        const polyline = new google.maps.Polyline({
            path: paths,
            strokeColor: lineColor,
            strokeWeight: 2
        });
        this.connectTheDotsPolylines.push(polyline);
        
        /* tslint:disable */
        if (isSnapped == true) {
            /* tslint:enable */
            this.snapToRoadsPolylines.push(polyline);
        } else {
            this.connectTheDotsPolylines.push(polyline);
        }

        polyline.setMap(this.map);
    }
    
    /*
    * Removes all polylines from the map
    */
    private clearPolylines() {
        this.clearSnappedPolylines();
        this.clearSimplePolylines();
    }

    /*
    * Removes all simple polylines from the map (connect the dot option)
    */
    private clearSimplePolylines() {
        if (this.connectTheDotsPolylines) {
            for (const i of this.connectTheDotsPolylines) {
                i.setMap(null);
            }
        }
        this.connectTheDotsPolylines = [];
    }

    /*
    * Removes all simple polylines from the map (connect the dot option)
    */
    private clearSnappedPolylines() {
        if (this.snapToRoadsPolylines) {
            for (const i of this.snapToRoadsPolylines) {
                i.setMap(null);
            }
        }
        this.snapToRoadsPolylines = [];
    }
}
