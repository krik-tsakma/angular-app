
// FRAMEWORK
import { Injectable } from '@angular/core';
import { UserOptionsService } from '../../shared/user-options/user-options.service';

@Injectable()
export class SinglePointBoundsService {
    // semi-axes of WGS-84 geoidal reference
    /* tslint:disable */
    private WGS84_a = 6378137.0;  // major semiaxis [m]
    private WGS84_b = 6356752.3;  // minor semiaxis [m]
    /* tslint:enable */
    private singlePointBoundingBoxDiagonalLengthKM: number;
    
    constructor(userOptions: UserOptionsService) {
        this.singlePointBoundingBoxDiagonalLengthKM = userOptions.getUser().singlePointBoundingBoxDiagonalLengthKM;
    }

    /*
    * returns a bounding box containing the specified lat/lng that will also have the specified diagonal length 
    * @param latLng:     the coordinates of the point
    * @param singlePointBoundingBoxDiagonalLengthKM:     the diagonal length of the map holding a single point (e.g. single asset) in KM
    */
    public calculate(latLng: google.maps.LatLng): google.maps.LatLngBounds {
        const bounds = new google.maps.LatLngBounds();

        // Create bounds from single point 
        bounds.extend(latLng);

        // bounding box surrounding the point at given coordinates, assuming local approximation of Earth surface as a sphere of radius given by WGS84
        const lat = this.deg2rad(latLng.lat());
        const lon = this.deg2rad(latLng.lng());
        const halfSide = 1000 * this.singlePointBoundingBoxDiagonalLengthKM / 2;

        // radius of Earth at given latitude
        const radius = this.WGS84EarthRadius(lat);
        // radius of the parallel at given latitude
        const pradius = radius * Math.cos(lat);

        const latMin = lat - halfSide / radius;
        const latMax = lat + halfSide / radius;
        const lonMin = lon - halfSide / pradius;
        const lonMax = lon + halfSide / pradius;

        // extend the bounding box
        // The zoom levels are discrete, with the scale doubling in each step. So in general you cannot fit the bounds you want exactly (unless you are very lucky with the particular map size).
        // We could extended it by an additional point subtracting .01 from the lat and lng as well, so it keeps the marker in the center. 
        const extendPoint1 = new google.maps.LatLng(this.rad2deg(latMin), this.rad2deg(lonMin));
        const extendPoint2 = new google.maps.LatLng(this.rad2deg(latMax), this.rad2deg(lonMax));
        bounds.extend(extendPoint1);
        bounds.extend(extendPoint2);

        return bounds;
    }


    // convert degrees to radians
    private deg2rad(degrees) {
        return Math.PI * degrees / 180.0;
    }

    // convert radians to degrees
    private rad2deg(radians) {
        return 180.0 * radians / Math.PI;
    }

    // calc  the earth radius at a given latitude, according to the WGS-84 ellipsoid [m]
    private WGS84EarthRadius(lat) {
        // http://en.wikipedia.org/wiki/Earth_radius
        const An = this.WGS84_a * this.WGS84_a * Math.cos(lat);
        const Bn = this.WGS84_b * this.WGS84_b * Math.sin(lat);
        const Ad = this.WGS84_a * Math.cos(lat);
        const Bd = this.WGS84_b * Math.sin(lat);
        return Math.sqrt((An * An + Bn * Bn) / (Ad * Ad + Bd * Bd));
    }

    // calc distance between two latlng
    private latLngDistance(latlng1, latlng2) {
        const dLat = this.deg2rad(latlng2.lat() - latlng1.lat());
        const dLon = this.deg2rad(latlng2.lng() - latlng1.lng());
        const lat1 = this.deg2rad(latlng1.lat());
        const lat2 = this.deg2rad(latlng2.lat());

        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const d = this.WGS84_a * c;
        return d;
    }

}
