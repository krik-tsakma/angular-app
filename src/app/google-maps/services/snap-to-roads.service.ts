// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

// RXJS
import { Observable, throwError, empty } from 'rxjs';
import { expand, catchError } from 'rxjs/operators';


// SERVICES
import { MapsLoaderService } from './maps-loader.service';
import { GeocoderService } from './geocoder.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
// import { } from '@types/googlemaps';
import { MapCurrentPositionControl } from '../types/custom-control.types';
import { GeocoderParsedAddress } from '../types/map.types';
import { SnapToRoadsResult } from '../types/snap-to-roads.types';


/* Use Google's Maps Roads API to pass GPS points of a route, 
*  and return a similar set of data, with the points snapped 
*  to the most likely roads the vehicle was traveling along. 
*/
@Injectable()
export class SnapToRoadsService {
    private snapToRoadsURL: string = 'https://roads.googleapis.com/v1/snapToRoads';
    private snapToRoadsKEY: string = 'AIzaSyCoNbnwXURqbXeTPDeNrse4htqQUk4vT4Y';
    
    constructor(private httpClient: HttpClient) {
        // foo 
    }

    // https://developers.google.com/maps/documentation/roads/snap
    public getSnappedPaths(coordinates: google.maps.LatLng[]): Promise<google.maps.LatLng[]> {
        if (!coordinates) {
            return;
        }
    
        const paths = coordinates.map((c) => {
            return c.lat() + ',' + c.lng();
        });

        return this.fetchSnappedCoordinates(paths);
        
    }

    private fetchSnappedCoordinates(paths): Promise<google.maps.LatLng[]> {
        let requestValues = [].concat.apply([], paths);
        let remainedValues = [];

        // Google Maps Roads API takes up to 100 GPS points
        // For routes larger than that, perform the request several times
        // if we have more than a 100 traces coordinates
        if (paths.length > 100) {
            // split them to chunck of  100 max
            const splittedPathValues = this.splitPathValues(paths);
            // get the values the first 100 for this request
            requestValues = splittedPathValues[0];
            // remove the first 100 and save the rest
            splittedPathValues.splice(0, 1);
            remainedValues = [].concat.apply([], splittedPathValues);
        }

        const snappedCoordinates: google.maps.LatLng[] = new Array<google.maps.LatLng>();
        return new Promise((resolve, reject) => {
            this.snapToRoads(requestValues)
                .pipe(
                    expand((value) =>  { 
                        // if this is the last chunk of traces, show them on map.
                        if (remainedValues.length === 0) {
                            return empty();
                        } else { // otherwise request the next 100
                            // split them to chunck of  100 max
                            const splittedPathValues = this.splitPathValues(remainedValues);
                            // get the values the first 100 for this request
                            requestValues = splittedPathValues[0];
                            // remove the first 100 and save the rest
                            splittedPathValues.splice(0, 1);
                            remainedValues = [].concat.apply([], splittedPathValues);
                        
                            return this.snapToRoads(requestValues);
                        }
                    })
                ).subscribe((res: SnapToRoadsResult) => {
                    res.snappedPoints.forEach((p) => {
                        snappedCoordinates.push(new google.maps.LatLng(p.location.latitude, p.location.longitude));
                    });

                    if (remainedValues.length === 0) {
                        resolve(snappedCoordinates);
                    } 
                });
        });
    }

    private snapToRoads(paths) {
        const params = new HttpParams({
            fromObject: {
                interpolate: 'true',
                key: this.snapToRoadsKEY,
                path: paths.join('|')
            }
        }); 

        return this.httpClient
            .get(this.snapToRoadsURL, { params })
            .pipe(               
                catchError((err) => throwError(err))
            );
        
    }


    private splitPathValues(paths) {
        const chunks = [];
        for (let i = 0, charsLength = paths.length; i < charsLength; i += 100) {
            const segment = paths.splice(0, 100);
            chunks.push(segment);
        }
        return chunks;
    }

}
