// FRAMEWORK
import { Injectable } from '@angular/core';

// SERVICES
import { MapsLoaderService } from './maps-loader.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { MapCustomOptionsService } from '../custom-options/custom-options.service';
import { GeocoderService } from './geocoder.service';
import { SinglePointBoundsService } from './single-point-bounds.service';
import { MarkerCreatorService } from './marker-creator.service';
import { InfoWindowService } from './info-window.service';

// TYPES
import { CustomMarker, MarkerExtended } from '../types/marker.types';
import { GoogleMap, MapZoomChange } from '../types/map.types';
import { MapCustomOption, MapCustomOptions } from '../custom-options/custom-options.types';
import { AssetIndexObject } from '../../common/entities/entity-types';
import { Asset, AssetExtended, AssetTypes } from '../../common/entities/entity-types';

declare const MarkerClusterer;

@Injectable()
export class AssetsMarkerManagerService {
    private maxUnclusteredNum = 250;
    private map: google.maps.Map;
    private markerClusterer: any;
    private eventListeners: google.maps.MapsEventListener[] = [];
    private markers: MarkerExtended[];
    private freezedMapData: MapZoomChange;
    private bounds: google.maps.LatLngBounds;

    constructor(private snackBar: SnackBarService,
                private mapsLoaderService: MapsLoaderService,
                private customOptionsService: MapCustomOptionsService,
                private geocoderService: GeocoderService,
                private singlePointBounds: SinglePointBoundsService,
                private markerCreator: MarkerCreatorService,
                private infoWindowMananger: InfoWindowService) {

        // when map finished loading
        mapsLoaderService.map.subscribe((mapInstance: GoogleMap) => {
            this.map = mapInstance.map;
            this.setupMarkerClusterer(mapInstance.map);
        });
        
        // when map changed zoom
        mapsLoaderService.zoomChange.subscribe((zoomChange: MapZoomChange) => {
            this.freezedMapData = zoomChange;
        });

        // when a user changed an option
        customOptionsService.optionChanged.subscribe((option: MapCustomOption) => {
            this.onChangeCustomOption(option);
        });
    }

    /*
    * Find the markers, and centers around them
    * @param points: the latitude of the marker
    */
    public set(assets: AssetExtended[], selected?: AssetIndexObject[]) {
        this.clearMarkers();
        if (!assets) {
            return;
        }   

        const that = this;
        const hideLbl = this.customOptionsService.getOptionStatus(MapCustomOptions.hideLabels);
        this.markerCreator.toAssetMarkers(assets, hideLbl).then((mh: MarkerExtended[]) => {
            this.markers = mh;
            this.markers.forEach((marker: MarkerExtended) => {
                // add marker click event
                const listener = marker.addListener('click', () => {
                    that.infoWindowMananger.close();
                    that.infoWindowMananger.open(marker);
                });
                this.eventListeners.push(listener);
            });

            // when no selected markers
            if (!selected || selected.length === 0) {
                this.markers.forEach((m) => {
                    m.setVisible(!m.point.hidden);
                });
            } else {

                this.markers.forEach((m) => {
                    m.setVisible(false);
                });

                selected.forEach((p) => {
                    const marker = this.markers.find((e) => { 
                        return e.point.assetType === p.assetType && e.point.vehicleID === p.vehicleID;
                    });
                    if (marker) {
                        marker.setVisible(true);
                    }
                });
            }

            const allVisibleMarkers = this.markers.filter((x) => x.getVisible() === true);
            if (allVisibleMarkers.length > 0) {
                this.bounds = null;
                if (allVisibleMarkers.length === 1) {
                    this.bounds = this.singlePointBounds.calculate(allVisibleMarkers[0].getPosition());
                }
                
                this.markerClusterer.addMarkers(allVisibleMarkers);
                this.center();
                if (this.shouldUncluster()) {
                    setTimeout(() => {
                        this.uncluster(); 
                    }, 1000);
                }
            } else {
                this.showCountry();
            }
        });
    }



    public clearMarkers() {
        if (this.markers) {
            this.markers.forEach((m) => {
                m.setMap(null);
            });
            this.markers = [];

            // remove listeners
            this.removeListeners(this.eventListeners);
            this.eventListeners = [];
        }
        if (this.markerClusterer) {
            this.markerClusterer.clearMarkers();
            this.markerClusterer.setMap(null);
            this.setupMarkerClusterer(this.map);
        }
    }



    /*
    * Either keep the current zoom level if it selected, or fit map to markers
    */
    public center() {
        if (!this.markers) {
            return;
        }

        if (this.customOptionsService.getOptionStatus(MapCustomOptions.freezeMap) === true ) {
            this.map.setZoom(this.freezedMapData.zoomLevel);
            this.map.setCenter(this.freezedMapData.center);
        } else {
            // if bounds exist then we are on single asset mode
            if (this.bounds) {
                this.map.fitBounds(this.bounds);       // auto-zoom
                this.map.panToBounds(this.bounds);     // auto-center  
            } else {
                this.markerClusterer.fitMapToMarkers();
            }
        }
    }

    // ------------
    // PRIVATE
    // ------------

    private onChangeCustomOption(option: MapCustomOption) {
        switch (option.id) {
            case MapCustomOptions.hideLabels: 
                this.setLabelVisibility(!option.checked);
                break;
            case MapCustomOptions.freezeMap: 
                // no need to take action, here
                // will happen on the next reload of markers
                break;
            case MapCustomOptions.clusterAssets: 
                this.setClusteringOption(option.checked);
                break;
        }
    }


    private hasMarkers(): boolean {
        if (!this.markerClusterer || this.markerClusterer.getClusters().length === 0) {
            return false;
        }

        return true;
    }


    /*
    * Hide/Show labels from all markers 
    */
    private setLabelVisibility(visible: boolean) {
        if (!this.markers) {
            return;
        }
        this.markers.forEach((m: any) => {
            m.labelVisible = visible;
            m.label.setVisible(visible);
        });
    }


    private setClusteringOption(on: boolean) {
        if (on === true) {
            // If you set your map back to the existing map object, the clusters will reappear.
            this.markerClusterer.setMap(this.map);

        } else {
            this.uncluster();            
        }
    }


    private uncluster() {
        const mapBounds = this.map.getBounds();
        // If the marker cluster doesn t exist, return
        if (!this.hasMarkers()) {
            return;
        }
        // uncluster all clusters visible to the user
        const clusters = this.markerClusterer.getClusters();
        let markerInBounds = [];
        const clustersToRemove = [];
        for (const cluster of clusters) {            
            cluster.toRemove = false;
            const clusterPos = cluster.getCenter();
            if (mapBounds.contains(clusterPos)) {
                const clusterMarkers = cluster.getMarkers();
                if (!cluster.toRemove) {
	                cluster.toRemove = true;
	                clustersToRemove.push(cluster);
                }
                markerInBounds = markerInBounds.concat(clusterMarkers);
            }
        }


        if (markerInBounds.length < this.maxUnclusteredNum) {
            // remove clusters           
            for (const cluster of clustersToRemove) {
                cluster.remove();
            }

            // show single markers 
            for (const marker of markerInBounds) {
                if (marker) {
                    marker.setMap(this.map);
                }
            }            
        } else {
            // show warning that the user should zoom in
            this.snackBar.open('map.uncluster_warning', 'form.actions.ok');            
        }  
              
    }

    private setupMarkerClusterer(map) {
        const clusterOptions = {
            averageCenter: true,
            imagePath: '/assets/images/map/clusters/m'
        };
        this.markerClusterer = new MarkerClusterer(map, null, clusterOptions);
        let endEventFired = false; // use this because 'clusteringend' event fires twice
        const clusterEndHandler = google.maps.event.addListener(this.markerClusterer, 'clusteringend', () => {
            if (endEventFired === false) {
                endEventFired = true;                
            } else {
                if (this.shouldUncluster()) {
                    this.uncluster();
                }
            }
        });
    }

    /*
    * Displays the user's country in case of nothing else to show
    */
    private showCountry() {
        this.geocoderService.geocodeCountry().then((countryBounds: google.maps.LatLngBounds) => {
            this.map.fitBounds(countryBounds);       // auto-zoom
            this.map.panToBounds(countryBounds);     // auto-center    
        });
    }

    /*
    * Removes google map event listeners 
    * @param eventHandlersArray: a list of event listeners
    */
    private removeListeners(eventHandlersArray) {
        if (eventHandlersArray) {            
            for (const i of eventHandlersArray) {
                google.maps.event.removeListener(i);
            }
        }
    }


    private shouldUncluster(): boolean {
        return this.customOptionsService.getOptionStatus(MapCustomOptions.clusterAssets) === false;
    }

}
