// FRAMEWORK
import { Injectable } from '@angular/core';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { InfoWindowService } from './info-window.service';

// TYPES
import { MarkerTypes, CustomMarker, MarkerExtended, AssetMarker } from '../types/marker.types';
import { TraceEvent, Trace, AssetExtended, AssetTypes } from '../../common/entities/entity-types';
import { TripEvStatuses } from '../../common/entities/status-types';
import { BatteryLevel } from '../../common/ev-battery-status';


// MOMENT
import moment from 'moment';

declare const MarkerWithLabel;

@Injectable()
export class MarkerCreatorService {
    private get dateFormat(): string {
        return this.userOptions.localOptions.short_date + ' HH:mm';
    }

    constructor(private userOptions: UserOptionsService,
                private infoWindowMananger: InfoWindowService) {
        // foo
    }

    /*
    * Converts assets to markers
    * @param asset
    */
    public toAssetMarkers(assets: AssetExtended[], hideLbl: boolean): Promise<MarkerExtended[]> {
        return new Promise<MarkerExtended[]>((resolve, reject) => {

            if (!assets) {
                resolve(null);
            }

            const markers = new Array<MarkerExtended>();
            assets.forEach((asset, index) => {
                if (asset.vehicleStatus === TripEvStatuses.NoStatus) {
                    return;
                }
                const m = {
                    type: MarkerTypes.Asset,
                    index,
                    latitude: asset.latitude,
                    longitude: asset.longitude,
                    timestamp: asset.timestamp,
                    draggable: false,
                    vehicleID: asset.vehicleID,
                    assetType: asset.assetType,
                    hidden: asset.isHidden,
                    asset: {
                        label: asset.assetType === AssetTypes.Vehicle ? asset.vehicleLabel : asset.driverLabel,
                        isEV: asset.isEv,
                        vehicleStatus: asset.vehicleStatus,
                        batteryLevel: asset.batteryLevel
                    } as AssetMarker
                } as CustomMarker;

                m.label = m.asset.label;

                if (asset.isEv) {
                    const socIcon = BatteryLevel.map(asset.batteryLevel);
                    switch (Number(asset.vehicleStatus)) {
                        case TripEvStatuses.NoStatus:
                            m.icon = '';
                            break;
                        case TripEvStatuses.Offline:
                            m.icon = 'offline.svg';
                            break;
                        default:
                            if (asset.vehicleStatus && TripEvStatuses[asset.vehicleStatus]) {
                                m.icon = TripEvStatuses[asset.vehicleStatus].toString().toLowerCase() + '_' + socIcon + '.svg';
                            } else {
                                m.icon = '';
                            }
                            break;
                    }
                    m.icon = 'ev-assets/' + m.icon;
                }
                const gm = this.toGoogleMarker(m, hideLbl);
                markers.push(gm);
            });
            resolve(markers);
        });
    }


   /*
    * Converts traces to markers
    * @param traces
    */
    public toTraceMarkers(traces: Trace[], hideLbl: boolean): Promise<MarkerExtended[]> {
        return new Promise<MarkerExtended[]>((resolve, reject) => {
            if (!traces) {
                resolve(null);
            }
            const markers = new Array<MarkerExtended>();
            let count = 0;
            for (const t of traces) {
                if (t.isHidden === true) {
                    return;
                }
                count ++;
                const max = traces.length;
                const m = {
                    latitude: t.latitude,
                    longitude: t.longitude,
                    draggable: false,
                    timestamp: t.timestamp,
                    vehicleID: t.vehicleID,
                    assetType: t.assetType,
                    hidden: t.isHidden
                } as CustomMarker;
                if (t === traces[0]) {
                    m.type = MarkerTypes.TripStart;
                    m.zIndex = 1000;
                    m.lblColor = 'green';
                    m.icon = 'start-driving.png';
                    m.label = moment(t.timestamp).format(this.dateFormat);
                } else if (t === traces[(max - 1)]) {
                    m.type = MarkerTypes.TripStop;
                    m.zIndex = 2000;
                    m.lblColor = 'red';
                    m.icon = 'stop-driving.png';
                    m.label = moment(t.timestamp).format(this.dateFormat);
                } else {
                    m.type = MarkerTypes.Trace;
                    m.zIndex = count;
                    m.gpsDirection = t.gpsDirection;
                    m.icon = this.getTraceDirection(t);
                }

                const gm = this.toGoogleMarker(m, hideLbl);
                markers.push(gm);
            }
            // Shift to move the stop driving in front, so that start/stop driving appear on top
            resolve(markers);
        });
    }


    /*
    * Converts events to markers
    * @param events
    */
    public toEventMarkers(events: TraceEvent[], hideLbl: boolean): Promise<MarkerExtended[]> {
        return new Promise<MarkerExtended[]>((resolve, reject) => {
            if (!events) {
                resolve(null);
            }
            const markers = new Array<MarkerExtended>();
            events.forEach((e, index) => {
                if (!e || e.isHidden === true) {
                    return;
                }
                const m = {
                    type: MarkerTypes.Event,
                    index,
                    latitude: e.latitude,
                    longitude: e.longitude,
                    timestamp: e.timestamp,
                    draggable: false,
                    vehicleID: e.vehicleID,
                    hidden: e.isHidden,
                    lblColor: 'default',
                    icon: 'event.png',
                    label: moment(e.timestamp).format(this.dateFormat)
                } as CustomMarker;

                const gm = this.toGoogleMarker(m, hideLbl);
                markers.push(gm);
            });
            resolve(markers);
        });
    }


    // ------------
    // PRIVATE
    // ------------

    private toGoogleMarker(cm: CustomMarker, hideLbl: boolean): any {
        let icon: any;
        const baseIconsURL = 'assets/marker-icons/';
        if (cm.icon.indexOf('.svg') > -1) {
            icon = {
                url: baseIconsURL + cm.icon,
                anchor: new google.maps.Point(45, 65),
            };
        } else {
            icon = baseIconsURL + cm.icon;
        }

        const marker = new MarkerWithLabel({
            position: new google.maps.LatLng(cm.latitude, cm.longitude),
            icon: icon,
            zIndex: cm.zIndex,
            labelContent: cm.label,
            labelAnchor: new google.maps.Point(25, 0),
            labelVisible: cm.label === undefined || hideLbl === true ? false : true,
            labelClass: 'marker-lbl ' + cm.lblColor,
            labelInBackground: true,
            visible: cm.hidden === true ? false : true
        });

        // add then initial object to the marker (to reference/search it later)
        marker.point = cm;

        return marker;
    }



    private getTraceDirection(trace: Trace): string {
        let icon = 'blank.png';

        const h = trace.gpsDirection;
        if (h === -1) {
            icon = 'z_z1_notavailable.png';
        } else {
            const _M_HEADING_LABEL = ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw'];
            let arc = 0;
            for (const j of _M_HEADING_LABEL) {
                const b = arc - 22.5;
                const e = arc + 22.5;
                if (h >= b && h <= e) {
                    icon = 'z_' + j + '.png';
                    break;
                }
                arc += 45;
            }

            if (!icon) {
                icon = 'blank.png';
            }
        }
        return icon;
    }


}
