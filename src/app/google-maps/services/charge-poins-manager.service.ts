// FRAMEWORK
import { Injectable } from '@angular/core';

// SERVICES
import { MapsLoaderService } from './maps-loader.service';
import { MapCustomOptionsService } from '../custom-options/custom-options.service';
import { SinglePointBoundsService } from './single-point-bounds.service';

// TYPES
import { CustomMarker, MarkerExtended } from '../types/marker.types';
import { GoogleMap, MapZoomChange } from '../types/map.types';
import { MapCustomOption, MapCustomOptions } from '../custom-options/custom-options.types';
import { ChargePointMarker } from '../types/charge-point.types';
import { BaseCustomMarker, MarkerWithLabelCreator } from '../types/custom-marker.types';


// import { } from '@types/googlemaps';
declare const MarkerWithLabel;

@Injectable()
export class ChargePointsMarkerManagerService {
    private map: google.maps.Map;
    private eventListeners: google.maps.MapsEventListener[] = [];
    private markers: Array<BaseCustomMarker<ChargePointMarker>>;
    private freezedMapData: MapZoomChange;
    private bounds: google.maps.LatLngBounds;

    constructor(private mapsLoaderService: MapsLoaderService,
                private customOptionsService: MapCustomOptionsService,
                private singlePointBounds: SinglePointBoundsService) {
        
        // when map finished loading
        mapsLoaderService.map.subscribe((mapInstance: GoogleMap) => {
            this.map = mapInstance.map;
        });
        
        // when map changed zoom
        mapsLoaderService.zoomChange.subscribe((zoomChange: MapZoomChange) => {
            this.freezedMapData = zoomChange;
        });

        // when a user changed an option
        customOptionsService.optionChanged.subscribe((option: MapCustomOption) => {
            this.onChangeCustomOption(option);
        });
    }

    /*
    * Find the markers, and centers around them
    * @param points: the latitude of the marker
    */
    public set(cps: Array<BaseCustomMarker<ChargePointMarker>>) {
        const hideLbl = this.customOptionsService.getOptionStatus(MapCustomOptions.hideLabels);
        this.clear();

        if (!cps) {
            return;
        }   
        this.markers = [];
        this.bounds = new google.maps.LatLngBounds();

        cps.forEach((m) => {
            const marker = new MarkerWithLabelCreator<ChargePointMarker>(m, 'assets/marker-icons/charge-points', hideLbl).marker;
            marker.setVisible(true);
            marker.setMap(this.map);

            this.markers.push(marker);
            this.bounds.extend(marker.getPosition());
        });

        const allVisibleMarkers = this.markers.filter((x) => x.getVisible() === true);
        if (allVisibleMarkers.length === 1) {
            this.bounds = this.singlePointBounds.calculate(allVisibleMarkers[0].getPosition());
        }

        this.center();
    }

    
    public clear() {
        this.clearMarkers();
    }


    /*
    * Either keep the current zoom level if it selected, or fit map to markers
    */
    public center() {
        if (!this.markers) {
            return;
        }

        if (this.customOptionsService.getOptionStatus(MapCustomOptions.freezeMap) === true) {
            this.map.setZoom(this.freezedMapData.zoomLevel);
            this.map.setCenter(this.freezedMapData.center);
        } else {
            if (!this.bounds || this.bounds.isEmpty()) {
                return;
            }

            this.map.fitBounds(this.bounds);       // auto-zoom
            this.map.panToBounds(this.bounds);     // auto-center  
        }
    }


    // ------------
    // PRIVATE
    // ------------
    
    private clearMarkers() {
        if (this.markers) {
            this.markers.forEach((m) => {
                m.setMap(null);
            });
            this.markers = [];
            
            // remove listeners
            this.removeListeners(this.eventListeners);
            this.eventListeners = [];
        }
    }

    private onChangeCustomOption(option: MapCustomOption) {
        switch (option.id) {
            case MapCustomOptions.hideLabels: 
                this.setLabelVisibility(!option.checked);
                break;
            case MapCustomOptions.freezeMap: 
                // no need to take action, here
                // will happen on the next reload of markers
                break;
            case MapCustomOptions.clusterAssets: 
                // no clustering here
                break;
            case MapCustomOptions.connectTheDots: 
            case MapCustomOptions.snapToRoads:
                // not applied
                break;
        }
    }
   
    /*
    * Hide/Show labels from all markers 
    */
    private setLabelVisibility(visible: boolean) {
        if (!this.markers) {
            return;
        }
        this.markers.forEach((m: any) => {
            m.labelVisible = visible;
            m.label.setVisible(visible);
        });
    }

    /*
    * Removes google map event listeners 
    * @param eventHandlersArray: a list of event listeners
    */
    private removeListeners(eventHandlersArray) {
        if (eventHandlersArray) {
            for (const i of eventHandlersArray) {
                google.maps.event.removeListener(i);
            }
        }
    }

}
