// FRAMEWORK
import { Injectable } from '@angular/core';

@Injectable()
export class ProgressBarService {
    public loading: boolean;

    constructor() {
       this.loading = false;
    }

    public startLoading() {
        this.loading = true;
    }
   
    public stopLoading() {
        this.loading = false;
    }
}
