// FRAMEWORK
import { Injectable } from '@angular/core';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';

// TYPES
import { GoogleMapConfigParams } from '../types/google-map.types';

/**
 * This is where all the necessary libraries should load,
 * and the initialization takes place
 */
@Injectable()
export class GoogleMapsConfigurationService {
    private configParams: GoogleMapConfigParams = {
        v: '3.37',
        key: 'AIzaSyDPwCmMyIm81Kc9oY3EhhIkCtuAsyhGfP8',
       // clientId: 'gme-sycadabv',
        libraries: ['geometry', 'visualization', 'places'],
      //  region: 'The Nederlands' // TO DO
    };

    /**
     * Lifecycle hooks, like OnInit() work with Directives and Components.
     * They do not work with other types, like our service.
     * So do not load immediately maps. Call only configure func
     */
    constructor(private userOptions: UserOptionsService) {
       // foo
    }

    public configure(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (!this.isConfigured()) {
                this.createGoogleMapsScript().then(() => {
                    // a small timeout is needed, because cause google script need a sec to be fully inserted into the dom
                    setTimeout(() => {
                        console.log('configured now');
                        resolve();
                    }, 2000);
                });
            }  else {
                setTimeout(() => {
                    resolve();
                }, 2000);
            }
        });
    }

    /**
     * Whether the google maps script, is appended on the dom
     */
    private isConfigured(): boolean {
        return document.getElementById('google-maps-script') !== null;
    }

    private createGoogleMapsScript(): Promise<void> {
        return new Promise((resolve, reject) => {
            // set the map language
            const userLocale = this.userOptions.getUser().culture;
            if (userLocale) {
                this.configParams.language  = userLocale;
            }
            this.configParams.callback = 'GoogleMapsLoaded';

            // Build the JS API request node.
            const script = document.createElement('script');
            script.src =  this.getScriptSrc(this.configParams);
            script.setAttribute('async', '');
            script.setAttribute('defer', '');
            script.setAttribute('id', 'google-maps-script');
            document.head.appendChild(script);

            window['GoogleMapsLoaded'] = (() => {
                resolve();
            });
        });
    }

    private getScriptSrc(queryParams: GoogleMapConfigParams): string {
        // fetch the maps api based on the protocol used (http or https)
        const params: string =
            Object.keys(queryParams)
                .filter((k: string) => queryParams[k] != null)
                .filter((k: string) => {
                // remove empty arrays
                return !Array.isArray(queryParams[k]) ||
                    (Array.isArray(queryParams[k]) && queryParams[k].length > 0);
                })
                .map((k: string) => {
                // join arrays as comma seperated strings
                const i = queryParams[k];
                if (Array.isArray(i)) {
                    return {key: k, value: i.join(',')};
                }
                return {key: k, value: queryParams[k]};
                })
                .map((entry: {key: string, value: string}) => `${entry.key}=${entry.value}`)
                .join('&');
        return `https://maps.googleapis.com/maps/api/js?${params}`;
    }

}
