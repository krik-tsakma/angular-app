// FRAMEWORK
import { Injectable } from '@angular/core';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { MapsLoaderService } from './maps-loader.service';
import { ProgressBarService } from './progress-bar.service';

// TYPES
import { GoogleMap, GeocoderParsedAddress } from '../types/map.types';

@Injectable()
export class GeocoderService {
    private geocoder: any;
    private geocodeInProcess = false;

    constructor(private userOptions: UserOptionsService,
                private mapsLoaderService: MapsLoaderService,
                private progressBarService: ProgressBarService) {

        // when map finished loading
        mapsLoaderService.map.subscribe((mapInstance: GoogleMap) => {
            this.geocoder = new google.maps.Geocoder();
        });
    }

    public geocode(request: google.maps.GeocoderRequest, showProgressBar: boolean): Promise<GeocoderParsedAddress[]> {
        const that = this;
        return new Promise<GeocoderParsedAddress[]>((resolve, reject) => {
            if (showProgressBar === true) {
                this.progressBarService.startLoading();
            }
            const userLocale = this.userOptions.getUser().culture;
            that.geocoder.geocode(request, (results: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
               
                if (status !== google.maps.GeocoderStatus.OK) {
                    reject('geocoding provider does not respond (status=' + status + ')');
                }
                if (results.length === 0) {
                    reject('unknown country for geocoding provider: n/a');
                }
                if (showProgressBar === true) {
                    this.progressBarService.stopLoading();
                }

                const addresses = this.parseGeocodingResults(results);
                resolve(addresses);

            });
       });
    }
    

    public geocodeCountry(): Promise<google.maps.LatLngBounds>   {
        let isoCountryCode: string = 'GB';
        const userLocale = this.userOptions.getUser().region;
        if (userLocale) {
            isoCountryCode = userLocale;
        }

        const that = this;
        return new Promise<google.maps.LatLngBounds>((resolve, reject) => {
            const geoRequest: google.maps.GeocoderRequest = {
                componentRestrictions: {
                    country: isoCountryCode
                }
            };

            const res = that.geocode(geoRequest, false).then((results: GeocoderParsedAddress[]) => {
                const countryBounds =  results[0].bounds;
                
                if (!countryBounds || countryBounds.isEmpty()) {
                    reject();
                }
                resolve(countryBounds);
            });
        });
    }

    // parsing of geocoding results 
    public parseGeocodingResults(results: google.maps.GeocoderResult[]): GeocoderParsedAddress[] {
        const addresses = new Array<GeocoderParsedAddress>(); 

        results.forEach((r) => {
            r.types.forEach((t) => {
                const address: GeocoderParsedAddress = {} as any;
                address.location = r.geometry.location;
                address.bounds = r.geometry.bounds;

                for (const c of r.address_components) {
                    for (const ct of c.types) {
                        switch (ct) {
                            case 'street_number':
                                address.streetNumber = (!address.streetNumber) ? c.long_name : address.streetNumber;
                                break;
                            case 'route':
                            case 'street_address':
                                address.streetName = (!address.streetName) ? c.long_name : address.streetName;
                                break;
                            case 'postal_code':
                                address.postalCode = (!address.postalCode) ? c.long_name : address.postalCode;
                                break;
                            case 'country':
                                address.country = (!address.country) ? c.long_name : address.country;
                                break;
                            case 'locality':
                                address.city = (!address.city) ? c.long_name : address.city;
                                break;
                            case 'administrative_area_level_1':
                            case 'administrative_area_level_2':
                            case 'administrative_area_level_3':
                            case 'airport':
                            case 'park':
                            case 'natural_feature':
                                address.area = (!address.area) ? c.long_name : address.area;
                        }

                    }
                }
                // do not leave street name empty, since it is part of what is shown to the user on autocomplete results (else undefined is shown)
                if (!address.streetName || address.streetName === '') {
                    address.streetName = r.formatted_address;
                }                    
                addresses.push(address);
            });
        });

        return addresses;
}

}
