// FRAMEWORK
import { Injectable } from '@angular/core';

// SERVICES
import { MapsLoaderService } from './maps-loader.service';
import { GeocoderService } from './geocoder.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
import { MapCurrentPositionControl } from '../types/custom-control.types';
import { GoogleMap, GeocoderParsedAddress } from '../types/map.types';

@Injectable()
export class CurrentPositionService {
    private map: google.maps.Map;
    private bounds: google.maps.LatLngBounds;
    private control: MapCurrentPositionControl;
    private headerText: string;

    constructor(private mapsLoaderService: MapsLoaderService,
                private geocoderService: GeocoderService,
                private translateService: CustomTranslateService) {
            
        // when map finished loading
        mapsLoaderService.map.subscribe((mapInstace: GoogleMap) => {
            this.map = mapInstace.map;
            this.headerText = this.translateService.instant('map.current_location');

            // Create the DIV to hold the control and call the CenterControl()
            // constructor passing in this DIV.
            const centerControlDiv = document.createElement('div');
            this.control = new MapCurrentPositionControl(centerControlDiv);

            mapInstace.map.controls[google.maps.ControlPosition.TOP_LEFT].push(centerControlDiv);
        });
        
        // when map finished changing bounds
        mapsLoaderService.boundsChange.subscribe(() => {
            this.getCurrentLocationDescription();
        });
    }

    private getCurrentLocationDescription() {
        const position = this.map.getCenter();
        const geoRequest: google.maps.GeocoderRequest = {
            location: position
        };

        this.geocoderService.geocode(geoRequest, false)
            .then((results: GeocoderParsedAddress[]) => {
                const adr = results[0];
                const zoom = this.map.getZoom();
                let text = '';
                 
                const country = adr.country;
                const area = adr.area ? adr.area : '';
                let town = adr.city ? adr.city : '';
    
                if (zoom > 4 && zoom <= 7) {
                    text = country;
                }
                if (zoom > 7 && zoom <= 10) {
                    const separator = country && area ? ' > ' : '';
                    text = area !== '' ? country + separator + area : country;
                }
                if (zoom > 10) {
                    const countrySeparator = area || town ? ' > ' : '';
                    const separator = (area !== '' && town !== '') ? ' > ' : '';
                    if (area === town) {
                        town = '';
                    }
    
                    text = country + countrySeparator + area + separator + town;
                }

                const descr = text ? '<span style="color:red">' + this.headerText + '</span> ' + text : '';
                this.control.setText(descr);
        });
    }
}
