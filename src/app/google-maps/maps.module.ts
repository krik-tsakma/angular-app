// FRAMEWORK
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientJsonpModule } from '@angular/common/http';

// MODULES
import { SharedModule } from '../shared/shared.module';

// COMPONENTS
import { MapsComponent } from './maps.component';
import { MapCustomOptionsComponent } from './custom-options/custom-options.component';

// SERVICES
import { GoogleMapsConfigurationService } from './services/google-maps-configuration.service';

/**
 * Contains all componenents of the core module.
 */
@NgModule({
    imports: [
        CommonModule,
        HttpClientJsonpModule,
        SharedModule
    ],
    declarations: [
        MapsComponent,
        MapCustomOptionsComponent
    ],
    exports: [
        MapsComponent,
        MapCustomOptionsComponent
    ],
    providers: [
        GoogleMapsConfigurationService
    ]
})
export class MapsModule { }
