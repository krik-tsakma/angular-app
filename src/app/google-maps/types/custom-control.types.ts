/**
 * The MapCurrentPositionControl adds a control to the map that 
 * displays in plain text the current center of the map 
 * This constructor takes the control DIV as an argument.
 * @constructor
 */
export class MapCurrentPositionControl {
    private controlUI: HTMLDivElement;
    private controlText: HTMLDivElement;
    
    constructor(controlDiv) {
        // Set CSS for the control border.
        this.controlUI = document.createElement('div');
        this.controlUI.style.backgroundColor = '#fff';
        this.controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        this.controlUI.style.marginTop = '2px';
        this.controlUI.style.marginLeft = '2px';
        this.controlUI.style.textAlign = 'center';
        controlDiv.appendChild(this.controlUI);

        // Set CSS for the control interior.
        this.controlText = document.createElement('div');
        this.controlText.style.color = 'rgb(25,25,25)';
        this.controlText.style.fontFamily = 'Arial,sans-serif';
        this.controlText.style.fontSize = '9px';
        // this.controlText.style.lineHeight = '18px';
        this.controlText.style.paddingLeft = '5px';
        this.controlText.style.paddingRight = '5px';
       
        this.controlUI.appendChild(this.controlText);
    }

    public setText(text: string) {
        this.controlText.innerHTML = text;
        if (text) {
            this.show();
        } else {
            this.hide();
        }
    }

    private show() {
        const isVisible =  this.controlUI.style.display === 'block';
        if (!isVisible) {
            this.controlUI.style.display = 'block';
            setTimeout(() => {
                this.hide();
            }, 10000);
        }
    }

    private hide() {
        this.controlUI.style.display = 'none';
    }
}
