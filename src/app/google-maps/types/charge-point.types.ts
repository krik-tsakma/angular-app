import { ChargePointStatusOptions } from '../../charge-point-manager/charging-point-manager-types';
import { MarkerTypes } from './marker.types';
import { BaseCustomMarker } from './custom-marker.types';

export interface ChargePointMarker {
    chargePointID: number;
    chargePointStatus: ChargePointStatusOptions;
}

export class ChargePointMarkerCreator {
    public marker: BaseCustomMarker<ChargePointMarker>;

    constructor(cpID: number, cpName: string, cpStatus: ChargePointStatusOptions, lat: number, lon: number) {
        this.marker = {
            latitude: lat,
            longitude: lon,
            label: cpName,
            hidden: false,
            draggable: false,
            clickable: false,
            data: { chargePointID: cpID, chargePointStatus: cpStatus } as ChargePointMarker
        } as BaseCustomMarker<ChargePointMarker>;

        switch (cpStatus) {
            case ChargePointStatusOptions.Available:
                this.marker.icon = 'map_pin_green.png';
                break;
            case ChargePointStatusOptions.Unknown:
                this.marker.icon = 'map_pin_gray.png';
                break;
            default:
                this.marker.icon = 'map_pin_red.png';
                break;
        }
    }
}
