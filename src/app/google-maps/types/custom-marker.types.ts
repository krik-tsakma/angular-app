declare const MarkerWithLabel;

export interface BaseCustomMarker<T> extends google.maps.Marker {
    index: number;
    draggable: boolean;
    clickable: boolean;
    zIndex: number;
    hidden?: boolean;
    icon: string;
    lblColor: string;
    label: string;
    latitude: number;
    longitude: number;
    data: T;
}

export class MarkerWithLabelCreator<T> {
    public marker: BaseCustomMarker<T>;

    constructor(cm: BaseCustomMarker<T>, baseIconsURL: string, hideLbl: boolean) {
        const icon = `${baseIconsURL}/${cm.icon}`;

        const marker = new MarkerWithLabel({
            position: new google.maps.LatLng(cm.latitude, cm.longitude),
            icon: icon,
            zIndex: cm.zIndex,
            labelContent: cm.label,
            labelAnchor: new google.maps.Point(25, 0),
            labelVisible: cm.label === undefined || hideLbl === true ? false : true,
            labelClass: 'marker-lbl ' + cm.lblColor,
            labelInBackground: true,
            visible: cm.hidden === true ? false : true,
            clickable: cm.clickable,
        });

        // add extra data object to the marker (to reference/search it later)
        marker.data = cm.data;

        this.marker = marker;
    }
}
