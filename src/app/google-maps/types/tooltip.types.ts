import { TraceEvent, Trace, AssetTypes } from '../../common/entities/entity-types';

export class AssetTooltipRequest {
    public vehicleID: number;
    public timestamp: string;
}

export class AssetTooltipResult {
    public speed: number;
    public vehicleLabel: string;
    public driverLabel: string;
    public timestamp: string;
    public events: TraceEvent[];
    public isEV: boolean;
    public soC?: number;
    public range?: number;
}


export class AssetTooltipTerms  {
    public information: string;
    public events: string;
    public driver: string;
    public vehicle: string;
    public speed: string;
    public timestamp: string;
    public eventAddress: string;
    public eventMessage: string;
    public eventDate: string;
    public soc: string;
    public range: string;
}
