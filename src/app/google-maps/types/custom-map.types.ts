
import { InrixSpeedBucket } from '../../auth/user';

export class MapCustomOverlay implements google.maps.MapType {
    public tileSize: google.maps.Size;
    public projection?: google.maps.Projection;
    private authToken: string;
    private mapsBaseURL: string;

    constructor(projection: google.maps.Projection, authToken: string, mapsURL: string) {
        this.tileSize = new google.maps.Size(256, 256);
        this.projection = projection;
        this.authToken = authToken;
        this.mapsBaseURL = decodeURIComponent(mapsURL);
    }

    public getTile(coord: google.maps.Point, zoom: number, ownerDocument: Document) {
        // Create an IMG element and append it to document
        const img = ownerDocument.createElement('img');
        const projection = this.mercatorProjection(zoom);
        const center = projection.lat() + '|' + projection.lng();
        // img.onerror = ''; // _M_CustomMapImageNotFound; //add an on error method to catch all the non existing images 
        img.src = this.setTrafficTileLayer(coord, zoom, center);
        return img;
    }
    
    public releaseTile(title) {
        // foo
    }

    private mercatorProjection(zoom) {
        const MERCATOR_RANGE = 256;
        // no need to implement our own mercator projection (like in mbox2). API 3 does it for us.
        const scale = Math.pow(2, zoom + 1); // TODO: Correct is  world coordinates = pixel coordinates / (2 to the power of zoom level)  according to api 3 docs, so why zoom +1 ?
        const worldCoordinates = MERCATOR_RANGE / scale;
        return this.projection.fromPointToLatLng(new google.maps.Point(worldCoordinates, worldCoordinates));
    }

    private setTrafficTileLayer(coord, zoom, center) {
        if (this.validateTrafficTileSettings(zoom) === false) { 
            return '';
        }

        const baseInrixURL = this.getInrixMapUrl(coord.x, zoom);
        // get the tile url
        const url = baseInrixURL
                    + '?Action=GetMapTile'
                    + '&Layers=T' // just give us the traffic tile layer. Options: (M) Base map, (W) Weather, and (T) Traffic
                    + '&Opacity=80'
                    + '&Center=' + center // center and zoom sets the position and size of the tile on the map
                    + '&Zoom=' + zoom
                    + '&East=' + coord.x
                    + '&North=' + (-coord.y)
                    + '&PenWidth=' + ((zoom / 4 * 3) + 1) // make the pen width adjustable based on zoom
                    + '&Height=' + 256
                    + '&Width=' + 256
                    + '&Format=png' // we have a good browser, give us PNGs for better quality
                    + '&FRCLevel=1,2,3,4,5,6,7' // + (zoom > 10 ? (zoom > 11 ? ",3,4,5" : ",3") : "") //show different roads details based on zoom level
                    + '&Coverage=8' // 8=(Real-time core + extended coverage); 255="Total Fusion (TM)" - i.e. all roads which INRIX covers
                    + '&SpeedBucketID=2' // Color Roads with speed. The default INRIX speed bucket which uses G/Y/O/R colors
                    + '&Token=' + this.authToken
            ;
        return url;
     
     }

    // Setting up path to call into INRIX TTS server, which serves up map and traffic tiles
    private getInrixMapUrl(x, zoom) {
        return 'http://' + ((x < Math.floor(Math.pow(2, zoom - 1) - Math.pow(2, zoom - 4))) ? 'na' : 'eu') + this.mapsBaseURL;
    }

     private validateTrafficTileSettings(zoom) {
        // Some zoom levels are too high or too low for GetMapTile to return an image (Inrix default zooms, max: 18, min: 5)
        if (zoom > 18 || zoom < 6) {
            return false;
        }
        if (!this.mapsBaseURL) {
            return false;
        }
        return true;
    }
}


export enum TrafficLayerTypes {
    camera,
    accident,
    construction,
    congestion,
    weather
}

export interface TrafficCamera {
    id: string;
    name: string;
    latitude: number;
    longitude: number;
    outOfService: boolean;
    copyright: string;
}

export interface TrafficIncident {
    id: string;
    type: TrafficLayerTypes; // he type of incident: 1=Construction, 2=Event, 3=Flow, 4=Incident, 5 = Road Weather, 6 = Police
    title: string;
    latitude: number;
    longitude: number;
    status: string;
    severity: number; // 0 – Minimal impact, 1 – Low impact, 2 – Moderate impact, 3 – High impact, 4 – Severe impact , 
    startTime: string;
    endTime: string;
    description: string;
    location: string;
    distance: string;
    freeFlowDelay: string;
    typicalFlowDelay: string;
}

export interface TrafficIncidentTerms {
    location: string;
    when: string;
    till: string;
    length: string;
    delayFromFreeFlow: string;
    delayFromTypicalFlow: string;
    description: string;
}

export class TrafficHtmlTemplates {

    /**
     * Traffic legend control
     */
    public static speedLegend(buckets: InrixSpeedBucket[], speedLabel: string, speedMetric: string) {
        let labels = '';
        let speeds = '';
        buckets.forEach((b) => {
            speeds += `<td class='inrixLegendEntry' style='background-color:${b.color};'></td>`;
            labels += `<td class='InrixLegendEntryText'>${b.label}</td>`;
        });
        
        return `<table id='M_InrixLegend' cellpadding='0' cellspacing='0' class='InrixLegendTable'>
                    <thead>
                        <tr>
                            <th colspan='8'>${speedLabel} (${speedMetric})</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class='inrixLegendEntry Entry_0 SmallCheckerBoard' title='Closed' style='background-color: white;'></td>
                            ${speeds}
                        </tr>
                        <tr>
                            <td class='InrixLegendEntryText'>Closed</td>
                            ${labels}
                        </tr>
                    </tbody>
                </table>`;
    }

    public static trafficCamera(name: string, copyright: string, imageSrc) {
        return `<div class="traffic-camera">
                    <div class="title">${name}</div>
                    <img class="images" width="235" height="230" alt="Traffic Camera Image" src="${imageSrc}" /> 
                    <div class="copyright">${copyright}</div>
                </div>`;
    }

    public static incident(incident: TrafficIncident, terms: TrafficIncidentTerms) {
        return `<div class="traffic-incident">
                    <div class="title">${incident.title}</div>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td class="label">
                                    ${terms.location}
                                </td>
                                <td>
                                    ${incident.location}
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    ${terms.when} (UTC)
                                </td>
                                <td>
                                    ${incident.startTime} ${terms.till} ${incident.endTime} 
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    ${terms.length}
                                </td>
                                <td>
                                    ${incident.distance}
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    ${terms.delayFromFreeFlow}
                                </td>
                                <td>
                                    ${incident.freeFlowDelay}
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    ${terms.delayFromTypicalFlow}
                                </td>
                                <td>
                                    ${incident.typicalFlowDelay}
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    ${terms.description}
                                </td>
                                <td>
                                    ${incident.description}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>`;
    }
}
