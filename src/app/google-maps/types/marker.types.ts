/// <reference types="@types/googlemaps" />
import { Asset, AssetTypes } from '../../common/entities/entity-types';
import { TripEvStatuses } from '../../common/entities/status-types';

// just an interface for type safety.
export interface CustomMarker extends Asset {
     index: number;
     draggable: boolean;
     zIndex: number;
     type: MarkerTypes;
     gpsDirection?: number;
     asset?: AssetMarker;
     hidden?: boolean;
     icon: string;
     lblColor: string;
     label: string;
}

export interface MarkerExtended extends google.maps.Marker {
    point: CustomMarker;
    labelVisible: boolean;
}

export enum MarkerTypes {
    Event = 0,
    Trace = 1,
    TripStart = 2,
    TripStop = 3,
    Asset = 4
}

export interface AssetMarker {
    label: string;
    isEV: boolean;
    soC?: number;
    range?: number;
    vehicleStatus?: TripEvStatuses;
}
