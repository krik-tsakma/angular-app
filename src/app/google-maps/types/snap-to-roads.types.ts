export interface SnapToRoadsResult {
    snappedPoints: SnappedPoint[];
}

export interface SnappedPoint {
    location: SnappedPointLatLng;
    originalIndex: number;
    placeId: string;
}

export interface SnappedPointLatLng {
    latitude: number;
    longitude: number;
}
