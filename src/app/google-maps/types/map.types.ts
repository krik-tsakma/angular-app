
export interface GoogleMap {
    /**
     * The html element id
     */
    mapID: string;
    /**
     * The map instance
     */
    map: google.maps.Map;   
}

  
export enum MapModeOptions {
    marker,
    heatmap   
}

export interface MapZoomChange {
    center: google.maps.LatLng;
    zoomLevel: number;
}

export interface GeocoderParsedAddress {
    bounds: google.maps.LatLngBounds;
    location: google.maps.LatLng;
    streetName: string;
    streetNumber: string;
    postalCode: string;
    country: string;
    city: string;
    area: string;
}
