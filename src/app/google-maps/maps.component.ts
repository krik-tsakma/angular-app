// FRAMEWORK
import {
    OnChanges, AfterViewInit, SimpleChanges, ChangeDetectorRef,
    Component, ViewEncapsulation, HostListener,
    Input, ElementRef, ViewChild, Output, EventEmitter
} from '@angular/core';

// SERVICES
import { ChargePointsMarkerManagerService } from './services/charge-poins-manager.service';
import { MapsLoaderService } from './services/maps-loader.service';
import { InfoWindowService } from './services/info-window.service';
import { GeocoderService } from './services/geocoder.service';
import { ProgressBarService } from './services/progress-bar.service';
import { HeatmapService } from './services/heatmap.service';
import { AssetsMarkerManagerService } from './services/assets-manager.service';
import { NonAssetsMarkerManagerService } from './services/non-assets-manager.service';
import { SinglePointBoundsService } from './services/single-point-bounds.service';
import { MapCustomOptionsService } from './custom-options/custom-options.service';
import { CurrentPositionService } from './services/current-position.service';
import { ScriptLoaderService } from '../common/script-loader.service';
import { TooltipService } from './services/tooltip.service';
import { SnapToRoadsService } from './services/snap-to-roads.service';
import { MarkerCreatorService } from './services/marker-creator.service';
import { InrixOverlaysService } from './services/inrix-overlays.service';
import { ResizeService } from '../shared/resize.service';

// TYPES
// import {} from '@types/googlemaps';
import { Asset, TraceEvent, Trace, AssetExtended } from '../common/entities/entity-types';
import { GoogleMap, MapModeOptions } from './types/map.types';
import { MapCustomOptionsGroups } from './custom-options/custom-options.types';
import { ChargePointMarker } from './types/charge-point.types';
import { BaseCustomMarker } from './types/custom-marker.types';
import { GoogleMapsConfigurationService } from './services/google-maps-configuration.service';

@Component({
    selector: 'google-map',
    templateUrl: './maps.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./maps.less'],
    // Providing the service at the component level ensures
    // that every instance of the component gets its own,
    // private instance of the service. No markers overwriting.
    providers: [MapsLoaderService, CurrentPositionService, MarkerCreatorService,
                AssetsMarkerManagerService, NonAssetsMarkerManagerService, ChargePointsMarkerManagerService,
                MapCustomOptionsService, SinglePointBoundsService, TooltipService,
                HeatmapService, InfoWindowService, ScriptLoaderService, SnapToRoadsService,
                GeocoderService, ProgressBarService, InrixOverlaysService]
})


/**
 * Identifiers for common MapTypes. Specify these by value, or by using the
 * constant's name. For example, 'satellite' or google.maps.MapTypeId.SATELLITE.
 */
export class MapsComponent implements AfterViewInit, OnChanges {
    @Input() public selected: Asset[];
    @Input() public traces: Trace[];
    @Input() public events: TraceEvent[];
    @Input() public assets: AssetExtended[];
    @Input() public chargePoints: Array<BaseCustomMarker<ChargePointMarker>>;
    @Input() public customOptionsGroups: MapCustomOptionsGroups[];
    @Input() public mode: MapModeOptions;
    @Input() public loading: boolean;
    @Output() public mapInitializing: EventEmitter<boolean> = new EventEmitter<boolean>();

    @ViewChild('myMap', { static: true }) public mapElement: ElementRef;
    public mapID: string;
    public mapHeight: number;
    private defaultHeight: number = 300;
    private map: google.maps.Map;
    private resizedFinished: any;

    constructor(
        public progressBarService: ProgressBarService,
        private configService: GoogleMapsConfigurationService,
        private mapsLoaderService: MapsLoaderService,
        private infoWindowService: InfoWindowService,
        private geocoderService: GeocoderService,
        private assetMarkerManagerService: AssetsMarkerManagerService,
        private nonAssetMarkerManagerService: NonAssetsMarkerManagerService,
        private cpsMarkerManagerService: ChargePointsMarkerManagerService,
        private heatmapService: HeatmapService,
        private inrixService: InrixOverlaysService,
        // private currentPositionService: CurrentPositionService,
        private changeDetectionRef: ChangeDetectorRef,
        private mapContainer: ElementRef,
        private resizeService: ResizeService,
    ) {
        this.mapInitializing.emit(false);
        this.mapHeight = this.defaultHeight;
        this.mapsLoaderService.map.subscribe((mapInstance: GoogleMap) => {
            this.map = mapInstance.map;
            this.updateMap();
        });
    }


    // listen for the window resize event & trigger Google Maps to update too
    @HostListener('window:resize')
    public recenter() {
        const that = this;
        clearTimeout(this.resizedFinished);
        this.resizedFinished = setTimeout(() => {
            google.maps.event.trigger(that.map, 'resize');
            if (that.mode === MapModeOptions.marker) {
                that.nonAssetMarkerManagerService.center();
                that.assetMarkerManagerService.center();
            } else {
                that.heatmapService.center();
            }
            that.setMapHeight();
        }, 250);
    }


    public ngAfterViewInit() {
        if (this.mode === undefined) {
            throw new Error('Map mode is not specified');
        }
        this.mapID = this.mapContainer.nativeElement.id + '_cnt';

        this.configService.configure().then(() => {
            // when map finished loading
            this.mapsLoaderService.load(this.mapElement.nativeElement);
            this.setMapHeight();
            this.mapInitializing.emit(true);
        });

        // manually trigger change detection for the current component.
        // otherwise "Expression ___ has changed after it was checked" error
        this.changeDetectionRef.detectChanges();
    }


    public ngOnChanges(changes: SimpleChanges) {
        if (!this.map) {
            return;
        }

        const loadingChange = changes['loading'];
        if (loadingChange) {
            if (loadingChange.currentValue === true) {
                this.progressBarService.startLoading();
            } else {
                this.progressBarService.stopLoading();
            }
        }

        // if any of the assets changed value the redraw map
        const eventsChange = changes['events'];
        const tracesChange = changes['traces'];
        const assetsChange = changes['assets'];
        const chargePointsChange = changes['chargePoints'];

        if (eventsChange || tracesChange || assetsChange || chargePointsChange) {
            this.updateMap();
        }

        // if the changed value is null, this is a reset
        const pointChange = changes['selected'];
        if (pointChange) {
             // otherwise, find the marker and open its info window to indicate selection
             this.updateMap();
        }
    }


    public resizeBy(height: number) {
        this.mapHeight = height;
        this.resizeService.trigger();
    }


    /*
    * updates markers on the map
    */
    private updateMap() {
        this.infoWindowService.close();
        if ((!this.traces || this.traces.length === 0) &&
            (!this.events || this.events.length === 0) &&
            (!this.assets || this.assets.length === 0) &&
            (!this.chargePoints || this.chargePoints.length === 0)
        ) {
            this.showCountry();
        } else {
            if (this.mode === MapModeOptions.marker) {
                this.assetMarkerManagerService.set(this.assets, this.selected);
                this.nonAssetMarkerManagerService.set(this.traces, this.events, this.selected);
                this.cpsMarkerManagerService.set(this.chargePoints);
            } else {
                this.heatmapService.set(this.events);
            }
        }
    }


    /*
    * Displays the user's country in case of nothing else to show
    */
    private showCountry() {
        this.assetMarkerManagerService.clearMarkers();
        this.nonAssetMarkerManagerService.clear();
        this.cpsMarkerManagerService.clear();
        this.geocoderService.geocodeCountry().then((countryBounds: google.maps.LatLngBounds) => {
            this.map.fitBounds(countryBounds);       // auto-zoom
            this.map.panToBounds(countryBounds);     // auto-center
        });
    }

    private setMapHeight() {
        const map = document.getElementById(this.mapID);
        if (map) {
            this.mapHeight = document.getElementById(this.mapID).clientHeight;
        }
    }
}
