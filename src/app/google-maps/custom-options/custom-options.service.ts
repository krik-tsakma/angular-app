// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Subject } from 'rxjs';

// SERVICES
import { MapsLoaderService } from '../services/maps-loader.service';

// TYPES
import { GoogleMap } from '../types/map.types';
import { MapCustomOption, MapCustomOptionsGroups, MapCustomOptions } from './custom-options.types';

@Injectable()
export class MapCustomOptionsService {
    public optionChanged: Subject<MapCustomOption> = new Subject<MapCustomOption>();
    public options: MapCustomOption[] = [
        {
            id: MapCustomOptions.hideLabels,
            term: 'map.custom_options.hide_labels',
            name: 'hideLabels',
            enabled: false,
            checked: false,
            group: MapCustomOptionsGroups.basic
        },
        {
            id: MapCustomOptions.freezeMap,
            term: 'map.custom_options.freeze_map',
            name: 'freezeMap',
            enabled: false,
            checked: false,
            group: MapCustomOptionsGroups.basic
        },
        {
            id: MapCustomOptions.clusterAssets,
            term: 'map.custom_options.cluster_assets',
            name: 'clusterAssets',
            enabled: false,
            checked: true,
            group: MapCustomOptionsGroups.basic
        }, 
        {
            id: MapCustomOptions.connectTheDots,
            term: 'map.custom_options.connect_the_dots',
            description: 'map.custom_options.connect_the_dots_description',
            name: 'connectTheDots',
            enabled: false,
            checked: false,
            group: MapCustomOptionsGroups.trip
        },
        {
            id: MapCustomOptions.snapToRoads,
            term: 'map.custom_options.snap_to_roads',
            description: 'map.custom_options.snap_to_roads_description',
            name: 'snapToRoads',
            enabled: false,
            checked: false,
            group: MapCustomOptionsGroups.trip
        },
        {
            id: MapCustomOptions.traffic,
            term: 'map.custom_options.traffic',
            description: 'map.custom_options.traffic_description',
            name: 'traffic',
            enabled: false,
            checked: false,
            group: MapCustomOptionsGroups.traffic
        },
        {
            id: MapCustomOptions.accident,
            term: 'map.custom_options.accident',
            description: 'map.custom_options.accident_description',
            name: 'accident',
            enabled: false,
            checked: false,
            group: MapCustomOptionsGroups.traffic
        },
        {
            id: MapCustomOptions.construction,
            term: 'map.custom_options.construction',
            description: 'map.custom_options.construction_description',
            name: 'construction',
            enabled: false,
            checked: false,
            group: MapCustomOptionsGroups.traffic
        },
        {
            id: MapCustomOptions.congestion,
            term: 'map.custom_options.congestion',
            description: 'map.custom_options.congestion_description',
            name: 'congestion',
            enabled: false,
            checked: false,
            group: MapCustomOptionsGroups.traffic,
        },
        {
            id: MapCustomOptions.roadWeather,
            term: 'map.custom_options.weather',
            description: 'map.custom_options.weather_description', 
            name: 'weather',
            enabled: false,
            checked: false,
            group: MapCustomOptionsGroups.traffic
        },
        {
            id: MapCustomOptions.camera,
            term: 'map.custom_options.traffic_cameras',
            description: 'map.custom_options.traffic_cameras_description', 
            name: 'traffic_camera',
            enabled: false,
            checked: false,
            group: MapCustomOptionsGroups.traffic,
        }
    ];

    private headerText: string;
    
    constructor(private mapsLoaderService: MapsLoaderService) {
        // when map finished loading
        mapsLoaderService.map.subscribe((mapInstance: GoogleMap) => {
            const control: HTMLElement = document.getElementById('map-custom-options_' + mapInstance.mapID); 
            if (control) {
                mapInstance.map.controls[google.maps.ControlPosition.TOP_LEFT].push(control);
                control.style.display = 'block';
            }
        });
    }

    public onChangeOption(option: MapCustomOption) {
        if (!option) {
            return;
        }
        option.checked = !option.checked;
        this.optionChanged.next(option);
    }

    public getOptionStatus(id: MapCustomOptions) {
        const option = this.options.find((x) => x.id === id);
        return option ? option.checked : false;
    }
}
