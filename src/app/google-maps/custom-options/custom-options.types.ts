import { KeyName } from '../../common/key-name';

export interface MapCustomOption extends KeyName {
    checked: boolean;
    description?: string;
    group: MapCustomOptionsGroups;
}

export enum MapCustomOptions {
    hideLabels = 1,
    freezeMap = 2,
    clusterAssets = 3,
    connectTheDots = 4,
    snapToRoads = 5,
    traffic = 6,
    accident = 7,
    construction = 8,
    congestion = 9,
    roadWeather = 10,
    camera = 11,
}

export enum MapCustomOptionsGroups {
    basic,
    trip,
    traffic,
}
