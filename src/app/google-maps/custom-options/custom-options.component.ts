// FRAMEWORK
import {
    Component, AfterViewInit, ViewEncapsulation, ChangeDetectorRef,
    ViewChild, ElementRef, Input, AfterViewChecked
} from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

// SERVICES
import { MapCustomOptionsService } from './custom-options.service';

// TYPES 
import { MapCustomOption, MapCustomOptionsGroups } from './custom-options.types';

@Component({
    selector: 'map-custom-options',
    templateUrl: './custom-options.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./custom-options.less'],
    animations: [
        trigger('openClose', [
            state('collapsed',
              style({ display: 'none' })),
            state('expanded',
              style({ display: 'flex' })),
            transition('collapsed <=> expanded', [
              animate(10)
            ])
          ]),
      ]
})


export class MapCustomOptionsComponent implements AfterViewInit, AfterViewChecked {
    @Input() public groups: MapCustomOptionsGroups[];

    public mapBasicOptions: MapCustomOption[] = null;
    public mapTripOptions: MapCustomOption[] = null;
    public mapTrafficOptions: MapCustomOption[] = null;
    public clusters = MapCustomOptionsGroups;
    public basicOptionsStatus = 'expanded';
    public tripOptionsStatus = 'expanded';
    public trafficOptionsStatus = 'expanded';
    public expanded: boolean = false;
    
    constructor(
        private service: MapCustomOptionsService,
        private changeDetectionRef: ChangeDetectorRef,
    ) {
       // foo
    }

    public ngAfterViewInit() {
        if (this.groups.find((x) => Number(x) === MapCustomOptionsGroups.basic) !== undefined) {
            this.mapBasicOptions = this.service.options.filter((x) => x.group === MapCustomOptionsGroups.basic);
        }

        if (this.groups.find((x) => Number(x) === MapCustomOptionsGroups.trip) !== undefined) {
            this.mapTripOptions = this.service.options.filter((x) => x.group === MapCustomOptionsGroups.trip);
        }

        if (this.groups.find((x) => Number(x) === MapCustomOptionsGroups.traffic) !== undefined) {
            this.mapTrafficOptions = this.service.options.filter((x) => x.group === MapCustomOptionsGroups.traffic);
        }
    }
    public ngAfterViewChecked() {
        // manually trigger change detection for the current component.
        // otherwise "Expression ___ has changed after it was checked" error   
        this.changeDetectionRef.detectChanges();
    }

    public toggleCluster(cluster: MapCustomOptionsGroups) {
        switch (cluster) {
            case MapCustomOptionsGroups.basic:
                this.basicOptionsStatus = this.basicOptionsStatus === 'expanded' ? 'collapsed' : 'expanded';
                break;
            case MapCustomOptionsGroups.trip:
                this.tripOptionsStatus = this.tripOptionsStatus === 'expanded' ? 'collapsed' : 'expanded';
                break;
            case MapCustomOptionsGroups.traffic:
                this.tripOptionsStatus = this.trafficOptionsStatus === 'expanded' ? 'collapsed' : 'expanded';
                break;
        }
    }

    public toggleBar() {
        this.expanded = !this.expanded;
    }
}
