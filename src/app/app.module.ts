// FRAMEWORK
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient, HttpHandler } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { LoginModule } from './core/login/login.module';
import { CompanyLogoModule } from './core/company-logo/company-logo.module';
import { UserMenuModule } from './core/user-menu/user-menu.module';

// COMPONENTS
import { AppComponent } from './core/app.component';
import { AccessDeniedComponent } from './core/error-pages/access-denied/access-denied.component';
import { PageNotFoundComponent } from './core/error-pages/404error/pageNotFound.component';
import { ForgotPasswordDialogComponent } from './core/forgot-password/forgot-password.component';
import { CreateAccountDialogComponent } from './core/create-account/create-account.component';
import { UnexpectedErrorComponent } from './core/error-pages/unexpected-error/unexpected-error.component';

// SERVICES
import { StoreManagerService } from './shared/store-manager/store-manager.service';
import { AuthService } from './auth/authService';
import { AuthGuard } from './auth/authGuard';
import { AuthHttp } from './auth/authHttp';
import { PreviousRouteService } from './core/previous-route/previous-route.service';
import { TranslateStateService } from './core/translateStore.service';

import { Config } from './config/config';
import 'hammerjs/hammer.min.js';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './multiTranslateHttpLoader';


export function createTranslateLoader(httpClient: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(httpClient, ['./assets/i18n/', './assets/i18n/core/reset-password/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

export function authServiceLoader(handler: HttpHandler, config: Config, storeManager: StoreManagerService) {
    return new AuthHttp(handler, config, storeManager);
}

@NgModule({
    imports: [
        LoginModule,
        SharedModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient, TranslateStateService],
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
        }),
        AppRoutingModule,
        CompanyLogoModule,
        UserMenuModule
    ],
    declarations: [
        PageNotFoundComponent,
        AccessDeniedComponent,
        UnexpectedErrorComponent,
        ForgotPasswordDialogComponent,
        CreateAccountDialogComponent,
        AppComponent
    ],
    providers: [
        AuthGuard,
        StoreManagerService,
        Config,
        AuthService,
        {
            provide: AuthHttp,
            useFactory: (authServiceLoader),
            deps: [HttpHandler, Config, StoreManagerService]
        },
        PreviousRouteService,
        TranslateStateService
    ],
    entryComponents: [
        ForgotPasswordDialogComponent,
        CreateAccountDialogComponent,
    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule {}
