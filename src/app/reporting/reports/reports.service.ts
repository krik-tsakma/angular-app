﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams,  HttpResponse } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { Report, ReportsList, ReportResponseCodeOptions } from './reports-types';
import { ReportExecuteRequest, ReportExecuteFormat, ExecuteHtmlReportResponse } from './execute/execute-report-types';
import { PeriodTypes, PeriodTypeOption } from '../../common/period-selector/period-selector-types';

@Injectable()
export class ReportsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/Reports';
        }

    // Get all the score definitions
    public get(): Observable<ReportsList> {
        return this.authHttp
            .get(this.baseURL);
    }

    public saveReport(report: Report): Observable<HttpResponse<ReportResponseCodeOptions | null | { id: number }>> {
        return this.authHttp
            .post(this.baseURL, report, { observe: 'response' });
    }

    public updateReport(report: Report): Observable<HttpResponse<ReportResponseCodeOptions | null>> {
        return this.authHttp
            .put(this.baseURL, report, { observe: 'response' });
    }

    public deleteReport(id: string): Observable<{}> {
        return this.authHttp
            .delete(this.baseURL + '/' + id);
    }

    public getByID(id: number): Observable<Report> {
        return this.authHttp
            .get(this.baseURL + '/' + id);
    }

    // HTML, PDF or XLSX
    public generateReport(data: ReportExecuteRequest): Observable<{}> {
        const resType = data.Format === ReportExecuteFormat.HTML ? 'json' : 'blob';
        return this.authHttp
            .post(this.config.get('apiUrl') + '/api/reporting/' + data.StandardType, data, { observe: 'response', responseType: resType })
            .pipe(
                map((res) => {
                    if (data.Format === ReportExecuteFormat.HTML) {
                        return res.body as ExecuteHtmlReportResponse;
                    } else {
                        return res.body;
                    }
                })
            );
    }

}
