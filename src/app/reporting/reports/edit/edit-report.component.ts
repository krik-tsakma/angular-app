﻿// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { AuthService } from '../../../auth/authService';
import { ReportsService } from '../reports.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { Config } from '../../../config/config';

// TYPES
import { Report, ReportBasicInfo, ReportUtils, ReportResponseCodeOptions } from '../reports-types';

// LODASH
import isEqual from 'lodash-es/isEqual';



@Component({
    selector: 'edit-report',
    templateUrl: 'edit-report.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-report.less']
})

export class EditReportComponent implements OnInit, OnDestroy {
    public loading: boolean;
    public report: Report;
    public editMode: boolean = false;
    public placeholder: string;
    public pageTitle: string;
    private _reportFromServer: Report;
    private serviceSubscriber: Subscription;

    constructor(
        public translate: CustomTranslateService,
        public authService: AuthService,
        public service: ReportsService,
        public userOptions: UserOptionsService,
        public router: Router,
        public activatedRoute: ActivatedRoute,
        public snackBar: SnackBarService,
        private previousRouteService: PreviousRouteService,
        private unsubscribe: UnsubscribeService,
        private configService: Config) {
            this.placeholder = translate.instant('reports.placeholder');
    }



    public ngOnInit() {
        this.activatedRoute
            .params
            .subscribe((params: {
                mode: string;
                id: number;
            }) => {
                if (params.id) {
                    this.get(params.id);
                }
            });
    }



    public ngOnDestroy() {
        // Clean sub to avoid memory leak
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }



    // Edit Report Name
    public editName() {
        this.editMode = !this.editMode;
    }



    public clearInput(e: Event) {
        if (e) {
            // do not bubble event otherwise focus is on the textbox
            e.stopPropagation();
        }
        // clear selection
        this.report.basicInfo.name  = '';
    }



    // When the user checks/unchecks a property or a score
    public onChangeOption(option) {
        option.value = !option.value;
        this.formInvalid();
    }



    // event trigger every time we write in report's name input
    public onKey(event: any) {
        this.report.basicInfo.name = event.target.value;
        this.formInvalid();
    }



    // Enable/Disable save button
    public formInvalid(): boolean {
        if (this.report.basicInfo.standard) {
            if (this.editMode && this.report.basicInfo.name.length > 0) {
                return false;
            } else {
                return true;
            }

        } else {
            if (isEqual(this._reportFromServer.definition.columns, this.report.definition.columns) &&
                isEqual(this._reportFromServer.definition.aggregations, this.report.definition.aggregations) &&
                isEqual(this._reportFromServer.definition.graphs, this.report.definition.graphs) &&
                (this.report.basicInfo.name === this._reportFromServer.basicInfo.name || this.report.basicInfo.name.length === 0)) {
                return true;
            } else {
                return false;
            }
        }
    }



    /*
    * Saves the report.
    * Create a new one when it comes to standard report,
    * edits an existing when it comes to custom report type
    */
    public save() {
        // POST
        if (this.report.basicInfo.standard) {
            this.service
                .saveReport(this.report)
                .subscribe((res: HttpResponse<ReportResponseCodeOptions | null | { id: number }>) => {
                    if (res.ok && res.body) {
                        if (typeof res.body === 'object' && res.body.id) {
                            this.snackBar.open('form.save_success', 'form.actions.ok');
                            this.back();
                        }
                        if (typeof res.body === 'number') {
                            this.showResponseMessage(+res.body);
                        }
                    }
                }, (err) => {
                    if (err.ok === false) {
                        this.snackBar.open('form.errors.unknown', 'form.actions.close');
                    }
                });
        } else { // PUT
            this.service
                .updateReport(this.report)
                .subscribe((res: HttpResponse<number | null>) => {
                    this.showResponseMessage(+res.body);
                }, (err) => {
                    if (!err.ok) {
                        if (err.status === 404) {
                            this.snackBar.open('reposts.response_code_options.not_found', 'form.actions.close');
                            return;
                        }
                        this.snackBar.open('form.errors.unknown', 'form.actions.close');
                    }
                });
        }
    }


    public showResponseMessage(code: ReportResponseCodeOptions) {
        switch (code) {
            case ReportResponseCodeOptions.Ok: {
                this.snackBar.open('form.save_success', 'form.actions.ok');
                this.back();
                break;
            }
            case ReportResponseCodeOptions.NameExists: {
                this.snackBar.open('reposts.response_code_options.name_exist', 'form.actions.close');
                break;
            }
            case ReportResponseCodeOptions.InvalidDefinition: {
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
                break;
            }
            case ReportResponseCodeOptions.CustomizeStandardError: {
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
                break;
            }
            case ReportResponseCodeOptions.NameMissing: {
                this.snackBar.open('reposts.response_code_options.name_missing', 'form.actions.close');
                break;
            }
            case ReportResponseCodeOptions.NotCustomizable: {
                this.snackBar.open('reposts.response_code_options.not_customizable', 'form.actions.close');
                break;
            }
            case ReportResponseCodeOptions.UnknownError: {
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
                break;
            }
            default: {
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
                break;
            }

        }
    }



    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }

    /*
    * Gets reports data
    * @param id The report id
    */
    private get(id: number) {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((res: Report) => {
                this.loading = false;
                if (res.basicInfo.standard) {
                    res.basicInfo.name = ReportUtils.getReportName(res.basicInfo, this.translate);
                }
                this.report = res;
                this._reportFromServer = JSON.parse(JSON.stringify(res));

                if (!this.report.basicInfo.standard) {
                    this.editMode = true;
                } else {
                    this.editMode = false;
                }
                this.setPageTitle(res.basicInfo);
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            });
    }

    /*
    * Sets the page's title to the following format: {Report type}: {Report action}
    * @param basicInfo Report's basic information
    */
    private setPageTitle(basicInfo: ReportBasicInfo) {
        this.translate
            .get(['reports.standard_reports', 'reports.custom_reports', 'form.actions.edit', 'customize_layout.customize'])
            .subscribe((t) => {
                const base = basicInfo.standard ? t['reports.standard_reports'] :  t['reports.custom_reports'];
                const action = basicInfo.standard ? t['customize_layout.customize'] :  t['form.actions.edit'];
                this.pageTitle = base + ': ' + action;
        });
    }

}
