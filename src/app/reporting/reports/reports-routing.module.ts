﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../../auth/authGuard';

import { ViewReportsComponent } from './view/view-reports.component';
import { EditReportComponent } from './edit/edit-report.component';
import { ExecuteReportComponent } from './execute/execute-report.component';


const ReportsRoutes: Routes = [
    {
        path: '',
        component: ViewReportsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        component: EditReportComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'execute/:id',
        component: ExecuteReportComponent,
        canActivate: [AuthGuard],
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(ReportsRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ReportsRoutingModule { }
