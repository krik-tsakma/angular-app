﻿// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Router } from '@angular/router';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { AuthService } from '../../../auth/authService';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { ReportsService } from '../reports.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { Config } from '../../../config/config';

// TYPES
import { ReportsList, ReportBasicInfo, ReportTypes } from '../reports-types';
import { ConfirmationDialogParams } from '../../../shared/confirm-dialog/confirm-dialog-types';
import { KeyName } from '../../../common/key-name';
import { ModuleOptions } from '../../../auth/acl.types';

@Component({
    selector: 'view-reports',
    templateUrl: './view-reports.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./view-reports.less'],
    animations: [
        trigger(
            'openClose',
            [
                state('collapsed', style({ display: 'none', opacity: '0', height: '0px' })),
                state('expanded', style({ display: 'block', opacity: '1', height: '*' })),
                transition('collapsed <=> expanded', animate('500ms ease-out'))
            ]
        ),
        trigger(
            'rotate',
            [
                state('down', style({ transform: 'rotate(90deg)'})),
                transition('* <=> down', animate('500ms ease-out'))
            ]
        )
    ]
})

export class ViewReportsComponent implements OnInit, OnDestroy {
    public loading: boolean;
    public standardReportsData: ReportBasicInfo[];
    public customReportsData: ReportBasicInfo[];
    public reportAssetTypes: KeyName[];
    private serviceSubscriber: Subscription;

    constructor(
        public router: Router,
        public userOptions: UserOptionsService,
        private previousRouteService: PreviousRouteService,
        private authService: AuthService,
        private service: ReportsService,
        private confirmDialogService: ConfirmDialogService,
        private snackBar: SnackBarService,
        private unsubscribe: UnsubscribeService,
        private configService: Config) {
            // foo
    }


    public ngOnInit(): void {
        this.reportAssetTypes = this.getReportSections();
        this.get();
    }

    public ngOnDestroy(): void {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }


    // Navigate function either for edit or execute
    public navigateFor(action, id): void {
        this.router.navigate(['/reports', action, id], { skipLocationChange: !this.configService.get('router') });
    }


    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }


    // Before permanent delete, opens the dialog and asks for confirmation
    public openDialog(item) {
        const confirmReq = {
            title: 'form.actions.delete',
            message: 'form.warnings.are_you_sure',
            submitButtonTitle: 'form.actions.delete'
        } as ConfirmationDialogParams;

        this.confirmDialogService
            .confirm(confirmReq)
            .subscribe((result) => {
                if (result === 'yes') {
                    this.deleteReport(item.id);
                }
            });
    }


    private get() {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.service
            .get()
            .subscribe((res: ReportsList) => {
                this.loading = false;
                this.standardReportsData = res.results.filter((report) => {
                    return report.standard;
                });
                this.customReportsData = res.results.filter((report) => {
                    return !report.standard;
                });
            },
            () => {
                    this.loading = false;
                    this.snackBar.open('data.error', 'form.actions.close');
                });
    }

    // Delete score method
    private deleteReport(id: string) {
        this.service
            .deleteReport(id)
            .subscribe((res: any) => {
                if (res.code === 1) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get();
                }
            },
            () => {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
        );
    }

    private getReportSections(): KeyName[] {
       return [
            {
                id: ReportTypes.Vehicles,
                name: 'reports.types.vehicles',
                enabled: true
            },
            {
                id: ReportTypes.Drivers,
                name: 'reports.types.drivers',
                enabled: true
            },
            {
                id: ReportTypes.Users,
                name: 'reports.types.users',
                enabled: true
            },
            {
                id: ReportTypes.ChargePoints,
                name: 'reports.types.chargepoints',
                enabled: this.authService.userHasAccess(ModuleOptions.EVOperations, null)
            },
            // {
            //     id: ReportTypes.Locations,
            //     name: 'assets.locations',
            //     enabled: false
            // },
            // {
            //     id: ReportTypes.Routes,
            //     name: 'assets.routes',
            //     enabled: false
            // }
        ];
    }

}
