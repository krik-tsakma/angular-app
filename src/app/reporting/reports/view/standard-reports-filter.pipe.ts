﻿// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// SERVICES
import { AuthService } from '../../../auth/authService';

// TYPES
import { ReportBasicInfo, ReportTypes } from '../reports-types';
import { ModuleOptions } from '../../../auth/acl.types';

@Pipe({
    name: 'myStandarReportFilter',
    pure: false
})

export class StandarReportFilterPipe implements PipeTransform {
    constructor(private authService: AuthService) {
        // foo
    }

    public transform(standardReportsData: ReportBasicInfo[], filter: number): ReportBasicInfo[] {
        
        if (!standardReportsData || !filter.toString()) {           
            return standardReportsData;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return standardReportsData.filter((item) => {
            switch (item.type) {
                case ReportTypes.Vehicles: 
                case ReportTypes.Drivers: 
                    if (item.standardType === 'BookNGoBookings' || item.standardType === 'BookNGoTrips') {
                        return item.type === filter && this.authService.userHasAccess(ModuleOptions.Bookings, null);
                    }
                    return item.type === filter;
                case ReportTypes.ChargePoints: 
                    return item.type === filter && 
                                            (this.authService.userHasAccess(ModuleOptions.EVOperations, null) || 
                                            this.authService.userHasAccess(ModuleOptions.ChargePointManager, null));
                default:
                    return item.type === filter;
            }
        });
    }
}
