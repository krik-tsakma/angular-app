﻿import { PagerResults, PagerRequest } from '../../../common/pager';
import { PeriodTypes } from '../../../common/period-selector/period-selector-types';
import { ReportAssetTypes, ReportKeyValue } from '../reports-types';


export interface ReportExecuteRequest extends PagerRequest {
    Assets?: Array<ReportKeyValue<ReportAssetTypes, number>>;
    Options?: Array<ReportKeyValue<ReportOptions, any>>;
    OptionsDictionary?: { [key: string]: number };
    PeriodType: PeriodTypes;
    ReportID: number;        
    Since?: string;
    Till?: string;  
    FilePerAsset: boolean;
    Format?: ReportExecuteFormat;  
    StandardType?: string;
}

export enum ReportExecuteFormat {
    HTML = 1,
    MSExcel,
    PDF
}

export enum ReportOptions {
    Score = 1,
    ScoreRepresentation = 2,
    EnergyGranularity = 3,
    ViewGranularity = 4,
    EventPriority = 5,
    EventClass = 6,
    EventStatus = 7
}

export enum ReportScoreRepresentation {
    AtoH = 1,
    Number
}
export enum ReportEnergyGranularity {
    Consolidated = 1,
    VehicleType = 2
}

export enum ReportViewGranularity {
    Daily = 0,
    Weekly = 1,
    Monthly = 2
}

export interface ExecuteHtmlReportResponse extends PagerResults {
    contents: string;
}

export enum ReportAssetSelectionOptions {
    Me = 1,
    Assets = 2,
    Users = 3,
    ChargePoints = 4
}
