﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { ScoresListModule } from '../../../common/scores-list/scores-list.module';
import { EventsStatusesListModule } from '../../../common/events-statuses-list/events-statuses-list.module';
import { EvenstClassesListModule } from '../../../common/events-classes-list/events-classes-list.module';
import { AssetGroupsModule } from '../../../common/asset-groups/asset-groups.module';
import { PeriodSelectorModule } from '../../../common/period-selector/period-selector.module';
import { EventsPrioritiesListModule } from '../../../common/events-priorities-list/events-priorities-list.module';

// SERVICES
import { ReportsService } from '../reports.service';
import { SubscriptionsService } from '../../subscriptions/subscriptions.service';

// COMPONENTS
import { ExecuteReportComponent } from './execute-report.component';
import { ReportSubscriptionsListComponent } from './subscriptions-list/subscriptions-list.component';
import { ChargePointSearchComponent } from './charge-point-search/charge-point-search.component';

@NgModule({
    imports: [
        SharedModule,
        AssetGroupsModule,
        PeriodSelectorModule,
        EventsPrioritiesListModule,
        EvenstClassesListModule,
        EventsStatusesListModule,
        ScoresListModule,
    ],
    declarations: [
        ExecuteReportComponent,
        ReportSubscriptionsListComponent,
        ChargePointSearchComponent
    ],
    providers: [
        ReportsService,
        SubscriptionsService
    ]
})

export class ExecuteReportModule { }
