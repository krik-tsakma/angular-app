// FRAMEWORK
import {
    Component, Input, Output, ViewChild, ElementRef,
    Renderer, EventEmitter, ViewEncapsulation 
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// RXJS
import { Observable, Subscription, Subject, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

// SERVICES
import { ChargePointsService } from '../../../../admin/charge-points/charge-points.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';

// TYPES
import { SearchChargePointsResults } from './charge-point-search-types';
import { KeyName } from '../../../../common/key-name';
import { SearchTermRequest } from '../../../../common/pager';
import { ChargePointsResult } from '../../../../admin/charge-points/charge-points.types';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'charge-point-search',
    templateUrl: './charge-point-search.html',
    encapsulation: ViewEncapsulation.None,
    providers: [ChargePointsService]
})

export class ChargePointSearchComponent {
    @ViewChild('searchTerm', { static: true }) public fileInput: ElementRef;
    public searchTermControl = new FormControl(); 
    @Output() public onSelect = new EventEmitter<KeyName>();
    public loading: boolean = false;
    public inputSelected: string;
    public items: KeyName[];
    
    private serviceSubscriber: Subscription;

    constructor(
        private service: ChargePointsService,
        private renderer: Renderer,
        private unsubscribe: UnsubscribeService) {
       
        this.searchTermControl.valueChanges    
            .pipe(        
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing             
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term: string) => {
                    this.get(term);
                    return of([]);
                })
            )
            .subscribe(); 
    }

    public selected(term?: KeyName) {       
        this.onSelect.emit(term);   
        this.clearInput(null);   
    }

    public clearInput(e?: Event) {
        // do not bubble event otherwise focus is on the textbox
        if (e) {
            e.stopPropagation();
        }

        // clear selection   
        this.inputSelected = null;
        // clear selection
        this.searchTermControl.setValue(null);

        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.fileInput.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }

    // Fetch Location groups data
    private get(searchTerm: string) {
        if (!searchTerm || searchTerm.length < 2) {
            return;
        }

        this.loading = true;

        const req = {
            searchTerm: searchTerm,
            pageNumber: 1,
            pageSize: 20,
            sorting: 'name desc'
        } as SearchTermRequest;
   
        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.serviceSubscriber);

        // then start the new request
        this.serviceSubscriber = this.service
            .get(req)
            .subscribe(
                (response: ChargePointsResult) => {
                    this.loading = false;
                    const res = response.results.map((r) => {
                        return { 
                            id: r.id, 
                            name: r.name,
                            enabled: true
                        } as KeyName;
                    });
                    this.items = res;
                },
                (err) => {
                    this.loading = false;
                    this.items = [];
                }
            );
    }
}
