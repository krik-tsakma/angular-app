﻿import { KeyName } from '../../../../common/key-name';
import { PagerResults, PagerRequest } from '../../../../common/pager';

export interface SearchChargePointsResults extends PagerResults {
    results: KeyName[];
}
