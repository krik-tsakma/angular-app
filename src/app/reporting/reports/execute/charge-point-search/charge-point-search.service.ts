﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../../../../config/config';
import { AuthHttp } from '../../../../auth/authHttp';

// TYPES
import { SearchChargePointsResults } from './charge-point-search-types';
import { KeyName } from '../../../../common/key-name';

@Injectable()
export class ChargePointSearchService {

    constructor(
        private authHttp: AuthHttp,
        private config: Config) { }

    public search(term: string): Observable<KeyName[]> {
        // Return only if term.lengh is more than 1 letter
        if (!term || term.length < 2) {
            return;
        }
        const params = new HttpParams({
            fromObject: {
                SearchTerm: term                              
            }
        }); 

        return this.authHttp
            .get(this.config.get('apiUrl') + '/api/EvChargePointManager/Search', { params })
            .pipe(
                map((res) => {
                    const myResults = res as SearchChargePointsResults;
                    return myResults.results;
                })
            );
    } 

}
