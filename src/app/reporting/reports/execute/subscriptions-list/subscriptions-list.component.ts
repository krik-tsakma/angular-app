﻿
// FRAMEWORK
import {
    Component, Input, Output, EventEmitter
} from '@angular/core';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { SubscriptionsService } from '../../../subscriptions/subscriptions.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';

// TYPES
import { KeyName } from '../../../../common/key-name';
import { SubscriptionList, ReportSubscription } from '../../../subscriptions/subscriptions-types';

@Component({
    selector: 'report-subscriptions-list',
    templateUrl: 'subscriptions-list.html',
    styleUrls: ['subscriptions-list.less'],
})

export class ReportSubscriptionsListComponent {
    @Input() public modelData?: number;
    @Input() public disableAddBtn?: boolean;
    @Output() public modelDataChange: EventEmitter<number> =
        new EventEmitter<number>();
    @Output() public onAddToSubscription: EventEmitter<number> =
        new EventEmitter<number>();
   
    public data: KeyName[];
    public loading: boolean;
    public subscriber: Subscription;

    constructor(private snackBar: SnackBarService,
                private translator: CustomTranslateService,
                private service: SubscriptionsService
            ) {
        this.get();
    }


    // ========================
    // EVENTS
    // ========================

    public onChangeOption(id: number) {
        this.modelData = id;
        this.modelDataChange.emit(this.modelData);
    }
    
    public onAddTo() {
        this.onAddToSubscription.emit(this.modelData);
    }

    // ========================
    // DATA
    // ========================

    private get() {
        this.loading = true;
        this.service
            .get()
            .subscribe((res: SubscriptionList) => {
                this.loading = false;
                this.data = res.results.map((data) => {
                    data.enabled = true;
                    return data;
                }); 
                
                if (this.data.length === 0) {
                    this.data.push({
                        id: null,
                        name: this.translator.instant('data.empty_records'),
                        enabled: false
                    } as KeyName);
                }
                if (this.modelData === null || this.modelData === undefined) {
                    this.onChangeOption(this.data[0].id);
                }
            },
            (err) => {
                this.snackBar.open('data.error', 'form.actions.close');
                this.loading = false;
            });
    }
}
