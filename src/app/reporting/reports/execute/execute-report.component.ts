// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Router, ActivatedRoute } from '@angular/router';

// RXJS
import { Subscription } from 'rxjs';

// COMPONENTS
import { SearchMechanismComponent } from '../../../shared/search-mechanism/search-mechanism.component';
import { ChargePointSearchComponent } from './charge-point-search/charge-point-search.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { ReportsService } from '../reports.service';
import { SubscriptionsService } from '../../subscriptions/subscriptions.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { DriversService } from '../../../admin/drivers/drivers.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { Config } from '../../../config/config';

// TYPES
import { KeyName } from '../../../common/key-name';
import { ReportAssetTypes, Report, ReportBasicInfo, ReportDataTypes, ReportTypes, ReportKeyValue } from '../reports-types';
import { PeriodTypes } from '../../../common/period-selector/period-selector-types';
import { SearchType } from '../../../shared/search-mechanism/search-mechanism-types';
import { PeriodTypeOption } from '../../../common/period-selector/period-selector-types';
import {
    ReportExecuteRequest,
    ReportExecuteFormat,
    ReportOptions,
    ReportScoreRepresentation,
    ReportEnergyGranularity,
    ReportViewGranularity,
    ReportAssetSelectionOptions,
} from './execute-report-types';
import {
    ReportSubscription
} from '../../subscriptions/subscriptions-types';
import { DriverSettings } from '../../../admin/drivers/drivers.types';


// MOMENT
import moment from 'moment';
import { AssetGroupItem, AssetGroupTypes } from '../../../admin/asset-groups/asset-groups.types';


@Component({
    selector: 'execute-report',
    templateUrl: './execute-report.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./execute-report.less'],
    providers: [DriversService],
    animations: [
        trigger(
            'openClose',
            [
                state('collapsed', style({ display: 'none', opacity: '0', height: '0px' })),
                state('expanded', style({ display: 'block', opacity: '1', height: '*' })),
                transition('collapsed <=> expanded', animate('500ms ease-out'))
            ]
        ),
        trigger(
            'rotate',
            [
                state('down', style({ transform: 'rotate(90deg)' })),
                transition('* <=> down', animate('500ms ease-out'))
            ]
        )
    ]
})

export class ExecuteReportComponent implements OnInit, OnDestroy {
    @ViewChild(SearchMechanismComponent, { static: false }) public searchAsset: SearchMechanismComponent;
    @ViewChild(ChargePointSearchComponent, { static: false }) public chargePointSearch: ChargePointSearchComponent;
    public pageTitle: string;
    public windowReference: Window;

    // loaders
    public loading: boolean;
    public reportExecutionLoading: boolean;

    // report
    public report: Report;
    public request: ReportExecuteRequest;
    public reportDataTypes: any = ReportDataTypes;
    public reportTypes: any = ReportTypes;
    public reportAssetTypes: any = ReportAssetTypes;
    public periodSelectorOptions: PeriodTypes[];

    // report format
    public formatOptions: any = ReportExecuteFormat;
    public reportFormatSelection: ReportExecuteFormat;
    public reportFormatOptions: KeyName[];

    // report custom options
    public reportSpecificOptions: any = ReportOptions;
    public viewGranularityOptions: any = ReportViewGranularity;
    public energyGranularityOptions: any = ReportEnergyGranularity;
    public scoreRepresentationOptions: any = ReportScoreRepresentation;

    // assets
    public assetSelectionMode: ReportAssetSelectionOptions = ReportAssetSelectionOptions.Assets;
    public dataAssetGroups: AssetGroupItem[] = [];
    public selectedGroupOption: number = -2;
    public assetSelectionOptions: any = ReportAssetSelectionOptions;
    public assetTypes: any = SearchType;
    public assetGroupTypes: any = AssetGroupTypes;

    // user - driver
    public isDriver: boolean;
    public isManager: boolean;
    private driverData: KeyName;

    // Subscribers
    private reportSubscriber: Subscription;
    private executeReportSubscriber: Subscription;
    private generateReportButtonSubscriber: Subscription;

    constructor(
        private translate: CustomTranslateService,
        private userOptions: UserOptionsService,
        private service: ReportsService,
        private subscriptionService: SubscriptionsService,
        private activatedRoute: ActivatedRoute,
        private snackBar: SnackBarService,
        private router: Router,
        private previousRouteService: PreviousRouteService,
        private driversService: DriversService,
        private unsubscribe: UnsubscribeService,
        private configService: Config) {

        const user = userOptions.getUser();
        this.isDriver = user.isDriver;
        this.isManager = user.isManager;
    }


    public ngOnInit() {
        this.activatedRoute
            .params
            .subscribe((params: {
                mode: string;
                id: number;
            }) => {
                if (params.id) {
                    this.get(params.id);
                }
            });
    }



    public ngOnDestroy() {
        // Clean sub to avoid memory leak
        this.unsubscribe.removeSubscription([
            this.reportSubscriber,
            this.executeReportSubscriber,
            this.generateReportButtonSubscriber
        ]);
    }



    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }


    // ====================
    // PERIOD SELECTION
    // ====================

    // Emit when selection is made from period custom-dropdown menu
    public onChangePeriod(pto: PeriodTypeOption) {
        if (pto.id === PeriodTypes.Custom) {
            this.request.Since = pto.datetimeSince.format('YYYY-MM-DD HH:mm');
            this.request.Till = pto.datetimeTill.format('YYYY-MM-DD HH:mm');
        }
        this.request.PeriodType = pto.id;

        if (this.showReportSpecificOption(ReportOptions.ViewGranularity)) {
            this.request.OptionsDictionary[ReportOptions[ReportOptions.ViewGranularity]] = this.getDefaultGranularity(pto.id);
        }
    }


    // ====================
    // ASSET GROUPS
    // ====================

    public onAssetGroupsLoaded(groups: AssetGroupItem[]) {
        this.dataAssetGroups = groups;
    }

    // Emits every time an asset group is selected
    public onSelectAssetGroup(group: AssetGroupItem, type: ReportAssetTypes) {
        if (group.id < 0) {
            return;
        }

        if (!this.request.Assets) {
            this.request.Assets = [];
        }

        const assetGroup = this.request.Assets.find((g) => g.value === group.id && g.key === type);
        if (assetGroup !== undefined) {
            return;
        }

        // add to the selected list
        this.request.Assets.push({
            key: type,
            value: group.id,
            label: group.name
        });

        // disable the group from selecting
        group.enabled = false;

        // select the 'Select...' option
        this.selectedGroupOption = -2;
    }


    // ====================
    // ASSETS
    // ====================

    // Emits every time a search option is selected from search inputbox's available options
    public onAssetSelect(item: KeyName, type: ReportAssetTypes) {
        if (!item) {
            return;
        }

        if (!this.request.Assets) {
            this.request.Assets = [];
        }

        // Only if driver performance report do not allow more than one selection
        if (this.request.Assets.length > 0 && this.isDriverPerformance()) {
            return;
        }

        const asset = this.request.Assets.find((s) => s.value === item.id && s.key === type);
        if (asset === undefined) {
            this.request.Assets.push({
                key: type,
                value: item.id,
                label: item.name
            });
        }

        if (this.searchAsset) {
            this.searchAsset.clearInput();
        }

        if (this.chargePointSearch) {
            this.chargePointSearch.clearInput();
        }
    }


    // Remove asset from 'selected box' by clicking 'x' button on chip
    public removeAsset(asset: ReportKeyValue<ReportAssetTypes, number>) {
        if (this.isDriver && !this.isManager) {
            return;
        }
        // remove from selected list
        this.request.Assets = this.request.Assets.filter((s) => !(s.value === asset.value && s.key === asset.key));

        // enable again in group list
        if (asset.key === ReportAssetTypes.VehicleGroup ||
            asset.key === ReportAssetTypes.DriverGroup ||
            asset.key === ReportAssetTypes.LocationGroup ||
            asset.key === ReportAssetTypes.RouteGroup ||
            asset.key === ReportAssetTypes.UserGroup ) {

            const group = this.dataAssetGroups.find((g) => g.id === asset.value);
            if (group) {
                group.enabled = true;
            }
        }
    }

    // preselect the currently logged in driver as the selected asset
    public selectDriver() {
        if (this.assetSelectionMode === ReportAssetSelectionOptions.Me) {
            this.request.Assets = [];
            this.onAssetSelect(this.driverData, ReportAssetTypes.Driver);
        }
    }

    // ====================
    // SPECIFIC OPTIONS
    // ====================

    public showReportSpecificOption(option: ReportOptions) {
        return this.report
            .definition
            .options
            .find((x) => String(x.key) === ReportOptions[option]) ? true : false;
    }

    public getGranularityOptionAvailability(option: ReportViewGranularity): boolean {
        let isDisabled: boolean;
        const periodType = Number(this.request.PeriodType);
        switch (periodType) {
            case PeriodTypes.YearToDate:
            case PeriodTypes.FiscalYearToDate:
            case PeriodTypes.Last12Months:
            case PeriodTypes.Last13Months:
            case PeriodTypes.PreviousFiscalYear:
                if (option === ReportViewGranularity.Daily) {
                    isDisabled = true;
                }
                break;

            case PeriodTypes.LastMonth:
            case PeriodTypes.ThisMonth:
                if (option === ReportViewGranularity.Monthly) {
                    isDisabled = true;
                }
                break;

            case PeriodTypes.LastWeek:
            case PeriodTypes.ThisWeek:
                if (option === ReportViewGranularity.Weekly
                    || option === ReportViewGranularity.Monthly) {
                    isDisabled = true;
                }
                break;

            default:
                isDisabled = false;
                break;
        }
        return isDisabled;
    }

    // ====================
    // GENERATE REPORT
    // ====================

    public reportAssetsValid() {
        if (!this.request.Assets) {
            return false;
        }
        return this.request.Assets.length > 0;
    }

    public generate(format: ReportExecuteFormat, newReport: boolean) {
        this.request.Format = format;
        this.setReportRequestValues();
        this.request.pageSize = 20;

        // Safari is blocking any call to window.open() when made inside an async call.
        // The solution is to call window.open before making an asnyc call and set the location when the promise resolves.
        if (newReport) {
            if (this.request.Format === ReportExecuteFormat.HTML) {
                this.request.pageNumber = 1;
                this.windowReference = window.open();
                this.windowReference.document.write(`<div style="width: 100%; position: relative;" id="content"></div>`);
                this.windowReference.document.getElementById('content').innerHTML =
                    `<img style="margin: 5em auto; display: block;" src="assets/images/favicons/sycada-gif.gif" width="55" height="55" />`;
            } else {
                this.request.pageNumber = 0;
            }
        }

        this.reportExecutionLoading = true;
        this.unsubscribe.removeSubscription(this.generateReportButtonSubscriber);
        this.generateReportButtonSubscriber = this.service
            .generateReport(this.request)
            .subscribe((res: any) => {
                this.reportExecutionLoading = false;
                this.reportFormatSelection = null;
                // HTML MANIPULATION
                if (this.request.Format === ReportExecuteFormat.HTML) {
                    this.htmlManipulation(res, newReport);
                } else {
                    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                        // To IE or Edge browser, using msSaveorOpenBlob method to download file.
                        window.navigator.msSaveOrOpenBlob(res, this.getReportFileName(this.request.Format));
                    } else {
                        const link = document.createElement('a');
                        link.href = window.URL.createObjectURL(res);
                        link.download = this.getReportFileName(this.request.Format);
                        document.body.appendChild(link);
                        link.onclick = () => {
                            requestAnimationFrame(() => {
                                URL.revokeObjectURL(link.href);
                                setTimeout(() =>  link.remove(), 300);
                            });
                        };

                        link.click();
                    }
                }
                if (newReport) {
                    this.snackBar.open('reports.execute.success', 'form.actions.close');
                }
            }, (err) => {
                if (this.windowReference) {
                    this.windowReference.close();
                }
                this.reportFormatSelection = null;
                this.reportExecutionLoading = false;

                if (err.status === 413) {
                    this.snackBar.open('reports.execute.error.exceeding_max_number_of_rows', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            });
    }


    // ========================
    // SUBSCRIPTIONS
    // ========================

    public disableAddToSubscription() {
        if (this.loading ||
            this.reportExecutionLoading ||
            !this.reportAssetsValid() ||
            this.request.PeriodType === PeriodTypes.Custom) {
            return true;
        }
        return false;
    }

    // Function that add the generated report to the subscription
    public addToSubscription(subscriptionID: number) {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.reportSubscriber);

        this.setReportRequestValues();
        const req = {
            id: 0,
            report: this.report,
            subscriptionID,
            isDriver: false,
            filePerAsset: this.request.FilePerAsset,
            assets: this.request.Assets,
            options: this.request.Options,
            periodType: this.request.PeriodType
        } as ReportSubscription;

        this.reportSubscriber = this.subscriptionService
            .addToSubsription(req)
            .subscribe((res) => {
                this.loading = false;
                this.snackBar.open('reports.execute.subscriptions.add_success', 'form.actions.ok');
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            });
    }

    // ========================
    // REPORT
    // ========================

    private get(id: number) {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.reportSubscriber);
        this.reportSubscriber = this.service
            .getByID(id)
            .subscribe((res: Report) => {
                this.loading = false;
                this.report = res;
                this.reportFormatOptions = this.getReportFormatOptions();

                if (this.isDriver) {
                    this.setDriverOptions();
                }

                this.request = {
                    ReportID: res.basicInfo.id,
                    PeriodType: PeriodTypes.ThisWeek,
                    FilePerAsset: false,
                    StandardType: res.basicInfo.standardType,
                    Options: [],
                    pageNumber: 1,
                    pageSize: 20
                };

                this.setPageTitle(res.basicInfo);
                this.setInitialOptionValues();
                this.setAvailablePeriodTypes();
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            });
    }

    // ====================
    // HELPERS
    // ====================

    private htmlManipulation(res, newReport) {
        const extraCSS = `#content { width: 100%; position: relative; } .datatable { width: 100%; }`;
        const reportButtons = `<div id="buttons">
                                <div align="left">
                                    <button aria-label="previous" id="previous">
                                        ${ this.translate.instant('reports.executed.html.previous') }
                                    </button>
                                </div>
                                <div align="right">
                                    <button aria-label="next" id="next"> ${ this.translate.instant('reports.executed.html.next') }</button>
                                </div>
                            </div>`;
        const html = res.contents;

        if (newReport) {
            const styles = html.substr(html.indexOf('<style>') + 7, html.indexOf('</style>'));
            const replaceFonFace = styles.replace('\'roboto\'', '\'Roboto\', sans-serif');

            this.windowReference.document.head.innerHTML =
                `<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
                <style>${extraCSS} ${replaceFonFace} </style>`;
            this.windowReference.document.body.innerHTML = '';
            this.windowReference.document.write(`<div id="content"></div>`);
        }
        const contentDIV = this.windowReference.document.getElementById('content');
        contentDIV.innerHTML = '';
        contentDIV.innerHTML = html.slice(html.indexOf('</style>') + 8, html.length);

        const rowLength = contentDIV.getElementsByTagName('tbody')[0].getElementsByTagName('tr').length;

        // for non aggegated data, add pager, otherwise we get timeouts
        if (this.report.basicInfo.dataType === ReportDataTypes.Aggregation) {
            return;
        }
        if (newReport && rowLength > 0) {
            this.windowReference.document.write(reportButtons);
        }
        const btns = this.windowReference.document.getElementById('buttons');
        if (!btns) {
            return;
        }

        const prevBtn = this.windowReference.document.getElementById('previous');
        const nextBtn = this.windowReference.document.getElementById('next');
        btns.style.opacity = '1';
        if (res.pageNumber < 2) {
            prevBtn.setAttribute('disabled', 'true');
            if (rowLength < res.pageSize) {
                nextBtn.setAttribute('disabled', 'true');
            } else {
                nextBtn.removeAttribute('disabled');
            }
        } else if (res.pageSize !== rowLength) {
            prevBtn.removeAttribute('disabled');
            nextBtn.setAttribute('disabled', 'true');
        } else {
            prevBtn.removeAttribute('disabled');
            nextBtn.removeAttribute('disabled');
        }

        const onClick = (): any => {
            this.windowReference.document.getElementById('content').innerHTML =
                `<img class="img" src="assets/images/favicons/sycada-gif.gif" />`;
            if (btns) {
                btns.style.opacity = '0';
            }
            this.generate(ReportExecuteFormat.HTML, false);
        };

        prevBtn.addEventListener('click', () => {
            this.request.pageNumber = res.pageNumber - 1;
            onClick();
        });
        nextBtn.addEventListener('click', () => {
            this.request.pageNumber = res.pageNumber + 1;
            onClick();
        });
    }


    private setReportRequestValues() {
        // set the report specific options
        this.request.Options = [];
        if (this.request.OptionsDictionary) {
            Object.keys(this.request.OptionsDictionary).forEach((key) => {
                this.request.Options.push({
                    key: ReportOptions[key], value: this.request.OptionsDictionary[key]
                });
             });
        }
    }

    /*
    * Initialize a dictionary of report specific option, to enable two way binding
    */
    private setInitialOptionValues() {
        this.request.OptionsDictionary = {};
        if (this.showReportSpecificOption(ReportOptions.Score)) {
            this.request.OptionsDictionary[ReportOptions[ReportOptions.Score]] = 0;
        }

        if (this.showReportSpecificOption(ReportOptions.ScoreRepresentation)) {
            this.request.OptionsDictionary[ReportOptions[ReportOptions.ScoreRepresentation]] = ReportScoreRepresentation.AtoH;
        }

        if (this.showReportSpecificOption(ReportOptions.EnergyGranularity)) {
            this.request.OptionsDictionary[ReportOptions[ReportOptions.EnergyGranularity]] = ReportEnergyGranularity.Consolidated;
        }

        if (this.showReportSpecificOption(ReportOptions.ViewGranularity)) {
            this.request.OptionsDictionary[ReportOptions[ReportOptions.ViewGranularity]] = ReportViewGranularity.Daily;
        }

        if (this.showReportSpecificOption(ReportOptions.EventClass)) {
            this.request.OptionsDictionary[ReportOptions[ReportOptions.EventClass]] = null;
        }

        if (this.showReportSpecificOption(ReportOptions.EventStatus)) {
            this.request.OptionsDictionary[ReportOptions[ReportOptions.EventStatus]] = null;
        }

        if (this.showReportSpecificOption(ReportOptions.EventPriority)) {
            this.request.OptionsDictionary[ReportOptions[ReportOptions.EventPriority]] = null;
        }
    }

    /*
    * In case of trip report data type, period selector allows max timeframe of 1 month
    */
    private setAvailablePeriodTypes() {
        if (this.report.basicInfo.dataType === ReportDataTypes.Aggregation) {
            this.periodSelectorOptions = [
                                            PeriodTypes.FiscalYearToDate,
                                            PeriodTypes.Last12Months,
                                            PeriodTypes.Last13Months,
                                            PeriodTypes.LastMonth,
                                            PeriodTypes.LastWeek,
                                            PeriodTypes.PreviousFiscalYear,
                                            PeriodTypes.ThisMonth,
                                            PeriodTypes.ThisWeek,
                                            PeriodTypes.YearToDate
                                        ];
        } else {
            this.periodSelectorOptions = [
                PeriodTypes.LastMonth,
                PeriodTypes.LastWeek,
                PeriodTypes.ThisMonth,
                PeriodTypes.ThisWeek,
                PeriodTypes.Custom
            ];
        }
    }

    /*
    * Set the default granularity options
    */
    private getDefaultGranularity(option: PeriodTypes): number {
        let granularity: number;
        switch (option) {
            case PeriodTypes.YearToDate:
            case PeriodTypes.FiscalYearToDate:
            case PeriodTypes.Last12Months:
            case PeriodTypes.Last13Months:
            case PeriodTypes.PreviousFiscalYear:
                granularity = ReportViewGranularity.Monthly;
                break;

             case PeriodTypes.LastMonth:
             case PeriodTypes.ThisMonth:
                granularity = ReportViewGranularity.Weekly;
                break;

              case PeriodTypes.LastWeek:
              case PeriodTypes.ThisWeek:
                granularity = ReportViewGranularity.Daily;
                break;

            default:
                granularity = ReportViewGranularity.Daily;
                break;
        }
        return granularity;
    }

    /*
    * In case the user is driver preselect him/her in the asset list
    */
    private setDriverOptions() {
        this.assetSelectionMode = ReportAssetSelectionOptions.Me;
        const u = this.userOptions.getUser();
        this.driversService
            .getByID(u.driverId)
            .subscribe((res: DriverSettings) => {
                this.driverData = {
                    id: res.id,
                    name: res.firstname + ' ' + res.lastname
                } as KeyName;
                this.onAssetSelect(this.driverData, ReportAssetTypes.Driver);
            }, (err) => {
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
        });
    }


    /*
    * Sets the page's title to the following format: {Report type}: {Report name}
    * @param basicInfo Report's basic information
    */
    private setPageTitle(basicInfo: ReportBasicInfo) {
        this.translate
            .get(['reports.standard_reports', 'reports.custom_reports', 'reports.execute.title'])
            .subscribe((t) => {
                const base = basicInfo.standard ? t['reports.standard_reports'] : t['reports.custom_reports'];
                this.pageTitle = base + ': ' + t['reports.execute.title'];
        });
    }


    /*
    * Gets filename of the produced report: {Report type}_{Now time}_{filextension}
    * @param format Report's selected format (PDF or XLSX)
    */
    private getReportFileName(format: ReportExecuteFormat): string {
        const fileExtension = this.request.FilePerAsset
            ? 'zip'
            : format === ReportExecuteFormat.PDF
                ? 'pdf'
                : 'xlsx';

        return `${this.report.basicInfo.standardType}_${moment().format('YYYYMMDDHHmm')}.${fileExtension}`;
    }

    /*
    * Gets the possible report type extensions
    */
    private getReportFormatOptions(): KeyName[] {
        const selectTerm = this.translate.instant('form.actions.select');
        return [
            {
                id: null,
                name: selectTerm + '...',
                term: 'form.actions.select',
                enabled: false,
            },
            {
                id: ReportExecuteFormat.PDF,
                name: 'PDF',
                enabled: true
            },
            {
                id: ReportExecuteFormat.MSExcel,
                name: 'MS-Excel',
                enabled: !this.isDriverPerformance()
            }
        ];
    }

    private isDriverPerformance(): boolean {
        return this.report.basicInfo.standardType === 'DriverPerformance';
    }
}
