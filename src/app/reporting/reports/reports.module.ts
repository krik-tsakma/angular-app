﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { ReportsRoutingModule } from './reports-routing.module';
import { ExecuteReportModule } from './execute/execute-report.module';

// COMPONENTS
import { ConfirmDialogComponent } from '../../shared/confirm-dialog/confirm-dialog.component';
import { ViewReportsComponent } from './view/view-reports.component';
import { EditReportComponent } from './edit/edit-report.component';

// SERVICES
import { ReportsService } from './reports.service';
import { TranslateStateService } from '../../core/translateStore.service';

// PIPE
import { StandarReportFilterPipe } from './view/standard-reports-filter.pipe';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/reports/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        ReportsRoutingModule,
        ExecuteReportModule,
        MatExpansionModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewReportsComponent,
        EditReportComponent,
        StandarReportFilterPipe
    ],
    entryComponents: [
        ConfirmDialogComponent
    ],
    providers: [
        ReportsService
    ]
})
export class ReportsModule { }
