﻿import { KeyName } from '../../common/key-name';
import { PagerResults } from '../../common/pager';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { ReportOptions } from './execute/execute-report-types';

export enum ReportAssetTypes {
    Vehicle = 1,
    Driver = 2,
    Location = 3,
    VehicleGroup = 4,
    DriverGroup = 5,
    LocationGroup = 6,
    AllVehs = 7,
    AllHRs = 8 ,
    Route = 9,
    RouteGroup = 10,
    User = 11,
    UserGroup = 12,
    ChargePoints = 13,
    ChargePointGroup = 14
}

export interface ReportBasicInfo extends KeyName {
    id: number;
    customizable: boolean;
    type: ReportTypes;
    name: string;
    standard: boolean;
    standardType?: string;
    dataType?: ReportDataTypes;
}

export enum ReportTypes {
    Vehicles = 1,
    Drivers = 2,
    Locations = 3,
    Routes = 4,
    Users = 5,
    ChargePoints = 6
}

export enum ReportDataTypes {
    Trip = 1,
    Aggregation = 2,
    Charge = 3,
    Booking = 4,
    Events = 5
}

export interface ReportDefinition {
    options: Array<ReportKeyValue<ReportOptions, any>>;
    aggregations: Array<ReportKeyValue<number, any>>;
    columns: Array<ReportKeyValue<number, any>>;
    graphs: Array<ReportKeyValue<number, any>>;
    groupings: Array<ReportKeyValue<number, any>>;
}


export interface ReportKeyValue<T1, T2> {
    key: T1;
    value: T2;

    // UI use only
    label?: string;
}

export interface Report {
    basicInfo: ReportBasicInfo;
    definition: ReportDefinition;
}

export interface ReportsList extends PagerResults {
    results: ReportBasicInfo[];
}


export enum ReportDeleteCodeOptions {
    Ok = 0,
    NotFound = 1
}

export enum ReportResponseCodeOptions {
    Ok = 0,
    NameExists = 1,
    NotFound = 2,
    NameMissing = 3,
    UnknownError = 4,
    InvalidDefinition = 5,
    NotCustomizable = 6,
    CustomizeStandardError = 7,
    Unauthorized = 8
}


export class ReportUtils {
    /*
    * Gets report name for display
    * @param rep The report's data
    */
   public static getReportName(rep: ReportBasicInfo, translate: CustomTranslateService): string {
        if (rep.standard === true) {
            return translate.instant('reports.standard_reports_names.' + rep.standardType);
        }
        return rep.name;
    }

     /*
    * Gets the list of the available report types
    */
    public static getReportTypes(): KeyName[] {
        return [
            {
                id: ReportTypes.Vehicles,
                name: 'reports.types.vehicles',
                enabled: true
            },
            {
                id: ReportTypes.Drivers,
                name: 'reports.types.drivers',
                enabled: true
            },
            {
                id: ReportTypes.Users,
                name: 'reports.types.users',
                enabled: true
            },
            {
                id: ReportTypes.ChargePoints,
                name: 'reports.types.chargepoints',
                enabled: true
            }
            // {
            //     id: ReportTypes.Locations,
            //     name: 'assets.locations',
            //     enabled: false
            // },
            // {
            //     id: ReportTypes.Routes,
            //     name: 'assets.routes',
            //     enabled: false
            // }
        ];
    }
}
