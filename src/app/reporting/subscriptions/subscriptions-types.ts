﻿import { Subscription } from 'rxjs/Subscription';
import { KeyName } from '../../common/key-name';
import { PagerResults } from '../../common/pager';
import { Report, ReportKeyValue, ReportAssetTypes } from '../reports/reports-types';
import { PeriodTypes } from '../../common/period-selector/period-selector-types';
import { ReportOptions } from '../reports/execute/execute-report-types';


export enum SubscriptionType {
    DistributionList = 1,
    DriverGroup = 2
}

export let SubscriptionTypesOptions: KeyName[] = [
    {
        id: SubscriptionType.DistributionList,
        name: 'distribution_list',
        term: 'subscription.type.distribution_list',
    },
    {
        id: SubscriptionType.DriverGroup,
        name: 'driver_group',
        term: 'subscription.type.driver_group',
    }
];

export enum SubscriptionScheduleMode {
    Daily = 1,
    Weekly,
    Monthly
}

export enum SubscriptionFormat {
    MSExcel = 2,
    PDF
}

export let SubscriptionFormatOptions: KeyName[] = [
    {
        id: SubscriptionFormat.PDF,
        name: 'PDF',
        enabled: true
    },
    {
        id: SubscriptionFormat.MSExcel,
        name: 'MS-Excel',
        enabled: true
    }
];


export interface ReportSubscription {
    assets?: Array<ReportKeyValue<ReportAssetTypes, number>>;
    filePerAsset: boolean;
    isDriver: boolean;
    id: number;
    options?: Array<ReportKeyValue<ReportOptions, any>>;
    report: Report;
    periodType: PeriodTypes;
    subscriptionID: number;
}

export interface SubscriptionBasicInfo extends KeyName {
    type: SubscriptionType;
}

export interface SubscriptionList extends PagerResults {
    results: SubscriptionBasicInfo[];
}

export interface SubscriptionRecipientsOption {
    id: SubscriptionType;
    name: string;
    isDisabledState: boolean;
    isCheckedState: boolean;
}

export class ReportsSubscription {
    public basicInfo: SubscriptionBasicInfo;
    public recipients?: string;
    public driverGroupIDs?: number[];
    public schedule: {
        mode: SubscriptionScheduleMode
        every: number;
        on?: number;
    };
    public format: SubscriptionFormat;
    public reports?: ReportSubscription[];
    public archive: boolean;

    constructor(title: string) {
        this.archive = false;
        this.basicInfo = {
            name: title,
            type: SubscriptionType.DriverGroup
        } as SubscriptionBasicInfo;
        this.driverGroupIDs = [];
        this.recipients = null;
        this.reports = [];
        this.format = SubscriptionFormat.PDF;
        this.schedule = {
            every: 1,
            mode: 1,
            on: null
        };
    }
}
