﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';


// MODULES
import { AssetGroupsModule } from '../../common/asset-groups/asset-groups.module';
import { PeriodSelectorModule } from '../../common/period-selector/period-selector.module';
import { SharedModule } from '../../shared/shared.module';
import { SubscriptionsRoutingModule } from './subscriptions-routing.module';

// COMPONENTS
import { ConfirmDialogComponent } from '../../shared/confirm-dialog/confirm-dialog.component';

// COMPONENTS
import { ViewSubscriptionsComponent } from './view/view-subscriptions.component';
import { EditSubscriptionComponent } from './edit/edit-subscription.component';

// SERVICES
import { SubscriptionsService } from './subscriptions.service';
import { TranslateStateService } from './../../core/translateStore.service';

// PIPE
import { SubscriptionReportsFilterPipe } from './edit/subscription-reports-filter.pipe';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(
        http,
        [
            './assets/i18n/subscriptions/',
            './assets/i18n/reports/',
        ],
        '.json',
        translateState
    );

}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        HttpClientJsonpModule,
        SharedModule,
        AssetGroupsModule,
        PeriodSelectorModule,
        SubscriptionsRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        }),
    ],
    declarations: [
        ViewSubscriptionsComponent,
        EditSubscriptionComponent,
        SubscriptionReportsFilterPipe
    ],
    entryComponents: [
        ConfirmDialogComponent
    ],
    providers: [
        SubscriptionsService
    ]
})
export class SubscriptionsModule { }
