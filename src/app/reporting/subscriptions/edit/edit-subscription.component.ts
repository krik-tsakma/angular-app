// FRAMEWORK
import {
    Component, OnInit, OnDestroy,
    ViewEncapsulation, ViewChild,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// RXJS
import { Subscription, combineLatest } from 'rxjs';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { AuthService } from '../../../auth/authService';
import { SubscriptionsService } from '../subscriptions.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { Config } from '../../../config/config';

// TYPES
import {
    SubscriptionType, SubscriptionScheduleMode,
    ReportsSubscription, SubscriptionFormatOptions,
    SubscriptionRecipientsOption
} from '../subscriptions-types';
import { ReportUtils } from '../../reports/reports-types';
import { AssetGroupTypes, AssetGroupItem } from '../../../admin/asset-groups/asset-groups.types';
import { KeyName } from '../../../common/key-name';
import { ConfirmationDialogParams } from '../../../shared/confirm-dialog/confirm-dialog-types';

// MOMENT
import moment from 'moment';


@Component({
    selector: 'edit-subscription',
    templateUrl: './edit-subscription.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./edit-subscription.less'],
})


export class EditSubscriptionComponent implements OnInit, OnDestroy {
    @ViewChild('editSubscriptionForm', { static: false }) public editForm: NgForm;
    public action: string;
    // loader
    public loading: boolean;
    public subscription: ReportsSubscription;
    public subscriptionScheduleMode = SubscriptionScheduleMode;
    public scheduleMode: SubscriptionScheduleMode;
    public subscriptionType = SubscriptionType;
    public subscriptionFormatOptions = SubscriptionFormatOptions;
    public occurencesNumber: number = 1;
    public nextOccurences: string[];
    public dateFormat: string;
    public confirmedRadioChange: SubscriptionType;

    // groups
    public assetGroupTypes = AssetGroupTypes;
    public selectedGroupOption: number = -2;
    public dataAssetGroups: AssetGroupItem[] = [];

    public scheduleOptions = [
        {
            id: SubscriptionScheduleMode.Daily,
            name: 'subscription.schedule_option.daily'
        },
        {
            id: SubscriptionScheduleMode.Weekly,
            name: 'subscription.schedule_option.weekly'
        },
        {
            id: SubscriptionScheduleMode.Monthly,
            name: 'subscription.schedule_option.monthly'
        }
    ];
    public daysOfTheWeek = [
        {
            id: 1,
            day: this.translate.instant('days_of_the_week.monday').slice(0, 3)
        },
        {
            id: 2,
            day: this.translate.instant('days_of_the_week.tuesday').slice(0, 3)
        },
        {
            id: 3,
            day: this.translate.instant('days_of_the_week.wednesday').slice(0, 3)
        },
        {
            id: 4,
            day: this.translate.instant('days_of_the_week.thursday').slice(0, 3)
        },
        {
            id: 5,
            day: this.translate.instant('days_of_the_week.friday').slice(0, 3)
        },
        {
            id: 6,
            day: this.translate.instant('days_of_the_week.saturday').slice(0, 3)
        },
        {
            id: 7,
            day: this.translate.instant('days_of_the_week.sunday').slice(0, 3)
        },
    ];
    public recipientsOption: SubscriptionRecipientsOption[] = [
        {
            id: SubscriptionType.DriverGroup,
            name: this.translate.instant('subscription.driver_group'),
            isDisabledState: null,
            isCheckedState: null
        },
        {
            id: SubscriptionType.DistributionList,
            name: this.translate.instant('subscription.distribution_list'),
            isDisabledState: null,
            isCheckedState: null
        }
    ];
    public reportTypes: KeyName[];

    // emails
    public email: string;
    public emailList: string[] = [];
    private emailSeparator: string = ';';

    // subscribers
    private serviceSubscriber: Subscription;
    private initValues: Subscription;


    constructor(
        public userOptions: UserOptionsService,
        public translate: CustomTranslateService,
        public authService: AuthService,
        public service: SubscriptionsService,
        public router: Router,
        public activatedRoute: ActivatedRoute,
        public snackBar: SnackBarService,
        private confirmDialogService: ConfirmDialogService,
        private previousRouteService: PreviousRouteService,
        private unsubscribe: UnsubscribeService,
        private configService: Config) {

            this.dateFormat = translate.instant('locale.full_date');
            this.reportTypes = ReportUtils.getReportTypes();
    }

    // ====================
    // LIFECYCLE
    // ====================

    public ngOnInit() {

        this.initValues = combineLatest([
                this.translate.get('subscription.title'),
                this.activatedRoute.params
            ]).subscribe((res) => {
                const defaultTitle = res[0] + ' ' + moment().format(this.dateFormat + ' HH:mm');
                this.subscription = new ReportsSubscription(defaultTitle);
                this.action = res[1].editMode === 'edit'
                            ? 'form.actions.edit'
                            : 'form.actions.create';
                if (res[1].id) {
                    this.get(res[1].id);
                } else {
                    this.updateRecipientsOption(this.subscription.basicInfo.type);
                    this.showDialogBeforeChangeRadio(
                        this.subscription.basicInfo.type,
                        this.recipientsOption.find((opt) => opt.id === this.subscription.basicInfo.type)
                    );
                    this.getDates();
                }
            });
    }



    public ngOnDestroy() {
        this.unsubscribe.removeSubscription([
            this.serviceSubscriber,
            this.initValues
        ]);
    }


    // On every tab change update checkedState in ReceipientsOption's array
    // After that update confirmedRatioChange (ngmodel)
    public updateRecipientsOption(type: SubscriptionType) {
        this.recipientsOption = this.recipientsOption.map((opt) => {
            opt.isCheckedState = opt.id === type ? true : false;
            return opt;
        });

        this.confirmedRadioChange = this.recipientsOption.find((opt) => opt.isCheckedState).id;
    }


    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router')});
    }


    // trigger when 'Schedule' radio button change
    public onScheduleChange() {
        switch (this.subscription.schedule.mode) {
            case SubscriptionScheduleMode.Daily: {
                this.subscription.schedule.on = null;
                break;
            }
            case SubscriptionScheduleMode.Monthly:
            case SubscriptionScheduleMode.Weekly: {
                this.subscription.schedule.on = 1;
                break;
            }
            default: {
                break;
            }
        }
        this.getDates();
    }



    // Check option 'schedule.on' if monthly is selected
    public checkOnNumber() {
        if (this.subscription.schedule.on > 31) {
            this.subscription.schedule.on = 31;
        }

        if (this.subscription.schedule.on < 1) {
            this.subscription.schedule.on = 1;
        }

        this.getDates();
    }



    // Occurences dates
    public getDates() {
        this.nextOccurences = [];
        const on = this.subscription.schedule.on;
        const every = this.subscription.schedule.every;
        const repeatMode =  this.subscription.schedule.mode;

        const startDate = moment();

        // DAILY
        if (repeatMode === SubscriptionScheduleMode.Daily) {
            if (isNaN(every) || every < 1) {
                return;
            }

            for (let i = 0; i < this.occurencesNumber; i++) {
                const dt = moment(startDate);
                dt.add((i + 1) * every, 'days');
                this.nextOccurences[i] = dt.format(this.dateFormat);
            }
        }

        // WEEKLY
        if (repeatMode === SubscriptionScheduleMode.Weekly) {
            if (isNaN(every) || every < 1) {
                return;
            }

            for (let i = 0; i < this.occurencesNumber; i++) {
                // set the start of the iso week
                const dt = moment(startDate).startOf('isoWeek');

                // add the repeat every week days
                dt.add((i + 1) * every * 7, 'days');

                // days are zero based in moment
                dt.isoWeekday(on);

                this.nextOccurences[i] = dt.format(this.dateFormat);
            }
        }

        // MONTHLY
        if (repeatMode === SubscriptionScheduleMode.Monthly) {
            if (isNaN(every) || every < 1) {
                return;
            }

            for (let i = 0; i < this.occurencesNumber; i++) {
                // set the start of the month
                const dt = moment(startDate).startOf('month');

                // add the repeat every month
                dt.add((i + 1) * every, 'month');

                // sanity check
                if (isNaN(on) || (on > 31) || (on < 1)) {
                    return;
                }

                // add the specific day of month
                dt.date(on);

                this.nextOccurences[i] = dt.format(this.dateFormat);
            }
        }
    }



    // On change recipients tab function
    public showDialogBeforeChangeRadio(type: SubscriptionType, recipient: SubscriptionRecipientsOption) {
        // if click the all ready selected radio, don't do nothing and return
        if (recipient.id === type) {
            return;
        }

        recipient.isDisabledState = true;

        if (this.subscription.recipients === null && this.subscription.driverGroupIDs.length === 0 &&
            this.dataAssetGroups.find((g) => !g.enabled && g.id > 0) === undefined && this.emailList.length === 0) {
            this.onRecipientsChange(recipient.id);
            recipient.isDisabledState = null;
            return;
        }

        // if click different radio than the selected, firstly disable the new selected
        // and open dialog explaining that all the changes are gonna be lost
        this.confirmDialogService
            .confirm({
                title: 'subscription.confirm_dialog.title',
                message: 'subscription.confirm_dialog.message',
                submitButtonTitle: 'form.actions.ok'
            } as ConfirmationDialogParams)
            .subscribe((result) => {
                if (result === 'yes') {
                    recipient.isDisabledState = null;
                    this.onRecipientsChange(recipient.id);
                } else {
                    recipient.isDisabledState = null;
                    this.updateRecipientsOption(type);
                }
            });
    }


    // trigger when 'Recipient' radio button change
    // empty list data on every change
    public onRecipientsChange(value: SubscriptionType) {
        this.subscription.basicInfo.type = value;
        this.updateRecipientsOption(value);
        this.subscription.driverGroupIDs = [];
        this.subscription.recipients = null;
        this.dataAssetGroups.map((g) => {
            if (g.id > 0) {
                g.enabled = true;
            }
        });
        this.emailList = [];
        this.formIsInvalid();
    }

    // ====================
    // ASSET GROUPS
    // ====================

    // when all groups fetched from server
    public onAssetGroupsLoaded(groups: AssetGroupItem[]) {
        let helpArray = [];

        if (this.dataAssetGroups.length > 0) {
            helpArray = this.dataAssetGroups.filter((asset) => {
                if (!asset.enabled && asset.id !== -2) {
                    return asset;
                }
            });
        }

        this.dataAssetGroups = groups;

        if (helpArray.length > 0) {
            helpArray.forEach((elem) => {
                this.onChangeAssetGroup(this.dataAssetGroups.find((asset) => asset.id === elem.id));
            });
        }

        this.setRecipientOptions();
    }


    // Add drivers group to receipients from select tag
    public onChangeAssetGroup(group: AssetGroupItem) {
        group.enabled = false;
    }

    // Remove drivers group from receipients (If there is any)
    public removeGroup(id) {
        this.dataAssetGroups.forEach((data) => {
            if (data.id === id && !data.enabled) {
                data.enabled = true;
            }
        });
    }


    // ====================
    // DISTRIBUTION LIST
    // ====================

    // Add email to Distribution List
    public addEmail(email: string) {
        if (this.emailList.find((e) => e === email) === undefined) {
            this.emailList.push(email);
        }
        this.email = null;
    }


    // Remove email from Distribution List (If there is any)
    public removeEmail(email: string) {
        this.emailList = this.emailList.filter((e) => e !== email);
    }

    // ====================
    // REPORT ITEMS
    // ====================

    // Remove email from Distribution List (If there is any)
    public removeReport(id) {
        if (this.subscription.reports && this.subscription.reports.length > 0) {
            this.subscription.reports = this.subscription.reports.filter((report) => report.id !== id);
        }
    }

    // ====================
    // FORM
    // ====================

    // Validation function that enables/disables save button
    public formIsInvalid(): boolean {
        if (this.editForm && this.editForm.invalid) {
            return true;
        }

        if (this.subscription.schedule.every < 1 || typeof this.subscription.schedule.every !== 'number') {
            return true;
        }

        if (this.subscription.schedule.mode === SubscriptionScheduleMode.Daily) {
            if (this.subscription.schedule.on !== null) {
                return true;
            }
        }

        if (this.subscription.schedule.mode === SubscriptionScheduleMode.Weekly) {
            if (this.subscription.schedule.on < 1 ||  this.subscription.schedule.on > 7) {
                return true;
            }
        }

        if (this.subscription.schedule.mode === SubscriptionScheduleMode.Monthly) {
            if (this.subscription.schedule.on < 1 ||  this.subscription.schedule.on > 31) {
                return true;
            }
        }

        if (this.subscription.basicInfo.type === SubscriptionType.DriverGroup) {
            if (this.dataAssetGroups.find((g) => g.enabled === false && g.id > 0) === undefined) {
                return true;
            }
        }

        if (this.subscription.basicInfo.type === SubscriptionType.DistributionList) {
            if (!this.emailList || this.emailList.length === 0) {
                return true;
            }
        }

        return false;
    }


    // ====================
    // SUBSCRIPTION DATA
    // ====================

    // POST/PUT function
    public save() {
        if (this.formIsInvalid()) {
            return;
        }

        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.assignRecipientOptions();


        // UPDATE
        if (this.subscription.basicInfo.id > 0) {
            this.serviceSubscriber = this.service
                .update(this.subscription)
                .subscribe((res) => {
                    this.loading = false;
                    this.snackBar.open('form.save_success', 'form.actions.ok');
                    this.back();
                }, (err) => {
                    this.loading = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                });
        } else { // CREATE
            this.serviceSubscriber = this.service
                .create(this.subscription)
                .subscribe((res) => {
                    this.snackBar.open('form.save_success', 'form.actions.ok');
                    this.back();
                }, (err) => {
                    this.loading = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                });
        }

    }

    private get(id: number) {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((data: ReportsSubscription) => {
                this.loading = false;
                this.subscription = data;
                this.updateRecipientsOption(this.subscription.basicInfo.type);
                this.getDates();
                this.setRecipientOptions();
                this.subscription.reports.forEach((res) => {
                    res.report.basicInfo.name = ReportUtils.getReportName(res.report.basicInfo, this.translate);
                });

                this.reportTypes.forEach((type) => {
                    type.enabled = this.subscription.reports.find((report) => {
                        return report.report.basicInfo.type === type.id;
                    }) !== undefined
                        ? true
                        : false;
                });
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            });
    }

    // ====================
    // HELPERS
    // ====================

    private setRecipientOptions() {
        if (this.subscription.recipients) {
            this.emailList = this.subscription.recipients.split(this.emailSeparator);
        }
        if (this.subscription.driverGroupIDs && this.subscription.driverGroupIDs.length > 0) {
            this.subscription.driverGroupIDs.forEach((groupID) => {
                const group = this.dataAssetGroups.find((g) => g.id === groupID);
                if (group) {
                    group.enabled = false;
                }
            });
        }

    }

    private assignRecipientOptions() {
        this.subscription.recipients = null;
        this.subscription.driverGroupIDs = [];
        if (this.subscription.basicInfo.type === SubscriptionType.DriverGroup) {
            this.subscription.driverGroupIDs = this.dataAssetGroups.filter((g) => g.enabled === false && g.id > 0).map((g) => g.id);
        } else {
            this.subscription.recipients = this.emailList.join(this.emailSeparator);
        }
    }
}
