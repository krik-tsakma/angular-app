﻿// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'mySubscriptionReportsFilter',
    pure: false
})

export class SubscriptionReportsFilterPipe implements PipeTransform {
    public transform(standardReportsData: any[], filter: any): any {
        
        if (!standardReportsData || !filter.toString()) {           
            return standardReportsData;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return standardReportsData.filter((item) => {
            return item.report.basicInfo.type === filter;
        });
    }
}
