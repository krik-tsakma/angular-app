﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { ReportsSubscription, SubscriptionList, ReportSubscription } from './subscriptions-types';

@Injectable()
export class SubscriptionsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) { 
            this.baseURL = config.get('apiUrl') + '/api/Subscriptions';
        }

    // Get all the score definitions
    public get(): Observable<SubscriptionList> {       
        return this.authHttp
            .get(this.baseURL);
    }

    public getByID(id: number): Observable<ReportsSubscription> {
        return this.authHttp
            .get(this.baseURL + '/' + id);
    }

    public addToSubsription(req: ReportSubscription): Observable<ReportsSubscription> {
        return this.authHttp
            .post(this.baseURL + '/Subscribe/', req);
    }

    public create(subscription: ReportsSubscription): Observable<{}> {
        return this.authHttp
            .post(this.baseURL, subscription);
    }

    public update(subscription: ReportsSubscription): Observable<{}> {
        return this.authHttp
            .put(this.baseURL, subscription);
    }

    public delete(id: number): Observable<{}> {
        return this.authHttp
            .delete(this.baseURL + '/' + id);
    }
} 
