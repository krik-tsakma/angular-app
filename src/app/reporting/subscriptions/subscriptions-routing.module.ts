﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../../auth/authGuard';

import { ViewSubscriptionsComponent } from './view/view-subscriptions.component';
import { EditSubscriptionComponent } from './edit/edit-subscription.component';


const SubscriptionsRoutes: Routes = [
    {
        path: '',
        component: ViewSubscriptionsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: ':editMode/:id',
        component: EditSubscriptionComponent,
        canActivate: [AuthGuard]       
    },
    {
        path: ':editMode',
        component: EditSubscriptionComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(SubscriptionsRoutes)
    ],
    exports: [
        RouterModule
    ]    
})
export class SubscriptionsRoutingModule { }
