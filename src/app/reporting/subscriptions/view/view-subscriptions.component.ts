﻿// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { SubscriptionsService } from '../subscriptions.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { Config } from '../../../config/config';

// TYPES
import {
    SubscriptionType, SubscriptionBasicInfo,
    SubscriptionList, SubscriptionTypesOptions
} from '../subscriptions-types';
import {
    TableColumnSetting, TableActionItem, TableActionOptions,
    TableCellFormatOptions, TableColumnSortOrderOptions, TableColumnSortOptions
} from '../../../shared/data-table/data-table.types';
import { ConfirmationDialogParams } from '../../../shared/confirm-dialog/confirm-dialog-types';


@Component({
    selector: 'view-subscriptions',
    templateUrl: './view-subscriptions.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: [ './view-subscriptions.less' ]
})

export class ViewSubscriptionsComponent implements OnInit, OnDestroy {
    public loading: boolean;
    public subscriptions: SubscriptionBasicInfo[];
    public tableSettings: TableColumnSetting[];
    private serviceSubscriber: Subscription;

    constructor(
        public router: Router,
        public userOptions: UserOptionsService,
        private translate: CustomTranslateService,
        private previousRouteService: PreviousRouteService,
        private service: SubscriptionsService,
        private confirmDialogService: ConfirmDialogService,
        private snackBar: SnackBarService,
        private unsubscribe: UnsubscribeService,
        private configService: Config) {
            SubscriptionTypesOptions.forEach((type) => {
                this.translate.get(type.term).subscribe((t) => {
                    type.name = t;
                });
            });

            this.tableSettings =  [
                {
                    primaryKey: 'name',
                    header: 'subscription.subscription_name',
                    sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
                },
                {
                    primaryKey: 'type',
                    header: 'subscription.type',
                    format: TableCellFormatOptions.custom,
                    formatFunction: this.formatSubscriptionType
                },
                {
                    primaryKey: ' ',
                    header: ' ',
                    actions: [TableActionOptions.edit, TableActionOptions.delete]
                }
            ];
    }



    public ngOnInit(): void {
        this.get();
    }

    public ngOnDestroy(): void {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }


    // Create new subscription
    public createSubscription() {
        this.router.navigate([this.router.url, 'add'], { skipLocationChange: !this.configService.get('router') });
    }


    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }


    // ----------------
    // TABLE CALLBACKS
    // ----------------

    public formatSubscriptionType(item: SubscriptionBasicInfo): string {
        if (item === undefined || item === null) {
            return ' ';
        }
        /* tslint:disable */
        return SubscriptionTypesOptions.find((x) => x.id == item.type).name;
        /* tslint:enable */
    }

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.edit:
                this.router.navigate([this.router.url, 'edit', id], { skipLocationChange: !this.configService.get('router') });
                break;
            case TableActionOptions.delete:
                this.openDialog(id);
                break;
            default:
                break;
        }
    }



    // Before permanent delete, opens the dialog and asks for confirmation
    public openDialog(id: number) {
        const confirmReq = {
            title: 'form.actions.delete',
            message: 'form.warnings.are_you_sure',
            submitButtonTitle: 'form.actions.delete'
        } as ConfirmationDialogParams;

        this.confirmDialogService
            .confirm(confirmReq)
            .subscribe((result) => {
                if (result === 'yes') {
                    this.delete(id);
                }
        });
    }


    // ----------------
    // PRIVATE
    // ----------------

    private get() {
        this.loading = true;

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.serviceSubscriber);

        // then start the new request
        this.serviceSubscriber = this.service
            .get()
            .subscribe(
                (response: SubscriptionList) => {
                    this.loading = false;
                    this.subscriptions = response.results;
                },
                (err) => {
                    this.loading = false;
                    this.snackBar.open('data.error', '');
                }
            );
    }

    // ----------------
    // DELETE
    // ----------------
    private delete(id: number) {
        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.loading = true;
        this.serviceSubscriber = this.service
            .delete(id)
            .subscribe((res: any) => {
                this.loading = false;
                if (res.code === 1) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    return;
                } else {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get();
                }
            },
            (error) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
            }
        );
    }

}
