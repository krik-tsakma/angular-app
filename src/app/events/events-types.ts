import { PagerResults } from '../common/pager';
import { Score, ScoreName } from '../common/score';
import { KeyName } from '../common/key-name';
import { PeriodTypes } from '../common/period-selector/period-selector-types';
import { DisplayParameterTypes } from '../common/display-parameter-types';
import { TraceEvent, Trace } from '../common/entities/entity-types';
import { EventFilters } from './event-filters/event-filters-types';

export interface EventDetail extends TraceEvent {            
    checked?: boolean;
}

export interface EventChangeItem {
    timestamp: string;
    fromStatus?: any;
    toStatus?: any;
    user: string;
}

export interface EventBaseParams {
    vehicleID: number;
    timestamp: string;
}


export interface EventsResults extends PagerResults {
    results: EventDetail[];
}

export interface EventsUpdate {
    Events: EventBaseParams[];
    StatusIdx: number;
}

export interface EventUpdateResult {
    code: number;
    event: EventBaseParams;
}

export interface EventsUpdateResponse {
    results: EventUpdateResult[];
}

export enum EventUpdateResultCodeOptions {
    Updated = 0,
    NoSuchEventExists = 1,
    NoChangeRequested = 2
}

