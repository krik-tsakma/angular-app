// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// SERVICES
import { EventsService } from '../events.service';
import { UpdateDialogService } from '../update-dialog/update-dialog.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { Config } from '../../config/config';

// TYPES
import { KeyName } from '../../common/key-name';
import { Trace } from '../../common/entities/entity-types';
import { MapModeOptions } from '../../google-maps/types/map.types';
import { MapCustomOptionsGroups } from '../../google-maps/custom-options/custom-options.types';
import { TableColumnSetting, TableCellFormatOptions } from '../../shared/data-table/data-table.types';
import { EventDetail, EventChangeItem, EventBaseParams, EventsUpdateResponse } from '../events-types';

// PIPES
import { DateFormatterDisplayOptions } from '../../shared/pipes/dateFormat.pipe';


@Component({
    selector: 'event-details',
    templateUrl: './event-details.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./event-details.less']
})

export class EventDetailsComponent implements OnInit, OnDestroy {
    public mapModes: any = MapModeOptions;
    public customOptionsGroups = MapCustomOptionsGroups;
    public eventDetails: EventDetail;
    public traces: Trace[];
    public historicData: EventChangeItem[];
    public historyTableSettings: TableColumnSetting[];
    public statuses: KeyName[];
    public dateDisplayOptions: any = DateFormatterDisplayOptions;

    // loaders
    public loading: boolean;
    public loadingTraces: boolean;
    public loadingHistoricData: boolean;

    // subscribers
    private serviceSubscriber: any;
    private tracesSubscriber: any;
    
    constructor(
        public userOptions: UserOptionsService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private service: EventsService,
        private previousRouteService: PreviousRouteService,
        private updateDialogService: UpdateDialogService,
        private unsubscribe: UnsubscribeService,
        private configService: Config) {    
            this.historyTableSettings =  [
                {
                    primaryKey: 'timestamp',
                    header: 'events.details.timestamp',
                    format: TableCellFormatOptions.dateTime
                },
                {
                    primaryKey: 'user',
                    header: 'events.details.user',
                },
                {
                    primaryKey: 'fromStatus',
                    header: 'events.details.fromStatus',
                },
                {
                    primaryKey: 'toStatus',
                    header: 'events.details.toStatus',
                },
            ];
    }


    // On initialize
    public ngOnInit() {
        this.activatedRoute.params
            .subscribe((params: EventBaseParams) => {
                this.get(params);
            });
    }

    // Clean tripDetailsSubscribe to avoid memory leak
    public ngOnDestroy() {
        this.unsubscribe.removeSubscription([
            this.serviceSubscriber,
            this.tracesSubscriber
        ]);     
    }

    // Click back arrow
    public back(): void {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }  


    // ----------------
    // DATA
    // ----------------

    private get(request: EventBaseParams) {
        this.loading = true;
        this.serviceSubscriber = this.service
            .getEventDetails(request)           
            .subscribe((res: EventDetail) => {
                this.loading = false;
                if (res) {
                    this.eventDetails = res;
                    this.getChanges(request);
                }
            },
            (err) => {
                this.loading = false;
            });        
    }

    private getChanges(params) {
        this.loadingHistoricData = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.service
            .getEventChanges(params)           
            .subscribe((data: EventChangeItem[]) => {
                this.loadingHistoricData = false;
                this.historicData = data;
            },
            (err) => {
                this.loadingHistoricData = false;                
            });        
    }

    // ----------------
    // UPDATE EVENT
    // ----------------

    private openDialog() {
        this.updateDialogService
            .confirm(this.statuses)
            .subscribe((result: any) => {
                if (result === 'no') {
                    return;
                }

                if (result) {
                    if (Number(result.status) === this.eventDetails.statusIDX) {
                        return;
                    } else {
                        const updateEvent = {
                            Events: [
                                {
                                    vehicleID: Number(this.eventDetails.vehicleID),
                                    timestamp: this.eventDetails.timestamp
                                } as EventBaseParams
                            ],
                            ClassIdx: null,
                            StatusIdx: Number(result.status),
                            PriorityIdx: null
                        };
                        this.service.updateEvents(updateEvent)
                            .subscribe(
                                (res: EventsUpdateResponse) => {
                                    if (res.results[0].code === 0) {
                                        const params = {
                                            vehicleID: Number(res.results[0].event.vehicleID),
                                            timestamp: res.results[0].event.timestamp
                                        } as EventBaseParams;
                                        this.get(params);
                                    }
                                    return;
                                },
                                (err) => {
                                    console.log('err ', err);
                                }
                            );
                    }
                }
            });
    }

}
