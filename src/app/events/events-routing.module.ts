﻿
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EventsListComponent } from './events-list/events-list.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { AuthGuard } from '../auth/authGuard';

const EventsRoutes: Routes = [
    {
        path: '',
        component: EventsListComponent,
        canActivate: [AuthGuard]
    },
    {
        path: ':vehicleID/:timestamp',
        component: EventDetailsComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(EventsRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})

export class EventsRoutingModule { }
