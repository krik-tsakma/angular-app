﻿// FRAMEWORK
import { Component, ViewEncapsulation, ChangeDetectorRef, Inject } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { DOCUMENT } from '@angular/common';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { EventStatusesService } from './../../common/events-statuses-list/events-statuses.service';

// TYPES
import { KeyName } from '../../common/key-name';

@Component({
    selector: 'update-dialog',
    templateUrl: './update-dialog.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./update-dialog.less'],
    providers: [EventStatusesService]
})

export class EventUpdateDialogComponent {    
    public status: number;    
    public statuses: KeyName[];
    public serviceSubscriber: Subscription;
    public loading: boolean;
    private openModalFinished: any;

    constructor(public dialogRef: MatDialogRef<EventUpdateDialogComponent>,
                public dialog: MatDialog,
                private changeDetectionRef: ChangeDetectorRef,
                private service: EventStatusesService,
                private unsubscribe: UnsubscribeService,
                @Inject(DOCUMENT) doc: any) {

        // Adding a class to the body if a dialog opens and
        // removing it after all open dialogs are closed
        dialog.afterOpen.subscribe(() => {
            if (!doc.body.classList.contains('dialog-overflow-visible')) {
                doc.body.classList.add('dialog-overflow-visible');
            }
            // if no statuses, fetch them
            // do it this way cause afterOpen fires more than once
            clearTimeout(this.openModalFinished);
            this.openModalFinished = setTimeout((x) => { 
                if (dialogRef.componentInstance) { 
                    const statuses = dialogRef.componentInstance.statuses;
                    if (!statuses || statuses.length === 0) {
                        this.getStatuses();
                    } else {
                        this.statuses = statuses.filter((status) => status.id > 0);
                        this.status = this.statuses[0].id; 
                    }
                }
            }, 200);
        });
        dialog.afterAllClosed.subscribe(() => {
            doc.body.classList.remove('dialog-overflow-visible');
        });
        
    }

    public onStatusChange(id: number): void {
        this.status = Number(id);
    }

    private getStatuses() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.loading = true;
        this.serviceSubscriber = 
            this.service
                .get()
                .subscribe((props: KeyName[]) => {
                    this.loading = false;
                    this.statuses = props.map((status) => {
                        status.enabled = true;
                        if (!status.name) {
                            status.name = status.id;
                        }
                        return status;
                    });
                    this.status =  this.statuses[0].id;
                }, 
                (err) => {
                    this.loading = false;
                });
    }
}
