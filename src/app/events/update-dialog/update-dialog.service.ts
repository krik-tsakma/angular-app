// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { EventUpdateDialogComponent } from './update-dialog.component';

// TYPES
import { KeyName } from '../../common/key-name';

@Injectable()
export class UpdateDialogService {

    constructor(private dialog: MatDialog) { 
        // foo
    }

    public confirm(statuses?: KeyName[]): Observable<string> {

        let dialogRef: MatDialogRef<EventUpdateDialogComponent>;
        dialogRef = this.dialog.open(EventUpdateDialogComponent);
        dialogRef.componentInstance.statuses = statuses;
        return dialogRef.afterClosed();

    }
}
