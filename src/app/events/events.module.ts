
// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../shared/shared.module';
import { MapsModule } from '../google-maps/maps.module';
import { PeriodSelectorModule } from '../common/period-selector/period-selector.module';
import { ScrollToTopModule } from '../common/scroll-to-top/scroll-to-top.module';
import { EventsRoutingModule } from './events-routing.module';
import { AssetGroupsModule } from '../common/asset-groups/asset-groups.module';
import { EventsPrioritiesListModule } from '../common/events-priorities-list/events-priorities-list.module';
import { EvenstClassesListModule } from '../common/events-classes-list/events-classes-list.module';
import { EventsStatusesListModule } from '../common/events-statuses-list/events-statuses-list.module';

// COMPONENTS
import { EventsListComponent } from './events-list/events-list.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventUpdateDialogComponent } from './update-dialog/update-dialog.component';
import { EventFiltersComponent } from './event-filters/event-filters.component';
import { EventsGridComponent } from './events-grid/events-grid.component';
import { EventManagerWidgetsComponent } from './widgets/widgets.component';

// widgets

import { EventHeatmapWidgetComponent } from './widgets/heatmap-widget/heatmap-widget.component';
import { EventDataWidgetComponent } from './widgets/data-widget/data-widget.component';

// SERVICES
import { EventsService } from './events.service';
import { UpdateDialogService } from './update-dialog/update-dialog.service';
import { TranslateStateService } from './../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/events/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        MapsModule,
        HttpClientJsonpModule,
        EventsRoutingModule,
        PeriodSelectorModule,
        ScrollToTopModule,
        AssetGroupsModule,
        EventsPrioritiesListModule,
        EvenstClassesListModule,
        EventsStatusesListModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        EventsListComponent,
        EventDetailsComponent,
        EventUpdateDialogComponent,
        EventFiltersComponent,
        EventsGridComponent,
        EventManagerWidgetsComponent,
        EventHeatmapWidgetComponent,
        EventDataWidgetComponent
    ],
    providers: [
        EventsService,
        UpdateDialogService,
    ],
    entryComponents: [
        EventUpdateDialogComponent
    ]
})
export class EventsModule { }
