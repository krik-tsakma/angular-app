// FRAMEWORK
import {
    Component, ChangeDetectorRef, OnInit,
        ViewChild, HostBinding, ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';

// COMPONENTS
import { EventsGridComponent } from '../events-grid/events-grid.component';
import { EventManagerWidgetsComponent } from '../widgets/widgets.component';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { UpdateDialogService } from '../update-dialog/update-dialog.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { MaximizeService } from '../../shared/maximize/maximize.service';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';

// TYPES
import { EventsPageOptions } from './events-list-types';
import { KeyName } from '../../common/key-name';
import { Pager } from '../../common/pager';
import { Sorting } from '../../common/sorting';
import { PeriodTypes } from '../../common/period-selector/period-selector-types';
import { StoreItems } from '../../shared/store-manager/store-items';
import { EventFilters } from '../event-filters/event-filters-types';
import { Resizer } from '../../shared/maximize/maximize-types';
import { MoreOptionsMenuOption } from '../../shared/more-options-menu/more-options-menu.types';

@Component({
    selector: 'events-list',
    templateUrl: './events-list.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./events-list.less'],
})

export class EventsListComponent implements OnInit {
    @ViewChild(EventsGridComponent, { static: true }) public eventsGridComponent: EventsGridComponent;
    @ViewChild(EventManagerWidgetsComponent, { static: true }) public eventsWidgetsComponent: EventManagerWidgetsComponent;

    public showFilters: boolean = false;
    public pageOptions = EventsPageOptions;
    public request: EventFilters = {} as EventFilters;
    public pager: Pager = new Pager();
    public statuses: KeyName[];
    public maximizer: Resizer = null;
    public autoRefreshEnabled: boolean = true;
    public searchLoading: boolean = false;

    constructor(
        public userOptions: UserOptionsService,
        public snackBar: SnackBarService,
        private router: Router,
        private maximizeService: MaximizeService,
        private updateDialogService: UpdateDialogService,
        private storeManager: StoreManagerService,
        private changeDetectionRef: ChangeDetectorRef) {

            this.maximizeService.resizer.subscribe((resizer: Resizer) => {
                this.maximizer = resizer;
            });
    }


    public ngOnInit() {
        this.request.periodType = PeriodTypes.Today;
        const savedData = this.storeManager.getOption(StoreItems.eventManagerList) as EventFilters;
        if (savedData) {
            this.request = savedData;
        }
        // manually trigger change detection for the current component.
        // otherwise "Expression ___ has changed after it was checked" error
        this.changeDetectionRef.detectChanges();
    }

    public onLoadEventFilters(statuses: KeyName[]) {
        this.statuses = statuses;
    }

    public onChangeEventFilter(request: EventFilters) {
        this.request =  request;
        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.eventManagerList, this.request);

        this.changeDetectionRef.detectChanges();

        this.eventsGridComponent.reload(request);

    }

    // we need this cause sorting takes place in events-grid component
    public onSortSelection(sorting: string) {
        this.request.sorting = sorting;

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.eventManagerList, this.request);
    }

    // WIDGETS CALLBACKS
    public onSelectPageOption(item: MoreOptionsMenuOption): void {
        if (item.id === 'customize') {
            this.eventsWidgetsComponent.customizeLayout();
        } else {
            this.autoRefreshEnabled = !this.autoRefreshEnabled;
        }
    }

    // new widget callback
    public newEventWidget(type) {
        this.eventsWidgetsComponent.create(type, this.request);
    }


    // RESIZE CALLBACKS
    public hideElement(id) {
        return this.maximizer !== null && this.maximizer.maximized === true && this.maximizer.htmlID !== id;
    }

}
