import {
    MoreOptionsMenuOptions, MoreOptionsMenuOption, MoreOptionsMenuTypes
} from '../../shared/more-options-menu/more-options-menu.types';

export let EventsPageOptions: MoreOptionsMenuOptions[] = [
    {
        term: 'customize_layout.title',
        options: [
            {
                id: 'customize',
                term: 'customize_layout.customize',
                enabled: true,
                type: MoreOptionsMenuTypes.svgIcon,
                svgIcon: 'customize_icon',
                svgIconClass: 'custom-svg-icon',
                svgUrl: 'images/widgets/customize_icon.svg',
                
            }
        ]
    },
    {
        term: 'Autorefresh',
        options: [
            {
                id: 'autorefresh',
                term: 'Autorefresh',
                enabled: true,
                type: MoreOptionsMenuTypes.checkbox,
                checkboxValue: true,
            }
        ]
    }
];
