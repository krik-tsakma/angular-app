// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../auth/authHttp';
import { Config } from '../config/config';

// TYPES
import { 
    EventUpdateResultCodeOptions, EventsResults,
    EventBaseParams, EventDetail,
    EventChangeItem, EventsUpdate, EventsUpdateResponse
} from './events-types';
import { EventFilters } from './event-filters/event-filters-types';
import { Trace } from '../common/entities/entity-types';

@Injectable()
export class EventsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) { 
            this.baseURL = config.get('apiUrl') + '/api/EventsManager';
        }

    // Get Trips for specific driver/vehicle
    public getEvents(request: EventFilters): Observable<EventsResults> {
        const obj = this.authHttp.removeObjNullKeys({
            Since: request.periodSince,
            Till: request.periodTill,
            VehicleID: request.vehicleID ? request.vehicleID.toString() : null,
            DriverID: request.driverID ? request.driverID.toString() : null,
            RuleID: request.ruleID ? request.ruleID.toString() : null,
            LocationID: request.locationID ? request.locationID.toString() : null,
            GroupType: request.groupType ? request.groupType.toString() : null,
            GroupID: request.groupID ? request.groupID.toString() : null,
            ClassIdx: request.classIdx,
            StatusIdx: request.statusIdx,
            PriorityIdx: request.priorityIdx,
            PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
            PageSize: request.pageSize ? request.pageSize.toString() : '20',
            Sorting: request.sorting
        });
       
        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }



    public updateEvents(request: EventsUpdate): Observable<EventsUpdateResponse> {
        return this.authHttp
            .put(this.baseURL, request);
    }


    // Get specific event for reviewing
    public getEventDetails(req: EventBaseParams): Observable<EventDetail> {

        const params = new HttpParams({
            fromObject: {
                VehicleID: req.vehicleID.toString(),
                Timestamp: req.timestamp
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetEvent', { params });
    }

    public getEventChanges(req: EventBaseParams): Observable<EventChangeItem[]> {

        const params = new HttpParams({
            fromObject: {
                VehicleID: req.vehicleID.toString(),
                Timestamp: req.timestamp
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetEventChanges', { params });
    }
}
