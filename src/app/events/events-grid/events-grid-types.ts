import {
    MoreOptionsMenuOptions, MoreOptionsMenuOption, MoreOptionsMenuTypes
} from '../../shared/more-options-menu/more-options-menu.types';

export let EventGridHeaderOptions: MoreOptionsMenuOptions[] = [
    {
        term: 'events.grid_menu.batch_edit',
        options: [
            {
                id: 'batch_edit',
                term: 'form.actions.update',
                type: MoreOptionsMenuTypes.none,
                enabled: true,
            }
        ]
    },
    {
        term: 'events.grid_menu.create_from',
        options: [
            {
                id: 'heatmap',
                term: 'events.grid_menu.create_from.heatmap',
                type: MoreOptionsMenuTypes.none,
                enabled: true,
            },
            {
                id: 'list',
                term: 'events.grid_menu.create_from.list',
                type: MoreOptionsMenuTypes.none,
                enabled: true,
            }
        ]
    }
];
