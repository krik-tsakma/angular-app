// FRAMEWORK
import {
    Component, ChangeDetectorRef, OnInit, OnDestroy, OnChanges, SimpleChanges,
    Input, Output, EventEmitter, AfterViewInit, ViewChild, HostBinding,
    ViewEncapsulation, ElementRef
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


// SERVICES
import { EventsService } from '../events.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { UpdateDialogService } from '../update-dialog/update-dialog.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { MaximizeService } from '../../shared/maximize/maximize.service';
import { MoreOptionsMenuOption } from '../../shared/more-options-menu/more-options-menu.types';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { Config } from '../../config/config';

// TYPES
import { EventDetail, EventsResults } from '../events-types';
import { KeyName } from '../../common/key-name';
import { Pager } from '../../common/pager';
import { PeriodTypes, PeriodTypesConverter } from '../../common/period-selector/period-selector-types';
import { EventWidgetTypes } from '../widgets/widgets-types';
import { EventFilters } from '../event-filters/event-filters-types';
import { EventGridHeaderOptions } from './events-grid-types';
import { Resizer } from '../../shared/maximize/maximize-types';
import {
    TableColumnSetting, TableActionItem, TableActionOptions,
    TableCellFormatOptions, TableColumnSortOrderOptions, TableColumnSortOptions
} from '../../shared/data-table/data-table.types';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'events-grid',
    templateUrl: './events-grid.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./events-grid.less'],
})

export class EventsGridComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
    @Input() public filters: EventFilters = {} as EventFilters;
    @Input() public widgetView: boolean;
    @Input() public statuses: KeyName[] = [];
    @Input() public autoRefreshEnabled: boolean;
    @Input() public headerOptionsEnabled: boolean;

    @Output() public createWidget: EventEmitter<EventWidgetTypes> = new EventEmitter();
    @Output() public onSortSelection: EventEmitter<string> = new EventEmitter();

    @ViewChild('eventsGrid', { static: true }) public eventsGrid: ElementRef;
    @ViewChild('eventsGridHeader', { static: true }) public eventsGridHeader: ElementRef;

    public loading: boolean = false;
    public message: string;
    public events: EventDetail[];
    public pager: Pager = new Pager();
    public eventWidgetTypes: any = EventWidgetTypes;
    public periodSelector: any;
    public tableSettings: TableColumnSetting[];
    public gridHeaderOptions: any;

    private initialHeight: number = null;
    private serviceSubscriber: any;
    private refreshIntervalHandler: any;

    constructor(
        public userOptions: UserOptionsService,
        public snackBar: SnackBarService,
        public elementRef: ElementRef,
        private maximizeService: MaximizeService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: EventsService,
        private updateDialogService: UpdateDialogService,
        private changeDetectionRef: ChangeDetectorRef,
        private unsubscribe: UnsubscribeService,
        private configService: Config) {
            this.tableSettings = [
                {
                    header: ' ',
                    primaryKey: 'timestamp',
                    actions: [TableActionOptions.check]
                },
                {
                    header: 'events.list.dateTime',
                    primaryKey: 'timestamp',
                    format: TableCellFormatOptions.dateTime,
                    sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
                },
                {
                    header: 'events.list.vehicle',
                    primaryKey: 'vehicleLabel',
                    sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
                },
                {
                    header: 'events.list.driver',
                    primaryKey: 'driverLabel',
                    sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
                },
                {
                    header: 'events.list.rule',
                    primaryKey: 'ruleName',
                    sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
                },
                {
                    header: 'events.list.priority',
                    primaryKey: 'priorityLabel',
                    sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
                },
                {
                    header: 'events.list.class',
                    primaryKey: 'classLabel',
                    sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
                },
                {
                    header: 'events.list.locationAddress',
                    primaryKey: 'address',
                    sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
                },
                {
                    header: 'events.list.status',
                    primaryKey: 'statusLabel',
                    sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
                },
                {
                    primaryKey: ' ',
                    header: ' ',
                    actions: [TableActionOptions.edit, TableActionOptions.details]
                }
            ];
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting && x.sorting.selected === true);
            this.filters.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
            this.filters.periodType = PeriodTypes.Today;
            this.periodSelector = new PeriodTypesConverter();

            this.maximizeService.resizer.subscribe((resizer: Resizer) => {
                if (resizer.htmlID !== this.elementRef.nativeElement.id) {
                    return;
                }

                this.resize(resizer.maximized, resizer.height);
            });
    }

    public ngOnInit() {
       // clone array
    }

    public ngOnChanges(changes: SimpleChanges) {
        const autoRefreshChange = changes['autoRefreshEnabled'];
        if (autoRefreshChange !== undefined && autoRefreshChange.currentValue !== undefined && autoRefreshChange.firstChange === false) {
            this.setAutoRefresh(autoRefreshChange.currentValue);
        }
    }

    // Clean subscribers to avoid memory leak
    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.setAutoRefresh(false);
    }

    public ngAfterViewInit() {
        if (this.autoRefreshEnabled === undefined || this.autoRefreshEnabled === null) {
            this.autoRefreshEnabled = true;
        }
        this.setAutoRefresh(this.autoRefreshEnabled);

        if (this.headerOptionsEnabled === true) {
            this.gridHeaderOptions = EventGridHeaderOptions;
        }

        if (this.filters.periodType !== PeriodTypes.Custom) {
            const dt = this.periodSelector.convert(this.filters.periodType);
            this.filters.periodSince = dt.datetimeSince.format('YYYY-MM-DD HH:mm');
            this.filters.periodTill = dt.datetimeTill.format('YYYY-MM-DD HH:mm');
        }

        this.get(true);

        // manually trigger change detection for the current component.
        // otherwise "Expression ___ has changed after it was checked" error
        this.changeDetectionRef.detectChanges();
    }

    // When the user selected a column to sort (from list view)
    public onSort(column: TableColumnSetting) {
        this.filters.sorting = column.sorting.stringFormat(column.primaryKey);
        this.onSortSelection.emit(this.filters.sorting);
        this.get(true);
    }

    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }

    public onManageEvent(item: TableActionItem) {
        if (!item) {
            return;
        }

        switch (item.action) {
            case TableActionOptions.edit:
                this.updateDialog([item.record], item.record.statusIDX);
                break;
            case TableActionOptions.details:
                this.eventDetail(item.record.timestamp, item.record.vehicleLabel);
                break;
            default:
                break;

        }
    }

    // Choose event for extra details
    public eventDetail(timestamp, vehicleName): void {
        const vehicle = this.events.find((event) => {
            return event.timestamp === timestamp && event.vehicleLabel === vehicleName;
        });

        this.router.navigate(
            [
                '/events',
                vehicle.vehicleID, timestamp.toString()
            ],
            { skipLocationChange: !this.configService.get('router') }
        );
    }

    public reload(request: EventFilters) {
        this.filters = request;
        this.get(true);
    }

    // ----------------
    // GRID ACTIONS
    // ----------------

    public onHeaderOptionSelection(option: MoreOptionsMenuOption) {
        if (!option || !option.id) {
            return;
        }

        switch (option.id) {
            case 'heatmap':
                this.createWidget.emit(EventWidgetTypes.HeatMap);
                break;
            case 'list':
                this.createWidget.emit(EventWidgetTypes.Data);
                break;
            case 'batch_edit':
                this.updateSelected();
                break;
        }
    }

    // WIDGETS
    public createViewFromFiltersAvailability(): boolean {
        return Number(this.filters.periodType) === PeriodTypes.Custom;
    }


    // UPDATE
    public selectAllForUpdate(e) {
        if (e.target.checked) {
            this.events.forEach((event) => {
                event.checked = true;
            });
        } else {
            this.events.forEach((event) => {
                event.checked = false;
            });
        }
    }

    public updateSelected() {
        const selected = this.events.filter((event) => {
            return event.checked;
        });

        if (selected.length > 0) {
            this.updateDialog(selected);
        } else {
            this.snackBar.open('events.list.snackbar', 'form.actions.ok');
        }
    }

    // RESIZE

    public resize(maximized: boolean, height: number) {
        // if we maximized the grid, set proper height
        if (maximized === true) {
            if (this.initialHeight === null) {
                this.initialHeight = this.eventsGrid.nativeElement.style.height;
            }
            const headerHeight = this.eventsGridHeader.nativeElement.style.height;
            this.eventsGrid.nativeElement.style.height = height - headerHeight - 55 + 'px';
        } else if (this.initialHeight !== null) { // else return to initial height
            this.eventsGrid.nativeElement.style.height = this.initialHeight;
        }
    }


    // ----------------
    // PRIVATE
    // ----------------

    // Fetch events data
    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.events = new Array<EventDetail>();
        }

        this.filters.pageNumber = this.pager.pageNumber;
        this.filters.pageSize = this.pager.pageSize;

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.serviceSubscriber);

        // then start the new request
        this.serviceSubscriber = this.service
            .getEvents(this.filters)
            .subscribe(
                (response: EventsResults) => {
                    this.loading = false;
                    // Get the actual data
                    if (response && response.results) {
                        const res = response.results.map((result) => {
                            result.checked = false;
                            return result;
                        });

                        this.events = this.events.concat(res);
                    }

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;

                    if (!this.events || this.events.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }

    private updateDialog(events: EventDetail[], statusInput?: number) {
        this.updateDialogService
            .confirm(this.statuses)
            .subscribe((result: any) => {
                if (result === 'no') {
                    return;
                }

                if (result) {
                    this.loading = true;
                    let updateEvent;
                    if (events.length === 1 && Number(result.status) === events[0].statusIDX) {
                        return;
                    } else {
                        updateEvent = {
                            Events: [],
                            ClassIdx: null,
                            StatusIdx: Number(result.status),
                            PriorityIdx: null
                        };
                        events.forEach((event) => {
                            updateEvent.Events.push({
                                VehicleID: event.vehicleID,
                                Timestamp: event.timestamp
                            });
                        });
                    }

                    this.service.updateEvents(updateEvent)
                        .subscribe(
                            (res) => {
                                if (res.results[0].code === 0) {
                                    this.get(true);
                                } else {
                                    this.loading = false;
                                }
                                return;
                            },
                            (err) => {
                                this.loading = false;
                                this.message = 'data.error';
                            }
                        );
                }
            });
    }

    private setAutoRefresh(autoRefreshEnabled: boolean) {
        if (autoRefreshEnabled === true) {
            // Datatable refreshes every RefreshInterval minute
            // if period type is today to fetch the new events
            const user = this.userOptions.getUser();
            this.refreshIntervalHandler = setInterval(() => {
            if (this.filters) {
                const today = moment();
                if (this.filters.periodType === PeriodTypes.Today ||
                    this.filters.periodType === PeriodTypes.ThisWeek ||
                    this.filters.periodType === PeriodTypes.ThisMonth ||
                    (this.filters.periodType === PeriodTypes.Custom && (
                        moment(this.filters.periodSince).isSame(today, 'day') || moment(this.filters.periodTill).isSame(today, 'day')))) {
                    this.get(true);
                }
            }
        }, (user.refreshIntervalSecs * 1000));
        } else {
            if (this.refreshIntervalHandler) {
                clearInterval(this.refreshIntervalHandler);
            }
        }
    }
}
