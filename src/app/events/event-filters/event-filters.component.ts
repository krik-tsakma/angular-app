// FRAMEWORK
import { Component, OnInit, AfterViewInit,
        ViewEncapsulation, Input, Output, EventEmitter, ViewChild
} from '@angular/core';

// COMPONENTS
import { PeriodSelectorComponent } from '../../common/period-selector/period-selector.component';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';

// TYPES
import { KeyName } from '../../common/key-name';
import { PeriodTypes, PeriodTypeOption } from '../../common/period-selector/period-selector-types';
import { SearchType } from '../../shared/search-mechanism/search-mechanism-types';
import { EventFilters } from './event-filters-types';
import { EntityTypes } from '../../shared/entity-types/entity-types';
import { AssetGroupTypes, AssetGroupItem } from '../../admin/asset-groups/asset-groups.types';
import { User } from '../../auth/user';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'event-filters',
    templateUrl: './event-filters.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./event-filters.less']
})

export class EventFiltersComponent implements OnInit, AfterViewInit {
    @ViewChild(PeriodSelectorComponent, { static: true }) public periodSelectorComponent: PeriodSelectorComponent;
    @Input() public request: EventFilters;
    @Output() public notifyOnChange: EventEmitter<EventFilters> =
        new EventEmitter<EventFilters>();
    @Output() public notifyOnLoad: EventEmitter<KeyName[]> =
        new EventEmitter<KeyName[]>();
    public searchTypes: any = SearchType;
    public periodSelectorOptions: PeriodTypes[] = [
        PeriodTypes.ThisMonth,
        PeriodTypes.LastMonth,
        PeriodTypes.LastWeek,
        PeriodTypes.ThisWeek,
        PeriodTypes.Today,
        PeriodTypes.Custom
    ];

    public trans: boolean = false;

    public user: User;
    public entityTypes = EntityTypes;
    public entityTypesOptions: EntityTypes[] = [EntityTypes.Me, EntityTypes.Drivers, EntityTypes.Vehicles];
    public entityOption: EntityTypes;

    public groupTypes = AssetGroupTypes;

    public visibility: boolean = true;

    constructor(public userOptions: UserOptionsService) {
          this.user = userOptions.getUser();
    }

    public ngOnInit() {
       // foo
    }

    public ngAfterViewInit() {
        if (!this.request) {
            this.request.periodType = PeriodTypes.Today;
        } else {
            if (this.request.periodType === PeriodTypes.Custom) {
                this.periodSelectorComponent.setCustomDates(
                    moment(this.request.periodSince),
                    moment(this.request.periodTill)
                );
            }
        }

        if (!this.request.periodSince) {
            this.request.periodSince = this.periodSelectorComponent.selectedOption.datetimeSince.format('YYYY-MM-DD HH:mm');
            this.request.periodTill = this.periodSelectorComponent.selectedOption.datetimeTill.format('YYYY-MM-DD HH:mm');
        }

        if (this.request.groupID) {
            if (this.request.groupType === AssetGroupTypes.Driver) {
                this.entityOption = EntityTypes.Drivers;
            } else {
                this.entityOption = EntityTypes.Vehicles;
            }
        } else {
            this.entityOption = EntityTypes.None;
        }
    }


    // Select VEHICLE or DRIVER or LOCATION or RULE
    public onVehicleSelect(item?: KeyName) {
        this.request.vehicleID = item ? item.id : null;
        this.request.vehicleName = item ? item.name : null;
        this.notifyOnChange.emit(this.request);
    }
    public onDriverSelect(item?: KeyName) {
        this.request.driverID = item ? item.id : null;
        this.request.driverName = item ? item.name : null;

        if (item === null && this.entityOption === EntityTypes.Me) {
            this.entityOption = EntityTypes.None;
        }
        this.notifyOnChange.emit(this.request);
    }
    public onLocationSelect(item?: KeyName) {
        this.request.locationID = item ? item.id : null;
        this.request.locationName = item ? item.name : null;
        this.notifyOnChange.emit(this.request);
    }
    public onRuleSelect(item?: KeyName) {
        this.request.ruleID = item ? item.id : null;
        this.request.ruleName = item ? item.name : null;
        this.notifyOnChange.emit(this.request);
    }

    // On every radio/select option
    public onEventPropertySelect(id?: number) {
        this.notifyOnChange.emit(this.request);
    }

    // =========================
    // PERIOD SELECTOR CALLBACK
    // =========================
    public onPeriodSelect(pto: PeriodTypeOption): void {
        this.request.periodSince = pto.datetimeSince.format('YYYY-MM-DD HH:mm');
        this.request.periodTill = pto.datetimeTill.format('YYYY-MM-DD HH:mm');
        this.request.periodType = pto.id;
        this.notifyOnChange.emit(this.request);
    }

    // =========================
    // ENTITY TYPES CALLBACK
    // =========================
    public onChangeEntityTypeGroup(newType: EntityTypes): void {
        this.entityOption = Number(newType);
        // when this is driver user driver settings
        switch (this.entityOption) {
            case EntityTypes.None:
                this.request.groupID = null;
                break;
            case EntityTypes.Me:
                this.request.driverID = this.user.driverId;
                this.request.driverName = this.user.fullname;
                this.request.groupID = null;
                break;
            case EntityTypes.Drivers:
                this.request.groupType = AssetGroupTypes.Driver;
                this.request.groupID = null;
                this.request.driverID = null;
                this.request.driverName = null;
                this.request.vehicleID = null;
                this.request.vehicleName = null;
                break;
            case EntityTypes.Vehicles:
                this.request.groupType = AssetGroupTypes.Vehicle;
                this.request.groupID = null;
                this.request.driverID = null;
                this.request.driverName = null;
                this.request.vehicleID = null;
                this.request.vehicleName = null;
                break;
            default:
                break;
        }

        this.notifyOnChange.emit(this.request);
    }

    // =========================
    // ASSET GROUP CALLBACKS
    // =========================
    // When the user selects a driver group from the DDL
    public onChangeGroup(newGroup?: AssetGroupItem): void {
        this.request.groupID = newGroup
            ? newGroup.id
            : null;

        this.request.driverID = null;
        this.request.driverName = null;
        this.request.vehicleID = null;
        this.request.vehicleName = null;

        this.notifyOnChange.emit(this.request);
    }

}
