
import { PeriodTypes } from '../../common/period-selector/period-selector-types';
import { AssetGroupTypes, AssetGroupItem } from '../../admin/asset-groups/asset-groups.types';
import { PagerRequest } from '../../common/pager';

export class EventFilters implements PagerRequest {
    public periodType: PeriodTypes;
    public periodSince: string;
    public periodTill: string;
    public priorityIdx: string;
    public classIdx: string;
    public statusIdx: string;
    public vehicleID?: number;
    public driverID?: number;
    public ruleID?: number;
    public locationID?: number;
    public groupID?: number;
    public groupType: AssetGroupTypes;
    public vehicleName?: string;
    public driverName?: string;
    public ruleName?: string;
    public locationName?: string;
    public pageNumber: number;
    public pageSize: number;
    public sorting: string;

    constructor() {
        this.pageNumber = 1;
        this.pageSize = 20;
        this.sorting = 'Timestamp asc';
    }
}
