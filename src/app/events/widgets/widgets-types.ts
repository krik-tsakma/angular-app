import { PeriodTypes } from '../../common/period-selector/period-selector-types';
import { EventFilters } from '../event-filters/event-filters-types';

export class EventWidget extends EventFilters {
    public title: string;
    public id: number;
    public order: number;
    public type: EventWidgetTypes;
}

export enum EventWidgetTypes {
    Data = 0,
    HeatMap = 1
}


export class EventWidgetsResults {
    public items: EventWidget[];
}

// CREATE
export enum EventWidgetCreateResultCodeOptions {
    Ok = 0
}

export class EventWidgetCreateResult {
    public code: EventWidgetCreateResultCodeOptions;
}

// UPDATE
export enum EventWidgetUpdateResultCodeOptions {
    Ok = 0,
    NotFound = 1
}

export class EventWidgetUpdateResult {
    public code: EventWidgetUpdateResultCodeOptions;
}

// UPDATE MANY
export class EventWidgetUpdateManyResult {
    public code: EventWidgetUpdateManyResultCodeOptions;
}

export enum EventWidgetUpdateManyResultCodeOptions {
    Ok = 0,
    UknownError = 1
}

// DELETE
export class EventWidgetDeleteResult {
    public code: EventWidgetDeleteResultCodeOptions;
}

export enum EventWidgetDeleteResultCodeOptions {
    Ok = 0,
    NotFound = 1
}
