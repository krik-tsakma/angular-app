// FRAMEWORK
import { Component, AfterViewInit, OnDestroy, Input, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';

// SERVICES
import { SnackBarService } from '../../shared/snackbar.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { MaximizeService } from '../../shared/maximize/maximize.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { EventManagerWidgetService } from './widgets.service';

// TYPES
import {
    EventWidget, EventWidgetTypes, EventWidgetUpdateManyResultCodeOptions,
    EventWidgetCreateResult, EventWidgetCreateResultCodeOptions
} from './widgets-types';
import { EventFilters } from '../event-filters/event-filters-types';
import { Resizer } from '../../shared/maximize/maximize-types';

// MOMENT
import moment from 'moment';


@Component({
    selector: 'event-manager-widgets',
    templateUrl: './widgets.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./widgets.less'],
    providers: [EventManagerWidgetService]
})

export class EventManagerWidgetsComponent implements AfterViewInit, OnDestroy  {
    @Input() public autoRefreshEnabled: boolean;
    public widgets: EventWidget[];
    public widgetTypes = EventWidgetTypes;
    public loading: boolean;
    public editLayoutMode: boolean = false;
    public maximizer: Resizer;

    private serviceSubscriber: any;

    constructor(
        public iconRegistry: MatIconRegistry,
        public sanitizer: DomSanitizer,
        private widgetService: EventManagerWidgetService,
        private snackBar: SnackBarService,
        private translateService: CustomTranslateService,
        private changeDetectionRef: ChangeDetectorRef,
        private maximizeService: MaximizeService,
        private unsubscribe: UnsubscribeService) {
            // register svg icons used in this page
            iconRegistry
                .addSvgIcon('customize_icon',
                    sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/widgets/customize_icon.svg'));

            this.maximizeService.resizer.subscribe((resizer: Resizer) => {
                this.maximizer = resizer;
            });
        }

    // On Init get widgets from Back-end
    public ngAfterViewInit(): void {
        this.get();
        // manually trigger change detection for the current component.
        // otherwise "Expression ___ has changed after it was checked" error
        this.changeDetectionRef.detectChanges();
    }

    // Clean widgetSubscribe to avoid memory leak
    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = null;
    }


    // ----------------
    // CRUD
    // ----------------

    // after a widget deletion, remove it from the list
    public create(widgetType: EventWidgetTypes, filters: EventFilters): void {
        const w = filters as EventWidget;
        w.type = widgetType;
        w.title = widgetType === EventWidgetTypes.Data
            ? this.translateService.instant('events.grid_menu.create_from.list')
            : this.translateService.instant('events.grid_menu.create_from.heatmap');
        w.title += ': ' + this.translateService.instant('events.widget.create_on') +  ' ' +  moment().format('LLLL');

        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.widgetService
            .create(w)
            .subscribe((response: EventWidgetCreateResult) => {
                this.loading = false;
                if (response.code !== EventWidgetCreateResultCodeOptions.Ok) {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                } else {
                    this.get();
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
            });
    }

     public update(w: EventWidget) {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.widgetService
            .update(w)
            .subscribe((res: any) => {
                this.snackBar.open('form.save_success', 'form.actions.ok');
                this.editLayoutMode = false;
                this.loading = false;
            },
            (err) => {
                if (err.ok === false || (err.status && err.status !== 200)) {
                    this.loading = false;
                    this.editLayoutMode = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            });
    }


    // after a widget deletion, remove it from the list
    public delete(id: number) {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.widgetService
            .delete(id)
            .subscribe((res: any) => {
                this.loading = false;
                this.widgets = this.widgets.filter((widget) => {
                    return widget.id !== id;
                });
                this.snackBar.open('form.delete_success', 'form.actions.ok');
            },
            (error) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
            });
    }


    // ----------------
    // LAYOUT
    // ----------------

    // enable edit layout mode
    public customizeLayout(): void {
        if (this.editLayoutMode === true ||
            (!this.widgets || this.widgets.length === 0)) {
            return;
        }

        this.editLayoutMode = true;
    }

    // save layout changes
     public onSaveEdit() {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.widgetService
            .updateMany(this.widgets)
            .subscribe((res: any) => {
                this.loading = false;
                this.editLayoutMode = false;
                if (res.code !== EventWidgetUpdateManyResultCodeOptions.Ok) {
                    this.snackBar.open('data.error', 'form.actions.close');
                    return;
                }
            },
            (err) => {
                if (err.ok === false || (err.status && err.status !== 200)) {
                    this.loading = false;
                    this.editLayoutMode = false;
                    this.snackBar.open('data.error', 'form.actions.close');
                }
            });
    }

    // cancel layout changes
    public onCancelEdit() {
        this.editLayoutMode = false;
        this.get();
    }

    public onChangeWidgetOrderUp(widget: EventWidget) {
        const currentIndex = this.widgets.findIndex((w) => w.id === widget.id);
        if (currentIndex === 0) {
            return;
        }

        const prevWidget = this.widgets[currentIndex - 1];
        if (prevWidget) {
            // clone current order num
            const current = JSON.parse(JSON.stringify(widget.order));
            const prev = JSON.parse(JSON.stringify(prevWidget.order));
            widget.order = prev;
            prevWidget.order = current;
        }

        this.sortAscendingOrder(this.widgets);
    }

    public onChangeWidgetOrderDown(widget: EventWidget) {
        const currentIndex = this.widgets.findIndex((w) => w.id === widget.id);

        const widgetsCount = this.widgets ? this.widgets.length : 0;
        if (widgetsCount === 0 ||
            widgetsCount === 1 ||
            currentIndex === (widgetsCount - 1)) {
            return;
        }

        const nextWidget = this.widgets[currentIndex + 1];
        if (nextWidget) {
            // clone current order num
            const current = JSON.parse(JSON.stringify(widget.order));
            const next = JSON.parse(JSON.stringify(nextWidget.order));
            widget.order = next;
            nextWidget.order = current;
        }

        this.sortAscendingOrder(this.widgets);
    }


    // ----------------
    // MAXIMIZE
    // ----------------
    public hideElement(id) {
        return this.maximizer != null && this.maximizer.maximized === true && this.maximizer.htmlID !== id;
    }

    // ----------------
    // PRIVATE
    // ----------------
    private sortAscendingOrder(widgets: EventWidget[]): EventWidget[] {
        return widgets.sort((x1, x2) => x1.order  - x2.order);
    }

    // Get method for widgets
    private get(): void {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.widgetService
            .get()
            .subscribe((widgets) => {
                this.loading = false;
                this.widgets = widgets.items;
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.close');
            });
    }
}
