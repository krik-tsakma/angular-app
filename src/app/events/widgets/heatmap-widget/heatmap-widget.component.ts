// FRAMEWORK
import {
    Component, EventEmitter, ViewChild, ElementRef, SimpleChanges,
    OnInit, AfterViewInit, ViewEncapsulation, OnChanges,
    Input, Output, ChangeDetectorRef, OnDestroy
} from '@angular/core';

// TYPES
import { EventWidget } from '../widgets-types';
import { EventsResults, EventDetail } from '../../events-types';
import { EventFilters } from '../../event-filters/event-filters-types';
import { PeriodTypesConverter, PeriodTypes } from '../../../common/period-selector/period-selector-types';
import { MapsComponent } from '../../../google-maps/maps.component';
import { MapModeOptions } from '../../../google-maps/types/map.types';

// SERVICES
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { EventManagerWidgetService } from '../widgets.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { EventsService } from '../../events.service';
import { MaximizeService } from '../../../shared/maximize/maximize.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { ConfirmationDialogParams } from '../../../shared/confirm-dialog/confirm-dialog-types';

@Component({
    selector: 'event-heatmap-widget',
    templateUrl: './heatmap-widget.html',
    encapsulation: ViewEncapsulation.None,
})

export class EventHeatmapWidgetComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
    @Input() public widget: EventWidget;
    @Input() public autoRefreshEnabled: boolean;
    @Output() public onDelete = new EventEmitter<number>();
    @Output() public onUpdate = new EventEmitter<EventWidget>();
    @ViewChild(MapsComponent, { static: true }) public googleMaps: MapsComponent;

    public loading: boolean;
    public events: EventDetail[] = [];
    public editMode: boolean;
    public originalTitle: string;
    public visibility: boolean;
    public mapModes: any = MapModeOptions;

    private serviceSubscriber: any;
    private refreshIntervalHandler: any;

    constructor(public elementRef: ElementRef,
                private userOptions: UserOptionsService,
                private eventWidgetsService: EventManagerWidgetService,
                private eventsService: EventsService,
                private confirmDialogService: ConfirmDialogService,
                private snackBar: SnackBarService,
                private changeDetectionRef: ChangeDetectorRef,
                private maximizeService: MaximizeService) {

        this.maximizeService.resizer.subscribe((resizer: any) => {
            if (resizer.htmlID !== this.elementRef.nativeElement.id) {
                return;
            }
            // if we maximized the grid, set proper height
            this.googleMaps.resizeBy(resizer.height);
        });
    }

    public ngOnInit() {
        this.visibility = true;
        this.loading = false;
        this.editMode = false;
    }

    public ngOnChanges(changes: SimpleChanges) {
        const autoRefreshChange = changes['autoRefreshEnabled'];
        if (autoRefreshChange !== undefined && autoRefreshChange.currentValue !== undefined && autoRefreshChange.firstChange === false) {
            this.setAutoRefresh(autoRefreshChange.currentValue);
        }
    }

    public ngAfterViewInit() {
        if (this.autoRefreshEnabled === undefined || this.autoRefreshEnabled === null) {
            this.autoRefreshEnabled = true;
        }

        this.setAutoRefresh(this.autoRefreshEnabled);

        this.get();

        // manually trigger change detection for the current component.
        // otherwise "Expression ___ has changed after it was checked" error
        this.changeDetectionRef.detectChanges();
    }

    // Clean subscribers to avoid memory leak
    public ngOnDestroy() {
        this.setAutoRefresh(false);
    }

    public onToggleVisibility(): void {
        this.visibility = !this.visibility;
    }

    public onEdit(cancel: boolean) {
        if (cancel === false) {
            this.editMode = true;
            this.originalTitle = this.widget.title;
        } else {
            this.editMode = false;
            this.widget.title = this.originalTitle;
        }
    }

    public onSave(w: EventWidget) {
        this.editMode = false;
        this.onUpdate.emit(w);
    }


   // EVENT HANDLERS
    public openDeleteDialog(id: number) {
        const confirmReq = {
            title: 'form.actions.delete',
            message: 'form.warnings.are_you_sure',
            submitButtonTitle: 'form.actions.delete'
        } as ConfirmationDialogParams;

        this.confirmDialogService
            .confirm(confirmReq)
            .subscribe((result) => {
                if (result === 'yes') {
                    this.onDelete.emit(id);
                }
            });
    }

    private get(): void {
        const params = this.widget as EventFilters;
        const ptc = new PeriodTypesConverter();
        const dt = ptc.convert(params.periodType);
        params.periodSince = dt.datetimeSince.format('YYYY-MM-DD HH:mm');
        params.periodTill = dt.datetimeTill.format('YYYY-MM-DD HH:mm');

        this.loading = true;
        this.serviceSubscriber = this.eventsService
            .getEvents(params)
            .subscribe((response: EventsResults) => {
                    this.loading = false;
                    if (response) {
                       this.events = response.results;
                    }
                },
                (err) => {
                    this.loading = false;
                    this.snackBar.open('data.error', 'form.actions.close');
                }
            );
    }

    // HELPERS

    private setAutoRefresh(autoRefreshEnabled: boolean) {
        if (autoRefreshEnabled === true) {
            // Datatable refreshes every RefreshInterval minute
            // if period type is today to fetch the new events
            const user = this.userOptions.getUser();
            this.refreshIntervalHandler = setInterval(() => {
                const params = this.widget as EventFilters;
                if (params && (params.periodType === PeriodTypes.Today ||
                    params.periodType === PeriodTypes.ThisWeek ||
                    params.periodType === PeriodTypes.ThisMonth)) {
                    this.get();
                }
            }, (user.refreshIntervalSecs * 1000));
        } else {
            if (this.refreshIntervalHandler) {
                clearInterval(this.refreshIntervalHandler);
            }
        }
    }
}
