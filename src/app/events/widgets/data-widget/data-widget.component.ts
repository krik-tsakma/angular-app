// FRAMEWORK
import {
    Component, EventEmitter, ElementRef, ViewChild,
    OnInit, AfterViewInit, ViewEncapsulation,
    Input, Output
} from '@angular/core';

// TYPES
import { EventWidget } from '../widgets-types';
import { EventFilters } from '../../event-filters/event-filters-types';
import { EventDetail } from '../../events-types';
import { PeriodTypesConverter } from '../../../common/period-selector/period-selector-types';
import { Resizer } from '../../../shared/maximize/maximize-types';
import { EventsGridComponent } from '../../events-grid/events-grid.component';

// SERVICES
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { MaximizeService } from '../../../shared/maximize/maximize.service';
import { ConfirmationDialogParams } from '../../../shared/confirm-dialog/confirm-dialog-types';

@Component({
    selector: 'event-data-widget',
    templateUrl: './data-widget.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./data-widget.less'],
})

export class EventDataWidgetComponent implements OnInit, AfterViewInit {
    @ViewChild('widgetName', { static: true }) public nameInput: ElementRef;
    @Input() public widget: EventWidget;
    @Input() public autoRefreshEnabled: boolean;
    @Output() public onDelete = new EventEmitter<number>();
    @Output() public onUpdate = new EventEmitter<EventWidget>();
    @ViewChild('eventsWidgetGrid', { static: true }) public gridComponent: EventsGridComponent;

    public loading: boolean;
    public events: EventDetail[] = [];
    public request: EventFilters;
    public editMode: boolean;
    public originalTitle: string;
    public visibility: boolean;

    constructor(public elementRef: ElementRef,
                private confirmDialogService: ConfirmDialogService,
                private maximizeService: MaximizeService) {

        this.maximizeService.resizer.subscribe((resizer: Resizer) => {
            if (resizer.htmlID !== this.elementRef.nativeElement.id) {
                return;
            }
            this.gridComponent.resize(resizer.maximized, resizer.height - 100);
        });
    }

    public ngOnInit() {
        this.visibility = true;
        this.loading = false;
        this.editMode = false;
    }

    public ngAfterViewInit() {
        this.request = this.widget as EventFilters;
        const ptc = new PeriodTypesConverter();
        const dt = ptc.convert(this.request.periodType);
        this.request.periodSince = dt.datetimeSince.format('YYYY-MM-DD HH:mm');
        this.request.periodTill = dt.datetimeTill.format('YYYY-MM-DD HH:mm');
    }

    public onToggleVisibility(): void {
        this.visibility = !this.visibility;
    }

    public onEdit(cancel: boolean) {
        if (cancel === false) {
            this.editMode = true;
            this.originalTitle = this.widget.title;
        } else {
            this.editMode = false;
            this.widget.title = this.originalTitle;
        }
    }

    public onSave(w: EventWidget) {
        this.editMode = false;
        this.onUpdate.emit(w);
    }

    public openDeleteDialog(id: number) {
        const confirmReq = {
            title: 'form.actions.delete',
            message: 'form.warnings.are_you_sure',
            submitButtonTitle: 'form.actions.delete'
        } as ConfirmationDialogParams;

        this.confirmDialogService
            .confirm(confirmReq)
            .subscribe((result) => {
                if (result === 'yes') {
                    this.onDelete.emit(id);
                }
            });
    }
}
