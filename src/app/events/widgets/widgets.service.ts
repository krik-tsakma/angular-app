// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { EventWidget, EventWidgetsResults, EventWidgetCreateResult,
    EventWidgetUpdateResult, EventWidgetDeleteResult, EventWidgetUpdateManyResult } from './widgets-types';

@Injectable()
export class EventManagerWidgetService {

    constructor(
        private authHttp: AuthHttp,
        private config: Config) { }

    // Get all the widgets
    public get(): Observable<EventWidgetsResults> {
        return this.authHttp
            .get(this.config.get('apiUrl') + '/api/EventsReviewWidgets');
    }

    // Create new widget - Not in use at the moment
    public create(widget: EventWidget): Observable<EventWidgetCreateResult> {
        return this.authHttp
            .post(this.config.get('apiUrl') + '/api/EventsReviewWidgets/', widget);
    }

    // Update method for widget in edit page
    public update(widget: EventWidget): Observable<EventWidgetUpdateResult> {
        return this.authHttp
            .put(this.config.get('apiUrl') + '/api/EventsReviewWidgets/', widget);
    }

    // Update method for widget orders
    public updateMany(widgets: EventWidget[]): Observable<EventWidgetUpdateManyResult> {
        return this.authHttp
            .put(this.config.get('apiUrl') + '/api/EventsReviewWidgets/ChangeOrder', widgets);
    }

    // Delete event widget
    public delete(widgetID: number): Observable<EventWidgetDeleteResult> {
        return this.authHttp
            .delete(this.config.get('apiUrl') + '/api/EventsReviewWidgets/' + widgetID);
    }
}
