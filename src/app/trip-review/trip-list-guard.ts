// FRAMEWORK
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

// SERVICES
import { UserOptionsService } from '../shared/user-options/user-options.service';

// TYPES
import { SearchType } from '../shared/search-mechanism/search-mechanism-types';

@Injectable()
export class TripListGuard implements CanActivate {

    constructor(private userOptions: UserOptionsService) { }

    public canActivate(route: ActivatedRouteSnapshot, innerState: RouterStateSnapshot) {
        const u = this.userOptions.getUser();
        if (u.isDriver === true && u.isManager === false) {
            if (Number(route.params.assetID) !== u.driverId || SearchType[SearchType.Driver] !== route.params.assetType) {
                return false;
            }      

            return true;
        } 

        return true;
        
    }
}
