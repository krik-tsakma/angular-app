// FRAMEWORK
import { OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// RXJS
import { Subject } from 'rxjs';

// SERVICES
import { TripReviewService } from './trip-review.service';
import { UserOptionsService } from '../shared/user-options/user-options.service';
import { ResizeService } from '../shared/resize.service';
import { Config } from '../config/config';

// TYPES
import { SearchType } from '../shared/search-mechanism/search-mechanism-types';
import { AssetTypes } from '../common/entities/entity-types';
import { TripReviewRouteParams } from './trip-review-types';

export class TripReviewBaseComponent implements OnInit {    
    public routeParameters: Subject<TripReviewRouteParams> = new Subject<TripReviewRouteParams>();
    private searchType: SearchType;
    private parameters: TripReviewRouteParams;

    constructor(
        protected router: Router,
        protected activatedRoute: ActivatedRoute,
        protected resizeService: ResizeService,
        protected userOptionsService: UserOptionsService,
        protected tripReviewService: TripReviewService,
        protected configService: Config
    ) {
            // foo
    }

    // ================
    // LIFE CYCLE 
    // ================

    public ngOnInit() {
        this.activatedRoute.params.subscribe((params: TripReviewRouteParams) => { 
            if (!params) {
                return;
            }
            this.parameters = params;
            const assetType = SearchType[params.assetType];
            this.searchType = assetType;

            // subscription is needed to alert url parameters changes
            // otherwise the details component will never know,
            // because activatedRoute.params.subscribe happens here 
            this.routeParameters.next(this.parameters);
        });
    }



    // ================
    // DATA
    // ================

    public getRouteParamers(): TripReviewRouteParams {
        return this.parameters;
    }
    
    public getAssetType() {
        return this.searchType === SearchType.Driver ? AssetTypes.HR : AssetTypes.Vehicle;
    }
    
    // ================
    // NAVIGATION 
    // ================

    public navigateToSearch() {
        this.router.navigate(['/trip-review'], { skipLocationChange: !this.configService.get('router') });
    }
            
    public navigateTo(timestamp?: string) {
        const args = ['/trip-review', SearchType[this.searchType], this.parameters.assetID];
        if (timestamp) {
            args.push(timestamp);
        }

        this.router.navigate(args, { skipLocationChange: !this.configService.get('router') });
    }          

}
