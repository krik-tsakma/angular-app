export interface DriverReassignmentRequest {
    driverID: number;
    vehicleID: number;
    startTimestamp: string;
    comment: string;
}

export interface DriverReassignmentResult {
    success: boolean;
    driverID: number;
}
