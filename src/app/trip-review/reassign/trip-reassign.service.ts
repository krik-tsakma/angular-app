
// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from './../../config/config';
import { AuthHttp } from './../../auth/authHttp';

// TYPES
import { DriverReassignmentRequest } from './trip-reassign.types';

@Injectable()
export class TripReassignService {
    private baseURL: string;
    
    constructor(
        private authHttp: AuthHttp,
        private configService: Config) {
            this.baseURL = this.configService.get('apiUrl') + '/api/TripReassignment';
        }

   
        public reassign(entity: DriverReassignmentRequest): Observable<number>  {
            return this.authHttp
                .put(this.baseURL, entity, { observe: 'response' })
                .pipe(
                    map((res) => {
                        // this is means empty Ok response from the server
                        if (res.status === 200 && !res.body) {
                            return null;
                        } else  {
                            return res.status;
                        }
                    })
                );            
        }
}
