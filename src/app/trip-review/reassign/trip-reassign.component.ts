
// FRAMEWORK
import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

// RxJS
import { Subscription } from 'rxjs/Subscription';

// SERVICES
import { TripReassignService } from './trip-reassign.service';
import { UnsubscribeService } from './../../shared/unsubscribe.service';
import { SnackBarService } from './../../shared/snackbar.service';

// TYPES
import { Trip } from '../trip-review-types';
import { DriverReassignmentRequest, DriverReassignmentResult } from './trip-reassign.types';
import { SearchType } from './../../shared/search-mechanism/search-mechanism-types';
import { KeyName } from './../../common/key-name';

@Component({
    selector: 'trip-reassign',
    templateUrl: 'trip-reassign.html',
    encapsulation: ViewEncapsulation.None
})

export class TripReassignDialogComponent {
    public loading: boolean;
    public request: DriverReassignmentRequest;
    public searchTypes: any = SearchType;
    public driverName: string;

    private trip: Trip;
    private serviceSubscriber: Subscription;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: Trip,
        public service: TripReassignService,
        public dialogRef: MatDialogRef<TripReassignDialogComponent>,        
        private snackBar: SnackBarService,
        private unsubscribe: UnsubscribeService) {
            this.trip = Object.assign({}, data);
            this.request = {
                vehicleID: this.trip.vehicleID,
                startTimestamp: this.trip.startTimestamp,
                driverID: this.trip.driverID,
                comment: null
            };
    }   

    // ===============
    // EVENT HANDLERS
    // ===============

    public closeDialog(successfullyEdited?: boolean) {
        const res = {
            success: successfullyEdited,
            driverID: this.request.driverID 
        } as DriverReassignmentResult;
        this.dialogRef.close(res);
    }

    public onDriverSelect(driver: KeyName) {
        if (!driver) {
            this.driverName = null;
            this.request.driverID = null;
            return;
        }

        if (driver.id === this.trip.driverID) {
            this.driverName = null;
            this.snackBar.open('trip_review.reassignment.error.same_driver', 'form.actions.close');
            return;
        }
        
        this.driverName = driver.name;
        this.request.driverID = driver.id;
    }

    public formInvalid() {
        if (!this.request.driverID) {
            return true;
        }

        if (this.request.driverID === this.trip.driverID) {
            return true;
        }

        return false;
    }


    public save({ value, valid }: { value: any, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);        
        this.serviceSubscriber = this.service
            .reassign(this.request)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.closeDialog(true);
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }
}
