// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from './../../shared/shared.module';

// COMPONENTS
import { TripReassignDialogComponent } from './trip-reassign.component';

// SERVICES
import { TripReassignService } from './trip-reassign.service';

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        TripReassignDialogComponent
    ],
    entryComponents: [
        TripReassignDialogComponent
    ],
    providers: [
        TripReassignService
    ]
})
export class TripReassignModule {}
