// FRAMEWORK
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

// SERVICES
import { UserOptionsService } from '../shared/user-options/user-options.service';
import { Config } from '../config/config';

// TYPES
import { SearchType } from '../shared/search-mechanism/search-mechanism-types';

@Injectable()
export class TripReviewGuard implements CanActivate {

    constructor(
        private router: Router,
        private userOptions: UserOptionsService,        
        private configService: Config) { 
            // foo
        }

    public canActivate(route: ActivatedRouteSnapshot, innerState: RouterStateSnapshot) {
        const u = this.userOptions.getUser();
        if (u.isDriver === true && u.isManager === false) {
            this.router.navigate(['/trip-review', SearchType[SearchType.Driver], u.driverId], { skipLocationChange: !this.configService.get('router') });
            return false;
        } 

        return true;
        
    }
}
