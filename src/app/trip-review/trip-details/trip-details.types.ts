import { AssetTypes } from '../../common/entities/entity-types';
import { Trip, TripDisplaySettings } from '../trip-review-types';
import { Score, ScoreName } from '../../common/score';


export interface TripDetailsRequest {
    id: number;
    type: AssetTypes;
    tripStart: string;
}

export interface TripBaseRequest {
    vehicleID: number;
    tripStartTimestamp: string;
    tripStopTimestamp: string;
}

export interface TripDetailsResult {
    distance: number;
    driverID: number;
    driverLabel: string;
    durationSecs: number;
    startAddress: string;
    startOdometer: any;
    startTimestamp: string;
    stopAddress: string;
    stopOdometer: any;
    stopTimestamp: string;
    tripType: string;
    vehicleID: number;
    vehicleLabel: string;
    energyScore: Score;
    scoreDescription?: TripScoreDescriptionResult;
    nextStartTimestamp?: string;
    nextStopTimestamp?: string;
    previousStartTimestamp?: string;
    previousStopTimestamp?: string;
    displaySettings: TripDisplaySettings;
}

/** Used to represent a trip score description object. */
export interface TripScoreDescription {
    /** The score name of the score */
    scoreName: ScoreName;
    /** The score value of the score */
    score: Score;
    dataPoints: DataPointDescription[];
}
export interface TripScoreDescriptionResult {
    driveCycleID: number;
    driveCycleName: string;
    scores: TripScoreDescription[];
}
export interface DataPointDescription {
    id: number;
    name: string;
    target: number;
    measured: number;
    targetDeviation: number;
    weight: number;
}
