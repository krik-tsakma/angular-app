import { ScoreBuildUpResult } from './../../dashboard/widgets/chart-widget/chart-types';
import { DriverReassignmentResult } from './../reassign/trip-reassign.types';
// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';

// RXJS
import { Subscription } from 'rxjs';

// COMPONENT
import { TripReviewBaseComponent } from '../trip-review-base.component';
import { TripReassignDialogComponent } from '../reassign/trip-reassign.component';

// SERVICES
import { TripReviewService } from '../trip-review.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { ResizeService } from '../../shared/resize.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { AuthService } from './../../auth/authService';
import { Config } from '../../config/config';

// TYPES
import {
    TripDetailsRequest,
    TripDetailsResult,
    DataPointDescription
} from './trip-details.types';
import { Trip, TripReviewRouteParams } from '../trip-review-types';
import { Score, ScoreFormatter, ScoreBuildUpChartItem } from '../../common/score';
import { AmChartTypes } from '../../amcharts/amchart-types';
import { TraceEvent, Trace, AssetTypes } from '../../common/entities/entity-types';
import { MapModeOptions } from '../../google-maps/types/map.types';
import { MapCustomOptionsGroups } from '../../google-maps/custom-options/custom-options.types';
import { TripReviewPropertyOptions, ModuleOptions } from './../../auth/acl.types';

// MOMENT
import moment from 'moment';
import { DateFormatterDisplayOptions } from '../../shared/pipes/dateFormat.pipe';


@Component({
    selector: 'trip-details',
    templateUrl: './trip-details.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./trip-details.less']
})

export class TripDetailsComponent extends TripReviewBaseComponent implements OnInit, OnDestroy {
    public mapModes: any = MapModeOptions;
    public customOptionsGroups = MapCustomOptionsGroups;
    public selectedEvent: TraceEvent;
    public selectedMainTab: number = 0;
    public chartType = AmChartTypes.Serial;
    public loading: boolean;
    public loadingTraces: boolean;
    public loadingEvents: boolean;
    public dateDisplayOptions: any = DateFormatterDisplayOptions;

    public trip: TripDetailsResult;
    public assetLabel: string;
    public traces: Trace[] = null;
    public events: TraceEvent[] = null;
    public tripLocationDataHidden: boolean = true;
    public scoreFormatter: any = ScoreFormatter;

    private request: TripDetailsRequest;

    private targetDeviationTitle: string;
    private scoreBuildUpTranslations: any;

    private subscriberTrip: Subscription;
    private subscriberTraces: Subscription;
    private subscriberEvents: Subscription;
    private tripReassignmentDialogRef: MatDialogRef<TripReassignDialogComponent>;


    constructor(
        protected router: Router,
        protected activatedRoute: ActivatedRoute,
        protected resizeService: ResizeService,
        protected userOptions: UserOptionsService,
        protected service: TripReviewService,
        protected configService: Config,
        private translate: CustomTranslateService,
        private unsubscribe: UnsubscribeService,
        private dialog: MatDialog,
        private authService: AuthService) {
            super(router, activatedRoute, resizeService, userOptions, service, configService);

            translate.get(['dashboard.chart_widget.target_deviation',
                            'score_build_up.good_on_target',
                            'score_build_up.high_on_target',
                            'score_build_up.low_on_target']).subscribe((t: any) => {
                this.targetDeviationTitle = t['dashboard.chart_widget.target_deviation'];
                this.scoreBuildUpTranslations = {
                    goodOnTarget: t['score_build_up.good_on_target'],
                    highOnTarget: t['score_build_up.high_on_target'],
                    lowOnTarget: t['score_build_up.low_on_target']
                };
            });

            // when a change in the route params happen
            // this means the next/previous date button were selected
            this.routeParameters.subscribe((params: TripReviewRouteParams) => {
                const assetType = this.getAssetType();
                this.request = {
                    id: params.assetID,
                    type: assetType,
                    tripStart: params.timestamp,
                } as TripDetailsRequest;
                this.getTripDetails();

            });
        }

    // =================
    // LIFECYCLE
    // =================

    // On initialize
    public ngOnInit() {
        super.ngOnInit();
    }

    public ngOnDestroy() {
        this.unsubscribe.removeSubscription([
            this.subscriberTrip,
            this.subscriberEvents,
            this.subscriberTraces
        ]);
    }

    // =================
    // EVENT HANDLERS
    // =================

    // Click on previous date button
    public onPreviousDate() {
        if (!this.trip || !this.trip.previousStartTimestamp) {
            return;
        }
        this.navigateTo(this.trip.previousStartTimestamp);
    }

    // Click on next date button
    public onNextDate() {
        if (!this.trip || !this.trip.nextStartTimestamp) {
            return;
        }
        this.navigateTo(this.trip.nextStartTimestamp);
    }

    public getScore(scoreID: number, trip: Trip): Score {
        if (!trip.scores || trip.scores.length === 0) {
            return null;
        }
        const score = trip.scores.find((x) => x.id === scoreID);
        if (score) {
            return score;
        }
        return null;
    }

    // =========================
    // DRIVER RE-ASSIGNMENT
    // =========================

    public showTripReassignmentBtn() {
        const hasAccess = this.authService.userHasAccess(ModuleOptions.TripReview, TripReviewPropertyOptions.ReassignDriver);
        let periodValid = false;
        if (moment(this.trip.startTimestamp) >= moment().add(-1, 'month') ) {
            periodValid = true;
        }

        return hasAccess && periodValid;
    }

    public reassignDriver() {
        this.tripReassignmentDialogRef = this.dialog.open(TripReassignDialogComponent, {
            data: this.trip
        });
        this.tripReassignmentDialogRef.afterClosed().subscribe((res: DriverReassignmentResult) => {
            if (res && res.success) {
                if (this.request.type === AssetTypes.HR) {
                    this.request.id = res.driverID;
                }
                this.getTripDetails();
            }
        });
    }

    // =========================
    // MAP
    // =========================

     public onEventSelect(event: TraceEvent) {
        // if the user selected the same event, then he wants to clear previous selection
        if (this.selectedEvent === event) {
            this.selectedEvent = null;
        } else {
            this.selectedEvent = event;
        }
    }

    // =========================
    // HELPERS
    // =========================

    public isSameDay(date1: string, date2: string) {
        return moment(date1).isSame(moment(date2), 'day');
    }

    public onSelectMainTab(index: number) {
        this.selectedMainTab = index;
        this.redrawCharts();
    }

    public redrawCharts() {
        // re-draw charts
        const allCharts: any = document.querySelectorAll('[data-score-id]');
        for (const value of allCharts) {
            const scoreID = Number(value.getAttribute('data-score-id'));
            const score = this.trip.scoreDescription.scores.find((x) => x.score.id === scoreID);
            if (score) {
                const elmID = 'schart' + scoreID;
                const exists = document.getElementById(elmID);
                if (exists.hasChildNodes() === false) {
                    console.log(elmID)
                    const chartOptions = this.getChartOptions(elmID, score.dataPoints);
                    window['AmCharts'].makeChart(elmID, chartOptions);
                }
            }
        }

        // trigger resize event for maps to load properly (to overcome the hidden element issue)
        this.resizeService.trigger();
    }


    // =========================
    // DATA
    // =========================

    private getTripDetails() {
        this.loading = true;
        this.selectedMainTab = 0;
        this.clearData();

        this.unsubscribe.removeSubscription(this.subscriberTraces);
        this.subscriberTraces = this.service
            .getTripDetails(this.request)
            .subscribe(
                (result: TripDetailsResult) => {
                    this.loading = false;
                    if (!result) {
                        console.log('error');
                    }
                    this.trip = result;

                    if (this.request.type === AssetTypes.Vehicle) {
                        this.assetLabel = this.trip.vehicleLabel;
                    } else {
                        this.assetLabel = this.trip.driverLabel;
                    }

                    this.getTripEvents(this.request);
                    this.getTripTraces(this.request);
                    this.resizeService.trigger();

                },
                (err) => {
                    if (err.status !== 200) {
                        this.loading = false;
                    }
                }
            );

    }

    private getTripEvents(params: TripDetailsRequest) {
        this.loadingEvents = true;
        this.unsubscribe.removeSubscription(this.subscriberEvents);
        this.subscriberEvents = this.service
            .getTripEvents(params)
            .subscribe(
                (result: TraceEvent[]) => {
                    this.loadingEvents = false;
                    this.events = result;
                },
                (err) => {
                    if (err.status !== 200) {
                        this.loadingEvents = false;
                    }
                }
            );
    }

    private getTripTraces(params: TripDetailsRequest) {
        this.loadingTraces = true;
        this.unsubscribe.removeSubscription(this.subscriberTraces);
        this.subscriberTraces = this.service
            .getTripTraces(params)
            .subscribe(
                (result: Trace[]) => {
                    this.loadingTraces = false;
                    this.traces = result;
                    if (this.traces.length > 0 && this.traces[0].isHidden === true) {
                        this.tripLocationDataHidden = true;
                    } else {
                        this.tripLocationDataHidden = false;
                    }
                },
                (err) => {
                    if (err.status !== 200) {
                        this.loadingTraces = false;
                    }
                }
            );
    }

    private clearData(): void {
        this.trip = null;
        this.events = null;
        this.traces = null;
    }

    // =========================
    // CHARTS
    // =========================

    private getChartOptions(htmlID, dataProvider: DataPointDescription[]) {
        const sbc = new window['ScoreBuildUpGraph'](
                    htmlID,
                    this.userOptions.localOptions.decimal_separator,
                    this.userOptions.localOptions.thousands_separator,
                    this.targetDeviationTitle,
                    this.scoreBuildUpTranslations.goodOnTarget,
                    this.scoreBuildUpTranslations.highOnTarget,
                    this.scoreBuildUpTranslations.lowOnTarget,
                    null
        );

        const data: ScoreBuildUpResult = {} as ScoreBuildUpResult;
        const finalDataProvider: ScoreBuildUpChartItem[] = [];
        dataProvider.forEach((element) => {
            if (element.weight > 0) {
                const item: ScoreBuildUpChartItem = new ScoreBuildUpChartItem();
                item.datapointID = element.id;
                item.datapointName = element.name;
                item.targetDeviation = element.targetDeviation;
                item.weight = element.weight;
                item.color = sbc.getColor(item.targetDeviation);
                finalDataProvider.push(item);
            }
        });

        data.items = finalDataProvider;
        return sbc.get(data);
    }
}
