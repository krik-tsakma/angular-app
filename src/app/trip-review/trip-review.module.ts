
// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler  } from '@ngx-translate/core';

// COMPONENTS
import { DriversVehiclesSearchComponent } from './drivers-vehicles-search/drivers-vehicles-search.component';
import { TripsListComponent } from './trips-list/trips-list.component';
import { TripDetailsComponent } from './trip-details/trip-details.component';

// MODULES
import { TripReviewRoutingModule } from './trip-review-routing.module';
import { QuickFiltersSharedModule } from '../common/quick-filters/quick-filters-shared.module';
import { PeriodSelectorModule } from '../common/period-selector/period-selector.module';
import { ScrollToTopModule } from '../common/scroll-to-top/scroll-to-top.module';
import { MapsModule } from '../google-maps/maps.module';
import { SharedModule } from '../shared/shared.module';
import { TripReassignModule } from './reassign/trip-reassign.module';

// SERVICES
import { TripReviewService } from './trip-review.service';
import { TripReviewGuard } from './trip-review-guard';
import { TripListGuard } from './trip-list-guard';
import { TranslateStateService } from './../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../multiTranslateHttpLoader';


export function myTripsTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, [ './assets/i18n/trip-review/' ], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        HttpClientJsonpModule,
        TripReviewRoutingModule,
        QuickFiltersSharedModule,
        PeriodSelectorModule,
        ScrollToTopModule,
        MapsModule,
        TripReassignModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTripsTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        }),
        TripReassignModule
    ],
    declarations: [
        DriversVehiclesSearchComponent,
        TripsListComponent,
        TripDetailsComponent
    ],
    providers: [
        TripReviewService,
        TripReviewGuard,
        TripListGuard
    ]
})
export class TripReviewModule {}
