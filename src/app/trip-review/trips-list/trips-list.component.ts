// FRAMEWORK
import {
    Component, OnInit, OnDestroy,
    ViewChild, ViewEncapsulation, ChangeDetectorRef
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// RXJS
import { Subscription } from 'rxjs';

// COMPONENTS
import { TripReviewBaseComponent } from '../trip-review-base.component';
import { PeriodSelectorComponent } from '../../common/period-selector/period-selector.component';

// SERVICES
import { ResizeService } from '../../shared/resize.service';
import { TripReviewService } from '../trip-review.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { MaximizeService } from '../../shared/maximize/maximize.service';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { Config } from '../../config/config';

// TYPES
import { Trip, TripsRequest, TripDisplaySettings } from '../trip-review-types';
import { QuickFiltersEntities, QuickFiltersChangeItem } from '../../common/quick-filters/quick-filters-types';
import { Pager } from '../../common/pager';
import { Score } from '../../common/score';
import { PeriodTypes, PeriodTypeOption } from '../../common/period-selector/period-selector-types';
import { StoreItems } from '../../shared/store-manager/store-items';
import { QuickFilterColumn } from '../../common/quick-filters/columns/quick-filter-column.types';
import {
    TableColumnSetting,
    TableActionOptions,
    TableActionItem,
    TableColumnSortOptions,
    TableColumnSortDefaults
} from '../../shared/data-table/data-table.types';

// MOMENT
import moment from 'moment';
import { DateFormatterDisplayOptions } from '../../shared/pipes/dateFormat.pipe';

@Component({
    selector: 'trips-list',
    templateUrl: 'trips-list.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['trips-list.less'],
})

export class TripsListComponent extends TripReviewBaseComponent implements OnInit, OnDestroy {
    @ViewChild(PeriodSelectorComponent, { static: true })
        public periodSelectorComponent: PeriodSelectorComponent;
    public dateDisplayOptions: any = DateFormatterDisplayOptions;

    public periodSelectorOptions: PeriodTypes[] = [
        PeriodTypes.ThisMonth,
        PeriodTypes.LastMonth,
        PeriodTypes.LastWeek,
        PeriodTypes.ThisWeek,
        PeriodTypes.Today,
        PeriodTypes.Custom
    ];

    public quickFilterEntities = QuickFiltersEntities;
    public tableSettings: QuickFilterColumn[] = [];

    public displaySettings: TripDisplaySettings;
    public previousDay: moment.Moment;
    public nextDay: moment.Moment;
    public message: string;
    public loading: boolean = true;
    public showBackBtn: boolean;
    public assetLabel: string;
    public trips: Trip[];
    public disableNextDate: boolean;
    public pager: Pager = new Pager();
    public request: TripsRequest = {} as TripsRequest;
    private serviceSubscriber: Subscription;

    constructor(
        protected router: Router,
        protected activatedRoute: ActivatedRoute,
        protected resizeService: ResizeService,
        protected userOptions: UserOptionsService,
        protected service: TripReviewService,
        protected configService: Config,
        private maximizeService: MaximizeService,
        private storeManager: StoreManagerService,
        private unsubscribe: UnsubscribeService,
        private cdr: ChangeDetectorRef) {
            super(router, activatedRoute, resizeService, userOptions, service, configService);

            const user = userOptions.getUser();
            this.showBackBtn = !user.isDriver || user.isManager;

            this.maximizeService.resizer.subscribe(() => {
                resizeService.trigger();
            });
    }


    // =========================
    // ANGULAR LIFECYCLE
    // =========================

    // On initialization
    public ngOnInit(): void {
        super.ngOnInit();
        const savedData = this.storeManager.getOption(StoreItems.quickFiltersTrips) as TripsRequest;
        if (savedData) {
            this.request = savedData;
            if (savedData.periodType === PeriodTypes.Custom) {
                this.periodSelectorComponent.setCustomDates(
                    moment(savedData.periodSince),
                    moment(savedData.periodTill)
                );
            }
        } else {
            this.request.periodType = PeriodTypes.ThisWeek;
        }

        // THIS SHOULD ALWAYS BE AFTER WE FETCH CASHED DATA
        this.request.assetID = super.getRouteParamers().assetID;
        this.request.assetType = super.getAssetType();

        // Option that disables next date button because next date > today
        this.disableNextDate = true;
        this.previousDay = moment().subtract(1, 'days');
        this.nextDay = moment().add(1, 'days');
    }


    // Clean subscribers to avoid memory leak
    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }


    // =========================
    // EVENT HANDLERS
    // =========================

    // Click on previous date button
    public onPreviousDate() {
        // Make the selection and fetch datas
        this.periodSelectorComponent.setCustomDates(
            this.previousDay.clone().startOf('day'),
            this.previousDay.clone().endOf('day')
        );

        // Update Moment previous date - next date values
        const prevDate = this.previousDay.clone().subtract(1, 'days');
        const nextDate = this.nextDay.clone().subtract(1, 'days');
        this.previousDay = prevDate;
        this.nextDay = nextDate;

        // Enables or disables next button if next date > today
        this.disableNextDate = moment().isSameOrBefore(nextDate);
    }

    // Click on next date button
    public onNextDate() {
        // Make the selection and fetch data
        this.periodSelectorComponent.setCustomDates(
            this.nextDay.clone().startOf('day'),
            this.nextDay.clone().endOf('day'))
        ;

        // Update Moment previous date - next date values
        const prevDate = this.previousDay.clone().add(1, 'days');
        const nextDate = this.nextDay.clone().add(1, 'days');
        this.previousDay = prevDate;
        this.nextDay = nextDate;

        // Enables or disables next button if next date > today
        this.disableNextDate = moment().isSameOrBefore(nextDate);
    }


    // =========================
    // TABLE CALLBACKS
    // =========================

    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }

    public onSort(column: TableColumnSetting) {
        if (column.sorting) {
            const sortCol = new TableColumnSortOptions(column.sorting.order, column.sorting.selected, column.sorting.name);
            if (sortCol) {
                this.request.sorting = sortCol.stringFormat(column.primaryKey);
                this.get(true);
            }
        }
    }

    public onTripDetails(item: TableActionItem) {
        if (!item) {
            return;
        }

        // Choose trip for extra details
        switch (item.action) {
            case TableActionOptions.details:
                this.navigateTo(item.record.startTimestamp);
                break;
            default:
                break;
        }
    }

    // =========================
    // QUICK FILTERS CALLBACKS
    // =========================

    // When a user makes any type of change in the quick filters toolbar, reload.
    // firstTimeLoad variable is declaired on top
    public onChangeQuickFilterOption(qfcc: QuickFiltersChangeItem): void {
        this.request.quickFilters = qfcc.request;
        if (!qfcc.columns || qfcc.columns.length === 0) {
            return;
        }

        if (this.request.sorting === undefined) {
            this.request.sorting = TableColumnSortDefaults.getFirstAvailableSortColumn(qfcc.columns);
        }

        qfcc.columns.push({
            primaryKey: ' ',
            header: ' ',
            actions: [TableActionOptions.details]
        } as QuickFilterColumn);

        this.tableSettings = qfcc.columns;
        this.cdr.detectChanges();

        this.get(true);
    }


    // =========================
    // PERIOD SELECTOR CALLBACKS
    // =========================

    // On period select
    public onPeriodSelect(pto: PeriodTypeOption): void {
        this.request.periodType = pto.id;
        this.request.periodSince = pto.datetimeSince.format('YYYY-MM-DD HH:mm');
        this.request.periodTill = pto.datetimeTill.format('YYYY-MM-DD HH:mm');
        this.get(true);
    }

    // =========================
    // HELPERS
    // =========================

    public isTheSameDay(timestamp1, timestamp2) {
        return moment(timestamp1).isSame(moment(timestamp2), 'day');
    }

    public getScore(scoreID: number, trip: Trip): Score {
        if (!trip || !trip.scores || trip.scores.length === 0) {
            return null;
        }
        const score = trip.scores.find((x) => x.id === scoreID);
        if (score) {
            return score;
        }
        return null;
    }

    // =========================
    // DATA
    // =========================

    // Fetch trips data
    private get(resetPageNumber: boolean) {
        // if no column is selected for display then cancel request
        if (!this.request.quickFilters
            || ((!this.request.quickFilters.propertyIDs || this.request.quickFilters.propertyIDs.length === 0 )
            && (!this.request.quickFilters.scoreIDs || this.request.quickFilters.scoreIDs.length === 0)
            && !this.request.quickFilters.id)) {
            return;
        }

        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.trips = new Array<Trip>();
        }

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        if (!this.request.periodSince) {
            this.request.periodSince = this.periodSelectorComponent.selectedOption.datetimeSince.format('YYYY-MM-DD HH:mm');
            this.request.periodTill = this.periodSelectorComponent.selectedOption.datetimeTill.format('YYYY-MM-DD HH:mm');
        }

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.serviceSubscriber);

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.quickFiltersTrips, this.request);

        // then start the new request
        this.serviceSubscriber = this.service
            .getTrips(this.request)
            .subscribe(
                (response) => {
                    this.loading = false;
                    if (response) {
                        this.assetLabel = response.assetLabel;
                    }
                    // Get the actual data
                    const res = response.results === null
                        ? response.resultsFiltered
                        : response.results;

                    res.forEach((r) => {
                        if (r['scores']) {
                            r['scores'].forEach((element) => {
                                r['s' + element.id] = element;
                            });
                        }
                    });

                    this.trips = this.trips.concat(res);
                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;

                    this.displaySettings = response.displaySettings;

                    if (!this.trips || this.trips.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }
}
