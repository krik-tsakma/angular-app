﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../auth/authHttp';
import { Config } from '../config/config';
import { UserOptionsService } from '../shared/user-options/user-options.service';

// TYPES
import { TripsRequest, TripsResults, } from './trip-review-types';
import { TripDetailsResult, TripDetailsRequest, TripBaseRequest } from './trip-details/trip-details.types';
import { TraceEvent, Trace, } from '../common/entities/entity-types';

@Injectable()
export class TripReviewService {
    private baseTripListURL: string;
    private baseTripDetailsURL: string;
    
    constructor(
        private authHttp: AuthHttp,
        private configService: Config,
        private userOptions: UserOptionsService) {
            this.baseTripListURL = this.configService.get('apiUrl') + '/api/Trips';
            this.baseTripDetailsURL = this.configService.get('apiUrl') + '/api/TripDetails';
        }

    
    // Get Trips for specific driver/vehicle
    public getTrips(request: TripsRequest): Observable<TripsResults> {
        const qfr = request.quickFilters;
        const params = new HttpParams({
            fromObject: {
                AssetID : request.assetID.toString(),
                PageNumber: request.pageNumber.toString(),
                Since: request.periodSince,
                Till: request.periodTill,
                Type: request.assetType.toString(),
                Sorting: request.sorting,
                QuickFilterID: qfr.id ? qfr.id.toString() : '',
                PropertyIDs: qfr.propertyIDs ? qfr.propertyIDs.join(',') : '',
                ScoreIDs: qfr.scoreIDs ? qfr.scoreIDs.join(',') : ''
            }
        });  
        
        return this.authHttp
            .get(this.baseTripListURL, { params });
    }

    // Get specific trip for reviewing
    public getTripDetails(req: TripDetailsRequest): Observable<TripDetailsResult> {

        const params = new HttpParams({
            fromObject: {
                AssetID: req.id.toString(),
                Type: req.type.toString(),
                TripStart: req.tripStart,
                Culture: this.userOptions.getCulture()
            }
        });
        
        return this.authHttp
            .get(this.baseTripDetailsURL, { params });
    }

    public getTripTraces(req: TripDetailsRequest): Observable<Trace[]> {

        const params = new HttpParams({
            fromObject: {
                AssetID: req.id.toString(),
                Type: req.type.toString(),
                TripStart: req.tripStart,
            }
        });
        
        return this.authHttp
            .get(this.baseTripDetailsURL + '/GetTripTraces', { params });
    }

    public getTripEvents(req: TripDetailsRequest): Observable<TraceEvent[]> {

        const params = new HttpParams({
            fromObject: {
                AssetID: req.id.toString(),
                Type: req.type.toString(),
                TripStart: req.tripStart,
            }
        });
        
        return this.authHttp
            .get(this.baseTripDetailsURL + '/GetTripEvents', { params });
    }
}
