﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../auth/authGuard';
import { TripReviewGuard } from './trip-review-guard';
import { TripListGuard } from './trip-list-guard';

import { DriversVehiclesSearchComponent } from './drivers-vehicles-search/drivers-vehicles-search.component';
import { TripsListComponent } from './trips-list/trips-list.component';
import { TripDetailsComponent } from './trip-details/trip-details.component';

const TripReviewRoutes: Routes = [
    {
        path: '',
        component: DriversVehiclesSearchComponent,
        canActivate: [AuthGuard, TripReviewGuard]
    },
    {
        path: ':assetType/:assetID',
        component: TripsListComponent,
        canActivate: [AuthGuard, TripListGuard]
    },
    {
        path: ':assetType/:assetID/:timestamp',
        component: TripDetailsComponent,
        canActivate: [AuthGuard, TripListGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(TripReviewRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class TripReviewRoutingModule { }
