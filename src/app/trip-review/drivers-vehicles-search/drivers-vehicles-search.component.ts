// FRAMEWORK
import {
    Component, ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, map } from 'rxjs/operators';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';
import { SearchMechanismService } from '../../shared/search-mechanism/search-mechanism.service';
import { Config } from '../../config/config';

// TYPES
import { StoreItems } from '../../shared/store-manager/store-items';
import { SearchType } from '../../shared/search-mechanism/search-mechanism-types';
import { KeyName } from '../../common/key-name';

@Component({
    selector: 'search-driver-vehicle',
    templateUrl: './drivers-vehicles-search.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./drivers-vehicles-search.less']
})

export class DriversVehiclesSearchComponent {
    public chosenOption?: SearchType;
    public startRedirecting: boolean;
    public searchOptions = SearchType;
    public loading: boolean;
    public isManager: boolean;    
    public termControl = new FormControl();
    public items: Observable<string[]>;
 
    constructor(
        private router: Router,
        private service: SearchMechanismService,
        private storeManager: StoreManagerService,
        private userOptions: UserOptionsService,
        private configService: Config) {
            const u = userOptions.getUser();
            this.isManager = u.isManager;

            const storedOption = this.storeManager.getOption(StoreItems.searchTypeTrips);
            if (storedOption) {
                this.chosenOption = storedOption as SearchType;
            } else {
                this.chosenOption = SearchType.Vehicle;
            }

            this.items = this.termControl.valueChanges
                .pipe(
                // Debounce 500 waits for 500ms until user stops pushing 
                    debounceTime(500),
                    // use this to handle scenarios like hitting backspace and retyping the same letter
                    distinctUntilChanged(),
                    // switchMap operator automatically unsubscribes from previous subscriptions,
                    // as soon as the outer Observable emits new values
                    // so we will always search with last term
                    switchMap((term) => {
                        if (!term || term.length < 2) {
                            return of([]);
                        }
                        this.loading = true;
                        const res = this.service
                            .search(this.chosenOption, term)
                            .pipe(
                                map((data) => {
                                    this.loading = false;
                                    return data;
                                })
                            );
                        return res;
                    })
                );
        }

    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.termControl.setValue(null);
    }

    // Search radio button option change -> me / driver / vehicle
    public clickOption() {
        if (SearchType[this.chosenOption] === undefined) {
            this.selected(null);
        } else {
            // the save the option for future use
            this.storeManager.saveOption(StoreItems.searchTypeTrips, this.chosenOption);

            const currentVal = this.termControl.value ? this.termControl.value.toString() : null;
             // clear selection
            this.termControl.setValue(null);
            if (currentVal) {
                // then reset to overcome the distinctUntilChanged option
                this.termControl.setValue(currentVal);
            }
        }
    }

    // Method that selects specific trip and redirects user
    // at trip-review page
    public selected(item: KeyName): void {
        let assetType: string;
        let assetID: number;
        switch (SearchType[this.chosenOption]) {
            case undefined:
            case null:
                const u = this.userOptions.getUser();
                assetID = u.driverId;
                assetType = SearchType[SearchType.Driver];
                break;
            default:
                assetID = item.id;
                assetType = SearchType[this.chosenOption];
                break;
        }

        this.startRedirecting = true;
        this.router.navigate(['/trip-review', assetType, assetID], { skipLocationChange: !this.configService.get('router') });
    }    
}
