import { PagerResults, PagerRequest } from '../common/pager';
import { Score } from '../common/score';
import { PeriodTypes } from '../common/period-selector/period-selector-types';
import { QuickFilter } from '../common/quick-filters/quick-filters-types';
import { AssetTypes } from '../common/entities/entity-types';
import { EnergyUnitOptions } from '../common/energy-types/energy-types';

// =========================
// COMMON
// =========================

export interface TripReviewRouteParams {
    assetType: string;
    assetID: number;
    timestamp?: string;
}

export interface Trip {
    distance: number;
    driverID: number;
    driverLabel: string;
    durationSecs: number;
    isClosed: boolean;
    isHidden: boolean;
    maxSpeed: number;
    avgSpeed: number;
    startAddress: string;
    startOdometer: any;
    startTimestamp: string;
    stopAddress: string;
    stopOdometer: any;
    stopTimestamp: string;
    tripType: string;
    vehicleID: number;
    vehicleLabel: string;
    energyScore: Score;
    peerGroupID?: number;
    peerGroupName: string;
    scores?: Score[];
    energyUnit?: EnergyUnitOptions;

    driveTimeNightPerc: number;
    idleTimePerc?: number;
    idleTimeAfterGracePeriodPerc?: number;
    inefficientAccelerationNumNormalized?: number;
    hardAccelerationNumNormalized?: number;
    excessiveAccelerationNumNormalized?: number;
    inefficientBrakingNumNormalized?: number;
    hardBrakingNumNormalized?: number;
    excessiveBrakingNumNormalized?: number;
    fastLeftCorneringNumNormalized?: number;
    fastRightCorneringNumNormalized?: number;
    durationVehicleSpeedInefficientPerc?: number;
    durationVehicleSpeedHardPerc?: number;
    durationEngineSpeedInefficientPerc?: number;
    driveTimeBrakePedalPerc?: number;
    driveTimeAccPedalPerc?: number;
    driveTimeRollingPerc?: number;
    durationExcessiveAccelerationPerc?: number;
    durationGreenAccelerationPerc?: number;
    durationYellowAccelerationPerc?: number;
    durationRedAccelerationPerc?: number;
    durationGreenBrakingPerc?: number;
    durationYellowBrakingPerc?: number;
    durationRedBrakingPerc?: number;
    driverAidAlertsEngineSpeedNormalized?: number;
    driverAidAlertsPedalAccelerationNormalized?: number;
    driverAidAlertsKineticAccelerationNormalized?: number;
    driverAidAlertsKineticBrakingNormalized?: number;
    driverAidAlertsSpeedNormalized?: number;
    driverAidAlertsIdlingNormalized?: number;
    degenBrakeNumberNormalized?: number;
    dechBrakeNumberNormalized?: number;
    eVEnergyRegenNormalized?: number;
}

export interface TripDisplaySettings {
  showDurationInHiddenTrips: boolean;
  showMileageInHiddenTrips: boolean;
  showOdometerStartstopInHiddenTrips;
}


// =========================
// TRIP LIST
// =========================
export interface TripsRequest extends PagerRequest {
    assetID: number;
    assetType: AssetTypes;
    quickFilters: QuickFilter;
    periodType: PeriodTypes;
    periodSince: string;
    periodTill: string;
}

export interface TripsResults extends PagerResults {
    assetLabel: string;
    results: Trip[];
    resultsFiltered: Trip[];
    displaySettings: TripDisplaySettings;
 }
