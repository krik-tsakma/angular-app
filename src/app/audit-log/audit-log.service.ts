﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams, HttpResponse } from '@angular/common/http';

// RxJS
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../auth/authHttp';
import { Config } from '../config/config';

// TYPES
import {
    AuditLogRequest, AuditLogsResult, AuditLogDetailResult
} from './audit-log-types';



@Injectable()
export class AuditLogService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
        this.baseURL = this.config.get('apiUrl') + '/api/AuditLog';
    }

    public showAllColumns(): Observable<boolean> {      
        return this.authHttp
            .get(this.baseURL + '/ShowAllColumns');
    }
    // Get all notification policies
    public get(request: AuditLogRequest): Observable<AuditLogsResult> {        
        const params = new HttpParams({
            fromObject: {
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',  
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Since: request.periodSince,
                Till: request.periodTill,
                Sorting: request.sorting,
                Action: request.action !== null && request.action !== undefined ? request.action.toString() : '',
                EntityType: request.entityType.toString()
            }
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }



    // Get details for Dialog
    public getByIdDetails(id: number): Observable<HttpResponse<AuditLogDetailResult>> {
        return this.authHttp
            .get(this.baseURL + '/' + id.toString(), { observe: 'response' });
    }
   
} 
