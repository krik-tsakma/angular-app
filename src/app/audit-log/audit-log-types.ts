﻿import { SearchTermRequest } from '../common/pager';
import { PagerResults } from '../common/pager';
import { PeriodTypes } from '../common/period-selector/period-selector-types';

export abstract class AuditLogActions {
    public static readonly CREATE = 0;
    public static readonly UPDATE = 1;
    public static readonly DELETE = 2;
}


export class AuditLogActionsUser {
    public static readonly PASSWORD_CHANGE = 0;
    public static readonly PASSWORD_RESET = 1;
}
export class AuditLogActionsTripReassign {
    public static readonly REASSIGN = 1;
}
export class AuditLogActionsDriverTakeOver {
    public static readonly TAKE_OVER = 0;
}

export class AuditLogActionsEventProperty {
    public static readonly UPDATE = 1;
}

export enum AuditLogEntityTypes {
    MY_ACCOUNT = -1,
    COMPANY = 0,
    VEHICLE = 1,
    DRIVER = 2,
    LOCATION = 3,
    ROUTE = 4,
    USER = 5,
    ASSET_LINK = 6,
    VEHICLE_GROUP = 7,
    DRIVER_GROUP = 8,
    LOCATION_GROUP = 9,
    ROUTE_GROUP = 10,
    VEHICLE_TYPE = 11,
    USER_GROUP = 12,
    USER_ROLE = 13,
    PASSWORD_POLICY = 14,
    TRIP_REASSIGNMENT = 15,
    DRIVER_TAKE_OVER = 16,
    EVENT_STATUSES = 17,
    EVENT_PRIORITIES = 18,
    EVENT_CLASSES = 19,
    CHARGE_POINT = 20,
    CHARGE_POINT_GROUP = 21
}

export interface AuditLogItem { 
    id: number;
    timestamp: string; 
    userLabel: string;
    action: AuditLogActions;
    entityType: AuditLogEntityTypes;
    entityName: string;
}

export interface AuditLogRequest extends SearchTermRequest {    
    periodType: PeriodTypes;
    periodSince: string;
    periodTill: string;
    action?: number;
    entityType: AuditLogEntityTypes;
}

export interface AuditLogsResult extends PagerResults {
    results: AuditLogItem[];
}

export interface AuditLogChangedValues {
    propertyName: string;
    value: string;    
}

export interface AuditLogDetailsChangedValues {
    label: string;
    newValue?: string;
    oldValue?: string;
}

export interface AuditLogDetailResult {    
    id: number;
    timestamp: string;
    userLabel: string;   
    action: AuditLogActions;
    entityType: AuditLogEntityTypes;
    before: AuditLogChangedValues[];
    after: AuditLogChangedValues[];
}
