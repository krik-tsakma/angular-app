// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../auth/authGuard';

// COMPONENTS
import { AuditLogComponent } from './audit-log.component';

const AuditLogRoutes: Routes = [
    {
        path: '',
        component: AuditLogComponent,
        canActivate: [AuthGuard]
    }    
];

@NgModule({
    imports: [
        RouterModule.forChild(AuditLogRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AuditLogRoutingModule { }
