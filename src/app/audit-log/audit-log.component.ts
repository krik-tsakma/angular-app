// FRAMEWORK
import {
    Component, ViewChild, OnInit,
    OnDestroy, ElementRef, Renderer
} from '@angular/core';
import { Router } from '@angular/router';

// RXJS
import { Subscription, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

// COMPONENTS
import { PeriodSelectorComponent } from '../common/period-selector/period-selector.component';

// SERVICES
import { AuthService } from './../auth/authService';
import { CustomTranslateService } from '../shared/custom-translate.service';
import { UnsubscribeService } from '../shared/unsubscribe.service';
import { SnackBarService } from '../shared/snackbar.service';
import { PreviousRouteService } from '../core/previous-route/previous-route.service';
import { AuditLogService } from './audit-log.service';
import { DetailsDialogService } from './details-dialog/details-dialog.service';
import { Config } from '../config/config';

// TYPES
import { AdminPropertyOptions, ModuleOptions, TripReviewPropertyOptions } from '../auth/acl.types';
import { Pager } from '../common/pager';
import { PeriodTypes, PeriodTypeOption } from '../common/period-selector/period-selector-types';
import { KeyName } from '../common/key-name';
import {
    TableColumnSetting, TableColumnSortOptions,
    TableColumnSortOrderOptions, TableActionOptions,
    TableActionItem, TableCellFormatOptions
} from '../shared/data-table/data-table.types';
import {
    AuditLogActions, AuditLogEntityTypes,
    AuditLogItem, AuditLogRequest, AuditLogsResult, AuditLogActionsDriverTakeOver, AuditLogActionsUser, AuditLogActionsTripReassign
} from './audit-log-types';

@Component({
    selector: 'audit-log',
    templateUrl: 'audit-log.html'
})

export class AuditLogComponent implements OnInit, OnDestroy {

    @ViewChild(PeriodSelectorComponent, { static: true })
    public periodSelectorComponent: PeriodSelectorComponent;
    @ViewChild('term', { static: true })
    public fileInput: ElementRef;

    public loading: boolean;
    public loadingSearch: boolean;
    public message: string;
    public inputSelected: any;
    public search: Subject<string> = new Subject();
    public request: AuditLogRequest = {} as AuditLogRequest;
    public pager: Pager = new Pager();
    public data: AuditLogItem[] = [] as AuditLogItem[];
    public entityTypeOptions: KeyName[] = [];
    public actionOptions: KeyName[] = [];
    public periodSelectorOptions: PeriodTypes[] = [
        PeriodTypes.LastWeek,
        PeriodTypes.ThisWeek,
        PeriodTypes.Today,
        PeriodTypes.Custom,
        PeriodTypes.LastMonth,
        PeriodTypes.ThisMonth,
    ];
    public tableSettings: TableColumnSetting[];
    public translatedKeys: any;

    private subscriber: Subscription;
    private searchSubscriber: Subscription;


    constructor(public service: AuditLogService,
                public unsubscribe: UnsubscribeService,
                public translate: CustomTranslateService,
                public authService: AuthService,
                private configService: Config,
                private router: Router,
                private previousRouteService: PreviousRouteService,
                private detailsDialogService: DetailsDialogService,
                private snackBarService: SnackBarService,
                private renderer: Renderer) {

        this.tableSettings = this.getTableSettings(true);
    }

    // ----------------
    // LIFECYCLE
    // ----------------

    public ngOnInit() {
        this.translate.get([
                    'assets.vehicles',
                    'assets.groups.vehicle',
                    'assets.vehicle_types',
                    'assets.drivers',
                    'assets.groups.driver',
                    'assets.groups.location',
                    'assets.users',
                    'assets.groups.user',
                    'audit_log.action_options.password_reset',
                    'audit_log.action_options.password_change',
                    'audit_log.user_roles',
                    'audit_log.trip_reassignment',
                    'audit_log.my_account',
                    'audit_log.password_policy',
                    'audit_log.admin.categories.company',
                    'audit_log.admin.categories.devices',
                    'audit_log.admin.categories.charge_points',
                    'audit_log.admin.categories.charge_points.groups',
                    'audit_log.action_options.take_over',
                    'audit_log.action_options.trip_reassign',
                    'audit_log.action_options.update',
                    'event_properties.priorities',
                    'event_properties.classes',
                    'event_properties.statuses',
                    'form.actions.all',
                    'sidebar.driver_take_over',
                ]).subscribe((res) => {
                    this.entityTypeOptions = this.getEntityTypeOptions(res);
                    this.service
                        .showAllColumns()
                        .subscribe((response: boolean) => {
                            this.tableSettings = this.getTableSettings(response);
                        });

                    const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
                    this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
                    this.request.entityType = this.entityTypeOptions[0].id;
                    this.request.periodType = PeriodTypes.ThisWeek;
                    this.setActionsForEntity(this.request.entityType);

                    this.searchSubscriber = this.search
                        .pipe(
                            debounceTime(700),
                            distinctUntilChanged(),
                        )
                        .subscribe((term) => {
                            this.request.searchTerm = term;
                            this.get(true);
                        });
                });
    }


    public ngOnDestroy() {
        this.unsubscribe.removeSubscription([
            this.subscriber,
            this.searchSubscriber
        ]);
    }



    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }



    // ----------------
    // FILTER BAR
    // ----------------

    // Seach
    public onSearch(term: any) {
        this.loadingSearch = true;
        if (term && term.length > 1) {
            this.search.next(term);
        } else {
            this.loadingSearch = false;
        }
    }


    // on audit log entity type selection
    public onEntitySelect(item: number) {
        this.request.entityType = item;
        this.request.action = null;
        this.setActionsForEntity(item);
        this.get(true);
    }


    // on audit log action selection
    public onActionSelect(action: number) {
        this.request.action = action;
        this.get(true);
    }



    // ----------------
    // TABLE
    // ----------------

    // fetch data
    public loadMore() {
        if (this.pager.pageNumber < this.pager.totalPages && this.loading === false) {
            this.pager.addPage();
            this.get(false);
        }
    }



    // When the user selected a column to sort (from list view)
    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }



    // On period select
    public onPeriodSelect(pto: PeriodTypeOption): void {
        this.request.periodType = pto.id;
        this.request.periodSince = pto.datetimeSince.format('YYYY-MM-DD HH:mm');
        this.request.periodTill = pto.datetimeTill.format('YYYY-MM-DD HH:mm');
        this.get(true);
    }


    // View details in Dialog
    public onAuditLogDetails(item: TableActionItem): void {
        if (!item) {
            return;
        }

        const record = item.record as AuditLogItem;
        if (item.action !== TableActionOptions.details) {
            return;
        }

        this.service
            .getByIdDetails(record.id)
            .subscribe((res) => {
                if (res.status === 200 && res.body) {
                    this.detailsDialogService.details(res.body);
                } else {
                    this.snackBarService.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                if (err.status === 404) {
                    this.snackBarService.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBarService.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    public clearInput(e?: Event) {
        // do not bubble event otherwise focus is on the textbox
        if (e) {
            e.stopPropagation();
        }

        // clear selection
        this.inputSelected = null;
        this.search.next(null);

        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.fileInput.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }


    // =========================
    // PRIVATE
    // =========================

    // Fetch ev charging points
    private get(resetPageNumber: boolean) {
        if (this.loading) {
            return;
        }

        this.loading = true;

        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.pager.pageSize = 20;
            this.data = new Array<AuditLogItem>();
        }

        this.request.pageSize = this.pager.pageSize;
        this.request.pageNumber = this.pager.pageNumber;

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.subscriber);

        // then start the new request
        this.subscriber = this.service
            .get(this.request)
            .subscribe((response: AuditLogsResult) => {
                this.loading = false;
                this.loadingSearch = false;
                if (resetPageNumber === true) {
                    this.data = response && response.results
                        ? response.results
                        : [];
                } else {
                    this.data = this.data.concat(response.results);
                }

                // Get the paging info
                this.pager.pageNumber = response.pageNumber;
                this.pager.pageSize = response.pageSize;
                this.pager.totalPages = response.totalPages;
                this.pager.totalRecords = response.totalRecords;
            },
            (err) => {
                this.loading = false;
                this.loadingSearch = false;
                this.data = [];
                this.message = 'data.error';
            });
    }

    private getTableSettings(showAll: boolean): TableColumnSetting[] {
        const cols: TableColumnSetting[] = [
            {
                primaryKey: 'timestamp',
                header: 'audit_log.header.date_time',
                format: TableCellFormatOptions.dateTime,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.desc, true, null)
            },
            {
                primaryKey: 'action',
                header: 'audit_log.header.action',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: AuditLogItem) => {
                    const action = this.actionOptions.find((x) => x.id === Number(item.action));
                    return action !== undefined
                        ? action.name
                        : 'N/A';
                },
            },
            {
                primaryKey: 'entityName',
                header: 'audit_log.header.entity_name',
            }
        ];

        if (showAll === true) {
            cols.push({
                primaryKey: 'userLabel',
                header: 'audit_log.header.user_label',
            },
            {
                primaryKey: 'takenOverUserLabel',
                header: 'audit_log.header.taken_over_user_label',
            });
        }

        cols.push({
            primaryKey: ' ',
            header: ' ',
            actions: [TableActionOptions.details],
            actionsVisibilityFunction: (action: TableActionOptions, record: AuditLogItem): boolean => {
                if (!record ||
                    record.action === AuditLogActions.DELETE ||
                    this.request.entityType === AuditLogEntityTypes.MY_ACCOUNT ||
                    this.request.entityType === AuditLogEntityTypes.DRIVER_TAKE_OVER) {
                    return false;
                }

                return true;
            }
        });

        return cols;
    }

    // =========================
    // ENTITY TYPE HELPERS
    // =========================

    private setActionsForEntity(entityType: number) {
        this.translate.get([
            'form.actions.all',
            'audit_log.action_options.password_reset',
            'audit_log.action_options.password_change',
            'audit_log.action_options.take_over',
            'audit_log.action_options.trip_reassign',
            'audit_log.action_options.update',
            'audit_log.action_options.create',
            'audit_log.action_options.update',
            'audit_log.action_options.delete',
        ]).subscribe((transRes) => {
            switch (entityType) {
                case AuditLogEntityTypes.MY_ACCOUNT:
                    this.actionOptions = this.getMeActionTypeOptions(transRes);
                    break;
                case AuditLogEntityTypes.DRIVER_TAKE_OVER:
                    this.actionOptions = this.getDriverTakeOverActionTypeOptions(transRes);
                    break;
                case AuditLogEntityTypes.TRIP_REASSIGNMENT:
                    this.actionOptions = this.getTripReassignActionTypeOptions(transRes);
                    break;
                case AuditLogEntityTypes.EVENT_PRIORITIES:
                case AuditLogEntityTypes.EVENT_CLASSES:
                case AuditLogEntityTypes.EVENT_STATUSES:
                    this.actionOptions = this.getEventPropertyActionTypeOptions(transRes);
                    break;
                default:
                    this.actionOptions = this.getCommonActionTypeOptions(transRes);
                    break;
            }

            this.actionOptions.unshift({
                id: null,
                name: transRes['form.actions.all'],
                enabled: true
            });
        });
    }

    private getEntityTypeOptions(translates: any): KeyName[] {
        const options =  [{
            id: AuditLogEntityTypes.MY_ACCOUNT,
            name: translates['audit_log.my_account'],
            enabled: true
        }];

        if (this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Company)) {
            options.push({
                id: AuditLogEntityTypes.COMPANY,
                name: translates['audit_log.admin.categories.company'],
                enabled: true
            }, {
                id: AuditLogEntityTypes.PASSWORD_POLICY,
                name: translates['audit_log.password_policy'],
                enabled: true
            }, {
                id: AuditLogEntityTypes.EVENT_PRIORITIES,
                name: translates['event_properties.priorities'],
                enabled: true
            }, {
                id: AuditLogEntityTypes.EVENT_CLASSES,
                name: translates['event_properties.classes'],
                enabled: true
            }, {
                id: AuditLogEntityTypes.EVENT_STATUSES,
                name: translates['event_properties.statuses'],
                enabled: true
            });
        }

        if (this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Vehicles)) {
            options.push({
                    id: AuditLogEntityTypes.VEHICLE,
                    name: translates['assets.vehicles'],
                    enabled: true
                }, {
                    id: AuditLogEntityTypes.VEHICLE_GROUP,
                    name: translates['assets.groups.vehicle'],
                    enabled: true
                }, {
                    id: AuditLogEntityTypes.VEHICLE_TYPE,
                    name: translates['assets.vehicle_types'],
                    enabled: true
                });
        }
        if (this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Drivers)) {
            options.push({
                id: AuditLogEntityTypes.DRIVER,
                name: translates['assets.drivers'],
                enabled: true
            }, {
                id: AuditLogEntityTypes.DRIVER_GROUP,
                name: translates['assets.groups.driver'],
                enabled: true
            });
        }
        if (this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Locations)) {
            options.push({
                id: AuditLogEntityTypes.LOCATION_GROUP,
                name: translates['assets.groups.location'],
                enabled: true
            });

            // {
            //     id: AuditLogEntityTypes.LOCATION,
            //     name: translates['assets.locations'],
            //     enabled: true
            // },
        }


        if (this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Users)) {
            options.push({
                id: AuditLogEntityTypes.USER,
                name: translates['assets.users'],
                enabled: true
            },
            {
                id: AuditLogEntityTypes.USER_GROUP,
                name: translates['assets.groups.user'],
                enabled: true
            },
            {
                id: AuditLogEntityTypes.USER_ROLE,
                name: translates['audit_log.user_roles'],
                enabled: true
            });
        }
        if (this.authService.userHasAccess(ModuleOptions.TripReview, TripReviewPropertyOptions.ReassignDriver)) {
            options.push( {
                id: AuditLogEntityTypes.TRIP_REASSIGNMENT,
                name: translates['audit_log.trip_reassignment'],
                enabled: true
            });
        }

        if (this.authService.userHasAccess(ModuleOptions.DriverTakeOver)) {
            options.push( {
                id: AuditLogEntityTypes.DRIVER_TAKE_OVER,
                name: translates['sidebar.driver_take_over'],
                enabled: true
            });
        }

        if (this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.AssetLinks)) {
            options.push({
                id: AuditLogEntityTypes.ASSET_LINK,
                name: translates['audit_log.admin.categories.devices'],
                enabled: true
            });
        }

        if (this.authService.userHasAccess(ModuleOptions.ChargePointManager)) {
            options.push({
                id: AuditLogEntityTypes.CHARGE_POINT,
                name: translates['audit_log.admin.categories.charge_points'],
                enabled: true
            },
            {
                id: AuditLogEntityTypes.CHARGE_POINT_GROUP,
                name: translates['audit_log.admin.categories.charge_points.groups'],
                enabled: true
            });
        }

        return options;
    }

    private getCommonActionTypeOptions(trans): KeyName[] {
        return [
            {
                id: AuditLogActions.CREATE,
                name: trans['audit_log.action_options.create'],
                enabled: true
            },
            {
                id: AuditLogActions.UPDATE,
                name:  trans['audit_log.action_options.update'],
                enabled: true
            },
            {
                id: AuditLogActions.DELETE,
                name: trans['audit_log.action_options.delete'],
                enabled: true
            }
        ];
    }
    private getMeActionTypeOptions(trans): KeyName[] {
        return [
            {
                id: AuditLogActionsUser.PASSWORD_CHANGE,
                name: trans['audit_log.action_options.password_change'],
                enabled: true
            },
            {
                id: AuditLogActionsUser.PASSWORD_RESET,
                name: trans['audit_log.action_options.password_reset'],
                enabled: true
            }
        ];
    }

    private getDriverTakeOverActionTypeOptions(trans): KeyName[] {
        return [
            {
                id: AuditLogActionsDriverTakeOver.TAKE_OVER,
                name: trans['audit_log.action_options.take_over'],
                enabled: true
            }
        ];
    }

    private getTripReassignActionTypeOptions(trans): KeyName[] {
        return [
            {
                id: AuditLogActionsTripReassign.REASSIGN,
                name: trans['audit_log.action_options.trip_reassign'],
                enabled: true
            }
        ];
    }

    private getEventPropertyActionTypeOptions(trans): KeyName[] {
        return [
            {
                id: AuditLogActions.UPDATE,
                name: trans['audit_log.action_options.update'],
                enabled: true
            }
        ];
    }
}
