// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../shared/shared.module';
import { AuditLogRoutingModule } from './audit-log-routing.module';
import { PeriodSelectorModule } from '../common/period-selector/period-selector.module';
import { AuditLogAclModule } from './details-dialog/rights/audit-log-acl.module';

// COMPONENTS
import { AuditLogComponent } from './audit-log.component';
import { DetailsDialogComponent } from './details-dialog/details-dialog.component';

// SERVICES
import { AuditLogService } from './audit-log.service';
import { DetailsDialogService } from './details-dialog/details-dialog.service';
import { TranslateStateService } from '../core/translateStore.service';

// PIPES
import { AuditLogParameterNamePipe } from './details-dialog/audit-log-parameter-name.pipe';
import { AuditLogParameterValuePipe } from './details-dialog/audit-log-parameter-value.pipe';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/audit-log/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}



@NgModule({
    imports: [
        SharedModule,
        AuditLogRoutingModule,
        PeriodSelectorModule,
        AuditLogAclModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        AuditLogComponent,
        DetailsDialogComponent,
        AuditLogParameterNamePipe,
        AuditLogParameterValuePipe
    ],
    providers: [
        AuditLogService,
        DetailsDialogService
    ],
    entryComponents: [
        DetailsDialogComponent
    ]
})
export class AuditLogModule { }
