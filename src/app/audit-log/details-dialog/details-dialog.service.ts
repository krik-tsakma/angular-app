﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { DetailsDialogComponent } from './details-dialog.component';

// TYPES
import { AuditLogDetailResult } from '../audit-log-types';


@Injectable()
export class DetailsDialogService {

    constructor(private dialog: MatDialog) { }

    public details(params: AuditLogDetailResult): Observable<string> {
        const dialogRef: MatDialogRef<DetailsDialogComponent> = this.dialog.open(DetailsDialogComponent);

        dialogRef.componentInstance.data = params;        

        return dialogRef.afterClosed();
    }
}
