import { CustomTranslateService } from '../../shared/custom-translate.service';
// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';
import { AuditLogEntityTypes } from '../audit-log-types';

@Pipe({
    name: 'myAuditLogParameterName',
    pure: false
})

export class AuditLogParameterNamePipe implements PipeTransform {
    constructor(private translate: CustomTranslateService) {
        // foo
    }

    public transform(val: string,  entityType: AuditLogEntityTypes): string {
        let term = '';
        switch (entityType) {
            case AuditLogEntityTypes.COMPANY:
                term = this.getCompanyParameter(val);
                break;
            case AuditLogEntityTypes.PASSWORD_POLICY:
                term = this.getPasswordPolicyParameter(val);
                break;
            case AuditLogEntityTypes.EVENT_CLASSES:
            case AuditLogEntityTypes.EVENT_STATUSES:
            case AuditLogEntityTypes.EVENT_PRIORITIES:
                term = this.getEventPropertyParameter(val);
                break;
            case AuditLogEntityTypes.VEHICLE:
                term = this.getVehicleParameter(val);
                break;
            case AuditLogEntityTypes.VEHICLE_GROUP:
            case AuditLogEntityTypes.LOCATION_GROUP:
            case AuditLogEntityTypes.CHARGE_POINT_GROUP:
                term = this.getAssetGroupParameter(val);
                break;
            case AuditLogEntityTypes.DRIVER:
                term = this.getDriverParameter(val);
                break;
            case AuditLogEntityTypes.DRIVER_GROUP:
                term = this.getDriverGroupParameter(val);
                break;
            case AuditLogEntityTypes.USER:
                term = this.getUserParameter(val);
                break;
            case AuditLogEntityTypes.USER_ROLE:
                term = this.getUserRoleParameter(val);
                break;
            case AuditLogEntityTypes.USER_GROUP:
                term = this.getUserGroupParameter(val);
                break;
            case AuditLogEntityTypes.VEHICLE_TYPE:
                term = this.getVehicleTypeParameter(val);
                break;
            case AuditLogEntityTypes.ASSET_LINK:
                term = this.getAssetLinkParameter(val);
                break;
            case AuditLogEntityTypes.TRIP_REASSIGNMENT:
                term = this.getTripReassignmentParameter(val);
                break;
            case AuditLogEntityTypes.CHARGE_POINT:
                term = this.getChargePointParameter(val);
                break;
            default:
                term = val;
                break;
        }
        return this.translate.instant(term) as string;
    }

    private getCompanyParameter(parameterName: string): string {
        switch (parameterName) {
            case 'Name':
                return 'company.name';
            case 'Description':
                return 'company.description';
            case 'TimeZoneIndex':
                return 'users.timezone';
            case 'Culture':
                return 'users.language';
            case 'MeasurementSystem':
                return 'measurement.system';
            case 'VehicleLabel':
                return 'labels.vehicle';
            case 'DriverLabel':
                return 'labels.driver';
            case 'AccountCreationCodePrefix':
                return 'company.account_creation_code_prefix';
            case 'AllowToUseAlias':
                return 'company.driver_label.allow_aliases_use';
            default:
                return parameterName;
        }
    }

    private getVehicleParameter(parameterName: string): string {
        switch (parameterName) {
            case 'LicencePlate':
                return 'vehicles.licence_plate';
            case 'OwnID':
                return 'vehicles.ID';
            case 'Description':
                return 'vehicles.description';
            case 'VehicleTypeID':
                return 'vehicles.vehicle_type';
            case 'ChargingID':
                return 'vehicles.chargingID';
            case 'GIDs':
                return 'assets.groups';
            default:
                return parameterName;
        }
    }

    private getDriverParameter(parameterName: string): string {
        switch (parameterName) {
            case 'FirstName':
                return 'users.firstname';
            case 'LastName':
                return 'users.lastname';
            case 'OwnID':
                return 'drivers.ID';
            case 'Description':
                return 'drivers.description';
            case 'Email':
                return 'users.email';
            case 'PhoneNumber':
                return 'users.phone_number';
            case 'TokenType':
                return 'drivers.pin.type';
            case 'TokenID':
                return 'drivers.pin';
            case 'UseAlias':
                return 'drivers.use_alias';
            case 'IsBlocked':
                return 'drivers.account.user.block';
            case 'AllowAccountCreation':
                return 'drivers.account.creation';
            case 'AccountCreationRoleID':
                return 'drivers.account.creation.role';
            case 'AccountRoleID':
                return 'drivers.category.user.role';
            case 'UserID':
                return 'drivers.category.user';
            case 'GIDs':
                return 'assets.groups';
            default:
                return parameterName;
        }
    }

    private getAssetGroupParameter(parameterName: string): string {
        switch (parameterName) {
            case 'Name':
                return 'assets.group.name';
            case 'Description':
                return 'assets.group.description';
            case 'MemberID':
                return 'assets.group.members';
            case 'AuthorizedUserID':
                return 'assets.group.access';
            default:
                return parameterName;
        }
    }

    private getDriverGroupParameter(parameterName: string): string {
        if (parameterName === 'DimensionID') {
            return 'assets.group.dimension_tag';
        } else {
            return this.getAssetGroupParameter(parameterName);
        }
    }

    private getUserParameter(parameterName: string): string {
        switch (parameterName) {
            case 'UserName':
                return 'users.username';
            case 'FirstName':
                return 'users.firstname';
            case 'LastName':
                return 'users.lastname';
            case 'Email':
                return 'users.email';
            case 'PhoneNumber':
                return 'users.phone_number';
            case 'VehicleLabel':
                return 'labels.vehicle';
            case 'DriverLabel':
                return 'labels.driver';
            case 'MeasurementSystem':
                return 'measurement.system';
            case 'TimeZoneIndex':
                return 'users.timezone';
            case 'Culture':
                return 'users.language';
            case 'GIDs':
                return 'assets.groups';
            case 'VehGIDs':
                return 'assets.groups.vehicle';
            case 'HrGIDs':
                return 'assets.groups.driver';
            case 'LocGIDs':
                return 'assets.groups.location';
            case 'CpGIDs':
                return 'assets.groups.charge_point';
            case 'RoleID':
                return 'users.role';
            default:
                return parameterName;
        }
    }

    private getUserRoleParameter(parameterName: string): string {
        switch (parameterName) {
            case 'Name':
                return 'user_roles.name';
            case 'IsDefaultDriver':
                return 'user_roles.is_driver';
            case 'IsDefaultAdmin':
                return 'user_roles.is_admin';
            case 'IsDefaultUser':
                return 'user_roles.is_user';
            case 'VehGIDs':
                return 'assets.groups.vehicle';
            case 'HrGIDs':
                return 'assets.groups.driver';
            case 'LocGIDs':
                return 'assets.groups.location';
            case 'CpGIDs':
                return 'assets.groups.charge_point';
            default:
                return parameterName;
        }
    }

    private getUserGroupParameter(parameterName: string): string {
        switch (parameterName) {
            case 'Name':
                return 'assets.group.name';
            case 'Description':
                return 'assets.group.description';
            case 'MemberID':
                return 'assets.group.members';
            default:
                return parameterName;
        }
    }

    private getVehicleTypeParameter(parameterName: string): string {
        switch (parameterName) {
            case 'Name':
                return 'vehicle_types.name';
            case 'Model':
                return 'vehicle_types.model';
            case 'Type':
                return 'vehicle_types.type';
            case 'Year':
                return 'vehicle_types.year';
            case 'EnergyTypeID':
                return 'vehicle_types.primary_energy_type';
            case 'SecondaryEnergyTypeID':
                return 'vehicle_types.secondary_energy_type';
            case 'EVType':
                return 'vehicle_types.ev_type';
            case 'BatteryNetCapacity':
                return 'vehicle_types.bev.battery_net_capacity';
            case 'BatteryGrossCapacity':
                return 'vehicle_types.bev.battery_gross_capacity';
            case 'MaxMotorPower':
                return 'vehicle_types.bev.max_motor_power';
            case 'BatterySoCClasificationLow':
                return this.translate.instant('vehicle_types.bev.soc_classification') + ' ' + this.translate.instant('vehicle_types.bev.soc_classification_low');
            case 'BatterySoCClasificationHigh':
                return this.translate.instant('vehicle_types.bev.soc_classification') + ' ' + this.translate.instant('vehicle_types.bev.soc_classification_high');
            case 'TypicalEconomy':
                return 'vehicle_types.book_n_go.typical_economy';
            case 'PreBookingChargingMarginSeconds':
                return 'vehicle_types.book_n_go.prebooking_charge_margin';
            default:
                return parameterName;
        }
    }


    private getAssetLinkParameter(parameterName: string): string {
        switch (parameterName) {
            case 'TrackingDeviceID':
                return 'assets.tracking_device';
            case 'VehicleID':
                return 'assets.vehicle';
            case 'HumanResourceID':
                return 'assets.driver';
            default:
                return parameterName;
        }
    }

    private getTripReassignmentParameter(parameterName: string): string {
        switch (parameterName) {
            case 'DriverID':
                return 'assets.driver';
            default:
                return parameterName;
        }
    }

    private getEventPropertyParameter(parameterName: string): string {
        switch (parameterName) {
            case 'Label':
                return 'event_properties.label';
            default:
                return parameterName;
        }
    }

    private getPasswordPolicyParameter(parameterName: string): string {
        switch (parameterName) {
            case 'MinPasswordLength':
                return 'password_policies.min_password_length';
            case 'MinLowerCaseLetters':
                return 'password_policies.min_lowercase_chars';
            case 'MinUpperCaseLetters':
                return 'password_policies.min_uppercase_chars';
            case 'MinDigits':
                return 'password_policies.min_numbers';
            case 'MinSpecialCharacters':
                return 'password_policies.min_special_characters';
            default:
                return parameterName;
        }
    }

    private getChargePointParameter(parameterName: string): string {
        switch (parameterName) {
            case 'Name':
                return 'charge_points.name';
            case 'OCPPID':
                return 'charge_points.OCPPID';
            case 'PowerInkW':
                return 'charge_points.power';
            case 'Latitude':
                return 'charge_points.latitude';
            case 'Longitude':
                return 'charge_points.longitude';
            case 'LocationID':
                return 'charge_points.location';
            case 'NotificationPolicyID':
                return 'charge_points.notification_policy';
            case 'ChargePointLinkID':
                return 'charge_points.linked_with';
            default:
                return parameterName;
        }
    }
}
