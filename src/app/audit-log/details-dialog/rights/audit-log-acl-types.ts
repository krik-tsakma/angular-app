import { Module } from '../../../auth/acl.types';

export interface AuditLogAclRecord {
    revoked: Module[];
    granted: Module[];
}
