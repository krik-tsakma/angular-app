// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { AccessRightsPipesModule } from '../../../common/access-rights/access-rights-pipes.modules';

// COMPONENTS
import { AuditLogAclComponent } from './audit-log-acl.component';

@NgModule({
    imports: [
        SharedModule,
        AccessRightsPipesModule
    ],
    declarations: [
        AuditLogAclComponent
    ],
    exports: [  
        AuditLogAclComponent
    ]
})
export class AuditLogAclModule { }
