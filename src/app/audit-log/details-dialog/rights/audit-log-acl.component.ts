// FRAMEWORK
import { Component, Input, OnChanges, SimpleChanges, ViewEncapsulation } from '@angular/core';

// TYPES
import { Module } from '../../../auth/acl.types';
import { AuditLogAclRecord } from './audit-log-acl-types';

@Component({
    selector: 'audit-log-acl',
    templateUrl: 'audit-log-acl.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['audit-log-acl.less']
})

export class AuditLogAclComponent implements OnChanges {
    @Input() public modelData: string;
    public revoked: Module[] = [];
    public granted: Module[] = [];

    constructor() { 
        // foo
    }

    public ngOnChanges(changes: SimpleChanges) {
        const dataChange = changes['modelData'];
        if (dataChange && !dataChange.previousValue && dataChange.currentValue) {
            const data = JSON.parse(this.modelData) as AuditLogAclRecord;
            if (data) {
                this.revoked = data.revoked;
                this.granted = data.granted;
            }
           
        }
    }
}
