import { CustomTranslateService } from '../../shared/custom-translate.service';
// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';
import { AuditLogEntityTypes } from '../audit-log-types';

@Pipe({
    name: 'myAuditLogParameterValue',
    pure: false
})

export class AuditLogParameterValuePipe implements PipeTransform {
    constructor(private translate: CustomTranslateService) {
        // foo
    }

    public transform(val: string,  entityType: AuditLogEntityTypes): string {
        let term = '';
        switch (val) {
            case 'True':
                term = 'form.actions.enabled';
                break;
            case 'False':
                term = 'form.actions.disabled';
                break;
            default:
                break;
        }
       
        switch (entityType) {
            case AuditLogEntityTypes.COMPANY:
                val = this.getCompanyValue(val);
                break;
            case AuditLogEntityTypes.DRIVER:
                val = this.getDriverValue(val);
                break;
            case AuditLogEntityTypes.USER:
                val = this.getUserValue(val);
                break;
            case AuditLogEntityTypes.VEHICLE_TYPE:
                val = this.getVehicleTypeValue(val);
                break;
        }

        return term ? this.translate.instant(term) : val;
    }

    private getCompanyValue(parameterValue: string): string {
        let term = '';
        switch (parameterValue) {
            case 'LicencePlate':
                term = 'labels.vehicle.licence_plate';
                break;
            case 'OtherID':
                term =  'labels.vehicle.other_id';
                break;
            case 'Name':
                term =  'labels.driver.name';
                break;
            case 'OtherID':
                term =  'labels.driver.other_id';
                break;
            case 'Metric':
                term =  'measurement.system.metric';
                break;
            case 'Imperial':
                term =  'measurement.system.imperial';
                break;
        }
        return term ? this.translate.instant(term) : parameterValue;
    }

    private getDriverValue(parameterValue: string): string {
        let term = '';
        switch (parameterValue) {
            case 'iButton':
                term = 'drivers.pin.type.iButton';
                break;
            case 'MYFARE_UID':
                term =  'drivers.pin.type.mifare_uid';
                break;
            case 'OwnID':
                term =  'drivers.ID';
                break;
            case 'DTCO_CARD':
                term =  'drivers.pin.type.dtco_card';
                break;
            case 'USER_ID':
                term = 'drivers.account.user.remove';
        }
        return term ? this.translate.instant(term) : parameterValue;
    }

    private getUserValue(parameterValue: string): string {
        let term = '';
        switch (parameterValue) {
            case 'LicencePlate':
                term = 'labels.vehicle.licence_plate';
                break;
            case 'OtherID':
                term =  'labels.vehicle.other_id';
                break;
            case 'Name':
                term =  'labels.driver.name';
                break;
            case 'OtherID':
                term =  'labels.driver.other_id';
                break;
            case 'Metric':
                term =  'measurement.system.metric';
                break;
            case 'Imperial':
                term =  'measurement.system.imperial';
                break;
        }
        return term ? this.translate.instant(term) : parameterValue;
    }

    private getVehicleTypeValue(parameterValue: string): string {
        let term = '';
        switch (parameterValue) {
            case 'HybridEV':
                term = 'vehicle_types.ev_type.hybridEV';
                break;
            case 'PluggableHybridEV':
                term =  'vehicle_types.ev_type.pluggableHybridEV';
                break;
            case 'BatteryEV':
                term =  'vehicle_types.ev_type.batteryEV';
                break;
        }
        return term ? this.translate.instant(term) : parameterValue;
    }

}
