export class Module {
    /* the module's ID  */
    public id: number;
    /* the module's name */
    public name: string;
    /* whether the user has access to this module */
    public hasAccess: boolean;
    /* the list of properties that this module includes */
    public properties: Property[];
}

export class Property {
    /* the module's ID  */
    public moduleID: number;
    /* the property's ID  */
    public id: number;
    /* The property's name */
    public name: string;
    /* whether the user has access to this property */
    public hasAccess: boolean;
}

export enum ModuleOptions {
    Dashboard = 1,
    TripReview = 2,
    FollowUp = 3,
    Events = 4,
    EVOperations = 5,
    Bookings = 6,
    Reports = 7,
    Admin = 8,
    ChargePointManager = 9,
    FiscalTripReview = 10,
    DriverTakeOver = 11
}

export enum TripReviewPropertyOptions {
    ViewTrips = 0x1,
    ReassignDriver = 0x2
}

export enum EVOperationsPropertyOptions {
    Vehicles = 0x1,
    ChargingMonitor = 0x2
}

export enum AdminPropertyOptions {
    Scores = 0x1,
    BookNGo = 0x2,
    Users = 0x4,
    Vehicles = 0x8,
    Drivers = 0x10,
    Locations = 0x20,
    AssetLinks = 0x40,
    Company = 0x80,
    ChargePoints = 0x100
}

export enum NoExtraRightsPageOptions {
    QuickFilters,
    AuditLog,
    AccessDenied,
    Announcements
}

export interface ModuleProperty {
    module: ModuleOptions;
    property?: any;
}

export class NoExtraUserRightsNeeded {
    public static get(page: string): boolean {
        for (const key in NoExtraRightsnPageMapping) {
            if (NoExtraRightsnPageMapping.hasOwnProperty(key)) {
                const value = NoExtraRightsnPageMapping[key];
                const startsWithExpression = new RegExp('^' + key, 'i');
                if (page.match(startsWithExpression)) {
                    return true;
                }
            }
        }
        return false;
    }
}

export class PageModuleMap {
    public static getForPage(page: string): ModuleProperty {
        const mp = {} as ModuleProperty;

        for (const key in ModulePageMapping) {
            if (ModulePageMapping.hasOwnProperty(key)) {
                const value = ModulePageMapping[key];
                const startsWithExpression = new RegExp('^' + key, 'i');
                if (page.match(startsWithExpression)) {
                    mp.module = value;
                    break;
                }
            }
        }
        if (mp.module === ModuleOptions.EVOperations) {
            for (const key in ModuleEvOperationsPageMapping) {
                if (ModuleEvOperationsPageMapping.hasOwnProperty(key)) {
                    const value = ModuleEvOperationsPageMapping[key];
                    const startsWithExpression = new RegExp('^' + key, 'i');
                    if (page.match(startsWithExpression)) {
                        mp.property = value;
                    }
                }
            }
        }

        if (mp.module === ModuleOptions.Admin) {
            for (const key in ModuleAdminPageMapping) {
                if (ModuleAdminPageMapping.hasOwnProperty(key)) {
                    const value = ModuleAdminPageMapping[key];
                    const startsWithExpression = new RegExp('^' + key, 'i');
                    if (page.match(startsWithExpression)) {
                        mp.property = value;
                    }
                }
            }
        }
        return mp;
    }
}

// =======================
// WEB SITE'S MAIN PAGES
// =======================

/** These are the keys used to store info in local storage. */
export class WebSiteMainPages {
    public static readonly dashboard = '/dashboard';
    public static readonly tripReview = '/trip-review';
    public static readonly followUp = '/follow-up';
    public static readonly events = '/events';
    public static readonly bookings = '/bookings';
    public static readonly reports = '/reports';
    public static readonly subscriptions = '/subscriptions';
    public static readonly admin = '/admin';
    public static readonly evOperations = '/ev-operations';
    public static readonly chargePointManager = '/charge-points-manager';
    public static readonly fiscalTrips = '/fiscal-trips';
    public static readonly driverTakeOver = '/driver-take-over';
}


// ====================
// MAPPINGS
// ====================
/* Maps a route in this website to a module from acl  */
export let ModulePageMapping: { [key: string]: ModuleOptions } = {
    [WebSiteMainPages.dashboard]: ModuleOptions.Dashboard,
    [WebSiteMainPages.tripReview]: ModuleOptions.TripReview,
    [WebSiteMainPages.followUp]: ModuleOptions.FollowUp,
    [WebSiteMainPages.events]: ModuleOptions.Events,
    [WebSiteMainPages.bookings]: ModuleOptions.Bookings,
    [WebSiteMainPages.reports]: ModuleOptions.Reports,
    [WebSiteMainPages.subscriptions]: ModuleOptions.Reports,
    [WebSiteMainPages.admin]: ModuleOptions.Admin,
    [WebSiteMainPages.evOperations]: ModuleOptions.EVOperations,
    [WebSiteMainPages.chargePointManager]: ModuleOptions.ChargePointManager,
    [WebSiteMainPages.fiscalTrips]: ModuleOptions.FiscalTripReview,
    [WebSiteMainPages.driverTakeOver]: ModuleOptions.DriverTakeOver
};

/* Maps a route in this website to an ev operation module's property from acl */
export let ModuleEvOperationsPageMapping: { [key: string]: EVOperationsPropertyOptions } = {
    ['/' + WebSiteMainPages.evOperations + '/vehicles']: EVOperationsPropertyOptions.Vehicles,
    ['/' + WebSiteMainPages.evOperations + '/charging-monitor']: EVOperationsPropertyOptions.ChargingMonitor,
};

/* Maps a route in this website to an admin module's property from acl */
export let ModuleAdminPageMapping: { [key: string]: AdminPropertyOptions } = {
    ['/' + WebSiteMainPages.admin + '/energy_score']: AdminPropertyOptions.Scores,
    ['/' + WebSiteMainPages.admin + '/score-definition']: AdminPropertyOptions.Scores,
    ['/' + WebSiteMainPages.admin + '/vehicle_types']: AdminPropertyOptions.Vehicles,
    ['/' + WebSiteMainPages.admin + '/fair-use-policies']: AdminPropertyOptions.BookNGo,
    ['/' + WebSiteMainPages.admin + '/tariffs']: AdminPropertyOptions.BookNGo,
    ['/' + WebSiteMainPages.admin + '/users']: AdminPropertyOptions.Users,
    ['/' + WebSiteMainPages.admin + '/user-roles']: AdminPropertyOptions.Users,
    ['/' + WebSiteMainPages.admin + '/vehicles']: AdminPropertyOptions.Vehicles,
    ['/' + WebSiteMainPages.admin + '/drivers']: AdminPropertyOptions.Drivers,
    ['/' + WebSiteMainPages.admin + '/charge-points']: AdminPropertyOptions.ChargePoints,
};

/* Maps a route in this website that doesnt need any extra validation apart from authorization */
export let NoExtraRightsnPageMapping: { [key: string]: NoExtraRightsPageOptions } = {
    ['/quick-filters']: NoExtraRightsPageOptions.QuickFilters,
    ['/audit-log']: NoExtraRightsPageOptions.AuditLog,
    ['/access-denied']: NoExtraRightsPageOptions.AccessDenied,
    ['/announcements']: NoExtraRightsPageOptions.Announcements
};
