import { CompanyAppearenceSettings } from '../admin/company/company.types';
import { Module } from './acl.types';
import { CountryOption } from '../common/country-selector/country-selector-types';


export interface UserAppOptions {
    uid: string;
    isManager: boolean;
    isDriver: boolean;
    driverID?: number;
    singlePointBoundingBoxDiagonalLengthKM: number;
    enhancedMapsOptions: EnhancedMapsOptions;
    accessRights: Module[];
    region: string;
    appearance: CompanyAppearenceSettings;
    refreshIntervalSecs: number;
}

export class User implements UserAppOptions {
    public region: string;
    public culture: string;
    public driverId: number;
    public isDriver: boolean;
    public isManager: boolean;
    public fullname: string;
    public measurementSystem: string;
    public initials: string;
    public refreshIntervalSecs: number;
    public singlePointBoundingBoxDiagonalLengthKM: number;
    public enhancedMapsOptions: EnhancedMapsOptions;
    public accessRights: Module[];
    public appearance: CompanyAppearenceSettings;
    public companyName: string;
    public uid: string;

    constructor(culture: string, firstname: string, lastname: string, measurementSystem: any, companyName: string) {
        this.culture = culture;
        this.isManager = false;
        this.fullname =  firstname + ' ' + lastname;
        this.measurementSystem = Number(measurementSystem) === 0
            ? 'metric'
            : 'imperial';
        this.companyName = companyName;
        this.initials = firstname.substring(0, 1).toUpperCase() +
            '' + lastname.substring(0, 1).toUpperCase();
    }

    public setAppOptions(uap: UserAppOptions) {
        this.isDriver = uap.driverID > 0;
        this.driverId = uap.driverID > 0 ? Number(uap.driverID) : null;
        this.isManager = uap.isManager;
        this.enhancedMapsOptions = uap.enhancedMapsOptions;
        this.accessRights = uap.accessRights;
        this.appearance = uap.appearance;

        this.refreshIntervalSecs = uap.refreshIntervalSecs ? uap.refreshIntervalSecs : 60;
        this.singlePointBoundingBoxDiagonalLengthKM = uap.singlePointBoundingBoxDiagonalLengthKM;
        this.region = uap.region;

        this.uid = uap.uid;
    }
}

export interface EnhancedMapsOptions {
    mapsBaseURL: string;
    serviceBaseURL: string;
    authToken: string;
    expiryDate: string;
    speedBuckets: InrixSpeedBucket[];
}

export interface InrixSpeedBucket {
    min: number;
    max: number;
    color: string;
    label: string;
}

export interface CaptchaVerificationRequest {
    captchaResponse: string;
}

export interface ShouldCaptchaRequest {
    username: string;
}

export interface LogFailedLoginRequest {
    username: string;
}

export let UserLanguageOptions: CountryOption[] = [
    {
        code: 'en-GB',
        codeCss: 'gb',
        label: 'English (UK)',
        default: true
    },
    {
        code: 'nl-NL',
        codeCss: 'nl',
        label: 'Nederlands',
        default: false
    },
    {
        code: 'de-DE',
        codeCss: 'de',
        label: 'Deutsch',
        default: false
    }
];
