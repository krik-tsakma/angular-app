export interface OAuthRejectResult {
    error: string;
    description: string;
}

export enum AuthResponseCodeOptions {
    /**
     * User authenticated successfully
     */
    authenticated = 0,
    /**
     * Wrong credentials
     */
    invalid_request = -1,
    /**
     * User account blocked
     */
    access_denied = -2,
    /**
     * Errors in request syntax
     */
    invalid_client = -3,
    invalid_request_object = -4,
    unsupported_grant_type = -5
}

export class OAuthResult {
    /* tslint:disable */

    /**
    * Will always be 'Bearer'.
    */
    public token_type: string;

    /**
    * Access token carry the necessary information to access a resource directly.
    */
    public access_token: string;

    /**
    * The lifetime in seconds of the access token.
    */
    public expires_in: number;

    /**
    * Refresh token carry the information necessary to get a new access token
    */
    public refresh_token: string;

    /**
     * ID tokens encode user information and are usually only meant to be used for display purposes on client-side apps
     */
    public id_token: string;
    /* tslint:enable */
}
