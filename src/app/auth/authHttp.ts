// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { StoreManagerService } from '../shared/store-manager/store-manager.service';

// TYPES
import { StoreItems } from '../shared/store-manager/store-items';
import { Config } from '../config/config';

@Injectable()
export class AuthHttp extends HttpClient {

    constructor(
        public httpHandler: HttpHandler,
        private config: Config,
        private storeManager: StoreManagerService) {
        super(httpHandler);
    }

    // Performs a request with `get` http method.
    public get(url: string, options?: any): Observable<any> {
        options = this.addAuthHeader(options);
        return super.get(this.roundRobinURL(url), options);
    }

    // Performs a request with `post` http method.
    public post(url: string, body: any, options?: any): Observable<any> {
        options = this.addAuthHeader(options);
        return super.post(this.roundRobinURL(url), body, options);
    }

    // Performs a request with `delete` http method.
    public delete(url: string, options?: any): Observable<any> {
        options = this.addAuthHeader(options);
        return super.delete(this.roundRobinURL(url), options);
    }

    // Performs a request with `put` http method.
    public put(url: string, body: any, options?: any): Observable<any> {
        options = this.addAuthHeader(options);
        return super.put(this.roundRobinURL(url), body, options);
    }


    public removeObjNullKeys(obj) {
        Object.keys(obj).forEach((key) => {
            if (obj[key] == null) {
                delete obj[key];
            }
        });

        return obj;
    }


    private roundRobinURL(url: string) {
        if (url.indexOf('{id}') >= 0) {
            const maxServerID = this.config.get('maxServerID');
            const id = Math.floor(Math.random() * maxServerID) + 1;
            const pattern = new RegExp('\\{id\\}', 'g');
            url = url.replace(pattern, id.toString());
        }
        return url;
    }

    private addAuthHeader(options?: any) {
        if (!options) {
            options = {};
        }

        const accessToken = this.storeManager.getOption(StoreItems.accessToken);

        if (!options.headers) {
            options.headers = new HttpHeaders({ Authorization: 'Bearer ' + accessToken});
        }

        return options;
    }
}



export class ObjectURLSearchParams extends HttpParams {

    constructor(obj: any) {
        const fromObject: any = obj;
        super({ fromObject });
    }
}
