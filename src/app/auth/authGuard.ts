// FRAMEWORK
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

// SERVICES
import { AuthService } from './authService';
import { Config } from '../config/config';

// TYPES
import { ModuleProperty, PageModuleMap, NoExtraUserRightsNeeded } from './acl.types';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        protected authService: AuthService,
        protected router: Router,
        private configService: Config) {
            // foo
        }

    public canActivate(route: ActivatedRouteSnapshot, stateIng: RouterStateSnapshot) {
        if (!this.authService.isLoggedIn()) {
            this.authService.logOut(true);
            return false;
        }

        const mp: ModuleProperty = PageModuleMap.getForPage(stateIng.url);
        // console.log(stateIng.url.indexOf('my-test'));
        // if (stateIng.url.indexOf('my-test') !== -1) {
        //     return true;
        // }
        if (NoExtraUserRightsNeeded.get(stateIng.url) === true) {
            return true;
        } else if (mp && mp.module && this.authService.userHasAccess(mp.module, mp.property)) {
            return true;
        } else {
            this.navigateToLogin();
            return false;
        }
    }

    private navigateToLogin() {
        // Navigate to the login page
        this.router.navigate(['/login'], { skipLocationChange: !this.configService.get('router') });
    }
}
