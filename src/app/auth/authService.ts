// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// RXJS
import { Observable, Subscription, Subject, of } from 'rxjs';
import { map, catchError, mergeMap } from 'rxjs/operators';

// SERVICES
import { UserOptionsService } from '../shared/user-options/user-options.service';
import { StoreManagerService } from '../shared/store-manager/store-manager.service';
import { AuthHttp } from './authHttp';
import { FileExportNotificationService } from '../admin/import-export/file-export-notification.service';

// TYPES
import { OAuthResult, AuthResponseCodeOptions, OAuthRejectResult } from './authResponse';
import { User, UserAppOptions, CaptchaVerificationRequest, ShouldCaptchaRequest, LogFailedLoginRequest } from './user';
import { StoreItems } from '../shared/store-manager/store-items';
import { Config } from '../config/config';
import { ModuleOptions, WebSiteMainPages, EVOperationsPropertyOptions } from './acl.types';

@Injectable()
export class AuthService {
    public logInSuccessfull: Subject<boolean> = new Subject<boolean>();
    private logOutSubsription: any;
    private refreshTokenSubscription: Subscription;
    constructor(
        private http: HttpClient,
        private config: Config,
        private authHttp: AuthHttp,
        private userOptions: UserOptionsService,
        private storeManager: StoreManagerService,
        private exportFileNotificationService: FileExportNotificationService
    ) {
             // foo
        }

    // Login service
    public logIn(username: string, password: string): Observable<AuthResponseCodeOptions> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let authRes: OAuthResult;
        return this.http
                .post('https://auth.sycada.com/oidc/token',
                    'grant_type=password'
                    + '&client_id=web'
                    + '&username=' + encodeURIComponent(username)
                    + '&password=' + encodeURIComponent(password)
            , { headers })
            .pipe(
                map((res: OAuthResult) => {
                    authRes = res;
                    return AuthResponseCodeOptions.authenticated;
                }),
                catchError((err) => {
                    const errorRes = err.error as OAuthRejectResult;
                    const errorCode: AuthResponseCodeOptions = AuthResponseCodeOptions[errorRes.error];
                    return of(errorCode);
                }),
                mergeMap((res: any) => {
                    if (res !== AuthResponseCodeOptions.authenticated) {
                        return of(res);
                    }
                    // after user logged in successfully, fetch the extra data we need
                    return this.afterAuthentication(authRes);
                })
            );
    }

    public performCaptchaCheck(captcha: CaptchaVerificationRequest) {
        return this.http.post(this.config.get('apiUrl') + '/api/Account/PerformCaptchaCheck', captcha, { observe: 'response' })
        .pipe(
            map((res) => {
                // this is means empty Ok response from the server
                if (res.status === 200 && !res.body) {
                    return null;
                } else  {
                    return res.body;
                }
            })
        );
    }


    public shouldCaptcha(check: ShouldCaptchaRequest) {
        return this.http.post(this.config.get('apiUrl') + '/api/Account/ShouldCaptcha', check, { observe: 'response' });
    }


    public logFailedLogin(fail: LogFailedLoginRequest) {
        return this.http.post(this.config.get('apiUrl') + '/api/Account/LogFailedLogin', fail, { observe: 'response' })
        .pipe(
            map((res) => {
                // this is means empty Ok response from the server
                if (res.status === 200 && !res.body) {
                    return null;
                } else  {
                    return res.body;
                }
            })
        );
    }



    public afterAuthentication(userResult: OAuthResult): Observable<AuthResponseCodeOptions> {
        const base64Response = userResult.id_token
                                .split('.')[1]
                                .replace('-', '+')
                                .replace('_', '/');
        const decodedResponse = decodeURIComponent(
            atob(base64Response).split('').map((c) => {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join('')
        );
        const u = JSON.parse(decodedResponse);

        const authorizedUser = new User(u.culture, u.firstname, u.lastname, u.measurementSystem, u.company);

        const d = new Date();
        const nowMS = d.getTime() + (userResult.expires_in * 1000);
        this.storeManager.saveOption(StoreItems.tokenExpirationTime, nowMS);
        this.storeManager.saveOption(StoreItems.accessToken, userResult.access_token);
        this.storeManager.saveOption(StoreItems.refreshToken, userResult.refresh_token);

        return this.authHttp
            .get(this.config.get('apiUrl') + '/api/User/GetUserSettings/')
            .pipe(
                map((r: any) => {
                    const uap = r as UserAppOptions;
                    // append to user object the extra options
                    authorizedUser.setAppOptions(uap);
                    // save user settings in storage
                    this.storeManager.saveOption(StoreItems.appUser, authorizedUser);

                    this.logInSuccessfull.next(true);
                    return AuthResponseCodeOptions.authenticated;
                }),
                catchError((err) => {
                    return of(AuthResponseCodeOptions.invalid_request_object);
                })
            );
    }

    // Check if it is LoggedIn service
    public isLoggedIn(): boolean {
        const tokenExpirationMS = this.storeManager.getOption(StoreItems.tokenExpirationTime);
        const currentUser = this.storeManager.getOption(StoreItems.appUser);
        if (tokenExpirationMS && currentUser) {
            const now = new Date();
            if (tokenExpirationMS <= now.getTime()) {
                // session timeout, refresh token
                if (!this.refreshTokenSubscription) {
                    this.refreshTokenSubscription = this.refreshToken().subscribe(() => {
                        this.refreshTokenSubscription = null;
                    });
                }

                return false;
            }
            return true;
        }
        return false;
    }

    // Logout Service
    public logOut(autoLogOff: boolean, redirect?: boolean): Promise<boolean> {
        this.exportFileNotificationService.Stop();

        // if logOutSubsription has value it means that user clicked on log out again.
        if (this.logOutSubsription) {
            return;
        }

        return new Promise((resolve) => {
            this.logOutSubsription = this.closeUserLog(autoLogOff)
            .subscribe(
                (res) => {
                    this.clearSessionData(redirect);
                },
                (err) => {
                    this.clearSessionData(redirect);
                },
                () => {
                    resolve(true);
                }
            );
        });

    }

    public userHasAccess(moduleID: number, propertyID?: number): boolean {
        const accessRights = this.userOptions.getUser().accessRights;
        if (accessRights === null || accessRights === undefined || accessRights.length === 0) {
            return false;
        } else {
            const module = accessRights.find((m) => m.id === moduleID);
            if (!module) {
                return false;
            }
            if (propertyID !== null && propertyID !== undefined) {
                const property = module.properties.find((p) => p.id === propertyID);
                if (!property) {
                    return false;
                }
                return module.hasAccess && property.hasAccess;
            } else {
                return module.hasAccess;
            }
        }
    }

    public defaultHomePage(): string {
        let defaultRedirectURL = '/dashboard';
        if (this.userHasAccess(ModuleOptions.Dashboard)) {
            defaultRedirectURL = WebSiteMainPages.dashboard;
        } else if (this.userHasAccess(ModuleOptions.TripReview)) {
            defaultRedirectURL = WebSiteMainPages.tripReview;
        } else if (this.userHasAccess(ModuleOptions.FollowUp)) {
            defaultRedirectURL = WebSiteMainPages.followUp;
        } else if (this.userHasAccess(ModuleOptions.Events)) {
            defaultRedirectURL = WebSiteMainPages.events;
        } else if (this.userHasAccess(ModuleOptions.Reports)) {
            defaultRedirectURL = WebSiteMainPages.reports;
        } else if (this.userHasAccess(ModuleOptions.Bookings)) {
            defaultRedirectURL = WebSiteMainPages.bookings;
        } else if (this.userHasAccess(ModuleOptions.DriverTakeOver)) {
            defaultRedirectURL = WebSiteMainPages.driverTakeOver;
        } else if (this.userHasAccess(ModuleOptions.Admin)) {
            defaultRedirectURL = WebSiteMainPages.admin;
        }  else if (this.userHasAccess(ModuleOptions.ChargePointManager)) {
            defaultRedirectURL = WebSiteMainPages.chargePointManager;
        } else if (this.userHasAccess(ModuleOptions.EVOperations)) {
            if (this.userHasAccess(ModuleOptions.EVOperations, EVOperationsPropertyOptions.Vehicles)) {
                defaultRedirectURL = '/' + WebSiteMainPages.evOperations + '/vehicles';
            } else if (this.userHasAccess(ModuleOptions.EVOperations, EVOperationsPropertyOptions.ChargingMonitor)) {
                defaultRedirectURL = '/' + WebSiteMainPages.evOperations + '/charging-monitor';
            }
        } else {
            defaultRedirectURL = '/access-denied';
        }

        return defaultRedirectURL;
    }

    private refreshToken(): Observable<AuthResponseCodeOptions> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.http.post('https://auth.sycada.com/oidc/token',
                'grant_type=refresh_token'
                + '&refresh_token=' + this.storeManager.getOption(StoreItems.refreshToken)
                + '&client_id=web',
                { headers })
            .pipe(
                map((res: OAuthResult) => {
                    return this.afterAuthentication(res);
                }),
                catchError((err) => {
                    this.logOut(true);
                    return of(null);
                })
            );
    }

    private clearSessionData(redirect?: boolean) {
        // save the user's culture and delete everything else from storage
        const languageSelection = this.storeManager.getOption(StoreItems.culture);
        const languageSelectionUserSet = this.storeManager.getOption(StoreItems.cultureSetByUser);

        this.storeManager.clear();
        if (languageSelection) {
            this.storeManager.saveOption(StoreItems.culture, languageSelection);
            if (languageSelectionUserSet) {
                this.storeManager.saveOption(StoreItems.cultureSetByUser, languageSelectionUserSet);
            }
        }
        // use window location instead of angular's router to reinstatiate app
        if (!redirect) {
            window.location.href = '/';
        }
    }

    // ==============
    // USER LOG
    // ==============

    private closeUserLog(autoLogOff: boolean): Observable<void>  {
        const params = { AutoLogOff: autoLogOff };
        return this.authHttp
            .put(this.config.get('apiUrl') + '/api/User/LogClose/', params, { observe: 'response' })
            .pipe(
                map((res: Response) => null)
            );
    }
}
