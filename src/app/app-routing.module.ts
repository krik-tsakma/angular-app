// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';

// COMPONENTS
import { PageNotFoundComponent } from './core/error-pages/404error/pageNotFound.component';
import { AccessDeniedComponent } from './core/error-pages/access-denied/access-denied.component';
import { UnexpectedErrorComponent } from './core/error-pages/unexpected-error/unexpected-error.component';

// SERVICES
import { SelectivePreloadingStrategyService } from './selective-preloading-strategy.service';


const appRoutes: Route[] = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule),
        data: {
            breadcrumb: 'sidebar.admin'
        }
    },
    {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
        data: {
            preload: true
        }
    },
    {
        path: 'trip-review',
        loadChildren: () => import('./trip-review/trip-review.module').then((m) => m.TripReviewModule)
    },
    {
        path: 'follow-up',
        loadChildren: () => import('./follow-up/follow-up.module').then((m) => m.FollowUpModule)
    },
    {
        path: 'quick-filters',
        loadChildren: () => import('./common/quick-filters/quick-filters.module').then((m) => m.QuickFiltersModule)
    },
    {
        path: 'events',
        loadChildren: () => import('./events/events.module').then((m) => m.EventsModule)
    },
    {
        path: 'ev-operations/vehicles',
        loadChildren: () => import('./ev-operations/ev-vehicles/ev-vehicles.module').then((m) => m.EvVehiclesModule)
    },
    {
        path: 'ev-operations/charging-monitor',
        loadChildren: () => import('./ev-operations/charging-monitor/charging-monitor.module').then((m) => m.ChargingMonitorModule)
    },
    {
        path: 'charge-points-manager',
        loadChildren: () => import('./charge-point-manager/charging-point-manager.module').then((m) => m.ChargingPointManagerModule)
    },
    {
        path: 'bookings',
        loadChildren: () => import('./bookings/bookings.module').then((m) => m.BookingsModule)
    },
    {
        path: 'login',
        loadChildren: () => import('./core/login/login.module').then((m) => m.LoginModule)
    },
    {
        path: 'reset-password',
        loadChildren: () => import('./core/reset-password/reset-password.module').then((m) => m.ResetPasswordModule)
    },
    {
        path: 'reports',
        loadChildren: () => import('./reporting/reports/reports.module').then((m) => m.ReportsModule)
    },
    {
        path: 'subscriptions',
        loadChildren: () => import('./reporting/subscriptions/subscriptions.module').then((m) => m.SubscriptionsModule)
    },
    {
        path: 'fiscal-trips',
        loadChildren: () => import('./fiscal-trips/fiscal-trips.module').then((m) => m.FiscalTripsModule)
    },
    {
        path: 'audit-log',
        loadChildren: () => import('./audit-log/audit-log.module').then((m) => m.AuditLogModule)
    },
    {
        path: 'announcements',
        loadChildren: () => import('./announcements/announcements.module').then((m) => m.AnnouncementsModule)
    },
    {
        path: 'driver-take-over',
        loadChildren: () => import('./driver-take-over/driver-take-over.module').then((m) => m.DriverTakeOverModule)
    },
    {
        path: 'access-denied',
        component: AccessDeniedComponent
    },
    {
        path: 'unexpected-error',
        component: UnexpectedErrorComponent
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes, {
                useHash: true,
                preloadingStrategy: SelectivePreloadingStrategyService
            }
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
