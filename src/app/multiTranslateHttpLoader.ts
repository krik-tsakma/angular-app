// FRAMEWORK
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateLoader } from '@ngx-translate/core';

// RXJS
import { forkJoin, Observable, of } from 'rxjs';
import { tap, map, pluck, publishReplay, refCount, catchError } from 'rxjs/operators';

// SERVICES
import { TranslateStateService } from './core/translateStore.service';
import { MissingTranslationHandler, MissingTranslationHandlerParams} from '@ngx-translate/core';



export class MultiTranslateHttpLoader implements TranslateLoader {

    constructor(
        private http: HttpClient,
        public resources: string[],
        public suffix: string,
        private translateState: TranslateStateService) {  }

  /**
   * Gets the translations from the server
   */
    public getTranslation(lang: string): Observable<{}> {
        const notdownloaded: string[] = this.resources.filter((resource) => {
            if (this.translateState.cachedArray.find((item) => item === `${resource}${lang}.json`) === undefined) {
                return resource;
            }
        });

        if (notdownloaded.length === 0) {
            return of(this.translateState.translations[lang] !== undefined
                ? this.translateState.translations[lang]
                : {});
        }

        return forkJoin(
                notdownloaded
                    .map((config) => {
                        this.translateState.cachedArray.push(`${config}${lang}.json`);
                        return this.http.get(`${config}${lang}.json`);
                    })
                ).pipe(
                    map((response: any) => {
                        response.forEach((res) => {
                            const myRes = new Object();
                            myRes[lang] = res;
                            this.translateState.translations = myRes;
                        });
                        return this.translateState.translations[lang];
                    }),
                    catchError((err) => {
                        return of({});
                    }),
                );
    }
}

export class MultiMissingTranslationHandler implements MissingTranslationHandler {
    public onlyOnceDownloaded: boolean = false;
    public onlyOnceCurrentDownloaded: boolean = false;
    public translations: any;
    public waitingResponse: boolean = false;
    public waitingCurrentResponse: boolean = false;

    constructor(public translateState: TranslateStateService, public resources?: string[]) { }

    public handle(params: MissingTranslationHandlerParams) {
        const defaultLang = params.translateService.store.defaultLang;
        const currentLang = params.translateService.store.currentLang;

        if (params.key === ' ' || defaultLang === undefined) {
            return;
        }

        // get translations that are stored in our custom state.(all languages)
        const getTranslation = Object.assign({}, this.translateState.translations);

        // Means that translation service has no translations at all. Neither for current lang
        // nor for default. So first call current lang.
        if (Object.entries(params.translateService.translations).length === 0 &&
            params.translateService.translations.constructor === Object) {
                if (!this.onlyOnceCurrentDownloaded) {
                    this.onlyOnceCurrentDownloaded = true;
                    this.translations = params.translateService.currentLoader
                        .getTranslation(currentLang)
                        .pipe(
                            tap(() => this.waitingCurrentResponse = true),
                            publishReplay(1),
                            refCount(),
                        );
                    return this.translations.pipe(pluck(params.key));
                } else {
                    if (getTranslation[currentLang][params.key] !== undefined) {
                        return getTranslation[currentLang][params.key];
                    } else if (!this.waitingCurrentResponse) {
                        return this.translations.pipe(pluck(params.key));
                    }
                }
                return;
        }

        // Translation service has current lang translations but can not find translations for the
        // current params.key. That's why it needs to download default lang's json.
        if ((Object.keys(getTranslation).find((key) => key === defaultLang) === undefined ||
            getTranslation[defaultLang][params.key] === undefined) && !this.onlyOnceDownloaded) {
                // console.log('onlyOnceDownloaded');
                this.onlyOnceDownloaded = true;
                this.translations = params.translateService.currentLoader
                                        .getTranslation(defaultLang)
                                        .pipe(
                                            tap(() => this.waitingResponse = true),
                                            publishReplay(1),
                                            refCount()
                                        );

                return this.translations.pipe(pluck(params.key));
        } else {
            // console.log('else');
            if (getTranslation[defaultLang][params.key] !== undefined) {
                // console.log('getTranslation ', getTranslation[defaultLang][params.key]);
                return getTranslation[defaultLang][params.key];
            } else if (!this.waitingResponse) {
                // console.log('waitingResponse is false');
                return this.translations.pipe(pluck(params.key));
            } else {
                // console.log('waitingResponse is true, key didnt find', params.key);
                return params.key;
            }
        }
    }

}
