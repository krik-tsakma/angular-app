import { KeyName } from '../key-name';
import { TripEvStatuses } from './status-types';

export enum AssetTypes {
    Vehicle = 0,
    HR = 1
}
export interface AssetIndexObject {
    vehicleID: number;
    timestamp: string;
    assetType: AssetTypes;
    
}
export interface AssetExtended extends Asset {
    isLinked: boolean;
    isEv?: boolean;
    vehicleStatus: TripEvStatuses;
    batteryLevel: TripEvStatuses;
}

export interface Asset extends AssetIndexObject {
    vehicleLabel: string;
    driverID: number;
    driverLabel: string;
    latitude: number;
    longitude: number; 
    isHidden?: boolean;
}
export interface Trace extends Asset {
    tripStart: string;
    tripStop: string;
    gpsValid: boolean;
    gpsDirection: number;
    isClosed: boolean;
    speed: number;
}

export interface TraceEvent extends Asset {
    description: string;
    address: string;
    ruleID: number;
    ruleName?: string;
    locationID: number;
    classIDX: number;
    classLabel: string;
    priorityIDX: number;
    priorityLabel: string;
    statusIDX: number;
    statusLabel: string;
    gpsValid: true;
}

// ====================
// ENUMS
// ====================

export enum MeasurementSystemOptions {
    Metric = 0,
    Imperial = 1,
}

export enum VehicleLabelOptions {
    LicencePlate = 0,
    OtherID = 1,
}

export enum DriverLabelOptions {
    Name = 0,
    OtherID = 1,
}


export let VehicleLabels: KeyName[] = [
    {
        id: VehicleLabelOptions.LicencePlate,
        term: 'labels.vehicle.licence_plate',
        name: 'Licence plate',
        enabled: true
    },
    {
        id: VehicleLabelOptions.OtherID,
        term: 'labels.vehicle.other_id',
        name: 'ID',
        enabled: true
    }
];

export let DriverLabels: KeyName[] = [
    {
        id: DriverLabelOptions.Name,
        term: 'labels.driver.name',
        name: 'Name',
        enabled: true
    },
    {
        id: DriverLabelOptions.OtherID,
        term: 'labels.driver.other_id',
        name: 'ID',
        enabled: true
    }
];

export let MeasurementSystems: KeyName[] = [
    {
        id: MeasurementSystemOptions.Metric,
        term: 'measurement.system.metric',
        name: 'Metric',
        enabled: true
    },
    {
        id: MeasurementSystemOptions.Imperial,
        term: 'measurement.system.imperial',
        name: 'Imperial',
        enabled: true
    }
];
