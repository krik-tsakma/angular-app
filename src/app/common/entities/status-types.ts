export enum TripEvStatuses {
    Offline = 1,
    Private = 2,
    Driving = 3,
    Parked = 4,
    NoStatus = 5,
    Charging = 6,
    BatteryLow = 7,
    BatteryMedium = 8,
    BatteryFull = 9
}

export interface TripEvStatusesArray {
    id: TripEvStatuses;
    name: string;
    enabled: boolean;
}
