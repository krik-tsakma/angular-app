// FRAMEWORK
import { Injectable } from '@angular/core';

// RXSK
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { KeyName } from '../key-name';

@Injectable()
export class TimezonesService {
    
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            // foo
        }

    public get(): Observable<KeyName[]> {
        return this.authHttp
            .get(this.config.get('apiUrl') + '/api/Lists/Timezones');
    }
}
