// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { TimezonesService } from './timezones-list.service';

// COMPONENTS
import { TimezonesListComponent } from './timezones-list.component';

@NgModule({
    imports: [
        SharedModule      
    ],
    declarations: [
        TimezonesListComponent
    ],
    exports: [
        TimezonesListComponent
    ],
    providers: [
        TimezonesService
    ]
})
export class TimezonesListModule {}
