// FRAMEWORK
import {
    Component, Input, Output, EventEmitter
} from '@angular/core';

// SERVICES
import { SnackBarService } from '../../shared/snackbar.service';
import { TimezonesService } from './timezones-list.service';

// TYPES
import { KeyName } from '../key-name';

@Component({
    selector: 'timezones-list',
    templateUrl: 'timezones-list.html'
})

export class TimezonesListComponent {
    @Input() public modelData?: string;
    @Output() public modelDataChange: EventEmitter<string> =
        new EventEmitter<string>();
   
    public data: KeyName[];
    public loading: boolean;

    constructor(private snackBar: SnackBarService,
                private service: TimezonesService
            ) {
      
        this.get();
    }


    public onChangeOption(id: string) {
        this.modelData = id;
        this.modelDataChange.emit(this.modelData);
    }
    

    private get() {
        this.loading = true;
        this.service
            .get()
            .subscribe((data: KeyName[]) => {
                this.loading = false;
                this.data = data;
                this.data.forEach((c) => {
                    c.enabled = true;
                });
                if (this.modelData === null || this.modelData === undefined) {
                    this.onChangeOption(this.data[0].id);
                }
            },
            (err) => {
                this.snackBar.open('data.error', 'form.actions.close');
                this.loading = false;
            });
    }

}
