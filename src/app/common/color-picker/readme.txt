﻿<color-picker 
	[options]="['#bd1919', '#fff', '#ccc', '#ddd', '#000']"
    [modelData]="'#000'"
    (notifyOnChange)="onChangeOption($event)">
</color-picker>

[option] = a string array with hex color codes
[selected] = the selected hex color
(notifyOnChange) = function that will take the selected color of the picker

