﻿// FRAMEWORK
import {
    Component, Input, Output, ChangeDetectorRef, ElementRef,
    EventEmitter, ViewEncapsulation, SimpleChanges,
    AfterViewInit } from '@angular/core';

@Component({
    selector: 'color-picker',
    templateUrl: './color-picker.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./color-picker.less']    
})


export class ColorPickerComponent {
    @Input() public options: string[];
    @Input() public modelData: string;
    @Output() public modelDataChange: EventEmitter<any> = new EventEmitter();

    public open: boolean;

    constructor(private elementRef: ElementRef) {
        // Foo
    }

    public setVisibility(shouldOpen?: boolean): void {
        if (shouldOpen !== undefined) {
            this.open = shouldOpen;
        } else {
            this.open = !this.open;
        }
    }

    public onOptionChange(option: string) {
        this.setVisibility(false);
        this.modelData = option;
        this.modelDataChange.emit(this.modelData);             
    }
}
