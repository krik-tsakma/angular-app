import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
// FRAMEWORK
import { NgModule } from '@angular/core';

// COMPONENTS
import { ColorPickerComponent } from './color-picker.component';

@NgModule({
    imports: [
        CommonModule                
    ],
    declarations: [
        ColorPickerComponent
    ],
    exports: [
        ColorPickerComponent
    ]
})
export class ColorPickerModule {}
