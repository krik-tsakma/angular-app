// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { GoogleMapsConfigurationService } from '../../google-maps/services/google-maps-configuration.service';

// COMPONENTS
import { AddressAutocompleteComponent } from './address-autocomplete.component';

/**
 * Contains all componenents of the places module. 
 */
@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        AddressAutocompleteComponent,
    ],
    exports: [
        AddressAutocompleteComponent,
    ], 
    providers: [
        GoogleMapsConfigurationService
    ]
})
export class PlacesAutocompleteModule { }
