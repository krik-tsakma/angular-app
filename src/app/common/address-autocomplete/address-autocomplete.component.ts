// FRAMEWORK
import { 
    Component, ViewEncapsulation, Input, Output, 
    EventEmitter, ViewChild, ElementRef, OnInit, OnDestroy
} from '@angular/core';
import { Subscription } from 'rxjs';

// SERVICES
import { GoogleMapsConfigurationService } from '../../google-maps/services/google-maps-configuration.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';

// TYPES
import { AddressAutocompleteResult } from './address-autocomplete-types';

@Component({
    selector: 'address-autocomplete',
    templateUrl: 'address-autocomplete.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['address-autocomplete.less'],
})

export class AddressAutocompleteComponent implements OnInit, OnDestroy {
    @ViewChild('addressSearch', { static: true }) public searchField: ElementRef;
    @Input() public modelData: AddressAutocompleteResult = null;
    @Output() public modelDataChange: EventEmitter<AddressAutocompleteResult> = new EventEmitter<AddressAutocompleteResult>();

    private autocompleteAddress: google.maps.places.Autocomplete;
    private subscriber: Subscription;

    constructor(private configService: GoogleMapsConfigurationService,
                private unsubscribeService: UnsubscribeService) {
                    // foo
    }

    public ngOnInit() {
        this.configService.configure().then(() => {
            this.createAutomplete();
        });
    }


    public ngOnDestroy(): void {
        this.unsubscribeService.removeSubscription(this.subscriber);
    }

    
    /**
     * Create the autocomplete object, restricting the search to geographical location types.
     */
    private createAutomplete(): void {
        const searchElm = this.searchField.nativeElement;
        this.autocompleteAddress = new google.maps.places.Autocomplete(searchElm, { types: ['geocode'] });

        // When the user selects an address from the dropdown, populate the address fields in the form.
        this.autocompleteAddress.addListener('place_changed', () => {
            // Get the address details from the autocomplete object.
            const place = this.autocompleteAddress.getPlace();
            const addressSelected = {
                address: place.formatted_address,
                latitude: place.geometry.location.lat(),
                longitude: place.geometry.location.lng()
            } as AddressAutocompleteResult;

            // console.log(addressSelected);
            this.modelDataChange.emit(addressSelected);
        });

        // add a custom keyup event listener so that we can publish the empty selection
        searchElm.addEventListener('keyup', () => {
            if (!searchElm.value) {
                const empty = { address: '', latitude: 0, longitude: 0 } as AddressAutocompleteResult;
                this.modelDataChange.emit(empty);
            }
        });
    }
}
