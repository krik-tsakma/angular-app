export interface AddressAutocompleteResult {
    address: string;
    latitude: number;
    longitude: number;
}
