// FRAMEWORK
import { NgModule  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';

// COMPONENT
import { CountrySelectorComponent } from './country-selector.component';

@NgModule({
    imports: [
        CommonModule,
        MatIconModule,
        MatButtonModule,
        MatMenuModule,
        TranslateModule
    ],
    declarations: [
        CountrySelectorComponent
    ],
    exports: [
        CountrySelectorComponent
    ],
    providers: [
        CustomTranslateService
    ]
})
export class CountrySelectorModule {}
