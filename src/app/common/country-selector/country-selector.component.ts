// FRAMEWORK
import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

// TYPES
import { CountryOption } from './country-selector-types';


@Component({
    selector: 'country-selector',
    templateUrl: './country-selector.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./country-selector.less']
})

export class CountrySelectorComponent  {
    @Input() public options: CountryOption[];
    @Input() public selected: CountryOption;
    @Input() public noneOption?: boolean;
    @Output() public onSelect: EventEmitter<CountryOption> = new EventEmitter<CountryOption>();

    constructor() {
        // foo
    }

    public selectCountry(country?: CountryOption) {
        this.selected = country;      
        this.onSelect.emit(country);
    }
}
