
export interface CountryOption  {
    code: string;
    codeCss: string;
    label: string;
    default: boolean;
}
