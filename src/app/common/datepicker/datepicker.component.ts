// FRAMEWORK
import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, HostListener } from '@angular/core';
import { DateAdapter } from '@angular/material/core';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'datepicker',
    templateUrl: 'datepicker.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['datepicker.less']
})

export class DatepickerComponent implements OnInit {
    @Input() public id: string;
    @Input() public min: moment.Moment;
    @Input() public max: moment.Moment;
    @Input() public modelData: moment.Moment;
    @Output() public modelDataChange: EventEmitter<moment.Moment> = new EventEmitter<moment.Moment>();
    public touchUiEnabled: boolean;

    constructor(private dateAdapter: DateAdapter<Date>,
                private userOptions: UserOptionsService) {

            this.modelData = moment();
    }

    @HostListener('window:resize', ['$event']) public onResize(event) {
        this.setTouchOption();
    }

    public ngOnInit() {
        this.setTouchOption();
        this.dateAdapter.setLocale(this.userOptions.localOptions.moment_locale);
        moment.updateLocale(this.userOptions.localOptions.moment_locale, {
            longDateFormat: {
                LT: 'h:mm A',
                LTS: 'h:mm:ss A',
                L: this.userOptions.localOptions.short_date,
                LL: 'MMMM Do YYYY',
                LLL: 'MMMM Do YYYY LT',
                LLLL: 'dddd, MMMM Do YYYY LT',
                l: this.userOptions.localOptions.short_date
            }
        });
    }

    public onSelectDate(event) {
        const dateSelected = moment(event.value);
        this.modelData = dateSelected;
        this.modelDataChange.emit(this.modelData);
    }

    private setTouchOption() {
        // Whether the calendar UI is in touch mode.
        // In touch mode the calendar opens in a dialog rather than
        // a popup and elements have more padding to allow for bigger touch targets.
        this.touchUiEnabled = window.innerWidth > 720
                            ? false
                            : true;
    }
}
