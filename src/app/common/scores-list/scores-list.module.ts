// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { ScoresListService } from './scores-list.service';

// COMPONENTS
import { ScoresListComponent } from './scores-list.component';

@NgModule({
    imports: [
        SharedModule      
    ],
    declarations: [
        ScoresListComponent
    ],
    exports: [
        ScoresListComponent
    ],
    providers: [
        ScoresListService
    ]
})
export class ScoresListModule {}
