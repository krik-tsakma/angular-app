import { KeyName } from '../key-name';

export interface ScoreListItem extends KeyName {
    default: boolean;
}