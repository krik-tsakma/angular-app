// FRAMEWORK
import {
    Component, Input, Output, EventEmitter,
    OnChanges, SimpleChanges
} from '@angular/core';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { ScoresListService } from './scores-list.service';

// TYPES
import { ScoreListItem } from './scores-list-types';

@Component({
    selector: 'scores-list',
    templateUrl: 'scores-list.html'
})

export class ScoresListComponent implements OnChanges {
    @Input() public energyScoreEnabled: boolean;
    @Input() public modelData?: number;
    @Output() public modelDataChange: EventEmitter<number> =
        new EventEmitter<number>();
   
    public data: ScoreListItem[];
    public loading: boolean;
    private energyScoreTranslation: string;
    private energyScoreID = 0;

    constructor(private translate: CustomTranslateService,
                private snackBar: SnackBarService,
                private service: ScoresListService
            ) {
        // initialization
        translate.get('datapoints.energy_score').subscribe((t) => {
            this.energyScoreTranslation = t;
        });
        this.get();
    }

    public ngOnChanges(changes: SimpleChanges) {
        const energyScoreChange = changes['energyScoreEnabled'];
        if (energyScoreChange && energyScoreChange.currentValue !==  undefined  && this.data) {
            this.setEnergyScoreVisibility(this.energyScoreEnabled);
        }
    }

    public onChangeOption(id: number) {
        this.modelData = Number(id);
        this.modelDataChange.emit(this.modelData);
    }

    private get() {
        this.loading = true;
        this.service
            .get()
            .subscribe((scores: ScoreListItem[]) => {
                this.loading = false;
                this.data = scores;
                if (scores != null) {
                    this.data.forEach((score) => {
                        score.enabled = true;
                    });
                }
                this.setEnergyScoreVisibility(this.energyScoreEnabled);
            },
            (err) => {
                this.snackBar.open('data.error', 'form.actions.close');
                this.loading = false;
            });
    }

    private setEnergyScoreVisibility(enable: boolean) {
        if (enable) {
            const scores = [
                {
                    id: this.energyScoreID,
                    name: this.energyScoreTranslation,
                    default: false,                
                    enabled: true
                },
                ...this.data
            ];                  
            
            this.data = scores.sort((a, b) => {
                return a.default ? -1 : 1;
            });

            // find if there is default score definition
            const findDefault = this.data.find((x) => x.default);
            
            // if there it's true make it by default selected
            if (findDefault) {
                this.onChangeOption(findDefault.id);
            }

        } else {
            this.data = this.data.filter((x) => {
                return x.id !== this.energyScoreID; 
            });

            if ((this.modelData === undefined || this.modelData === null) && this.data.length > 0) {                
                const toSelect =  this.data[0].id;
                this.onChangeOption(toSelect);
            }
            if (this.energyScoreEnabled === false && this.modelData === 0 && this.data.length > 0) {                
                const toSelect = this.data.find((d) => d.id > 0).id;
                this.onChangeOption(toSelect);
            }
        }
    }
}
