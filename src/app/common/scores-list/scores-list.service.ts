
// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES 
import { ScoreListItem } from './scores-list-types';

@Injectable()
export class ScoresListService {
    private baseApiURL: string;
    
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseApiURL = this.config.get('apiUrl') + '/api/ScoresList';
        }

    public get(): Observable<ScoreListItem[]> {
        return this.authHttp.get(this.baseApiURL);
    }
}
