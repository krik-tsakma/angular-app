// FRAMEWORK
import {
    AfterViewInit, Component, Input, Output,
    EventEmitter, ChangeDetectorRef
} from '@angular/core';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
import { DistributionTypes, DistributionTypeOption, DistributionTypeOptions } from './distribution-types';

@Component({
    selector: 'distribution-types',
    templateUrl: './distribution-types.html'
})

export class DistributionTypesComponent implements AfterViewInit {
    @Input() public selected?: DistributionTypes;
    @Input() public enabled?: DistributionTypes[];
  
    // Callback function on group change
    @Output() public notifyOnChange: EventEmitter<DistributionTypes> =
        new EventEmitter<DistributionTypes>();

    public isDriver: boolean;
    public isManager: boolean;
    public options: DistributionTypeOption[] = DistributionTypeOptions;

    constructor(private cdr: ChangeDetectorRef,
                private translate: CustomTranslateService) {
        
                    // initialization
        this.options.forEach((element) => {
            element.name = this.translate.instant(element.term);
            element.enabled = true;
        });
    }

    public ngAfterViewInit() {
        if (this.enabled) {
            this.options.forEach((element) => {
                element.enabled = true;
                if (this.enabled.find((x) => x as DistributionTypes === element.id ) === undefined) {
                    element.enabled = false;
                }
            });
        } 

        if (!this.selected) {
            this.selected = this.options.find((x) => x.enabled === true).id;
        }
        // manually trigger change detection for the current component.
        // otherwise "Expression ___ has changed after it was checked" error
        this.cdr.detectChanges();

    }

    public onChangeOption(event) {
        this.selected = Number(event);
        this.notifyOnChange.emit(event);
    }

    public isEnabled(type: DistributionTypes) {
        if (this.enabled && !this.enabled.find((x) => x === type)) {
            return false;
        }
        return true;
    }
}
