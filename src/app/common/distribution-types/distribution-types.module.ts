// FRAMEWORK
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { DistributionTypesComponent } from './distribution-types.component';

@NgModule({
    imports: [
        SharedModule      
    ],
    declarations: [
        DistributionTypesComponent
    ],
    exports: [
        DistributionTypesComponent
    ]
})
export class DistributionTypesModule {}
