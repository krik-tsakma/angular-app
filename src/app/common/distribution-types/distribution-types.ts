import { KeyName } from '../key-name';

export enum DistributionTypes {
    NumOfTrips = 0,
    Distance = 1,
    Duration = 2
}

export interface DistributionTypeOption extends KeyName {
    term: string;
}

export let DistributionTypeOptions: DistributionTypeOption[] = [
    {
        id: DistributionTypes.NumOfTrips,
        term: 'distribution_types.num_of_trips',
        enabled: true,
        name: ''
    },
    {
        id: DistributionTypes.Distance,
        term: 'distribution_types.distance',
        enabled: true,
        name: ''
    },
    {
        id: DistributionTypes.Duration,
        term: 'distribution_types.duration',
        enabled: true,
        name: ''
    }
];
