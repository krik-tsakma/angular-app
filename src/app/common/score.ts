/** Used to represent a score object. */
export class Score {
    /** The id of the score i.e. 0 */
    public id: number;
    /** The value of the score i.e 5 */
    public value: number;
    /** The normalized value of the score i.e 5 */
    public normalized: number;
    /** The alphabetical letter of the score i.e C */
    public letter: string;
    /** The name of the score i.e 5 */
    public name: string;
}

/** Used to represent a score name object. */
export class ScoreName {
    /** The id of the score i.e. 0 */
    public id: number | string;
    /** The name of the score i.e 5 */
    public name: string;

    public enabled?: boolean;
}

/** Used to define the score display options. */
export enum ScoreFormatter {
    all,
    letter,
    value
}

export class ScoreColorMap {

    // CAUTION: make sure aligned with styles/variables.less
    public getForLetter(letter: string): string {

        switch (letter) {
            case 'A' :
                return '#00A74E';
            case 'B' :
                return '#4BBA42';
            case 'C' :
                return '#B3CA15';
            case 'D' :
                return '#FFD80F';
            case 'E' :
                return '#FEBA00';
            case 'F' :
                return '#F57000';
            case 'G' :
                return '#E00001';
            case 'H' :
                return '#A10202';     
            default:
                return '';       
        }
    }

}


export class DataPointColorMap {

    // CAUTION: make sure aligned with styles/variables.less
    public getNormalized(normalized: number): string {

        if (normalized >= 75) {
            return '#00A74E';
        } else if (normalized >= 50) {
            return '#4BBA42';
        } else if (normalized >= 25) {
            return '#B3CA15';
        } else if (normalized >= 0) {
            return '#FFD80F';
        } else if (normalized >= -25) {
            return '#FEBA00';
        } else if (normalized >= -50) {
            return '#F57000';
        } else if (normalized >= -25) {
            return '#E00001';
        } else {
            return '#A10202';
        }
    }
}

export class ScoreBuildUpChartItem {
    public datapointID: number;
    public datapointName: string;
    public targetDeviation?: number;
    public weight?: number;
    public letter?: string;
    public color: string;
    public referenceTargetDeviation: number;
    public referenceTargetDeviationColor: string;
}
