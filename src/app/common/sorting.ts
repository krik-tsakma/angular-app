
/** Used to represent sorting data request. */
export class Sorting {
    public column: string;
    public order: string;

    /** set the sort column and the sort order */
    public set(sortColumn: string, sortOrder: string) {
        this.column = sortColumn;
        this.order = sortOrder;
    }

    /** The parameter to sort the requested data and sort order. i.e. "StartTimestamp asc" */
    public splitAndSet(sorting: string) {
        if (!sorting) {
            return;
        }
        const sort = sorting.split(' ');

        this.column = sort[0];
        this.order = sort[1];
    }

    /** Get the sort column */
    public get(): string {
         return this.column + ' ' + this.order;
    }
}

export class SortingOrder {
    public static readonly asc = 'asc';
    public static readonly desc = 'desc';
}
