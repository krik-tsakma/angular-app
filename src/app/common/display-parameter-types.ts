export enum DisplayParameterTypes {
    string,
    distance,
    speed,
    date,
    time,
    timeAndPerc,
    percentage,
    counter,
    counterNormalized,
    score,
    energyMJ,
    energyMJNormalized,
    energyGJ,
    energyGJNormalized,
    emission
}
