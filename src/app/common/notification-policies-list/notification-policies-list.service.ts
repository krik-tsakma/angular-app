
// FRAMEWORK
import { Injectable } from '@angular/core';

// RxJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import { NotificationPolicyListItem } from './notification-policies-list-types';

@Injectable()
export class NotificationPoliciesListService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/NotificationPoliciesList';
        }


    // Get all notification policies
    public get(): Observable<NotificationPolicyListItem[]> {
    
        return this.authHttp
            .get(this.baseURL);
    }
} 
