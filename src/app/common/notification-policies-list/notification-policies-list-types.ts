import { NotificationPolicyItem } from '../../admin/company/notification-policies/notification-policies.types';

export interface NotificationPolicyListItem extends NotificationPolicyItem {
   enabled: boolean;
}
