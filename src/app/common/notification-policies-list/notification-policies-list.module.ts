// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { NotificationPoliciesListService } from './notification-policies-list.service';

// COMPONENTS
import { NotificationPoliciesListComponent } from './notification-policies-list.component';

@NgModule({
    imports: [
        SharedModule      
    ],
    declarations: [
        NotificationPoliciesListComponent
    ],
    exports: [
        NotificationPoliciesListComponent
    ],
    providers: [
        NotificationPoliciesListService
    ]
})
export class NotificationPoliciesListModule {}
