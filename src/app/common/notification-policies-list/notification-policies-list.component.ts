// FRAMEWORK
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

// SERVICES
import { SnackBarService } from '../../shared/snackbar.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { NotificationPoliciesListService } from './notification-policies-list.service';

// TYPES
import { NotificationPolicyListItem } from './notification-policies-list-types';

@Component({
    selector: 'notification-policies-list',
    templateUrl: './notification-policies-list.html'
})

export class NotificationPoliciesListComponent implements OnInit {
    @Input() public showNoneOption?: boolean;
    @Input() public modelData?: number;
    @Output() public modelDataChange: EventEmitter<number> =
        new EventEmitter<number>();
    @Output() public onLoad: EventEmitter<NotificationPolicyListItem[]> =
        new EventEmitter<NotificationPolicyListItem[]>();
    
    public data: NotificationPolicyListItem[];
    public loading: boolean;

    constructor(private snackBar: SnackBarService,
                private translator: CustomTranslateService,
                private service: NotificationPoliciesListService
            ) {
        // foo
    }

    public ngOnInit(): void {
        this.get();
    }

    public onChangeOption(id?: number) {
        this.modelData = id ? Number(id) : null;
        this.modelDataChange.emit(this.modelData);
    }

    private get() {
        this.loading = true;
        this.service
            .get()            
            .subscribe((res: NotificationPolicyListItem[]) => {
                this.loading = false;
                this.data = res.map((r) => {
                    r.enabled = true;
                    return r;
                });
                 
                this.onLoad.emit( this.data);

                if (this.showNoneOption === true) {
                    this.data.unshift({ 
                        name: this.translator.instant('form.actions.none'),
                        isDefault: false
                    } as NotificationPolicyListItem);
                }

                // if none selected option was provided select the first one
                if (this.data && this.data.length > 0 && (this.modelData === undefined || this.modelData === null)) {
                    this.modelData = this.data[0].id;
                }
            },
            (err) => {
                this.snackBar.open('data.error', 'form.actions.close');
                this.loading = false;
            });
    }

}
