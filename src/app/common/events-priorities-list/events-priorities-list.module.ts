﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { EventPrioritiesService } from './events-priorities.service';

// COMPONENTS
import { EventsPrioritiesListComponent } from './events-priorities-list.component';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        EventsPrioritiesListComponent
    ],
    entryComponents: [
        EventsPrioritiesListComponent
    ],
    exports: [
        EventsPrioritiesListComponent
    ],
    providers: [
        EventPrioritiesService
    ]
})
export class EventsPrioritiesListModule { 

}
