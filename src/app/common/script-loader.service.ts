// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';

@Injectable()
export class ScriptLoaderService {
    private scripts: string[];
    private callback: any;
    private index: number;

    constructor() { 
        // foo
    }

    public load(scriptsSrc: string[], callback?: any) {
        this.scripts = scriptsSrc;
        this.callback = callback;

        this.index = 0;
        this.loadJS(this.scripts[this.index]);
    }

    /**
     *  Add a script tag into the head html tag
     */
    private loadJS(src) {
        const that = this;
        // do not add this script if it is already included to the page
        if (this.isScriptAlreadyIncluded(src) === true) {
            // this script was already loaded. continue to the next
            setTimeout(() => {
                 that.onScriptLoad();
            }, 500);
        } else {
            // Adding the script tag to the head as suggested before
            const head = document.getElementsByTagName('head')[0];
            const script = document.createElement('script');
            script.type = 'text/javascript';
            script.async = true;
            script.defer = true;
            script.src = src;

            // Then bind the event to the callback function.
            // There are several events for cross browser compatibility.
            const innerThat = this;
            script.onload = () => {
                setTimeout(() => {
                    innerThat.onScriptLoad();
                }, 500);
            };

            // Fire the loading
            head.appendChild(script);
        }
    }

    /**
     * When a script has finished loading
     */
    private onScriptLoad() {
        // when the last js file completed loading, fetch the next
        this.index++;
        if (this.index < this.scripts.length) {
            this.loadJS(this.scripts[this.index]);
        } else {
            // we finished loading. call the callback function 
            if (this.callback) {
                this.callback();
            }
        }
    }


    /**
     * Check whether or not a script tag already exists in the document
     * @param src The url of the script to check.
     */
    private isScriptAlreadyIncluded(src) {
        const scripts: any = document.getElementsByTagName('script');
        for (const value of scripts) {
            if (value.getAttribute('src') === src) {
                return true;
            }
        }
        return false;
    }
}
