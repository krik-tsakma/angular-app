import { KeyName } from './../../key-name';

// FRAMEWORK
import { 
    Component, OnInit, OnChanges, ViewEncapsulation, 
    Input, SimpleChanges, Output, EventEmitter
} from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { QuickFiltersService } from '../quick-filters.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { QuickFilterColumnsService } from '../columns/quick-filter-columns.service';
import { Config } from '../../../config/config';

// TYPES
import {
    QuickFilter, QuickFiltersEntities, QuickFiltersChangeItem 
} from '../quick-filters-types';
import { AssetTypes } from '../../entities/entity-types';
import { EntityTypes } from '../../../shared/entity-types/entity-types';
import { AssetGroupTypes, AssetGroupItem } from '../../../admin/asset-groups/asset-groups.types';
import { QuickFilterColumn } from '../columns/quick-filter-column.types';
import { VehicleTypeListItem } from '../../vehicle-types-list/vehicle-type-list.types';
import { QuickFiltersColumnsChangeItem } from '../columns/quick-filter-column.types';

@Component({
    selector: 'toolbar',
    templateUrl: './toolbar.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./toolbar.less']
})

export class ToolbarComponent implements OnInit, OnChanges {
    @Input() public entityID: number;
    @Input() public showSaveOption: boolean;
    @Input() public params: QuickFilter;
    @Input() public sorting: string;
    @Output() public requestChange: EventEmitter<QuickFiltersChangeItem> = new EventEmitter<QuickFiltersChangeItem>();
    public request: QuickFilter = {} as QuickFilter;

    public userRoleDefault: QuickFilter;
    public qfData: QuickFilter[] = null;
    public loading: boolean;

    // asset groups
    public showGroups: boolean;
    public groupTypes = AssetGroupTypes;
    public entityOption: EntityTypes;
    public entityTypes: any = EntityTypes;
    public entityTypesData: EntityTypes[] = [
        EntityTypes.Me, 
        EntityTypes.Drivers,
        EntityTypes.VehicleTypes
    ];
    public vehicleTypes: VehicleTypeListItem[];
    
    constructor(private translate: CustomTranslateService,
                private router: Router,
                private service: QuickFiltersService,
                private userOptions: UserOptionsService,
                private qfcs: QuickFilterColumnsService,
                private configService: Config) {
                    qfcs.columnsChanged.subscribe((columns: QuickFilterColumn[]) => {
                        this.emitQuickFilterChange(columns);
                    });
    }


    // =========================
    // ANGULAR LIFECYCLE
    // =========================

    public ngOnChanges(changes: SimpleChanges): void {
        const entityChange = changes['entityID'];
        if (entityChange && entityChange.currentValue) {
            switch (entityChange.currentValue) {
                case QuickFiltersEntities.FollowUp:
                    this.showGroups = true;
                    this.entityOption = EntityTypes.Drivers;
                    this.initializeOptionsForEntity(this.entityOption, null, null);
                    break;
                case QuickFiltersEntities.Trips:
                    this.showGroups = false;
                    break;
            }
        }
    }

    public ngOnInit(): void {
        // fetch quick filters
        this.getUserDefaults().then(() => {
            this.get(this.params);
        });
    }
    

    // =========================
    // CRUD CALLBACKS
    // =========================
    
    // Navigate to quick-filters page
    public onEditQuickFilters() {
        this.router.navigate(['/quick-filters', this.entityID], { skipLocationChange: !this.configService.get('router') });
    }

    public onCreateQuickFilters() {
        this.router.navigate(['/quick-filters', this.entityID, 'add'], { skipLocationChange: !this.configService.get('router') });
    }


    // =========================
    // ENTITY TYPES CALLBACKS
    // =========================

    public onChangeEntityType(newType: EntityTypes): void {
        this.initializeOptionsForEntity(newType, null, null);
        this.emitQuickFilterChange(null);
    }


    // =========================
    // ASSET GROUP CALLBACKS
    // =========================

    // When the user selects a driver group from the DDL
    public onChangeGroup(newGroup?: AssetGroupItem): void {
        this.request.id = null;
        this.request.assetID = null;
        this.request.groupID = newGroup
            ? newGroup.id
            : null;

        this.emitQuickFilterChange(null);
    }


    // =========================
    // VEHICLE TYPE CALLBACKS
    // =========================

    public onLoadVehicleTypes(vehicleTypes: VehicleTypeListItem[]) {
        this.vehicleTypes = vehicleTypes;
        if (this.request.vehicleTypeID) {
            this.onChangeVehicleType({ id: this.request.vehicleTypeID } as KeyName);
        }
    }

    // When the user selects a driver group from the DDL
    public onChangeVehicleType(vehicleType: KeyName): void {
        this.request.id = null;
        this.request.assetID = null;
        this.request.groupID = null;
        this.request.vehicleTypeID = vehicleType && vehicleType.id ? Number(vehicleType.id) : null;
        this.setEnergyUnit(vehicleType.id);

        this.emitQuickFilterChange(null);
    }

    // =========================
    // QUICK FILTERS CALLBACKS
    // =========================

    public onColumnChange(item: QuickFiltersColumnsChangeItem) {
        this.request.id = null;
        this.request.scoreIDs = item.scoreIDs;
        this.request.propertyIDs = item.propertyIDs;
    }

    // Select and apply a filter (on button click)
    public onSelectQuickFilter(qf: QuickFilter) {
        if (this.request.id === qf.id) {
            this.useRoleDefaults();
            this.request.id = null;
        } else {
            this.request = Object.assign({}, qf);
        }
        this.setEntityOptionFromAssetType();
       
    }

    // Select and apply a filter (on ddl change)
    public onSelectQuickFilterDDL(quickFilterID?: number) {
        if (quickFilterID) {
            const qf = this.qfData.find((f) => f.id === quickFilterID);
            this.request = Object.assign({}, qf);
        } else {
            this.useRoleDefaults();
            this.request.id = null;
        }
        this.setEntityOptionFromAssetType();
    }

    // =========================
    // DATA
    // =========================
    private getUserDefaults(): Promise<void> {
        return new Promise((resolve, reject) => {
        this.loading = true;
        this.service
            .getUserDefault(this.entityID)
            .subscribe((data: QuickFilter) => {
                this.loading = false;
                this.userRoleDefault = data;
                resolve();
            }, (err) => {
                reject();
            });
        });
    }
    // Fetch quick filters function
    private get(params: QuickFilter) {
        this.loading = true;
        this.service
            .getAll(this.entityID)
            .subscribe((data: QuickFilter[]) => {
                this.loading = false;
                this.qfData = data;
                this.qfData.forEach((f) => {
                    f.enabled = true;
                });
                if (this.qfData.length > 0) {
                    // add a none option at the top of the list (mobile use only with ddl)
                    this.qfData.unshift({
                        entityID: this.entityID,
                        assetID: null,
                        groupID: null,
                        vehicleTypeID: null,
                        groupName: null,
                        isDefault: false,
                        id: null,
                        name: this.translate.instant('form.actions.none'),
                        enabled: true,
                        assetType: AssetTypes.HR
                    });
                } else {
                    this.useRoleDefaults();
                }
                // if quick filters is not set and we have a default
                const qfDefault = this.qfData.find((q) => q.isDefault === true);
                if ((!params || Object.keys(params).length === 0)) {
                    if (qfDefault) {
                        this.onSelectQuickFilter(qfDefault);
                    } else {
                        this.useRoleDefaults();
                    }
                } else {
                    this.request = Object.assign({}, params);
                    this.setEntityOptionFromAssetType();
                    this.emitQuickFilterChange(null);
                }
            });
    }

    // =========================
    // HELPERS
    // =========================

    private initializeOptionsForEntity(entityType: EntityTypes, groupID?: number, vehicleTypeID?: number) {
        this.entityOption = entityType ? Number(entityType) : EntityTypes.Drivers;
        // when this is driver user driver settings
        switch (this.entityOption) {
            case EntityTypes.Me: 
                this.request.assetID = this.userOptions.getUser().driverId;
                this.request.assetType = AssetTypes.HR;
                this.request.groupID = null;
                this.request.vehicleTypeID = null;
                this.request.energyUnit = null;
                break;
            case EntityTypes.Drivers:
                this.request.assetType = AssetTypes.HR;
                this.request.groupID = groupID; 
                this.request.assetID = null;
                this.request.vehicleTypeID = null; 
                this.request.energyUnit = null;
                break;
            case EntityTypes.VehicleTypes:
                this.request.assetType = AssetTypes.Vehicle; 
                this.request.assetID = null;
                this.request.groupID = null; 
                this.request.vehicleTypeID = vehicleTypeID;
                this.setEnergyUnit(vehicleTypeID);
                break;
            default:
                break;

        }
    }

    private setEntityOptionFromAssetType() {
        switch (this.request.assetType) {
            case AssetTypes.HR:
                if (this.request.assetID) {
                    this.entityOption = EntityTypes.Me;
                } else {
                    this.entityOption = EntityTypes.Drivers;
                }
                break;
            case AssetTypes.Vehicle:
                if (this.request.vehicleTypeID) {
                    this.entityOption = EntityTypes.VehicleTypes;
                    if (this.request.vehicleTypeID) {
                        this.setEnergyUnit(this.request.vehicleTypeID);
                    }
                } else {
                    this.entityOption = EntityTypes.Vehicles;
                }
                break;
            default:
                break;
        }
    }

    private setEnergyUnit(vehicleTypeID?: number) {
        this.request.energyUnit = null;
        if (!this.vehicleTypes || this.vehicleTypes.length === 0) {
            return;
        }
        if (vehicleTypeID !== null && isNaN(vehicleTypeID) === false) {
            const vt =  this.vehicleTypes.find((x) => x.id === Number(vehicleTypeID));
            if (vt) {
                this.request.energyUnit  = vt.energyUnit;
            }
        }
    }

    private useRoleDefaults() {
        this.request = Object.assign({}, this.userRoleDefault);
    }

    private emitQuickFilterChange(cols: QuickFilterColumn[]) {
        // if qfData is null then we re still loading quick filters
        if (this.qfData === null) {
            return;
        }
        
        // ensure asset type exists in follow up
        if (this.entityID === QuickFiltersEntities.FollowUp 
            && (this.request.assetType === undefined || this.request.assetType === null)
        ) {
            this.request.assetType = AssetTypes.HR;
        }

        this.requestChange.emit({ 
            request: this.request, 
            columns: cols 
        } as QuickFiltersChangeItem);
    }
}
