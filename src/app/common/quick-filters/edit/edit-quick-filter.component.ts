// FRAMEWORK
import {
    Component, ViewChild, OnInit,
    OnDestroy, ViewEncapsulation
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// SERVICES
import { QuickFiltersService } from '../quick-filters.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { Config } from '../../../config/config';

// TYPES
import { AssetGroupItem, AssetGroupTypes } from '../../../admin/asset-groups/asset-groups.types';
import { EntityTypes } from '../../../shared/entity-types/entity-types';
import { User } from '../../../auth/user';
import { AssetTypes } from '../../entities/entity-types';
import {
    EditQuickFilterResults, QuickFilter,
    QuickFiltersEntities, QuickFilterEntityTypes
} from '../quick-filters-types';
import { QuickFiltersColumnsChangeItem } from '../columns/quick-filter-column.types';
import { NgForm } from '@angular/forms';
import { KeyName } from './../../key-name';


@Component({
    selector: 'edit-quick-filter',
    templateUrl: './edit-quick-filter.html',
    encapsulation: ViewEncapsulation.None,
    providers: [QuickFiltersService],
    styleUrls: ['./edit-quick-filter.less']
})

export class EditQuickFilterComponent implements OnInit, OnDestroy {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public qfEntityTypes = QuickFilterEntityTypes;
    public entityTypes = EntityTypes;
    public groupTypes = AssetGroupTypes;
    public item: QuickFilter = {} as QuickFilter;
    public action: string;
    public showGroups: boolean;
    public entityOption: EntityTypes;
    
    private user: User;
    private sub: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private service: QuickFiltersService,
        private userOptions: UserOptionsService,
        private translate: CustomTranslateService,
        private snackBar: SnackBarService,
        private configService: Config) {
            this.user = this.userOptions.getUser();
    }

    public ngOnInit(): void {
        this.sub = this.activatedRoute
            .params
            .subscribe((params: {
                entityID: QuickFiltersEntities;
                editMode: string;
                id?: number;
                pids?: string;
                sids?: string;
            }) => {
            this.item.entityID = Number(params.entityID);
            this.action = params.editMode === 'add'
                ? 'form.actions.create'
                : 'form.actions.edit';
            
            if (params.id > 0) {
                this.get(params.id);
            } else {
                this.item.propertyIDs = params.pids ? params.pids.split(',').map(Number) : [];
                this.item.scoreIDs = params.sids ? params.sids.split(',').map(Number) : [];
            }

            this.showGroups = this.item.entityID === Number(QuickFiltersEntities.FollowUp);
            if (this.showGroups) {
                this.entityOption = this.user.isDriver ?  EntityTypes.Me :  EntityTypes.Drivers;
            }
        });
    }


    public ngOnDestroy() {
        // Clean sub to avoid memory leak
        this.sub.unsubscribe();
    }

    public back() {
        this.router.navigate(['/quick-filters',  this.item.entityID], { skipLocationChange: !this.configService.get('router') });
    }

    // =========================
    // FORM
    // =========================    
    public onSubmit({ value, valid }: { value: QuickFilter, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }

        // This is create
        this.loading = true;
        if (typeof (this.item.id) === 'undefined' || this.item.id === 0) {
            this.service.create(this.item).subscribe((res) => {
                this.loading = false;
                if (res !== EditQuickFilterResults.Ok) {
                    const errorMessage = this.getEditResult(res);
                    this.snackBar.open(errorMessage, 'form.actions.close');
                } else {
                    this.back();
                }
            });
        } else { // This is an update
            this.loading = false;
            this.service.update(this.item).subscribe((res) => {
                if (res !== EditQuickFilterResults.Ok) {
                    const errorMessage = this.getEditResult(res);
                    this.snackBar.open(errorMessage, 'form.actions.close');
                } else {
                    this.back();
                }
            });
        }
    }

    // =========================
    // ENTITY TYPES CALLBACKS
    // =========================

    public onChangeEntityType(newType: EntityTypes): void {
        this.entityOption = Number(newType);
        if (this.entityOption === EntityTypes.Me) {
            this.item.assetType = AssetTypes.HR;
            this.item.assetID = this.user.driverId;
        } else {
            this.item.groupID = null;
            this.item.assetID = null;
            this.item.vehicleTypeID = null;
            switch (this.entityOption) {
                case EntityTypes.Drivers:
                    this.item.assetType = AssetTypes.HR;
                    break;
                case EntityTypes.VehicleTypes:
                    this.item.assetType = AssetTypes.Vehicle;
                    break;
            }
        }
    }


    // =========================
    // DRIVER GROUPS CALLBACKS
    // =========================

    // when the user selects a driver group from the DDL
    public onChangeTargetGroup(newGroup: AssetGroupItem): void {
        this.item.assetID = null;
        this.item.vehicleTypeID = null;
        this.item.groupID = newGroup
            ? newGroup.id
            : null;
        this.item.assetType = AssetTypes.HR;
    }

    // =========================
    // VEHICLE TYPES CALLBACKS
    // =========================

    // When the user selects a driver group from the DDL
    public onChangeVehicleType(vehicleType: KeyName): void {
        this.item.assetID = null;
        this.item.groupID = null;
        this.item.vehicleTypeID = vehicleType && vehicleType.id ? Number(vehicleType.id) : null;  
        this.item.assetType = AssetTypes.Vehicle;
    }

    
    // ===============================
    // QUICK FILTER COLUMNS CALLBACKS
    // ===============================

    public onColumnChange(item: QuickFiltersColumnsChangeItem) {
        this.item.scoreIDs = item.scoreIDs;
        this.item.propertyIDs = item.propertyIDs;
    }



    // =========================
    // DATA
    // =========================

    private get(id: number) {
            this.service
                .getByID(id)
                .subscribe((data) => { 
                    this.loading = false;
                    this.item = data;
                    if (this.item.assetID) {
                        this.entityOption = EntityTypes.Me;
                    } else {
                        this.entityOption = (this.item.assetType === undefined 
                                            || this.item.assetType === null 
                                            || this.item.assetType === AssetTypes.HR) ? 
                                        EntityTypes.Drivers : EntityTypes.VehicleTypes;
                }
        });
    }

    private getEditResult(code: EditQuickFilterResults): string {
        let message: string;
        switch (code) {
            case EditQuickFilterResults.NotFound:
                message = 'form.errors.not_found';
                break;
            case EditQuickFilterResults.SameNameExists:
                message = 'form.errors.name_exists';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
