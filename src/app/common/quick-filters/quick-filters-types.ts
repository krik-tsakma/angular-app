import { QuickFilterColumn } from './columns/quick-filter-column.types';
import { ScoreName } from '../score';
import { DisplayParameterTypes } from '../display-parameter-types';
import { EntityTypes } from '../../shared/entity-types/entity-types';
import { KeyName } from '../key-name';
import { AssetTypes } from '../entities/entity-types';
import { EnumTranslation } from '../enum-translation';
import { EnergyUnitOptions } from '../energy-types/energy-types';

export interface QuickFilter extends KeyName {
    entityID: number;
    id: number;
    name: string;
    assetID?: number;
    groupID?: number;
    vehicleTypeID?: number;
    assetType: AssetTypes;
    groupName: string;
    propertyIDs?: number[];
    propertiesDescription?: string;
    scoreIDs?: number[];
    scoresDescription?: string;
    isDefault: boolean;
    energyUnit?: EnergyUnitOptions; // TO BE REMOVED

}

export enum EditQuickFilterResults {
    NotFound,
    Ok,
    SameNameExists,
    Unknown
}


export enum QuickFiltersEntities {
    FollowUp = 1,
    Trips = 2
}

export let QuickFilterEntityTypes: EntityTypes[] = [
    EntityTypes.Me,
    EntityTypes.Drivers,
    EntityTypes.VehicleTypes
];


export interface QuickFiltersChangeItem {
    columns: QuickFilterColumn[];
    request: QuickFilter;
}
