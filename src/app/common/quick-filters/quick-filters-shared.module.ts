
// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { AssetGroupsModule } from '../asset-groups/asset-groups.module';
import { SharedModule } from '../../shared/shared.module';
import { QuickFilterColumnsModule } from './columns/quick-filter-columns.module';
import { VehicleTypesListModule } from '../vehicle-types-list/vehicle-types-list.module';

// COMPONENTS
import { ToolbarComponent } from './toolbar/toolbar.component';

// SERVICES
import { QuickFilterColumnsService } from './columns/quick-filter-columns.service';
import { QuickFiltersService } from './quick-filters.service';

@NgModule({
    imports: [
        SharedModule,
        AssetGroupsModule,
        VehicleTypesListModule,
        QuickFilterColumnsModule
    ],
    declarations: [
        ToolbarComponent
    ],
    providers: [
        QuickFiltersService,
        QuickFilterColumnsService,
    ],
    exports: [
        ToolbarComponent
    ]
})
export class QuickFiltersSharedModule {}
