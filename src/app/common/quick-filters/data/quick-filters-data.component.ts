
// FRAMEWORK
import {
    Component, OnInit, OnDestroy,
    ViewEncapsulation
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { QuickFiltersService } from '../quick-filters.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { Config } from '../../../config/config';
import { ScoresListService } from './../../scores-list/scores-list.service';

// TYPES
import {
    QuickFilter,
    EditQuickFilterResults,
    QuickFiltersEntities,
} from '../quick-filters-types';
import {
    TableColumnSetting, TableActionItem,
    TableActionOptions, TableCellFormatOptions
} from '../../../shared/data-table/data-table.types';
import { QuickFilterColumn, FollowUpTableColumnOptions, TripReviewTableColumnOptions } from '../columns/quick-filter-column.types';
import { ConfirmationDialogParams } from '../../../shared/confirm-dialog/confirm-dialog-types';
import { KeyName } from './../../key-name';

@Component({
    selector: 'quick-filters-data',
    templateUrl: './quick-filters-data.html',
    encapsulation: ViewEncapsulation.None,
    providers: [QuickFiltersService],
})

export class QuickFiltersComponent implements OnInit, OnDestroy {
    public loading: boolean;
    public qfData: QuickFilter[];

    public entityID: QuickFiltersEntities;

    public columns: QuickFilterColumn[];
    public tableSettings: TableColumnSetting[];

    private subscriber: Subscription;
    private scoresList: KeyName[] = null;

    constructor(
        public snackBar: SnackBarService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private translate: CustomTranslateService,
        private service: QuickFiltersService,
        private scoresService: ScoresListService,
        private confirmDialogService: ConfirmDialogService,
        private unsubscribe: UnsubscribeService,
        private configService: Config) {

            this.tableSettings = this.getTableSettings();
    }

    public ngOnInit(): void {
        this.subscriber = this.activatedRoute.params.subscribe((routeParams) => {
            if (!routeParams['entityID']) {
                return;
            }
            this.entityID = Number(routeParams['entityID']);
            switch (this.entityID) {
                case QuickFiltersEntities.FollowUp:
                    this.columns = FollowUpTableColumnOptions.slice();
                    break;
                case QuickFiltersEntities.Trips:
                    this.columns = TripReviewTableColumnOptions.slice();
                    break;
            }
            this.getScoresList().then(() => {
                this.getQuickFilterData();
            });
        });
    }

    public ngOnDestroy() {
        // Clean sub to avoid memory leak
        this.unsubscribe.removeSubscription(this.subscriber);
    }

    public back() {
        let backURL: string;
        switch (this.entityID) {
            case QuickFiltersEntities.FollowUp:
                backURL = 'follow-up';
                break;
            case QuickFiltersEntities.Trips:
                backURL = 'trip-review';
                break;
        }
        this.router.navigate([backURL], { skipLocationChange: !this.configService.get('router') });
    }

    public onAddQuickFilter() {
        this.router.navigate([this.router.url, 'add'], { skipLocationChange: !this.configService.get('router') });
    }

    // Navigate to edit-quick-filters page
    public onManageQuickFilter(item: TableActionItem) {
        if (!item) {
            return;
        }

        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.edit:
                this.router.navigate([this.router.url, 'edit', id], { skipLocationChange: !this.configService.get('router') });
                break;
            case TableActionOptions.delete:
                this.openDialog(id);
                break;
            default:
                break;

        }
    }

    public openDialog(id: number) {
        const confirmReq = {
            title: 'form.actions.delete',
            message: 'form.warnings.are_you_sure',
            submitButtonTitle: 'form.actions.delete'
        } as ConfirmationDialogParams;

        this.confirmDialogService
            .confirm(confirmReq)
            .subscribe((result) => {
                if (result === 'yes') {
                    this.onDeleteQuickFilter(id);
                }
            });
    }

    public onDeleteQuickFilter(id: number) {
        this.service.delete(id).subscribe((code) => {
            if (code !== EditQuickFilterResults.Ok) {
                 switch (code) {
                    case EditQuickFilterResults.NotFound:
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                        break;
                    default:
                        this.snackBar.open('form.errors.unknown', 'form.actions.close');
                        break;
                }
            } else {
                this.snackBar.open('form.delete_success', 'form.actions.ok');
                this.getQuickFilterData();
            }
        });
    }

    // Fetch quick filters function
    private getQuickFilterData() {
        this.loading = true;
        this.service.getAll(this.entityID)
            .subscribe((data: QuickFilter[]) => {
                this.qfData = data;
                this.loading = false;
        });
    }

    private getScoresList(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (this.scoresList) {
                resolve();
                return;
            }
            this.loading = true;
            this.scoresService.get()
                .subscribe((data: KeyName[]) => {
                    this.loading = false;
                    this.scoresList = data;
                    resolve();
            });
        });
    }

    private getTableSettings() {
        return [
            {
                primaryKey: 'isDefault',
                header: 'quick_filters.default',
                format: TableCellFormatOptions.checkIcon
            },
            {
                primaryKey: 'name',
                header: 'quick_filters.name'
            },
            {
                primaryKey: 'propertyIDs',
                header: 'quick_filters.columns',
                format: TableCellFormatOptions.custom,
                formatFunction: (record: QuickFilter) => {
                    const description = [];
                    record.propertyIDs.forEach((p) => {
                        // Get the term name
                        const name = this.columns.find((x) => x.id === p).header;
                        // Get the translation
                        const term = this.translate.instant(name);
                        description.push(term);
                    });
                    return description.join(', ');
                }
            },
            {
                primaryKey: 'scoreIDs',
                header: 'quick_filters.scores',
                format: TableCellFormatOptions.custom,
                formatFunction: (record: QuickFilter) => {
                    const description = [];
                    record.scoreIDs.forEach((s) => {
                        // Get the term name
                        const scoreName = this.scoresList.find((x) => x.id === s);
                        if (scoreName) {
                            description.push(scoreName.name);
                        }
                    });
                    return description.join(', ');
                }
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete]
            }
        ];
    }
}
