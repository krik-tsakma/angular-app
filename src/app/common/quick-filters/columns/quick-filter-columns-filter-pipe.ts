﻿// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// TYPES
import { QuickFilterColumn, QuickFilterColumnClusters } from './quick-filter-column.types';

@Pipe({
    name: 'myQuickFiltersColumnFilter',
    pure: false
})

export class QuickFiltersColumnFilterPipe implements PipeTransform {
    public transform(columns: QuickFilterColumn[], cluster: QuickFilterColumnClusters): any {
        // filter columns based on their cluster
        return columns.filter((c) => {
            return c.cluster === cluster;
        });
    }
}
