// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Subject } from 'rxjs';

// TYPES
import { QuickFilterColumn } from './quick-filter-column.types';
import { QuickFiltersEntities, QuickFilter } from '../quick-filters-types';

@Injectable()
export class QuickFilterColumnsService {
    public columnsChanged: Subject<QuickFilterColumn[]> = new Subject<QuickFilterColumn[]>();
    
    constructor() {
        // foo
    }
    
    public onChangeOption(columns: QuickFilterColumn[]) {
        const checkedColumns = columns.filter((o) => o.checked === true);
        this.columnsChanged.next(checkedColumns);
    }
}
