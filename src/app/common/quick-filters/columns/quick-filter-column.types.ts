import { QuickFiltersEntities } from '../quick-filters-types';
import { TableColumnSetting, TableColumnSortOrderOptions, TableColumnSortOptions, TableCellFormatOptions } from '../../../shared/data-table/data-table.types';
import { KeyName } from '../../key-name';

export enum QuickFilterColumnClusters {
    trip,
    driveStyle,
    scores
}

export interface QuickFiltersColumnsChangeItem {
    propertyIDs: number[];
    scoreIDs: number[];
}

export interface QuickFilterColumn extends TableColumnSetting {
    id: number;
    cluster: QuickFilterColumnClusters;
    checked?: boolean;
    order?: number;
}

export let FollowUpTableColumnOptions: QuickFilterColumn[] = [
    {
        id: -1,
        primaryKey: 'firstname',
        header: 'follow_up.firstname',
        cluster: QuickFilterColumnClusters.trip,
        checked: true,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
    },
    {
        id: -2,
        primaryKey: 'lastname',
        header: 'follow_up.lastname',
        cluster: QuickFilterColumnClusters.trip,
        checked: true,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: -3,
        primaryKey: 'totalDistance',
        header: 'datapoints.distance',
        cluster: QuickFilterColumnClusters.trip,
        format: TableCellFormatOptions.distance,
        checked: true,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 9,
        primaryKey: 'totalDuration',
        header: 'datapoints.duration',
        cluster: QuickFilterColumnClusters.trip,
        format: TableCellFormatOptions.time,
        checked: true,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 10,
        primaryKey: 'averageSpeed',
        header: 'datapoints.avg_speed',
        cluster: QuickFilterColumnClusters.trip,
        format: TableCellFormatOptions.speed,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 11,
        primaryKey: 'driveTimeNightPerc',
        header: 'datapoints.driving_night_perc_of_time',
        cluster: QuickFilterColumnClusters.trip,
        format: TableCellFormatOptions.percentage,
    },
    {
        id: 1,
        primaryKey: 'groupNamesAll',
        header: 'assets.groups.driver',
        cluster: QuickFilterColumnClusters.trip,
        checked: true,        
    },
    {
        id: 2,
        primaryKey: 'energyUsed',
        header: 'datapoints.energy_used',
        cluster: QuickFilterColumnClusters.trip,
        format: TableCellFormatOptions.energyGJ,
        checked: true, 
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 3,
        primaryKey: 'energyEfficiency',
        header: 'datapoints.energy_efficiency',
        cluster: QuickFilterColumnClusters.trip,
        format: TableCellFormatOptions.energyGJNormalized,
        checked: true,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 4,
        primaryKey: 'energySavings',
        header: 'datapoints.energy_savings',
        cluster: QuickFilterColumnClusters.trip,
        format: TableCellFormatOptions.energyMJ,
        checked: true,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 5,
        primaryKey: 'energySavingsPerc',
        header: 'datapoints.energy_savings_percentage',
        cluster: QuickFilterColumnClusters.trip,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 6,
        primaryKey: 'energySavingsPotential',
        header: 'datapoints.energy_savings_potential',
        cluster: QuickFilterColumnClusters.trip,
        format: TableCellFormatOptions.energyMJ,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 7,
        primaryKey: 'energySavingsPotentialPerc',
        header: 'datapoints.energy_savings_potential_percentage',
        cluster: QuickFilterColumnClusters.trip,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 12,
        primaryKey: 'idleTimePerc',
        header: 'datapoints.idle_time_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 13,
        primaryKey: 'idleTimeAfterGracePeriodPerc',
        header: 'datapoints.idle_time_after_grace_period_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 14,
        primaryKey: 'inefficientAccelerationNumNormalized',
        header: 'datapoints.inefficient_acceleration_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 15,
        primaryKey: 'hardAccelerationNumNormalized',
        header: 'datapoints.hard_acceleration_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 16,
        primaryKey: 'excessiveAccelerationNumNormalized',
        header: 'datapoints.excessive_acceleration_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 17,
        primaryKey: 'inefficientBrakingNumNormalized',
        header: 'datapoints.inefficient_braking_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 18,
        primaryKey: 'hardBrakingNumNormalized',
        header: 'datapoints.hard_braking_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 19,
        primaryKey: 'excessiveBrakingNumNormalized',
        header: 'datapoints.excessive_braking_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 20,
        primaryKey: 'fastLeftCorneringNumNormalized',
        header: 'datapoints.fast_left_cornering_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 21,
        primaryKey: 'fastRightCorneringNumNormalized',
        header: 'datapoints.fast_right_cornering_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 22,
        primaryKey: 'durationVehicleSpeedInefficientPerc',
        header: 'datapoints.duration_vehicle_speed_inefficient_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 23,
        primaryKey: 'durationVehicleSpeedHardPerc',
        header: 'datapoints.duration_vehicle_speed_hard_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage
    },
    {
        id: 24,
        primaryKey: 'durationEngineSpeedInefficientPerc',
        header: 'datapoints.duration_engine_speed_inefficient_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 25,
        primaryKey: 'driveTimeBrakePedalPerc',
        header: 'datapoints.drive_time_brake_pedal_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 26,
        primaryKey: 'driveTimeAccPedalPerc',
        header: 'datapoints.drive_time_acc_pedal_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage
    },
    {
        id: 27,
        primaryKey: 'driveTimeRollingPerc',
        header: 'datapoints.drive_time_rolling_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 28,
        primaryKey: 'durationExcessiveAccelerationPerc',
        header: 'datapoints.duration_excessive_acceleration_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 29,
        primaryKey: 'durationGreenAccelerationPerc',
        header: 'datapoints.duration_green_acceleration_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 30,
        primaryKey: 'durationYellowAccelerationPerc',
        header: 'datapoints.duration_yellow_acceleration_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 31,
        primaryKey: 'durationRedAccelerationPerc',
        header: 'datapoints.duration_red_acceleration_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 32,
        primaryKey: 'durationGreenBrakingPerc',
        header: 'datapoints.duration_green_braking_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 33,
        primaryKey: 'durationYellowBrakingPerc',
        header: 'datapoints.duration_yellow_braking_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage
    },
    {
        id: 34,
        primaryKey: 'durationRedBrakingPerc',
        header: 'datapoints.duration_red_braking_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        format: TableCellFormatOptions.percentage,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 35,
        primaryKey: 'driverAidAlertsEngineSpeedNormalized',
        header: 'datapoints.driver_aid_alerts_engine_speed',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 36,
        primaryKey: 'driverAidAlertsKineticAccelerationNormalized',
        header: 'datapoints.driver_aid_alerts_kinetic_acceleration',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 37,
        primaryKey: 'driverAidAlertsKineticBrakingNormalized',
        header: 'datapoints.driver_aid_alerts_kinetic_braking',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 38,
        primaryKey: 'driverAidAlertsSpeedNormalized',
        header: 'datapoints.driver_aid_alerts_speed',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 39,
        primaryKey: 'driverAidAlertsPedalAccelerationNormalized',
        header: 'datapoints.driver_aid_alerts_pedal_acceleration',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 40,
        primaryKey: 'driverAidAlertsIdlingNormalized',
        header: 'datapoints.driver_aid_alerts_idling',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 41,
        primaryKey: 'regenBrakeNumberNormalized',
        header: 'datapoints.regen_brake_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 42,
        primaryKey: 'mechBrakeNumberNormalized',
        header: 'datapoints.mech_brake_number',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 43,
        primaryKey: 'evEnergyRegenNormalized',
        header: 'datapoints.ev_energy_regen',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 8,
        primaryKey: 'energyScore',
        header: 'datapoints.energy_score',
        cluster: QuickFilterColumnClusters.scores,
        format: TableCellFormatOptions.score,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 's0')
    },
];


export let TripReviewTableColumnOptions: QuickFilterColumn[] = [
    {
        id: 1,
        primaryKey: 'tripType',
        header: 'trip_review.trips.options.trip_type',
        cluster: QuickFilterColumnClusters.trip
    },
    {
        id: -1,
        primaryKey: 'startTimestamp',
        header: 'trip_review.trips.options.start',
        checked: true,
        format: TableCellFormatOptions.dateTime,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
    },
    {
        id: -2,
        primaryKey: 'stopTimestamp',
        header: 'trip_review.trips.options.end',
        checked: true,
        format: TableCellFormatOptions.dateTime,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)

    },
    {
        id: -3,
        primaryKey: 'vehicleLabel',
        header: 'trip_review.trips.options.vehicle',
        checked: true,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: -4,
        primaryKey: 'driverLabel',
        header: 'trip_review.trips.options.driver',
        checked: true,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 2,
        primaryKey: 'distance',
        header: 'datapoints.distance',
        checked: true,
        format: TableCellFormatOptions.distance,
        cluster: QuickFilterColumnClusters.trip ,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'DistanceInMeters')
    },
    {
        id: 3,
        primaryKey: 'durationSecs',
        header: 'datapoints.duration',
        checked: true,
        format: TableCellFormatOptions.time,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'DurationInSeconds')
    },
    {
        id: 4,
        primaryKey: 'startAddress',
        header: 'trip_review.trips.options.start_address',
        format: TableCellFormatOptions.string,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 5,
        primaryKey: 'stopAddress',
        header: 'trip_review.trips.options.end_address',
        format: TableCellFormatOptions.string,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 6,
        primaryKey: 'startOdometer',
        header: 'trip_review.trips.options.start_odometer',
        format: TableCellFormatOptions.distance,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'StartTotalDistanceInMeters')
    },
    {
        id: 7,
        primaryKey: 'stopOdometer',
        header: 'trip_review.trips.options.stop_odometer',
        format: TableCellFormatOptions.distance,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'StopTotalDistanceInMeters')
    },
    {
        id: 8,
        primaryKey: 'avgSpeed',
        header: 'datapoints.avg_speed',
        format: TableCellFormatOptions.speed,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'AverageSpeed')
    },
    {
        id: 9,
        primaryKey: 'maxSpeed',
        header: 'datapoints.max_speed',
        format: TableCellFormatOptions.speed,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 18,
        primaryKey: 'driveTimeNightPerc',
        header: 'datapoints.driving_night_perc_of_time',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 11,
        primaryKey: 'energyUsed',
        header: 'datapoints.energy_used',
        format: TableCellFormatOptions.energyMJ,
        metricInline: true,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'EnergyVolumePrimary')
    },
    {
        id: 12,
        primaryKey: 'energyEfficiency',
        header: 'datapoints.energy_efficiency',
        format: TableCellFormatOptions.energyMJNormalized,
        metricInline: true,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'EfficiencyPrimary')
    },
    {
        id: 13,
        primaryKey: 'energySaved',
        header: 'datapoints.energy_savings',
        cluster: QuickFilterColumnClusters.trip,
        format: TableCellFormatOptions.energyMJ,
        metricInline: true,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'EnergySavedPrimary')
    },
    {
        id: 14,
        primaryKey: 'energySavedPerc',
        header: 'datapoints.energy_savings_percentage',
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'EnergySavedPrimaryNormalized')
    },
    {
        id: 15,
        primaryKey: 'energyPotential',
        header: 'datapoints.energy_savings_potential',
        format: TableCellFormatOptions.energyMJ,
        metricInline: true,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'EnergyWastedPrimary')
    },
    {
        id: 16,
        primaryKey: 'energyPotentialPerc',
        header: 'datapoints.energy_savings_potential_percentage',
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'EnergyWastedPrimaryNormalized')
    },
    {
        id: 17,
        primaryKey: 'cO2Emission',
        header: 'trip_review.trips.options.co2_emission',
        format: TableCellFormatOptions.weight,
        cluster: QuickFilterColumnClusters.trip,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'EmissionPrimary')
    },
    {
        id: 19,
        primaryKey: 'idleTimePerc',
        header: 'datapoints.idle_time_percentage',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 20,
        primaryKey: 'idleTimeAfterGracePeriodPerc',
        header: 'datapoints.idle_time_after_grace_period_percentage',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 21,
        primaryKey: 'inefficientAccelerationNumNormalized',
        header: 'datapoints.inefficient_acceleration_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 22,
        primaryKey: 'hardAccelerationNumNormalized',
        header: 'datapoints.hard_acceleration_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 23,
        primaryKey: 'excessiveAccelerationNumNormalized',
        header: 'datapoints.excessive_acceleration_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 24,
        primaryKey: 'inefficientBrakingNumNormalized',
        header: 'datapoints.inefficient_braking_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 25,
        primaryKey: 'hardBrakingNumNormalized',
        header: 'datapoints.hard_braking_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 26,
        primaryKey: 'excessiveBrakingNumNormalized',
        header: 'datapoints.excessive_braking_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 27,
        primaryKey: 'fastLeftCorneringNumNormalized',
        header: 'datapoints.fast_left_cornering_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 28,
        primaryKey: 'fastRightCorneringNumNormalized',
        header: 'datapoints.fast_right_cornering_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 29,
        primaryKey: 'durationVehicleSpeedInefficientPerc',
        header: 'datapoints.duration_vehicle_speed_inefficient_percentage',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'InefficientVehicleSpeedSecsPerc')
    },
    {
        id: 30,
        primaryKey: 'durationVehicleSpeedHardPerc',
        header: 'datapoints.duration_vehicle_speed_hard_percentage',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'HardVehicleSpeedSecsPerc')
    },
    {
        id: 31,
        primaryKey: 'durationEngineSpeedInefficientPerc',
        header: 'datapoints.duration_engine_speed_inefficient_percentage',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'InefficientEngineSpeedSecsPerc')
    },
    {
        id: 32,
        primaryKey: 'driveTimeBrakePedalPerc',
        header: 'datapoints.drive_time_brake_pedal_percentage',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 33,
        primaryKey: 'driveTimeAccPedalPerc',
        header: 'datapoints.drive_time_acc_pedal_percentage',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 34,
        primaryKey: 'driveTimeRollingPerc',
        header: 'datapoints.drive_time_rolling_percentage',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 35,
        primaryKey: 'durationExcessiveAccelerationPerc',
        header: 'datapoints.duration_excessive_acceleration_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 36,
        primaryKey: 'durationGreenAccelerationPerc',
        header: 'datapoints.duration_green_acceleration_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 37,
        primaryKey: 'durationYellowAccelerationPerc',
        header: 'datapoints.duration_yellow_acceleration_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 38,
        primaryKey: 'durationRedAccelerationPerc',
        header: 'datapoints.duration_red_acceleration_percentage',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 39,
        primaryKey: 'durationGreenBrakingPerc',
        header: 'datapoints.duration_green_braking_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 40,
        primaryKey: 'durationYellowBrakingPerc',
        header: 'datapoints.duration_yellow_braking_percentage',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 41,
        primaryKey: 'durationRedBrakingPerc',
        header: 'datapoints.duration_red_braking_percentage',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 42,
        primaryKey: 'driverAidAlertsEngineSpeedNormalized',
        header: 'datapoints.driver_aid_alerts_engine_speed',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 43,
        primaryKey: 'driverAidAlertsKineticAccelerationNormalized',
        header: 'datapoints.driver_aid_alerts_kinetic_acceleration',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 44,
        primaryKey: 'driverAidAlertsKineticBrakingNormalized',
        header: 'datapoints.driver_aid_alerts_kinetic_braking',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 45,
        primaryKey: 'driverAidAlertsSpeedNormalized',
        header: 'datapoints.driver_aid_alerts_speed',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 46, 
        primaryKey: 'driverAidAlertsPedalAccelerationNormalized',
        header: 'datapoints.driver_aid_alerts_pedal_acceleration',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'DriverAidAlertsThrottleNormalised')
    },
    {
        id: 47,
        primaryKey: 'driverAidAlertsIdlingNormalized',
        header: 'datapoints.driver_aid_alerts_idling',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 48,
        primaryKey: 'regenBrakeNumberNormalized',
        header: 'datapoints.regen_brake_num',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 49,
        primaryKey: 'mechBrakeNumberNormalized',
        header: 'datapoints.mech_brake_number',
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
    },
    {
        id: 50,
        primaryKey: 'evEnergyRegenNormalized',
        header: 'datapoints.ev_energy_regen',
        format: TableCellFormatOptions.percentage,
        cluster: QuickFilterColumnClusters.driveStyle,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 'EVEnergyRegenNormalized')
    },
    {
        id: 10,
        primaryKey: 'energyScore',
        header: 'datapoints.energy_score',
        format: TableCellFormatOptions.score,
        cluster: QuickFilterColumnClusters.scores,
        sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, 's0')
    },
];
