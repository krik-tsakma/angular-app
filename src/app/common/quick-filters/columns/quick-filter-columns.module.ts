
// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';

// SERVICES
import { QuickFilterColumnsService } from './quick-filter-columns.service';
import { ScoresListService } from '../../scores-list/scores-list.service';

// COMPONENTS
import { QuickFilterColumnsComponent } from './quick-filter-columns.component';

// PIPES
import { QuickFiltersColumnFilterPipe } from './quick-filter-columns-filter-pipe';

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        QuickFilterColumnsComponent,
        QuickFiltersColumnFilterPipe
    ],
    exports: [
        QuickFilterColumnsComponent,
        QuickFiltersColumnFilterPipe
    ], 
    providers: [
        ScoresListService,
        QuickFilterColumnsService
    ]
})
export class QuickFilterColumnsModule {}
