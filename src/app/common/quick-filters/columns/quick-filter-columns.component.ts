// FRAMEWORK
import { 
    Component, AfterViewInit, ElementRef, Renderer, ChangeDetectorRef,
    Input, Output, EventEmitter, HostListener, ViewEncapsulation, OnChanges, SimpleChanges
} from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { QuickFilterColumnsService } from './quick-filter-columns.service';
import { ScoresListService } from '../../scores-list/scores-list.service';
import { Config } from '../../../config/config';

// TYPES
import { QuickFilterColumn, TripReviewTableColumnOptions, QuickFiltersColumnsChangeItem } from './quick-filter-column.types';
import { 
    TableColumnSortOptions,
    TableCellFormatOptions, TableColumnSortOrderOptions 
} from '../../../shared/data-table/data-table.types';
import { FollowUpTableColumnOptions, QuickFilterColumnClusters } from './quick-filter-column.types';
import { QuickFiltersEntities } from '../quick-filters-types';
import { KeyName } from '../../key-name';

@Component({
    selector: 'quick-filter-columns',
    templateUrl: './quick-filter-columns.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./quick-filter-columns.less']
})

export class QuickFilterColumnsComponent implements OnChanges, AfterViewInit {
    @Input() public entityID: number;
    @Input() public modalView: boolean;
    @Input() public showSaveOption: boolean;
    @Input() public sorting: string;
    @Input() public propertyIDs: number[];
    @Input() public scoreIDs: number[];

    @Output() public onColumnChange: EventEmitter<QuickFiltersColumnsChangeItem> = new EventEmitter();

    public columns: QuickFilterColumn[];
    public open: boolean;
    public clusters: KeyName[] = [
        {
            id: QuickFilterColumnClusters.trip,
            name: 'quick_filters.cluster.trip'
        },
        {
            id: QuickFilterColumnClusters.driveStyle,
            name: 'quick_filters.cluster.drive_style'
        },
        {
            id: QuickFilterColumnClusters.scores,
            name: 'quick_filters.cluster.scores'
        }
    ];
    private changeHappened: boolean;

    constructor(
        private translate: CustomTranslateService,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private router: Router,
        private cdr: ChangeDetectorRef,
        private scoresService: ScoresListService,
        private qfcs: QuickFilterColumnsService,
        private configService: Config) {
           // foo
    }

    @HostListener('document:click', ['$event', '$event.target'])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
        event.stopPropagation();
        event.cancelBubble = true;

        if (!targetElement || targetElement.id === 'quick-filters-apply-btn') {
            return;
        }

        // decide whether or not we should apply the filters and close the modal views
        if (!this.modalView) {
            return;
        }

        const clickedInside = this.elementRef.nativeElement.contains(targetElement);
        if (!clickedInside && this.open === true) {
            this.onApply();
        }
    }

    public ngOnChanges(changes: SimpleChanges) {
        const entityChange = changes['entityID'];
        if (entityChange && entityChange.currentValue) {
            switch (entityChange.currentValue) {
                case QuickFiltersEntities.FollowUp:
                    this.columns = JSON.parse(JSON.stringify(FollowUpTableColumnOptions)); 
                    break;
                case QuickFiltersEntities.Trips:
                    this.columns = JSON.parse(JSON.stringify(TripReviewTableColumnOptions));
                    break;
            }
            this.getDriveScores();
        }

        const propertiesChange = changes['propertyIDs'];
        if (propertiesChange && propertiesChange.currentValue && propertiesChange.firstChange === false) {
            const pids = propertiesChange.currentValue as number[];
            this.columns.forEach((c) => {
                if (c.cluster !== QuickFilterColumnClusters.scores || c.primaryKey === 'energyScore') {
                    c.checked = false;
                    if (pids && pids.find((p) => p === c.id)) {
                        c.checked = true;
                    }
                }
            });
            this.emitCheckedColumns();
        }

        const scoresChange = changes['scoreIDs'];
        if (scoresChange && scoresChange.currentValue && scoresChange.firstChange === false) {
            const sids = scoresChange.currentValue as number[];
            this.columns.forEach((c) => {
                if (c.cluster === QuickFilterColumnClusters.scores && c.primaryKey !== 'energyScore') {
                    c.checked = false;
                    if (sids && sids.find((p) => p === c.id)) {
                        c.checked = true;
                    }
                }
            });
            this.emitCheckedColumns();
        }
    }

    public ngAfterViewInit() {
        if (!this.columns) {
            return;
        }

        this.columns.forEach((c) => {
            c.checked = false;

            if (c.cluster === QuickFilterColumnClusters.scores
                && c.primaryKey !== 'energyScore'
                && this.scoreIDs
                && this.scoreIDs.find((p) => p === c.id)) {
                c.checked = true;
            } else if (this.propertyIDs 
                    && (c.cluster !== QuickFilterColumnClusters.scores || c.primaryKey === 'energyScore')
                    && this.propertyIDs.find((p) => p === c.id)) {
                    c.checked = true;
            }
        });
        if (this.modalView === true) {
            this.emitCheckedColumns();
        }

        this.cdr.detectChanges();
    }


    public setVisibility(shouldOpen?: boolean): void {
        if (shouldOpen !== undefined) {
            this.open = shouldOpen;
        } else {
            this.open = !this.open;
        }
        this.changeHappened = false;

    }
 
    public toggle(id: number) {
        const cluster = document.getElementById('cluster_' + id);
        if (cluster) {
            const clusterCnt = cluster.getElementsByClassName('cluster-wrapper')[0] as HTMLElement;
            if (clusterCnt) {
                clusterCnt.style.display =  (!clusterCnt.style.display || clusterCnt.style.display === 'flex') ? 'none' : 'flex';
            }
        }
    }

    public onApply() {
        if (this.changeHappened) {
            this.emitCheckedIDsAndColumns();
        }
        this.setVisibility(false);
    }

    // When the user checks/unchecks a property or a score    
    public onChangeOption(column: QuickFilterColumn) {
        column.checked = !column.checked;
        this.changeHappened = true;
        if (this.modalView === false) {
            this.emitCheckedIDsAndColumns();
        } 
    }
    // When the user selected the option to create a new 
    // quick filter based on the current selection of properties and scores
    public onCreateNew() {
        this.router.navigate(['/quick-filters', this.entityID, 'add', { 
            pids: this.propertyIDs.toString(), 
            sids: this.scoreIDs.toString()  
        } ], { skipLocationChange: !this.configService.get('router') });
    }

    private getDriveScores() {
        this.scoresService
            .get()
            .subscribe((scores: KeyName[]) => {
                scores.forEach((s) => {
                    const scoreKey = 's' + s.id;
                    let sortEnabled = false;
                    let sortDirection = TableColumnSortOrderOptions.asc;
                    let check = false;
                    if (this.scoreIDs) {
                        check = this.scoreIDs.find((id) => id === s.id) ? true : false;
                        if (this.sorting) {
                            sortDirection = this.sorting.indexOf('asc') > -1 ? TableColumnSortOrderOptions.asc : TableColumnSortOrderOptions.desc;
                            sortEnabled = this.sorting.indexOf(scoreKey) > -1;

                            if (sortEnabled) {
                                this.columns.forEach((c) => {
                                    if (c.sorting) {
                                        c.sorting.selected = false;
                                    }
                                });
                            }
                        }
                    }
                    const col: QuickFilterColumn =  {
                        id: s.id,
                        primaryKey: scoreKey,
                        header: s.name,
                        checked: check,
                        format: TableCellFormatOptions.score,
                        cluster: QuickFilterColumnClusters.scores,
                        sorting: new TableColumnSortOptions(sortDirection, sortEnabled, null)
                    };
                    this.columns.push(col);
                });
                this.emitCheckedColumns();
            });
    }

    private emitCheckedIDsAndColumns() {
        this.scoreIDs = [];
        this.propertyIDs = [];
        this.columns.forEach((x) => {
            if (x.checked === false) {
                return;
            }
            if (x.cluster === QuickFilterColumnClusters.scores && x.primaryKey !== 'energyScore') {
                this.scoreIDs.push(x.id);
            } else {
                this.propertyIDs.push(x.id);
            }
        });

        this.onColumnChange.emit({
            propertyIDs: this.propertyIDs,
            scoreIDs: this.scoreIDs 
        } as QuickFiltersColumnsChangeItem);
        this.emitCheckedColumns();
    }

    private emitCheckedColumns() {
        this.qfcs.columnsChanged.next(this.columns.filter((x) => x.checked === true));
    }
}
