﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { QuickFiltersSharedModule } from './quick-filters-shared.module';
import { QuickFiltersRoutingModule } from './quick-filters-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { AssetGroupsModule } from '../asset-groups/asset-groups.module';
import { QuickFilterColumnsModule } from './columns/quick-filter-columns.module';
import { VehicleTypesListModule } from '../vehicle-types-list/vehicle-types-list.module';

// COMPONENTS
import { QuickFiltersComponent } from './data/quick-filters-data.component';
import { EditQuickFilterComponent } from './edit/edit-quick-filter.component';

// SERVICES
import { ScoresListService } from './../scores-list/scores-list.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/quick-filters/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        AssetGroupsModule,
        QuickFiltersRoutingModule,
        QuickFiltersSharedModule,
        QuickFilterColumnsModule,
        VehicleTypesListModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        QuickFiltersComponent,
        EditQuickFilterComponent
    ],
    providers: [
        ScoresListService
    ]
})
export class QuickFiltersModule {}
