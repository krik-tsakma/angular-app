﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../../auth/authGuard';

import { QuickFiltersComponent } from './data/quick-filters-data.component';
import { EditQuickFilterComponent } from './edit/edit-quick-filter.component';

const QuickFiltersRoutes: Routes = [
    {
        path: ':entityID',
        component: QuickFiltersComponent,
        canActivate: [AuthGuard]
    },
    {
        path: ':entityID/edit/:id',
        component: EditQuickFilterComponent,
        canActivate: [AuthGuard]
    },
    {
        path: ':entityID/add',
        component: EditQuickFilterComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(QuickFiltersRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class QuickFiltersRoutingModule { }
