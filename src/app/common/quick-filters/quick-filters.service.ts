// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import {
    QuickFilter, EditQuickFilterResults
} from './quick-filters-types';

@Injectable()
export class QuickFiltersService {
    private baseApiURL: string;
    constructor(
        private authHttp: AuthHttp,
        private configService: Config) {
        this.baseApiURL = configService.get('apiUrl') + '/api/QuickFilters';
     }

    public getByID(id: number): Observable<QuickFilter> {
        return this.authHttp
            .get(this.baseApiURL + '/' + id);
    }

    public getAll(entityID: number): Observable<QuickFilter[]> {
        const params = new HttpParams({
            fromObject: {
                EntityID: String(entityID)
            }
        });
        
        return this.authHttp
            .get(this.baseApiURL, { params });
    }

    public getUserDefault(entityID: number): Observable<QuickFilter> {
        return this.authHttp
            .get(this.baseApiURL + '/GetDefaultFilters/' + entityID);
    }


    public create(filter: QuickFilter): Observable<EditQuickFilterResults>  {
        return this.authHttp
            .post(this.baseApiURL, filter, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return EditQuickFilterResults.Ok;
                    } else if (res.status === 200) {
                        const code = res.json() as number;
                        if (code === -1) {
                            return EditQuickFilterResults.SameNameExists;
                        }
                    } else {
                        return EditQuickFilterResults.Unknown;
                    }
                })
            );
    }

    public update(filter: QuickFilter): Observable<EditQuickFilterResults>  {
        return this.authHttp
            .put(this.baseApiURL, filter, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 200) {
                        const code = res.body as number;
                        if (code === 0) {
                            return EditQuickFilterResults.Ok;
                        } else if (code === -1) {
                            return EditQuickFilterResults.SameNameExists;
                        }
                    } else if (res.status === 404) {
                        return EditQuickFilterResults.NotFound;
                    } else {
                        return EditQuickFilterResults.Unknown;
                    }
                })
            ); 
    }

    public delete(id: number): Observable<EditQuickFilterResults>  {
        return this.authHttp
            .delete(this.baseApiURL + '/' + id, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 200) {
                        return EditQuickFilterResults.Ok;
                    } else if (res.status === 404) {
                        return EditQuickFilterResults.NotFound;
                    } else {
                        return EditQuickFilterResults.Unknown;
                    }
                })
            );            
    }
}
