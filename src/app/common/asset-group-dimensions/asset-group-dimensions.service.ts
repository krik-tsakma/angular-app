// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams, HttpHeaders } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { KeyName } from '../key-name';
import { AssetGroupDimensionCreateCodeOptions } from './asset-group-dimensions-types';

@Injectable()
export class AssetGroupDimensionsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/AssetGroupDimensions';
        }

    // Get all company user roles
    public get(): Observable<KeyName[]> {
        return this.authHttp
            .get(this.baseURL);
    }



    public create(label: string): Observable<AssetGroupDimensionCreateCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, { name: label } as KeyName, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as AssetGroupDimensionCreateCodeOptions;
                    }
                })
            );
    }


    // Delete dimension
    public delete(id: number): Observable<number> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((res) => res.status)
            ); 
    }

} 

