
// FRAMEWORK
import { 
    Component, EventEmitter, Input, ElementRef, OnChanges, SimpleChanges,
    OnInit, Output, ViewChild, ViewEncapsulation 
} from '@angular/core';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { AssetGroupDimensionsService } from './asset-group-dimensions.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { ConfirmDialogService } from '../../shared/confirm-dialog/confirm-dialog.service';

// TYPES
import { KeyName } from '../key-name';
import { ConfirmationDialogParams } from '../../shared/confirm-dialog/confirm-dialog-types';
import { AssetGroupDimensionCreateCodeOptions } from './asset-group-dimensions-types';

@Component({
    selector: 'asset-group-dimensions',
    templateUrl: 'asset-group-dimensions.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['asset-group-dimensions.less'],
    providers: [AssetGroupDimensionsService]
})

export class AssetGroupDimensionsComponent implements OnInit {
    @Input() public htmlID: string;
    @Input() public editing: boolean;
    @Input() public showAllOption?: boolean;
    @Input() public showNoneOption?: boolean;
    @Input() public modelData?: number = null;
    @Output() public modelDataChange: EventEmitter<number> =
        new EventEmitter<number>();
    public loading: boolean;
    public data: KeyName[];
    public showAddInput: boolean = false;

    private translations: string[] = [];
    
    constructor(private translate: CustomTranslateService,
                private snackBar: SnackBarService,
                private confirmDialogService: ConfirmDialogService,
                private service: AssetGroupDimensionsService) { 
                    translate
                    .get(['assets.groups.all', 'form.actions.none'])
                    .subscribe((res) => { 
                        this.translations = res;
                    });
                }


    public ngOnInit() {
        this.get();
    }

    public onChangeDimension(dimensionID?: number) {
        this.modelData = dimensionID;
        this.modelDataChange.emit(dimensionID);
    }


    public onAddDimension() {
        this.showAddInput = true;
    }

    public onCancelDimension() {
        this.showAddInput = false;
    }

    public onDeleteDimension() {
        if (this.modelData === null || this.modelData === undefined) {
            return;
        }
        const confirmReq = {
            title: 'form.actions.delete',
            message: 'form.warnings.are_you_sure',
            submitButtonTitle: 'form.actions.delete'
        } as ConfirmationDialogParams;
        
        this.confirmDialogService
            .confirm(confirmReq)
            .subscribe((result) => {
                if (result === 'yes') {
                    this.delete(this.modelData);
                } 
            });
    }


    public onSaveDimension(dimension: string): void {
        if (!dimension) {
            return;
        }

        this.loading = true;
        this.service
            .create(dimension)
            .subscribe((res: AssetGroupDimensionCreateCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.showAddInput = false;
                    this.get();
                } else {
                    const errorMessage = this.getCreateResult(res);
                    this.snackBar.open(errorMessage, 'form.actions.close');
                }
        }, (err) => {
            this.loading = false;
        });
    }


    public delete(id: number): void {
        
        this.loading = true;
        this.service
            .delete(id)
            .subscribe((res) => {
                if (res === 200) {
                    this.data = [];
                    if (this.modelData === id) {
                        this.modelData = null;
                    }
                    this.get();
                } else {
                    this.snackBar.open('forms.error.unknown', 'OK');
                }
                this.loading = false;
                this.showAddInput = false;

            }, (err) => {
                this.loading = false;
            });
    }


    private get(): void {
        this.loading = true;
        this.service
            .get()
            .subscribe((dimensions) => {
                this.data = dimensions;
                this.loading = false;
                // if all option is present add to the list
                if (this.showAllOption === true) {
                    this.data.unshift({ id: null, name: this.translations['assets.groups.all'], enabled: true });
                }

                // if none option is present add to the list
                if (this.showNoneOption === true) {
                    this.data.unshift({ id: -1, name: this.translations['form.actions.none'], enabled: true });
                }

                this.data.forEach((d) => {
                    d.enabled = true;
                });

                if (!this.modelData && this.data.length > 0) {
                    this.modelData = this.data[0].id;
                }
        }, (err) => {
            this.loading = false;
            this.snackBar.open('data.error', 'form.actions.ok');
        });
    }    

    private getCreateResult(code: AssetGroupDimensionCreateCodeOptions): string {
        let message = '';
        switch (code) {
            case AssetGroupDimensionCreateCodeOptions.NameAlreadyExists:
                message = 'form.errors.name_exists';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
