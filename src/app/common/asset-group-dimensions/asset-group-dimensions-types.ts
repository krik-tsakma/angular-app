
export enum AssetGroupDimensionCreateCodeOptions {
    Created = 0,
    DimensionLabelRequired = 1,
    NameAlreadyExists = 2
}
