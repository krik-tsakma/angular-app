// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';

// COMPONENTS
import { AssetGroupDimensionsComponent } from './asset-group-dimensions.component';

// SERVICES
import { AssetGroupDimensionsService } from './asset-group-dimensions.service';


@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        AssetGroupDimensionsComponent
    ],
    exports: [
        AssetGroupDimensionsComponent
    ],
    providers: [
        AssetGroupDimensionsService
    ]
})
export class AssetGroupDimensionsModule { }
