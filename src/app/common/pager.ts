
/** Used to represent paging data request. */
export class Pager implements PagerResults {
    /** The index of the requested page */
    public pageNumber: number;
    /** The total number of records in a page */
    public pageSize: number;
    /** The total number of pages */
    public totalPages: number;
    /** The total number of records */
    public totalRecords: number;

     /** Get the last record in the pager */
    get lastRecord(): number {
        const last = this.pageNumber * this.pageSize;
        return last > this.totalRecords
            ? this.totalRecords
            : last;
    }

    /** Get the first record in the pager */
    get firstRecord(): number {
        return this.lastRecord - this.pageSize + 1;
    }

    constructor() {
        this.pageNumber = 1;
        this.pageSize = 20;
        this.totalPages = 1;
        this.totalRecords = 0;
    }

    /** increase the page number by 1 */
    public addPage() {
        this.pageNumber += 1;
    }
}

export interface PagerResults {
    pageNumber: number;
    pageSize: number;
    totalPages: number;
    totalRecords: number;
}


export interface PagerRequest {
    pageNumber: number;
    pageSize: number;
    sorting?: string;
}


export interface SearchTermRequest extends PagerRequest {
    searchTerm?: string;
}
