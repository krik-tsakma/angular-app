import { KeyName } from '../key-name';

export interface AssetGroupMembership extends KeyName {
    selected: boolean;
}
