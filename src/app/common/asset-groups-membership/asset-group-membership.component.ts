// FRAMEWORK
import { 
    Component, EventEmitter, Input, OnChanges, 
    SimpleChanges, Output, ViewEncapsulation 
} from '@angular/core';

// SERVICES
import { AssetGroupsService } from '../../admin/asset-groups/asset-groups.service';

// TYPES
import { AssetGroupMembership } from './asset-group-membership.types';
import { AssetGroupTypes, AssetGroupsRequest } from '../../admin/asset-groups/asset-groups.types';

@Component({
    selector: 'asset-group-membership',
    templateUrl: 'asset-group-membership.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['asset-group-membership.less'],
    providers: [AssetGroupsService]
})

export class AssetGroupMembershipComponent implements OnChanges {
    @Input() public roleID?: number;
    @Input() public groupType: AssetGroupTypes;
    @Input() public groupIDs: number[] = [];
    @Input() public includeUngrouped: boolean;
    @Input() public showSelectAll: boolean;
    @Output() public groupIDsChange: EventEmitter<number[]> = new EventEmitter();
    
    public loading: boolean;
    public types = AssetGroupTypes;
    public groups: AssetGroupMembership[] = [];
    public title: string;
    public message: string;
    public selectedAll: boolean;

    private initGroupIDs: number[] = [];

    constructor(private service: AssetGroupsService) { 
        // foo
    }

    public ngOnChanges(changes: SimpleChanges) {
        const gidsChange = changes['groupIDs'];
        if (gidsChange && !gidsChange.previousValue && gidsChange.currentValue) {
            this.initGroupIDs = (gidsChange.currentValue as number[]).slice(0);
        } else {
            this.checkIfAllSelected(gidsChange.currentValue ? gidsChange.currentValue.length : 0, this.groups.length); 
        }

        const groupTypeChange = changes['groupType'];        
        if (groupTypeChange && groupTypeChange.currentValue !== undefined) {
            switch (Number(groupTypeChange.currentValue)) {
                case Number(AssetGroupTypes.Vehicle): {
                    this.title = 'assets.groups.vehicle';
                    break;
                }
                case Number(AssetGroupTypes.Driver): {
                    this.title = 'assets.groups.driver';
                    break;
                }
                case Number(AssetGroupTypes.Location): {
                    this.title = 'assets.groups.location';
                    break;
                }
                case Number(AssetGroupTypes.ChargePoint): {
                    this.title = 'assets.groups.charge_point';
                    break;
                }
            }

            this.get();
        }
    }

    public onChangeOption() {
        this.groupIDs = this.groups.filter((x) => x.selected === true).map((x) => x.id);
        this.groupIDsChange.emit(this.groupIDs);
    }

    public onSelectionAll() {
        if (this.groups.length > 0) {
            this.groups.forEach((group) => {
                group.selected = this.selectedAll;
            });
            this.onChangeOption();
        }
    }

    private get() {
        this.loading = true;
        this.getAssetGroups(this.groupType).then((data) => {
            this.groups = data;
            this.loading = false;
            if (this.initGroupIDs) {
                this.checkIfAllSelected(this.initGroupIDs.length, this.groups.length);
                this.groupIDs = this.initGroupIDs.splice(0);
                const gids = this.groupIDs.splice(0);

                gids.forEach((g) => {
                    this.groups.map((x) => {
                        // if the selected ids, do not exist in user's role, then remove them
                        if (!gids.find((d) => d === x.id) && this.roleID > 0) {
                            const vIndex = this.groupIDs.indexOf(x.id);
                            this.groupIDs.splice(vIndex, 0);
                            return;
                        }
                        /* tslint:disable */
                        if (x.id == g) {
                            /* tslint:enable */
                            x.selected = true;
                        }
                    });
                });

            }
        });
    }

    private getAssetGroups(type: AssetGroupTypes): Promise<AssetGroupMembership[]> {
        return new Promise<AssetGroupMembership[]>((resolve, reject) => {
            this.message = '';
            const data: AssetGroupMembership[] = [];
            this.service
                .get({ type: type, roleID: this.roleID, includeUngrouped: this.includeUngrouped } as AssetGroupsRequest)
                .subscribe((res) => {
                    res.forEach((x) => {
                        const g = { id: x.id, name: x.name, selected: false } as AssetGroupMembership;
                        data.push(g);
                    });
                    resolve(data);
                }, (err) => {
                    this.message = 'data.error';
                });
        });
    }

    private checkIfAllSelected(selectedOptions: number, groupLength: number) {
        this.selectedAll = selectedOptions === groupLength ? true : false;
    }
}
