// FRAMEWORK
import { NgModule } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';

// MODULES
import { SharedModule } from '../../shared/shared.module';

// COMPONENTS
import { AssetGroupMembershipComponent } from './asset-group-membership.component';


@NgModule({
    imports: [
        SharedModule,
        MatExpansionModule
    ],
    declarations: [
        AssetGroupMembershipComponent
    ],
    exports: [AssetGroupMembershipComponent]
})
export class AssetGroupMembershipModule { }
