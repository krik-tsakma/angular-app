import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { LocationGroupsComponent } from './location-groups.component';

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        LocationGroupsComponent
    ],
    exports: [
        LocationGroupsComponent
    ]
})
export class  LocationGroupsModule {}
