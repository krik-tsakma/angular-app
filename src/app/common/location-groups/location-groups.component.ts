// FRAMEWORK
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

// SERVICES
import { LocationGroupsService } from '../../admin/location-groups/location-groups.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
import { SearchTermRequest } from '../pager';
import { LocationGroupResultItem, LocationGroupsResult } from '../../admin/location-groups/location-groups.types';

@Component({
    selector: 'location-groups',
    templateUrl: './location-groups.html',
    providers: [LocationGroupsService]
})

export class LocationGroupsComponent implements OnInit {
    @Input() public htmlID: string;
    @Input() public showAllOption?: boolean;
    @Input() public showNoneOption?: boolean;
    @Input() public showCustomOption?: string;
    @Input() public selected?: number;
    @Input() public fetchData: boolean;
    @Input() public data: LocationGroupResultItem[] = new Array<LocationGroupResultItem>();
    // Callback function on group initialization
    @Output() public notifyOnInit: EventEmitter<LocationGroupResultItem[]> =
        new EventEmitter<LocationGroupResultItem[]>();
    // Callback function on group change
    @Output() public notifyOnChange: EventEmitter<LocationGroupResultItem> =
        new EventEmitter<LocationGroupResultItem>();
    

    public loading: boolean;
    public selectedOption: number;

    private translations: string[] = [];

    constructor(private translate: CustomTranslateService,
                private service: LocationGroupsService) { 
                    translate
                        .get(['assets.groups.all', 'form.actions.none'])
                        .subscribe((res) => { 
                            this.translations = res;
                        });
                }


    public ngOnInit() {
        this.get();
    }


    public onChangeGroup(groupID?: number) {
        groupID = groupID !== null ? Number(groupID) : null;
        if (Number.isNaN(groupID) || groupID === 0) {
            groupID = null;
        }
        
        const group = this.data.find((item) => item.id === groupID);
        if (group) {
            this.notifyOnChange.emit(group);
            this.selectedOption = group.id;
        }
    }

    public select(id?: number) {
        this.selectedOption = id;
    }

    private get(): void {
        if (this.fetchData) {
            // perasmena karfwta. Check sto mellon
            // to pws tha to ftiaksoume.
            // TODO
            const LocalGroupReq = {
                pageNumber: 1,
                pageSize: 100,
                sorting: 'name asc'
            } as SearchTermRequest;

            this.loading = true;
            this.service
                .get(LocalGroupReq)
                .subscribe((groups: LocationGroupsResult) => {
                    this.data = groups.results.map((group) => {
                        group.enabled = true;
                        return group;
                    });
                
                    // after groups are fetched get the initial data
                    this.notifyOnInit.emit(this.data);
                    this.loading = false;
                    
                    // if all option is not present set to true
                    if (this.showAllOption === undefined) {
                        this.showAllOption = true;
                    } 
                    // if all option is present add to the list
                    if (this.showAllOption === true) {
                        this.data.unshift({ 
                            id: null, 
                            name: this.translations['assets.groups.all'], 
                            enabled: true                            
                        });
                    }


                    // if none option is not present set to false
                    if (this.showNoneOption === undefined) {
                        this.showNoneOption = false;
                    }

                    // if none option is present add to the list
                    if (this.showNoneOption === true) {
                        this.data.unshift({ id: -1, 
                            name: this.translations['form.actions.none'], 
                            enabled: true                            
                        });
                    }
              

                    if (this.showCustomOption !== undefined && this.showNoneOption === false && this.showAllOption === false) {
                        this.data.unshift({ 
                            id: -2, 
                            name: this.translate.instant(this.showCustomOption), 
                            enabled: false                            
                        });
                    } 

                    // if no value is selected, the select the first value on the list
                    if ((this.selected === null || Number(this.selected) === 0 || Number.isNaN(this.selected)) 
                        && this.data.length > 0) {   
                        if (this.showAllOption === true) {   
                            this.onChangeGroup(null);
                        } else {
                            this.onChangeGroup(this.data[0].id);
                        }
                    } else {
                        this.onChangeGroup(this.selected);
                    }
            });
        }
    }
}
