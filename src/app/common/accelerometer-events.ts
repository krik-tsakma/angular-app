
export class AccelerometerEventsResult {
    public items: AccelerometerEventsResultItem[];
    public referenceItems: AccelerometerEventsResultItem[];
}

export class AccelerometerEventsResultItem {
    public name: string;
    public value: number;
}
export class AccelerometerEventsChartItem extends AccelerometerEventsResultItem {
    public referenceItemValue: number;
}

export class AccelerometerEventsChart {
    public makeChart(
        dataProvider: AccelerometerEventsResult,   
        decimalSeparator: string,
        thousandsSeparator: string, 
        distanceMetric: string): any { 

        const newDataProvider: AccelerometerEventsChartItem[] = [];
        if (dataProvider && dataProvider.items) {
            dataProvider.items.forEach((element) => {
                if (element.value) {
                    const item = { 
                        name: element.name,
                        value: Number(element.value.toFixed(2)),
                    } as AccelerometerEventsChartItem;

                    if (dataProvider.referenceItems && dataProvider.referenceItems.length > 0) {
                        const referenceItem = dataProvider.referenceItems.filter((x) => x.name === item.name);
                        if (referenceItem) {
                            item.referenceItemValue = Number(referenceItem[0].value.toFixed(2));
                        }
                    }
                    newDataProvider.push(item);
                }
            });
        }

        return {
            type: 'serial',
            theme: 'default',
            numberFormatter: {
                decimalSeparator: decimalSeparator,
                thousandsSeparator: thousandsSeparator
            },
            dataProvider: newDataProvider,
            valueAxes: [{
                gridColor: '#FFFFFF',
                gridAlpha: 0.2,
                dashLength: 0,
                title: '#/100' + distanceMetric
            }],
            gridAboveGraphs: true,
            startDuration: 0,
            graphs: [{
                balloonText: '[[category]]: <b>[[value]]</b>',
                fillAlphas: 0.8,
                lineAlpha: 0.2,
                type: 'column',
                valueField: 'value',
                fillColors: ['#0d8ecf'],
                lineColor: '#67b7dc'

            },
            // REFERENCE ASSETS
            {
                id: 'ReferenceAssetsLine',
                balloonText: '[[title]]: [[value]]',
                bullet: 'round',
                lineThickness: 3,
                bulletSize: 7,
                bulletBorderAlpha: 1,
                bulletColor: '#4BBA42',
                lineColor: '#1C5074',
                useLineColorForBulletBorder: true,
                bulletBorderThickness: 2,
                fillAlphas: 0,
                lineAlpha: 1,
                title: 'Reference assets savings',
                valueField: 'referenceItemValue',
                dashLength: 5
            }],
            categoryField: 'name',
            categoryAxis: {
                gridPosition: 'start',
                gridAlpha: 0,
                tickPosition: 'start',
                tickLength: 10,
                autoWrap: true
            },
            responsive: {
                enabled: true
            }
        };
    }

}
