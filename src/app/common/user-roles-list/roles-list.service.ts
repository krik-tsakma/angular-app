import { UserRolesResult } from './../../admin/user-roles/user-roles.types';

// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES 
import { UserRolesListItem } from './roles-list-types';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class UserRolesListService {
    private baseApiURL: string;
    
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseApiURL = config.get('apiUrl') + '/api/UserRolesAdmin';
        }


    public get(): Observable<UserRolesListItem[]> {       
        const params = new HttpParams({
            fromObject: {
                SearchTerm: '',
                PageNumber: '1',
                PageSize: '200',
                Sorting: 'Name asc'
            }
        });

        return this.authHttp
            .get(this.baseApiURL, { params })
            .pipe(
                map((r) => {
                    const res = r as UserRolesResult;
                    res.results.forEach((ur) => {
                        ur.enabled = true;
                    });
                    return res.results;
                })
            );
    }
}
