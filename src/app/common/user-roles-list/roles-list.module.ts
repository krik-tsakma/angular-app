// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { UserRolesListService } from './roles-list.service';

// COMPONENTS
import { UserRolesListComponent } from './roles-list.component';

@NgModule({
    imports: [
        SharedModule      
    ],
    declarations: [
        UserRolesListComponent
    ],
    exports: [
        UserRolesListComponent
    ],
    providers: [
        UserRolesListService
    ]
})
export class UserRolesListModule {}
