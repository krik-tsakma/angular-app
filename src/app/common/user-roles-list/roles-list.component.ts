// FRAMEWORK
import {
    Component, Input, Output, EventEmitter
} from '@angular/core';

// SERVICES
import { SnackBarService } from '../../shared/snackbar.service';
import { UserRolesListService } from './roles-list.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
import { KeyName } from '../key-name';
import { UserRolesResultItem } from '../../admin/user-roles/user-roles.types';

@Component({
    selector: 'user-roles-list',
    templateUrl: './roles-list.html'
})

export class UserRolesListComponent {
    @Input() public showAllOption?: boolean = false;
    @Input() public modelData?: number;
    @Output() public modelDataChange: EventEmitter<number> =
        new EventEmitter<number>();
   
    public data: KeyName[];
    public loading: boolean;

    constructor(private snackBar: SnackBarService,
                private translate: CustomTranslateService,
                private service: UserRolesListService
            ) {
        this.get();
    }

    public onChangeOption(id?: number) {
        this.modelData = id === null ? null : Number(id);
        this.modelDataChange.emit(this.modelData);
    }

    private get() {
        this.loading = true;
        
        this.service
            .get()
            .subscribe((res: UserRolesResultItem[]) => {
                this.loading = false;
                this.data = [];
              
                if (this.showAllOption) {
                    this.data.push({
                        id: null,
                        name: this.translate.instant('form.actions.all'),
                        enabled: true
                    });
                }
                res.forEach((x) => {
                    this.data.push({
                        id: x.id,
                        name: x.name,
                        enabled: x.isDefaultSycadaAdmin ? false : true
                    });
                });

                if (this.modelData === null || this.modelData === undefined) {
                    this.onChangeOption(this.data[0].id);
                }
            },
            (err) => {
                this.snackBar.open('data.error', 'form.actions.close');
                this.loading = false;
            }
        );
    }
}
