import { KeyName } from '../key-name';

export interface UserRolesListItem extends KeyName {
    isDefaultSycadaAdmin: boolean;
}
