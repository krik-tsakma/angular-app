﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { WeeklyTimeScheduleDialogService } from './weekly-time-schedule.service';

// COMPONENTS
import { WeeklyTimeScheduleComponent } from './weekly-time-schedule.component';

@NgModule({
    imports: [
        SharedModule 
    ],
    declarations: [
        WeeklyTimeScheduleComponent
    ],
    entryComponents: [
        WeeklyTimeScheduleComponent
    ],
    exports: [
        WeeklyTimeScheduleComponent
    ],
    providers: [
        WeeklyTimeScheduleDialogService
    ]
})
export class WeeklyTimeScheduleModule {}
