﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { WeeklyTimeScheduleComponent } from './weekly-time-schedule.component';

// TYPES
import { WeeklyTimeSchedule } from './weekly-time-schedule-types';


@Injectable()
export class WeeklyTimeScheduleDialogService {

    constructor(private dialog: MatDialog) { }

    public open(params: WeeklyTimeSchedule): Observable<WeeklyTimeSchedule> {

        const dialogRef: MatDialogRef<WeeklyTimeScheduleComponent> = this.dialog.open(WeeklyTimeScheduleComponent);
        dialogRef.componentInstance.importWeek.items = params.items;

        return dialogRef.afterClosed();
    }
}
