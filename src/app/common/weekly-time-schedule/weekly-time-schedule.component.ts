﻿// FRAMEWORK
import {
    Component, AfterViewInit, ViewChild,
    EventEmitter, ViewEncapsulation, ChangeDetectorRef
} from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSelectionList } from '@angular/material/list';

// TYPES
import { DayOfWeek, WeeklyTimeScheduleItem, WeeklyTimeSchedule } from './weekly-time-schedule-types';


@Component({
    selector: 'weekly-time-schedule',
    templateUrl: 'weekly-time-schedule.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['weekly-time-schedule.less']
})

export class WeeklyTimeScheduleComponent implements AfterViewInit  { 
    // Pass weekly time schedule from main component to dialog component
    public importWeek: WeeklyTimeSchedule = {} as WeeklyTimeSchedule;

    // Make enum as public variable
    public dayOfWeek = DayOfWeek;

    public selectedAll: boolean = false;

    @ViewChild('sunday', { static: true }) public sunday: MatSelectionList;
    @ViewChild('monday', { static: true }) public monday: MatSelectionList;
    @ViewChild('tuesday', { static: true }) public tuesday: MatSelectionList;
    @ViewChild('wednesday', { static: true }) public wednesday: MatSelectionList;
    @ViewChild('thursday', { static: true }) public thursday: MatSelectionList;
    @ViewChild('friday', { static: true }) public friday: MatSelectionList;
    @ViewChild('saturday', { static: true }) public saturday: MatSelectionList;

    // Helper array for showing if all days are checked or not
    public daysArray: any[] = [
        {
            id: DayOfWeek.Sunday,
            checked: false,
            partiallyChecked: false
        },
        {
            id: DayOfWeek.Monday,
            checked: false,
            partiallyChecked: false
        },
        {
            id: DayOfWeek.Tuesday,
            checked: false,
            partiallyChecked: false
        },
        {
            id: DayOfWeek.Wednesday,
            checked: false,
            partiallyChecked: false
        },
        {
            id: DayOfWeek.Thursday,
            checked: false,
            partiallyChecked: false
        },
        {
            id: DayOfWeek.Friday,
            checked: false,
            partiallyChecked: false
        },
        {
            id: DayOfWeek.Saturday,
            checked: false,
            partiallyChecked: false
        }
    ];

    // List table with hours - see constructor
    public hours: string[] = [];

    constructor(
        public dialogRef: MatDialogRef<WeeklyTimeScheduleComponent>,
        private changeDetectionRef: ChangeDetectorRef) {

        // Creation of the list table with hours 
        let listNum = 0;
        const getStringNumber = (num) => {
            return num.length === 1
                ? `0${num}:00`
                : `${num}:00`;           
        };

        while (listNum < 24) {
            const from: string = getStringNumber(listNum.toString());
            listNum++;
            const to: string = listNum !== 24
                ? getStringNumber(listNum.toString())
                : getStringNumber('00');   
            this.hours.push(`${from} - ${to}`);
        }
    }

    // Check if the array from main component has any hour checked
    public ngAfterViewInit() {        

        if (this.importWeek && this.importWeek.items) {
            if (!this.importWeek.items.find((day) => day.hours.length < 24)) {               
                this.checkAllTimes(true);
            } else {
                this.importWeek.items.forEach((day) => {
                    switch (day.day) {
                        case DayOfWeek.Sunday:
                            this.conditionCheck(this.sunday, day.hours, day.day);
                            break;
                        case DayOfWeek.Monday:
                            this.conditionCheck(this.monday, day.hours, day.day);
                            break;
                        case DayOfWeek.Tuesday:
                            this.conditionCheck(this.tuesday, day.hours, day.day);
                            break;
                        case DayOfWeek.Wednesday:
                            this.conditionCheck(this.wednesday, day.hours, day.day);
                            break;
                        case DayOfWeek.Thursday:
                            this.conditionCheck(this.thursday, day.hours, day.day);
                            break;
                        case DayOfWeek.Friday:
                            this.conditionCheck(this.friday, day.hours, day.day);
                            break;
                        case DayOfWeek.Saturday:
                            this.conditionCheck(this.saturday, day.hours, day.day);
                            break;
                        default:
                            break;
                    }
                });
            }
        }

        this.changeDetectionRef.detectChanges();
    }


    // Find which hours are checked per day
    public Hourfinder(hours: number[], day: MatSelectionList) {
        if (hours && hours.length > 0) {
            hours.forEach((hour) => {
                day.options.find((d) => {
                    return d.value === hour;
                }).selected = true;
            });
        }
    }


    // Used every time dialog opens
    public conditionCheck(day: MatSelectionList, hours: number[], dayOfWeek: DayOfWeek) {
        if (hours.length === 24) {
            // If all day is checked
            this.checkAllDay(dayOfWeek, true);
        } else {
            // Find if there are any hours checked per day
            this.Hourfinder(hours, day);
            // Checks if all or some of the hours are checked for UI coloring perposes
            this.changedChecks(day, dayOfWeek);
        }
        
    }



    // Check and uncheck full day/days
    public checkAllTimes(event, dayOfWeek?: DayOfWeek) {
        if (dayOfWeek || dayOfWeek === 0) {
            this.checkAllDay(dayOfWeek, event);           
        } else {            
            for (const item in this.dayOfWeek) {
                if (isNaN(Number(item))) {
                    this.checkAllDay(Number(DayOfWeek[item]), event);
                }
            } 
        }
    }    


    // Event trigger from each list, every time a checkbox is checked.
    // Also is used every time dialog opens to colorize day of the week
    public changedChecks(day: MatSelectionList, dayOfWeek: DayOfWeek) {

        const partialCheck = (check: boolean) => {
            const selectedDay = this.daysArray.find((d) => d.id === dayOfWeek);
            if (selectedDay) {
                selectedDay.partiallyChecked = check;
            }
        };

        if (day.selectedOptions && day.selectedOptions.selected) {
            if (day.selectedOptions.selected.length > 0) {
                partialCheck(true);      
                this.checkDays(dayOfWeek, day.selectedOptions.selected.length === 24 ? true : false);                
            } else {
                partialCheck(false);
                this.checkDays(dayOfWeek, false);
            }
        } 
    }



    // Save function
    public onSave() {

        this.importWeek = {
            items: [
                {
                    day: DayOfWeek.Sunday,
                    hours: this.sunday.selectedOptions.selected.map((option) => {
                        return option.value;
                    })
                },
                {
                    day: DayOfWeek.Monday,
                    hours: this.monday.selectedOptions.selected.map((option) => {
                        return option.value;
                    })
                },
                {
                    day: DayOfWeek.Tuesday,
                    hours: this.tuesday.selectedOptions.selected.map((option) => {
                        return option.value;
                    })
                },
                {
                    day: DayOfWeek.Wednesday,
                    hours: this.wednesday.selectedOptions.selected.map((option) => {
                        return option.value;
                    })
                },
                {
                    day: DayOfWeek.Thursday,
                    hours: this.thursday.selectedOptions.selected.map((option) => {
                        return option.value;
                    })
                },
                {
                    day: DayOfWeek.Friday,
                    hours: this.friday.selectedOptions.selected.map((option) => {
                        return option.value;
                    })
                },
                {
                    day: DayOfWeek.Saturday,
                    hours: this.saturday.selectedOptions.selected.map((option) => {
                        return option.value;
                    })
                }
            ]
        };

        this.dialogRef.close(this.importWeek);

    }



    // Close without saving
    public onClose() {
        this.dialogRef.close(this.importWeek);
    }



    // ----------------
    // PRIVATE
    // ----------------



    // DayArray checked key becomes true if all checkbuttons are checked
    private checkDays(dayOfWeek: DayOfWeek, check: boolean) {
        this.daysArray.forEach((day) => {
            if (dayOfWeek === day.id) {
                day.checked = check;
            }
        });        
    }


    // Select/Diselect all day
    private checkAllDay(dayOfWeek: DayOfWeek, event: boolean) {

        const checkForAll = (day: MatSelectionList, check: boolean) => {                       
            if (check) {
                day.selectAll();
            } else {
                day.deselectAll();
            }            
        };

        switch (dayOfWeek) {
            case DayOfWeek.Sunday:
                checkForAll(this.sunday, event);
                this.changedChecks(this.sunday, dayOfWeek);
                break;
            case DayOfWeek.Monday:                
                checkForAll(this.monday, event);
                this.changedChecks(this.monday, dayOfWeek);                
                break;
            case DayOfWeek.Tuesday:
                checkForAll(this.tuesday, event);
                this.changedChecks(this.tuesday, dayOfWeek);
                break;
            case DayOfWeek.Wednesday:                                
                checkForAll(this.wednesday, event);
                this.changedChecks(this.wednesday, dayOfWeek);                
                break;
            case DayOfWeek.Thursday:                
                checkForAll(this.thursday, event);
                this.changedChecks(this.thursday, dayOfWeek);                
                break;
            case DayOfWeek.Friday:                
                checkForAll(this.friday, event);
                this.changedChecks(this.friday, dayOfWeek);                
                break;
            case DayOfWeek.Saturday:               
                checkForAll(this.saturday, event);
                this.changedChecks(this.saturday, dayOfWeek);
                break;
            default:                
                break;
        }

        this.selectedAll = this.daysArray.filter((day) => day.checked === false).length === 0
            ? true
            : false;
    }


}
