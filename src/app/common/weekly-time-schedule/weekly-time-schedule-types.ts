﻿export enum DayOfWeek {
    Sunday = 0,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,    
}

export interface WeeklyTimeScheduleItem {
    day: DayOfWeek;
    hours: number[];
}

export interface WeeklyTimeSchedule {
    items: WeeklyTimeScheduleItem[];
}
