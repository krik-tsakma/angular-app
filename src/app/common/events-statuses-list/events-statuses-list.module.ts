﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { EventStatusesService } from './events-statuses.service';

// COMPONENTS
import { EventsStatusesListComponent } from './events-statuses-list.component';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        EventsStatusesListComponent
    ],
    entryComponents: [
        EventsStatusesListComponent
    ],
    exports: [
        EventsStatusesListComponent
    ],
    providers: [
        EventStatusesService
    ]
})
export class EventsStatusesListModule { 

}
