﻿// FRAMEWORK
import {
    Component, AfterViewInit, ViewEncapsulation,
    EventEmitter, Input, Output, OnInit
} from '@angular/core';

// SERVICE
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { EventStatusesService } from './events-statuses.service';

// TYPES
import { KeyName } from '../key-name';

@Component({
    selector: 'events-statuses-list',
    templateUrl: 'events-statuses-list.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['events-statuses-list.less']
})

export class EventsStatusesListComponent implements OnInit, AfterViewInit {
    @Input() public ddlMode?: boolean = false;
    @Input() public modelData?: number = null;
    @Output() public modelDataChange: EventEmitter<number> =
        new EventEmitter<number>();
    public data: KeyName[];
    public loading: boolean;

    constructor(
        public service: EventStatusesService,
        private translate: CustomTranslateService) {
        // foo
    }

    public ngOnInit() {
        this.loading = true;
        this.service
            .get()
            .subscribe((res: KeyName[]) => {
                this.loading = false;
                if (!res) {
                    return;
                }
                const nullable: KeyName = {
                    id: null,
                    name: this.translate.instant('form.actions.all'),
                    enabled: true
                };
                this.data = res.map((cls) => {
                    cls.enabled = true;
                    if (!cls.name) {
                        cls.name = cls.id;
                    }
                    return cls;
                });
                this.data.unshift(nullable);
            },
            (err) => {
                this.loading = false;
            });
    }

    public ngAfterViewInit() {
        if (!this.modelData) {
            this.modelData = null;
        }
    }

    public onSelectOption(id?: number) {
        this.modelData = !id ? null : Number(id);
        this.modelDataChange.emit(this.modelData);
    }
}
