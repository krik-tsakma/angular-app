
import { KeyName } from '../key-name';

export enum EnergyUnitOptions {
    Litre = 0,
    KilowattHour
}

export interface EnergyType extends KeyName {
    unit: EnergyUnitOptions;
}
