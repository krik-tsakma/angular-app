// FRAMEWORK
import { Injectable } from '@angular/core';

// RXSK
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
import { EnergyType, EnergyUnitOptions } from './energy-types';

@Injectable()
export class EnergyTypesService {
    private translations: any;
    
    constructor(
        private authHttp: AuthHttp,
        private config: Config,
        private translate: CustomTranslateService) {
            this.translate
                .get(
                    [
                        'form.actions.none',
                        'form.actions.all',
                    ]
                )
                .subscribe((res) => {
                    if (res) {
                        this.translations = res;
                    }
                });
        }

    public get(allOption: boolean, noneOption: boolean): Observable<EnergyType[]> {
        return this.authHttp
            .get(this.config.get('apiUrl') + '/api/EnergyTypes', { observe: 'response' })
            .pipe(
                map((r) => {
                    let res = r.body as EnergyType[];
                    if (!res) {
                        res = new Array<EnergyType>();
                    }
                    res = res.map((type) => {
                        type.enabled = true;
                        return type;
                    });
                    if (allOption && this.translations['form.actions.all']) {
                        res.unshift({
                            id: null,
                            name: this.translations['form.actions.all'],
                            enabled: true,
                            unit: null
                        });
                    }
                    if (noneOption && this.translations['form.actions.none']) {
                        res.unshift({
                            id: -1,
                            name: this.translations['form.actions.none'],
                            enabled: true,
                            unit: null
                        });
                    }
                    return res;
                })
            );
    }


    public getEnergyUnit(energyTypeID: number): Observable<EnergyUnitOptions> {
        return this.authHttp
            .get(this.config.get('apiUrl') + '/api/EnergyTypes/GetByID/' + energyTypeID);
    }
}
