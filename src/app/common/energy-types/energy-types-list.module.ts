// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { EnergyTypesService } from './energy-types.service';

// COMPONENTS
import { EnergyTypesListComponent } from './energy-types-list.component';

@NgModule({
    imports: [
        SharedModule      
    ],
    declarations: [
        EnergyTypesListComponent
    ],
    exports: [
        EnergyTypesListComponent
    ],
    providers: [
        EnergyTypesService
    ]
})
export class EnergyTypesListModule {}
