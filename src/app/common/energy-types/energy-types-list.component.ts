
// FRAMEWORK
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

// SERVICES
import { SnackBarService } from '../../shared/snackbar.service';
import { EnergyTypesService } from './energy-types.service';

// TYPES
import { KeyName } from '../key-name';
import { EnergyType } from './energy-types';

@Component({
    selector: 'energy-types-list',
    templateUrl: './energy-types-list.html'
})

export class EnergyTypesListComponent implements OnInit {
    @Input() public showAllOption?: boolean;
    @Input() public showNoneOption?: boolean;
    @Input() public modelData?: number;
    @Output() public modelDataChange: EventEmitter<number> =
        new EventEmitter<number>();
   
    public data: KeyName[];
    public loading: boolean;

    constructor(private snackBar: SnackBarService,
                private service: EnergyTypesService,
            ) {
            // foo
    }

    public ngOnInit(): void {
        this.get();
    }

    public onChangeOption(id: number) {
        this.modelData = Number(id);
        this.modelDataChange.emit(this.modelData);
    }

    private get() {
        this.loading = true;
        this.service
            .get(this.showAllOption, this.showNoneOption)            
            .subscribe((res: EnergyType[]) => {
                this.loading = false;
                this.data = res;

                // if none selected option was provided select the first one
                if (this.data && this.data.length > 0 && (this.modelData === undefined || this.modelData === null)) {
                    this.modelData = this.data[0].id;
                }
            },
            (err) => {
                this.snackBar.open('data.error', 'form.actions.close');
                this.loading = false;
            });
    }

}
