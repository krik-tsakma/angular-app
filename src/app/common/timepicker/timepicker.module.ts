// FRAMEWORK
import { NgModule  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';

// COMPONENT
import { TimePickerComponent } from './timepicker.component';

@NgModule({
    imports: [
        CommonModule,
        MatIconModule,
        FormsModule,
        TranslateModule
    ],
    declarations: [
        TimePickerComponent
    ],
    exports: [
        TimePickerComponent
    ],
    providers: [
    ]
})
export class TimePickerModule {}
