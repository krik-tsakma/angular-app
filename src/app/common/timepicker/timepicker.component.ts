// FRAMEWORK
import { 
    Component, Input, Output, OnChanges, SimpleChanges,
    EventEmitter, ViewEncapsulation
} from '@angular/core';


@Component({
    selector: 'timepicker',
    templateUrl: 'timepicker.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['timepicker.less']
})

export class TimePickerComponent implements OnChanges {
    @Input() public hours: number;
    @Input() public minutes: number;
    @Input() public stepMinutes: number = 1;
    @Output() public hoursChange: EventEmitter<number> = new EventEmitter<number>();
    @Output() public minutesChange: EventEmitter<number> = new EventEmitter<number>();

    public hoursString: string;
    public minutesString: string;
    
    constructor() {
        // foo
    }

    public ngOnChanges(changes: SimpleChanges) {
        const hoursChange = changes['hours'];
        if (hoursChange) {
            this.hoursString = this.pad2(hoursChange.currentValue);
        } 
        
        const minutesChange = changes['minutes'];
        if (minutesChange) {
            this.minutesString = this.pad2(minutesChange.currentValue);
        } 
    }

    public incrementValue(type: string) {
        if (type === 'minutes') {
            this.minutes = this.increment(this.minutes, type);
            this.minutesChange.emit(this.minutes);
        } else {
            this.hours = this.increment(this.hours, type);
            this.hoursChange.emit(this.hours);
            
        }
    }

    public decrementValue(type: string) {
        if (type === 'minutes') {
            this.minutes = this.decrement(this.minutes, type);
            this.minutesChange.emit(this.minutes);
        } else {
            this.hours = this.decrement(this.hours, type);
            this.hoursChange.emit(this.hours);
        }
    }


    public onFocusOut(e, type: string): void {
        const typedNumber = Number(e.srcElement.value);
        if (type === 'minutes') {
            this.minutes = this.validationCheck(typedNumber, 59, type);
            this.minutesString = this.pad2(this.minutes);
            this.minutesChange.emit(this.minutes);
        } else {
            this.hours = this.validationCheck(typedNumber, 23, type);
            this.hoursString = this.pad2(this.hours);
            this.hoursChange.emit(this.hours);
        }
    }

    private validationCheck(value: number, maxValue: number, type: string): number {
        if (value === undefined || value === null || value < 0 || isNaN(value)) {
            return 0;
        } else if (value > maxValue) {
            return maxValue;
        }
        const valueFinal = this.getClosestNumberToStepValue(type, value);
        return valueFinal;
    }

    private increment(value: number, type: string): number {
        const maxValue = (type === 'minutes') ? 59 : 23;
        if (value === undefined || value === null) {
            return 0;
        } else if (value === maxValue) {
            return value;
        } else {
            const step = this.getStepForType(type);
            const valueWithStep = value + step;
            return valueWithStep > maxValue ? maxValue : valueWithStep;
        }
    }


    private decrement(value: number, type: string): number {
        if (value === undefined || value === null || value === 0) {
            return 0;
        } else {
            const step = this.getStepForType(type);
            const valueMinusStep = value - step;
            const valueFinal = this.getClosestNumberToStepValue(type, valueMinusStep);
            return valueFinal < 0 ? 0 : valueFinal;
        }
    }

    private pad2(val: number): string {
        if (val === undefined || val === null) {
            return '';
        }
       
        const valStr = val.toString();
        const prefix = (val < 10 ? '0' : '');
        return prefix + valStr;
    }

    private getStepForType(type: string): number {
        if (type === 'minutes') {
            return this.stepMinutes;
        } else {
            return 1;
        }
    }

    private getClosestNumberToStepValue(type: string, value: number) {
        const step = this.getStepForType(type);
        const val = Math.floor(value / step);
        return step * val;
    }
}
