// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { CulturesService } from './cultures-list.service';

// COMPONENTS
import { CulturesListComponent } from './cultures-list.component';

@NgModule({
    imports: [
        SharedModule      
    ],
    declarations: [
        CulturesListComponent
    ],
    exports: [
        CulturesListComponent
    ],
    providers: [
        CulturesService
    ]
})
export class CulturesListModule {}
