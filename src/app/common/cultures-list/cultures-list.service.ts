// FRAMEWORK
import { Injectable } from '@angular/core';

// RXSK
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { KeyName } from '../key-name';
import { CultureItem } from './cultures-list.types';

@Injectable()
export class CulturesService {
    
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            // foo
        }

    public get(): Observable<CultureItem[]> {
        return this.authHttp
            .get(this.config.get('apiUrl') + '/api/Lists/Cultures');
    }
}
