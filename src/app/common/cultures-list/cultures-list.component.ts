import { CultureItem } from './cultures-list.types';
// FRAMEWORK
import {
    Component, Input, Output, EventEmitter
} from '@angular/core';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { CulturesService } from './cultures-list.service';

// TYPES
import { KeyName } from '../key-name';

@Component({
    selector: 'cultures-list',
    templateUrl: 'cultures-list.html'
})

export class CulturesListComponent {
    @Input() public modelData?: string;
    @Output() public modelDataChange: EventEmitter<string> =
        new EventEmitter<string>();
   
    public data: KeyName[];
    public loading: boolean;

    constructor(private translate: CustomTranslateService,
                private snackBar: SnackBarService,
                private service: CulturesService
            ) {
      
        this.get();
    }


    public onChangeOption(id: string) {
        this.modelData = id;
        this.modelDataChange.emit(this.modelData);
    }
    

    private get() {
        this.loading = true;
        this.service
            .get()
            .subscribe((data: CultureItem[]) => {
                this.loading = false;
                this.data = data.map((c) => {
                    return { 
                        id: c.code, 
                        name: c.name,
                        enabled: true
                    } as KeyName;
                });
                if (this.modelData === null || this.modelData === undefined) {
                    this.onChangeOption(this.data[0].id);
                }
            },
            (err) => {
                this.snackBar.open('data.error', 'form.actions.close');
                this.loading = false;
            });
    }

}
