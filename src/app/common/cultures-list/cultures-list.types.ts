export interface CultureItem {
    code: string;
    name: string;
}
