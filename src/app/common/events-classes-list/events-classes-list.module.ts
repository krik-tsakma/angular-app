﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { EventClassesService } from './events-classes.service';

// COMPONENTS
import { EventsClassesListComponent } from './events-classes-list.component';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        EventsClassesListComponent
    ],
    entryComponents: [
        EventsClassesListComponent
    ],
    exports: [
        EventsClassesListComponent
    ],
    providers: [
        EventClassesService
    ]
})
export class EvenstClassesListModule {

}
