
// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from './../../config/config';
import { AuthHttp } from './../../auth/authHttp';

// TYPES
import { KeyName } from '../key-name';

@Injectable()
export class EventClassesService {
    private baseURL: string;
   
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/EventPropertiesList';
        }

    public get(): Observable<KeyName[]> {
        return this.authHttp
            .get(this.baseURL + '/GetEventClasses');
    }
   
} 
