import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

import { ScrollToTopComponent } from './scroll-to-top.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [
        ScrollToTopComponent
    ],
    exports: [
       ScrollToTopComponent
    ]
})
export class ScrollToTopModule {}
