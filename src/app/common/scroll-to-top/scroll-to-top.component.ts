// FRAMEWORK
import {
    Component, Renderer, ElementRef, 
    ViewEncapsulation, OnInit, ChangeDetectorRef
} from '@angular/core';

@Component({
    selector: 'scroll-to-top',
    templateUrl: './scroll-to-top.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./scroll-to-top.less']
})

export class ScrollToTopComponent implements OnInit {
    private scrollableElm: HTMLElement;

    constructor(private el: ElementRef,
                private renderer: Renderer,
                private changeDetectionRef: ChangeDetectorRef) {
        this.scrollableElm = el.nativeElement.parentNode;
    }
       // On initialization
    public ngOnInit() {
        this.changeDetectionRef.detectChanges();
    }
    public show() {
        if (!this.scrollableElm) {
            return;
        }
        return this.scrollableElm.scrollTop > 200;
    }

    public scrollToTop() {
        if (!this.scrollableElm) {
            return;
        }

        this.scrollableElm.scrollTop = 0;
    }
}
