
/** Used to set the correct size of a dom element inside the screen. */
export class SizeManager {
    /** The id of the element to set its height */
    private elementID: string;
    /** The element id that follows our element */
    private footerID: string;
    /** The function to call after the resize */
    private callback: any;


    constructor(elementID: string, footerID?: string, callback?: any) {
        this.elementID = elementID;
        this.footerID = footerID;
        this.callback = callback;
        
        const that = this;
        let resizedFinished;
        window.onresize = () => {
            clearTimeout(resizedFinished);
            resizedFinished = setTimeout(() => {
                // console.log('Resized finished.');
                that.setHeight();
            }, 250);
        };
    }

     /** set the height of the dom element */
     public setHeight() {
        let height = window.innerHeight;
        const lm = document.getElementById(this.elementID);
        if (lm) {
            const distanceFromTop = this.getPosition(lm);
            height = height - distanceFromTop.y;
            if (this.footerID) {
                const dateHeight = document.getElementById(this.footerID).offsetHeight;
                height = height - dateHeight;
            }

            lm.style.maxHeight = height + 'px';
            lm.style.height = height + 'px';

            if (this.callback) {
                this.callback(height);
            }
        }
    }

    // offsetTop only looks at the element's parent. 
    // Just loop through parent nodes until you run out of parents and add up their offsets.
    private getPosition(element) {
        let xPosition = 0;
        let yPosition = 0;

        while (element) {
            xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
            yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
            element = element.offsetParent;
        }

        return { x: xPosition, y: yPosition };
    }
}
