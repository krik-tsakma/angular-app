import moment from 'moment';
import { KeyName } from '../key-name';

export enum PeriodTypes {
    Today = -1 ,
    YearToDate = 0,
    FiscalYearToDate = 1,
    Last12Months = 2,
    LastMonth = 3,
    ThisMonth = 4,
    LastWeek = 5,
    ThisWeek = 6,
    Custom = 7,
    PreviousFiscalYear = 8,
    Last13Months = 12
}

export class PeriodTypesConverter {
    public convert(type: PeriodTypes): PeriodTypeValues {
        const dt = {
            datetimeSince: moment(),
            datetimeTill: moment()
        } as PeriodTypeValues;

        switch (type) {
            case PeriodTypes.Today:
                dt.datetimeSince = moment().startOf('day');
                dt.datetimeTill = moment().endOf('day');
                break;
            case PeriodTypes.YearToDate:
                dt.datetimeSince = moment().startOf('year');
                dt.datetimeTill = moment().endOf('day');
                break;
            case PeriodTypes.ThisWeek:
                dt.datetimeSince = moment().startOf('isoWeek').subtract(1, 'day');
                dt.datetimeTill = moment().endOf('day');
                break;
            case PeriodTypes.ThisMonth:
                dt.datetimeSince = moment().startOf('month');
                dt.datetimeTill = moment().endOf('day');
                break;
            case PeriodTypes.LastWeek:
                // sunday
                dt.datetimeSince = moment().subtract(1, 'weeks').startOf('isoWeek').subtract(1, 'day');
                // saturday
                dt.datetimeTill = moment().subtract(1, 'weeks').startOf('isoWeek').endOf('week').subtract(1, 'day');
                break;
            case PeriodTypes.LastMonth:
                dt.datetimeSince = moment().subtract(1, 'months').startOf('month');
                dt.datetimeTill = moment().subtract(1, 'months').endOf('month');
                break;
            case PeriodTypes.Last12Months:
                dt.datetimeSince = moment().subtract(11, 'month').startOf('month');
                dt.datetimeTill = moment().endOf('month');
                break;
            case PeriodTypes.Last13Months:
                dt.datetimeSince = moment().subtract(12, 'month').startOf('month');
                dt.datetimeTill = moment().endOf('month');
                break;
            default:
                break;
        }

        return dt;
    }
}

export interface PeriodTypeValues {
    datetimeSince?: moment.Moment;
    datetimeTill?: moment.Moment;
}

export interface PeriodTypeOption extends KeyName, PeriodTypeValues { }

export let PeriodTypeOptionsDefaults: PeriodTypeOption[] = [
    {
        term: 'period_selector.today',
        id: PeriodTypes.Today,
        enabled: true,
        name: 'Today',
    },
    {
        term: 'period_selector.this_week',
        id: PeriodTypes.ThisWeek,
        enabled: true,
        name: 'This week',
    },
    {
        term: 'period_selector.last_week',
        id: PeriodTypes.LastWeek,
        enabled: true,
        name: 'Last week',
    },
    {
        term: 'period_selector.this_month',
        id: PeriodTypes.ThisMonth,
        enabled: true,
        name: 'This month',
    },
    {
        term: 'period_selector.last_month',
        id: PeriodTypes.LastMonth,
        enabled: true,
        name: 'Last month',
    },
    {
        term: 'period_selector.last_12_months',
        id: PeriodTypes.Last12Months,
        enabled: true,
        name: 'Last 12 months',
    },
    {
        term: 'period_selector.last_13_months',
        id: PeriodTypes.Last13Months,
        enabled: true,
        name: 'Last 13 months',
    },
    {
        term: 'period_selector.year_to_date',
        id: PeriodTypes.YearToDate,
        enabled: true,
        name: 'Year to date',
    },
    {
        term: 'period_selector.fiscal_year_to_date',
        id: PeriodTypes.FiscalYearToDate,
        enabled: true,
        name: 'Fiscal year to date',
    },
    {
        term: 'period_selector.previous_fiscal_year',
        id: PeriodTypes.PreviousFiscalYear,
        enabled: false,
        name: 'Previous fiscal year',
    },
    {
        term: 'period_selector.custom',
        id: PeriodTypes.Custom,
        enabled: true,
        name: 'Custom',
    }
];
