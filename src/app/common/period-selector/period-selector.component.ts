// FRAMEWORK
import {
    Component, Input, Output, OnChanges,
    SimpleChanges, EventEmitter, ViewEncapsulation
} from '@angular/core';

// SERVICES
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { SnackBarService } from '../../shared/snackbar.service';

// TYPES
import {
    PeriodTypes, PeriodTypeOption,
    PeriodTypeOptionsDefaults, PeriodTypesConverter
} from './period-selector-types';

// MOMENT
import moment from 'moment';


@Component({
    selector: 'period-selector',
    templateUrl: './period-selector.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./period-selector.less']
})

export class PeriodSelectorComponent implements OnChanges {
    @Input() public htmlID: string;
    @Input() public options: PeriodTypes[];
    @Input() public showSearchButton: boolean = true;
    @Input() public modelData: PeriodTypes;
    @Output() public modelDataChange: EventEmitter<PeriodTypeOption> = new EventEmitter<PeriodTypeOption>();
    public matSinceDatePicker: moment.Moment;
    public matTillDatePicker: moment.Moment;
    public todayDayStart: moment.Moment = moment().startOf('day');
    public todayDayEnd: moment.Moment = moment().endOf('day');
    public selectedOption: PeriodTypeOption;
    public showDatepicker: boolean = false;
    public periodOptions: PeriodTypeOption[] = [];
    public touchOption: boolean;
    private customOptionValue: PeriodTypeOption;

    constructor(
        public userOptions: UserOptionsService,
        public snackBar: SnackBarService,
        private translate: CustomTranslateService) {
            this.customOptionValue = PeriodTypeOptionsDefaults
                                    .find((x) => x.id === PeriodTypes.Custom).id;
    }


    public ngOnChanges(changes: SimpleChanges) {
        // this is for first time set of options
        const optionsChange = changes['options'];
        if (optionsChange && optionsChange.firstChange === true) {
            PeriodTypeOptionsDefaults.forEach((option) => {
                this.translate.get(option.term).subscribe((res) => {
                    option.name = res;
                });
                const opt = this.options.find((x) => Number(x) === option.id);
                if (opt !== undefined && opt !== null) {
                    option.enabled = true;
                } else {
                    option.enabled = false;
                }
                this.periodOptions.push(option);
            });
        }

        // on every change of the model data
        const modelDataChange = changes['modelData'];
        if (this.periodOptions.length !== 0 && modelDataChange && modelDataChange.currentValue) {
            this.onChangeOption(modelDataChange.currentValue as PeriodTypeOption, true);
        }
    }



    public setCustomDates(datetimeSince: moment.Moment, datetimeTill: moment.Moment, submit: boolean = true) {

        this.matSinceDatePicker = moment(datetimeSince);
        this.matTillDatePicker = moment(datetimeTill);

        if (submit === true) {
            this.datepickerSubmit();
        }
    }



    public datepickerSubmit() {
        this.onChangeOption(this.customOptionValue, true);
    }


    public async onChangeOption(id: PeriodTypeOption, submitDates?: boolean) {
        let result: PeriodTypeOption;
        try {
            result = await this.selectOption(id);
            if ((result.id !== this.customOptionValue || this.showSearchButton === false) || submitDates === true) {
                this.modelData = result.id;
                this.modelDataChange.emit(result);
            }
        } catch (e) {
            // could be on initialization where periodOptions are not yet populated
        }
    }


    public onSelectSinceDate(dateSelected: moment.Moment) {
        const clonedSince = this.matSinceDatePicker.clone();
        const sincePlus30Days = clonedSince.add(30, 'days').endOf('day');

        if (this.matTillDatePicker > sincePlus30Days ) {
            this.matTillDatePicker = clonedSince;
        }

        if (this.matTillDatePicker < this.matSinceDatePicker && this.matSinceDatePicker <= this.todayDayEnd) {
            this.matTillDatePicker = this.matSinceDatePicker.clone().endOf('day');
        }

        if (this.showSearchButton === false) {
            this.datepickerSubmit();
        }
    }

    public onSelectTillDate(dateSelected: moment.Moment) {
        const clonedSince = this.matSinceDatePicker.clone();
        const sincePlus30Days = clonedSince.add(30, 'days').endOf('day');

        if (sincePlus30Days < this.matTillDatePicker) {
            const cloneTill = this.matTillDatePicker.clone();
            const tillSubtract30Days = cloneTill.subtract(30, 'days').startOf('day');

            this.matSinceDatePicker = tillSubtract30Days.clone();

        }

        if (this.matTillDatePicker < this.matSinceDatePicker) {
            this.matSinceDatePicker = this.matTillDatePicker.clone().subtract(30, 'days').startOf('day');
        }


        if (this.showSearchButton === false) {
            this.datepickerSubmit();
        }
    }

    // public tillMax(): moment.Moment {
    //     if (!this.matSinceDatePicker) {
    //         return null;
    //     }
    //     const clonedSince = this.matSinceDatePicker.clone();
    //     const sincePlus30Days = clonedSince.add(30, 'days').endOf('day');
    //     const today = moment().endOf('day');
    //     if (sincePlus30Days > today) {
    //         return today;
    //     }
    //     return sincePlus30Days;
    // }


    private selectOption(optionValue: PeriodTypeOption): Promise<PeriodTypeOption> {
        return new Promise((resolve, reject) => {
            const option = this.periodOptions.find((item) => item.id === Number(optionValue));
            if (!option) {
                // could be on initialization where periodOptions are not yes populated
                reject('option not found');
                return;
            }
            this.showDatepicker = false;

            const dt = new PeriodTypesConverter().convert(option.id);

            // Start of week is Sunday (like bq)
            switch (option.id) {
                case PeriodTypes.Custom:
                    this.showDatepicker = true;
                    option.datetimeSince = this.matSinceDatePicker.startOf('day');
                    option.datetimeTill = this.matTillDatePicker.endOf('day');
                    break;
                default:
                    option.datetimeSince = dt.datetimeSince;
                    option.datetimeTill = dt.datetimeTill;
                    break;
            }

            // assign values to datepickers only when not fiscal year
            // (these are company settings based calculations)
            if (option.id !== PeriodTypes.FiscalYearToDate
                 && option.id !== PeriodTypes.PreviousFiscalYear) {
                     this.setCustomDates(option.datetimeSince, option.datetimeTill, false);
            }
            this.selectedOption = option;
            resolve(this.selectedOption);
        });
    }

}
