// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { DatepickerModule } from '../datepicker/datepicker.module';

// COMPONENT
import { PeriodSelectorComponent } from './period-selector.component';

@NgModule({
    imports: [
        SharedModule,
        DatepickerModule
    ],
    declarations: [
        PeriodSelectorComponent
    ],
    exports: [
       PeriodSelectorComponent
    ]
})
export class PeriodSelectorModule {}
