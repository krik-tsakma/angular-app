// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXSK
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { 
    EnergyCorrectionFactor, 
    EnergyCorrectionFactorsRequest, 
    EnergyCorrectionFactorUpdateRequest, 
    EnergyCorrectionFactorCreateCodeOptions, 
    EnergyCorrectionFactorUpdateCodeOptions, 
    EnergyCorrectionFactorsResult
} from './energy-correction-factors.types';

@Injectable()
export class EnergyCorrectionFactorsService {
    private baseURL: string;
    
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = config.get('apiUrl') + '/api/EnergyCorrectionFactors';
        }

    public get(req: EnergyCorrectionFactorsRequest): Observable<EnergyCorrectionFactorsResult> {
        const params = new HttpParams({
            fromObject: {
                VehicleID: req.vehicleID ? String(req.vehicleID) : '',
                VehicleTypeID: req.vehicleTypeID ? String(req.vehicleTypeID) : ''
            }
        });
        return this.authHttp
            .get(this.baseURL, { params });
    }


    public create(entity: EnergyCorrectionFactor): Observable<EnergyCorrectionFactorCreateCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as EnergyCorrectionFactorCreateCodeOptions;
                    }
                })
            );
    }


    public update(entity: EnergyCorrectionFactorUpdateRequest): Observable<EnergyCorrectionFactorUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as EnergyCorrectionFactorUpdateCodeOptions;
                    }
                })
            );            
    }

    public delete(id: number): Observable<number> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((res) => res.status)
            ); 
    }
}
