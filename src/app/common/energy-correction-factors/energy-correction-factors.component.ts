import { MatDialogRef } from '@angular/material/dialog';
// FRAMEWORK
import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

// MOMENT
import moment from 'moment';

// SERVICES
import { SnackBarService } from '../../shared/snackbar.service';
import { EnergyCorrectionFactorsService } from './energy-correction-factors.service';
import { ConfirmDialogService } from '../../shared/confirm-dialog/confirm-dialog.service';

// TYPES
import {
    EnergyCorrectionFactorsRequest,
    EnergyCorrectionFactor,
    EnergyCorrectionFactorsResult,
    EnergyCorrectionFactorEditRequest
} from './energy-correction-factors.types';
import { TableColumnSetting, TableActionOptions, TableCellFormatOptions, TableActionItem } from '../../shared/data-table/data-table.types';
import { EditEnergyCorrectionFactorComponent } from './edit/edit-correction-factor.component';
import { ConfirmationDialogParams } from '../../shared/confirm-dialog/confirm-dialog-types';

@Component({
    selector: 'energy-correction-factors',
    templateUrl: 'energy-correction-factors.html'
})

export class EnergyCorrectionFactorsComponent implements OnChanges {
    @Input() public vehicleID?: number;
    @Input() public vehicleTypeID?: number;

    public data: EnergyCorrectionFactor[];
    public loading: boolean;
    public tableSettings: TableColumnSetting[];

    private editRequest: EnergyCorrectionFactorEditRequest = {} as EnergyCorrectionFactorEditRequest;
    private editDialogRef: any;

    constructor(private snackBar: SnackBarService,
                private service: EnergyCorrectionFactorsService,
                private dialogService: ConfirmDialogService,
                public dialog: MatDialog
            ) {
        this.tableSettings =  [
            {
                primaryKey: 'startTimestamp',
                header: 'energy_category.correction_factors.start_date',
                format: TableCellFormatOptions.date
            },
            {
                primaryKey: 'primaryValue',
                header:  'energy_category.correction_factors.value',
                format: TableCellFormatOptions.string
            },
            // {
            //     primaryKey: 'secondaryValue',
            //     header:  ' ',
            //     visible: false
            // },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete]
            }
        ];
    }

    public ngOnChanges(): void {
        this.get();
    }

    public onAdd() {
        this.editRequest.entity = {
            vehicleID: this.vehicleID,
            vehicleTypeID: this.vehicleTypeID,
            startTimestamp: moment().toISOString()
        } as EnergyCorrectionFactor;
        this.showEditingDialog(this.editRequest);
    }

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.edit:
                this.editRequest.entity = item.record;
                this.showEditingDialog(this.editRequest);
                break;
            case TableActionOptions.delete:
                this.dialogService
                    .confirm({
                        title: 'form.actions.delete',
                        message: 'form.warnings.are_you_sure',
                        submitButtonTitle: 'form.actions.delete'
                    } as ConfirmationDialogParams)
                    .subscribe((response) => {
                        if (response === 'yes') {
                            this.delete(id);
                        }
                    });
                break;
            default:
                break;

        }
    }

    private get() {
        this.loading = true;
        this.data = [];

        const req = {
            vehicleID: this.vehicleID,
            vehicleTypeID: this.vehicleTypeID
        } as EnergyCorrectionFactorsRequest;

        this.service
            .get(req)
            .subscribe((res: EnergyCorrectionFactorsResult) => {
                this.loading = false;
                this.data = res.items;
            },
            (err) => {
                this.snackBar.open('data.error', 'form.actions.close');
                this.loading = false;
            });
    }

    // ----------------
    // DELETE
    // ----------------
    private delete(id: number) {
        this.loading = true;

        this.service
            .delete(id)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 200) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // ----------------
    // DIALOG
    // ----------------

    private showEditingDialog(item: EnergyCorrectionFactorEditRequest) {
        const dialogRef = this.dialog.open(EditEnergyCorrectionFactorComponent, {
            data: item
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result === 'ok') {
                this.get();
            }
        });
    }
}
