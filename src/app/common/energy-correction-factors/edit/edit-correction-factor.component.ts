// FRAMEWORK
import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

// RXJS
import { Subscription } from 'rxjs';

// MOMENT
import moment from 'moment';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { EnergyCorrectionFactorsService } from '../energy-correction-factors.service';

// TYPES
import {
    EnergyCorrectionFactor,
    EnergyCorrectionFactorCreateCodeOptions,
    EnergyCorrectionFactorUpdateCodeOptions,
    EnergyCorrectionFactorUpdateRequest,
    EnergyCorrectionFactorEditRequest
} from '../energy-correction-factors.types';

@Component({
    selector: 'edit-correction-factor',
    templateUrl: 'edit-correction-factor.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-correction-factor.less']
})

export class EditEnergyCorrectionFactorComponent {
    public loading: boolean;
    public action: string;
    public entity: EnergyCorrectionFactor = {} as EnergyCorrectionFactor;
    public serviceSubscriber: Subscription;
    public startTimestamp: moment.Moment;

    constructor(
        protected translator: CustomTranslateService,
        protected snackBar: SnackBarService,
        private service: EnergyCorrectionFactorsService,
        public dialogRef: MatDialogRef<EditEnergyCorrectionFactorComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any

    ) {
        this.entity = Object.assign({}, data.entity);
        this.startTimestamp = moment(this.entity.startTimestamp);
    }

    // =========================
    // EVENTS
    // =========================

    public closeDialog(result?: string) {
        this.dialogRef.close(result);
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: EnergyCorrectionFactor, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.entity.startTimestamp = this.startTimestamp.startOf('day').format('YYYY-MM-DD HH:mm');

        // This is create
        if (typeof(this.entity.id) === 'undefined' || this.entity.id === 0) {
            this.serviceSubscriber = this.service
                .create(this.entity)
                .subscribe((res: EnergyCorrectionFactorCreateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.closeDialog('ok');
                    } else {
                        const message = this.getCreateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else {
            // This is an update
            this.service
                .update({
                    id: this.entity.id,
                    startTimestamp: this.entity.startTimestamp,
                    primaryValue: this.entity.primaryValue
                } as EnergyCorrectionFactorUpdateRequest)
                .subscribe((res: EnergyCorrectionFactorUpdateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.closeDialog('ok');
                    } else {
                        const message = this.getUpdateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        }
    }

    // =========================
    // RESULTS
    // =========================
    private getCreateResult(code: EnergyCorrectionFactorCreateCodeOptions): string {
        let message = '';
        switch (code) {
            case EnergyCorrectionFactorCreateCodeOptions.EntityRequired:
                message = 'form.errors.not_found';
                break;
            case EnergyCorrectionFactorCreateCodeOptions.StartDateOvelapping:
                message = 'energy_category.correction_factors.errors.start_date_overlapping';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUpdateResult(code: EnergyCorrectionFactorUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case EnergyCorrectionFactorUpdateCodeOptions.EnergyCorrectionFactorNotFound:
                message = 'form.errors.not_found';
                break;
            case EnergyCorrectionFactorUpdateCodeOptions.StartDateOvelapping:
                message = 'energy_category.correction_factors.errors.start_date_overlapping';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
