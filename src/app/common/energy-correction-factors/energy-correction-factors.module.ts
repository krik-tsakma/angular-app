// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// MODULES
import { DatepickerModule } from '../datepicker/datepicker.module';

// SERVICES
import { EnergyCorrectionFactorsService } from './energy-correction-factors.service';

// COMPONENTS
import { EnergyCorrectionFactorsComponent } from './energy-correction-factors.component';
import { EditEnergyCorrectionFactorComponent } from './edit/edit-correction-factor.component';

@NgModule({
    imports: [
        SharedModule,
        DatepickerModule
    ],
    declarations: [
        EnergyCorrectionFactorsComponent,
        EditEnergyCorrectionFactorComponent
    ],
    exports: [
        EnergyCorrectionFactorsComponent
    ],
    providers: [
        EnergyCorrectionFactorsService
    ],
    entryComponents: [
        EditEnergyCorrectionFactorComponent
    ]
})
export class EnergyCorrectionFactorsModule {}
