
import { KeyName } from '../key-name';

export interface EnergyCorrectionFactor {
    id: number;
    startTimestamp: string;
    vehicleID?: number;
    vehicleTypeID?: number;
    primaryValue: number;
}

// ================
// GET
// ================

export interface EnergyCorrectionFactorsRequest {
    vehicleID?: number;
    vehicleTypeID?: number;
}

export interface EnergyCorrectionFactorsResult {
    items: EnergyCorrectionFactor[];
    energyTypeName: string;
    secondaryEnergyTypeName: string;
}

// ================
// EDIT
// ================

export interface EnergyCorrectionFactorEditRequest {
    entity: EnergyCorrectionFactor;
}

export interface EnergyCorrectionFactorUpdateRequest {
    id: number;
    primaryValue: number;
    startTimestamp: string;
}

// ================
// CODES
// ================

export enum EnergyCorrectionFactorCreateCodeOptions {
    Created = 0,
    EntityRequired = 1,
    StartDateOvelapping = 2
}

export enum EnergyCorrectionFactorUpdateCodeOptions {
    Updated = 0,
    EnergyCorrectionFactorNotFound = 1,
    StartDateOvelapping = 2
}

