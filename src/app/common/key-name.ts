// Used to represent a key/name object.
export interface KeyName {
    // The id  i.e. 0
    id: any;
    // The name  i.e Test
    name: string;
    // Non-mandatory type for disabled drop down option
    enabled?: boolean;
    // Non-mandatory type for ui term
    term?: string;
}
