// FRAMEWORK
import { 
    Component, EventEmitter, Input, OnChanges, SimpleChanges,
    Output, ViewEncapsulation 
} from '@angular/core';

// SERVICES
import { AccessRightsService } from './access-rights.service';

// TYPES
import { Module, Property } from '../../auth/acl.types';

@Component({
    selector: 'access-rights',
    templateUrl: 'access-rights.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['access-rights.less'],
})

export class AccessRightsComponent implements OnChanges {
    @Input() public roleID?: number;
    @Input() public modelData: Module[] = [];
    @Input() public showSelectAll: boolean;
    @Output() public modelDataChange: EventEmitter<Module[]> = new EventEmitter();

    public selectedAll: boolean;
    private companyOrRoleRights: Module[] = [];

    constructor(private service: AccessRightsService) { 
        // foo
    }

    public ngOnChanges(changes: SimpleChanges) {
        const dataChange = changes['modelData'];
        if (dataChange && !dataChange.previousValue && dataChange.currentValue) {
            this.get();           
        } else {
            // TODO
        }

        const roleChange = changes['roleID'];
        if (roleChange) {
            this.get();
        }
    }


    public onChangeModuleOption(module: Module) {
        if (module.hasAccess === false && module.properties.length > 0) {
            module.properties.forEach((p) => {
                p.hasAccess = false;
            });
        }
        this.modelDataChange.emit(this.modelData);
        this.checkIfAllSelected();
    }
    

    public onChangePropertyOption(module: Module, property: Property) {
        if (property.hasAccess === true) {
            if (module) {
                module.hasAccess = true;
            }
        }
        this.modelDataChange.emit(this.modelData);
        this.checkIfAllSelected();
    }


    public moduleCompanyOrRoleHas(module: Module): boolean { 
        const exist = this.companyOrRoleRights.find((x) => x.id === module.id);
        if (exist) {
            return exist.hasAccess;
        }
        return false;
    }

    public propertyCompanyOrRoleHas(property: Property): boolean {
        const module = this.companyOrRoleRights.find((x) => x.id === property.moduleID);
        if (module) {
            const prop = module.properties.find((p) => p.id === property.id);
            if (prop) {
                return prop.hasAccess;
            }
        }
        return false;
    }


    public onSelectionAll() {
        if (this.modelData.length > 0) {
            this.modelData.forEach((model) => {
                model.hasAccess = this.selectedAll;
                model.properties.forEach((prop) => {
                    prop.hasAccess = this.selectedAll;
                });
            });
            this.compareToCompany();
            this.modelDataChange.emit(this.modelData);
            this.checkIfAllSelected();
        }
    }

    private get() {
        this.service
            .get(this.roleID)
            .subscribe((res: Module[]) => {
                this.companyOrRoleRights = JSON.parse(JSON.stringify(res));
                if (!this.modelData) {
                    this.modelData = JSON.parse(JSON.stringify(res));                    
                } 
                this.compareToCompany();
            },
            (err) => {
               // error
            }
        );
    }

    private compareToCompany() {
        this.modelData.forEach((m) => {
            if (this.moduleCompanyOrRoleHas(m) === false) {
                m.hasAccess = false;
            }
            m.properties.forEach((p) => {
                if (this.propertyCompanyOrRoleHas(p) === false) {
                    p.hasAccess = false;
                }
            });
        });
        this.modelDataChange.emit(this.modelData);
        this.checkIfAllSelected();
    }

    private checkIfAllSelected() {
        const moduleHelper = this.modelData.filter((m) => m.hasAccess);
        if (moduleHelper.length === this.modelData.length) {
            const notAllChecked: any[] = []; 
            moduleHelper.forEach((m) => {
                const propertyHelper = m.properties.filter((p) => p.hasAccess);
                if (propertyHelper.length !== m.properties.length) {
                    notAllChecked.push('not all');
                }                
            });
            this.selectedAll = notAllChecked.length === 0;
        } else {
            this.selectedAll = false;
        }
    }
}
