
// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';


// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
import { 
    ModuleOptions, 
    AdminPropertyOptions, 
    EVOperationsPropertyOptions,
    TripReviewPropertyOptions
} from '../../auth/acl.types';

@Pipe({
    name: 'myAccessRightsPropertyName',
    pure: false
})

export class AccessRightsPropertyNamePipe implements PipeTransform {
    constructor(private translate: CustomTranslateService) {
        // foo
    }

    public transform(module: ModuleOptions, property: any): string {
        let term = '';
        switch (module) {
            case ModuleOptions.EVOperations:
                term = this.getEvOperationsProperty(property);
                break;
            case ModuleOptions.Admin:
                term = this.getAdminProperty(property);
                break;
            case ModuleOptions.TripReview:
                term = this.getTripReviewProperty(property);
                break;
            case ModuleOptions.Dashboard:
            case ModuleOptions.FollowUp:
            case ModuleOptions.Events:
            case ModuleOptions.Reports:
            case ModuleOptions.Bookings:
            case ModuleOptions.ChargePointManager:
            case ModuleOptions.FiscalTripReview:
            case ModuleOptions.DriverTakeOver:
            default:
                term = property;
                break;
        }
        return this.translate.instant(term) as string;
    }

    private getEvOperationsProperty(prop: EVOperationsPropertyOptions): string {
        let term = '';
        switch (prop) {
            case EVOperationsPropertyOptions.Vehicles:
                term = 'sidebar.ev_operations.vehicles';
                break;
            case EVOperationsPropertyOptions.ChargingMonitor:
                term = 'sidebar.ev_operations.charging_monitor';
                break;
            default:
                term = prop;
                break;
        }

        return term;
    }

    private getTripReviewProperty(prop: TripReviewPropertyOptions): string {
        let term = '';
        switch (prop) {
            case TripReviewPropertyOptions.ViewTrips:
                term = 'trip_review.catefories.view_trips';
                break;
            case TripReviewPropertyOptions.ReassignDriver:
                term = 'trip_review.catefories.reassign_driver';
                break;
            default:
                term = prop;
                break;
        }
        return term;
    }

    private getAdminProperty(prop: AdminPropertyOptions): string {
        let term = '';
        switch (prop) {
            case AdminPropertyOptions.Company:
                term = 'admin.categories.company';
                break;
            case AdminPropertyOptions.Scores:
                term = 'admin.categories.scores';
                break;
            case AdminPropertyOptions.BookNGo:
                term = 'admin.categories.book_n_go';
                break;
            case AdminPropertyOptions.Users:
                term = 'admin.categories.users';
                break;
            case AdminPropertyOptions.Vehicles:
                term = 'admin.categories.vehicles';
                break;
            case AdminPropertyOptions.Drivers:
                term = 'admin.categories.drivers';
                break;
            case AdminPropertyOptions.Locations:
                term = 'admin.categories.locations';
                break;
            case AdminPropertyOptions.AssetLinks:
                term = 'admin.categories.devices.links';
                break;
            case AdminPropertyOptions.ChargePoints:
                term = 'admin.categories.charge_points';
                break;
            default:
                term = prop;
                break;
        }

        return term;
    }
}
