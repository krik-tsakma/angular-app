// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';


// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
import { ModuleOptions } from '../../auth/acl.types';

@Pipe({
    name: 'myAccessRightsModuleName',
    pure: false
})

export class AccessRightsModuleNamePipe implements PipeTransform {
    constructor(private translate: CustomTranslateService) {
        // foo
    }

    public transform(module: ModuleOptions): string {
        let term = '';
        switch (module) {
            case ModuleOptions.Dashboard:
                term = 'sidebar.dashboard';
                break;
            case ModuleOptions.TripReview:
                term = 'sidebar.trip_review';
                break;
            case ModuleOptions.FollowUp:
                term = 'sidebar.follow_up';
                break;
            case ModuleOptions.Events:
                term = 'sidebar.events';
                break;
            case ModuleOptions.EVOperations:
                term = 'sidebar.ev_operations';
                break;
            case ModuleOptions.Bookings:
                term = 'sidebar.book_n_go';
                break;
            case ModuleOptions.Reports:
                term = 'sidebar.reporting.reports';
                break;
            case ModuleOptions.Admin:
                term = 'sidebar.admin';
                break;
            case ModuleOptions.ChargePointManager:
                term = 'sidebar.ev_operations.charge_points_manager';
                break;
            case ModuleOptions.FiscalTripReview:
                term = 'sidebar.fiscal_trips';
                break;
            case ModuleOptions.DriverTakeOver:
                term = 'sidebar.driver_take_over';
                break;
            default:
                term = module;
                break;
        }
        return this.translate.instant(term) as string;
    }
}
