// FRAMEWORK
import { NgModule } from '@angular/core';

// PIPES
import { AccessRightsModuleNamePipe } from './access-rights-module-name.pipe';
import { AccessRightsPropertyNamePipe } from './access-rights-property-name.pipe';

@NgModule({
    imports: [
    ],
    declarations: [
        AccessRightsModuleNamePipe,
        AccessRightsPropertyNamePipe
    ],
    exports: [
        AccessRightsModuleNamePipe,
        AccessRightsPropertyNamePipe
    ]
})
export class AccessRightsPipesModule { }
