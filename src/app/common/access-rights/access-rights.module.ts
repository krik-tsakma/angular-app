// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { AccessRightsPipesModule } from './access-rights-pipes.modules';

// SERVICES
import { AccessRightsService } from './access-rights.service';

// COMPONENTS
import { AccessRightsComponent } from './access-rights.component';

@NgModule({
    imports: [
        SharedModule,
        AccessRightsPipesModule
    ],
    declarations: [
        AccessRightsComponent,
    ],
    exports: [
        AccessRightsComponent,
    ],
    providers: [
        AccessRightsService
    ]
})
export class AccessRightsModule { }
