// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { Module } from '../../auth/acl.types';

@Injectable()
export class AccessRightsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/AccessRights';
        }

    public get(roleID?: number) {
        if (roleID) {
            return this.getForRole(roleID);
        } else {
            return this.getForCompany();
        }
    }

    private getForRole(roleID: number): Observable<Module[]> {
        return this.authHttp
            .get(this.baseURL + '/Role/' + roleID);
    }

    private getForCompany(): Observable<Module[]> {
        return this.authHttp
            .get(this.baseURL + '/Company');
    }
} 
