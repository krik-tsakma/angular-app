
export interface AssetLinkWarningMessage {
    link: AssetLinkWarningItem;
    message: string;
}

export interface AssetLinkWarningItem {
    trackingDeviceLabel: string;
    vehicleLabel: string;
    driverLabel: string;
}

export interface AssetLinkSingleRequest {
    vehicleID?: number;
    driverID?: number;
    trackingDeviceID?: number;
}

export enum AssetLinkWarningCodeOptions {
    Found = 0,
    NotFound = -1,
    NotAllowedVehicle = -2,
    NotAllowedDriver = -3,
}
