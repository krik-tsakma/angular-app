// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// PIPES
import { StringFormatPipe } from '../../shared/pipes/stringFormat.pipe';
import { AssetLinkSingleRequest, AssetLinkWarningMessage, AssetLinkWarningItem, AssetLinkWarningCodeOptions } from './asset-link-warning-types';


@Injectable()
export class AssetLinksWarningService {
    private baseURL: string;

    constructor(private translator: CustomTranslateService,
                private stringFormatPipe: StringFormatPipe,
                private authHttp: AuthHttp,
                private config: Config) {
            
            this.baseURL = this.config.get('apiUrl') + '/api/AssetLinksWarning';
    }

    public GetForDevice(id: number): Promise<AssetLinkWarningMessage> {
        return new Promise((resolve, reject) => {
            this.getAssetLink({ trackingDeviceID: id } as AssetLinkSingleRequest)
                .subscribe((res: AssetLinkWarningMessage) => {
                    if (res.link && res.link.trackingDeviceLabel && res.link.trackingDeviceLabel.length > 0) {
                        const text = this.translator.instant('assets.links.tracker.message');
                        if (res.link.trackingDeviceLabel === '') {
                            res.link.trackingDeviceLabel = '-';
                        }
                
                        res.message = this.stringFormatPipe.transform(text, [res.link.vehicleLabel, res.link.driverLabel]);
                    }
                    resolve(res);
                },
                (err) => {
                    reject(this.translator.instant('form.errors.unknown'));
                });

        });
    }

    public GetForVehicle(id: number): Promise<AssetLinkWarningMessage> {
        return new Promise((resolve, reject) => {
            this.getAssetLink({ vehicleID: id } as AssetLinkSingleRequest)
                .subscribe((res: AssetLinkWarningMessage) => {
                    if (res.link && res.link.trackingDeviceLabel && res.link.trackingDeviceLabel.length > 0) {
                        const text = this.translator.instant('assets.links.vehicle.message');
                        if (res.link.driverLabel === '') {
                            res.link.driverLabel = '-';
                        }
                
                        res.message = this.stringFormatPipe.transform(text, [res.link.trackingDeviceLabel, res.link.driverLabel]);
                    }

                    resolve(res);
                },
                (err) => {
                    reject(this.translator.instant('form.errors.unknown'));
                });

        });
    }

    public GetForDriver(id: number): Promise<AssetLinkWarningMessage> {
        return new Promise((resolve, reject) => {
            this.getAssetLink({ driverID: id } as AssetLinkSingleRequest)
                .subscribe((res: AssetLinkWarningMessage) => {
                    if (res.link && res.link.trackingDeviceLabel && res.link.trackingDeviceLabel.length > 0) {
                        const text = this.translator.instant('assets.links.driver.message');
                        if (res.link.vehicleLabel === '') {
                            res.link.vehicleLabel = '-';
                        }
                        res.message = this.stringFormatPipe.transform(text, [res.link.trackingDeviceLabel, res.link.vehicleLabel]);
                    }
                    resolve(res);
                },
                (err) => {
                    reject(this.translator.instant('form.errors.unknown'));
                });
        });
    }


    // Get a single asset link for vehicle or driver
    private getAssetLink(request: AssetLinkSingleRequest): Observable<AssetLinkWarningMessage> {
        const params = new HttpParams({
            fromObject: {
                TrackingDeviceID: request.trackingDeviceID ? request.trackingDeviceID.toString() : '',
                VehicleID: request.vehicleID ? request.vehicleID.toString() : '',
                DriverID: request.driverID ? request.driverID.toString() : ''
            }
        });

        return this.authHttp
                    .get(this.baseURL + '/GetSingle', { params })
                    .pipe(
                        map((res) => {
                            if (typeof(res) === 'number') {
                                let msg = 'form.errors.unknown';
                                const errorCode = res as AssetLinkWarningCodeOptions;
                                switch (errorCode) {
                                    case AssetLinkWarningCodeOptions.NotFound:
                                        msg = null;
                                        break;
                                    case AssetLinkWarningCodeOptions.NotAllowedVehicle: 
                                    case AssetLinkWarningCodeOptions.NotAllowedDriver:
                                        msg = 'assets.links.warning.unauthorized_asset_view';
                                        break;
                                    default: 
                                        break;
                                }
                            
                                return {
                                    message: msg ? this.translator.instant(msg) : null
                                } as AssetLinkWarningMessage;
                            } else {
                                const response = {
                                    link: res as AssetLinkWarningItem
                                } as AssetLinkWarningMessage;

                                return response;
                            } 
                        })               
                    );               
    }
} 
