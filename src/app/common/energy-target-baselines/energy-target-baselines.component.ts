import { EnergyBaselineDeleteRequest } from './energy-target-baselines-types';
// FRAMEWORK
import { Component, Input, OnChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

// MOMENT
import moment from 'moment';

// SERVICES
import { SnackBarService } from '../../shared/snackbar.service';
import { ConfirmDialogService } from '../../shared/confirm-dialog/confirm-dialog.service';
import { EnergyBaselinesService } from './energy-target-baselines.service';

// TYPES
import {
    TableColumnSetting,
    TableActionOptions,
    TableCellFormatOptions,
    TableActionItem
} from '../../shared/data-table/data-table.types';
import { ConfirmationDialogParams } from '../../shared/confirm-dialog/confirm-dialog-types';
import {
    EnergyTargetBaseline,
    EnergyBaselineEditRequest,
    EnergyBaselinesRequest,
    EnergyBaselinesResult
} from './energy-target-baselines-types';
import { EditEnergyBaselineComponent } from './edit/edit-target-baseline.component';

@Component({
    selector: 'energy-target-baselines',
    templateUrl: 'energy-target-baselines.html'
})

export class EnergyTargetBaselinesComponent implements OnChanges {
    @Input() public vehicleID?: number;
    @Input() public vehicleTypeID?: number;

    public data: EnergyTargetBaseline[];
    public loading: boolean;
    public tableSettings: TableColumnSetting[];
    public showTable: boolean;

    private editRequest: EnergyBaselineEditRequest = {} as EnergyBaselineEditRequest;

    constructor(private snackBar: SnackBarService,
                private service: EnergyBaselinesService,
                private dialogService: ConfirmDialogService,
                public dialog: MatDialog
            ) {
        this.tableSettings =  [
            {
                primaryKey: 'startTimestamp',
                header: 'energy_category.target_baselines.start_date',
                format: TableCellFormatOptions.date,
                grouping: true
            },
            {
                primaryKey: 'driveCycleName',
                header: 'energy_category.target_baselines.drive_cycle',
            },
            {
                primaryKey: 'baselineValue',
                header: 'energy_category.target_baselines.baseline_value',
                format: TableCellFormatOptions.string

            },
            {
                primaryKey: 'targetValue',
                header: 'energy_category.target_baselines.target_value',
                format: TableCellFormatOptions.string
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete]
            }
        ];
    }

    public ngOnChanges(): void {
        this.get();
    }

    public onAdd() {
        this.editRequest.entity = {
            vehicleID: this.vehicleID,
            vehicleTypeID: this.vehicleTypeID,
            startTimestamp: moment().toISOString()
        } as EnergyTargetBaseline;
        this.showEditingDialog(this.editRequest);
    }

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const record = item.record as EnergyTargetBaseline;
        switch (item.action) {
            case TableActionOptions.edit:
                this.editRequest.entity = record;
                this.showEditingDialog(this.editRequest);
                break;
            case TableActionOptions.delete:
                this.dialogService
                    .confirm({
                        title: 'form.actions.delete',
                        message: 'form.warnings.are_you_sure',
                        submitButtonTitle: 'form.actions.delete'
                    } as ConfirmationDialogParams)
                    .subscribe((response) => {
                        if (response === 'yes') {
                            this.delete(record.targetID, record.baselineID);
                        }
                    });
                break;
            default:
                break;

        }
    }

    private get() {
        this.loading = true;
        this.data = [];
        const req = {
            vehicleID: this.vehicleID,
            vehicleTypeID: this.vehicleTypeID
        } as EnergyBaselinesRequest;

        this.service
            .get(req)
            .subscribe((res: EnergyBaselinesResult) => {
                this.loading = false;
                this.data = res.items;
            },
            () => {
                    this.snackBar.open('data.error', 'form.actions.close');
                    this.loading = false;
                });
    }

    // ----------------
    // DELETE
    // ----------------
    private delete(targetID: number, baselineID: number) {
        this.loading = true;

        this.service
            .delete({
                targetID,
                baselineID
            } as EnergyBaselineDeleteRequest)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 200) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // ----------------
    // DIALOG
    // ----------------

    private showEditingDialog(item: EnergyBaselineEditRequest) {
        const dialogRef = this.dialog.open(EditEnergyBaselineComponent, {
            data: item
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result === 'ok') {
                this.get();
            }
        });
    }
}
