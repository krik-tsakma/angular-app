// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// MODULES
import { DatepickerModule } from '../datepicker/datepicker.module';

// SERVICES
import { EnergyBaselinesService } from './energy-target-baselines.service';
import { DriveCyclesService } from '../../admin/drive-cycles/drive-cycles.service';

// COMPONENTS
import { EnergyTargetBaselinesComponent } from './energy-target-baselines.component';
import { EditEnergyBaselineComponent } from './edit/edit-target-baseline.component';

@NgModule({
    imports: [
        SharedModule,
        DatepickerModule
    ],
    declarations: [
        EnergyTargetBaselinesComponent,
        EditEnergyBaselineComponent
    ],
    exports: [
        EnergyTargetBaselinesComponent
    ],
    providers: [
        EnergyBaselinesService,
        DriveCyclesService
    ],
    entryComponents: [
        EditEnergyBaselineComponent
    ]
})
export class EnergyTargetBaselinesModule {}
