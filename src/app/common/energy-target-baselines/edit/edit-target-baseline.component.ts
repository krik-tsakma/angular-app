// FRAMEWORK
import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

// RXJS
import { Subscription } from 'rxjs';

// MOMENT
import moment from 'moment';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { EnergyBaselinesService } from '../energy-target-baselines.service';
import { DriveCyclesService } from '../../../admin/drive-cycles/drive-cycles.service';

// TYPES
import {
    EnergyTargetBaseline,
    EnergyBaselineEditRequest,
    EnergyBaselineCreateCodeOptions,
    EnergyBaselineUpdateCodeOptions
} from './../energy-target-baselines-types';
import { KeyName } from '../../key-name';

@Component({
    selector: 'edit-target-baseline',
    templateUrl: 'edit-target-baseline.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-target-baseline.less']
})

export class EditEnergyBaselineComponent {
    public loading: boolean;
    public action: string;
    public entity: EnergyTargetBaseline = {} as EnergyTargetBaseline;
    public serviceSubscriber: Subscription;
    public startTimestamp: moment.Moment;

    public driveCycles: KeyName[];
    public loadingDriveCycles: boolean;

    constructor(
        protected translator: CustomTranslateService,
        protected snackBar: SnackBarService,
        private service: EnergyBaselinesService,
        private driveCyclesService: DriveCyclesService,
        public dialogRef: MatDialogRef<EditEnergyBaselineComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.entity = Object.assign({}, data.entity);
        this.startTimestamp = moment(this.entity.startTimestamp);
        this.getDriveCycles();
    }

    // =========================
    // EVENTS
    // =========================

    public closeDialog(result?: string) {
        this.dialogRef.close(result);
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: EnergyTargetBaseline, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.entity.startTimestamp = this.startTimestamp.startOf('day').format('YYYY-MM-DD HH:mm');

        // This is create
        if (typeof(this.entity.targetID) === 'undefined' ||
            this.entity.targetID === 0 ||
            typeof(this.entity.baselineID) === 'undefined' ||
            this.entity.baselineID === 0) {
            this.serviceSubscriber = this.service
                .create(this.entity)
                .subscribe((res: EnergyBaselineCreateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.closeDialog('ok');
                    } else {
                        const message = this.getCreateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else {
            // This is an update
            this.service
                .update(this.entity)
                .subscribe((res: EnergyBaselineUpdateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.closeDialog('ok');
                    } else {
                        const message = this.getUpdateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        }
    }

    private getDriveCycles() {
        this.loadingDriveCycles = true;
        this.driveCyclesService
            .get()
            .subscribe((res: KeyName[]) => {
                this.driveCycles = res;
                this.driveCycles.map((dc) => { dc.enabled = true; });
                this.loadingDriveCycles = false;
                if (!this.entity.driveCycleID) {
                    this.entity.driveCycleID = this.driveCycles[0].id;
                }
            },
            (err) => {
                this.snackBar.open('data.error', 'form.actions.close');
                this.loadingDriveCycles = false;
            });
    }

    // =========================
    // RESULTS
    // =========================
    private getCreateResult(code: EnergyBaselineCreateCodeOptions): string {
        let message = '';
        switch (code) {
            case EnergyBaselineCreateCodeOptions.EntityRequired:
                message = 'form.errors.not_found';
                break;
            case EnergyBaselineCreateCodeOptions.DriveCycleNotFound:
                message = 'energy_category.target_baselines.errors.drive_cycle';
                break;
            case EnergyBaselineCreateCodeOptions.StartDateOvelapping:
                message = 'energy_category.target_baselines.errors.start_date_overlapping';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUpdateResult(code: EnergyBaselineUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case EnergyBaselineUpdateCodeOptions.TargetNotFound:
            case EnergyBaselineUpdateCodeOptions.BaselineNotFound:
                message = 'form.errors.not_found';
                break;
            case EnergyBaselineUpdateCodeOptions.StartDateOvelapping:
                message = 'energy_category.target_baselines.errors.start_date_overlapping';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
