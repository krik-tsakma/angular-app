// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXSK
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';
import {
    EnergyBaselinesRequest,
    EnergyBaselinesResult,
    EnergyTargetBaseline,
    EnergyBaselineCreateCodeOptions,
    EnergyBaselineUpdateCodeOptions,
    EnergyBaselineDeleteRequest
} from './energy-target-baselines-types';

// TYPES


@Injectable()
export class EnergyBaselinesService {
    private baseURL: string;

    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = config.get('apiUrl') + '/api/EnergyTargetsBaselines';
        }

    public get(req: EnergyBaselinesRequest): Observable<EnergyBaselinesResult> {
        const params = new HttpParams({
            fromObject: {
                VehicleID: req.vehicleID ? String(req.vehicleID) : '',
                VehicleTypeID: req.vehicleTypeID ? String(req.vehicleTypeID) : ''
            }
        });
        return this.authHttp
            .get(this.baseURL, { params });
    }


    public create(entity: EnergyTargetBaseline): Observable<EnergyBaselineCreateCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as EnergyBaselineCreateCodeOptions;
                    }
                })
            );
    }


    public update(entity: EnergyTargetBaseline): Observable<EnergyBaselineUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as EnergyBaselineUpdateCodeOptions;
                    }
                })
            );
    }

    public delete(req: EnergyBaselineDeleteRequest): Observable<number> {
        const params = new HttpParams({
            fromObject: {
                TargetID: req.targetID.toString(),
                BaselineID: req.baselineID.toString()
            }
        });

        return this.authHttp
            .delete(this.baseURL, { params, observe: 'response' })
            .pipe(
                map((res) => res.status)
            );
    }
}
