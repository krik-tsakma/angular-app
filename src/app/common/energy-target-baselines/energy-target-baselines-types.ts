
export interface EnergyTargetBaseline {
    startTimestamp: string;
    vehicleID?: number;
    vehicleTypeID?: number;
    targetID: number;
    targetValue: number;
    baselineID: number;
    baselineValue: number;
    driveCycleID: number;
    driveCycleName: string;
}

// ================
// GET
// ================

export interface EnergyBaselinesRequest {
    vehicleID?: number;
    vehicleTypeID?: number;
}

export interface EnergyBaselinesResult {
    items: EnergyTargetBaseline[];
}

// ================
// EDIT
// ================

export interface EnergyBaselineEditRequest {
    entity: EnergyTargetBaseline;
}

export interface EnergyBaselineDeleteRequest {
    targetID: number;
    baselineID: number;
}

// ================
// CODES
// ================

export enum EnergyBaselineCreateCodeOptions {
    Created = 0,
    EntityRequired = 1,
    DriveCycleNotFound = 2,
    StartDateOvelapping = 3
}

export enum EnergyBaselineUpdateCodeOptions {
    Updated = 0,
    TargetNotFound = 1,
    BaselineNotFound = 2,
    StartDateNotMatching = 3,
    StartDateOvelapping = 4
}

export enum EnergyBaselineDeleteCodeOptions {
    Removed = 0,
    TargetNotFound = 1,
    BaselineNotFound = 2,
    StartDateNotMatching = 3,
}
