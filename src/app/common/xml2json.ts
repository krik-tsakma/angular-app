/*	This work is licensed under Creative Commons GNU LGPL License.

	License: http://creativecommons.org/licenses/LGPL/2.1/
    Version: 0.9
	Author:  Stefan Goessner/2006
	Web:     http://goessner.net/ 
*/

/* tslint:disable */

export class xml2json {
    public get(passedXml, tab) {
        const X = {
            toObj: (xml) => {
                let o = {};
                if (xml.nodeType == 1) {   // element node ..

                    if (xml.attributes.length) {  // element with attributes  ..                       
                        for (const i of xml.attributes) {
                            o['@' + i.nodeName] = (i.nodeValue || '').toString();
                        }
                    }

                    if (xml.firstChild) { // element has child nodes ..
                        let textChild = 0;
                        let cdataChild = 0;
                        let hasElementChild = false;

                        for (let n = xml.firstChild; n; n = n.nextSibling) {
                            if (n.nodeType == 1) {
                                hasElementChild = true;
                            } else if (n.nodeType == 3 && n.nodeValue.match(/[^ \f\n\r\t\v]/)) {
                                textChild++; // non-whitespace text
                            } else if (n.nodeType == 4) {
                                cdataChild++;
                            } // cdata section node
                        }
                        if (hasElementChild) {
                            if (textChild < 2 && cdataChild < 2) { // structured element with evtl. a single text or/and cdata node ..
                                X.removeWhite(xml);
                                for (let n = xml.firstChild; n; n = n.nextSibling) {
                                    if (n.nodeType == 3) { // text node
                                        o['#text'] = X.escape(n.nodeValue);
                                    } else if (n.nodeType == 4) {  // cdata node
                                        o['#cdata'] = X.escape(n.nodeValue);
                                    } else if (o[n.nodeName]) {  // multiple occurence of element ..
                                        if (o[n.nodeName] instanceof Array) {
                                            o[n.nodeName][o[n.nodeName].length] = X.toObj(n);
                                        } else {
                                            o[n.nodeName] = [o[n.nodeName], X.toObj(n)];
                                        }
                                    } else { // first occurence of element..
                                        o[n.nodeName] = X.toObj(n);
                                    }
                                }
                            } else { // mixed content
                                if (!xml.attributes.length) {
                                    o = X.escape(X.innerXml(xml));
                                } else {
                                    o['#text'] = X.escape(X.innerXml(xml));
                                }
                            }
                        } else if (textChild) { // pure text
                            if (!xml.attributes.length) {
                                o = X.escape(X.innerXml(xml));
                            } else {
                                o['#text'] = X.escape(X.innerXml(xml));
                            }
                        } else if (cdataChild) { // cdata
                            if (cdataChild > 1) {
                                o = X.escape(X.innerXml(xml));
                            } else {
                                for (let n = xml.firstChild; n; n = n.nextSibling) {
                                    o['#cdata'] = X.escape(n.nodeValue);
                                }
                            }
                        }
                    }
                    if (!xml.attributes.length && !xml.firstChild) {
                        o = null;
                    }
                } else if (xml.nodeType == 9) { // document.node
                    o = X.toObj(xml.documentElement);
                } else {
                    alert('unhandled node type: ' + xml.nodeType);
                }
                return o;
            },
            toJson: (o, name, ind) => {
                let myJson = name ? ('\"' + name + '\"') : '';
                if (o instanceof Array) {                    
                    for (let i of o) {
                        i = X.toJson(i, '', ind + '\t');
                    }
                    myJson += (name ? ':[' : '[') + (o.length > 1 ? ('\n' + ind + '\t' + o.join(',\n' + ind + '\t') + '\n' + ind) : o.join('')) + ']';
                } else if (o == null) {
                    myJson += (name && ':') + 'null';
                } else if (typeof (o) == 'object') {
                    const arr = [];
                    for (const m in o) {
                        arr[arr.length] = X.toJson(o[m], m, ind + '\t');
                    }
                    myJson += (name ? ':{' : '{') + (arr.length > 1 ? ('\n' + ind + '\t' + arr.join(',\n' + ind + '\t') + '\n' + ind) : arr.join('')) + '}';
                } else if (typeof (o) == 'string') {
                    myJson += (name && ':') + '\"' + o.toString() + '\"';
                } else {
                    myJson += (name && ':') + o.toString();
                }
                return myJson;
            },
            innerXml: (node) => {
                let s = '';
                if ('innerHTML' in node) {
                    s = node.innerHTML;
                } else {
                    const asXml = (n) => {
                        let tag = '';
                        if (n.nodeType == 1) {
                            tag += '<' + n.nodeName;                           
                            for (const i of n.attributes) {
                                tag += ' ' + i.nodeName + '=\"' + (i.nodeValue || '').toString() + '\"';
                            }

                            if (n.firstChild) {
                                tag += '>';
                                for (let c = n.firstChild; c; c = c.nextSibling) {
                                    tag += asXml(c);
                                }                                
                                tag += '</' + n.nodeName + '>';
                            } else {
                                tag += '/>';
                            }
                        } else if (n.nodeType == 3) {
                            tag += n.nodeValue;
                        } else if (n.nodeType == 4) {
                            tag += '<![CDATA[' + n.nodeValue + ']]>';
                        }
                        return tag;
                    };
                    for (let c = node.firstChild; c; c = c.nextSibling) {
                        s += asXml(c);
                    }
                }
                return s;
            },
            escape: (txt) => {
                return txt.replace(/[\\]/g, "\\\\")
                        .replace(/[\"]/g, '\\"')
                        .replace(/[\n]/g, '\\n')
                        .replace(/[\r]/g, '\\r');
            },
            removeWhite: (e) => {
                e.normalize();
                for (let n = e.firstChild; n; ) {
                    if (n.nodeType == 3) {  // text node
                        if (!n.nodeValue.match(/[^ \f\n\r\t\v]/)) { // pure whitespace text node
                            const nxt = n.nextSibling;
                            e.removeChild(n);
                            n = nxt;
                        } else {
                            n = n.nextSibling;
                        }
                    } else if (n.nodeType == 1) {  // element node
                        X.removeWhite(n);
                        n = n.nextSibling;
                    } else {                      // any other node
                        n = n.nextSibling;
                    }
                }
                return e;
            }
        };
    
        passedXml = this.parseXml(passedXml);
        if (passedXml.nodeType == 9) { // document node
            passedXml = passedXml.documentElement;
        }
        const json = X.toJson(X.toObj(X.removeWhite(passedXml)), passedXml.nodeName, '\t');
        return '{\n' + tab + (tab ? json.replace(/\t/g, tab) : json.replace(/\t|\n/g, '')) + '\n}';
    }

    private parseXml(xmlStr) {
        if (typeof window['DOMParser'] != 'undefined') {
            return (new window['DOMParser']()).parseFromString(xmlStr, 'text/xml');
        } else if (typeof window['ActiveXObject'] != 'undefined' &&
            new window['ActiveXObject']('Microsoft.XMLDOM')) {
            const xmlDoc = new window['ActiveXObject']('Microsoft.XMLDOM');
            xmlDoc.async = 'false';
            xmlDoc.loadXML(xmlStr);
            return xmlDoc;
        } else {
            throw new Error('No XML parser found');
        }
    }
}

/* tslint:enable */
