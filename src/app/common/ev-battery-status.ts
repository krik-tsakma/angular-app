import { TripEvStatuses } from './entities/status-types';

export class BatteryLevel {
    /*
    * Selects the proper color scheme based on battery soc
    * @param soc
    */
    public static map(batteryLevel: TripEvStatuses): string {
        //  low
        switch (batteryLevel) {
            case TripEvStatuses.BatteryLow:
                return 'low';
            case TripEvStatuses.BatteryMedium:
                return 'medium';
            case TripEvStatuses.BatteryFull:
                return 'high';
        }
        return 'low';
    }

}
