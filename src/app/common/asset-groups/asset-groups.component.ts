// FRAMEWORK
import {
    Component, EventEmitter, Input, OnInit, OnChanges,
    Output, SimpleChanges, ViewEncapsulation
} from '@angular/core';

// SERVICES
import { AssetGroupsService } from '../../admin/asset-groups/asset-groups.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
import { AssetGroupsRequest } from '../../admin/asset-groups/asset-groups.types';
import { AssetGroupTypes, AssetGroupItem } from '../../admin/asset-groups/asset-groups.types';

@Component({
    selector: 'asset-groups',
    templateUrl: './asset-groups.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./asset-groups.less'],
    providers: [AssetGroupsService]
})

export class AssetGroupsComponent implements OnInit, OnChanges {
    @Input() public htmlID: string;
    @Input() public type: AssetGroupTypes;
    @Input() public includeUngrouped?: boolean;
    @Input() public showAllOption?: boolean;
    @Input() public showNoneOption?: boolean;
    @Input() public showCustomOption?: string;
    @Input() public disabled?: number;
    @Input() public selected?: number;
    // Callback function on group initialization
    @Output() public notifyOnInit: EventEmitter<AssetGroupItem[]> =
        new EventEmitter<AssetGroupItem[]>();
    // Callback function on group change
    @Output() public notifyOnChange: EventEmitter<AssetGroupItem> =
        new EventEmitter<AssetGroupItem>();
    @Input() public fetchData: boolean;
    @Input() public data: AssetGroupItem[] = new Array<AssetGroupItem>();

    public loading: boolean;
    public selectedOption: number;

    private translations: string[] = [];

    constructor(private translate: CustomTranslateService,
                private service: AssetGroupsService) { 
                    translate
                        .get(['assets.groups.all', 'form.actions.none'])
                        .subscribe((res) => { 
                            this.translations = res;
                        });
                }


    public ngOnInit() {
        this.get();
    }

    public ngOnChanges(changes: SimpleChanges) {
        const changeType = changes['type'];
        if (changeType && this.fetchData === true && changeType.firstChange === false) {
            this.get();
        }
    }

    public onChangeGroup(groupID?: number) {
        groupID = groupID !== null ? Number(groupID) : null;
        if (Number.isNaN(groupID) || groupID === 0) {
            groupID = null;
        }
        
        const group = this.data.find((item) => item.id === groupID);
        if (group) {
            this.notifyOnChange.emit(group);
            this.selectedOption = group.id;
        }
    }

    public select(id?: number) {
        this.selectedOption = id;
    }

    private get(): void {
        if (this.fetchData) {
            this.loading = true;
            this.service
                .get({ type: this.type, includeUngrouped: this.includeUngrouped } as AssetGroupsRequest)
                .subscribe((groups) => {
                    this.data = groups.map((group) => {
                        group.enabled = true;
                        return group;
                    });
                
                    // after groups are fetched get the initial data
                    this.notifyOnInit.emit(this.data);
                    this.loading = false;
                    
                    // if all option is not present set to true
                    if (this.showAllOption === undefined) {
                        this.showAllOption = true;
                    } 
                    // if all option is present add to the list
                    if (this.showAllOption === true) {
                        this.data.unshift({ 
                            id: null, 
                            name: this.translations['assets.groups.all'], 
                            enabled: true,
                            isDefault: false 
                        });
                    }


                    // if none option is not present set to false
                    if (this.showNoneOption === undefined) {
                        this.showNoneOption = false;
                    }

                    // if none option is present add to the list
                    if (this.showNoneOption === true) {
                        this.data.unshift({ id: -1, 
                            name: this.translations['form.actions.none'], 
                            enabled: true,  
                            isDefault: false 
                        });
                    }
              

                    if (this.showCustomOption !== undefined && this.showNoneOption === false && this.showAllOption === false) {
                        this.data.unshift({ 
                            id: -2, 
                            name: this.translate.instant(this.showCustomOption), 
                            enabled: false,  
                            isDefault: false 
                        });
                    } 

                    // if no value is selected, the select the first value on the list
                    if ((this.selected === null || Number(this.selected) === 0 || Number.isNaN(this.selected)) 
                        && this.data.length > 0) {   
                        if (this.showAllOption === true) {   
                            this.onChangeGroup(null);
                        } else {
                            this.onChangeGroup(this.data[0].id);
                        }
                    } else {
                        this.onChangeGroup(this.selected);
                    }
            });
        }
    }
}
