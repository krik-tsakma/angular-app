import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AssetGroupsComponent } from './asset-groups.component';

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        AssetGroupsComponent
    ],
    exports: [
       AssetGroupsComponent
    ]
})
export class  AssetGroupsModule {}
