import { KeyName } from './key-name';

export enum FileUploadResultCodeOptions {
    Ok = 0,
    Null = -1,
    Empty = -2,
    InvalidType = -3,
    MaxSize = -4,
    MinWidth = -5,
    
}

export interface FileUploadResult {
    code: FileUploadResultCodeOptions;
    url: string;
}

export let FileUploadErrorMessages: KeyName[] = [
    {
        id: FileUploadResultCodeOptions.Null,
        name: 'null',
        term: 'file_upload.errors.null'
    },
    {
        id: FileUploadResultCodeOptions.Empty,
        name: 'empty',
        term: 'file_upload.errors.empty'
    },
    {
        id: FileUploadResultCodeOptions.InvalidType,
        name: 'invalid_type',
        term: 'file_upload.errors.invalid_type'
    },
    {
        id: FileUploadResultCodeOptions.MaxSize,
        name: 'max_size',
        term: 'file_upload.errors.max_size'
    },
    {
        id: FileUploadResultCodeOptions.MinWidth,
        name: 'dimensions',
        term: 'file_upload.errors.dimensions'
    }
];
