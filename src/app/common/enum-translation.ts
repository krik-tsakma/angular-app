﻿export interface EnumTranslation {
    // Option e.x  AssetGroupTypes.Vehicle or QuickFiltersEntities.Trips
    enumOption: any;
    // translation string for locales
    translation: string;
}
