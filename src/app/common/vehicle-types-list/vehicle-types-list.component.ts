// FRAMEWORK
import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

// SERVICES
import { SnackBarService } from '../../shared/snackbar.service';
import { VehicleTypesListService } from './vehicle-types-list.service';
import { VehicleTypesService } from '../../admin/vehicle-types/vehicle-types.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';

// TYPES
import { VehicleTypeListItem } from './vehicle-type-list.types';
import { SearchTermRequest } from '../pager';
import { VehicleTypesResult } from '../../admin/vehicle-types/vehicle-types.types';
import { KeyName } from '../key-name';

@Component({
    selector: 'vehicle-types-list',
    templateUrl: './vehicle-types-list.html'
})

export class VehicleTypesListComponent implements OnChanges {
    @Input() public showAllOption?: boolean;
    @Input() public showNoneOption?: boolean;
    @Input() public authorizedOnly: boolean;
    @Input() public modelData?: number;
    @Output() public modelDataChange: EventEmitter<KeyName> =
        new EventEmitter<KeyName>();
    @Output() public onLoad: EventEmitter<VehicleTypeListItem[]> =
        new EventEmitter<VehicleTypeListItem[]>();
    
    public data: VehicleTypeListItem[];
    public loading: boolean;
    public selectedOption: number;

    private translations: any;
    constructor(private snackBar: SnackBarService,
                private translate: CustomTranslateService,
                private service: VehicleTypesListService,
                private adminService: VehicleTypesService
            ) {
         
        this.translate
            .get(
                [
                    'form.actions.none',
                    'form.actions.all',
                ]
            )
            .subscribe((res) => {
                if (res) {
                    this.translations = res;
                }
            });
    }

    public ngOnChanges(changes: SimpleChanges): void {
        const authorizedOnlyOption = changes['authorizedOnly'];
        if (authorizedOnlyOption) {
            if (authorizedOnlyOption.currentValue === true) {
                this.getAuthorizedOnly();
            } else {
                this.getAll();
            }
        }
    }

    public onChangeOption(id?: number) {
        id = id !== null ? Number(id) : null;
        if (Number.isNaN(id) || id === 0) {
            id = null;
        }
        
        const vehType = this.data.find((item) => item.id === id);
        if (vehType) {
            this.modelDataChange.emit(vehType);
        }
    }

    private getAuthorizedOnly() {
        this.loading = true;
        this.service
            .getAuthorized()            
            .subscribe((res: VehicleTypeListItem[]) => {
                this.data = res;
                this.onAfterDataLoad();
            },
            (err) => {
                this.snackBar.open('data.error', 'form.actions.close');
                this.loading = false;
            });
    }

    private getAll() {
        this.loading = true;
        this.adminService
            .get({ searchTerm: null, pageSize: 1000, pageNumber: 1 } as SearchTermRequest)
            .subscribe((res: VehicleTypesResult) => {
                this.data = res.results.map((t) => {
                    return {
                        id: t.id,
                        name: t.name,
                        enabled: true,
                    } as VehicleTypeListItem;
                });
                this.onAfterDataLoad();
            },
            (err) => {
                this.snackBar.open('data.error', 'form.actions.close');
                this.loading = false;
            });
    }

    private onAfterDataLoad() {
        this.loading = false;
        if (this.showAllOption && this.translations['form.actions.all']) {
            this.data.unshift({
                id: null,
                name: this.translations['form.actions.all'],
                enabled: true,
                energyUnit: null
            });
        }
        if (this.showNoneOption && this.translations['form.actions.none']) {
            this.data.unshift({
                id: -1,
                name: this.translations['form.actions.none'],
                enabled: true,
                energyUnit: null,
            });
        }
        this.onLoad.emit(this.data);
       
        // if none selected option was provided select the first one
        if (this.data && this.data.length > 0 && (this.modelData === undefined || this.modelData === null)) {
            this.modelData = this.data[0].id;
        }
    }

}
