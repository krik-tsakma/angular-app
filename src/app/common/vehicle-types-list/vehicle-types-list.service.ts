
// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES 
import { VehicleTypeListItem } from './vehicle-type-list.types';

@Injectable()
export class VehicleTypesListService {
    private baseApiURL: string;
    
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseApiURL = config.get('apiUrl') + '/api/VehicleTypes';
        }


    public getAuthorized(): Observable<VehicleTypeListItem[]> {       

        return this.authHttp
            .get(this.baseApiURL + '/GetAuthorized')
            .pipe(
                map((r) => {
                    let res = r as VehicleTypeListItem[];
                    if (!res) {
                        res = new Array<VehicleTypeListItem>();
                    }
                    res.forEach((vt) => {
                        vt.enabled = true;
                    });
                    return res;
                })
            );
    }
}
