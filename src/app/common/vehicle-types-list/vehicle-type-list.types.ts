import { KeyName } from '../key-name';
import { EnergyUnitOptions } from '../energy-types/energy-types';

export interface VehicleTypeListItem extends KeyName {
    energyUnit: EnergyUnitOptions;
    energyTypeID?: number;
}
