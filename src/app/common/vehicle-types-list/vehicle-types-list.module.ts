// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

// SERVICES
import { VehicleTypesListService } from './vehicle-types-list.service';
import { VehicleTypesService } from '../../admin/vehicle-types/vehicle-types.service';

// COMPONENTS
import { VehicleTypesListComponent } from './vehicle-types-list.component';

@NgModule({
    imports: [
        SharedModule      
    ],
    declarations: [
        VehicleTypesListComponent
    ],
    exports: [
        VehicleTypesListComponent
    ],
    providers: [
        VehicleTypesListService,
        VehicleTypesService
    ]
})
export class VehicleTypesListModule {}
