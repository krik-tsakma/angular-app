﻿// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'fiscal-trips-unauthorized',
    templateUrl: 'fiscal-trips-unauthorized.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['fiscal-trips-unauthorized.less']
})

export class FiscalTripsUnauthorizedComponent { }
