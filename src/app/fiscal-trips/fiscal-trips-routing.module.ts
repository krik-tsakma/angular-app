﻿
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../auth/authGuard';
import { FiscalTripsGuard } from './fiscal-trips-guard';
import { FiscalTripsUnauthorizedComponent } from './fiscal-trips-unauthorized/fiscal-trips-unauthorized.component';

const FiscalTripsRoutes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard, FiscalTripsGuard]
    },
    {
        path: 'nl',
        loadChildren: () => import('./fiscal-trips-nl/fiscal-trips-nl.module').then((m) => m.FiscalTripNLModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'unauthorized',
        component: FiscalTripsUnauthorizedComponent,
        canActivate: [AuthGuard]
    },
    // {
    //     path: 'de',
    //     loadChildren: './fiscal-trips-nl/fiscal-trips-de.module#FiscalTripDEModule',
    // },
];

@NgModule({
    imports: [
        RouterModule.forChild(FiscalTripsRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class FiscalTripsRoutingModule { }
