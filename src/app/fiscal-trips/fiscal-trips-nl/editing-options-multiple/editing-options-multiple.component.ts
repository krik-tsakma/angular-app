// FRAMEWORK
import { 
    Component, ViewEncapsulation, Input, 
    Output, EventEmitter, OnChanges, SimpleChanges, ViewChild, ElementRef
} from '@angular/core';

// TYPES
import { FiscalTrip } from '../fiscal-trips-nl-types';
import { KeyName } from '../../../common/key-name';
import { FiscalTripsNLMultipleEditingOptions } from './editing-options-multiple';

@Component({
    selector: 'fiscal-trips-nl-multiple-edit-options',
    templateUrl: 'editing-options-multiple.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['editing-options-multiple.less']
})

export class FiscalTripNLEditingMultipleOptionsComponent implements OnChanges {
    @ViewChild('editingOptionsMultiple', { static: true }) public cnt: ElementRef;
    @Input() public trips: FiscalTrip[] = [];
    @Output() public onSelectOption: EventEmitter<FiscalTripsNLMultipleEditingOptions> = new EventEmitter<FiscalTripsNLMultipleEditingOptions>();
   
    public options: KeyName[];

    public ngOnChanges(changes: SimpleChanges) {
        const trips = changes['trips'];
        if (trips.currentValue) {
            this.options = this.getOptions(trips.currentValue);
            if (this.options.filter((o) => o.enabled === true).length > 0) {
                this.cnt.nativeElement.style.display = 'block';
            } else {
                this.cnt.nativeElement.style.display = 'none';
            }
        }
    }

    public selectOption(option: KeyName) {
        if (!option.enabled) {
            return;
        }
        this.cnt.nativeElement.style.display = 'none';
        this.onSelectOption.emit(option.id);
    }

    private getOptions(trips: FiscalTrip[]): KeyName[] {
        return [
            {
                id: FiscalTripsNLMultipleEditingOptions.combine,
                term: 'fiscal_trips.nl.multiple_records_options.combine_selected_trips',
                name: 'combine',
                enabled: this.enableCombineOption(trips),
            },
            {
                id: FiscalTripsNLMultipleEditingOptions.validate,
                term: 'fiscal_trips.nl.multiple_records_options.validate_selected_trips',
                name: 'validate',
                enabled: this.enableValidateOption(trips)
            }
        ];
    }

    private enableValidateOption(trips: FiscalTrip[]): boolean {
        const tripsNonValidated = trips.filter((t) => { 
            return t.isValidated === false;
        });

        if (tripsNonValidated.length > 0) {
            return true;
        }
        return false;
    }

    private enableCombineOption(trips: FiscalTrip[]): boolean {
        if (trips.length < 2) {
            return false;
        } 

        return true;
    }
}
