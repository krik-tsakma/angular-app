﻿import { PagerResults, PagerRequest } from '../../common/pager';
import { PeriodTypes } from '../../common/period-selector/period-selector-types';


export interface FiscalTripsNLRequest extends PagerRequest {   
    driverID: number; 
    periodType: PeriodTypes;
    periodSince: string;
    periodTill: string;
    onlyNonValidated: boolean;
}

export interface FiscalTripsNLTripsResults extends PagerResults {
    results: FiscalTrip[];
}

export interface FiscalTrip {
    vehicleID: number;
    vehicleLabel: string;
    startAddress: string;    
    startTimestamp: string;
    startLatitude?: number;
    startLongitude?: number;
    startCummTotalDistance: number;
    stopAddress: string;
    stopTimestamp: string;
    stopCummTotalDistance: number;    
    stopLatitude?: number;
    stopLongitude?: number;
    duration: number;
    totalDistance: number;
    isValidated: boolean;
    isCombined: boolean;
    isCorrection: boolean;
    isReplacementVehicle: boolean;
    businessDistance: number;
    privateDistance: number;
    deviationDistance?: number;
    deviationComment?: string;
    comment: string;
    currentReplacementVehicleID?: number;
    currentStartTimestamp: string;
}

// ================
// EDIT
// ================  

export enum FiscalTripNLEditCodeOptions {
    Ok = 0,
    StartAddressEmpty = -1,
    StopAddressEmpty = -2,
    TripNotFound = -3,
    DeviationDistGreaterOrEqualToTripDist = -4,
    PrivateAndBusinessDistNotEqualToTripDist = -5,
}

// ================
// VALIDATE
// ================  

export interface FiscalTripNLValidateMultipleRequest {
    from: string;
    till: string;
}

export enum FiscalTripNLValidateCodeOptions {
    Ok = 0,
    TripNotFound = -1,
}

// =======================
// INSERT CORRECTION TRIP
// ======================

export interface FiscalTripNLCorrectionTripRequest {
    odometer: number;
}

export enum FiscalTripNLCorrectionCodeOptions {
    Ok = 0,
    TripOverlapsWithTimestamp = -1,
    NoLaterTripExists = - 2
}

// ================
// SPLIT
// ================  

export interface FiscalTripNLSplitRequest {
    vehicleID: number;
    startTimestamp: string;
}

export enum FiscalTripNLSplitCodeOptions {
    Ok = 0,
    NotAMergedTrip = -1,
}

// ====================
// REPLACEMENT VEHICLE
// ====================  

export interface ReplacementVehicleCreateRequest {
    LicencePlate: string;
}

export interface ReplacementVehicleCreateResult {
    code: FiscalTripNLReplacementVehicleTripCodeOptions;
    data: number;
    exception?: string;
}

export enum FiscalTripNLReplacementVehicleTripCodeOptions {
    Ok = 0,
    StartAddressEmpty = -1,
    StopAddressEmpty = -2,
    StartShouldBeEarlierThanStop = -3,
    DistanceZero = -4,
    TripOverlaps = -5,
}
