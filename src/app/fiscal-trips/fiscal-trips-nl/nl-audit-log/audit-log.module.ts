// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { FiscalTripsNLAuditLogRoutingModule } from './audit-log-routing.module';
import { PeriodSelectorModule } from '../../../common/period-selector/period-selector.module';

// SERVICES
import { FiscalTripsNLDetailsDialogService } from './details-dialog/details-dialog.service';
import { FiscalTripsNLAuditLogService } from './audit-log.service';

// PIPES
import { FiscalTripsNLAuditLogParameterNamePipe } from './details-dialog/audit-log-parameter-name.pipe';
import { FiscalTripsNLAuditLogParameterValuePipe } from './details-dialog/audit-log-parameter-value.pipe';

// COMPONENTS
import { FiscalTripsNLAuditLogComponent } from './audit-log.component';
import { FiscalTripsNLDetailsDialogComponent } from './details-dialog/details-dialog.component';

@NgModule({
    imports: [
        SharedModule,
        FiscalTripsNLAuditLogRoutingModule,
        PeriodSelectorModule,
    ],
    declarations: [
        FiscalTripsNLAuditLogComponent,
        FiscalTripsNLDetailsDialogComponent,
        FiscalTripsNLAuditLogParameterNamePipe,
        FiscalTripsNLAuditLogParameterValuePipe
    ],
    providers: [
        FiscalTripsNLAuditLogService,
        FiscalTripsNLDetailsDialogService
    ],
    entryComponents: [
        FiscalTripsNLDetailsDialogComponent
    ]
})
export class FiscalTripsNLAuditLogModule { }
