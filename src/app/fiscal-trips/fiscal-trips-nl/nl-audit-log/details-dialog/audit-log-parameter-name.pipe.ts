import { CustomTranslateService } from '../../../../shared/custom-translate.service';
// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';
import { FiscalTripsNLAuditLogRevisionTypes } from '../audit-log-types';

@Pipe({
    name: 'myFiscalTripsNLAuditLogParameterName',
    pure: false
})

export class FiscalTripsNLAuditLogParameterNamePipe implements PipeTransform {
    constructor(private translate: CustomTranslateService) {
        // foo
    }

    public transform(val: string,  entityType: FiscalTripsNLAuditLogRevisionTypes): string {
        let term = '';
        switch (entityType) {
            case FiscalTripsNLAuditLogRevisionTypes.CommentRevision:
                term = this.getCommentRevision(val);
                break;
            case FiscalTripsNLAuditLogRevisionTypes.DeviationRevision:
                term = this.getDeviationRevision(val);
                break;
            case FiscalTripsNLAuditLogRevisionTypes.DistanceRevision:
                term = this.getDistanceRevision(val);
                break;
            case FiscalTripsNLAuditLogRevisionTypes.StartLocationRevision:
                term = this.getStartLocationRevision(val);
                break;
            case FiscalTripsNLAuditLogRevisionTypes.StopLocationRevision:
                term = this.getStopLocationRevision(val);
                break;
        }
        return term ? this.translate.instant(term) : val;
    }

    private getCommentRevision(parameterName: string): string {
        switch (parameterName) {
            case 'Comment':
                return 'fiscal_trips.nl.comment';
            default:
                return parameterName;
        }
    }

    private getDeviationRevision(parameterName: string): string {
        switch (parameterName) {
            case 'Comment':
                return 'fiscal_trips.nl.comment';
            case 'Distance':
                return 'fiscal_trips.nl.distance';
            default:
                return parameterName;
        }
    }

    private getDistanceRevision(parameterName: string): string {
        switch (parameterName) {
            case 'Business':
                return 'fiscal_trips.nl.business_distance';
            case 'Private':
                return 'fiscal_trips.nl.private_distance';
            default:
                return parameterName;
        }
    }

    private getStartLocationRevision(parameterName: string): string {
        switch (parameterName) {
            case 'StartLocation':
                return 'fiscal_trips.nl.start_address';
            default:
                return parameterName;
        }
    }

    private getStopLocationRevision(parameterName: string): string {
        switch (parameterName) {
            case 'StopLocation':
                return 'fiscal_trips.nl.stop_address';
            default:
                return parameterName;
        }
    }
}
