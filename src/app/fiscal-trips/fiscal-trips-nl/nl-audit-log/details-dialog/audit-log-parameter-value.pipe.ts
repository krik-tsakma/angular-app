import { CustomTranslateService } from '../../../../shared/custom-translate.service';
// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';
import { FiscalTripsNLAuditLogRevisionTypes } from '../audit-log-types';

@Pipe({
    name: 'myFiscalTripsNLAuditLogParameterValue',
    pure: false
})

export class FiscalTripsNLAuditLogParameterValuePipe implements PipeTransform {
    constructor(private translate: CustomTranslateService) {
        // foo
    }

    public transform(val: string,  revisionType: FiscalTripsNLAuditLogRevisionTypes): string {
       
        switch (revisionType) {
            // case FiscalTripsNLAuditLogEntryTypes.CommentRevision:
            //     val = this.getCompanyValue(val);
            //     break;
            default: 
                break;
        }

        return val;
    }
}
