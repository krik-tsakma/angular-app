﻿// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// SERVICES
import { UserOptionsService } from '../../../../shared/user-options/user-options.service';


import { DateFormatterDisplayOptions } from '../../../../shared/pipes/dateFormat.pipe';

// TYPES
import { FiscalTripsNLAuditLogDetailResult, FiscalTripsNLAuditLogDetailsChangedValues, FiscalTripsNLAuditLogRevisionTypes } from '../audit-log-types';

@Component({
    selector: 'details-dialog',
    templateUrl: 'details-dialog.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['details-dialog.less']
})

export class FiscalTripsNLDetailsDialogComponent implements OnInit {

    public data: FiscalTripsNLAuditLogDetailResult;
    public dateDisplayOptions: any = DateFormatterDisplayOptions;
    public revisionTypes: any = FiscalTripsNLAuditLogRevisionTypes;
    public details: FiscalTripsNLAuditLogDetailsChangedValues[] = [];

    constructor(
        public userOptions: UserOptionsService,
        private dialogRef: MatDialogRef<FiscalTripsNLDetailsDialogComponent>) {
        // foo
    }    

    public ngOnInit() {
        this.data.after.forEach((afterItem) => {
            const beforeItem = this.data.before.find((item) => item.propertyName === afterItem.propertyName);
            if (beforeItem && beforeItem.value !== afterItem.value) {
                this.details.push({
                    label: afterItem.propertyName,
                    newValue: afterItem.value,
                    oldValue: beforeItem.value
                });
            } else {
                this.details.push({
                    label: afterItem.propertyName,
                    newValue: afterItem.value
                });
            }

        });

        this.data.before.forEach((beforeItem) => {
            const afterItem = this.data.after.find((item) => item.propertyName === beforeItem.propertyName);
            if (!afterItem) {
                this.details.push({
                    label: beforeItem.propertyName,                    
                    oldValue: beforeItem.value
                });
            }
        });
    }

    public closeDialog() {
        this.dialogRef.close();
    }
}
