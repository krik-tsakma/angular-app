﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// COMPONENTS
import { FiscalTripsNLComponent } from './fiscal-trips-nl.component';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

const FiscalTripsNLRoutes: Routes = [
    {
        path: '',
        component: FiscalTripsNLComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'audit-log',
        loadChildren: () => import('./nl-audit-log/audit-log.module').then((m) => m.FiscalTripsNLAuditLogModule),
        canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(FiscalTripsNLRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class FiscalTripsNLRoutingModule { }
