// FRAMEWORK
import {
    Component, OnInit, OnChanges, OnDestroy, Input, Output, ViewChild, ElementRef,
    Renderer, EventEmitter, ViewEncapsulation, SimpleChanges 
} from '@angular/core';

// RXJS
import { Observable, Subject, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, map } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';

// SERVICES
import { FiscalTripsNLService } from '../../fiscal-trips-nl.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';

// TYPES
import { ReplacementVehicleCreateRequest, ReplacementVehicleCreateResult } from '../../fiscal-trips-nl-types';
import { KeyName } from '../../../../common/key-name';

@Component({
    selector: 'replacement-vehicle-search',
    templateUrl: './replacement-vehicle-search.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: []
})

export class ReplacementVehicleSearchComponent implements OnInit, OnChanges, OnDestroy {
    @ViewChild('term', { static: true }) public fileInput: ElementRef;
    @Input('placeholder') public placeholder: string;
    @Input() public groupID?: number;
    @Input() public name: string;
    @Input() public inputVehicle?: KeyName;
    @Output() public onSelect = new EventEmitter<KeyName>();
    @Output() public onNewAdd ?= new EventEmitter<KeyName>();
    public loading: boolean = false;
    public inputSelected: any;
    public result: any;
    public searchTermStream: Subject<string> = new Subject();
    public listSelected: boolean;
   
    public items: Observable<KeyName[]> = this.searchTermStream
        .pipe(
            debounceTime(500),
            distinctUntilChanged(),
            switchMap((term) => {
                // use this to empty result set in case of change            
                if (term === null || term.length < 3) {
                    this.loading = false;
                    return of([]);
                } else {
                    this.loading = false;                    
                    return this.service.getReplacementVehicles(term);                                
                }
            })
        );

    private replacementVehiclesSubscriber: Subscription;

    constructor(private service: FiscalTripsNLService ,
                private unsubscribeService: UnsubscribeService,
                private renderer: Renderer) {
        // foo
    }
 
    public ngOnChanges(changes: SimpleChanges) {
        console.log(changes);
        const changeTerm = changes['term'];
        if (changeTerm) {            
            this.inputSelected = changeTerm.currentValue;
        }
        const changeName = changes['name'];
        if (changeName) {            
            this.inputSelected = changeName.currentValue;
        }
    }

    public ngOnInit() {
        if (this.inputVehicle) {
            this.inputSelected = this.inputVehicle.name; 
        }

        this.items.subscribe((responses: KeyName[]) => {
            this.listSelected = false;
            responses.forEach((res) => {
                if (res.name === this.inputSelected) {
                    this.listSelected = true;
                }
            });                     
        });
    }


    public ngOnDestroy() {
        this.unsubscribeService
            .removeSubscription(this.replacementVehiclesSubscriber);
    }

    // Method that pass
    public search(term: any) {   
        this.loading = true;                 
        if (term && term.length > 1) {
            this.searchTermStream.next(term);
        } else {
            this.loading = false;     
        }
    }

    public selected(term?: KeyName) {  
        if (!term) {
            return;
        }            
        this.inputSelected = term.name;
        this.listSelected = true;
        this.onSelect.emit(term);
        this.clearInput(true);
    }


    public onAddNew(term?: string) {            
        const request: ReplacementVehicleCreateRequest = {
            LicencePlate: term
        };

        this.unsubscribeService.removeSubscription(this.replacementVehiclesSubscriber);
        this.replacementVehiclesSubscriber = this.service.insertReplacementVehicle(request)
            .subscribe((res: ReplacementVehicleCreateResult) => {
                console.log('replacementVehiclesSubscriber : ', res);
                if (res) {
                    this.inputSelected = term;
                    this.listSelected = true;
                    this.onNewAdd.emit({name: term, id: res.data} as KeyName);
                    this.clearInput(true);      
                }
            });       
        
       
    }


    public clearInput(fromSelect: boolean, e?: Event) {
        // do not bubble event otherwise focus is on the textbox
        if (e) {
            e.stopPropagation();
        }

        if (!fromSelect) {
            // clear selection
            this.inputSelected = null;
            this.selected(null);
            this.inputVehicle = null;
        }

    
        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.fileInput.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }
}
