﻿// FRAMEWORK
import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// SERVICES
import { FiscalTripsNLService } from '../fiscal-trips-nl.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';

// PIPES
import { StringFormatPipe } from '../../../shared/pipes/stringFormat.pipe';

// TYPES
import { FiscalTrip, FiscalTripNLReplacementVehicleTripCodeOptions } from '../fiscal-trips-nl-types';
import { AddressAutocompleteResult } from '../../../common/address-autocomplete/address-autocomplete-types';
import { KeyName } from '../../../common/key-name';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'insert-replacement-vehicle-trip',
    templateUrl: 'insert-replacement-vehicle-trip.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['insert-replacement-vehicle-trip.less'],
    providers: [StringFormatPipe]
})

export class InsertReplacementVehicleTripComponent implements OnInit {
    @ViewChild('stopAddress', { static: true }) public stopAddress: any;
    @ViewChild('startAddress', { static: true }) public startAddress: any;
    public trip: FiscalTrip;
    public inputVehicle: KeyName;
    public loading: boolean;
    public startLocation: AddressAutocompleteResult = {} as AddressAutocompleteResult;
    public stopLocation: AddressAutocompleteResult  = {} as AddressAutocompleteResult;
    public maxStartTime: moment.Moment = moment().subtract(1, 'seconds');
    public maxStopTime: moment.Moment = moment();
    public startDate: moment.Moment = moment();
    public startDateHours: number  = 0;
    public startDateMinutes: number  = 0;
    public stopDate: moment.Moment = moment();
    public stopDateHours: number = 1;
    public stopDateMinutes: number  = 0;

    private serviceSubscriber: any;

    constructor(
        public service: FiscalTripsNLService,
        private dialogRef: MatDialogRef<InsertReplacementVehicleTripComponent>,
        private snackBar: SnackBarService,
        private unsubscribe: UnsubscribeService,
        private translate: CustomTranslateService,
        private stringFormatPipe: StringFormatPipe) {
            // foo
    }

    public ngOnInit(): void {
            console.log('ngOnInit ', this.trip);
            this.startLocation = {
                address: !this.trip.startAddress ? null : this.trip.startAddress,
                latitude: !this.trip.startLatitude ? 0 : this.trip.startLatitude,
                longitude: !this.trip.startLongitude ? 0 : this.trip.startLongitude
            } as AddressAutocompleteResult;

            this.stopLocation = {
                address: !this.trip.stopAddress ? null : this.trip.stopAddress,
                latitude: !this.trip.stopLatitude ? 0 : this.trip.stopLatitude,
                longitude: !this.trip.stopLongitude ? 0 : this.trip.stopLongitude
            } as AddressAutocompleteResult;

            if (this.trip.startTimestamp && this.trip.stopTimestamp) {
                this.startDate = moment(this.trip.startTimestamp);
                this.startDateHours = this.startDate.clone().hours();
                this.startDateMinutes = this.startDate.clone().minutes();
                this.stopDate = moment(this.trip.stopTimestamp);
                this.stopDateHours = this.stopDate.clone().hours();
                this.stopDateMinutes = this.stopDate.clone().minutes();
            }

            this.inputVehicle = this.trip.vehicleID
                ? {
                    id: this.trip.vehicleID,
                    name: this.trip.vehicleLabel
                }
                : null;
    }


    public closeDialog(successfullyEdited?: boolean) {
        this.dialogRef.close(successfullyEdited);
    }

    public save({ value, valid }: { value: any, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }

        this.trip.startTimestamp = this.startDate.startOf('day')
            .add(this.startDateHours, 'hours')
            .add(this.startDateMinutes, 'minutes')
            .format('YYYY-MM-DD HH:mm');
        this.trip.stopTimestamp = this.stopDate.startOf('day')
            .add(this.stopDateHours, 'hours')
            .add(this.stopDateMinutes, 'minutes')
            .format('YYYY-MM-DD HH:mm');

        console.log('save ', this.trip);

        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        if (this.trip.isReplacementVehicle) {
            this.serviceSubscriber = this.service
                .editReplacementFiscalTrip(this.trip)
                .subscribe((res: any) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.closeDialog(true);
                    } else {
                        const message = this.getUpdateResult(res.code);
                        console.log(res, message);
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        } else {
            this.serviceSubscriber = this.service
                .insertReplacementVehicleFiscalTrip(this.trip)
                .subscribe((res: any) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.closeDialog(true);
                    } else {
                        const message = this.getUpdateResult(res.code);
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        }
    }


    public checkMilage(selectedInput: string): void {
        if (selectedInput !== 'business' && selectedInput !== 'private') {
            return;
        }

        if (selectedInput === 'business') {
            if (this.trip.businessDistance < 0) {
                this.trip.businessDistance = 0;
                this.trip.privateDistance = this.trip.totalDistance;
            } else if (this.trip.businessDistance > this.trip.totalDistance) {
                this.trip.businessDistance = this.trip.totalDistance;
                this.trip.privateDistance = 0;
            } else {
                this.trip.privateDistance = this.trip.totalDistance - this.trip.businessDistance;
            }
        }

        if (selectedInput === 'private') {
            if (this.trip.privateDistance < 0) {
                this.trip.privateDistance = 0;
                this.trip.businessDistance = this.trip.totalDistance;
            } else if (this.trip.privateDistance > this.trip.totalDistance) {
                this.trip.privateDistance = this.trip.totalDistance;
                this.trip.businessDistance = 0;
            } else {
                this.trip.businessDistance = this.trip.totalDistance - this.trip.privateDistance;
            }
        }
    }


    // =========================
    // ADDRESS SEARCH
    // =========================

    public locationChange(loc: AddressAutocompleteResult, type: string) {
        if (type === 'start') {
            this.trip.startAddress = !loc ? '' : loc.address;
            this.trip.startLatitude = !loc ? 0 : loc.latitude;
            this.trip.startLongitude = !loc ? 0 : loc.longitude;
            this.startAddress.control.markAsDirty();
        } else {
            this.trip.stopAddress = !loc ? '' : loc.address;
            this.trip.stopLatitude = !loc ? 0 : loc.latitude;
            this.trip.stopLongitude = !loc ? 0 : loc.longitude;
            this.stopAddress.control.markAsDirty();
        }
    }


    // =========================
    // VEHICLE SEARCH OR ADD
    // =========================
    public onListItemSelectOrAddNew(event: KeyName) {
        console.log('onListItemSelect', event);
        this.trip.vehicleID = event.id;
        this.trip.vehicleLabel = event.name;
    }

    // =========================
    // RESULTS
    // =========================

    private getUpdateResult(code: FiscalTripNLReplacementVehicleTripCodeOptions): string {
        let message = '';
        const required = this.translate.instant('form.errors.required');
        console.log(code === FiscalTripNLReplacementVehicleTripCodeOptions.TripOverlaps, code);
        switch (code) {
            case FiscalTripNLReplacementVehicleTripCodeOptions.StartAddressEmpty:
                const startAddress = this.translate.instant('fiscal_trips.nl.start_address');
                message = this.stringFormatPipe.transform(required, [startAddress]);
                break;
            case FiscalTripNLReplacementVehicleTripCodeOptions.StopAddressEmpty:
                const stopAddress = this.translate.instant('fiscal_trips.nl.stop_address');
                message = this.stringFormatPipe.transform(required, [stopAddress]);
                break;
            case FiscalTripNLReplacementVehicleTripCodeOptions.StartShouldBeEarlierThanStop:
                message = 'fiscal_trips.nl.insert_replacement_vehicle_trip.error.start_should_be_earlier_than_stop';
                break;
            case FiscalTripNLReplacementVehicleTripCodeOptions.DistanceZero:
                message = 'fiscal_trips.nl.insert_replacement_vehicle_trip.error.distance_zero';
                break;
            case FiscalTripNLReplacementVehicleTripCodeOptions.TripOverlaps:
                console.log('mpika sto case');
                message = 'fiscal_trips.nl.insert_replacement_vehicle_trip.error.trip_overlaps';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

}
