﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { InsertReplacementVehicleTripComponent  } from './insert-replacement-vehicle-trip.component';

// TYPES
import { FiscalTrip } from '../fiscal-trips-nl-types';

@Injectable()
export class InsertReplacementVehicleTripService {
    constructor(private dialog: MatDialog) { }

    public open(trip?: FiscalTrip): Observable<boolean> {
        const dialogRef: MatDialogRef<InsertReplacementVehicleTripComponent> = 
            this.dialog.open(InsertReplacementVehicleTripComponent);              

        dialogRef.componentInstance.trip = trip 
            ? trip 
            : {} as FiscalTrip;

        return dialogRef.afterClosed();
    }    
}
