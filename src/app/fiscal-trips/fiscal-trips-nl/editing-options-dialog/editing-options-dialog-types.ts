import { FiscalTrip } from '../fiscal-trips-nl-types';

export enum FiscalTripsNLEditingOptions {
    edit = 0,
    validate = 1,
    split = 2,
    remove = 3
}

export interface FiscalTripsNLEditingOptionsDialogPosition {
    x: number;
    y: number;
}

export interface FiscalTripsNLEditingOptionsDialogCloseResult {
    action: FiscalTripsNLEditingOptions;
    trip: FiscalTrip;
}
