
// FRAMEWORK
import { 
    Component, ViewEncapsulation, ViewChild,
    ElementRef, HostListener 
} from '@angular/core';
import { Subject } from 'rxjs';

// TYPES
import { FiscalTrip } from '../fiscal-trips-nl-types';
import { KeyName } from '../../../common/key-name';
import { 
    FiscalTripsNLEditingOptions, 
    FiscalTripsNLEditingOptionsDialogPosition, 
    FiscalTripsNLEditingOptionsDialogCloseResult 
} from './editing-options-dialog-types';


@Component({
    selector: 'fiscal-trips-nl-edit-options-dialog',
    templateUrl: 'editing-options-dialog.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['editing-options-dialog.less']
})

export class FiscalTripNLEditingOptionsDialogComponent {
    @ViewChild('editingOptionsDialog', { static: true }) public dialog: ElementRef;
    public afterClosed: Subject<FiscalTripsNLEditingOptionsDialogCloseResult> = new Subject<FiscalTripsNLEditingOptionsDialogCloseResult>();
    public options: KeyName[];
    private trip: FiscalTrip;

    @HostListener('document:touchstart', ['$event', '$event.target'])
    @HostListener('document:click', ['$event', '$event.target'])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
        event.stopPropagation();
        event.cancelBubble = true;
        if (targetElement.tagName === 'MAT-ICON'  || targetElement.tagName === 'BUTTON') {
            return;
        }

        const clickedInside = this.dialog.nativeElement.contains(targetElement);
        if (!clickedInside && this.dialog.nativeElement.style.display === 'block') {
            this.onClose();
        }
    }
    
    public open(trip: FiscalTrip, position: FiscalTripsNLEditingOptionsDialogPosition) {
        this.trip = trip;
        this.options = this.getOptions(trip);

        if (this.dialog) {
            this.dialog.nativeElement.style.display = 'block';
            this.dialog.nativeElement.style.left = `${ position.x - this.dialog.nativeElement.offsetWidth - 20  }px`;
            this.dialog.nativeElement.style.top = `${ position.y - 20 }px`;
        }
    }

    public onSelectOption(option: KeyName) {
        this.onClose(option.id);
    }

    private onClose(option?: FiscalTripsNLEditingOptions) {
        this.dialog.nativeElement.style.display = 'none';
        this.afterClosed.next({trip: this.trip, action: option });
    }

    private getOptions(trip: FiscalTrip): KeyName[] {
        return [
            {
                id: FiscalTripsNLEditingOptions.edit,
                term: 'fiscal_trips.nl.record_options.edit_trip',
                name: 'edit',
                // only raw trips can be edited
                enabled: !trip.isCombined && !trip.isCorrection,                    
            },
            {
                id: FiscalTripsNLEditingOptions.validate,
                term: 'fiscal_trips.nl.record_options.validate',
                name: 'validate',
                enabled: !trip.isValidated,
            },
            {
                id: FiscalTripsNLEditingOptions.split,
                term: 'fiscal_trips.nl.record_options.split',
                name: 'split',
                // only combined trips can be splitted
                enabled: trip.isCombined,
            },
            {
                id: FiscalTripsNLEditingOptions.remove,
                term: 'fiscal_trips.nl.record_options.remove',
                name: 'remove',
                // only replacement vehicle trips can be deleted
                enabled: trip.isReplacementVehicle,
            }
        ];
    }
}
