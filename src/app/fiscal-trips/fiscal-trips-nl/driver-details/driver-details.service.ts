﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { DriverDetailsComponent } from './driver-details.component';

// TYPES
import { FiscalTrip } from '../fiscal-trips-nl-types';

@Injectable()
export class DriverDetailsService {
    private baseURL: string;
    constructor(private dialog: MatDialog) { }

    public open(fiscalTrip: FiscalTrip): Observable<FiscalTrip | null> {

        let dialogRef: MatDialogRef<DriverDetailsComponent>;

        dialogRef = this.dialog.open(DriverDetailsComponent);
        dialogRef.componentInstance.trip = fiscalTrip;

        return dialogRef.afterClosed();
    }    
}
