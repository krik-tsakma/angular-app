﻿// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// SERVICES
import { FiscalTripsNLService } from '../fiscal-trips-nl.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';

// PIPES
import { FormValidationMessagePipe } from '../../../shared/pipes/form-validation-message.pipe';
import { TranslatePipe } from '@ngx-translate/core';
import { StringFormatPipe } from '../../../shared/pipes/stringFormat.pipe';

// TYPES
import { FiscalTrip, FiscalTripNLEditCodeOptions } from '../fiscal-trips-nl-types';

@Component({
    selector: 'driver-details',
    templateUrl: 'driver-details.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['driver-details.less'],
    providers: [ FormValidationMessagePipe, StringFormatPipe, TranslatePipe]
})

export class DriverDetailsComponent {
    public trip: FiscalTrip;
    public loading: boolean;
    public emailSent: boolean;

    private serviceSubscriber: any;

    constructor(
        public service: FiscalTripsNLService,
        private dialogRef: MatDialogRef<DriverDetailsComponent>,        
        private snackBar: SnackBarService,
        private unsubscribe: UnsubscribeService,
        private translate: CustomTranslateService,
        private stringFormatPipe: StringFormatPipe) {
        // foo
    }

    public formIsInvalid(): boolean {        
        return false;
    }

    public closeDialog(successfullyEdited?: boolean) {
        this.dialogRef.close(successfullyEdited);
    }

    public save({ value, valid }: { value: any, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        // this.unsubscribe.removeSubscription(this.serviceSubscriber);
        // this.serviceSubscriber = this.service
        //     .editFiscalTrip(this.trip)
        //     .subscribe((res: FiscalTripNLEditCodeOptions) => {
        //         this.loading = false;
        //         if (res === null) {
        //             this.snackBar.open('form.save_success', 'form.actions.close');
        //             this.closeDialog(true);
        //         } else {
        //             const message = this.getUpdateResult(res);
        //             this.snackBar.open(message, 'form.actions.close');
        //         }
        //     },
        //     (err) => {
        //         this.loading = false;
        //         if (err.status === 404) {
        //             this.snackBar.open('form.errors.not_found', 'form.actions.close');
        //         } else {
        //             this.snackBar.open('form.errors.unknown', 'form.actions.ok');
        //         }
        //     });
    }


    // =========================
    // RESULTS
    // =========================

    private getUpdateResult(code: FiscalTripNLEditCodeOptions): string {
        let message = '';
        const required = this.translate.instant('form.errors.required');
        switch (code) {
          
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
