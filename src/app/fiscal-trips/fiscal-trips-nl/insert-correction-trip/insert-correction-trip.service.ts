﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { InsertCorrectionTripComponent } from './insert-correction-trip.component';

@Injectable()
export class InsertCorrectionTripService {
    constructor(private dialog: MatDialog) { }

    public open(): Observable<boolean> {
        const dialogRef = this.dialog.open(InsertCorrectionTripComponent);
        return dialogRef.afterClosed();
    }    
}
