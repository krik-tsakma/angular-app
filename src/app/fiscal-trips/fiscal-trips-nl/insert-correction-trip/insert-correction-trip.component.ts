﻿// FRAMEWORK
import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// SERVICES
import { FiscalTripsNLService } from '../fiscal-trips-nl.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';

// TYPES
import { FiscalTripNLCorrectionTripRequest, FiscalTripNLCorrectionCodeOptions } from '../fiscal-trips-nl-types';


@Component({
    selector: 'insert-correction-trip',
    templateUrl: 'insert-correction-trip.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['insert-correction-trip.less'],
})

export class InsertCorrectionTripComponent implements OnInit {
    public request: FiscalTripNLCorrectionTripRequest = {} as FiscalTripNLCorrectionTripRequest;
    public loading: boolean;    
    
    private serviceSubscriber: any;

    constructor(
        public service: FiscalTripsNLService,
        private dialogRef: MatDialogRef<InsertCorrectionTripComponent>,        
        private snackBar: SnackBarService,
        private unsubscribe: UnsubscribeService) {
            // foo
    }

    public ngOnInit(): void {
        this.request = {
            odometer: 0
        };
    }

    public closeDialog(successfullyEdited?: boolean) {
        this.dialogRef.close(successfullyEdited);
    }

    public insert({ value, valid }: { value: any, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.service
            .insertCorrectionTrip(this.request)
            .subscribe((res: FiscalTripNLCorrectionCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('fiscal_trips.nl.insert_correction_trip.success', 'form.actions.close');
                    this.closeDialog(true);
                } else {
                    const message = this.getUpdateResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }


    
    // =========================
    // RESULTS
    // =========================

    private getUpdateResult(code: FiscalTripNLCorrectionCodeOptions): string {
        let message = '';
        switch (code) {
            case FiscalTripNLCorrectionCodeOptions.NoLaterTripExists:
                message = 'fiscal_trips.nl.insert_correction_trip.error.no_later_trip';
                break;
            case FiscalTripNLCorrectionCodeOptions.TripOverlapsWithTimestamp:
                message = 'fiscal_trips.nl.insert_correction_trip.error.trip_overlaps_with_timestamp';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
