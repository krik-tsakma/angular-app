﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// COMPONENTS
import { FiscalTripsNLComponent } from './fiscal-trips-nl.component';
import { EditTripComponent } from './edit-trip/edit-trip.component';
import { FiscalTripsNLDriverSummaryComponent } from './driver-summary/fiscal-trips-nl-driver-summary.component';
import { DriverDetailsComponent } from './driver-details/driver-details.component';
import {
    InsertCorrectionTripComponent
} from './insert-correction-trip/insert-correction-trip.component';
import {
    InsertReplacementVehicleTripComponent
} from './insert-replacement-vehicle-trip/insert-replacement-vehicle-trip.component';
import {
    ReplacementVehicleSearchComponent
} from './insert-replacement-vehicle-trip/replacement-vehicle-search/replacement-vehicle-search.component';
import { CombineTripsComponent } from './combine-trips/combine-trips.component';
import { FiscalTripNLEditingOptionsDialogComponent } from './editing-options-dialog/editing-options-dialog.component';
import { FiscalTripNLEditingMultipleOptionsComponent } from './editing-options-multiple/editing-options-multiple.component';
import { FiscalTripsNLNotificationComponent } from './notification-service/fiscal-trips-nl-notification.component';

// MODULES
import { FiscalTripsNLRoutingModule } from './fiscal-trips-nl-routing.module';
import { PeriodSelectorModule } from '../../common/period-selector/period-selector.module';
import { SharedModule } from '../../shared/shared.module';
import { PlacesAutocompleteModule } from '../../common/address-autocomplete/address-autocomplete.module';
import { TimePickerModule } from '../../common/timepicker/timepicker.module';
import { DatepickerModule } from '../../common/datepicker/datepicker.module';

// SERVICES
import { FiscalTripsNLService } from './fiscal-trips-nl.service';
import { EditTripService } from './edit-trip/edit-trip.service';
import { DriverDetailsService } from './driver-details/driver-details.service';
import { InsertCorrectionTripService } from './insert-correction-trip/insert-correction-trip.service';
import { InsertReplacementVehicleTripService } from './insert-replacement-vehicle-trip/insert-replacement-vehicle-trip.service';
import { CombineTripsService } from './combine-trips/combine-trips.service';
import { FiscalTripsNLNotificationService } from './notification-service/fiscal-trips-nl-notification.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/fiscal-trips/nl/'], '.json', translateState);
}


export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        FiscalTripsNLRoutingModule,
        PeriodSelectorModule,
        PlacesAutocompleteModule,
        DatepickerModule,
        TimePickerModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        FiscalTripsNLComponent,
        EditTripComponent,
        FiscalTripsNLDriverSummaryComponent,
        DriverDetailsComponent,
        InsertCorrectionTripComponent,
        InsertReplacementVehicleTripComponent,
        ReplacementVehicleSearchComponent,
        CombineTripsComponent,
        FiscalTripNLEditingOptionsDialogComponent,
        FiscalTripNLEditingMultipleOptionsComponent,
        FiscalTripsNLNotificationComponent
    ],
    entryComponents: [
        FiscalTripsNLComponent,
        EditTripComponent,
        DriverDetailsComponent,
        InsertCorrectionTripComponent,
        InsertReplacementVehicleTripComponent,
        CombineTripsComponent,
        FiscalTripsNLNotificationComponent
    ],
    providers: [
        FiscalTripsNLService,
        EditTripService,
        DriverDetailsService,
        InsertCorrectionTripService,
        InsertReplacementVehicleTripService,
        CombineTripsService,
        FiscalTripsNLNotificationService
    ]
})
export class FiscalTripNLModule { }
