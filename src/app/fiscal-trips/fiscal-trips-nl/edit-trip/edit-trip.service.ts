﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { EditTripComponent } from './edit-trip.component';

// TYPES
import { FiscalTrip } from '../fiscal-trips-nl-types';

@Injectable()
export class EditTripService {
    constructor(private dialog: MatDialog) { }

    public open(fiscalTrip: FiscalTrip): Observable<boolean> {

        let dialogRef: MatDialogRef<EditTripComponent>;

        dialogRef = this.dialog.open(EditTripComponent);
        dialogRef.componentInstance.trip = fiscalTrip;

        return dialogRef.afterClosed();
    }    
}
