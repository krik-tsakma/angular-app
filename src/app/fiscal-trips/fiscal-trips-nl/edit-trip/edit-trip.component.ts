﻿// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// RxJS
import { Subscription } from 'rxjs/Subscription';

// SERVICES
import { FiscalTripsNLService } from '../fiscal-trips-nl.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UserOptionsService } from './../../../shared/user-options/user-options.service';

// PIPES
import { StringFormatPipe } from '../../../shared/pipes/stringFormat.pipe';

// TYPES
import { FiscalTrip, FiscalTripNLEditCodeOptions } from '../fiscal-trips-nl-types';

@Component({
    selector: 'edit-trip',
    templateUrl: 'edit-trip.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-trip.less'],
    providers: [StringFormatPipe]
})

export class EditTripComponent {
    public trip: FiscalTrip;
    public loading: boolean;

    private serviceSubscriber: Subscription;

    constructor(
        public service: FiscalTripsNLService,
        private dialogRef: MatDialogRef<EditTripComponent>,        
        private snackBar: SnackBarService,
        private unsubscribe: UnsubscribeService,
        private translate: CustomTranslateService,
        private stringFormatPipe: StringFormatPipe,
        private userOptions: UserOptionsService) {
            // foo
    }   


    public closeDialog(successfullyEdited?: boolean) {
        this.dialogRef.close(successfullyEdited);
    }

    public save({ value, valid }: { value: any, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);        
        this.serviceSubscriber = this.service
            .editFiscalTrip(this.trip)
            .subscribe((res: FiscalTripNLEditCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.closeDialog(true);
                } else {
                    const message = this.getUpdateResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    public checkMilage(selectedInput: string): void {
        if (selectedInput !== 'business' && selectedInput !== 'private') {
            return;
        }
        const decimals = this.countDecimals(this.trip.totalDistance);
        if (selectedInput === 'business') {
            if (this.trip.businessDistance > this.trip.totalDistance) {
                this.trip.businessDistance = this.trip.totalDistance;
                this.trip.privateDistance = 0;
            } else {
                const privateDistance = (this.trip.totalDistance - this.trip.businessDistance).toFixed(decimals);
                this.trip.privateDistance = parseFloat(privateDistance);
            }
        }

        if (selectedInput === 'private') {
            if (this.trip.privateDistance > this.trip.totalDistance) {
                this.trip.privateDistance = this.trip.totalDistance;
                this.trip.businessDistance = 0;
            } else {
                const businessDistance = (this.trip.totalDistance - this.trip.privateDistance).toFixed(decimals);
                this.trip.businessDistance = parseFloat(businessDistance);
            }
        }
    }

    private countDecimals(num: number): number {
        if (Math.floor(num.valueOf()) === num.valueOf()) { 
            return 0; 
        }
        return num.toString().split(this.userOptions.localOptions.decimal_separator)[1].length || 0; 
    }


    // =========================
    // RESULTS
    // =========================

    private getUpdateResult(code: FiscalTripNLEditCodeOptions): string {
        let message = '';
        const required = this.translate.instant('form.errors.required');
        switch (code) {
            case FiscalTripNLEditCodeOptions.StartAddressEmpty:
                const startAddress = this.translate.instant('fiscal_trips.nl.start_address');
                message = this.stringFormatPipe.transform(required, [startAddress]);
                break;
            case FiscalTripNLEditCodeOptions.StopAddressEmpty:
                const stopAddress = this.translate.instant('fiscal_trips.nl.stop_address');
                message = this.stringFormatPipe.transform(required, [stopAddress]);
                break;
            case FiscalTripNLEditCodeOptions.TripNotFound:
                message = 'form.errors.not_found';
                break;
            case FiscalTripNLEditCodeOptions.PrivateAndBusinessDistNotEqualToTripDist:
                message = 'private_and_business_dist_not_equal_to_trip_dist';
                break;
            case FiscalTripNLEditCodeOptions.DeviationDistGreaterOrEqualToTripDist:
                message = 'fiscal_trips.nl.edit.errors.fiscal_trips.nl.edit.errors.deviation_dist_greater_or_equal_to_trip_dist';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
