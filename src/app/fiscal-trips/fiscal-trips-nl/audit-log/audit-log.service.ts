﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams, HttpResponse } from '@angular/common/http';

// RxJS
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../../../auth/authHttp';
import { Config } from '../../../config/config';

// TYPES
import {
    FiscalTripsNLAuditLogRequest, 
    FiscalTripsNLAuditLogsResult,
    FiscalTripsNLAuditLogDetailResult,
    FiscalTripsNLAuditLogItem
} from './audit-log-types';


@Injectable()
export class FiscalTripsNLAuditLogService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
        this.baseURL = this.config.get('apiUrl') + '/api/FiscalTripsNLAuditLog';
    }

    // Get all audit log entries within range
    public get(request: FiscalTripsNLAuditLogRequest): Observable<FiscalTripsNLAuditLogsResult> {        
        const params = new HttpParams({
            fromObject: {
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',  
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Since: request.periodSince,
                Till: request.periodTill,
                Sorting: request.sorting,
                RevisionType: request.revisionType !== null && request.revisionType !== undefined ? request.revisionType.toString() : '',
            }
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }



    // Get audit log entry details
    public getDetails(request: FiscalTripsNLAuditLogItem): Observable<FiscalTripsNLAuditLogDetailResult> {
        const params = new HttpParams({
            fromObject: {
                CreatedTimestamp: request.creationTimestamp,
                StartTimestamp: request.startTimestamp,
                VehicleID: request.vehicleID.toString(),
                RevisionType: request.revisionType.toString()
            }
        });
        return this.authHttp
            .get(this.baseURL + '/GetDetails/', { params });
    }
   
} 
