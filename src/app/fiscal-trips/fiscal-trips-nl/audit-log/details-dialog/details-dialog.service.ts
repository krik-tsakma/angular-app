﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { FiscalTripsNLDetailsDialogComponent } from './details-dialog.component';

// TYPES
import { FiscalTripsNLAuditLogDetailResult } from '../audit-log-types';


@Injectable()
export class FiscalTripsNLDetailsDialogService {

    constructor(private dialog: MatDialog) { }

    public details(params: FiscalTripsNLAuditLogDetailResult): Observable<string> {
        const dialogRef: MatDialogRef<FiscalTripsNLDetailsDialogComponent> = this.dialog.open(FiscalTripsNLDetailsDialogComponent);

        dialogRef.componentInstance.data = params;        

        return dialogRef.afterClosed();
    }
}
