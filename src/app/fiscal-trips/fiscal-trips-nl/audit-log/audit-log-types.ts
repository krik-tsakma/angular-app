﻿import { SearchTermRequest, PagerResults } from '../../../common/pager';
import { PeriodTypes } from '../../../common/period-selector/period-selector-types';


export enum FiscalTripsNLAuditLogRevisionTypes {
    CommentRevision = 0,
    DeviationRevision = 1,
    DistanceRevision = 2,
    StartLocationRevision = 3,
    StopLocationRevision = 4,
}


export interface FiscalTripsNLAuditLogItem { 
    vehicleID: number;
    vehicleLabel: string; 
    startTimestamp: string; 
    creationTimestamp: string; 
    revisionType: FiscalTripsNLAuditLogRevisionTypes;
}

export interface FiscalTripsNLAuditLogRequest extends SearchTermRequest {    
    periodType: PeriodTypes;
    periodSince: string;
    periodTill: string;
    revisionType?: FiscalTripsNLAuditLogRevisionTypes;
    searchTerm: string;
}

export interface FiscalTripsNLAuditLogsResult extends PagerResults {
    results: FiscalTripsNLAuditLogItem[];
}

// ==========
// DETAILS
// ==========

export interface FiscalTripsNLAuditLogChangedValues {
    propertyName: string;
    value: string;    
}

export interface FiscalTripsNLAuditLogDetailResult {    
    timestamp: string;
    revisionType: FiscalTripsNLAuditLogRevisionTypes;
    before: FiscalTripsNLAuditLogChangedValues[];
    after: FiscalTripsNLAuditLogChangedValues[];
}

export interface FiscalTripsNLAuditLogDetailsChangedValues {
    label: string;
    newValue?: string;
    oldValue?: string;
}
