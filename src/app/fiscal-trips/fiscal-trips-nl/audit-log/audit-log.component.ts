﻿// FRAMEWORK
import {
    Component, ViewChild, OnInit,
    OnDestroy, ElementRef, Renderer
} from '@angular/core';
import { Router } from '@angular/router';

// RXJS
import { Subscription, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

// COMPONENTS
import { PeriodSelectorComponent } from '../../../common/period-selector/period-selector.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { FiscalTripsNLAuditLogService } from './audit-log.service';
import { FiscalTripsNLDetailsDialogService } from './details-dialog/details-dialog.service';
import { StoreManagerService } from '../../../shared/store-manager/store-manager.service';
import { Config } from '../../../config/config';

// TYPES
import { Pager } from '../../../common/pager';
import { PeriodTypes, PeriodTypeOption } from '../../../common/period-selector/period-selector-types';
import { KeyName } from '../../../common/key-name';
import {
    TableColumnSetting,
    TableActionOptions,
    TableActionItem,
    TableCellFormatOptions,
    TableColumnSortOrderOptions,
    TableColumnSortOptions
} from '../../../shared/data-table/data-table.types';
import {
    FiscalTripsNLAuditLogRevisionTypes,
    FiscalTripsNLAuditLogItem,
    FiscalTripsNLAuditLogRequest,
    FiscalTripsNLAuditLogsResult
} from './audit-log-types';
import { StoreItems } from '../../../shared/store-manager/store-items';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'audit-log',
    templateUrl: 'audit-log.html'
})

export class FiscalTripsNLAuditLogComponent implements OnInit, OnDestroy {

    @ViewChild(PeriodSelectorComponent, { static: true })
    public periodSelectorComponent: PeriodSelectorComponent;
    @ViewChild('term', { static: true })
    public fileInput: ElementRef;

    public loading: boolean;
    public loadingSearch: boolean;
    public message: string;
    public search: Subject<string> = new Subject();
    public request: FiscalTripsNLAuditLogRequest = {} as FiscalTripsNLAuditLogRequest;
    public pager: Pager = new Pager();
    public data: FiscalTripsNLAuditLogItem[] = [] as FiscalTripsNLAuditLogItem[];
    public actionOptions: KeyName[] = [];
    public periodSelectorOptions: PeriodTypes[] = [
        PeriodTypes.LastWeek,
        PeriodTypes.ThisWeek,
        PeriodTypes.Today,
        PeriodTypes.Custom,
        PeriodTypes.LastMonth,
        PeriodTypes.ThisMonth,
    ];
    public tableSettings: TableColumnSetting[];

    private subscriber: Subscription;
    private searchSubscriber: Subscription;


    constructor(public service: FiscalTripsNLAuditLogService,
                public unsubscribe: UnsubscribeService,
                public translate: CustomTranslateService,
                private router: Router,
                private previousRouteService: PreviousRouteService,
                private detailsDialogService: FiscalTripsNLDetailsDialogService,
                private snackBarService: SnackBarService,
                private renderer: Renderer,
                private storeManager: StoreManagerService,
                private configService: Config) {
        this.actionOptions = this.getActionTypeOptions();
        this.tableSettings = this.getTableSettings();
    }


    // ----------------
    // LIFECYCLE
    // ----------------

    public ngOnInit() {
        const savedData = this.storeManager.getOption(StoreItems.fiscalTripsAuditLog) as FiscalTripsNLAuditLogRequest;
        if (savedData) {
            this.request = savedData;
            if (savedData.periodType === PeriodTypes.Custom) {
                this.periodSelectorComponent.setCustomDates(
                    moment(savedData.periodSince),
                    moment(savedData.periodTill)
                );
            }
        } else {
            this.request.periodType = PeriodTypes.ThisWeek;
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting && x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchSubscriber = this.search
            .pipe(
                debounceTime(700),
                distinctUntilChanged(),
            )
            .subscribe((term) => {
                this.request.searchTerm = term;
                this.get(true);
            });

        this.get(true);
    }


    public ngOnDestroy() {
        this.unsubscribe.removeSubscription([
            this.subscriber,
            this.searchSubscriber
        ]);
    }



    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }



    // ----------------
    // FILTER BAR
    // ----------------

    // Seach
    public onSearch(term: any) {
        this.loadingSearch = true;
        if (term && term.length > 1) {
            this.search.next(term);
        } else {
            this.loadingSearch = false;
        }
    }



    // Entity type and Action function
    public onCustomSelect(item: FiscalTripsNLAuditLogRevisionTypes) {
        this.get(true);
    }



    // ----------------
    // TABLE
    // ----------------

    // fetch data
    public loadMore() {
        if (this.pager.pageNumber < this.pager.totalPages && this.loading === false) {
            this.pager.addPage();
            this.get(false);
        }
    }



    // When the user selected a column to sort (from list view)
    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }



    // On period select
    public onPeriodSelect(pto: PeriodTypeOption): void {
        this.request.periodType = pto.id;
        this.request.periodSince = pto.datetimeSince.format('YYYY-MM-DD HH:mm');
        this.request.periodTill = pto.datetimeTill.format('YYYY-MM-DD HH:mm');
        this.get(true);
    }


    // View details in Dialog
    public onAuditLogDetails(item: TableActionItem): void {
        if (!item) {
            return;
        }

        const record = item.record as FiscalTripsNLAuditLogItem;
        if (item.action !== TableActionOptions.details) {
            return;
        }

        this.service
            .getDetails(record)
            .subscribe((res) => {
                this.detailsDialogService.details(res);
            },
            (err) => {
                if (err.status === 404) {
                    this.snackBarService.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBarService.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    public clearInput(e?: Event) {
        // do not bubble event otherwise focus is on the textbox
        if (e) {
            e.stopPropagation();
        }

        // clear selection
        this.request.searchTerm = null;
        this.search.next(null);

        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.fileInput.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }


    // =========================
    // PRIVATE
    // =========================

    // Fetch audit entries
    private get(resetPageNumber: boolean) {
        if (this.loading || !this.request.periodSince || !this.request.periodTill) {
            return;
        }

        this.loading = true;

        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.pager.pageSize = 20;
            this.data = new Array<FiscalTripsNLAuditLogItem>();
        }

        this.request.pageSize = this.pager.pageSize;
        this.request.pageNumber = this.pager.pageNumber;

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.fiscalTripsAuditLog, this.request);

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.subscriber);

        // then start the new request
        this.subscriber = this.service
            .get(this.request)
            .subscribe((response: FiscalTripsNLAuditLogsResult) => {
                this.loading = false;
                this.loadingSearch = false;
                if (resetPageNumber === true) {
                    this.data = response && response.results
                        ? response.results
                        : [];
                } else {
                    this.data = this.data.concat(response.results);
                }

                // Get the paging info
                this.pager.pageNumber = response.pageNumber;
                this.pager.pageSize = response.pageSize;
                this.pager.totalPages = response.totalPages;
                this.pager.totalRecords = response.totalRecords;
            },
            () => {
                this.loading = false;
                this.loadingSearch = false;
                this.data = [];
                this.message = 'data.error';
            });
    }

    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                primaryKey: 'creationTimestamp',
                header: 'fiscal_trips.nl.audit_log.revision.created',
                format: TableCellFormatOptions.dateTime,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.desc, true, null)
            },
            {
                primaryKey: 'revisionType',
                header: 'fiscal_trips.nl.audit_log.revision_type',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: FiscalTripsNLAuditLogItem) => {
                    const action = this.actionOptions.find((x) => x.id === Number(item.revisionType));
                    return action !== undefined
                        ? action.name
                        : 'N/A';
                },
            },
            {
                primaryKey: 'startTimestamp',
                header: 'fiscal_trips.nl.audit_log.revision.trip_start',
                format: TableCellFormatOptions.dateTime,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.desc, true, null)
            },
            {
                primaryKey: 'vehicleLabel',
                header: 'labels.vehicle.licence_plate',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.desc, true, null)
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.details],
            }
        ];
    }

    private getActionTypeOptions(): KeyName[] {
        return [
            {
                id: null,
                name: 'all',
                term: 'form.actions.all',
                enabled: true
            },
            {
                id: FiscalTripsNLAuditLogRevisionTypes.CommentRevision,
                name: 'comment_revision',
                term: 'fiscal_trips.nl.audit_log.revision_type.comment_revision',
                enabled: true
            },
            {
                id: FiscalTripsNLAuditLogRevisionTypes.DeviationRevision,
                name: 'deviation_revision',
                term: 'fiscal_trips.nl.audit_log.revision_type.deviation_revision',
                enabled: true
            },
            {
                id: FiscalTripsNLAuditLogRevisionTypes.DistanceRevision,
                name: 'distance_revision',
                term: 'fiscal_trips.nl.audit_log.revision_type.distance_revision',
                enabled: true
            },
            {
                id: FiscalTripsNLAuditLogRevisionTypes.StartLocationRevision,
                name: 'start_location_revision',
                term: 'fiscal_trips.nl.audit_log.revision_type.start_location_revision',
                enabled: true
            },
            {
                id: FiscalTripsNLAuditLogRevisionTypes.StopLocationRevision,
                name: 'stop_location_revision',
                term: 'fiscal_trips.nl.audit_log.revision_type.stop_location_revision',
                enabled: true
            },
        ];
    }
}
