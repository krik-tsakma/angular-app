// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';

// COMPONENTS
import { FiscalTripsNLAuditLogComponent } from './audit-log.component';

const FiscalTripsNLAuditLogRoutes: Routes = [
    {
        path: '',
        component: FiscalTripsNLAuditLogComponent,
        canActivate: [AuthGuard]
    }    
];

@NgModule({
    imports: [
        RouterModule.forChild(FiscalTripsNLAuditLogRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class FiscalTripsNLAuditLogRoutingModule { }
