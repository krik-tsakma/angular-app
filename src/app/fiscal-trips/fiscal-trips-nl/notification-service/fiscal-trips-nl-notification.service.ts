// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

// RXJS
import { Subject } from 'rxjs';

// SIGNALR
import * as signalR from '@aspnet/signalr';

// SERVICES
import { Config } from '../../../config/config';

// TYPES
import { FiscalTripNLCorrectionCodeOptions } from '../fiscal-trips-nl-types';
import { FiscalTripsNLNotificationComponent } from './fiscal-trips-nl-notification.component';

@Injectable()
export class FiscalTripsNLNotificationService {
    public adjustOdometerNotificationReceived: Subject<FiscalTripNLCorrectionCodeOptions> = new Subject<FiscalTripNLCorrectionCodeOptions>();

    private hubConnection: signalR.HubConnection = null;
    private hubEndPoint: string = this.config.get('fiscalTripsReviewHub');
   
    constructor(private config: Config,
                private dialog: MatDialog) {
        // foo
    }
        
    public Start(): void {
     
        this.hubConnection = new signalR.HubConnectionBuilder()
                            .withUrl(this.hubEndPoint)
                            .build();


        this.hubConnection.on('OnCorrectionTrip', (res: FiscalTripNLCorrectionCodeOptions) => {
            console.log('odometer notification:', res);
            this.adjustOdometerNotificationReceived.next(res);
        });


        this.hubConnection.on('SendAction', (res: any) => {
            console.log(res);
        });
        
        this.hubConnection
            .start()
            .then(() => console.log('Connection with Fiscal Trips Review started!'))
            .catch(() => console.log('Error while establishing connection with Fiscal Trips Review Hub'));
    }

    public Stop(): void {
        if (this.hubConnection === null) {
            return;
        }
        this.hubConnection
            .stop()
            .then(() => {
                this.hubConnection = null;
            })
            .catch(() => console.log('Error while terminating connection with Fiscal Trips Review Hub'));
    }

    public ShowMessage(message: string) {
        this.dialog.open(FiscalTripsNLNotificationComponent, {
            data: message
        });
    }
} 
