﻿// FRAMEWORK
import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'fiscal-trip-nl-notification',
    templateUrl: 'fiscal-trip-nl-notification.html',
    encapsulation: ViewEncapsulation.None,
})

export class FiscalTripsNLNotificationComponent {
    
    constructor(public dialogRef: MatDialogRef<FiscalTripsNLNotificationComponent>,
                @Inject(MAT_DIALOG_DATA) public data: string) { 
            // foo
    }


    public closeDialog() {
        this.dialogRef.close();
    }
}
