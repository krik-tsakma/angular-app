﻿
// ================
// DRIVER SUMMARY
// ================  

export interface FiscalTripsNLDriverSummaryRequest {        
    periodSince: string;
    periodTill: string;
}


export interface FiscalTripsNLDriverSummaryResult {        
    totalBusiness: number;
    totalPrivate: number;
    oldestNonValidatedTrip: string;
}

