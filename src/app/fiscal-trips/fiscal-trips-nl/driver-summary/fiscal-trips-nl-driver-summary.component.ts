﻿// FRAMEWORK
import {
    Component, OnDestroy,
    ViewEncapsulation, OnChanges,
    SimpleChanges,
    Input,
    EventEmitter,
    Output
} from '@angular/core';

// SERVICES
import { FiscalTripsNLService } from '../fiscal-trips-nl.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';

// TYPES
import { FiscalTripsNLDriverSummaryRequest, FiscalTripsNLDriverSummaryResult } from './fiscal-trips-nl-driver-summary-types';
import { DateFormatterDisplayOptions } from '../../../shared/pipes/dateFormat.pipe';

@Component({
    selector: 'fiscal-trips-nl-driver-summary',
    templateUrl: './fiscal-trips-nl-driver-summary.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./fiscal-trips-nl-driver-summary.less']
})

export class FiscalTripsNLDriverSummaryComponent implements OnDestroy, OnChanges {
    @Input() public modelData: FiscalTripsNLDriverSummaryRequest;
    @Output() public oldestNonValidatedTripEvent: EventEmitter<string> = new EventEmitter<string>();

    public loading: boolean;
    public message: string;
    public dateDisplayOptions: any = DateFormatterDisplayOptions;
    public data: FiscalTripsNLDriverSummaryResult;
  
    private serviceSubscriber: any;

    constructor(
        public service: FiscalTripsNLService,
        public userOptions: UserOptionsService,
        private unsubscribe: UnsubscribeService) {
            // foo
    }

    
    // =========================
    // LIFECYCLE
    // =========================

    public ngOnChanges(changes: SimpleChanges) {
        // on every change of the model data
        const modelDataChange = changes['modelData'];
        if (modelDataChange && modelDataChange.currentValue) {
            if (this.modelData.periodSince && this.modelData.periodTill) {
                this.get();
            }
        }
    }

    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(
            [
                this.serviceSubscriber,
            ]
        );
    }

    // =========================
    // EVENT HANDLERS
    // =========================

    public oldestNonValidatedTripClick() {
        this.oldestNonValidatedTripEvent.emit(this.data.oldestNonValidatedTrip);
    }

    // =========================
    // DATA
    // =========================

    // Fetch trips data
    private get() {
        this.message = '';
        this.loading = true;
   
        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.serviceSubscriber);

        // then start the new request
        this.serviceSubscriber = this.service
            .getFiscalTripsNLDriverSummary(this.modelData)
            .subscribe((response: FiscalTripsNLDriverSummaryResult) => {
                this.loading = false;                
                this.data = response;
            }, (err) => {
                this.loading = false;
                this.message = 'data.error';
            });
    }
}
