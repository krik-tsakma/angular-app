﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { 
    FiscalTripsNLRequest, 
    FiscalTripsNLTripsResults, 
    FiscalTrip, 
    FiscalTripNLValidateCodeOptions, 
    FiscalTripNLEditCodeOptions,
    FiscalTripNLValidateMultipleRequest, 
    FiscalTripNLCorrectionTripRequest,
    FiscalTripNLCorrectionCodeOptions,
    FiscalTripNLSplitCodeOptions,
    FiscalTripNLSplitRequest,
    ReplacementVehicleCreateRequest,
    ReplacementVehicleCreateResult,
    FiscalTripNLReplacementVehicleTripCodeOptions
} from './fiscal-trips-nl-types';
import { KeyName } from '../../common/key-name';
import { FiscalTripsNLDriverSummaryResult, FiscalTripsNLDriverSummaryRequest } from './driver-summary/fiscal-trips-nl-driver-summary-types';
import { CombineTripsRequest } from './combine-trips/combine-trips-types';


@Injectable()
export class FiscalTripsNLService {
    private baseURL: string;    

    constructor(
        private authHttp: AuthHttp,
        private configService: Config) {
        this.baseURL = this.configService.get('apiUrl') + '/api/FiscalTripsNL';
    }

    // Get fiscal trips driver summary
    public getFiscalTripsNLDriverSummary(request: FiscalTripsNLDriverSummaryRequest): Observable<FiscalTripsNLDriverSummaryResult> {

        const params = new HttpParams({
            fromObject: {
                Since: request.periodSince,
                Till: request.periodTill,
            }
        });

        return this.authHttp
            .get(this.baseURL + '/DriverSummary/', { params });
    }

    // Get Fiscal Trips NL for driver
    public getFiscalTripsNL(request: FiscalTripsNLRequest): Observable<FiscalTripsNLTripsResults> {

        const params = new HttpParams({
            fromObject: {
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Since: request.periodSince,
                Till: request.periodTill,
                Sorting: request.sorting,
                OnlyNonValidated: request.onlyNonValidated !== undefined ? request.onlyNonValidated.toString() : 'false'
            }
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }

    // Edit selected Fiscal Trip
    public editFiscalTrip(request: FiscalTrip): Observable<FiscalTripNLEditCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/EditFiscalTrip', request, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as FiscalTripNLEditCodeOptions;
                    }
                })
            );  
    }


    // Edit selected Fiscal Trip
    public insertCorrectionTrip(request: FiscalTripNLCorrectionTripRequest): Observable<FiscalTripNLCorrectionCodeOptions> {
        return this.authHttp
            .post(this.baseURL + '/InsertCorrectionTrip', request, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as FiscalTripNLCorrectionCodeOptions;
                    }
                })
            );  
    }


    // =================
    // VALIDATE TRIP(S)
    // =================

    public validateFiscalTrip(entity: FiscalTrip): Observable<FiscalTripNLValidateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/ValidateFiscalTrip', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as FiscalTripNLValidateCodeOptions;
                    }
                })
            );            
    }


    public validateFiscalTripsWithingRange(entity: FiscalTripNLValidateMultipleRequest): Observable<FiscalTripNLValidateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/ValidateTripsWithinRange', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as FiscalTripNLValidateCodeOptions;
                    }
                })
            );            
    }

    // ========================
    // COMBINE / SPLIT TRIP(S)
    // ========================
    
    
    public combineSelectedTrips(request: CombineTripsRequest): Observable<FiscalTripNLEditCodeOptions> {
        return this.authHttp
            .post(this.baseURL + '/CombineTrips', request, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as FiscalTripNLEditCodeOptions;
                    }
                })
            );  
    }


    public splitCompinedFiscalTrip(entity: FiscalTripNLSplitRequest): Observable<FiscalTripNLSplitCodeOptions>  {
        return this.authHttp
            .post(this.baseURL + '/SplitCombinedTrip', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as FiscalTripNLSplitCodeOptions;
                    }
                })
            );            
    }


    // =========================
    // REPLACEMENT VEHICLES
    // ========================

    public getReplacementVehicles(request: string): Observable<KeyName[]> {
        const params = new HttpParams({
            fromObject: {
                SearchTerm: request
            }
        });

        return this.authHttp
            .get(this.configService.get('apiUrl') + '/api/FiscalTripsNLReplacememtVehicle', { params });
    }


    public insertReplacementVehicle(request: ReplacementVehicleCreateRequest): Observable<ReplacementVehicleCreateResult> {     
        return this.authHttp
            .post(this.configService.get('apiUrl') + '/api/FiscalTripsNLReplacememtVehicle', request, { observe: 'response' })
            .pipe(
                map((res) => {
                    console.log(res);
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ReplacementVehicleCreateResult;
                    }
                })
            );  
    }

    public insertReplacementVehicleFiscalTrip(request: FiscalTrip): Observable<FiscalTripNLReplacementVehicleTripCodeOptions> {
        return this.authHttp
            .post(this.baseURL + '/InsertReplacementVehicleFiscalTrip', request, { observe: 'response' })
            .pipe(
                map((res) => {                    
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as FiscalTripNLReplacementVehicleTripCodeOptions;
                    }
                })
            );  
    }

    public deleteReplacementVehicleFiscalTrip(request: FiscalTrip): Observable<number> {
        const params = new HttpParams({
            fromObject: {
                VehicleID: request.vehicleID.toString(),
                StartTimestamp: request.startTimestamp.toString()
            }
        });

        return this.authHttp
            .delete(this.baseURL + '/RemoveReplacementFiscalTrip',  { params, observe: 'response' })
            .pipe(
                map((res) => res.status)
            ); 
    }

    public editReplacementFiscalTrip(request: FiscalTrip): Observable<FiscalTripNLReplacementVehicleTripCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/EditReplacementFiscalTrip', request, { observe: 'response' })
            .pipe(
                map((res) => {                    
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as FiscalTripNLReplacementVehicleTripCodeOptions;
                    }
                })
            );  
    }
}
