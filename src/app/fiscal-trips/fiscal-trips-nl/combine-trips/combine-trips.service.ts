﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { CombineTripsComponent } from './combine-trips.component';

// TYPES
import { FiscalTrip } from '../fiscal-trips-nl-types';

@Injectable()
export class CombineTripsService {
    constructor(private dialog: MatDialog) { }

    public open(fiscalTrips: FiscalTrip[]): Observable<boolean> {

        let dialogRef: MatDialogRef<CombineTripsComponent>;

        dialogRef = this.dialog.open(CombineTripsComponent);
        dialogRef.componentInstance.trips = fiscalTrips;

        return dialogRef.afterClosed();
    }    
}
