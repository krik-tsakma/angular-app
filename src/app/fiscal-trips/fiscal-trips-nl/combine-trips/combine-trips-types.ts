
export interface CombineTripsRequest {   
    VehicleID: number;
    FirstTripStartTimestamp: string;
    LastTripStopTimestamp: string;
    Comment?: string;
}

export interface CombinedTripMessage {
    startTimestamp: string;
    stopTimestamp: string;
    duration: string;
    mileage: string;
}
