﻿// FRAMEWORK
import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// SERVICES
import { FiscalTripsNLService } from '../fiscal-trips-nl.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';

// PIPES
import { NumberFormatPipe } from '../../../shared/pipes/numberFormat.pipe';
import { StringFormatPipe } from '../../../shared/pipes/stringFormat.pipe';

// TYPES
import { FiscalTrip, FiscalTripNLEditCodeOptions } from '../fiscal-trips-nl-types';
import { CombineTripsRequest, CombinedTripMessage } from './combine-trips-types';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'combine-trips',
    templateUrl: 'combine-trips.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['combine-trips.less'],
    providers: [ StringFormatPipe, NumberFormatPipe ]
})

export class CombineTripsComponent implements OnInit {
    public trips: FiscalTrip[];
    public combinedTrip: CombinedTripMessage = {} as CombinedTripMessage;
    public request: CombineTripsRequest = {} as CombineTripsRequest;
    public comment: string;
    public loading: boolean;
    private serviceSubscriber: any;

    constructor(
        public service: FiscalTripsNLService,
        private userOptions: UserOptionsService,
        private dialogRef: MatDialogRef<CombineTripsComponent>,
        private snackBar: SnackBarService,
        private unsubscribe: UnsubscribeService,
        private translate: CustomTranslateService,
        private stringFormatPipe: StringFormatPipe,
        private numberFormatPipe: NumberFormatPipe) {
        // foo
    }



    public ngOnInit() {
        this.combinedTrip = {
            startTimestamp: moment(this.trips[0].startTimestamp).format(this.userOptions.localOptions.short_date + ' HH:mm'),
            stopTimestamp:
                moment(this.trips[this.trips.length - 1].stopTimestamp).format(this.userOptions.localOptions.short_date + ' HH:mm'),
            duration: this.findDuration(this.trips[this.trips.length - 1].stopTimestamp, this.trips[0].startTimestamp),
            mileage: this.numberFormatPipe.transform(
                this.trips.reduce((accumulator, currentValue) => {
                    return accumulator + currentValue.totalDistance;
                }, 0),
                {
                    decimalSeparator: this.userOptions.localOptions.decimal_separator,
                    thousandsSeparator: this.userOptions.localOptions.thousands_separator,
                    decimals: 2
                }
            )
        };
    }



    public findDuration(endTime, startTime) {
        const diff = moment(endTime).diff(moment(startTime));
        const ms = moment.duration(diff);
        const hours = Math.floor(ms.asHours());
        const mins = (Math.floor(ms.asMinutes()) - hours * 60) < 10
            ? `0${Math.floor(ms.asMinutes()) - hours * 60}`
            : Math.floor(ms.asMinutes()) - hours * 60;

        return `${hours}:${mins}`;
    }


    public closeDialog(successfullyEdited?: boolean) {
        this.dialogRef.close(successfullyEdited);
    }


    public combine() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.request = {
            VehicleID: this.trips[0].vehicleID,
            FirstTripStartTimestamp: this.trips[0].startTimestamp,
            LastTripStopTimestamp: this.trips[this.trips.length - 1].stopTimestamp,
            Comment: this.comment
        };

        this.serviceSubscriber = this.service
            .combineSelectedTrips(this.request)
            .subscribe((res: FiscalTripNLEditCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.closeDialog(true);
                } else {
                    const message = this.getUpdateResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }



    // =========================
    // RESULTS
    // =========================

    private getUpdateResult(code: FiscalTripNLEditCodeOptions): string {
        let message = '';
        const required = this.translate.instant('form.errors.required');
        switch (code) {
            case FiscalTripNLEditCodeOptions.StartAddressEmpty:
                const startAddress = this.translate.instant('fiscal_trips.nl.start_address');
                message = this.stringFormatPipe.transform(required, [startAddress]);
                break;
            case FiscalTripNLEditCodeOptions.StopAddressEmpty:
                const stopAddress = this.translate.instant('fiscal_trips.nl.stop_address');
                message = this.stringFormatPipe.transform(required, [stopAddress]);
                break;
            case FiscalTripNLEditCodeOptions.TripNotFound:
                message = 'form.errors.not_found';
                break;
            case FiscalTripNLEditCodeOptions.PrivateAndBusinessDistNotEqualToTripDist:
                message = 'fiscal_trips.nl.edit.errors.private_and_business_dist_not_equal_to_trip_dist';
                break;
            case FiscalTripNLEditCodeOptions.DeviationDistGreaterOrEqualToTripDist:
                message = 'fiscal_trips.nl.edit.errors.deviation_dist_greater_or_equal_to_trip_dist';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
