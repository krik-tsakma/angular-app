// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewChild, ViewEncapsulation, } from '@angular/core';
import { Router } from '@angular/router';

// RXJS
import { Subscription } from 'rxjs/Subscription';

// COMPONENTS
import { PeriodSelectorComponent } from '../../common/period-selector/period-selector.component';
import {
    MoreOptionsEditingOptionsDialogComponent
} from '../../shared/data-table/more-options-editing-options-dialog/more-options-edit-options-dialog.component';

// SERVICES
import { FiscalTripsNLService } from './fiscal-trips-nl.service';
import { EditTripService } from './edit-trip/edit-trip.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';
import { ConfirmDialogService } from '../../shared/confirm-dialog/confirm-dialog.service';
import { InsertCorrectionTripService } from './insert-correction-trip/insert-correction-trip.service';
import { InsertReplacementVehicleTripService } from './insert-replacement-vehicle-trip/insert-replacement-vehicle-trip.service';
import { CombineTripsService } from './combine-trips/combine-trips.service';
import { FiscalTripsNLNotificationService } from './notification-service/fiscal-trips-nl-notification.service';
import { Config } from '../../config/config';

// TYPES
import {
    FiscalTrip,
    FiscalTripsNLRequest,
    FiscalTripNLValidateCodeOptions,
    FiscalTripNLValidateMultipleRequest,
    FiscalTripNLCorrectionCodeOptions,
    FiscalTripNLSplitCodeOptions
} from './fiscal-trips-nl-types';
import { Pager } from '../../common/pager';
import { KeyName } from '../../common/key-name';
import { PeriodTypes, PeriodTypeOption } from '../../common/period-selector/period-selector-types';
import {
    MoreOptionsMenuOption,
    MoreOptionsMenuOptions,
    MoreOptionsMenuTypes
} from '../../shared/more-options-menu/more-options-menu.types';
import { ConfirmationDialogParams } from '../../shared/confirm-dialog/confirm-dialog-types';
import { StoreItems } from '../../shared/store-manager/store-items';
import { FiscalTripsNLDriverSummaryRequest } from './driver-summary/fiscal-trips-nl-driver-summary-types';
import {
    TableColumnSetting, TableActionOptions,
    TableCellFormatOptions, TableColumnSortOrderOptions, TableColumnSortOptions, TableActionItem
} from '../../shared/data-table/data-table.types';
import {
    FiscalTripsNLEditingOptions,
    FiscalTripsNLEditingOptionsDialogCloseResult
} from './editing-options-dialog/editing-options-dialog-types';
import { FiscalTripsNLMultipleEditingOptions } from './editing-options-multiple/editing-options-multiple';

// PIPES
import { DateFormatterDisplayOptions } from '../../shared/pipes/dateFormat.pipe';

// LODASH-ES
import orderBy from 'lodash-es/orderBy';

// MOMENT
import moment from 'moment';

@Component({
    selector: 'fiscal-trips-nl',
    templateUrl: './fiscal-trips-nl.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./fiscal-trips-nl.less']
})

export class FiscalTripsNLComponent implements OnInit, OnDestroy {
    @ViewChild(PeriodSelectorComponent, { static: true }) public periodSelectorComponent: PeriodSelectorComponent;
    @ViewChild(MoreOptionsEditingOptionsDialogComponent, { static: true })
        public editingOptionsDialog: MoreOptionsEditingOptionsDialogComponent;

    public pageOptions: MoreOptionsMenuOptions[];
    public dateDisplayOptions: any = DateFormatterDisplayOptions;
    public periodSelectorOptions: PeriodTypes[] = [
        PeriodTypes.ThisMonth,
        PeriodTypes.LastMonth,
        PeriodTypes.LastWeek,
        PeriodTypes.ThisWeek,
        PeriodTypes.Today,
        PeriodTypes.Custom
    ];
    public previousDay: moment.Moment;
    public nextDay: moment.Moment;
    public message: string;
    public loading: boolean;
    public fiscalTrips: FiscalTrip[];
    public disableNextDate: boolean;
    public pager: Pager = new Pager();
    public request: FiscalTripsNLRequest = {} as FiscalTripsNLRequest;
    public driverSummaryRequest: FiscalTripsNLDriverSummaryRequest;
    public selectedTrips: FiscalTrip[] = [];
    public tableSettings: TableColumnSetting[];

    private serviceSubscriber: Subscription;
    private validateTripSubscriber: Subscription;
    private combineTripsSubscriber: Subscription;
    private splitTripSubscriber: Subscription;
    private removeTripSubscriber: Subscription;

    constructor(
        public service: FiscalTripsNLService,
        public editTripService: EditTripService,
        public translate: CustomTranslateService,
        private confirmDialogService: ConfirmDialogService,
        private insertCorrectionTripService: InsertCorrectionTripService,
        private insertReplacementVehicleTripService: InsertReplacementVehicleTripService,
        private combineSelectedTripsService: CombineTripsService,
        private unsubscribe: UnsubscribeService,
        private snackBar: SnackBarService,
        private storeManager: StoreManagerService,
        private router: Router,
        private notificationService: FiscalTripsNLNotificationService,
        private configService: Config) {
            this.tableSettings = this.getTableSettings();
            this.pageOptions = this.fiscalPageOptions();
    }


    // =========================
    // LIFECYCLE
    // =========================

    public ngOnInit() {
        const savedData = this.storeManager.getOption(StoreItems.fiscalTrips) as FiscalTripsNLRequest;
        if (savedData) {
            this.request = savedData;
            if (savedData.periodType === PeriodTypes.Custom) {
                this.periodSelectorComponent.setCustomDates(
                    moment(savedData.periodSince),
                    moment(savedData.periodTill)
                );
            }
        } else {
            this.request.periodType = PeriodTypes.Today;
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting && x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        // Option that disables next date button because next date > today
        this.disableNextDate = true;
        this.previousDay = moment().subtract(1, 'days');
        this.nextDay = moment().add(1, 'days');

        this.get(true);

        this.editingOptionsDialog.afterClosed().subscribe((res: FiscalTripsNLEditingOptionsDialogCloseResult) => {
            switch (res.action) {
                case FiscalTripsNLEditingOptions.edit:
                    this.editFiscalTrip(res.trip);
                    break;
                case FiscalTripsNLEditingOptions.validate:
                    this.validateFiscalTrip(res.trip);
                    break;
                case FiscalTripsNLEditingOptions.split:
                    this.splitCombinedFiscalTrip(res.trip);
                    break;
                case FiscalTripsNLEditingOptions.remove:
                    this.removeReplacementFiscalTrip(res.trip);
                    break;
            }
        });

        this.notificationService.Start();
        this.notificationService.adjustOdometerNotificationReceived.subscribe((res: FiscalTripNLCorrectionCodeOptions) => {
            // console.log('adjust odometer completed with code:',  res);
            this.notificationService.ShowMessage('adjust odometer completed with code:' +  res);
        });
    }


    public ngOnDestroy() {
        this.notificationService.Stop();
        this.unsubscribe.removeSubscription(
            [
                this.serviceSubscriber,
                this.validateTripSubscriber,
                this.combineTripsSubscriber,
                this.removeTripSubscriber
            ]
        );
    }


    // =========================
    // EVENT HANDLERS
    // =========================

    public onSelectPageOption(item: MoreOptionsMenuOption): void {
        switch (item.id) {
            case 'auditLog':
                this.router.navigate([this.router.url, 'audit-log'], { skipLocationChange: !this.configService.get('router') });
                break;
            case 'insertCorrectionTrip':
                this.insertCorrectionTrip();
                break;
            case 'insertReplacementVehicleTrip':
                this.insertReplacementVehicleTrip();
                break;
        }
    }

    public onAllNonValidated() {
        this.get(true);
    }

    // =========================
    // MULTIPLE ROWS ACTIONS
    // =========================

    public onMultipleRowActionSelect(action: FiscalTripsNLMultipleEditingOptions) {
        switch (action) {
            case FiscalTripsNLMultipleEditingOptions.combine:
                this.combineSelectedTrips();
                break;
            case FiscalTripsNLMultipleEditingOptions.validate:
                this.validateTripsWithinRange();
                break;
            default:
                break;
        }
    }

    // =========================
    // DATA TABLE CALLBACKS
    // =========================

    // When the user selects the more options or checkbox  actions
    public onShowTripMenu(item: TableActionItem) {
        if (!item) {
            return;
        }
        const trip = item.record as FiscalTrip;
        switch (item.action) {
            case TableActionOptions.check:
                const trips = this.selectedTrips.slice(0);
                const index: number = trips.findIndex((t) => {
                    return t.vehicleID === trip.vehicleID && t.startTimestamp === trip.startTimestamp;
                });
                if (index === -1) {
                    trips.push(trip);
                } else {
                    trips.splice(index, 1);
                }

                // to enforce model state change first set table to null and then add/remove values
                this.selectedTrips = trips;

                break;
            case TableActionOptions.moreOptions:
                const options = this.getOptions(trip);
                this.editingOptionsDialog.open(trip, options, { x: item.e.x, y: item.e.y });
                break;
            default:
                break;
        }
    }


    // When the user selected a column to sort
    public onSort(column: TableColumnSetting) {
        if (column.sorting) {
            const sortCol = new TableColumnSortOptions(column.sorting.order, column.sorting.selected, column.sorting.name);
            if (sortCol) {
                this.request.sorting = sortCol.stringFormat(column.primaryKey);
                console.log(this.request.sorting);
                this.get(true);
            }
        }
    }


    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    // =========================
    // DRIVER SUMMARY CALLBACK
    // =========================

    public oldestNonValidatedTripClick(timestamp: string) {
        const date = moment(timestamp);
        this.periodSelectorComponent.setCustomDates(
            date.startOf('day'),
            date.endOf('day')
        );
        this.get(true);
    }


    // =========================
    // PERIOD SELECTOR CALLBACK
    // =========================

    // On period select
    public onPeriodSelect(pto: PeriodTypeOption): void {
        this.request.periodType = pto.id;
        this.request.periodSince = pto.datetimeSince.format('YYYY-MM-DD HH:mm');
        this.request.periodTill = pto.datetimeTill.format('YYYY-MM-DD HH:mm');

        this.driverSummaryRequest = {
            periodSince: this.request.periodSince,
            periodTill: this.request.periodTill
        } as FiscalTripsNLDriverSummaryRequest;

        this.get(true);
    }


    // =================================
    // DATES EVENT HANDLERS / HELPERS
    // =================================

    // Click on previous date button
    public onPreviousDate() {
        // Make the selection and fetch datas
        this.periodSelectorComponent.setCustomDates(
            this.previousDay.clone().startOf('day'),
            this.previousDay.clone().endOf('day')
        );

        // Update Moment previous date - next date values
        const prevDate = this.previousDay.clone().subtract(1, 'days');
        const nextDate = this.nextDay.clone().subtract(1, 'days');
        this.previousDay = prevDate;
        this.nextDay = nextDate;

        // Enables or disables next button if next date > today
        this.disableNextDate = moment().isSameOrBefore(nextDate);
    }

    // Click on next date button
    public onNextDate() {
        // Make the selection and fetch data
        this.periodSelectorComponent.setCustomDates(
            this.nextDay.clone().startOf('day'),
            this.nextDay.clone().endOf('day'));

        // Update Moment previous date - next date values
        const prevDate = this.previousDay.clone().add(1, 'days');
        const nextDate = this.nextDay.clone().add(1, 'days');
        this.previousDay = prevDate;
        this.nextDay = nextDate;

        // Enables or disables next button if next date > today
        this.disableNextDate = moment().isSameOrBefore(nextDate);
    }

    public isTheSameDay(timestamp1, timestamp2) {
        return moment(timestamp1).isSame(moment(timestamp2), 'day');
    }

    // =========================
    // DATA
    // =========================

    // Fetch trips data
    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.fiscalTrips = new Array<FiscalTrip>();
            // clear any previous selections
            this.selectedTrips = [];
        }

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.serviceSubscriber);

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.fiscalTrips, this.request);

        // then start the new request
        this.serviceSubscriber = this.service
            .getFiscalTripsNL(this.request)
            .subscribe((response) => {
                this.loading = false;
                // Get the actual data
                this.fiscalTrips = this.fiscalTrips.concat(response.results);

                // Get the paging info
                this.pager.pageNumber = response.pageNumber;
                this.pager.pageSize = response.pageSize;
                this.pager.totalPages = response.totalPages;
                this.pager.totalRecords = response.totalRecords;

                if (!this.fiscalTrips || this.fiscalTrips.length === 0) {
                    this.message = 'data.empty_records';
                }
            }, (err) => {
                this.loading = false;
                this.message = 'data.error';
            });
    }

    // Edit selected fiscal trip
    private editFiscalTrip(item: FiscalTrip) {
        if (item.isReplacementVehicle) {
            item.currentReplacementVehicleID = item.vehicleID;
            item.currentStartTimestamp = item.startTimestamp;
            this.insertReplacementVehicleTrip(Object.assign({}, item));
        } else {
            this.editTripService
                .open(Object.assign({}, item))
                .subscribe((result: boolean) => {
                    if (result === true) {
                        this.get(true);
                        this.refreshSummaryWidget();
                    }
                });
        }
    }


    // Insert correction trip after specified trip
    private insertCorrectionTrip() {
        this.insertCorrectionTripService
            .open()
            .subscribe((result: boolean) => {
                if (result === true) {
                    this.get(true);
                    this.refreshSummaryWidget();
                }
            });
    }

    // =================
    // VALIDATE TRIP(S)
    // =================

    // Validate selected fiscal trip
    private validateFiscalTrip(item: FiscalTrip) {
        const req = {
            title: 'fiscal_trips.nl.validate.title',
            message: 'form.warnings.are_you_sure',
            submitButtonTitle: 'fiscal_trips.nl.validate.button'
        } as ConfirmationDialogParams;

        this.unsubscribe.removeSubscription(this.validateTripSubscriber);
        this.confirmDialogService
            .confirm(req)
            .subscribe((result) => {
                if (result === 'yes') {
                    this.validateTripSubscriber = this.service
                        .validateFiscalTrip(item)
                        .subscribe((res: FiscalTripNLValidateCodeOptions) => {
                            if (res === null) {
                                this.snackBar.open('fiscal_trips.nl.validate.success', 'form.actions.close');
                                this.get(true);
                                this.refreshSummaryWidget();
                            } else {
                                this.snackBar.open('form.errors.not_found', 'form.actions.close');
                            }
                        },
                        (err) => {
                            if (err.status === 404) {
                                this.snackBar.open('form.errors.not_found', 'form.actions.close');
                            } else {
                                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                            }
                        });
                }
                return false;
            });
    }


    // Validate multiple fiscal trips
    private validateTripsWithinRange() {
        const req = {
            title: 'fiscal_trips.nl.validate.title',
            message: 'form.warnings.are_you_sure',
            submitButtonTitle: 'fiscal_trips.nl.validate.button'
        } as ConfirmationDialogParams;

        const sorted = this.selectedTrips.sort((t1, t2) => {
            return moment(t1.startTimestamp).milliseconds() - moment(t2.startTimestamp).milliseconds();
        });
        this.confirmDialogService
            .confirm(req)
            .subscribe((result) => {
                if (result === 'yes') {
                    const entity = {
                        from: sorted[0].startTimestamp,
                        till: sorted[sorted.length - 1].startTimestamp
                    } as FiscalTripNLValidateMultipleRequest;
                    this.validateTripSubscriber = this.service
                        .validateFiscalTripsWithingRange(entity)
                        .subscribe((res: FiscalTripNLValidateCodeOptions) => {
                            if (res === null) {
                                this.snackBar.open('fiscal_trips.nl.validate.success', 'form.actions.close');
                                this.get(true);
                                this.refreshSummaryWidget();
                            }
                        },
                        (err) => {
                            this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                        });
                }
                return false;
            });
    }

    // ========================
    // COMBINE / SPLIT TRIP(S)
    // ========================


    // Combine multiple trips into one
    private combineSelectedTrips() {
        // always orderBy startTimestamp before checking if selected
        const orderedTripsByDate = orderBy(this.fiscalTrips.map((res) => res), ['startTimestamp'], ['asc']);
        // always orderBy startTimestamp selected trips
        const selectedOrdered = orderBy(this.selectedTrips, ['startTimestamp'], ['asc']);

        // find selected indexes in orderedTripsByDate array
        const indexedSortedTrips = selectedOrdered.map((selectedTrip) => {
            return orderedTripsByDate.findIndex((elem) => elem === selectedTrip);
        });

        // Check if indexes are consecutive
        const checked = indexedSortedTrips.some((elem, index) => {
            if (index > 0) {
                return !(elem === (indexedSortedTrips[index - 1] + 1));
            }
        });

        // if checked is false, selected trips are consecutive;
        if (!checked) {
            // check if selected trips are made by the same vehicle
            if (selectedOrdered.length === selectedOrdered.filter((elem) => elem.vehicleLabel === selectedOrdered[0].vehicleLabel).length) {
                this.combineTripsSubscriber = this.combineSelectedTripsService
                    .open(selectedOrdered)
                    .subscribe((result: boolean) => {
                        if (result === true) {
                            this.get(true);
                        }
                    });
            } else {
                this.snackBar.open('fiscal_trips.nl.combine_trips.warning', 'form.actions.ok');
            }
        } else {
            this.snackBar.open('fiscal_trips.nl.combine_trips.warning', 'form.actions.ok');
        }
    }


    // Split selected combined fiscal trip
    private splitCombinedFiscalTrip(item: FiscalTrip) {
        if (!item.isCombined) {
            return;
        }

        const req = {
            title: 'fiscal_trips.nl.split.title',
            message: 'form.warnings.are_you_sure',
            submitButtonTitle: 'fiscal_trips.nl.split.button'
        } as ConfirmationDialogParams;

        this.unsubscribe.removeSubscription(this.splitTripSubscriber);
        this.confirmDialogService
            .confirm(req)
            .subscribe((result) => {
                if (result === 'yes') {
                    this.splitTripSubscriber = this.service
                        .splitCompinedFiscalTrip(item)
                        .subscribe((res: FiscalTripNLSplitCodeOptions) => {
                            if (res === null) {
                                this.snackBar.open('fiscal_trips.nl.split.success', 'form.actions.close');
                                this.get(true);
                                this.refreshSummaryWidget();
                            } else {
                                this.snackBar.open('form.errors.not_found', 'form.actions.close');
                            }
                        },
                        (err) => {
                            if (err.status === 404) {
                                this.snackBar.open('form.errors.not_found', 'form.actions.close');
                            } else {
                                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                            }
                        });
                }
                return false;
            });
    }


    // =========================
    // REPLACEMENT VEHICLES
    // ========================

    // Insert replacement vehicle trip after last trip
    private insertReplacementVehicleTrip(trip?) {
        this.insertReplacementVehicleTripService
            .open(trip)
            .subscribe((result: boolean) => {
                if (result === true) {
                    this.get(true);
                    this.refreshSummaryWidget();
                }
            });
    }

    // Remove a replacement fiscal trip
    private removeReplacementFiscalTrip(item: FiscalTrip) {
        const req = {
            title: 'fiscal_trips.nl.remove.title',
            message: 'form.warnings.are_you_sure',
            submitButtonTitle: 'fiscal_trips.nl.remove.button'
        } as ConfirmationDialogParams;

        this.unsubscribe.removeSubscription(this.removeTripSubscriber);
        this.confirmDialogService
            .confirm(req)
            .subscribe((result) => {
                if (result === 'yes') {
                    this.removeTripSubscriber = this.service
                        .deleteReplacementVehicleFiscalTrip(item)
                        .subscribe((res: number) => {
                            if (res === 200) {
                                this.snackBar.open('fiscal_trips.nl.remove.success', 'form.actions.close');
                                this.get(true);
                                this.refreshSummaryWidget();
                            } else {
                                this.snackBar.open('form.errors.not_found', 'form.actions.close');
                            }
                        },
                        (err) => {
                            if (err.status === 404) {
                                this.snackBar.open('form.errors.not_found', 'form.actions.close');
                            } else {
                                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                            }
                        });
                }
                return false;
            });
    }


    // =========================
    // HELPERS
    // =========================

    private refreshSummaryWidget(): void {
        const req = Object.assign({}, this.driverSummaryRequest);
        this.driverSummaryRequest = null;
        this.driverSummaryRequest = req;
    }

    private fiscalPageOptions(): MoreOptionsMenuOptions[] {
        return [
            {
                term: 'fiscal_trips.nl.top_menu_options.header.options',
                options: [
                    // {
                    //     id: 'details',
                    //     term: 'fiscal_trips.nl.top_menu_options.driver_details',
                    //     type: MoreOptionsMenuTypes.none,
                    //     enabled: true,
                    // },
                    {
                        id: 'insertCorrectionTrip',
                        term: 'fiscal_trips.nl.top_menu_options.correction_trip',
                        type: MoreOptionsMenuTypes.none,
                        enabled: true,
                    },
                    {
                        id: 'insertReplacementVehicleTrip',
                        term: 'fiscal_trips.nl.top_menu_options.replacement_vehicle_trip',
                        type: MoreOptionsMenuTypes.none,
                        enabled: true,
                    },
                    {
                        id: 'auditLog',
                        term: 'fiscal_trips.nl.top_menu_options.audit_log',
                        type: MoreOptionsMenuTypes.none,
                        enabled: true,
                    },
                ]
            },
            // {
            //     term: 'fiscal_trips.nl.top_menu_options.header.export',
            //     options: [
            //         {
            //             id: 'customize',
            //             term: 'fiscal_trips.nl.top_menu_options.export_trip_data',
            //             type: MoreOptionsMenuTypes.none,
            //             enabled: true,
            //         },
            //         {
            //             id: 'restore',
            //             term: 'fiscal_trips.nl.top_menu_options.export_tax_file',
            //             type: MoreOptionsMenuTypes.none,
            //             enabled: true,
            //         }
            //     ]
            // }
        ];
    }


    // Data table settings definition
    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                header: ' ',
                primaryKey: ' ',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null),
                format: TableCellFormatOptions.custom,
                formatFunction: (record: FiscalTrip) => {
                    return this.getNotationIcons(record);
                }
            },
            {
                header: 'fiscal_trips.nl.vehicle_label',
                primaryKey: 'vehicleLabel',
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null),
            },
            {
                header: 'fiscal_trips.nl.start_timestamp',
                primaryKey: 'startTimestamp',
                format: TableCellFormatOptions.dateTime,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null),
            },
            {
                header: 'fiscal_trips.nl.start_address',
                primaryKey: 'startAddress',
            },
            {
                header: 'fiscal_trips.nl.start_odometer',
                primaryKey: 'startCummTotalDistance',
                format: TableCellFormatOptions.distance,
            },
            {
                header: 'fiscal_trips.nl.stop_timestamp',
                primaryKey: 'stopTimestamp',
                format: TableCellFormatOptions.dateTime,
                sorting: new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null),
            },
            {
                header: 'fiscal_trips.nl.stop_address',
                primaryKey: 'stopAddress',
            },
            {
                header: 'fiscal_trips.nl.stop_odometer',
                primaryKey: 'stopCummTotalDistance',
                format: TableCellFormatOptions.distance
            },
            {
                header: 'fiscal_trips.nl.duration',
                primaryKey: 'duration',
                format: TableCellFormatOptions.time,
            },
            {
                header: 'fiscal_trips.nl.business_distance',
                primaryKey: 'businessDistance',
                format: TableCellFormatOptions.distance,
            },
            {
                header: 'fiscal_trips.nl.private_distance',
                primaryKey: 'privateDistance',
                format: TableCellFormatOptions.distance,
            },
            {
                header: 'fiscal_trips.nl.deviation_distance',
                primaryKey: 'deviationDistance',
                format: TableCellFormatOptions.distance,
            },
            {
                header: 'fiscal_trips.nl.comment',
                primaryKey: 'comment',
            },
            {
                primaryKey: 'startTimestamp',
                header: ' ',
                actions: [TableActionOptions.check, TableActionOptions.moreOptions],
                actionsVisibilityFunction: (action: TableActionOptions, record: FiscalTrip): boolean => {
                    // no actions allowed for a correction trip
                    if (record.isCorrection) {
                        return false;
                    }

                    // a replacement vehicle trip cannot be editted. any other option should not show
                    if (action === TableActionOptions.check && record.isReplacementVehicle) {
                        return false;
                    }

                    return true;
                }
            }
        ];
    }


    private getOptions(trip: FiscalTrip): KeyName[] {
        return [
            {
                id: FiscalTripsNLEditingOptions.edit,
                term: 'fiscal_trips.nl.record_options.edit_trip',
                name: 'edit',
                // only raw trips can be edited
                enabled: !trip.isCombined && !trip.isCorrection,
            },
            {
                id: FiscalTripsNLEditingOptions.validate,
                term: 'fiscal_trips.nl.record_options.validate',
                name: 'validate',
                enabled: !trip.isValidated,
            },
            {
                id: FiscalTripsNLEditingOptions.split,
                term: 'fiscal_trips.nl.record_options.split',
                name: 'split',
                // only combined trips can be splitted
                enabled: trip.isCombined,
            },
            {
                id: FiscalTripsNLEditingOptions.remove,
                term: 'fiscal_trips.nl.record_options.remove',
                name: 'remove',
                // only replacement vehicle trips can be deleted
                enabled: trip.isReplacementVehicle,
            }
        ];
    }

    private getNotationIcons(record: FiscalTrip): string {
        let icons = '';
        if (!record.isValidated) {
            icons += `<span class="fiscal-trips-notation non-validated"></span>`;
        }

        if (record.isCorrection) {
            icons += `<span class="fiscal-trips-notation correction"></span>`;
        }

        if (record.isReplacementVehicle) {
            icons += `<span class="fiscal-trips-notation replacement-vehicle"></span>`;
        }

        if (record.isCombined) {
            icons += `<span class="fiscal-trips-notation combined"></span>`;
        }

        return icons;
    }
}
