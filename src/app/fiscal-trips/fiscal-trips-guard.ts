// FRAMEWORK
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

// SERVICES
import { UserOptionsService } from '../shared/user-options/user-options.service';
import { Config } from '../config/config';

// TYPES

@Injectable()
export class FiscalTripsGuard implements CanActivate {

    constructor(
        private router: Router,
        private userOptions: UserOptionsService,
        private configService: Config) {
            // foo
        }

    public canActivate(route: ActivatedRouteSnapshot, innerState: RouterStateSnapshot) {
        const u = this.userOptions.getUser();
        // test only
        // this.router.navigate([innerState.url, 'nl'], { skipLocationChange: !this.configService.get('router') });
        //     return false;
        // end - test only
        if (!u.isDriver) {
            this.router.navigate([innerState.url, 'unauthorized'], { skipLocationChange: !this.configService.get('router') });
            return false;
        }
        switch (u.region) {
            case 'NL':
                this.router.navigate([innerState.url, 'nl'], { skipLocationChange: !this.configService.get('router') });
                return false;
            case 'DE':
                this.router.navigate([innerState.url, 'de'], { skipLocationChange: !this.configService.get('router') });
                return false;
        }

        return false;
    }
}
