﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../shared/shared.module';
import { FiscalTripsRoutingModule } from './fiscal-trips-routing.module';

import { FiscalTripsGuard } from './fiscal-trips-guard';
import { FiscalTripsUnauthorizedComponent } from './fiscal-trips-unauthorized/fiscal-trips-unauthorized.component';

@NgModule({
    imports: [
        SharedModule,
        FiscalTripsRoutingModule
    ],
    declarations: [
        FiscalTripsUnauthorizedComponent
    ],
    providers: [
        FiscalTripsGuard
    ]
})
export class FiscalTripsModule { }
