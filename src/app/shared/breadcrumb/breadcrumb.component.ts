﻿// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

// RXJS
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

// SERVICES
import { Config } from '../../config/config';

// TYPES
import { BreadCrumb } from './breadcrumb-types';
import { DisabledRoutes } from './disabled-roots';

@Component({
    selector: 'breadcrumb',
    templateUrl: './breadcrumb.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./breadcrumb.less']
})

export class BreadcrumbComponent {

    public skipLocChange = !this.configService.get('router');
    public hide?: boolean;
    public breadcrumbs$ = this.router.events
        .pipe(
            filter((event) => event instanceof NavigationEnd),
            distinctUntilChanged(),
            map((event) => this.buildBreadCrumb(this.activatedRoute.root))
        );
    // Build your breadcrumb starting with the root route of your current activated route

    constructor(private activatedRoute: ActivatedRoute,
                private router: Router,
                private configService: Config) {
    }


    public buildBreadCrumb(route: ActivatedRoute, url: string = '', breadcrumbs: BreadCrumb[] = []): BreadCrumb[] {
        let path: string = '';
        const label = route.routeConfig && route.routeConfig.data ? route.routeConfig.data['breadcrumb'] : null;

        if (route.routeConfig && route.routeConfig.path) {
            this.hide = !label ? true : false;

            route.url.subscribe((value) => {
                const pathArray = value.map((val) => val.path);
                path = pathArray.join('/');
            });
        }

        const nextUrl = `${url}${path}/`;
        let breadcrumb: BreadCrumb;
        const newBreadcrumbs: BreadCrumb[] = [...breadcrumbs];

        if (label) {
            this.hide = DisabledRoutes.find((disabled) => disabled === path) !== undefined
                ? true
                : false;

            breadcrumb = {
                label,
                url: nextUrl
            };

            newBreadcrumbs.push(breadcrumb);
        }

        if (route.firstChild) {
            // If we are not on our current path yet,
            // there will be more children to look after, to build our breadcumb
            return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
        }

        return newBreadcrumbs;
    }
}
