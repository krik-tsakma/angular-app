﻿
// Inline with 'locale' set in terms
export interface LocalOptions {
    moment_locale?: string;
    short_date?: string;
    decimal_separator?: string;
    thousands_separator?: string;
}

// Inline with 'measurement' set in terms
export interface Metrics {
    distance?: string;
    speed?: string;
    energyMJ?: string;
    energyGJ?: string;
    weight?: string;
    litres?: string;
    kWh?: string;
}
