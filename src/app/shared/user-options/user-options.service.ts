// FRAMEWORK
import { Injectable } from '@angular/core';

// SERVICES
import { StoreManagerService } from '../store-manager/store-manager.service';
import { CustomTranslateService } from '../custom-translate.service';
import { TranslateService } from '@ngx-translate/core';

// TYPES
import { LocalOptions, Metrics } from './user-options-types';
import { StoreItems } from '../store-manager/store-items';
import { User, UserLanguageOptions } from '../../auth/user';
import { EnergyUnitOptions } from '../../common/energy-types/energy-types';
import { CompanyTheme } from '../../admin/company/company.types';

// MOMENT
import * as moment from 'moment';

@Injectable()
export class UserOptionsService {
    public localOptions: LocalOptions = {} as LocalOptions;
    public metrics: Metrics = {} as Metrics;

    constructor(
        private translateService: TranslateService,
        private customTraslateService: CustomTranslateService,
        private storeManager: StoreManagerService) {

        // reset user's data (for refresh page)
        const user = this.getUser();
        if (user) {
            // save to storage culture again
            this.setCulture(user.culture);
            this.setMeasurementSystem(user.measurementSystem);
        } else {
            this.setMeasurementSystem('metric');
        }
    }

    public async setCulture(culture: string) {
        // set user's lang and datetime options across system
        const savedLangSelection = this.storeManager.getOption(StoreItems.culture) as string;
        const langSelectionByUser = this.storeManager.getOption(StoreItems.cultureSetByUser) as boolean;
        if (!this.getUser()) {
            if (savedLangSelection && langSelectionByUser) {
                culture = savedLangSelection;
            } else {
                const exists = UserLanguageOptions.find((x) => x.code === culture);
                if (exists) {
                    this.storeManager.saveOption(StoreItems.culture, exists.code);
                } else {
                    culture = UserLanguageOptions.find((x) => x.default === true).code;
                    this.storeManager.saveOption(StoreItems.culture, culture);
                }
            }
        } else {
            if (savedLangSelection && langSelectionByUser) {
                culture = savedLangSelection;
            } else {
                culture = this.getUser().culture;
                this.storeManager.saveOption(StoreItems.culture, culture);
            }
        }

        this.translateService.use(culture);
        moment.locale(culture);
    }

    public async setMeasurementSystem(measurementSystem: string) {
        // set user's measurement options
        const result = await this.customTraslateService.get(['locale.moment_locale',
            'locale.short_date',
            'locale.decimal_separator',
            'locale.thousands_separator',
            'measurement.' + measurementSystem + '.distance',
            'measurement.' + measurementSystem + '.speed',
            'measurement.' + measurementSystem + '.energyGJ',
            'measurement.' + measurementSystem + '.energyMJ',
            'measurement.' + measurementSystem + '.weight',
            'measurement.' + measurementSystem + '.litres',
            'measurement.' + measurementSystem + '.kWh'
        ]).toPromise();

        this.localOptions = {
            moment_locale: result['locale.moment_locale'],
            short_date: result['locale.short_date'],
            decimal_separator: result['locale.decimal_separator'],
            thousands_separator: result['locale.thousands_separator']
        } as LocalOptions;


        this.metrics = {
            distance: result['measurement.' + measurementSystem + '.distance'],
            speed: result['measurement.' + measurementSystem + '.speed'],
            energyMJ: result['measurement.' + measurementSystem + '.energyMJ'],
            energyGJ: result['measurement.' + measurementSystem + '.energyGJ'],
            weight: result['measurement.' + measurementSystem + '.weight'],
            litres: result['measurement.' + measurementSystem + '.litres'],
            kWh: result['measurement.' + measurementSystem + '.kWh']
        } as Metrics;
    }

    public getCalculatedUnit(type: string, normalizedNum?: number,  energyUnit?: EnergyUnitOptions): string {
        let lbl = this.metrics[type];
        if (lbl) {
            if (energyUnit !== null && energyUnit !== undefined) {
                switch (energyUnit) {
                    case EnergyUnitOptions.KilowattHour:
                        lbl = this.metrics.kWh;
                        break;
                    default:
                        lbl = this.metrics.litres;
                        break;
                }
            }
        }

        lbl = !lbl ? type : lbl;
        if (normalizedNum !== undefined && normalizedNum !== null) {
            lbl += '/' + normalizedNum + this.metrics.distance;
        }
        return lbl;
    }

    public getUser(): User {
         return this.storeManager.getOption(StoreItems.appUser);
    }

    public getCulture(): string {
        return this.storeManager.getOption(StoreItems.culture);
    }

    public setTheme(theme?: CompanyTheme): void {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < document.body.classList.length; i++) {
            document.body.classList.remove(document.body.classList[i]);
        }

        // set the company's theme
        if (theme) {
            const appUser = this.getUser();
            appUser.appearance.theme = theme;
            this.storeManager.saveOption(StoreItems.appUser, appUser);
            document.body.classList.add(`theme-${theme.primaryColor}-${theme.secondaryColor}`);
        } else {
            const userTheme = this.getUser().appearance.theme;
            if (userTheme) {
                document.body.classList.add(`theme-${userTheme.primaryColor}-${userTheme.secondaryColor}`);
            } else {
                document.body.classList.add('theme-blue-green');
            }
        }
    }
}
