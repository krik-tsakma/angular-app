// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Subject } from 'rxjs';

// TYPES
import { Resizer } from './maximize-types';

@Injectable()
export class MaximizeService {
    public resizer: Subject<Resizer> = new Subject<Resizer>();
    constructor() {
        // foo
    }

    /** set the height of the dom element */
     public maximize(maximize: boolean, referenceID: string) {

        let height = window.innerHeight;
        const elm = document.getElementById(referenceID);
        if (elm) {
            elm.scrollTop = 0;
            const headerElm = document.getElementsByTagName('page-container');
            const dataHeight = this.getDistanceFromTop(headerElm);
            height = height - dataHeight;
            
            if (maximize) { 
                this.addClass(elm, 'maximized');
            } else {
                this.removeClass(elm, 'maximized');
            }
            
            const res = new Resizer();
            res.height = height;
            res.maximized = maximize;
            res.htmlID = referenceID;
            this.resizer.next(res);
        }
    }

    private hasClass(el, className) {
        if (el.classList) {
            return el.classList.contains(className);
        } else {
            return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
        }
    }

    private addClass(el, className) {
        if (el.classList) {
            el.classList.add(className);
        } else if (!this.hasClass(el, className)) {
            el.className += ' ' + className;
        }
    }

    private removeClass(el, className) {
        if (el.classList) {
            el.classList.remove(className);
        } else if (this.hasClass(el, className)) {
            const reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
    }

    private getDistanceFromTop(elem) {
        // Get an element's distance from the top of the page
        let location = 0;
        if (elem.offsetParent) {
            do {
                location += elem.offsetTop;
                elem = elem.offsetParent;
            } while (elem);
        }
        return location >= 0 ? location : 0;
    }

}
