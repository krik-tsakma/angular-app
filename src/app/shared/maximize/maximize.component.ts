// FRAMEWORK
import { 
    Input, ViewEncapsulation, Output,
    Component, EventEmitter
} from '@angular/core';

// SERVICES
import { MaximizeService } from './maximize.service';

// TYPES
import { Resizer } from './maximize-types';


@Component({
    selector: 'maximize-button',
    template: `
        <button aria-label="maximize" class="icon-button" (click)="maximize()">
            <mat-icon class="primary-color" *ngIf="!maximized">fullscreen</mat-icon>
            <mat-icon class="primary-color" *ngIf="maximized">fullscreen_exit</mat-icon>
        </button>
    `,
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['maximize.less'],
})


export class MaximizeComponent {
    @Input() public referenceID: string;
    // Callback function on group change
    @Output() public notifyOnResize: EventEmitter<Resizer> =
        new EventEmitter<Resizer>();
    public maximized: boolean;

    constructor(private maximizer: MaximizeService) {
        this.maximized = false;
    }

    /** set the height of the dom element */
     public maximize() {
        this.maximized = !this.maximized;
        this.maximizer.maximize(this.maximized, this.referenceID);
    }
}
