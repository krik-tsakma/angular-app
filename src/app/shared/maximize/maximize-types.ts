export class Resizer {
    public htmlID: string;
    public height: number;
    public maximized: boolean;
}
