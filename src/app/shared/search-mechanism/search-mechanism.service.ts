﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import { SearchType, SearchAssetResults } from './search-mechanism-types';
import { KeyName } from '../../common/key-name';

@Injectable()
export class SearchMechanismService {

    constructor(
        private authHttp: AuthHttp,
        private config: Config) { }

    public search(param: SearchType, term: string, groupID?: number): Observable<KeyName[]> {
        // Return only if term.lengh is more than 1 letter
        if (!term || term.length < 2) {
            return;
        }
        const params = new HttpParams({
            fromObject: {
                SearchTerm: term,
                Type: param.toString(),
                GroupID: groupID ? groupID.toString() : ''
            }
        }); 

        return this.authHttp
            .get(this.config.get('apiUrl') + '/api/Search', { params })
            .pipe(
                map((res) => {
                    const myResults = res as SearchAssetResults;
                    return myResults.results;
                })
            );
    } 

}
