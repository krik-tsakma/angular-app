﻿import { KeyName } from '../../common/key-name';
import { PagerResults } from '../../common/pager';

export enum SearchType {
    Vehicle = 0,
    Driver = 1,
    Location = 2,
    Rule = 3,
    Route = 4,
    User = 5,
    DriverUser = 6,
    TrackingDevice = 7,
    ChargePoint = 8
}


export interface SearchAssetResults extends PagerResults {
    results: KeyName[];
}
