// FRAMEWORK
import {
    Component, OnChanges, Input, Output, ViewChild, ElementRef,
    Renderer, EventEmitter, ViewEncapsulation, SimpleChanges 
} from '@angular/core';

// RXJS
import { Observable, Subject, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

// SERVICES
import { SearchMechanismService } from './search-mechanism.service';

// TYPES
import { SearchType } from './search-mechanism-types';
import { KeyName } from '../../common/key-name';

@Component({
    selector: 'search-mechanism',
    templateUrl: './search-mechanism.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: []
})

export class SearchMechanismComponent implements OnChanges {
    @ViewChild('term', { static: true }) public fileInput: ElementRef;
    @Input('placeholder') public placeholder: string;
    @Input() public chosenOption: SearchType;
    @Input() public groupID?: number;
    @Input() public name: string;
    @Input() public disabled: boolean;
    @Output() public onSelect = new EventEmitter<KeyName>();
    public loading: boolean = false;
    public inputSelected: any;
    public result: any;
    public searchTermStream: Subject<string> = new Subject();
   
    public items: Observable<any[]> = this.searchTermStream
        .pipe(
            debounceTime(500),
            distinctUntilChanged(),
            switchMap((term) => {
                // use this to empty result set in case of change            
                if (term === null) {
                    this.loading = false;
                    return of([]);
                } else {
                    this.loading = false;
                    return this.service.search(this.chosenOption, term, this.groupID);
                }
            })
        );

    constructor(private service: SearchMechanismService,
                private renderer: Renderer) {
        // foo
    }

    public ngOnChanges(changes: SimpleChanges) {
        const changeTerm = changes['term'];
        if (changeTerm) {            
            this.inputSelected = changeTerm.currentValue;
        }
        const changeName = changes['name'];
        if (changeName) {            
            this.inputSelected = changeName.currentValue;
        }
    }

    // Method that pass
    public search(term: any) {   
        this.loading = true;                 
        if (term && term.length > 1) {
            this.searchTermStream.next(term);
        } else {
            this.loading = false;     
        }
    }

    public selected(term?: KeyName) {
        this.onSelect.emit(term);
    }

    public clearInput(e?: Event) {
        // do not bubble event otherwise focus is on the textbox
        if (e) {
            e.stopPropagation();
        }

        // clear selection
        this.inputSelected = null;
        this.selected(null);

        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.fileInput.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }
}
