﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { ConfirmDialogComponent } from './confirm-dialog.component';

// TYPES
import { ConfirmationDialogParams } from './confirm-dialog-types';


@Injectable()
export class ConfirmDialogService {

    constructor(private dialog: MatDialog) { }

    public confirm(params: ConfirmationDialogParams): Observable<string> {
        const dialogRef: MatDialogRef<ConfirmDialogComponent> = this.dialog.open(ConfirmDialogComponent);
        dialogRef.componentInstance.request.title = params.title;
        dialogRef.componentInstance.request.message = params.message;
        dialogRef.componentInstance.request.param = params.param;
        dialogRef.componentInstance.request.submitButtonTitle = params.submitButtonTitle;

        return dialogRef.afterClosed();
    }
}
