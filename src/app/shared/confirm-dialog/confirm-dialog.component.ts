﻿// FRAMEWORK
import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// TYPES
import { ConfirmationDialogParams } from './confirm-dialog-types';

@Component({
    selector: 'confirm-dialog',
    templateUrl: './confirm-dialog.html',
})

export class ConfirmDialogComponent {
    public request: ConfirmationDialogParams = {} as ConfirmationDialogParams;

    constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>) { }

}
