﻿export interface ConfirmationDialogParams {
    title: string;
    message: string;
    submitButtonTitle: string;
    param?: object;
}
