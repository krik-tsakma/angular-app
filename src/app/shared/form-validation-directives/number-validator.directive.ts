
import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { ValidationRegexPatterns } from './validation-regex-patterns';

@Directive({
    selector: '[validateNumber][formControlName],[validateNumber][formControl],[validateNumber][ngModel]',
    providers: [
        { 
            provide: NG_VALIDATORS, 
            useExisting: NumberValidatorDirective, 
            multi: true 
        }
    ]
})
export class NumberValidatorDirective implements Validator {
    constructor() {
        // foo
    }

    public validate(c: AbstractControl): { [key: string]: any } {
        // do the check only if field has value
        if (c.value === null || c.value === undefined || c.value === '') {
            return null;
        }

        const pattern = new RegExp(ValidationRegexPatterns.number);
        return pattern.test(c.value) ? null : {
            validateNumber: {
                valid: false
            }
        };
    }
}
