﻿import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, FormControl, Validator } from '@angular/forms';
import { ValidationRegexPatterns } from './validation-regex-patterns';


@Directive({
    selector: '[validateEmail][formControlName],[validateEmail][formControl],[validateEmail][ngModel]',
    providers: [
        { 
            provide: NG_VALIDATORS, 
            useExisting: forwardRef(() => EmailValidatorDirective), 
            multi: true 
        }
    ]
})

export class EmailValidatorDirective implements Validator {
    constructor() {
        // foo
    }

    public validate(c: FormControl) {
        // do the check only if field has value
        if (c.value === null || c.value === undefined || c.value === '') {
            return null;
        }

        const pattern = new RegExp(ValidationRegexPatterns.email);
        return pattern.test(c.value) ? null : {
            validateEmail: {
                valid: false
            }
        };
    }
}
