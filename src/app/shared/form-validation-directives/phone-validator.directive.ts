
import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { ValidationRegexPatterns } from './validation-regex-patterns';

@Directive({
    selector: '[validatePhoneNumber][formControlName],[validatePhoneNumber][formControl],[validatePhoneNumber][ngModel]',
    providers: [
        { 
            provide: NG_VALIDATORS, 
            useExisting: PhoneNumberValidatorDirective, 
            multi: true 
        }
    ]
})
export class PhoneNumberValidatorDirective implements Validator {
    constructor() {
        // foo
    }

    public validate(c: AbstractControl): { [key: string]: any } {
        // do the check only if field has value
        if (c.value === null || c.value === undefined || c.value === '') {
            return null;
        }

        const pattern = new RegExp(ValidationRegexPatterns.phoneNumber);
        return pattern.test(c.value) ? null : {
            validatePhoneNumber: {
                valid: false
            }
        };
    }
}
