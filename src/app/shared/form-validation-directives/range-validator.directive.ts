
import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[validateRange][formControlName],[validateRange][formControl],[validateRange][ngModel]',
    providers: [
        { 
            provide: NG_VALIDATORS, 
            useExisting: RangeValidatorDirective, 
            multi: true 
        }
    ]
})
export class RangeValidatorDirective implements Validator {
    @Input() public min: number;
    @Input() public max: number;

    constructor() {
        // foo
    }

    public validate(c: AbstractControl): { [key: string]: any } {
        // do the check only if field has value
        if (c.value === null || c.value === undefined || c.value === '') {
            return null;
        }

        const isNumber = !isNaN(c.value);
        if (isNumber && (Number(c.value) >= Number(this.min) && c.value <= Number(this.max))) {
            return null;
        }

        return {
            validateRange: {
                valid: false
            }
        };
    }
}
