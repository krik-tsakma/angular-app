
import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { ValidationRegexPatterns } from './validation-regex-patterns';

@Directive({
    selector: '[validateInteger][formControlName],[validateInteger][formControl],[validateInteger][ngModel]',
    providers: [
        { 
            provide: NG_VALIDATORS, 
            useExisting: IntegerValidatorDirective, 
            multi: true 
        }
    ]
})
export class IntegerValidatorDirective implements Validator {
    constructor() {
        // foo
    }

    public validate(c: AbstractControl): { [key: string]: any } {
        // do the check only if field has value
        if (c.value === null || c.value === undefined || c.value === '') {
            return null;
        }

        const pattern = new RegExp(ValidationRegexPatterns.integer);
        return pattern.test(c.value) ? null : {
            validateInteger: {
                valid: false
            }
        };
    }
}
