﻿import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { ValidationRegexPatterns } from './validation-regex-patterns';

@Directive({
    selector: '[validateIP][formControlName],[validateIP][formControl],[validateIP][ngModel]',
    providers: [
        { 
            provide: NG_VALIDATORS, 
            useExisting: ValidateIPValidatorDirective, 
            multi: true 
        }
    ]
})
export class ValidateIPValidatorDirective implements Validator {
    constructor() {
        // foo
    }

    public validate(c: AbstractControl): { [key: string]: any } {
        // do the check only if field has value
        if (c.value === null || c.value === undefined || c.value === '') {
            return null;
        }

        const pattern = new RegExp(ValidationRegexPatterns.ip);
        return pattern.test(c.value) ? null : {
            validateIP: {
                valid: false
            }
        };
    }
}
