import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[validateEqual][formControlName],[validateEqual][formControl],[validateEqual][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => EqualValidatorDirective), multi: true }
    ]
})
export class EqualValidatorDirective implements Validator {
    constructor( @Attribute('validateEqual') public validateEqualValue: string) {

    }

    public validate(c: AbstractControl): { [key: string]: any } {
    
       // self value (e.g. retype password)
        const v = c.value;

        // other control value (e.g. password)
        const e = c.root.get(this.validateEqualValue);
        if (!e) {
            return null;
        }
        // subscribe to value changes of the reference element
        e.valueChanges.subscribe((newControlValue) => {
            return this.areEqual(v, newControlValue);
        });

        return this.areEqual(v,  e.value);
    }

    private areEqual(value: any, confirmValue: any) {
        // value not equal
        if (value !== confirmValue) {
            return {
                validateEqual: {
                    valid: false
                }
            };
        }

        return null;
    }
}
