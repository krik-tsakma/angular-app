
/** These regex patterns can be used with as value for Validators.pattern(regex) in form controls. */
export class ValidationRegexPatterns {
    public static readonly number = /^0$|^-?([0-9]*[.])?[0-9]+$/;
    public static readonly integer = /^0$|^-?[1-9]\d*(\.\d+)?$/;
    public static readonly email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // a phone needs to start with the + sign or 00 and must be followed by at least 4 digits.
    public static readonly phoneNumber = /^(00|\+)([\-0-9]{4,20})+$/;
    public static readonly ip = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;     
}
