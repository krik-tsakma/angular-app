import { AfterViewInit, Directive, ElementRef, Renderer } from '@angular/core';

@Directive({
    selector: '[pageHeader]'
})

export class PageHeaderDirective implements AfterViewInit {
   
    constructor(private elementRef: ElementRef, private renderer: Renderer) {
       // foo
    }

    public ngAfterViewInit() {
        const cnt = this.elementRef.nativeElement;
        this.renderer
            .setElementStyle(
                cnt,
                'display',
                'none'
            );
        const that = this;
        setTimeout(() => {
            const pl = document.getElementById('title-placeholder');
            if (pl) {
                pl.innerHTML = '';
                that.renderer.setElementStyle(
                    cnt,
                    'display',
                    'block'
                );
                pl.appendChild(cnt);
            }
        }, 5);
     }
}
