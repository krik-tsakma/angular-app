import { ResizeService } from './../../resize.service';
import { OnInit, Directive, ElementRef } from '@angular/core';

@Directive({
    selector: '[pageFooter]'
})

export class PageFooterDirective implements OnInit {
   
    constructor(private elementRef: ElementRef, 
                private resizeService: ResizeService) {
       // foo
    }

    public ngOnInit() {
        const cnt = this.elementRef.nativeElement;
        const pl = document.getElementsByClassName('fixed-footer')[0];
        if (pl) {
            pl.innerHTML = '';
            pl.appendChild(cnt);

            setTimeout(() => {
                this.resizeService.trigger();
            }, 1000);
        }
     }
}
