// FRAMEWORK
import {
    Component, ViewEncapsulation, Input,  ViewChild,
    ElementRef, Output, EventEmitter, AfterViewInit
} from '@angular/core';

// SERVICES
import { ResizeService } from './../resize.service';

// TYPES
import { 
    MoreOptionsMenuOptions, 
    MoreOptionsMenuOption 
} from './../more-options-menu/more-options-menu.types';

@Component({
    selector: 'page-container',
    templateUrl: 'page-container.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['page-container.less']
})

export class PageContainerComponent implements AfterViewInit {
    @ViewChild('pageContent', { static: false }) public pageContents: ElementRef;
    @ViewChild('pageFooter', { static: false }) public pageFooter: ElementRef;

    @Input() public loading: boolean; 
    @Input() public title: string;
    @Input() public backButton: boolean;
    @Input() public mainClass: boolean;
    @Input() public addButton: boolean;
    @Input() public options: MoreOptionsMenuOptions[];

    @Output() public notifyOnBack: EventEmitter<any> = new EventEmitter();
    @Output() public notifyOnAdd: EventEmitter<any> = new EventEmitter();
    @Output() public notifyOnOptionSelection: EventEmitter<MoreOptionsMenuOption> = new EventEmitter();

    constructor(private resizeService: ResizeService) { 

        this.resizeService.resizeFinished.subscribe(() => {
            setTimeout(() => {
                this.setMaxHeight();
            }, 200);
        });
    }


    public ngAfterViewInit(): void {
        this.setMaxHeight();
    }

    public onAdd() {
        this.notifyOnAdd.emit();             
    }

    public onOptionSelection(option: MoreOptionsMenuOption) {
        this.notifyOnOptionSelection.emit(option);             
    }
    
    // Back to page
    public onBack(): void {
        this.notifyOnBack.emit();
    }

    private setMaxHeight() {
        const elm = this.pageContents.nativeElement as HTMLElement; 
        const footerElm = this.pageFooter.nativeElement as HTMLElement;
        let footerHeight = 0;
        if (footerElm.childNodes.length > 0) {
            footerHeight = (footerElm.childNodes[0] as HTMLElement).offsetHeight;
        }

        const distancePageContainerFromTop = elm.getBoundingClientRect();
        const height = window.innerHeight - distancePageContainerFromTop.top - footerHeight;
        // console.log(window.innerHeight,  distancePageContainerFromTop, footerHeight, footerElm.offsetHeight);
        elm.style.height = elm.style.maxHeight = height + 'px';
    }
}
