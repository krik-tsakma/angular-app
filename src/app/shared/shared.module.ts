// FRAMEWORK
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

// MATERIAL
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { OverlayModule } from '@angular/cdk/overlay';

// -----------------
// CUSTOMISATIONS
// -----------------
// MODULES
import { CustomPipesModule } from './pipes/custom-pipes.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { PasswordRulesModule } from './password-rules/password-rules.module';
import { RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaFormsModule, RecaptchaLoaderService } from 'ng-recaptcha';
import { TabsModule } from './tabs/tabs.module';

// DIRECTIVES
import { PageHeaderDirective } from './page-container/page-header-directive/page-header.directive';
import { PageFooterDirective } from './page-container/page-footer-directive/page-footer.directive';
import { EmailValidatorDirective } from './form-validation-directives/email-validator.directive';
import { EqualValidatorDirective } from './form-validation-directives/equal-validator.directive';
import { PhoneNumberValidatorDirective } from './form-validation-directives/phone-validator.directive';
import { RangeValidatorDirective } from './form-validation-directives/range-validator.directive';
import { NumberValidatorDirective } from './form-validation-directives/number-validator.directive';
import { ValidateIPValidatorDirective } from './form-validation-directives/ip-validator.directive';
import { IntegerValidatorDirective } from './form-validation-directives/integer-validator.directive';

// COMPONENTS
import { EntityTypesComponent } from './entity-types/entity-types.component';
import { MaximizeComponent } from './maximize/maximize.component';
import { AmChartComponent } from '../amcharts/amchart.component';
import { SearchMechanismComponent } from './search-mechanism/search-mechanism.component';
import { CustomSelectorComponent } from './custom-selector/custom-selector.component';
import { AnnouncementsBottomSheetComponent } from './announcements-bottom-sheet/announcements-bottom-sheet.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { TableLayoutComponent } from './data-table/data-table.component';
import { MoreOptionsMenuComponent } from './more-options-menu/more-options-menu.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { FiltersComponent } from './filters/filters.component';
import { PageContainerComponent } from './page-container/page-container.component';
import { MoreOptionsEditingOptionsDialogComponent } from './data-table/more-options-editing-options-dialog/more-options-edit-options-dialog.component';

// SERVICES
import { AnnouncementsBottomSheetService } from './announcements-bottom-sheet/announcements-bottom-sheet.service';
import { AnnouncementsService } from '../announcements/announcements.service';
import { AssetGroupsService } from '../admin/asset-groups/asset-groups.service';
import { ConfirmDialogService } from './confirm-dialog/confirm-dialog.service';
import { CustomTranslateService } from './custom-translate.service';
import { HeightCheckService } from './custom-selector/height-check.service';
import { UserOptionsService } from './user-options/user-options.service';
import { MaximizeService } from './maximize/maximize.service';
import { ResizeContainerComponent } from './resize-container/resize-container.component';
import { ResizeService } from './resize.service';
import { SnackBarService } from './snackbar.service';
import { SearchMechanismService } from './search-mechanism/search-mechanism.service';
import { UnsubscribeService } from './unsubscribe.service';


@NgModule({
    imports: [
        CommonModule,
        CustomPipesModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        RouterModule,
        // MATERIAL
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatMomentDateModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatToolbarModule,
        MatTooltipModule,
        OverlayModule,
        InfiniteScrollModule,
        PasswordRulesModule,
        RecaptchaModule,
        RecaptchaFormsModule,
        TabsModule
    ],
    declarations: [
        // components
        AmChartComponent,
        AnnouncementsBottomSheetComponent,
        ConfirmDialogComponent,
        EntityTypesComponent,
        MaximizeComponent,
        MoreOptionsEditingOptionsDialogComponent,
        SearchMechanismComponent,
        CustomSelectorComponent,
        TableLayoutComponent,
        MoreOptionsMenuComponent,
        BreadcrumbComponent,
        ResizeContainerComponent,
        PageContainerComponent,
        FiltersComponent,

        // directives
        PageHeaderDirective,
        PageFooterDirective,
        EmailValidatorDirective,
        EqualValidatorDirective,
        IntegerValidatorDirective,
        NumberValidatorDirective,
        PhoneNumberValidatorDirective,
        RangeValidatorDirective,
        ValidateIPValidatorDirective
    ],
    //  reduce the repetition by having SharedModule re-export CommonModule and FormsModule
    //  so that importers of SharedModule get CommonModule and FormsModule for free.
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        RouterModule,
        RecaptchaModule,
        RecaptchaFormsModule,
        // custom
        TabsModule,
        CustomPipesModule,
        PasswordRulesModule,
        AmChartComponent,
        ConfirmDialogComponent,
        EntityTypesComponent,
        MaximizeComponent,
        MoreOptionsEditingOptionsDialogComponent,
        SearchMechanismComponent,
        CustomSelectorComponent,
        TableLayoutComponent,
        MoreOptionsMenuComponent,
        BreadcrumbComponent,
        ResizeContainerComponent,
        PageContainerComponent,
        FiltersComponent,

        // directives
        PageHeaderDirective,
        PageFooterDirective,
        EmailValidatorDirective,
        EqualValidatorDirective,
        NumberValidatorDirective,
        IntegerValidatorDirective,
        PhoneNumberValidatorDirective,
        RangeValidatorDirective,
        ValidateIPValidatorDirective,

        // MATERIAL
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatMomentDateModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatToolbarModule,
        MatTooltipModule,
        OverlayModule,
        InfiniteScrollModule
    ],
    entryComponents: [
        AnnouncementsBottomSheetComponent,
        BreadcrumbComponent,
        ConfirmDialogComponent,
        SearchMechanismComponent,
        CustomSelectorComponent,
        FiltersComponent,
        MoreOptionsEditingOptionsDialogComponent
    ],
    providers: [
        AnnouncementsService,
        AnnouncementsBottomSheetService,
        AssetGroupsService,
        ConfirmDialogService,
        CustomTranslateService,
        HeightCheckService,
        MaximizeService,
        ResizeService,
        SearchMechanismService,
		SnackBarService,
        UserOptionsService,
        UnsubscribeService,
        RecaptchaLoaderService,
        {
            provide: RECAPTCHA_SETTINGS,
            useValue: {
              siteKey: '6LfzlXQUAAAAAKSbjsp9M5c1jVIxviRZPw8DxgPg',
            }
        }
    ]
})

export class SharedModule { }
