import { Component, Input, TemplateRef } from '@angular/core';

@Component({
    selector: 'tab',
    template: `
      <div class="tab-contents" [hidden]="!active">
        <ng-content></ng-content>
      </div>
    `
})

export class TabComponent {
    @Input('headerTemplate') public headerTemplate: TemplateRef<any>;
    @Input('tabTitle') public title: string;
    @Input('disabled') public disabled: boolean;
    @Input() public active: boolean = false;
}
