// FRAMEWORK
import { 
    Component, 
    QueryList,
    AfterContentInit,
    ViewEncapsulation,
    ContentChildren,
    Output,
    EventEmitter,
    Input,
    OnChanges,
    SimpleChanges
} from '@angular/core';

// COMPONENTS
import { TabComponent } from './tab.component';

// TYPES

@Component({
    selector: 'tabs',
    templateUrl: 'tabs.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['tabs.less'],
})

export class TabsComponent implements AfterContentInit, OnChanges {
   
    @ContentChildren(TabComponent) public tabs: QueryList<TabComponent>;
    @Input() public selectedTab: number;
    @Output() public onTabSelection: EventEmitter<number> = new EventEmitter<number>();

    // contentChildren are set
    public ngAfterContentInit() {
        // add a small timeout to avoid ExpressionChangedAfterItHasBeenCheckedError error
        setTimeout(() => {
            this.userIndexDefaultSelection();
        }, 1000);
    }

    public ngOnChanges(changes: SimpleChanges): void {
        const tabSelectionChanges = changes['selectedTab'];
        if (tabSelectionChanges && !tabSelectionChanges.firstChange) {
            this.userIndexDefaultSelection();
        }
    }
    
    public selectTab(tab: TabComponent) {
        if (!tab || tab.disabled) {
            return;
        }

        let tabIndex = 0;
        // deactivate all tabs
        this.tabs.toArray().forEach((t, index) => {
            t.active = false;

            if (t === tab) {
                tabIndex = index;
            }
        });
        
        // activate the tab the user has clicked on.
        tab.active = true;

        this.onTabSelection.emit(tabIndex);
    }


    private userIndexDefaultSelection() {
            // if there is no active tab set, activate the first
        if (!this.selectedTab) {
            this.selectTab(this.tabs.first);
        } else {
            this.tabs.toArray().forEach((t, index) => {
                if (index === this.selectedTab) {
                    this.selectTab(t);
                    return;
                }
            });
        }
    }
}

