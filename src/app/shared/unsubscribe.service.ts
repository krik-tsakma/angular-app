﻿// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Subscription } from 'rxjs';

@Injectable()
export class UnsubscribeService {
   
    constructor() {
        // Foo
    }
   
    public removeSubscription(subscriber: Subscription | any | any[]): void {
        if (Array.isArray(subscriber)) {
            subscriber.forEach((item) => {
                if (item) {
                    item.unsubscribe();
                }
            });
        } else {
            if (subscriber) {
                subscriber.unsubscribe();
            }
        }        
    }
}
