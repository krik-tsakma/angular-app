﻿// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material';

// TYPES
import { Announcement, AnnouncementTypes } from './announcements-bottom-sheet-types';

@Component({
    selector: 'announcements-dialog',
    templateUrl: './announcements-bottom-sheet.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./announcements-bottom-sheet.less']
})

export class AnnouncementsBottomSheetComponent {

    public announcements: Announcement[];
    public headerEnable: boolean;
    public announcementTypes = AnnouncementTypes;

    constructor(public bottomSheetRef: MatBottomSheetRef<AnnouncementsBottomSheetComponent>) { }

}
