﻿
export enum AnnouncementTypes {
    information = 1,
    warning = 2
}

export interface Announcement {
    id: number;    
    culture: string;
    type: AnnouncementTypes;
    title: string;
    description: string;
    showOnPage?: string;
    startDate?: string;
    endDate?: string;
}

export interface Announcements {
    Announcement: Announcement[];
}

export interface AnnoucementsObj {
    Announcements: Announcements;
}
