﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { AnnouncementsBottomSheetComponent } from './announcements-bottom-sheet.component';

// SERVICES
import { StoreManagerService } from '../store-manager/store-manager.service';

// TYPES
import { Announcement } from './announcements-bottom-sheet-types';
import { StoreItems } from '../store-manager/store-items';



@Injectable()
export class AnnouncementsBottomSheetService {

    constructor(private bottomSheet: MatBottomSheet,
                private storeManager: StoreManagerService) { }


    // check if which announcement it is already shown
    public storageChecker(announcementsArray: Announcement[]): Announcement[] {
        const alreadyShownAnnouncements: number[] = this.storeManager.getOption(StoreItems.alreadyAnnouns);

        if (alreadyShownAnnouncements && alreadyShownAnnouncements.length > 0) {
            const arrayToReturn = announcementsArray.filter((announcement) => {
                return alreadyShownAnnouncements.find((id) => id === announcement.id) ? false : true;
            });            

            this.saveInStorage(arrayToReturn, alreadyShownAnnouncements);
                      
            return arrayToReturn;
        } else {
            this.saveInStorage(announcementsArray, []);
            return announcementsArray;
        }      
    }


    // Save announcements in session storage
    public saveInStorage(newAnnouncements: Announcement[], sessionItemsArray: number[]) {        
        newAnnouncements.forEach((elem) => {
            sessionItemsArray.push(elem.id);
        });

        this.storeManager.saveOption(StoreItems.alreadyAnnouns, sessionItemsArray);
    }


    // open bottom sheet with the announcements
    public announs(announcementsArray: Announcement[]): Observable<string> {

        let bottomSheetRef: MatBottomSheetRef<AnnouncementsBottomSheetComponent>;

        bottomSheetRef = this.bottomSheet.open(AnnouncementsBottomSheetComponent);       
        bottomSheetRef.instance.announcements = announcementsArray;

        return bottomSheetRef.afterDismissed();
    }
}
