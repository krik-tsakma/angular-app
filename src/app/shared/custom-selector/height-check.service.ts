﻿// FRAMEWORK
import { Injectable } from '@angular/core';

@Injectable()
export class HeightCheckService {
    public static maxHeight = 300;
    public static minHeight = 150;
    public static topPadding = 90;

    constructor() {
        // foo
    }    

    public checkComputed(referenceID: string) {
        const el = document.getElementById(referenceID);
        if (el) {
            const compStyle = window.getComputedStyle(el, null);
            return compStyle.height;
        }      
        return null;
    }

    public checkHeight(referenceID: string) {
        
        let height = window.innerHeight;
        const footerElm = document.getElementsByTagName('fixed-footer')[0] as HTMLElement;
        if (footerElm && footerElm.childNodes.length > 0) {
            height = height - footerElm.offsetHeight;
        }
        const elm = document.getElementById(referenceID);
        const rect = elm.getBoundingClientRect();

        if (elm.clientHeight > HeightCheckService.maxHeight) {            
            elm.style.height = HeightCheckService.maxHeight + 'px';
            elm.style.overflowY = 'scroll';            
        }
              
        if ((height - rect.top) < elm.clientHeight ) {            
            elm.style.bottom = '100%';               
        } else {
            elm.style.top = '100%';
        }  

        if (elm.style.bottom === '100%') {           
            if ((rect.top - HeightCheckService.topPadding) < elm.clientHeight) {
                if ((rect.top - HeightCheckService.topPadding) < HeightCheckService.minHeight) {
                    elm.style.top = '100%';
                    elm.style.bottom = null;
                    elm.style.height = (height - rect.top - 10) + 'px';
                } else {
                    elm.style.height = (rect.top - HeightCheckService.topPadding) + 'px';
                    elm.style.overflowY = 'scroll';
                }
            }           
        }                           
    }

    public cleanStyles(referenceID: string) {
        const elm = document.getElementById(referenceID);
        if (elm) {
            elm.style.bottom = null;
            elm.style.top = null;
            elm.style.height = null;
        }
    }

}
