﻿// FRAMEWORK
import {
    Component, Input, Output,
    EventEmitter, ViewEncapsulation,
    SimpleChanges, OnChanges
} from '@angular/core';

// SERVICES
import { CustomTranslateService } from '../custom-translate.service';
import { HeightCheckService } from './height-check.service';

// TYPES
import { KeyName } from '../../common/key-name';

@Component({
    selector: 'custom-selector',
    templateUrl: './custom-selector.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./custom-selector.less']
})

export class CustomSelectorComponent implements OnChanges {
    @Input() public dataID: string;
    @Input() public loading: boolean;
    @Input() public disabled: boolean = false;
    @Input() public options: KeyName[];
    @Input() public modelData: any;
    @Output() public modelDataChange: EventEmitter<any> = new EventEmitter();
    public active: boolean = false;
    public selectedText: string;


    constructor(
        public heightCheck: HeightCheckService,
        private translate: CustomTranslateService) {

    }

    // triggered when preselected Changed
    public ngOnChanges(changes: SimpleChanges) {
        const selectedChange = changes['modelData'];
        if (selectedChange && selectedChange.currentValue !== undefined && this.options) {
            this.setSelectedText(selectedChange.currentValue);
        }

        const optionsChange = changes['options'];
        if (optionsChange) {
            if (optionsChange.currentValue && optionsChange.currentValue.length > 0) {
                this.options.forEach((o) => {
                    if (!o.term) {
                        return;
                    }
                    this.translate
                        .get(o.term)
                        .subscribe((t) => {
                            o.name = t;
                        });
                });
                this.setSelectedText(optionsChange.currentValue.id);
             }
        }
        const dataIdChanged = changes['dataID'];
        if (dataIdChanged && dataIdChanged.currentValue !== undefined && dataIdChanged.currentValue !== null) {            
            const checkedHeight = this.heightCheck.checkComputed(dataIdChanged.currentValue);
            if (checkedHeight && Number(checkedHeight.substring(0, checkedHeight.length - 2)) > 300) {
                this.heightCheck.cleanStyles(this.dataID);
                this.heightCheck.checkHeight(this.dataID);
            }
        }
    }


    // <li> click function: if option is enabled and different from current
    // innerValue(not clicked again the same option - without reason repeat same call),
    // sends to parent the chosen option(full object)
    public onOptionChange(option: KeyName) {
        // console.log(option);
        if (!option.enabled || option.id === this.modelData) {
            return;
        }

        this.modelData = option.id;
        this.setSelectedText(option.id);
        this.modelDataChange.emit(option.id);
    }


    // show drop drown menu when div is clicked
    public toggleOpen() {
        this.active = !this.active;
        if (this.active) {
            // small timeout, for elm to become (in)visible
            setTimeout(() => {
                this.heightCheck.cleanStyles(this.dataID);
                this.heightCheck.checkHeight(this.dataID);
            }, 3);
        }
    }


    // hide drop down menu when focus out
    public onBlurEvent() {
        this.active = false;
    }

    private setSelectedText(id: any) {
        /* tslint:disable */
        const selectedOption = this.options.find((x) => x.id == this.modelData);
        /* tslint:enable */
        if (selectedOption) {
            this.selectedText = selectedOption.name;
        }
    }
}
