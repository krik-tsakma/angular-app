// FRAMEWORK
import { 
    Component, Input, Output, EventEmitter, OnInit,
    SimpleChanges, QueryList, AfterViewInit,
    OnChanges, ViewEncapsulation, ViewChild, 
    ElementRef, ViewChildren, HostListener
} from '@angular/core';

// PIPES 
import { DateFormatPipe } from '../pipes/dateFormat.pipe';
import { TimeFormatPipe } from '../pipes/timeFormat.pipe';
import { NumberFormatPipe } from '../pipes/numberFormat.pipe';
import { ScoreFormatPipe } from '../pipes/scoreFormat.pipe';

// TYPES
import { 
    TableColumnSetting, TableActionItem, TableActionOptions,
    TableCellFormatOptions, TableColumnSortOrderOptions
} from './data-table.types';
import { Pager } from '../../common/pager';
import {
    MoreOptionsMenuOptions, MoreOptionsMenuOption
} from '../more-options-menu/more-options-menu.types';
import { EnergyUnitOptions } from '../../common/energy-types/energy-types';


@Component({
    selector: 'data-table',
    templateUrl: 'data-table.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['data-table.less'],
    providers: [ 
        NumberFormatPipe, DateFormatPipe, 
        TimeFormatPipe, ScoreFormatPipe 
    ]
})

export class TableLayoutComponent implements OnInit, OnChanges, AfterViewInit { 
    @ViewChild('dataTable', { static: true }) public dataTable: ElementRef;
    @ViewChild('tableHeadElm', { static: true }) public thead: ElementRef;
    @ViewChildren('tableDataRows') public dataRows: QueryList<ElementRef>;
    @Input() public uniqueID?: string;
    @Input() public loading: boolean;
    @Input() public records: any[] = [];
    @Input() public pager: Pager;
    @Input() public settings: TableColumnSetting[];
    @Input() public sorting: string;
    @Input() public headerOptions: MoreOptionsMenuOptions[];
    // TODO : ANOTHER WAY AROUND IT
    @Input() public energyUnit?: EnergyUnitOptions;

    @Output() public onActionClick: EventEmitter<TableActionItem> = new EventEmitter<TableActionItem>();
    @Output() public onSortSelection: EventEmitter<TableColumnSetting> = new EventEmitter<TableColumnSetting>();
    @Output() public onScrollDown: EventEmitter<void> = new EventEmitter<void>();
    @Output() public notifyOnHeaderOptionSelection: EventEmitter<MoreOptionsMenuOption> = new EventEmitter();
    @Output() public settingsChanged: EventEmitter<TableColumnSetting[]> = new EventEmitter<TableColumnSetting[]>();
    @Output() public onClickKeyItem: EventEmitter<{}> = new EventEmitter();
    
    
    public actionOptions: any = TableActionOptions;
    public formatOptions: any = TableCellFormatOptions;
    public sortOrderOptions: any = TableColumnSortOrderOptions;

    constructor() {
      // foo
    }

    // subscribe to window resize event and reset the column widths
    @HostListener('window:resize')
    public resizeTableHeader() {
        if (this.dataRows.first) {
            const rowFirstCells = (this.dataRows.first.nativeElement as HTMLElement).children;
            setTimeout(() => {
                this.setHeaderCellsWidth(rowFirstCells);
            }, 100);
        }
    }

    // ===================
    // LIFECYCLE
    // ===================

    public ngOnInit() {
        // do this for header to show records.length 
        if (this.records === null || this.records === undefined) {
            this.records = [];
        }
    }
    public ngAfterViewInit() {
        // subscribe to changes in the tbody
        this.dataRows.changes.subscribe((t: QueryList<ElementRef>) => {
            if (t.first) {
                const rowFirstCells = (t.first.nativeElement as HTMLElement).children;
                setTimeout(() => {
                    this.destroyGrouping().then(() => {
                        this.groupRows(t).then(() => { 
                            this.setHeaderCellsWidth(rowFirstCells);
                        });
                    });
                    this.setHeaderCellsWidth(rowFirstCells);
                }, 100);
            }
        });
    }

    public ngOnChanges(changes: SimpleChanges) {
        if (!this.settings) {
            // no settings, create column maps with defaults
            this.settings = Object.keys(this.records[0])
                .map((key) => {
                    return {
                        primaryKey: key,
                        header: key.slice(0, 1).toUpperCase() +
                        key.replace(/_/g, ' ').slice(1)
                    };
            });
        }

        // when table settings change. re-set the sort column
        const settingsChange = changes['settings'];
        if (settingsChange && settingsChange.currentValue) {
            this.setInitialSortOptions(this.sorting);
            this.dataTableSanityCheck();
        }

        // when sorting option is set for the first time, set the corresponding table column
        const sortingChanges = changes['sorting'];
        if (sortingChanges && sortingChanges.currentValue) {
            this.setInitialSortOptions(sortingChanges.currentValue);
        }
    }

    /*
    * When a user scrolls down 
    */
    public onScroll(): void {
        if (this.pager != null) {
            this.onScrollDown.emit();
        }
    }

    /*
    * When a user selects a column for sorting
    * @param column: the column selected
    */
    public onSort(column: TableColumnSetting): void {
        this.changeSortOption(column);
        this.onSortSelection.emit(column);
    }

    /*
    * When a user clicks on a table action i.e. edit
    * @param action: the action in question
    * @param record: the data of the current table row
    */
    public onAction(action: TableActionOptions, record: any, e: MouseEvent): void {
        const ai = {
            action: action, 
            record: record,
            e: e
        } as TableActionItem;

        this.onActionClick.emit(ai);
    }

    /*
    * When a user select an option from table's options menu
    */
    public onHeaderOptionSelection(option: MoreOptionsMenuOption) {
        this.notifyOnHeaderOptionSelection.emit(option);             
    }

    /*
    * Adds a set of classes to a cell, upon conditions
    */
    public addClassToCell(map: TableColumnSetting, record: any) {
        let classList = '';
        if (map.visible) {
            classList += 'hide-col';
        }
        if (map.actions) {
            classList += 'no-wrap';
        }
        if (map.customClass) {
            classList += map.customClass(record);
        }
        return classList;
    }

    // ===================
    // PRIVATE
    // ===================

    /** Adjust the width of column cells to match the width of table cells */
    private setHeaderCellsWidth(cells: HTMLCollection) {
        // Get the tbody columns width array
        const columnWidths = [];
        for (let i = 0; i < cells.length; i++) {
            const td = cells.item(i) as HTMLElement;
            columnWidths.push(td.clientWidth);
        }

        // Set the width of thead columns
        const headerRowCells = (this.thead.nativeElement as HTMLElement).children;
        // select all columns that are not grouped (for proper indexing and matching with visible cells)
        const ungroupedHeaderCells = []; 
        for (let j = 0; j < headerRowCells.length; j++) {
            const th = headerRowCells.item(j) as HTMLElement;
            if (!th.classList.contains('hide-col')) {
                ungroupedHeaderCells.push(th);
            }
        }
        for (let j = 0; j < ungroupedHeaderCells.length; j++) {
            const th = ungroupedHeaderCells[j] as HTMLElement;
            th.style.maxWidth = th.style.width = columnWidths[j] + 'px';
        }
    }


    /** Set the initial sort option from the string representation.
     * Usefull on initialization purposes where sorting is saved to storage as a string
     */
    private setInitialSortOptions(sorting: string) {
        if (!sorting || !this.settings || this.settings.length === 0) {
            return;
        }
        const sort = sorting.split(' ');

        const column = sort[0];
        const order = TableColumnSortOrderOptions[sort[1]];
        const sortColumn = this.settings
                            .find((x) => x.sorting && (
                                x.sorting.name === column || 
                                x.primaryKey === column)
                            );

        if (sortColumn) {
            sortColumn.sorting.order = order ===  TableColumnSortOrderOptions.asc ? TableColumnSortOrderOptions.desc : TableColumnSortOrderOptions.asc;
            this.changeSortOption(sortColumn);
        } 
    }

    private changeSortOption(column: TableColumnSetting): void {
        if (!column || !column.sorting) {
            return;
        }
        // do this way to endorce change event (for two way binding purposes)
        // change in property does not trigger a change
        const columns =  this.settings.slice();
        this.settings = [];
        columns.forEach((c) => {
            if (c.sorting) {
                c.sorting.selected = false;
            }
        });

        // change the sort order
        if (column.sorting.order === TableColumnSortOrderOptions.asc) {
            column.sorting.order = TableColumnSortOrderOptions.desc;
        } else {
            column.sorting.order = TableColumnSortOrderOptions.asc;
        }
        // select the current column 
        column.sorting.selected = true;
        
        // send the change
        this.settings = columns;
        this.settingsChanged.emit(this.settings);
    }

    // ===============
    // GROUPING ROWS
    // ===============

    /** Set the initial sort option from the string representation.
     * Usefull on initialization purposes where sorting is saved to storage as a string
     */
    private groupRows(rows: QueryList<ElementRef>): Promise<void> {
        return new Promise((resolve, reject) => {
            /*** First, find the index of the grouped column ***/
            const groupedColumn = this.settings.filter((s) => s.grouping === true);
            if (!groupedColumn || groupedColumn.length !== 1) {
                return;
            }

            const groupedColumnIndex = this.settings.indexOf(groupedColumn[0]);
            const visibleColumns = this.settings.filter((s) => s.visible !== false);
            const groupedHeader = (this.thead.nativeElement as HTMLElement).children[groupedColumnIndex];

            let run = 1;
            const rowsArray = rows.toArray();
            for (let i = rowsArray.length - 1; i > -1; i--) {
                const currentTr = (rowsArray[i].nativeElement as HTMLElement);
                const cells = currentTr.children;
                const currentCell: Element = cells[groupedColumnIndex];
                let prevCell: Element = null;
                if (i > 0) {
                    prevCell = (rowsArray[i - 1].nativeElement as HTMLElement).children[groupedColumnIndex];
                }
                // console.log('current', currentCell, 'index', i);
                
                if (prevCell && currentCell.textContent === prevCell.textContent) {
                    prevCell.textContent = currentCell.textContent;
                    run++;
                } else {
                    // create a copy of the grouped cell
                    const newCell: Element = document.createElement('td');
                    newCell.setAttribute('colspan', visibleColumns.length.toString());
                    newCell.innerHTML = `${groupedHeader.textContent}: ${currentCell.textContent}`;
                    
                    // append to a new tr
                    const tr: Element = document.createElement('tr');
                    tr.appendChild(newCell);
                    tr.classList.add('group');

                    // add click event to grouped row, to expand/collapse rows belonging to this group
                    tr.addEventListener('click', (e) => {
                        this.expandCollapseGroupedRows(tr);
                    });

                    // add the new grouped row to the table
                    (this.dataTable.nativeElement as HTMLElement).insertBefore(tr, currentTr);
                    run = 1;
                }

                // remove the grouped cell
                currentCell.remove();
            }

            // remove the grouped header
            groupedHeader.classList.add('hide-col');

            resolve();
        });
    }

    /** Toggle visibility of rows that belond to a group */
    private expandCollapseGroupedRows(tr: Element) {
        // Setup siblings array
        const siblings = [];

        // Get the next sibling element
        let nextTr = tr.nextElementSibling;
        // As long as a sibling exists
        while (nextTr) {

            // If we've reached our match, bail
            if (nextTr.matches('.group')) {
                break;
            }
            if (nextTr.classList.contains('collapsed')) {
                nextTr.classList.remove('collapsed');
            
            } else {
                nextTr.classList.add('collapsed');
            }

            // Otherwise, push it to the siblings array
            siblings.push(nextTr);

            // Get the next sibling element
            nextTr = nextTr.nextElementSibling;
        }
    }

     /* Destroy grouping to allow proper redraw of table (in case of data change) 
      *
      * IMPORTANT NOTE: nodeList is dynamic and will change when we remove the items (tr with class group). 
      * We can work around that by processing the array in reverse order so the only part of the nodeList that changes 
      * when you remove an item is the part we've already processed, not the part we still have to go
     */
    private destroyGrouping(): Promise<void> {
        return new Promise((resolve, reject) => {
            const nodeList = document.getElementsByClassName('group') as HTMLCollection;
            for (let i = nodeList.length - 1; i >= 0; i--) {
                const tr = nodeList.item(i) as HTMLElement;
                // console.log(tr);
                tr.remove();
            }

            const groupedHeader = document.getElementsByClassName('hide-col') as HTMLCollection;
            if (groupedHeader.length === 1) {
                groupedHeader[0].classList.remove('hide-col');
            }

            resolve();
        });
    }

    /** Throws an error if more than on column have been marked as grouping column */
    private dataTableSanityCheck(): void {
        const numOfGroupedColumns = this.settings.filter((s) => s.grouping === true);
        if (numOfGroupedColumns.length > 1) {
            throw new RangeError('Only one column can be grouped');
        }
    }
}
