
// FRAMEWORK
import { Component, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Subject } from 'rxjs';

// TYPES
import { KeyName } from '../../../common/key-name';
import { MoreOptionsEditingOptionsDialogPosition } from './more-options-edit-options-dialog-types';


@Component({
    selector: 'more-options-edit-options-dialog',
    templateUrl: 'more-options-edit-options-dialog.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['more-options-edit-options-dialog.less']
})

export class MoreOptionsEditingOptionsDialogComponent {
    @ViewChild('moreOptionEditingOptionsDialog', { static: true }) public dialog: ElementRef;

    public options: KeyName[];
    private closed: Subject<{}> = new Subject<{}>();
    private element: any;

    @HostListener('document:touchstart', ['$event', '$event.target'])
    @HostListener('document:click', ['$event', '$event.target'])

    public afterClosed() {
        return this.closed;
    }

    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
        event.stopPropagation();
        event.cancelBubble = true;
        if (targetElement.tagName === 'MAT-ICON'  || targetElement.tagName === 'BUTTON') {
            return;
        }

        const clickedInside = this.dialog.nativeElement.contains(targetElement);
        if (!clickedInside && this.dialog.nativeElement.style.display === 'block') {
            this.onClose();
        }
    }

    public open(element: any, options: KeyName[], position: MoreOptionsEditingOptionsDialogPosition) {
        this.element = element;
        this.options = options;

        if (this.dialog) {
            this.dialog.nativeElement.style.display = 'block';
            this.dialog.nativeElement.style.left = `${ position.x - this.dialog.nativeElement.offsetWidth - 20  }px`;
            this.dialog.nativeElement.style.top = `${ position.y - 20 }px`;
        }
    }

    public onSelectOption(option: KeyName) {
        this.onClose(option.id);
    }

    private onClose(option?: any) {
        this.dialog.nativeElement.style.display = 'none';
        this.closed.next({ element: this.element, action: option });
    }

}
