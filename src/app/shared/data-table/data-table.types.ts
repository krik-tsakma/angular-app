/**
 * The possible column settings
 */
export class TableColumnSetting {
    /** The parameter name of the values that will be shown in this column. */
    public primaryKey: string;
    /** The title of the column. May provide an untranslated text. */
    public header?: string;
    /** The format type of the values */
    public format?: TableCellFormatOptions;
    /** A custom class for the cell */
    public customClass?: (record: any) => string;
    /**
     * @type {function}
     * @param {any} record - Current row's data
     * Called only when TableCellFormatOptions.custom
     */
    public formatFunction?: (record: any) => void; 
    /** Whether the column will be shown. */
    public visible?: boolean;
    public actions?: TableActionOptions[];
    /** Whether an action icon of type TableActionOptions, should be shown or not (Default: true). */
    public actionsVisibilityFunction?: (action: TableActionOptions, record: any) => boolean;  
    public sorting?: TableColumnSortOptions;
    public metricInline?: boolean;
    public grouping?: boolean;
}

export class TableActionItem {
    public action: TableActionOptions;
    public record: any;
    public e: MouseEvent;
}

export class TableClickRowItem {
    public event: Event;
    public record: any;
}

export enum TableActionOptions {
    details,
    edit,
    delete,
    check,
    moreOptions,
    download
}

export enum TableCellFormatOptions {
    default,
    string,
    checkIcon,
    dateTime,
    date,
    time,
    percentage,
    distance,
    speed,
    score,
    energyGJ,
    energyGJNormalized,
    energyMJ,
    energyMJNormalized,
    weight,
    custom
}

export class TableColumnSortOptions {
    public order: TableColumnSortOrderOptions;
    public selected: boolean;
    public name?: string;

    constructor(order: TableColumnSortOrderOptions, selected: boolean, name: string) {
        this.order = order;
        this.selected = selected;
        this.name = name;
    }

    /** Get the sort column */
    public stringFormat(columnPrimaryKey: string) {
        const sortColumn = this.name ? this.name : columnPrimaryKey;
        return sortColumn + ' ' + TableColumnSortOrderOptions[this.order];
    }
}

/**
 * Sorting options
 */
export enum TableColumnSortOrderOptions {
    asc,
    desc
}

/** Helper to get the default sort column if the existing cannot be found */
export class TableColumnSortDefaults {
    public static getFirstAvailableSortColumn(columns: TableColumnSetting[]): string {
        const firstAvailableSortColumn =  columns.find((x) => x.sorting !== null && x.sorting !== undefined);
        firstAvailableSortColumn.sorting = new TableColumnSortOptions(firstAvailableSortColumn.sorting.order, true, firstAvailableSortColumn.sorting.name);
        return firstAvailableSortColumn.sorting.stringFormat(firstAvailableSortColumn.primaryKey);
    }
}
