// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// SERVICES
import { UserOptionsService } from '../user-options/user-options.service';

// TYPES
import { TableCellFormatOptions } from './data-table.types';
import { EnergyUnitOptions } from '../../common/energy-types/energy-types';

@Pipe({ name: 'myFormatCellMetric' })
export class FormatCellMetricPipe implements PipeTransform {
    constructor(
        private userOptions: UserOptionsService
    ) { }
    public transform(format: TableCellFormatOptions, energyUnit?: EnergyUnitOptions) {

        let metric = '';
        switch (format) {
            case TableCellFormatOptions.distance:
                metric = this.userOptions.metrics.distance;
                break;
            case TableCellFormatOptions.percentage:
                metric = '%';
                break;
            case TableCellFormatOptions.speed:
                metric = this.userOptions.metrics.speed;
                break;
            case TableCellFormatOptions.energyGJ:
                metric = this.userOptions.getCalculatedUnit('energyGJ', null, energyUnit);
                break;
            case TableCellFormatOptions.energyGJNormalized:
                metric = this.userOptions.getCalculatedUnit('energyGJ', 100, energyUnit);
                break;
            case TableCellFormatOptions.energyMJ:
                metric = this.userOptions.getCalculatedUnit('energyMJ', null, energyUnit);
                break;
            case TableCellFormatOptions.energyMJNormalized:
                metric = this.userOptions.getCalculatedUnit('energyMJ', 100, energyUnit);
                break;
            case TableCellFormatOptions.weight:
                metric = this.userOptions.metrics.weight;
                break;
            case TableCellFormatOptions.default:
            default: 
                break;
        }

        if (metric) {
            return '(' + metric + ')';
        }
        return '';

    }
}
