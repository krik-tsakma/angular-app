
// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// PIPES
import { DateFormatPipe , DateFormatter, DateFormatterDisplayOptions } from '../pipes/dateFormat.pipe';
import { TimeFormatPipe, TimeFormatter } from '../pipes/timeFormat.pipe';
import { NumberFormatPipe } from '../pipes/numberFormat.pipe';
import { ScoreFormatPipe } from '../pipes/scoreFormat.pipe';
import { ScoreFormatter } from '../../common/score';

// SERVICES
import { UserOptionsService } from '../user-options/user-options.service';

// TYPES
import { TableCellFormatOptions } from './data-table.types';

@Pipe({ name: 'myFormatCell' })
export class FormatCellPipe implements PipeTransform {
    constructor(
        private timePipe: TimeFormatPipe,
        private datePipe: DateFormatPipe,
        private numberFormatPipe: NumberFormatPipe,
        private scoreFormatPipe: ScoreFormatPipe,
        private userOptions: UserOptionsService
    ) { }

    public transform(value: any, format: TableCellFormatOptions) {
        if (value === undefined || value === null) {
            return 'N/A';
        }

        if (value === '') {
            return '-';
        }

        switch (format) {
            case TableCellFormatOptions.date:
                return this.datePipe.transform(value, { display: DateFormatterDisplayOptions.date, dateFormat: this.userOptions.localOptions.short_date } as DateFormatter );
            case TableCellFormatOptions.time:
                return this.timePipe.transform(value, { format: 'HH:mm', includeDays: true } as TimeFormatter );
            case TableCellFormatOptions.dateTime:
                return this.datePipe.transform(value, { display: DateFormatterDisplayOptions.both, dateFormat: this.userOptions.localOptions.short_date } as DateFormatter );
            case TableCellFormatOptions.distance:
            case TableCellFormatOptions.percentage:
            case TableCellFormatOptions.speed:
            case TableCellFormatOptions.energyGJ:
            case TableCellFormatOptions.energyGJNormalized:
            case TableCellFormatOptions.energyMJ:
            case TableCellFormatOptions.energyMJNormalized:
                return this.formatToNumber(value);
            case TableCellFormatOptions.score:
                return this.scoreFormatPipe.transform(value, ScoreFormatter.letter);
            case TableCellFormatOptions.string: 
                return value;
            case TableCellFormatOptions.default:
            default: {
                if (typeof value === 'number') {
                    return this.formatToNumber(value);
                }
                if (typeof value === 'object') {
                    return value.name;
                }
                if (Array.isArray(value)) {
                    return this.formatArrayToString(value);
                }
            }
        }

            
        return value;
    }

    private formatToNumber(value: any) {
        return this.numberFormatPipe.transform(value, { 
            decimalSeparator: this.userOptions.localOptions.decimal_separator,
            thousandsSeparator: this.userOptions.localOptions.thousands_separator, 
            decimals: 2});
    }

    private formatArrayToString(value: any) {
        if (typeof value[0] !== 'object') {
            return value.join(', ');
        } else {
            return value.map((obj) => {
                return obj.name;
            }).join(', ');
        }
    }
}
