﻿// FRAMEWORK
import { Injectable } from '@angular/core';

// RxJS
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { PasswordRules } from './password-rules-types';

@Injectable()
export class PasswordRulesService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api';
        }


    // Get the password policy for the company
    public getForIssuer(): Observable<PasswordRules> {
        return this.authHttp.get(this.baseURL + '/PasswordRulesAuth');
    }

    public getForUser(code: string): Observable<PasswordRules> {
        return this.authHttp.get(this.baseURL + '/PasswordRules/GetForUser?code=' + code);
    }

    public getForDriver(code: string): Observable<PasswordRules> {
        return this.authHttp.get(this.baseURL + '/PasswordRules/GetForDriver?code=' + code);
    }
} 
