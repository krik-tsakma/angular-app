﻿export interface PasswordRules {
    minPasswordLength: number;
    minLowerCaseChars: number;
    minUpperCaseChars: number;
    minDigits: number;
    minSpecialChars: number;
}

export interface PasswordRulesValidation {
    minPasswordLengthInvalid: boolean;
    minLowerCaseCharsInvalid: boolean;
    minUpperCaseCharsInvalid: boolean;
    minDigitsInvalid: boolean;
    minSpecialCharsInvalid: boolean;
}

export enum PasswordRulesFromOptions {
    issuer = 0,
    driver = 1, 
    user = 2
}
