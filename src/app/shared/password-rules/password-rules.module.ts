// FRAMEWORK
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material';

// MODULES
import { TranslateModule } from '@ngx-translate/core';
import { CustomPipesModule } from './../pipes/custom-pipes.module';

// SERVICES
import { PasswordRulesService } from './password-rules.service';

// COMPONENTS
import { PasswordRulesComponent } from './password-rules.component';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        CustomPipesModule,
        MatIconModule
    ],
    declarations: [
        PasswordRulesComponent,
    ],      
    providers: [
        PasswordRulesService,
    ],
    exports: [
        PasswordRulesComponent
    ]
})
export class PasswordRulesModule { }
