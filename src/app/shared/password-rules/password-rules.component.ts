﻿// FRAMEWORK]
import { 
    Component, 
    OnInit, 
    Input, 
    OnChanges, 
    SimpleChanges, 
    Output,
    EventEmitter
} from '@angular/core';

// RxJS
import { Subscription } from 'rxjs';

// SERVICES
import { PasswordRulesService } from './password-rules.service';

// TYPES
import { 
    PasswordRules, 
    PasswordRulesValidation, 
    PasswordRulesFromOptions 
} from './password-rules-types';

@Component({
    selector: 'password-rules',
    templateUrl: './password-rules.html',
    styleUrls: ['password-rules.less']
})

export class PasswordRulesComponent implements OnInit, OnChanges {
    @Input() public password: string;
    @Input() public type: PasswordRulesFromOptions;
    @Input() public code: string;
    @Output() public onValidation: EventEmitter<boolean> = new EventEmitter();

    public loading: boolean;
    public rules: PasswordRules = {} as PasswordRules;
    public validation: PasswordRulesValidation = {} as PasswordRulesValidation;
    public isPasswordValid: boolean;
    
    private serviceSubscriber: Subscription;

    constructor(private service: PasswordRulesService) { }

    public ngOnInit(): void {
        this.resetValidation();
        this.get();
    }

    public ngOnChanges(changes: SimpleChanges): void {
        const passChange = changes['password'];
        if (!passChange) {
            return;
        }
        const pass = passChange.currentValue;
        this.resetValidation();
        this.validation.minPasswordLengthInvalid = ((pass || '').length < this.rules.minPasswordLength);
        this.validation.minLowerCaseCharsInvalid = (this.lowerCaseCount(pass) < this.rules.minLowerCaseChars);
        this.validation.minUpperCaseCharsInvalid = (this.upperCaseCount(pass) < this.rules.minUpperCaseChars);
        this.validation.minDigitsInvalid = (this.numericCount(pass) < this.rules.minDigits);
        this.validation.minSpecialCharsInvalid = (this.nonAlphaCount(pass) < this.rules.minSpecialChars);
    
        this.isPasswordValid = !(this.validation.minPasswordLengthInvalid || 
                        this.validation.minLowerCaseCharsInvalid ||
                        this.validation.minUpperCaseCharsInvalid ||
                        this.validation.minDigitsInvalid ||
                        this.validation.minSpecialCharsInvalid);

        this.onValidation.emit(this.isPasswordValid);
    }

    private resetValidation() {
        this.isPasswordValid = false;
        this.onValidation.emit(this.isPasswordValid);
        this.validation = {
            minPasswordLengthInvalid: true,
            minLowerCaseCharsInvalid: true,
            minUpperCaseCharsInvalid: true,
            minDigitsInvalid: true,
            minSpecialCharsInvalid: true
        };
    }

    private upperCaseCount(password: string): number {
        return ((password || '').match(/[A-Z]/g) || []).length;
    }

    private lowerCaseCount(password: string): number {
        return ((password || '').match(/[a-z]/g) || []).length;
    }

    private numericCount(password: string): number {
        return ((password || '').match(/[0-9]/g) || []).length;
    }

    private nonAlphaCount(password: string): number {
        return ((password || '').match(/[^0-9a-zA-Z\._]/g) || []).length;
    }

    private get() {
        this.loading = true;
        if (this.serviceSubscriber) {
            this.serviceSubscriber.unsubscribe();
        }
        
        switch (this.type) {
            case PasswordRulesFromOptions.issuer:
                this.serviceSubscriber = this.service
                .getForIssuer()
                .subscribe((res: PasswordRules) => {
                    this.loading = false;
                    this.rules = res;
                },
                (err) => {
                    this.loading = false;
                });
                break;
            case PasswordRulesFromOptions.user:
                if (!this.code || this.code.length === 0) {
                    return;
                }
                this.serviceSubscriber = this.service
                .getForUser(this.code)
                .subscribe((res: PasswordRules) => {
                    this.loading = false;
                    this.rules = res;
                },
                (err) => {
                    this.loading = false;
                });
                break;
            case PasswordRulesFromOptions.driver:
                if (!this.code || this.code.length === 0) {
                    return;
                }
                this.serviceSubscriber = this.service
                .getForDriver(this.code)
                .subscribe((res: PasswordRules) => {
                    this.loading = false;
                    this.rules = res;
                },
                (err) => {
                    this.loading = false;
                });
                break;
        }
        
    }
}
