// FRAMEWORK
import {
    Component, Input, Output, OnInit,
    EventEmitter, ViewEncapsulation
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';

// TYPES
import {
    MoreOptionsMenuOptions, MoreOptionsMenuOption, MoreOptionsMenuTypes
} from './more-options-menu.types';

@Component({
    selector: 'more-options-menu',
    templateUrl: 'more-options-menu.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['more-options-menu.less']    
})


export class MoreOptionsMenuComponent implements OnInit {
    @Input() public options: MoreOptionsMenuOptions[];   
    @Input() public subTitles: boolean;
    @Input() public isForHeader: boolean;
    @Output() public notifyOnOptionSelection: EventEmitter<MoreOptionsMenuOption> = new EventEmitter();
    
    public types: any = MoreOptionsMenuTypes;

    constructor(private iconRegistry: MatIconRegistry,
                private sanitizer: DomSanitizer) {
        // Foo
    }


    public ngOnInit() {   
        if (this.options && this.options.length > 0) {            
            // register svg icons used in this page
            this.options.forEach((group) => {
                group.options.forEach((option) => {
                    this.iconRegistry
                        .addSvgIcon(option.svgIcon,
                        this.sanitizer.bypassSecurityTrustResourceUrl('../../assets/' + option.svgUrl));
                });
            });
        }
    }

    public onOptionSelection(option: MoreOptionsMenuOption) {
        if (option.type === MoreOptionsMenuTypes.checkbox) {
            option.checkboxValue = !option.checkboxValue;
        }
        this.notifyOnOptionSelection.emit(option);
    }

}
