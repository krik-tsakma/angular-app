export interface MoreOptionsMenuOptions {
    term: string;
    options: MoreOptionsMenuOption[];
}

export interface MoreOptionsMenuOption {
    id: any;
    term: string;
    type: MoreOptionsMenuTypes;
    enabled: boolean;
    checkboxValue?: boolean;
    svgIcon?: string;
    svgIconClass?: string;
    svgUrl?: string;
}

export enum MoreOptionsMenuTypes {
    none,
    svgIcon,
    checkbox
}
