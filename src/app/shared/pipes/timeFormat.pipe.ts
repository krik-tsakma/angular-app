// FRAMWORK
import { Pipe, PipeTransform } from '@angular/core';

// SERVICE
import { CustomTranslateService } from '../custom-translate.service';

// MOMENT
import * as moment from 'moment';

/*
 * This pipe a seconds to time format. If a format is not provided,
seconds will be formatted to hours:minutes
 * Usage:
 *   seconds | timeFormat:arguments
 * Example:
 *   {{ 1432 | timeFormat:{ format:"HH:mm:ss" } }}
 *   formats to:  00:23:52
*/
export interface TimeFormatter {
    format: string;
    includeDays: boolean;
}
@Pipe({name: 'myTimeFormat'})
export class TimeFormatPipe implements PipeTransform {
    private daysLbl: string;
    constructor(public translate: CustomTranslateService) {
        this.daysLbl = 'days';
        translate.get('time.days').subscribe((t: any) => {
            this.daysLbl = t;
        });
    }

    public transform(seconds: any, args: TimeFormatter): string {
        if (typeof(seconds) !== 'number') {
            return seconds;
        }
        let format = 'HH:mm';
        if (args.format) {
            format = args.format;
        }

        const timeFormatted = moment.utc(moment.duration(seconds, 'seconds')
                .asMilliseconds())
                .format(format);

        if (args.includeDays !== undefined && args.includeDays === true) {
            const d = moment.duration(seconds, 'seconds');
            const days =  Math.floor(d.asDays());

            const durationInDays =  days > 0 ? days + ' ' + this.daysLbl + ' '  + timeFormatted : timeFormatted;
            return durationInDays;
        } else {
            return timeFormatted;
        }
    }
}
