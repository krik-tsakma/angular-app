// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// TYPES
import { TripDisplaySettings } from '../../trip-review/trip-review-types';

// MOMENT
import * as moment from 'moment';

/*
 * This pipe provides the raw html that is used to represent a score object
 * Usage:
 *   tripPropertyValue | tripPropertyFormat:arguments
 * Example:
 *   {{ 56  | {
        name: 'durationSecs',
        closed: true,
        hidden: true,
        settings: {
            showDurationInHiddenTrips: false,
            showMileageInHiddenTrips: false,
            showOdometerStartstopInHiddenTrips: false
        }
    }
} }}
 *   formats to: '-'
*/
export interface TripFormatter {
    name: string;
    closed: boolean;
    hidden: boolean;
    settings: TripDisplaySettings;
}

@Pipe({name: 'myTripPropertyFormat'})
export class TripPropertyFormatPipe implements PipeTransform {
    public transform(value: any, args: TripFormatter): any {
         // if trip is NOT closed hide end values
        if (!args.closed) {
            switch (args.name) {
                case 'stopTimestamp':
                case 'stopAddress':
                case 'stopOdometer':
                    value = '-';
                    break;
                default:
                    break;
            }
        }
         // If trip is hidden and company settings instruct not to show one of the following
        if (args.hidden) {
            switch (args.name) {
                case 'durationSecs':
                case 'idleSecs':
                case 'idleSecsPerc':
                case 'driveSecs':
                case 'driveSecsPerc':
                case 'startTimestamp':
                case 'stopTimestamp':
                    if (!args.settings.showDurationInHiddenTrips) {
                      value = '-';
                    }
                    break;
                case 'distance':
                    if (!args.settings.showMileageInHiddenTrips) {
                      value = '-';
                    }
                    break;
                case 'startOdometer':
                case 'stopOdometer':
                    if (!args.settings.showOdometerStartstopInHiddenTrips) {
                      value = '-';
                    }
                    break;
                case 'maxSpeed':
                case 'avgSpeed':
                    value = '-';
                    break;
                default:
                    break;
            }
        }
        return value;
    }
}
