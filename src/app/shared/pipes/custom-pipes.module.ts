// FRAMEWORK
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// MODULES
import { FormValidationMessagePipeModule } from './form-validation-message.module';

// PIPES
import { StringFormatPipe } from './stringFormat.pipe';
import { NumberFormatPipe } from './numberFormat.pipe';
import { DateFormatPipe } from './dateFormat.pipe';
import { TimeFormatPipe } from './timeFormat.pipe';
import { ScoreFormatPipe } from './scoreFormat.pipe';
import { TripPropertyFormatPipe } from './tripPropertyFormat.pipe';
import { FormatCellPipe } from '../data-table/format-cell.pipe';
import { FormatCellMetricPipe } from '../data-table/format-cell-metric.pipe';


@NgModule({
    imports: [
        FormValidationMessagePipeModule
    ],
    declarations: [
        DateFormatPipe,
        StringFormatPipe,
        NumberFormatPipe,
        TimeFormatPipe,
        ScoreFormatPipe,
        TripPropertyFormatPipe,
        FormatCellPipe,
        FormatCellMetricPipe
    ],
    exports: [
        FormValidationMessagePipeModule,
        DateFormatPipe,
        StringFormatPipe,
        NumberFormatPipe,
        TimeFormatPipe,
        ScoreFormatPipe,
        TripPropertyFormatPipe,
        FormatCellPipe,
        FormatCellMetricPipe,
    ]
 })

 export class CustomPipesModule {

    public static forRoot() {
        return {
            ngModule: CustomPipesModule,
            providers: [],
        };
    }
 }
