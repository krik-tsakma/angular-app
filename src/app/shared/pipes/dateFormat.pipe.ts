// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// MOMENT
import moment from 'moment';

/*
 * This pipe provides number variable replacement in a decimal separator and rounding if asked
 * Usage:
 *   timestamp | dateFormat:arguments
 * Example:
 *   {{ 2016-12-15T09:25:47.01 | dateFormat:{ display: dateDisplayOptions.both, dateFormat: "M/D/YYYY" } }}
 *   formats to:  12/15/2016 09:25
*/

export interface DateFormatter {
    display: DateFormatterDisplayOptions;
    dateFormat: string;
    showSeconds?: boolean;
}

export enum DateFormatterDisplayOptions {
    time,
    date,
    both
}

@Pipe({name: 'myDateFormat'})
export class DateFormatPipe implements PipeTransform {
    public transform(timestamp: any, args: DateFormatter): string {
        const timeFormat = args.showSeconds === true ? 'HH:mm:ss' : 'HH:mm';
        switch (args.display) {
            case DateFormatterDisplayOptions.date:
                return moment(timestamp).format(args.dateFormat);
            case DateFormatterDisplayOptions.time:
                return moment(timestamp).format(timeFormat);
            case DateFormatterDisplayOptions.both:
                return moment(timestamp).format(args.dateFormat + ' ' + timeFormat);
        }
    }
}
