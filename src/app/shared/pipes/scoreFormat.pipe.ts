// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// TYPES
import { Score, ScoreFormatter } from '../../common/score';

// MOMENT
import * as moment from 'moment';

/*
 * This pipe provides the raw html that is used to represent a score object
 * Usage:
 *   scoreObject | scoreFormat:arguments
 * Example:
 *   {{ { id:1, letter: 'A', value: 10 }  | myScoreFormat }}
 *   formats to:  <span class="score_letter A'>A</span>
*/

@Pipe({name: 'myScoreFormat'})
export class ScoreFormatPipe implements PipeTransform {
    public transform(score?: Score, args?: ScoreFormatter): string {
        if (!score || score.normalized === null) {
            return '';
        }
        if (args === null || typeof(args) === 'undefined') {
            args = ScoreFormatter.letter;
        }
        const letter = `<span class="score_letter ${ score.letter }">
                            ${ score.letter }
                        </span>`;
        const value = `<span class="score_value"> ${ score.normalized } </span>`;
        
        switch (args) {
            case ScoreFormatter.all: 
                return letter + value;
            case ScoreFormatter.letter: 
                return letter;
            case ScoreFormatter.value: 
                return value;
            default:
                return '';
        }
    }
}
