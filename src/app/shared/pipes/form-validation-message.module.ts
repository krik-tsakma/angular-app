// FRAMEWORK
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// SERVICES
import { CustomTranslateService } from '../custom-translate.service';

// PIPES
import { StringFormatPipe } from './stringFormat.pipe';
import { FormValidationMessagePipe } from './form-validation-message.pipe';
import { TranslatePipe } from '@ngx-translate/core';

// If we do not set the pipes in the providers we'll get pipe provider injection error
@NgModule({
    declarations: [
        FormValidationMessagePipe
    ],
    exports: [
        FormValidationMessagePipe,
    ], 
    providers: [
        StringFormatPipe,
        TranslatePipe,
        CustomTranslateService
    ]
})

 export class FormValidationMessagePipeModule {

    public static forRoot() {
        return {
            ngModule: FormValidationMessagePipeModule,
            providers: [],
        };
    }
 }
