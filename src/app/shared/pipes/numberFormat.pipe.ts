// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';
/*
 * This pipe provides number variable replacement
 * in a decimal separator and rounding if asked
 * Usage:
 *   value | numberFormat:arguments
 * Example:
 *   {{ 4.50123 | numberFormat:[',', 2] }}
 *   formats to:  4,50
*/

export interface NumberFormatter {
    decimalSeparator: string;
    thousandsSeparator: string;
    decimals: number;
}
@Pipe({name: 'myNumberFormat'})
export class NumberFormatPipe implements PipeTransform {
    public transform(value: any, args: NumberFormatter): string {
        // If this is not a number
        if (typeof(value) !== 'number') {
            return value;
        }
        // Convert to string value
        let textValue = value.toString();

        // Convert number to fixed point
        textValue = value.toFixed(args.decimals);

        // Get the decimal separator
        const num = textValue.replace('.', args.decimalSeparator);
        return num.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + args.thousandsSeparator);
    }
}
