// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// SERVICES
import { CustomTranslateService } from '../custom-translate.service';

// PIPES
import { StringFormatPipe } from './stringFormat.pipe';

/*
 * This pipe provides the translated message for a form field
 * Usage:
 *   errors | myFormValidationMessage:arguments
 * Example:
 *   {{ email.error | myFormValidationMessage: : { field: 'create_account.email_address' } }}
 *   formats to:  Please enter a valid email address.
*/

export interface FormValidationFormatter {
    field: string;
    compareField: string;
    min: number;
    max: number;
    minLength: number;
    maxLength: number;
}
export class FormValidationErrorsCommon {
    public static readonly required = 'required';
    public static readonly validateEmail = 'validateEmail';
    public static readonly validateEqual = 'validateEqual';
    public static readonly validateNumber = 'validateNumber';
    public static readonly validateInteger = 'validateInteger';
    public static readonly validatePhoneNumber = 'validatePhoneNumber';
    public static readonly validateRange = 'validateRange';
    public static readonly validateIP = 'validateIP';
    public static readonly minLength = 'minlength';
    public static readonly maxLength = 'maxlength';
}

@Pipe({name: 'myFormValidationMessage'})
export class FormValidationMessagePipe implements PipeTransform {
    constructor(
        private translateService: CustomTranslateService,
        private stringFormatPipe: StringFormatPipe,
    ) { 
        // foo
    }

    public transform(errors: any, args: FormValidationFormatter): string {
        if (!errors) {
            return;
        }
        
        const fieldLabelTranslated = this.translateService.instant(args.field);
        if (errors[FormValidationErrorsCommon.required]) {
            const required = this.translateService.instant('form.errors.required');
            return this.stringFormatPipe.transform(required, [fieldLabelTranslated]);
        } 
        if (errors[FormValidationErrorsCommon.validateEmail]) {
            return this.translateService.instant('form.errors.email_invalid');
        }         
        
        if (errors[FormValidationErrorsCommon.validateEqual]) {
            const compareFieldLabelTranslated = this.translateService.instant(args.compareField);
            const notEqual = this.translateService.instant('form.errors.not_equal');
            return this.stringFormatPipe.transform(notEqual, [compareFieldLabelTranslated, fieldLabelTranslated]);
        }

        if (errors[FormValidationErrorsCommon.validateNumber]) {
            return this.translateService.instant('form.errors.number_invalid');
        }

        if (errors[FormValidationErrorsCommon.validateInteger]) {
            return this.translateService.instant('form.errors.field_integer_invalid');
        }

        if (errors[FormValidationErrorsCommon.validatePhoneNumber]) {
            return this.translateService.instant('form.errors.phone_number_invalid');
        }

        if (errors[FormValidationErrorsCommon.minLength]) {
            const minTranslation = this.translateService.instant('form.errors.field_min_length');
            return this.stringFormatPipe.transform(minTranslation, [fieldLabelTranslated, args.minLength.toString()]);
        }

        if (errors[FormValidationErrorsCommon.maxLength]) {
            const maxTranslation = this.translateService.instant('form.errors.field_max_length');
            return this.stringFormatPipe.transform(maxTranslation, [fieldLabelTranslated, args.maxLength.toString()]);
        }

        if (errors[FormValidationErrorsCommon.validateRange]) {
            const rangeTranslation = this.translateService.instant('form.errors.field_range_error');
            return this.stringFormatPipe.transform(rangeTranslation, [fieldLabelTranslated, 
                                                                    args.min.toString(), 
                                                                    args.max.toString()]);
        }

        if (errors[FormValidationErrorsCommon.validateIP]) {
            return this.translateService.instant('form.errors.ip_invalid');
        } 

        return '';
    }
}
