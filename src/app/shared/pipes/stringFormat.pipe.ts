// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';
/*
 * This pipe provides string variable replacement similar to C# string.Format("{0}", "something");
 * Usage:
 *   text | stringFormat:arguments
 * Example:
 *   {{ The {0} field is required | stringFormat:['fieldName'] }}
 *   formats to:  The fieldName field is required
*/
@Pipe({name: 'myStringFormat'})
export class StringFormatPipe implements PipeTransform {
    public transform(text: string, args: string[]): string {
        // We access all arguments passed to the 
        // functions(explicit and implicit) via 'arguments'
        if (!text) {
            return text; // if string is null or undefined or empty, just return it
        }
        // if it's not a string, this call will fail. 
        // We won't worry about that. Let it fail if it's not!
        for (let i = 0; i < args.length; i++) { // exclude first argument 'text' and start at 1

            // The reason we use regex is that JavaScript 
            // "replace" method when passed (string, string)
            // does replace only one occurance of the first string, 
            // what if {0} existed twice in template ?
            // Regex, with "g" as option, applies a search in 
            // string for all occurances.So we use that
            const pattern = new RegExp('\\{' + (i) + '\\}', 'g'); // remember 'i' starts at 1 not 0
            text = text.replace(pattern, args[i]);
        }
        return text;
    }
}
