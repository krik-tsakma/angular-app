// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { CustomTranslateService } from './custom-translate.service';

@Injectable()
export class SnackBarService {
    private defaultTerms: any;
    private config = new MatSnackBarConfig();

    constructor(
        private translateService: CustomTranslateService,
        private snackBar: MatSnackBar
    ) { 
        this.config.duration = 5000;
    }

    /**
     * Opens a snackbar with a message and an optional action.
     * @param message The message to show in the snackbar.
     * @param action The label for the snackbar action.
     * @param config Additional configuration options for the snackbar.
     */
    public open(message: string, action: string) {
        this.translateService.get([message, action]).subscribe((t: any) => {
            const messageText = t[message];
            const actionText = t[action];

            this.snackBar.open(
                messageText,
                actionText,
                this.config
            );  
        });
    }
}
