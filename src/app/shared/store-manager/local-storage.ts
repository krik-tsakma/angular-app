/** Simple sessionStorage with Cookie Fallback. */
export class LocalStore {
    private lsSupport: boolean = false;
    constructor() {
        if (this.storageAvailable('sessionStorage')) {
            this.lsSupport = true;
        }    
    }
    
    /**
     * Inserts or update an item to the store 
     * @param  key       The key or identifier of the item
     * @param  value     Contents of the item
     */

    public set(key: string, value: any): void {
        // Convert object values to JSON
        if (typeof value === 'object' ) {
            value = JSON.stringify(value);
        }
        // Set the store
        if (this.lsSupport) { // Native support
            sessionStorage.setItem(key, value);
        } else { // Use Cookie
            this.createCookie(key, value, 30);
        }
    }
    
     /**
      * Removes an item from the store with the specified key
      * @param  key       The key or identifier of the item in store
      */

    public remove(key: string): void {
        if (this.lsSupport) { // Native support
            sessionStorage.removeItem(key);
        } else { // Use cookie
            this.createCookie(key, '', -1);
        }
    }
    
     /**
      * Removes alls items from the store 
      */

    public empty(): void {
        if (this.lsSupport) { // Native support
            sessionStorage.clear();
        } else { // Use cookie
            const cookies: any = document.cookie.split(';');
            for (const value of cookies) {
                this.createCookie(value.split('=')[0], '', -1);
            }
        }
    }

    /**
     * Retrieves an item of the store with the specified key
     * @param  key       The key or identifier of the item in store
     */
    
    public get(key: string): any {
        let data: any;
        // Get value
        if (this.lsSupport) { // Native support
            data = sessionStorage.getItem(key);
        } else { // Use cookie 
            data = this.readCookie(key);
        }

        // Try to parse JSON...
        try {
            data = JSON.parse(data);
        } catch (e) {
            data = data;
        }

        return data;
    }

    // Check for native support
    // broadened for private browsing iOS Safari
    private storageAvailable(type) {
        try {
            const storage = window[type];
            const x = '__storage_test__';
            storage.setItem(x, x);
            // Adding a slight delay to remove the item
            // Sometimes (in FF, ie) removeItem() would not fire (setItem was not completed)
            setTimeout(() => { 
                storage.removeItem(x); 
            }, 400);
            return true;
        } catch (e) {
            return false;
        }
    }

     /**
      * Creates new cookie or removes cookie with negative expiration
      * @param  key       The key or identifier for the store
      * @param  value     Contents of the store
      * @param  exp       Expiration - creation defaults to 30 days
      */
    
    private createCookie(key, value, exp) {
        const date = new Date();
        date.setTime(date.getTime() + (exp * 24 * 60 * 60 * 1000));
        const expires = '; expires=' + date.toUTCString();
        document.cookie = key + '=' + value + expires + '; path=/';
    }
    
    /**
     * Returns contents of cookie
     * @param  key       The key or identifier for the store
     */
    
    private readCookie(key) {
        const nameEQ = key + '=';
        const ca = document.cookie.split(';');
        for (let i = 0, max = ca.length; i < max; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) === 0) {
                return c.substring(nameEQ.length, c.length);
            }
        }
        return null;
    }

}
