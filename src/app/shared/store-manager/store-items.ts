/** These are the keys used to store info in local storage. */
export class StoreItems {
    public static readonly accessToken = 'at';
    public static readonly alreadyAnnouns = 'announs';
    public static readonly refreshToken = 'rt';
    public static readonly tokenExpirationTime = 'exp';
    public static readonly appUser = 'u';
    public static readonly assetLinks = 'asls';
    public static readonly bookings = 'bookings';
    public static readonly chargePoints = 'cps';
    public static readonly culture = 'culture';
    public static readonly cultureSetByUser = 'culture_explicit';
    public static readonly diagnostics = 'diagnostics';
    public static readonly drivers = 'drvs';
    public static readonly driverTakeOver = 'drvtkov';
    public static readonly driverGroups = 'hrgrps';
    public static readonly evChargingMonitor = 'evcm';
    public static readonly eventManagerList = 'evml';
    public static readonly evVehicles = 'evc';
    public static readonly evVehiclesExtraInfo = 'evcexin';
    public static readonly BackToAdminButton = 'btab';

    public static readonly exportFiles = 'export_files';
    public static readonly fairUsePolicies = 'fup';
    public static readonly fiscalTrips = 'fiscaltrs';
    public static readonly fiscalTripsAuditLog = 'fiscaltrsal';
    public static readonly locationGroups = 'locgrps';
    public static readonly locations = 'locs';
    public static readonly notificationPolicies = 'notpols';
    public static readonly ocpp = 'ocpp';
    public static readonly quickFiltersTrips = 'qftr';
    public static readonly quickFiltersFollowUp = 'qffu';
    public static readonly searchTypeTrips = 'sttr';
    public static readonly superAdminGUID = 'guid';
    public static readonly tariffs = 'tarrifs';
    public static readonly cpm = 'cpm';
    public static readonly transactions = 'transactions';
    public static readonly cpActionsHistory = 'cpActionsHistory';
    public static readonly userRoles = 'usrrls';
    public static readonly users = 'usrs';
    public static readonly vehileGroups = 'vehgrps';
    public static readonly vehicleTypes = 'vehtps';
    public static readonly vehicles = 'vehs';
    public static readonly scoreDefinition = 'sd';

}
