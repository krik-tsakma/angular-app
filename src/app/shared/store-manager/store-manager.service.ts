﻿// FRAMEWORK
import { Injectable } from '@angular/core';

// TYPES
import { LocalStore } from './local-storage';

@Injectable()
export class StoreManagerService {
    private storeManager: LocalStore;
    
    constructor() {
         this.storeManager = new LocalStore();
    }

    public getOption(item: string) {
         return this.storeManager.get(item);
    }

    public saveOption(item: string, data: any) {
         return this.storeManager.set(item, data);
    }

    public deleteOption(item: string) {
        return this.storeManager.remove(item);
    }

    public clear() {
        return this.storeManager.empty();
   }
}
