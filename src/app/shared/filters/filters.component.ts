// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'filters',
    templateUrl: './filters.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./filters.less']
})

export class FiltersComponent {
    public showFilters: boolean = false; 
    constructor() { 
        // FOO
    } 
}
