// FRAMEWORK
import {
    Component, ViewEncapsulation, Input,  
    ElementRef, AfterViewInit, OnInit
} from '@angular/core';


@Component({
    selector: 'resize-container',
    templateUrl: 'resize-container.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['resize-container.less']
})

export class ResizeContainerComponent implements OnInit, AfterViewInit { 
    @Input() public footerID: string;
    @Input() public hideOverflow: boolean;
    @Input() public ignoreOnMobile: boolean;

    constructor(private elementRef: ElementRef) { }

    public ngOnInit() {
        const that = this;
        let resizedFinished;
        window.onresize = () => {
            clearTimeout(resizedFinished);
            resizedFinished = setTimeout(() => {
                // console.log('Resized finished.');
                if (that.shouldResize()) {
                    that.setHeight();
                }
            }, 250);
        };
    }

    public ngAfterViewInit() {
        if (this.hideOverflow === true) {
            this.elementRef.nativeElement.classList.add('hide-overflow');
        }
        const that = this;
        setTimeout(() => {
            if (that.shouldResize()) {
                that.setHeight();
            }
        }, 1000);
    }

    /** set the height of the dom element */
    private setHeight(): void {
        let height = window.innerHeight;
        const elm = this.elementRef.nativeElement;
        if (elm) {
            elm.scrollTop = 0;
            const distanceFromTop = this.getPosition(elm);
            // console.log(distanceFromTop)
            let heightFooterFromTop = 0;
            if (this.footerID) {
                const footerElm = document.getElementById(this.footerID);
                if (footerElm) {
                    // offsetHeight = VISIBLE content & padding + border + scrollbar
                    // const footerHeight = footerElm.offsetHeight;
                    const distanceFooterFromTop = this.getPosition(footerElm);
                    // console.log(distanceFooterFromTop)
                    heightFooterFromTop = distanceFooterFromTop.y;
                }
            }
            height = heightFooterFromTop > 0 ? heightFooterFromTop - distanceFromTop.y : height - distanceFromTop.y;

            elm.style.maxHeight = height + 'px';
            elm.style.height = height + 'px';
        }
    }

    // offsetTop only looks at the element's parent. 
    // Just loop through parent nodes until you run out of parents and add up their offsets.
    private getPosition(element: any) {
        let xPosition = 0;
        let yPosition = 0;
        
        while (element) {
            xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
            yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
            element = element.offsetParent;
        }

        return { x: xPosition, y: yPosition };
    }


    private shouldResize(): boolean {
        return Boolean(this.ignoreOnMobile) === false || (Boolean(this.ignoreOnMobile) === true && window.innerWidth >= 720);
    }
}
