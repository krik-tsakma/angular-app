// FRAMEWORK
import {
    AfterViewInit, Component, Input, Output, OnChanges,
    EventEmitter, ChangeDetectorRef, SimpleChanges
} from '@angular/core';

// SERVICES
import { UserOptionsService } from '../user-options/user-options.service';
import { CustomTranslateService } from '../custom-translate.service';

// TYPES
import { EntityTypes, EntityTypeOption, EntityTypeOptions } from './entity-types';

@Component({
    selector: 'entity-types',
    templateUrl: './entity-types.html'
})

export class EntityTypesComponent implements AfterViewInit, OnChanges {
    @Input() public htmlID: string;
    @Input() public showNoneOption?: boolean;
    @Input() public selected?: EntityTypes;
    @Input() public enabled?: EntityTypes[];
    @Input() public isDriverRole?: boolean;

    // Callback function on group change
    @Output() public notifyOnChange: EventEmitter<EntityTypes> =
        new EventEmitter<EntityTypes>();

    public isDriver: boolean;
    public isManager: boolean;
    public options: EntityTypeOption[] = EntityTypeOptions;

    constructor(private cdr: ChangeDetectorRef,
                private translate: CustomTranslateService,
                private userOptions: UserOptionsService) {
        const u = userOptions.getUser();
        this.isDriver = u.isDriver;
        this.isManager = u.isManager;

        this.options.forEach((element) => {
            if (!element.term) {
                return;
            }
            element.name = this.translate.instant(element.term);
        });
    }


    public ngAfterViewInit() {
        this.setOptionsAvailability();

        // if none option is not present set to false
        if (this.showNoneOption === undefined) {
            this.showNoneOption = false;
        }

        // if none option is present add to the list, otherwise remove it
        const noneOption = this.options.find((x) => x.id as EntityTypes === EntityTypes.None);
        if (this.showNoneOption === true) {
            if (!noneOption) {
                const none = {
                    id: EntityTypes.None,
                    enabled: true,
                    name: this.translate.instant('form.actions.none')
                } as EntityTypeOption;
                this.options.unshift(none);
            } else {
                noneOption.enabled = true;
            }
        } else {
            this.options = this.options.filter((x) => x.id as EntityTypes !== EntityTypes.None);
        }

        if (!this.selected) {
            this.selected = this.options.find((x) => x.enabled === true).id;
        }

        this.options = this.options.filter((opt) => {
            return !(Number(opt.id) === 1 && (!this.isDriver && !this.isManager && !this.isDriverRole));
        });

        // if group dimension disabled, do not show at all
        const drgDimension = this.options.find((x) => x.id === EntityTypes.DriverGroupDimension);
        if (drgDimension && drgDimension.enabled === false) {
            this.options = this.options.filter((opt) => {
                return opt.id !== EntityTypes.DriverGroupDimension;
            });
        }



        // manually trigger change detection for the current component.
        // otherwise "Expression ___ has changed after it was checked" error
        this.cdr.detectChanges();
    }


    public ngOnChanges(changes: SimpleChanges) {
        const enabledChange = changes['enabled'];
        if (enabledChange && enabledChange.firstChange === false) {
            this.setOptionsAvailability();
        }
    }


    public onChangeOption(id: any) {
        this.notifyOnChange.emit(id);
    }


    public isEnabled(type: EntityTypes) {
        if (this.enabled && !this.enabled.find((x) => x === type)) {
            return false;
        }
        return true;
    }

    private setOptionsAvailability() {
        if (this.enabled) {
            this.options.forEach((element) => {
                element.enabled = true;
                if (this.enabled.find((x) => x === element.id) === undefined) {
                    element.enabled = false;
                }
            });
        } else {
            this.options.forEach((element) => {
                element.enabled = true;
            });
        }
    }
}
