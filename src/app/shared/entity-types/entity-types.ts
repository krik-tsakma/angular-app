import { KeyName } from '../../common/key-name';

export enum EntityTypes {
    None = -1,
    Me = 1,
    Vehicles = 2,
    Drivers = 3,
    VehicleTypes = 4,
    DriverGroupDimension = 5
}

export interface EntityTypeOption extends KeyName {
    term: string;
}

export let EntityTypeOptions: EntityTypeOption[] = [
    {
        id: EntityTypes.Me,
        term: 'assets.me',
        enabled: true,
        name: ''
    },
    {
        id: EntityTypes.Vehicles,
        term: 'assets.groups.vehicle',
        enabled: true,
        name: ''
    },
    {
        id: EntityTypes.Drivers,
        term: 'assets.groups.driver',
        enabled: true,
        name: ''
    },
    {
        id: EntityTypes.VehicleTypes,
        term: 'assets.vehicle_types',
        enabled: true,
        name: ''
    },
    {
        id: EntityTypes.DriverGroupDimension,
        term: 'assets.groups.driver.dimensions',
        enabled: true,
        name: ''
    }
];
