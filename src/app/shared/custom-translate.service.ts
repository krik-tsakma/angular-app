// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class CustomTranslateService {
    private defaultTerms: any;

    constructor(private translateService: TranslateService) {
        // do no use this: this.translateService.getTranslation('en-GB')
        // because it overrides the already loaded user's lang
        // i.e. in a page refresh
        this.defaultTerms = this.translateService.store.translations ? this.translateService.store.translations['en-GB'] : null;
    }

    // Get a translation json object.
    // If an object does not exist in the local version that is requested, fill in with the respective english terms.
    public get(key: string | string[]): Observable<any> {
        if (!key) {
            return null;
        }
        const selected = this.translateService.store.translations[this.translateService.store.currentLang];
        return this.translateService
            .get(key)
            .pipe(
                map((res: any) => {
                    if (key instanceof Array && this.defaultTerms) {
                        key.forEach((item) => {
                            res[item] = selected[item];
                            const enTranslated = this.defaultTerms[item];
                            if (!res[item]) {
                                res[item] = enTranslated;
                            }
                        });
                    }

                    return res;
                })
            );
    }

    public instant(key: string | string[]): string | any {
        if (!key) {
            return null;
        }

        const selected = this.translateService.store.translations[this.translateService.store.currentLang];
        let res = !(key instanceof Array) ? selected[key] : this.translateService.instant(key);
        if (res === undefined && !(key instanceof Array)) {
            res = this.defaultTerms[key] !== undefined
                ? this.defaultTerms[key]
                : key ;
        }
        if (key instanceof Array && this.defaultTerms) {
            key.forEach((item) => {
                const enTranslated = this.defaultTerms[item];
                for (const t in enTranslated) {
                    if (!res[item][t]) {
                        res[item][t] = enTranslated[t];
                    }
                }
            });
        }
        return res;
    }

}
