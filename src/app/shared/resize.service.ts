// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Subject } from 'rxjs';

@Injectable()
export class ResizeService {
    public resizeFinished: Subject<void> = new Subject<void>();
    private resizedFinishedInterval: any;
    
    constructor() { 
        window.onresize = () => {
            clearTimeout(this.resizedFinishedInterval);
            this.resizedFinishedInterval = setTimeout(() => {
                // console.log('Resized finished.');
                this.resizeFinished.next();
            }, 200);
        };
    }

    /**
     * Trigger a window resize event.
     */
    public trigger() {
        if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
            const evt = document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);
        } else {
            window.dispatchEvent(new Event('resize'));
        }
    }
}
