import { QuickFilter } from '../common/quick-filters/quick-filters-types';

import { PagerResults, PagerRequest } from '../common/pager';
import { Score, ScoreName } from '../common/score';
import { PeriodTypes } from '../common/period-selector/period-selector-types';

export interface DriverSummaryResultItem {
    firstname: string;
    lastname: string;
    energyTypeID?: number;
    totalDistance: number;
    energyUsed: number;
    energyEfficiency: number;
    energySavings: number;
    energySavingsPerc: number;
    energySavingsPotential: number;
    energySavingsPotentialPerc: number;
    energyScore?: Score;
    scores?: Score[];
    averageSpeed: number;
    driveTimeNightPerc: number;
    idleTimePerc?: number;
    idleTimeAfterGracePeriodPerc?: number;
    inefficientAccelerationNumNormalized?: number;
    hardAccelerationNumNormalized?: number;
    excessiveAccelerationNumNormalized?: number;
    inefficientBrakingNumNormalized?: number;
    hardBrakingNumNormalized?: number;
    excessiveBrakingNumNormalized?: number;
    fastLeftCorneringNumNormalized?: number;
    fastRightCorneringNumNormalized?: number;
    durationVehicleSpeedInefficientPerc?: number;
    durationVehicleSpeedHardPerc?: number;
    durationEngineSpeedInefficientPerc?: number;
    driveTimeBrakePedalPerc?: number;
    driveTimeAccPedalPerc?: number;
    driveTimeRollingPerc?: number;
    durationExcessiveAccelerationPerc?: number;
    durationGreenAccelerationPerc?: number;
    durationYellowAccelerationPerc?: number;
    durationRedAccelerationPerc?: number;
    durationGreenBrakingPerc?: number;
    durationYellowBrakingPerc?: number;
    durationRedBrakingPerc?: number;
    driverAidAlertsEngineSpeedNormalized?: number;
    driverAidAlertsPedalAccelerationNormalized?: number;
    driverAidAlertsKineticAccelerationNormalized?: number;
    driverAidAlertsKineticBrakingNormalized?: number;
    driverAidAlertsSpeedNormalized?: number;
    driverAidAlertsIdlingNormalized?: number;
    degenBrakeNumberNormalized?: number;
    dechBrakeNumberNormalized?: number;
    eVEnergyRegenNormalized?: number;
}
export interface DriverSummaryRequest extends PagerRequest {
    quickFilters: QuickFilter;
    periodType: PeriodTypes;
}
export interface DriverSummaryResults extends PagerResults {
    scoreNames: ScoreName[];
    results: DriverSummaryResultItem[];
    resultsFiltered: DriverSummaryResultItem[];
}
