// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';

// MODULES
import { FollowUpRoutingModule } from './follow-up-routing.module';
import { QuickFiltersSharedModule } from '../common/quick-filters/quick-filters-shared.module';
import { PeriodSelectorModule } from '../common/period-selector/period-selector.module';
import { ScrollToTopModule } from '../common/scroll-to-top/scroll-to-top.module';

// SERVICES
import { FollowUpService } from './follow-up.service';

// COMPONENTS
import { FollowUpComponent } from './follow-up.component';


@NgModule({
    imports: [
        HttpClientJsonpModule,
        HttpClientModule,
        SharedModule,
        FollowUpRoutingModule,
        QuickFiltersSharedModule,
        PeriodSelectorModule,
        ScrollToTopModule
    ],
    declarations: [
        FollowUpComponent
    ],
    providers: [
        FollowUpService
    ]
})
export class FollowUpModule { }
