﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../auth/authGuard';
import { FollowUpComponent } from './follow-up.component';

const FollowUpRoutes: Routes = [
    {
        path: '',
        component: FollowUpComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(FollowUpRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class FollowUpRoutingModule { }
