
// FRAMEWORK
import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { FollowUpService } from './follow-up.service';
import { MaximizeService } from '../shared/maximize/maximize.service';
import { StoreManagerService } from '../shared/store-manager/store-manager.service';
import { ResizeService } from '../shared/resize.service';
import { UnsubscribeService } from '../shared/unsubscribe.service';

// TYPES
import { StoreItems } from '../shared/store-manager/store-items';
import { QuickFilter, QuickFiltersEntities, QuickFiltersChangeItem } from '../common/quick-filters/quick-filters-types';
import { Pager } from '../common/pager';
import { EnergyUnitOptions } from '../common/energy-types/energy-types';
import { DriverSummaryRequest, DriverSummaryResultItem, DriverSummaryResults } from './follow-up-types';
import {
    PeriodTypes,
    PeriodTypeOption
} from '../common/period-selector/period-selector-types';
import {
    QuickFilterColumn
} from '../common/quick-filters/columns/quick-filter-column.types';
import { TableColumnSetting, TableColumnSortOptions, TableColumnSortDefaults } from '../shared/data-table/data-table.types';

@Component({
    selector: 'follow-up',
    templateUrl: './follow-up.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./follow-up.less']
})

export class FollowUpComponent implements OnInit, OnDestroy {
    public quickFilterEntities = QuickFiltersEntities;
    public tableSettings: QuickFilterColumn[] = [];

    public periodSelectorOptions: PeriodTypes[] = [
        PeriodTypes.FiscalYearToDate,
        PeriodTypes.Last12Months,
        PeriodTypes.Last13Months,
        PeriodTypes.LastMonth,
        PeriodTypes.LastWeek,
        PeriodTypes.PreviousFiscalYear,
        PeriodTypes.ThisMonth,
        PeriodTypes.ThisWeek,
        PeriodTypes.YearToDate
    ];
    public message: string;
    public loading: boolean;
    public driverSummaryData: DriverSummaryResultItem[];
    public pager: Pager = new Pager();
    public request: DriverSummaryRequest = {} as DriverSummaryRequest;
    public energyTypeUnit?: EnergyUnitOptions;

    private serviceSubscribe: Subscription;

    constructor(
        private service: FollowUpService,
        private maximizeService: MaximizeService,
        private storeManager: StoreManagerService,
        private resizeService: ResizeService,
        private unsubscribe: UnsubscribeService,
        private cdr: ChangeDetectorRef) {
            this.maximizeService.resizer.subscribe(() => {
                resizeService.trigger();
            });
        }

    // =========================
    // ANGULAR LIFECYCLE
    // =========================

    // On initialization
    public ngOnInit(): void {
        const savedData = this.storeManager.getOption(StoreItems.quickFiltersFollowUp) as DriverSummaryRequest;
        if (savedData) {
            this.request = savedData;
        } else {
            this.request.periodType = PeriodTypes.ThisWeek;
        }
    }

    // Clean subscribers to avoid memory leak
    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscribe);
    }


    // =========================
    // QUICK FILTERS CALLBACKS
    // =========================

    // When a user makes any type of change in the quick filters toolbar, reload.
    public onChangeQuickFilterOption(qfcc: QuickFiltersChangeItem): void {
        this.request.quickFilters = qfcc.request;
        this.energyTypeUnit = qfcc.request.energyUnit;

        // if columns do not exist, it means that a change happened but not in column visiblity. i.e. driver group selection
        if (qfcc.columns && qfcc.columns.length > 0) {
            if (this.request.sorting === undefined) {
                this.request.sorting = TableColumnSortDefaults.getFirstAvailableSortColumn(qfcc.columns);
            }
            this.tableSettings = qfcc.columns;
        }

        this.get(true);
        this.cdr.detectChanges();
    }


    // =========================
    // PERIOD SELECTOR CALLBACKS
    // =========================

    // On period select
    public onPeriodSelect(pto: PeriodTypeOption): void {
        this.request.periodType = pto.id;
        this.get(true);
    }


    // =========================
    // TABLE CALLBACKS
    // =========================

    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        if (column.sorting) {
            const sortCol = new TableColumnSortOptions(column.sorting.order, column.sorting.selected, column.sorting.name);
            if (sortCol) {
                this.request.sorting = sortCol.stringFormat(column.primaryKey);
                this.get(true);
            }
        }
    }


    public getEnergyUnit() {
        return this.request && this.request.quickFilters ?  this.request.quickFilters.energyUnit : null;
    }

    // =========================
    // PRIVATE
    // =========================

    // Fetch driver summary data
    private get(resetPageNumber: boolean) {
        // if no column is selected for display then cancel request
        if (!this.request.quickFilters
            || ((!this.request.quickFilters.propertyIDs || this.request.quickFilters.propertyIDs.length === 0)
            && (!this.request.quickFilters.scoreIDs || this.request.quickFilters.scoreIDs.length === 0)
            && !this.request.quickFilters.id)) {
            return;
        }

        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.driverSummaryData = new Array<DriverSummaryResultItem>();
        }
        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.serviceSubscribe);

        // the save the request's data for future use
        this.storeManager.saveOption(StoreItems.quickFiltersFollowUp, this.request);

        // then start the new request
        this.serviceSubscribe = this.service
            .getDriversSummary(this.request)
            .subscribe((response: DriverSummaryResults) => {
                this.loading = false;
                // Get the actual data
                const res = response.results === null
                    ? response.resultsFiltered
                    : response.results;
                res.forEach((r) => {
                    if (r['scores']) {
                        r['scores'].forEach((element) => {
                            r['s' + element.id] = element;
                        });
                    }
                });
                this.driverSummaryData = this.driverSummaryData.concat(res);

                // Get the paging info
                this.pager.pageNumber = response.pageNumber;
                this.pager.pageSize = response.pageSize;
                this.pager.totalPages = response.totalPages;
                this.pager.totalRecords = response.totalRecords;

                if (!this.driverSummaryData || this.driverSummaryData.length === 0) {
                    this.message = 'data.empty_records';
                }
            },
            (err) => {
                this.loading = false;
                this.message = 'data.error';
            }
        );
    }
}
