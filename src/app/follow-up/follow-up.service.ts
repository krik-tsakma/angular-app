﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from '../config/config';
import { AuthHttp } from '../auth/authHttp';
import { Pager } from '../common/pager';

// TYPES
import { DriverSummaryRequest, DriverSummaryResults } from './follow-up-types';
import { PeriodTypes } from '../common/period-selector/period-selector-types';
import { QuickFilter } from '../common/quick-filters/quick-filters-types';

@Injectable()
export class FollowUpService {
    constructor(
        private authHttp: AuthHttp,
        private configService: Config) {}

    public getDriversSummary(req: DriverSummaryRequest): Observable<DriverSummaryResults> {
        const qfr = req.quickFilters;        
        const params = new HttpParams({
            fromObject: {
                AssetID: qfr.assetID ? qfr.assetID.toString() : '',
                GroupID: qfr.groupID ? qfr.groupID.toString() : '',
                VehicleTypeID: qfr.vehicleTypeID ? qfr.vehicleTypeID.toString() : '',
                AssetType: qfr.assetType.toString(),
                PropertyIDs: qfr.propertyIDs ? qfr.propertyIDs.join(',') : '',
                ScoreIDs: qfr.scoreIDs ? qfr.scoreIDs.join(',') : '',
                QuickFilterID: qfr.id ? qfr.id.toString() : '',
                PageNumber: req.pageNumber.toString(),
                PageSize: req.pageSize.toString(),
                Sorting: req.sorting,
                PeriodType: req.periodType.toString()
            }
        });

        return this.authHttp
            .get(this.configService.get('apiUrl') + '/api/FollowUp', { params });
    }
}
