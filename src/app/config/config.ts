﻿// FRAMEWORK
import { Injectable } from '@angular/core';

// ENVIRONMENT
import { environment } from '../../environments/environment';

@Injectable()
export class Config {

    constructor() {}

    // Get config
    public get(key: any) {
        return environment[key];
    }

}
