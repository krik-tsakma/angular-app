// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap, map } from 'rxjs/operators';
import { of } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../admin/admin-base.component';

// SERVICES
import { AuthService } from '../../auth/authService';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { ConfirmDialogService } from '../../shared/confirm-dialog/confirm-dialog.service';
import { StoreManagerService } from '../../shared/store-manager/store-manager.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { DriverTakeOverService } from '../driver-take-over.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { Config } from '../../config/config';

// TYPES
import { Pager } from '../../common/pager';
import { StoreItems } from '../../shared/store-manager/store-items';
import { DriversResult, DriverResultItem, DriversRequest, TakeOverResult, AuthCode } from '../driver-take-over-types';
import {
    TableColumnSetting, TableActionItem, TableActionOptions,
    TableCellFormatOptions, TableColumnSortOrderOptions, TableColumnSortOptions
} from '../../shared/data-table/data-table.types';
import { ConfirmationDialogParams } from '../../shared/confirm-dialog/confirm-dialog-types';
import { OAuthResult, AuthResponseCodeOptions } from '../../auth/authResponse';
import { MoreOptionsMenuOptions } from './../../shared/more-options-menu/more-options-menu.types';

@Component({
    selector: 'view-driver-take-over',
    templateUrl: 'view-driver-take-over.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['view-driver-take-over.less']
})

export class ViewDriverTakeOverComponent extends AdminBaseComponent implements OnInit {
    public drivers: DriverResultItem[] = [];
    public searchTermControl = new FormControl();
    public request: DriversRequest = {} as DriversRequest;
    public pager: Pager = new Pager();
    public loading: boolean;
    public loadingToTakeOver: boolean;
    public tableSettings: TableColumnSetting[];
    public pageOptions: MoreOptionsMenuOptions[];

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBarService: SnackBarService,
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,
        protected storeManager: StoreManagerService,
        protected configService: Config,
        private service: DriverTakeOverService,
        private authService: AuthService
    ) {
        super(translator, previousRouteService, router, unsubscriber, null, configService, snackBarService, storeManager, confirmDialogService);
        this.tableSettings =  this.getTableSettings();
    }


    public ngOnInit(): void {
        super.ngOnInit();

        const savedData = this.storeManager.getOption(StoreItems.driverTakeOver) as DriversRequest;
        if (savedData) {
            this.request = savedData;
            this.searchTermControl.setValue(this.request.searchTerm);
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchTermControl.valueChanges
            .pipe(
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    this.request.searchTerm = term;
                    this.get(true);
                    return of([]);
                })
            )
            .subscribe();

        this.get(true);
    }

    // ===================
    // EVENT HANDLERS
    // ===================

    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.searchTermControl.setValue(null);
    }

    // =========================
    // TABLE CALLBACKS
    // =========================

    // Take over option
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const req = {
            title: 'sidebar.driver_take_over',
            message: 'form.warnings.are_you_sure',
            submitButtonTitle: 'form.actions.ok'
        } as ConfirmationDialogParams;


        this.confirmDialogService
            .confirm(req)
            .pipe(
                switchMap((result: string) => {
                    if (result === 'yes') {
                        this.loadingToTakeOver = true;
                        return this.service.takeOverAuth({ driverID : item.record.id });
                    }
                    // click cancel button
                    return of(null);
                })
            ).subscribe((takeOver: TakeOverResult) => {
                // handle cancel button event
                if (!takeOver) {
                    return;
                }
                // if auth returns an error
                if (takeOver.code === AuthCode.error) {

                    const errorRes = JSON.parse(takeOver.response).error;
                    const errorCode: AuthResponseCodeOptions = AuthResponseCodeOptions[`${errorRes}`];
                    this.loadingToTakeOver = false;
                    switch (errorCode) {
                        case AuthResponseCodeOptions.access_denied: {
                            this.snackBar.open('auth_response.locked_out', 'form.actions.ok');
                            break;
                        }
                        default: {
                            this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                            break;
                        }
                    }
                    return;
                }

                const takeOverDriver: OAuthResult = JSON.parse(takeOver.response);
                const user = this.storeManager.getOption(StoreItems.appUser);
                this.authService.logOut(false, true)
                    .then((res) => {
                        this.authService.afterAuthentication(takeOverDriver)
                                .subscribe((result: AuthResponseCodeOptions) => {
                                    if (result === AuthResponseCodeOptions.authenticated) {
                                        this.storeManager.saveOption(StoreItems.BackToAdminButton, user.fullname);
                                        window.location.href = '/';
                                    }
                                    this.loadingToTakeOver = false;
                                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');

                                },
                                (err) => {
                                    this.loadingToTakeOver = false;
                                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                                    window.setTimeout(() => {
                                        window.location.href = '/';
                                    }, 2500);
                                });
                    })
                    .catch((err) => {
                        this.loadingToTakeOver = false;
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    });
            }, (err) => {
                this.loadingToTakeOver = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });

    }


    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }


    // =========================
    // PRIVATE
    // =========================

    // Fetch events data
    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.drivers = new Array<DriverResultItem>();
        }

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        // first stop currently executing requests
        this.unsubscribeService();

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.driverTakeOver, this.request);

        // then start the new request
        this.serviceSubscriber = this.service
            .get(this.request)
            .subscribe(
                (response: DriversResult) => {
                    this.loading = false;

                    this.drivers = this.drivers.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;

                    if (!this.drivers || this.drivers.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                }
            );
    }

    // =================
    // HELPERS
    // =================

    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                primaryKey: 'name',
                header: 'driver_take_over.users.fullname',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null),
                customClass: (record: DriverResultItem) => {
                    return record.userAccountBlocked ? 'blocked-account' : '';
                }
            },
            {
                primaryKey: 'ownID',
                header: 'driver_take_over.drivers.ID',
                customClass: (record: DriverResultItem) => {
                    return record.userAccountBlocked ? 'blocked-account' : '';
                }
            },
            {
                primaryKey: 'userAccountBlocked',
                header: 'driver_take_over.drivers.account.user.blocked',
                format: TableCellFormatOptions.custom,
                formatFunction: (record: DriverResultItem) => {
                    if (record.userAccountBlocked === true) {
                        return `<div class="blocked-account-icon-wrapper"><div class="blocked-account-icon"></div></div>`;
                    } else {
                        return ' ';
                    }
                }
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.details]
            }
        ];
    }

}
