import { KeyName } from '../common/key-name';
import { PagerResults, SearchTermRequest } from '../common/pager';

export const DriverSessionName: string = 'dn';

export enum AuthCode {
    success = 0,
    error = 1
}

export interface DriversRequest extends SearchTermRequest {
    groupID?: number;
}

export interface DriversResult extends PagerResults {
    results: DriverResultItem[];
}

export interface DriverResultItem extends KeyName {
    groups: string;
    email: string;
    userAccountBlocked: boolean;
}

export interface TakeOverDriverRequest {
    driverID: number;
}

export interface TakeOverResult {
    code: AuthCode;
    response: string;
}
