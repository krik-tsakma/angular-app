﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable, of } from 'rxjs';
import { map, catchError, mergeMap } from 'rxjs/operators';

// SERVICES
import { Config } from '../config/config';
import { AuthHttp } from '../auth/authHttp';
import { OAuthResult, AuthResponseCodeOptions, OAuthRejectResult } from '../auth/authResponse';

// TYPES
import { TakeOverDriverRequest, TakeOverResult } from './driver-take-over-types';
import { DriversResult, DriversRequest } from '../admin/drivers/drivers.types';

@Injectable()
export class DriverTakeOverService {
    private authUser: OAuthResult;
    private baseApiURL: string;
    private baseApiManagerURL: string;
    private baseDriverUserApiUrl: string;

    constructor(private _authHttp: AuthHttp,
                private configService: Config) {
        this.baseApiURL = this.configService.get('apiUrl') + '/api/DriverTakeOver'; 
        this.baseApiManagerURL = this.configService.get('apiUrl') + '/api/BackToManager'; 
        this.baseDriverUserApiUrl = this.configService.get('apiUrl') + '/api/DriversUsers';
               
    }

    // Get all company user roles
    public get(request: DriversRequest): Observable<DriversResult> {

        const params = new HttpParams({
            fromObject: {
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Sorting: request.sorting,
                GroupID: request.groupID ? request.groupID.toString() : ''
            }
        });

        return this._authHttp
            .get(this.baseDriverUserApiUrl, { params });
    }

    public takeOverAuth(req: TakeOverDriverRequest): Observable<TakeOverResult> {

        return this._authHttp
            .post(this.baseApiURL, req);
    }    

    public backToManager(): Observable<TakeOverResult> {
        return this._authHttp
            .post(this.baseApiManagerURL, null);
    }
    
}
