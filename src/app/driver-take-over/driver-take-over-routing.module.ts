// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../auth/authGuard';

// COMPONENTS
import { ViewDriverTakeOverComponent } from './view/view-driver-take-over.component';


const DriverTakeOverRoutes: Routes = [
    {
        path: '',
        component: ViewDriverTakeOverComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(DriverTakeOverRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class DriverTakeOverRoutingModule { }
