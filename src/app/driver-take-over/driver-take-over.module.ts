// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../shared/shared.module';
import { DriverTakeOverRoutingModule } from './driver-take-over-routing.module';
import { AssetGroupsModule } from '../common/asset-groups/asset-groups.module';

// SERVICES
import { DriverTakeOverService } from './driver-take-over.service';
import { DriversService } from '../admin/drivers/drivers.service';
import { TranslateStateService } from '../core/translateStore.service';

// COMPONENTS
import { ViewDriverTakeOverComponent } from './view/view-driver-take-over.component';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/driver-take-over/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}


@NgModule({
    imports: [
        SharedModule,
        DriverTakeOverRoutingModule,
        AssetGroupsModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewDriverTakeOverComponent
    ],
    providers: [
        DriversService,
        DriverTakeOverService
    ]
})
export class DriverTakeOverModule { }
