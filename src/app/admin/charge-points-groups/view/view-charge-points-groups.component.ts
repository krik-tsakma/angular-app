// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { StoreManagerService } from '../../../shared/store-manager/store-manager.service';
import { AdminLabelService } from '../../admin-label.service';
import { ChargePointsGroupsService } from '../charge-points-groups.service';
import { Config } from '../../../config/config';

// TYPES
import { SearchTermRequest, Pager } from '../../../common/pager';
import { 
    TableColumnSetting, TableColumnSortOptions, 
    TableColumnSortOrderOptions, TableActionOptions, 
    TableActionItem, TableCellFormatOptions 
} from '../../../shared/data-table/data-table.types';
import { StoreItems } from '../../../shared/store-manager/store-items';
import { ChargePointsGroupsResult, ChargePointsGroupSessionName, ChargePointsGroupResultItem } from '../charge-points-groups.types';

@Component({
    selector: 'view-charge-points-groups',
    templateUrl: 'view-charge-points-groups.html',
    encapsulation: ViewEncapsulation.None
})

export class ViewChargePointsGroupsComponent extends AdminBaseComponent implements OnInit {
    public groups: ChargePointsGroupResultItem[] = [];
    public searchTermControl = new FormControl();     
    public request: SearchTermRequest = {} as SearchTermRequest;
    public loading: boolean;
    public tableSettings: TableColumnSetting[];
    public pager: Pager = new Pager();
    
    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,        
        protected snackBarService: SnackBarService,        
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,
        protected storeManager: StoreManagerService,
        protected labelService: AdminLabelService,        
        protected configService: Config,
        private service: ChargePointsGroupsService) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBarService, storeManager, confirmDialogService);
        this.labelService.setSessionItemName(ChargePointsGroupSessionName);
        this.tableSettings =  [
            {
                primaryKey: 'name',
                header: 'assets.group.name',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'membersCount',
                header: 'assets.group.members',
                format: TableCellFormatOptions.string,
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete],
                actionsVisibilityFunction: this.showUDButtons
            }
        ];
    }


    public ngOnInit(): void {
        super.ngOnInit();

        const savedData = this.storeManager.getOption(StoreItems.vehileGroups) as SearchTermRequest;
        if (savedData) {
            this.request = savedData;
            this.searchTermControl.setValue(this.request.searchTerm);
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchTermControl.valueChanges
            .pipe(            
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing             
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    this.request.searchTerm = term;
                    this.get(true);
                    return of([]);
                })
            )
            .subscribe(); 

        this.get(true);
    }



    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.searchTermControl.setValue(null);
    }

    
    // ----------------
    // TABLE
    // ----------------

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.edit: 
                this.labelService.set(item.record.name);
                this.manage(id);
                break;
            case TableActionOptions.delete: 
                this.openDialog().subscribe((response) => {
                    if (response === true) {
                        this.delete(id);
                    }
                });
                break;
            default: 
                break;

        }
    }


    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }


    public showUDButtons(action: TableActionOptions, record: ChargePointsGroupResultItem): boolean {
        if (!record || record.isDefault) {
            return false;
        }
       
        return true;
    }

    // ----------------
    // PRIVATE
    // ----------------

    // Fetch Location groups data
    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.groups = new Array<ChargePointsGroupResultItem>();
        }   

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;
   
        // first stop currently executing requests
        this.unsubscribeService();

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.vehileGroups, this.request);
            
        // then start the new request
        this.serviceSubscriber = this.service
            .get(this.request)
            .subscribe(
                (response: ChargePointsGroupsResult) => {
                    this.loading = false;

                    this.groups = this.groups.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;                                          
                    
                    if (!this.groups || this.groups.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }
    
    // ----------------
    // DELETE
    // ----------------
    private delete(id: number) {
        this.loading = true;
        
        // first stop currently executing requests
        this.unsubscribeService();
        
        this.serviceSubscriber = this.service
            .delete(id)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 200) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get(true);
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

}
