
// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { EditChargePointsGroupSettingsComponent } from './edit/settings/edit-charge-points-group-settings.component';
import { ViewChargePointsGroupsComponent } from './view/view-charge-points-groups.component';

const ChargePointsGroupsRoutes: Routes = [
    {
        path: '',
        component: ViewChargePointsGroupsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditChargePointsGroupSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-charge-points-group.module').then((m) => m.EditChargePointsGroupModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }   
];

@NgModule({
    imports: [
        RouterModule.forChild(ChargePointsGroupsRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class ChargePointsGroupsRoutingModule { }
