// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RxJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { 
    ChargePointsGroupsResult, 
    ChargePointsGroupSettings, 
    ChargePointsGroupCreateCodeOptions, 
    ChargePointsGroupSettingsCodeOptions, 
    ChargePointsGroupAddMemberCodeOptions, 
    ChargePointsGroupRemoveMemberCodeOptions, 
    ChargePointsGroupAuthorizeUserCodeOptions, 
    ChargePointsGroupUnauthorizeUserCodeOptions 
} from './charge-points-groups.types';
import { SearchTermRequest } from '../../common/pager';
import { AssetGroupMembersRequest, AssetGroupMembersResult, AssetGroupMemberAddRemoveRequest } from '../asset-groups/asset-groups.types';

@Injectable()
export class ChargePointsGroupsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/ChargePointsGroups';
        }

        // Get all location groups
        public get(request: SearchTermRequest): Observable<ChargePointsGroupsResult> {
        
            const params = new HttpParams({
                fromObject: {
                    SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                    PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                    PageSize: request.pageSize ? request.pageSize.toString() : '20',
                    Sorting: request.sorting
                }
            });

            return this.authHttp
                .get(this.baseURL, { params });
        }


        public getByID(id: number): Observable<ChargePointsGroupSettings> {
            return this.authHttp
                .get(this.baseURL + '/' + id);
        }


        public create(entity: ChargePointsGroupSettings): Observable<ChargePointsGroupCreateCodeOptions>  {
            return this.authHttp
                .post(this.baseURL, entity, { observe: 'response' })
                .pipe(
                    map((res) => {
                        if (res.status === 201) {
                            return null;
                        } else  {
                            return res.body as ChargePointsGroupCreateCodeOptions;
                        }
                    })
                );
        }


        public update(entity: ChargePointsGroupSettings): Observable<ChargePointsGroupSettingsCodeOptions>  {
            return this.authHttp
                .put(this.baseURL, entity, { observe: 'response' })
                .pipe(
                    map((res) => {
                        // this is means empty Ok response from the server
                        if (res.status === 200 && !res.body) {
                            return null;
                        } else  {
                            return res.body as ChargePointsGroupSettingsCodeOptions;
                        }
                    })
                );            
        }

        public delete(id: number): Observable<number> {
            return this.authHttp
                .delete(this.baseURL + '/' + id, { observe: 'response' })
                .pipe(
                     map((r) => r.status as number)
                ); 
        }

    // ====================
    // MEMBERS
    // ====================

    public getMembers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20'
            }
        });

        return this.authHttp.get(this.baseURL + '/GetMembers', { params });
    }

    public getNonMembers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20'
            }
        });

        return this.authHttp.get(this.baseURL + '/GetNonMembers', { params });
    }


    public addMember(entity: AssetGroupMemberAddRemoveRequest): Observable<ChargePointsGroupAddMemberCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/AddMember', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ChargePointsGroupAddMemberCodeOptions;
                    }
                })
            );            
    }

    public removeMember(entity: AssetGroupMemberAddRemoveRequest): Observable<ChargePointsGroupRemoveMemberCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/RemoveMember', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ChargePointsGroupRemoveMemberCodeOptions;
                    }
                })
            );            
    }


    // ====================
    // AUTHORIZED USERS
    // ====================

    public getAuthorizedUsers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20'
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetAuthorizedUsers', { params });
    }


    public authorizeUser(entity: AssetGroupMemberAddRemoveRequest): Observable<ChargePointsGroupAuthorizeUserCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/AuthorizeUser', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ChargePointsGroupAuthorizeUserCodeOptions;
                    }
                })
            );            
    }

    public getUnauthorizedUsers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetUnauthorizedUsers', { params });
    }


    public unauthorizeUser(entity: AssetGroupMemberAddRemoveRequest): Observable<ChargePointsGroupUnauthorizeUserCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/UnauthorizeUser', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ChargePointsGroupUnauthorizeUserCodeOptions;
                    }
                })
            );            
    }
} 
