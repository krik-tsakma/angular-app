﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';

// COMPONENTS
import { EditChargePointsGroupComponent } from './edit-charge-points-group.component';
import { EditChargePointsGroupSettingsComponent } from './settings/edit-charge-points-group-settings.component';
import { EditChargePointsGroupMembersComponent } from './members/edit-charge-points-group-members.component';
import { EditChargePointsGroupAuthorizedUsersComponent } from './authorized-users/edit-charge-points-group-authorized-users.component';

const EditChargePointsGroupRoutes: Routes = [
    {
        path: '',
        component: EditChargePointsGroupComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'settings',
        component: EditChargePointsGroupSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.settings'
        }
    },
    {
        path: 'members',
        component: EditChargePointsGroupMembersComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.membership'
        }
    },
    {
        path: 'authorized-users',
        component: EditChargePointsGroupAuthorizedUsersComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.access'
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(EditChargePointsGroupRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class EditChargePointsGroupRoutingModule { }
