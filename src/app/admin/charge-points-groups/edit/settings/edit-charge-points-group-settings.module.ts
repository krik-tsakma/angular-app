﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../../shared/shared.module';

// COMPONENTS
import { EditChargePointsGroupSettingsComponent } from './edit-charge-points-group-settings.component';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        EditChargePointsGroupSettingsComponent
    ],
    exports: [
        EditChargePointsGroupSettingsComponent
    ]
})

export class EditChargePointsGroupSettingsModule { }
