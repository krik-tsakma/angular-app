// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, Renderer } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { AdminLabelService } from '../../../admin-label.service';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { ChargePointsGroupsService } from '../../charge-points-groups.service';
import { Config } from '../../../../config/config';

// TYPES
import { KeyName } from '../../../../common/key-name';
import { TableColumnSetting, TableActionOptions, TableColumnSortOrderOptions, TableColumnSortOptions, TableActionItem } from '../../../../shared/data-table/data-table.types';
import { SearchType } from '../../../../shared/search-mechanism/search-mechanism-types';
import { AssetGroupMembersRequest, AssetGroupMembersResult, AssetGroupMemberAddRemoveRequest } from '../../../asset-groups/asset-groups.types';
import { ChargePointsGroupSessionName, ChargePointsGroupAddMemberCodeOptions, ChargePointsGroupRemoveMemberCodeOptions } from '../../charge-points-groups.types';
import { ConfirmationDialogParams } from '../../../../shared/confirm-dialog/confirm-dialog-types';
import { Pager } from '../../../../common/pager';

@Component({
    selector: 'edit-charge-points-group-members',
    templateUrl: 'edit-charge-points-group-members.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditChargePointsGroupMembersComponent extends AdminBaseComponent implements OnInit {
    public loading: boolean;
    public loadingNonMembersCps: boolean;

    public members: KeyName[] = [];
    public cpsNonMembers: KeyName[] = [];
    
    public pager: Pager = new Pager();
    public request: AssetGroupMembersRequest = {} as AssetGroupMembersRequest;
    public tableSettings: TableColumnSetting[];
    public searchTypes: any = SearchType;
    public memberToAdd: string;

    @ViewChild('wrapper', { static: false }) public wrapperElm: ElementRef;
    
    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected confirmDialogService: ConfirmDialogService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: ChargePointsGroupsService,
        private renderer: Renderer
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar, null, confirmDialogService);
        labelService.setSessionItemName(ChargePointsGroupSessionName);

        this.tableSettings =  [
            {
                primaryKey: 'name',
                header: 'assets.group.name',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'id',
                header: ' ',
                actions: [TableActionOptions.delete]
            }
        ];
    }


    // =========================
    // LIFECYCLE
    // =========================

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
                    this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
                    this.request.id = params.id;
                    this.get(true);
                }
            });
    }

    // =========================
    // TABLE
    // =========================

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }
        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.delete: 
                const req = {
                    title: 'assets.group.members.remove_member',
                    message: 'form.warnings.are_you_sure',
                    submitButtonTitle: 'form.actions.continue'
                } as ConfirmationDialogParams;
                this.openDialog(req).subscribe((response) => {
                    if (response === true) {
                        this.onRemoveMember(id);
                    }
                });
                break;
            default: 
                break;

        }
    }

    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }


    // Fetch cps that are not already members of the group
    public getCpGroupNonMembers(term: string) {
        if (!term || term.length < 2) {
            return;
        }

        this.message = '';
        this.loadingNonMembersCps = true;
        this.cpsNonMembers = new Array<KeyName>();

        // first stop currently executing requests
        this.unsubscribeService();

        const req = {
            id: this.request.id,
            sorting: this.request.sorting,
            searchTerm: term
        } as AssetGroupMembersRequest;
        this.serviceSubscriber = this.service
            .getNonMembers(req)
            .subscribe(
                (response: AssetGroupMembersResult) => {
                    this.loadingNonMembersCps = false;
                    this.cpsNonMembers = response.results;

                    if (!this.cpsNonMembers || this.cpsNonMembers.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loadingNonMembersCps = false;
                    this.message = 'data.error';
                }
            );
    }


    public onAddMember(cp: KeyName) {
        if (!cp || !cp.id) {
            return;
        }
        
        this.loading = true;
        this.unsubscribeService();

        const req = {
            memberID: cp.id,
            groupID: this.request.id
        } as AssetGroupMemberAddRemoveRequest;

        this.memberToAdd = null;
        this.focusOutFromAutocomplete();

        this.serviceSubscriber = this.service
            .addMember(req)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('assets.group.members.add_member.success', 'form.actions.close');
                    this.get(true);
                } else {
                    const message = this.getAddMemberResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // =========================
    // PRIVATE
    // =========================

    private onRemoveMember(memberID: number) {
        this.loading = true;
        this.unsubscribeService();

        const req = {
            memberID: memberID,
            groupID: this.request.id
        } as AssetGroupMemberAddRemoveRequest;
        
        this.serviceSubscriber = this.service
            .removeMember(req)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('assets.group.members.remove_member.success', 'form.actions.close');
                    this.get(true);
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // Fetch location groups
    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.members = new Array<KeyName>();
        }     
        
        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        // first stop currently executing requests
        this.unsubscribeService();

        // then start the new request
        this.serviceSubscriber = this.service
            .getMembers(this.request)
            .subscribe(
                (response: AssetGroupMembersResult) => {
                    this.loading = false;
                    this.members = this.members.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;         

                    if (!this.members || this.members.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }

    private focusOutFromAutocomplete() {
        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.wrapperElm.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }


    // =========================
    // RESULTS
    // =========================

    private getAddMemberResult(code: ChargePointsGroupAddMemberCodeOptions): string {
        let message = '';
        switch (code) {
            case ChargePointsGroupAddMemberCodeOptions.LocationAlreadyAMember:
                message = 'assets.group.members.add_member.error';
                break;
            case ChargePointsGroupAddMemberCodeOptions.LocationGroupNotFound:
            case ChargePointsGroupAddMemberCodeOptions.LocationNotFound:
                message = 'form.errors.not_found';
                break;
            case ChargePointsGroupAddMemberCodeOptions.IssuerNotFound:
            case ChargePointsGroupAddMemberCodeOptions.NotAllowedLocation:
            case ChargePointsGroupAddMemberCodeOptions.NotAllowedLocationGroup:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getRemoveMemberResult(code: ChargePointsGroupRemoveMemberCodeOptions): string {
        let message = '';
        switch (code) {
            case ChargePointsGroupRemoveMemberCodeOptions.LocationNotAMember:
                message = 'assets.group.members.remove_member.error';
                break;
            case ChargePointsGroupRemoveMemberCodeOptions.LocationGroupNotFound:
                message = 'form.errors.not_found';
                break;
            case ChargePointsGroupRemoveMemberCodeOptions.IssuerNotFound:
            case ChargePointsGroupRemoveMemberCodeOptions.NotAllowedLocation:
            case ChargePointsGroupRemoveMemberCodeOptions.NotAllowedLocationGroup:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
