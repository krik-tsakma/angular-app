
// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';
import { EditChargePointsGroupRoutingModule } from './edit-charge-points-group-routing.module';
import { EditChargePointsGroupSettingsModule } from './settings/edit-charge-points-group-settings.module';
import { AssetGroupsModule } from '../../../common/asset-groups/asset-groups.module';

// COMPONENTS
import { EditChargePointsGroupComponent } from './edit-charge-points-group.component';
import { EditChargePointsGroupMembersComponent } from './members/edit-charge-points-group-members.component';
import { EditChargePointsGroupAuthorizedUsersComponent } from './authorized-users/edit-charge-points-group-authorized-users.component';

// SERVICES
import { ChargePointsGroupsService } from '../charge-points-groups.service';

@NgModule({
    imports: [
        SharedModule,
        EditChargePointsGroupRoutingModule,
        EditChargePointsGroupSettingsModule,
        SettingsListModule,
        AssetGroupsModule,
    ],
    declarations: [
        EditChargePointsGroupComponent,
        EditChargePointsGroupMembersComponent,
        EditChargePointsGroupAuthorizedUsersComponent
    ],
    providers: [
        ChargePointsGroupsService
    ]
})
export class EditChargePointsGroupModule { }
