import { KeyName } from './../../common/key-name';
import { PagerResults } from '../../common/pager';

export const ChargePointsGroupSessionName: string = 'cpgn';

export interface ChargePointsGroupsResult extends PagerResults {
    results: ChargePointsGroupResultItem[];
}

export interface ChargePointsGroupResultItem extends KeyName {
    isDefault?: boolean;
    isUnlinked?: boolean;
}

// ====================
// BASIC INFO
// ====================

export interface ChargePointsGroupSettings {
    id: number;
    name: string;
    description: string;
}

export enum ChargePointsGroupCreateCodeOptions {
    Created = 0,
    NameAlreadyExists = 1,
    IssuerNotFound = 2,
    UnknownError = 10,
}

export enum ChargePointsGroupSettingsCodeOptions {
    Set = 0,
    NoSuchGroupExists = 1,
    NameAlreadyExists = 2,
    IssuerNotFound = 3,
    NotAllowedLocationGroup = 4,
    UnknownError = 10,
}

// ====================
// MEMBERS
// ===================

export enum ChargePointsGroupAddMemberCodeOptions {
    Added = 0,
    LocationGroupNotFound = 1,
    LocationGroupIsDefault = 2,
    LocationNotFound = 3,
    LocationAlreadyAMember = 4,
    IssuerNotFound = 5,
    NotAllowedLocation = 6,
    NotAllowedLocationGroup = 7,
    UnknownError = 20
}


export enum ChargePointsGroupRemoveMemberCodeOptions {
    Removed = 0,
    LocationGroupNotFound = 1,
    LocationNotAMember = 2,
    IssuerNotFound = 3,
    NotAllowedLocation = 4,
    NotAllowedLocationGroup = 5,
    UnknownError = 10
}

// ====================
// AUTHORIZED USERS
// ===================

export enum ChargePointsGroupAuthorizeUserCodeOptions {
    Added = 0,
    LocationGroupNotFound = 1,
    UserNotFound = 2,
    UserAuthorized = 3,
    IssuerNotFound = 4,
    NotAllowedLocation = 5,
    NotAllowedLocationGroup = 6,
    UnknownError = 10
}

export enum ChargePointsGroupUnauthorizeUserCodeOptions {
    Removed = 0,
    LocationGroupNotFound = 1,
    UserNotFound = 2,
    UserNotAuthorized = 3,
    IssuerNotFound = 4,
    NotAllowedLocationGroup = 5,
    UnknownError = 9
}
