// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { SettingsListModule } from '../settings-list/settings-list.module';
import { EditChargePointsGroupSettingsModule } from './edit/settings/edit-charge-points-group-settings.module';
import { ChargePointsGroupsRoutingModule } from './charge-points-groups-routing.module';

// COMPONENTS
import { ViewChargePointsGroupsComponent } from './view/view-charge-points-groups.component';

// SERVICES
import { ChargePointsGroupsService } from './charge-points-groups.service';

@NgModule({
    imports: [
        SharedModule,
        ChargePointsGroupsRoutingModule,
        EditChargePointsGroupSettingsModule,
        SettingsListModule,
    ],
    declarations: [
        ViewChargePointsGroupsComponent
    ],
    providers: [
        ChargePointsGroupsService
    ]
})
export class ChargePointsGroupsModule { }
