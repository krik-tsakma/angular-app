
// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';
import { FileUploadResult } from '../../common/file-upload-types';
import { 
    VehicleTypeUpdateBookNGoResultCodeOptions, 
    VehicleTypeBEVSettings, 
    VehicleTypeUpdateBEVResultCodeOptions, 
    VehicleTypeBookNGoSettings, 
    VehicleTypeSettings, 
    VehicleTypesResult,
    VehicleTypeCreateCodeOptions,
    VehicleTypeUpdateSettingsCodeOptions,
    VehicleTypeRemoveCodeOptions
} from './vehicle-types.types';
import { SearchTermRequest } from '../../common/pager';

// TYPES 

@Injectable()
export class VehicleTypesService {
    private translations: any;
    private baseURL: string;
    
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = config.get('apiUrl') + '/api/VehicleTypesAdmin';
        }


     // Get all vehicle groups
     public get(request: SearchTermRequest): Observable<VehicleTypesResult> {

        const obj = this.authHttp.removeObjNullKeys({
            SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
            PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
            PageSize: request.pageSize ? request.pageSize.toString() : '20',
            Sorting: request.sorting
        });

        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }


    // ====================
    // BASIC INFO
    // ====================

    public getByID(id: number): Observable<VehicleTypeSettings> {
        return this.authHttp
            .get(this.baseURL + '/' + id);
    }


    public create(entity: VehicleTypeSettings): Observable<VehicleTypeCreateCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as VehicleTypeCreateCodeOptions;
                    }
                })
            );
    }


    public update(entity: VehicleTypeSettings): Observable<VehicleTypeUpdateSettingsCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleTypeUpdateSettingsCodeOptions;
                    }
                })
            );            
    }

    public delete(id: number): Observable<VehicleTypeRemoveCodeOptions> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleTypeRemoveCodeOptions;
                    }
                })        
            ); 
    }

    // ====================
    // BOOk N GO
    // ====================

    public getBookNGoSettingsByID(id: number): Observable<VehicleTypeBookNGoSettings> {
        return this.authHttp
            .get(this.baseURL + '/BookNGo/' + id);
    }

    public updateBookNGoSettings(entity: VehicleTypeBEVSettings): Observable<VehicleTypeUpdateBookNGoResultCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/BookNGo/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleTypeUpdateBookNGoResultCodeOptions;
                    }
                })
            );            
    }

    // Delete method 
    public uploadEvImage(fileToUpload: any, vehicleTypeID: number): Observable<FileUploadResult> {
        const input = new FormData();
        input.append('File', fileToUpload);
        input.append('VehicleTypeID', vehicleTypeID.toString());
        
        return this.authHttp
            .post(this.baseURL + '/UploadEvImage', input);
    }

     // Delete method 
     public removeEvImage(id: number): Observable<number>  {
        return this.authHttp
            .delete(this.baseURL + '/RemoveEvImage/' + id, { observe: 'response' })
            .pipe(
                map((res) => res.status)                
            );
    }

    // ====================
    // BEV
    // ====================

    public getBEVSettingsByID(id: number): Observable<VehicleTypeBEVSettings> {
        return this.authHttp
            .get(this.baseURL + '/BEV/' + id);
    }


    public updateBEVSettings(entity: VehicleTypeBEVSettings): Observable<VehicleTypeUpdateBEVResultCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/BEV/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleTypeUpdateBEVResultCodeOptions;
                    }
                })
            );            
    }
}
