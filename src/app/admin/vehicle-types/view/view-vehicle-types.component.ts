// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { VehicleTypesService } from '../vehicle-types.service';
import { StoreManagerService } from '../../../shared/store-manager/store-manager.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { AdminLabelService } from '../../admin-label.service';
import { ImportExportDialogService } from '../../import-export/import-export-dialog.service';
import { Config } from '../../../config/config';

// TYPES
import {
    TableColumnSetting, TableActionItem, TableColumnSortOrderOptions,
    TableActionOptions, TableCellFormatOptions, TableColumnSortOptions
} from '../../../shared/data-table/data-table.types';
import { StoreItems } from '../../../shared/store-manager/store-items';
import {
    VehicleTypeResultItem,
    VehicleTypesResult,
    VehicleTypeEVTypesOptions,
    VehicleTypeSessionName,
    VehicleTypeRemoveCodeOptions
} from '../vehicle-types.types';
import { SearchTermRequest, Pager } from '../../../common/pager';
import { MoreOptionsMenuOptions, MoreOptionsMenuTypes, MoreOptionsMenuOption } from '../../../shared/more-options-menu/more-options-menu.types';
import { ImportExportTypeOptions } from '../../import-export/import-export.types';

@Component({
    selector: 'view-vehicle-types',
    templateUrl: 'view-vehicle-types.html',
    encapsulation: ViewEncapsulation.None
})

export class ViewVehicleTypesComponent extends AdminBaseComponent implements OnInit {
    public vehicleTypes: VehicleTypeResultItem[] = [];
    public searchTermControl = new FormControl();
    public request: SearchTermRequest = {} as SearchTermRequest;
    public loading: boolean;
    public tableSettings: TableColumnSetting[];
    public pager: Pager = new Pager();
    public pageOptions: MoreOptionsMenuOptions[] = [
        {
            term: 'assets.vehicle_type',
            options: [
                {
                    id: 'add',
                    term: 'form.actions.create',
                    type: MoreOptionsMenuTypes.none,
                    enabled: true
                }
            ]
        },
        {
            term: 'import_export.title',
            options: [
                {
                    id: ImportExportTypeOptions.VehicleTypeEnergyTargetsBaselines,
                    term: 'energy_category.target_baselines',
                    type: MoreOptionsMenuTypes.none,
                    enabled: true
                },
                {
                    id: ImportExportTypeOptions.VehicleTypeEnergyCorrectionFactors,
                    term: 'energy_category.correction_factors',
                    type: MoreOptionsMenuTypes.none,
                    enabled: true
                }
            ]
        }
    ];
    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translate: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBarService: SnackBarService,
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,
        protected storeManager: StoreManagerService,
        protected configService: Config,
        private service: VehicleTypesService,
        private importExportDialog: ImportExportDialogService,
    ) {
        super(translate, previousRouteService, router, unsubscriber, labelService,
            configService, snackBarService, storeManager, confirmDialogService);
        labelService.setSessionItemName(VehicleTypeSessionName);

        this.tableSettings =  [
            {
                primaryKey: 'name',
                header: 'vehicle_types.name',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'evType',
                header: 'vehicle_types.ev_type',
                format: TableCellFormatOptions.custom,
                formatFunction: (record: VehicleTypeResultItem) => {
                    if (record === undefined || record === null) {
                        return ' ';
                    }
                    return VehicleTypeEVTypesOptions.find((x) => x.id === record.evType).name;
                }
            },
            {
                primaryKey: 'year',
                header: 'vehicle_types.year',
                format: TableCellFormatOptions.string
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete]
            }
        ];
    }


    public ngOnInit(): void {
        super.ngOnInit();

        const savedData = this.storeManager.getOption(StoreItems.vehicleTypes) as SearchTermRequest;
        if (savedData) {
            this.request = savedData;
            this.searchTermControl.setValue(this.request.searchTerm);
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchTermControl.valueChanges
            .pipe(
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    this.request.searchTerm = term;
                    this.get(true);
                    return of([]);
                })
            )
            .subscribe();

        VehicleTypeEVTypesOptions.forEach((type) => {
            this.translate.get(type.term).subscribe((t) => {
                type.name = t;
            });
        });

        this.get(true);
    }

    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.searchTermControl.setValue(null);
    }


    public onSelectPageOption(item: MoreOptionsMenuOption): void {
        if (item.id === 'add') {
            this.manage();
        } else {
            this.importExportDialog
                .openDialog({
                    title: item.term,
                    type: item.id
                });
        }
    }


    // ----------------
    // TABLE
    // ----------------

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const record = item.record as VehicleTypeResultItem;
        switch (item.action) {
            case TableActionOptions.edit:
                this.manage(record.id, record.name);
                break;
            case TableActionOptions.delete:
                this.openDialog().subscribe((response) => {
                    if (response === true) {
                       this.delete(record.id);
                    }
                });
                break;
            default:
                break;

        }
    }


    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }


    // ----------------
    // PRIVATE
    // ----------------

    // Fetch vehicle types data
    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.vehicleTypes = new Array<VehicleTypeResultItem>();
        }

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        // first stop currently executing requests
        this.unsubscribeService();

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.vehicleTypes, this.request);

        // then start the new request
        this.serviceSubscriber = this.service
            .get(this.request)
            .subscribe(
                (response: VehicleTypesResult) => {
                    this.loading = false;

                    this.vehicleTypes = this.vehicleTypes.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;

                    if (!this.vehicleTypes || this.vehicleTypes.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }


    // ----------------
    // DELETE
    // ----------------

    private delete(id: number) {
        this.loading = true;

        // first stop currently executing requests
        this.unsubscribeService();

        this.serviceSubscriber = this.service
            .delete(id)
            .subscribe((res: VehicleTypeRemoveCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get(true);
                } else {
                    const errorMessage = this.getRemoveResult(res);
                    this.snackBar.open(errorMessage, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    private getRemoveResult(code: VehicleTypeRemoveCodeOptions): string {
        let message = '';
        switch (code) {
            case VehicleTypeRemoveCodeOptions.NotFound:
                message = 'form.errors.not_found';
                break;
            case VehicleTypeRemoveCodeOptions.AssociatedToVehicles:
                message = 'vehicle_types.errors.associated_with_vehicle';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

}
