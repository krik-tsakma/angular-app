import { KeyName } from '../../common/key-name';
import { PagerResults } from '../../common/pager';
import { EnergyUnitOptions } from '../../common/energy-types/energy-types';

export const VehicleTypeSessionName: string = 'vtn';

export interface VehicleTypesResult extends PagerResults {
    results: VehicleTypeResultItem[];
}

export interface VehicleTypeResultItem {
    id: number;
    name: string;
    year?: number;
    evType: VehicleTypeEVTypes;
}


// ====================
// BASIC INFO
// ====================

export interface VehicleTypeSettings {
    id: number;
    name: string;
    year?: number;
    model?: string;
    type?: string;
    evType: VehicleTypeEVTypes;
    energyUnit: EnergyUnitOptions;
    primaryEnergyTypeID?: number;
    secondaryEnergyTypeID?: number;
}


export enum VehicleTypeCreateCodeOptions {
    Created = 0,
    NotFound = 1,
    NameEmpty = 2,
    NameAlreadyExists = 3,
    UnknownError = 20,
}

export enum VehicleTypeUpdateSettingsCodeOptions {
    Set = 0,
    NotFound = 1,
    NameEmpty = 2,
    NameAlreadyExists = 3,
    UnknownError = 20,
}


export enum VehicleTypeRemoveCodeOptions {
    Removed = 0,
    NotFound = 1,
    AssociatedToVehicles = 2,
    UnknownError = 10,
}

// ====================
// BEV
// ====================

export interface VehicleTypeBEVSettings {
    id: number;
    batteryNetCapacity?: number;
    batteryGrossCapacity?: number;
    maxMotorPower?: number;
    batterySoCClasificationLow?: number;
    batterySoCClasificationHigh?: number;
}

export enum VehicleTypeUpdateBEVResultCodeOptions {
    Updated = 0,
    NoSuchVehicleTypeExists = 1,
    SoCClassificationInvalid = 2
}

// ====================
// BOOK N GO
// ====================

export interface VehicleTypeBookNGoSettings {
    id: number;
    typicalEconomy?: number;
    preBookingChargingMarginHours?: number;
    preBookingChargingMarginMinutes?: number;
}

export enum VehicleTypeUpdateBookNGoResultCodeOptions {
    Updated = 0,
    NoSuchVehicleTypeExists = 1,
}

// UPLOAD IMAGE
export enum VehicleTypeUploadImageResultCodeOptions {
    Ok = 0,
    NotFound = 1,
    Unknown
}

// ====================
// ENUMS
// ====================

export enum VehicleTypeEVTypes {
    None = 0,
    HybridEV,
    PluggableHybridEV,
    BatteryEV
}


export let VehicleTypeEVTypesOptions: KeyName[] = [
    {
        id: VehicleTypeEVTypes.None,
        name: 'none',
        term: 'vehicle_types.ev_type.none',
        enabled: true
    },
    {
        id: VehicleTypeEVTypes.HybridEV,
        name: 'HybridEV',
        term: 'vehicle_types.ev_type.hybridEV',
        enabled: true
    },
    {
        id: VehicleTypeEVTypes.PluggableHybridEV,
        name: 'PluggableHybridEV',
        term: 'vehicle_types.ev_type.pluggableHybridEV',
        enabled: true
    },
    {
        id: VehicleTypeEVTypes.BatteryEV,
        name: 'BatteryEV',
        term: 'vehicle_types.ev_type.batteryEV',
        enabled: true
    }
];
