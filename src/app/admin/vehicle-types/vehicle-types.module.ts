// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { VehicleTypesRoutingModule } from './vehicle-types-routing.module';
import { EditVehicleTypeSettingsModule } from './edit/settings/edit-vehicle-type-settings.module';
import { ImportExportModule } from '../import-export/import-export.module';

// COMPONENTS
import { ViewVehicleTypesComponent } from './view/view-vehicle-types.component';

// SERVICES
import { VehicleTypesService } from './vehicle-types.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/vehicles-types/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        HttpClientJsonpModule,
        SharedModule,
        VehicleTypesRoutingModule,
        EditVehicleTypeSettingsModule,
        ImportExportModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewVehicleTypesComponent,
    ],
    entryComponents: [
    ],
    providers: [
        VehicleTypesService,

    ]
})

export class VehicleTypesModule { }
