// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { AdminLabelService } from '../../../admin-label.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { VehicleTypesService } from '../../vehicle-types.service';
import { Config } from '../../../../config/config';

// TYPES
import { 
    VehicleTypeSettings, 
    VehicleTypeSessionName, 
    VehicleTypeUpdateSettingsCodeOptions, 
    VehicleTypeCreateCodeOptions,
    VehicleTypeEVTypes
} from '../../vehicle-types.types';

@Component({
    selector: 'edit-vehicle-type-settings',
    templateUrl: 'edit-vehicle-type-settings.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-vehicle-type-settings.less']
})

export class EditVehicleTypeSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public vehicleType: VehicleTypeSettings = {} as VehicleTypeSettings;
    public evTypes: any = VehicleTypeEVTypes;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected router: Router,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: VehicleTypesService
        
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(VehicleTypeSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.vehicleType.evType = VehicleTypeEVTypes.None;
        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                this.action = params.id
                            ? 'form.actions.edit'
                            : 'form.actions.create';
                if (params.id) {
                    this.get(params.id);
                }
            });
    }


    public showNoneOptionForPrimaryEnergyType(): boolean {
        return this.vehicleType.id > 0 && this.vehicleType.primaryEnergyTypeID === null;
    }

    public onSubmit({ value, valid }: { value: VehicleTypeSettings, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is create
        if (typeof(this.vehicleType.id) === 'undefined' || this.vehicleType.id === 0) {
            this.serviceSubscriber = this.service
                .create(this.vehicleType)
                .subscribe((res: VehicleTypeCreateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {    
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const errorMessage = this.getCreateResult(res);
                        this.snackBar.open(errorMessage, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else { 
        // This is an update
        this.serviceSubscriber = this.service
            .update(this.vehicleType)
            .subscribe((res: VehicleTypeUpdateSettingsCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.labelService.set(this.vehicleType.name);
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    const errorMessage = this.getUpdateResult(res);
                    this.snackBar.open(errorMessage, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
        }
    }


    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((res: VehicleTypeSettings) => {
                this.loading = false;
                this.vehicleType = res;
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }


    // =========================
    // RESULTS
    // =========================
    private getCreateResult(code: VehicleTypeCreateCodeOptions): string {
        let message = '';
        switch (code) {
            case VehicleTypeCreateCodeOptions.NameAlreadyExists:
                message = 'form.errors.name_exists';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUpdateResult(code: VehicleTypeUpdateSettingsCodeOptions): string {
        let message = '';
        switch (code) {
            case VehicleTypeUpdateSettingsCodeOptions.NameAlreadyExists:
                message = 'form.errors.name_exists';
                break;
            case VehicleTypeUpdateSettingsCodeOptions.NotFound:
                message = 'form.errors.not_found';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
