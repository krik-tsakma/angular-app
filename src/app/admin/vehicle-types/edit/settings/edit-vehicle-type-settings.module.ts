﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../../shared/shared.module';
import { EnergyTypesListModule } from '../../../../common/energy-types/energy-types-list.module';

// COMPONENTS
import { EditVehicleTypeSettingsComponent } from './edit-vehicle-type-settings.component';

@NgModule({
    imports: [
        SharedModule,
        EnergyTypesListModule
    ],
    declarations: [
        EditVehicleTypeSettingsComponent
    ],
    exports: [
        EditVehicleTypeSettingsComponent
    ]
})

export class EditVehicleTypeSettingsModule { }
