// FRAMEWORK
import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { AdminLabelService } from '../../../admin-label.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { Config } from '../../../../config/config';

// TYPES
import { AdminBaseComponent } from '../../../admin-base.component';
import { VehicleTypeSessionName } from '../../vehicle-types.types';
import { EnergyTargetBaselinesComponent } from '../../../../common/energy-target-baselines/energy-target-baselines.component';


@Component({
    selector: 'edit-vehicle-type-target-baselines',
    templateUrl: 'edit-target-baselines.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditVehicleTypeTargetBaselinesComponent extends AdminBaseComponent implements OnInit {
    @ViewChild(EnergyTargetBaselinesComponent, { static: true }) public targetBaselinesComponent: EnergyTargetBaselinesComponent;

    public loading: boolean;
    public vehicleTypeID: number;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translate: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected router: Router,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute
    ) {
        super(translate, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(VehicleTypeSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.vehicleTypeID = params.id;
                }
            });
    }

    public addFactor() {
        this.targetBaselinesComponent.onAdd();
    }
}
