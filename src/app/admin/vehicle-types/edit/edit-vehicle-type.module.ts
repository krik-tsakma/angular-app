﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { EditVehicleTypesRoutingModule } from './edit-vehicle-type-routing.module';
import { TimePickerModule } from '../../../common/timepicker/timepicker.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';
import { EditVehicleTypeSettingsModule } from './settings/edit-vehicle-type-settings.module';
import { EnergyCorrectionFactorsModule } from '../../../common/energy-correction-factors/energy-correction-factors.module';
import { EnergyTargetBaselinesModule } from '../../../common/energy-target-baselines/energy-target-baselines.module';

// COMPONENTS
import { EditVehicleTypeComponent } from './edit-vehicle-type.component';
import { EditVehicleTypeBEVSettingsComponent } from './bev/edit-bev-settings.component';
import { EditVehicleTypeBookNGoSettingsComponent } from './book-n-go/edit-bookngo-settings.component';
import { EditVehicleTypeCorrectionFactorsComponent } from './correction-factors/edit-correction-factors.component';
import { EditVehicleTypeTargetBaselinesComponent } from './target-baselines/edit-target-baselines.component';

// SERVICES
import { VehicleTypesService } from '../vehicle-types.service';

@NgModule({
    imports: [
        HttpClientJsonpModule,
        SharedModule,
        EditVehicleTypesRoutingModule,
        TimePickerModule,
        SettingsListModule,
        EditVehicleTypeSettingsModule,
        EnergyCorrectionFactorsModule,
        EnergyTargetBaselinesModule
    ],
    declarations: [
        EditVehicleTypeComponent,
        EditVehicleTypeBEVSettingsComponent,
        EditVehicleTypeBookNGoSettingsComponent,
        EditVehicleTypeCorrectionFactorsComponent,
        EditVehicleTypeTargetBaselinesComponent
    ],
    providers: [
        VehicleTypesService
    ]
})

export class EditVehicleTypesModule { }
