
// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { AdminLabelService } from '../../../admin-label.service';
import { VehicleTypesService } from '../../vehicle-types.service';
import { Config } from '../../../../config/config';

// TYPES
import { VehicleTypeBEVSettings, VehicleTypeUpdateBEVResultCodeOptions, VehicleTypeSessionName } from '../../vehicle-types.types';

@Component({
    selector: 'edit-vehicle-type-bev-settings',
    templateUrl: 'edit-bev-settings.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditVehicleTypeBEVSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public vehicleType: VehicleTypeBEVSettings = {} as VehicleTypeBEVSettings;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translate: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected router: Router,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: VehicleTypesService
    ) {
        super(translate, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(VehicleTypeSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.get(params.id);
                }
            });
    }

    public onSubmit({ value, valid }: { value: VehicleTypeBEVSettings, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }

        this.serviceSubscriber = this.service
            .updateBEVSettings(this.vehicleType)
            .subscribe((res: VehicleTypeUpdateBEVResultCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    const message = this.getUpdateResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    // =========================
    // DATA
    // =========================
    
    private get(id: number) {
        this.loading = true;
        this.service
            .getBEVSettingsByID(id)
            .subscribe((res: VehicleTypeBEVSettings) => {
                this.loading = false;
                this.vehicleType = res;
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }

    // =========================
    // RESULTS
    // =========================

    private getUpdateResult(code: VehicleTypeUpdateBEVResultCodeOptions): string {
        let message: string;
        switch (code) {
            case VehicleTypeUpdateBEVResultCodeOptions.NoSuchVehicleTypeExists:
                message = 'form.errors.not_found';
                break;
            case VehicleTypeUpdateBEVResultCodeOptions.SoCClassificationInvalid:
                message = 'form.errors.name_exists';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

}
