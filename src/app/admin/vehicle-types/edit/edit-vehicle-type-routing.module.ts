﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';

// COMPONENTS
import { EditVehicleTypeComponent } from './edit-vehicle-type.component';
import { EditVehicleTypeBEVSettingsComponent } from './bev/edit-bev-settings.component';
import { EditVehicleTypeBookNGoSettingsComponent } from './book-n-go/edit-bookngo-settings.component';
import { EditVehicleTypeSettingsComponent } from './settings/edit-vehicle-type-settings.component';
import { EditVehicleTypeCorrectionFactorsComponent } from './correction-factors/edit-correction-factors.component';
import { EditVehicleTypeTargetBaselinesComponent } from './target-baselines/edit-target-baselines.component';

const EditVehicleTypesRoutes: Routes = [
    {
        path: '',
        component: EditVehicleTypeComponent,
        canActivate: [AuthGuard],                  
    },
    {
        path: 'settings',
        component: EditVehicleTypeSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.settings'
        }
    },     
    {
        path: 'correction-factors',
        component: EditVehicleTypeCorrectionFactorsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'energy_category.correction_factors'
        }
    },
    {
        path: 'target-baselines',
        component: EditVehicleTypeTargetBaselinesComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'energy_category.target_baselines'
        }
    },
    {
        path: 'bev',
        component: EditVehicleTypeBEVSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'vehicle_types.category.energy.bev'
        }
    },
    {
        path: 'bookngo',
        component: EditVehicleTypeBookNGoSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'vehicle_types.category.book_n_go'
        }
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(EditVehicleTypesRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class EditVehicleTypesRoutingModule { }

