// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { AdminLabelService } from '../../admin-label.service';
import { AuthService } from '../../../auth/authService';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES 
import { SettingList } from '../../settings-list/settings-list.types';
import { VehicleTypeSessionName } from '../vehicle-types.types';
import { ModuleOptions, AdminPropertyOptions } from '../../../auth/acl.types';

@Component({
    selector: 'edit-vehicle-type',
    templateUrl: 'edit-vehicle-type.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditVehicleTypeComponent {
    public options: SettingList[] = [
        {
            title: 'vehicle_types.category.basic',
            items: [
                {
                    option: 'assets.group.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
            ]
        },
        {
            title: 'energy_category',
            items: [
                {
                    option: 'vehicle_types.category.energy.bev',
                    params: ['bev'],
                    icon: 'battery_charging_full'
                },
                {
                    option: 'energy_category.target_baselines',
                    params: ['target-baselines'],
                    icon: 'equalizer'
                },
                {
                    option: 'energy_category.correction_factors',
                    params: ['correction-factors'],
                    icon: 'note_add'
                },
            ]
        },
        {
            title: 'vehicle_types.category.book_n_go',
            items: [
                {
                    option: 'vehicle_types.category.book_n_go.settings',
                    params: ['bookngo'],
                    icon: 'timeline',
                    allowed: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.BookNGo)
                }
            ]
        }
    ];

    constructor(public labelService: AdminLabelService,
                private router: Router,
                private previousRouteService: PreviousRouteService,
                private authService: AuthService,
                private configService: Config) {
        labelService.setSessionItemName(VehicleTypeSessionName);
    }
    
    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
