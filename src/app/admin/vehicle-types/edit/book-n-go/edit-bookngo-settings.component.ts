// FRAMEWORK
import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { UserOptionsService } from '../../../../shared/user-options/user-options.service';
import { AdminLabelService } from '../../../admin-label.service';
import { VehicleTypesService } from '../../vehicle-types.service';
import { Config } from '../../../../config/config';

// TYPES
import { VehicleTypeUpdateBookNGoResultCodeOptions, VehicleTypeBookNGoSettings, VehicleTypeSessionName } from '../../vehicle-types.types';
import { FileUploadResult, FileUploadErrorMessages, FileUploadResultCodeOptions } from '../../../../common/file-upload-types';

@Component({
    selector: 'edit-vehicle-type-bookngo',
    templateUrl: 'edit-bookngo-settings.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-bookngo-settings.less']
})

export class EditVehicleTypeBookNGoSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('fileEvImageInput', { static: true }) public fileEvImageInput;
    @ViewChild('evImage', { static: true }) public evImage;
    
    public loading: boolean;
    public loadingEvImageUpload: boolean;
    public vehicleType: VehicleTypeBookNGoSettings = {} as VehicleTypeBookNGoSettings;
    public vehicleTypeImageURL: string;
    private imageEmpty: string = 'assets/images/empty-image.png';
    private baseEvImageUrl: string = this.configService.get('evImageUrl');

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translate: CustomTranslateService,
        protected router: Router,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: VehicleTypesService,
        public userOptions: UserOptionsService,
    ) {
        super(translate, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(VehicleTypeSessionName);

        FileUploadErrorMessages.forEach((type) => {
            translate.get(type.term).subscribe((t) => {
                type.name = t;
            });
        });
    }

    public ngOnInit(): void {
        super.ngOnInit();
        
        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.vehicleTypeImageURL = this.baseEvImageUrl + '/' + params.id + '.png';
                    this.get(params.id);
                }
            });
    }

    public onSubmit({ value, valid }: { value: VehicleTypeBookNGoSettings, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }

        this.serviceSubscriber = this.service
            .updateBookNGoSettings(this.vehicleType)
            .subscribe((res: VehicleTypeUpdateBookNGoResultCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                   // const message = this.getUpdateResult(res);
                    this.snackBar.open('message', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });

    }

    // =========================
    // EV IMAGE
    // =========================
    
    /**
     * upload ev image file to server
     */
    public uploadEvImageFile(): void {
        const fi = this.fileEvImageInput.nativeElement as HTMLInputElement;
        if (fi.files && fi.files[0]) {
            this.loadingEvImageUpload = true;
            const fileToUpload = fi.files[0];
            this.service
                .uploadEvImage(fileToUpload, this.vehicleType.id)
                .subscribe((res: FileUploadResult) => {
                    this.loadingEvImageUpload = false;
                    if (res.code === FileUploadResultCodeOptions.Ok) {
                        this.vehicleTypeImageURL = '';
                        this.vehicleTypeImageURL = res.url;
                        fi.value = '';
                    } else {
                        let message = 'form.errors.unknown';
                        const messageCode = FileUploadErrorMessages.find((x) => x.id === res.code);
                        if (messageCode) {
                            message = messageCode.name;
                        } 
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                    this.loadingEvImageUpload = false;
                });
        } else {
            this.snackBar.open('file_upload.errors.null', 'form.actions.close');
        }
    }
    
    /**
     * remove ev image file from server
     */
    public removeEvImageFile(): void {
        this.loadingEvImageUpload = true;
        this.service
            .removeEvImage(this.vehicleType.id)
            .subscribe((status: number) => {
                this.loadingEvImageUpload = false;
                if (status === 200) {
                    const fi = this.fileEvImageInput.nativeElement as HTMLInputElement;
                    fi.value = '';
                    this.vehicleTypeImageURL = '';
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
                this.loadingEvImageUpload = false;
            });
    }

    /**
     * show an empty image if none was found in the server
     */
    public imgErrorHandler(event) {
        event.target.src = this.imageEmpty;
    }

    public imageExists(): boolean {
        if (!this.evImage) {
            return false;
        }
        const img = this.evImage.nativeElement as HTMLImageElement;
        return img.src.indexOf(this.imageEmpty) > -1 ? false : true;
    }


    // =========================
    // DATA
    // =========================
       
    private get(id: number) {
        this.loading = true;
        this.service
            .getBookNGoSettingsByID(id)
            .subscribe((res: VehicleTypeBookNGoSettings) => {
                this.loading = false;
                this.vehicleType = res;
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }
}
