// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { ViewVehicleTypesComponent } from './view/view-vehicle-types.component';
import { EditVehicleTypeSettingsComponent } from './edit/settings/edit-vehicle-type-settings.component';

const VehicleTypesRoutes: Routes = [
    {
        path: '',
        component: ViewVehicleTypesComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditVehicleTypeSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-vehicle-type.module').then((m) => m.EditVehicleTypesModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }   
];

@NgModule({
    imports: [
        RouterModule.forChild(VehicleTypesRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class VehicleTypesRoutingModule { }

