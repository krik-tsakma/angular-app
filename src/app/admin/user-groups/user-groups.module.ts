// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { SettingsListModule } from '../settings-list/settings-list.module';
import { EditUserGroupSettingsModule } from './edit/settings/edit-user-group-settings.module';
import { UserGroupsRoutingModule } from './user-groups-routing.module';

// COMPONENTS
import { ViewUserGroupsComponent } from './view/view-user-groups.component';

// SERVICES
import { UserGroupsService } from './user-groups.service';


@NgModule({
    imports: [
        SharedModule,
        UserGroupsRoutingModule,
        EditUserGroupSettingsModule,
        SettingsListModule
    ],
    declarations: [
        ViewUserGroupsComponent
    ],
    providers: [
        UserGroupsService
    ]
})
export class UserGroupsModule { }
