// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RxJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { 
    UserGroupsResult, 
    UserGroupSettings, 
    UserGroupCreateCodeOptions, 
    UserGroupSettingsCodeOptions, 
    UserGroupAddMemberCodeOptions, 
    UserGroupRemoveMemberCodeOptions,
    UserGroupDeleteCodeOptions, 
} from './user-groups.types';
import { SearchTermRequest } from '../../common/pager';
import { AssetGroupMembersRequest, AssetGroupMembersResult, AssetGroupMemberAddRemoveRequest } from '../asset-groups/asset-groups.types';

@Injectable()
export class UserGroupsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = config.get('apiUrl') + '/api/UserGroups';
        }

    // Get all location groups
    public get(request: SearchTermRequest): Observable<UserGroupsResult> {
    
        const params = new HttpParams({
            fromObject: {
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Sorting: request.sorting
            }
        });

        return this.authHttp
            .get(this.baseURL, { params })
            .pipe();
    }


    public getByID(id: number): Observable<UserGroupSettings> {
        return this.authHttp
            .get(this.baseURL + '/' + id)
            .pipe();
    }


    public create(entity: UserGroupSettings): Observable<UserGroupCreateCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as UserGroupCreateCodeOptions;
                    }
                })
            );
    }


    public update(entity: UserGroupSettings): Observable<UserGroupSettingsCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserGroupSettingsCodeOptions;
                    }
                })
            );            
    }

    public delete(id: number): Observable<UserGroupDeleteCodeOptions> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserGroupDeleteCodeOptions;
                    }
                })
            ); 
    }

    // ====================
    // MEMBERS
    // ====================

    public getMembers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
            const params = new HttpParams({
                fromObject: {
                    ID: request.id.toString(),
                    SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                    Sorting: request.sorting,
                    PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                    PageSize: request.pageSize ? request.pageSize.toString() : '20'
                }
            });

            return this.authHttp
                    .get(this.baseURL + '/GetMembers', { params })
                    .pipe();
    }


    public addMember(entity: AssetGroupMemberAddRemoveRequest): Observable<UserGroupAddMemberCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/AddMember', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserGroupAddMemberCodeOptions;
                    }
                }),
            );            
    }

    public removeMember(entity: AssetGroupMemberAddRemoveRequest): Observable<UserGroupRemoveMemberCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/RemoveMember', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserGroupRemoveMemberCodeOptions;
                    }
                }),
            );            
    }
} 
