﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../../shared/shared.module';

// COMPONENTS
import { EditUserGroupSettingsComponent } from './edit-user-group-settings.component';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        EditUserGroupSettingsComponent
    ],
    exports: [
        EditUserGroupSettingsComponent
    ]
})

export class EditUserGroupSettingsModule { }
