// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { AdminLabelService } from '../../../admin-label.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { UserGroupsService } from '../../user-groups.service';
import { Config } from '../../../../config/config';

// TYPES
import { 
    UserGroupSettings, 
    UserGroupCreateCodeOptions, 
    UserGroupSettingsCodeOptions, 
    UserGroupSessionName 
} from '../../user-groups.types';

@Component({
    selector: 'edit-user-group-settings',
    templateUrl: 'edit-user-group-settings.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditUserGroupSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public group: UserGroupSettings = {} as UserGroupSettings;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: UserGroupsService
        
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(UserGroupSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                this.action = params.id
                            ? 'form.actions.edit'
                            : 'form.actions.create';
                if (params.id) {
                    this.get(params.id);
                }
            });
    }

    public onSubmit({ value, valid }: { value: UserGroupSettings, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is create
        if (typeof(this.group.id) === 'undefined' || this.group.id === 0) {
            this.serviceSubscriber = this.service
                .create(this.group)
                .subscribe((res: UserGroupCreateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const errorMessage = this.getCreateResult(res);
                        this.snackBar.open(errorMessage, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else { 
        // This is an update
        this.serviceSubscriber = this.service
            .update(this.group)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === null) {
                    this.labelService.set(this.group.name);
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    const errorMessage = this.getUpdateResult(res);
                    this.snackBar.open(errorMessage, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
        }
    }


    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((res: UserGroupSettings) => {
                this.loading = false;
                this.group = res;
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }


    // =========================
    // RESULTS
    // =========================
    private getCreateResult(code: UserGroupCreateCodeOptions): string {
        let message = '';
        switch (code) {
            case UserGroupCreateCodeOptions.NameAlreadyExists:
                message = 'form.errors.name_exists';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUpdateResult(code: UserGroupSettingsCodeOptions): string {
        let message = '';
        switch (code) {
            case UserGroupSettingsCodeOptions.NameAlreadyExists:
                message = 'form.errors.name_exists';
                break;
            case UserGroupSettingsCodeOptions.NoSuchGroupExists:
                message = 'form.errors.not_found';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
