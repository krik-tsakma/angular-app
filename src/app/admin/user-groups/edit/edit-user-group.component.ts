// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { AdminLabelService } from '../../admin-label.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES 
import { SettingList } from '../../settings-list/settings-list.types';
import { UserGroupSessionName } from '../user-groups.types';

@Component({
    selector: 'edit-user-group',
    templateUrl: 'edit-user-group.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditUserGroupComponent {
    public options: SettingList[] = [
        {
            title: 'assets.group.category.basic',
            items: [
                {
                    option: 'assets.group.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
                {
                    option: 'assets.group.category.basic.membership',
                    params: ['members'],
                    icon: 'person'
                }
            ]
        }
    ];

    constructor(public labelService: AdminLabelService,
                private previousRouteService: PreviousRouteService,
                private router: Router,
                private configService: Config) {
            labelService.setSessionItemName(UserGroupSessionName);
    }

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
