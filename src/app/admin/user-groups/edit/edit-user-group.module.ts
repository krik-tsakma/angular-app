
// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';
import { EditUserGroupRoutingModule } from './edit-user-group-routing.module';
import { EditUserGroupSettingsModule } from './settings/edit-user-group-settings.module';
import { AssetGroupsModule } from '../../../common/asset-groups/asset-groups.module';

// COMPONENTS
import { EditUserGroupComponent } from './edit-user-group.component';
import { EditUserGroupMembersComponent } from './members/edit-user-group-members.component';

// SERVICES
import { UserGroupsService } from '../user-groups.service';

@NgModule({
    imports: [
        SharedModule,
        EditUserGroupRoutingModule,
        EditUserGroupSettingsModule,
        SettingsListModule,
        AssetGroupsModule
    ],
    declarations: [
        EditUserGroupComponent,
        EditUserGroupMembersComponent
    ],
    providers: [
        UserGroupsService
    ]
})
export class EditUserGroupModule { }
