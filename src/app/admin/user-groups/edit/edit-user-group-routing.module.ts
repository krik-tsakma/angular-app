﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';

// COMPONENTS
import { EditUserGroupComponent } from './edit-user-group.component';
import { EditUserGroupSettingsComponent } from './settings/edit-user-group-settings.component';
import { EditUserGroupMembersComponent } from './members/edit-user-group-members.component';

const EditUserGroupRoutes: Routes = [
    {
        path: '',
        component: EditUserGroupComponent,
        canActivate: [AuthGuard]
    },   
    {
        path: 'settings',
        component: EditUserGroupSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.settings'
        }
    },
    {
        path: 'members',
        component: EditUserGroupMembersComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.membership'
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(EditUserGroupRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class EditUserGroupRoutingModule { }
