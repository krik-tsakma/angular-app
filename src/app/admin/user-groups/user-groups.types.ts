import { PagerResults } from '../../common/pager';

export const UserGroupSessionName: string = 'usrgn';

export interface UserGroupsResult extends PagerResults {
    results: UserGroupResultItem[];
}

export interface UserGroupResultItem {
    groupID: number;
    name: string;
    isDefault: boolean;
}

// ====================
// BASIC INFO
// ====================

export interface UserGroupSettings {
    id: number;
    name: string;
    description: string;
}

export enum UserGroupCreateCodeOptions {
    Created = 0,
    NameAlreadyExists = 1,
    UnknownError = 2,
}

export enum UserGroupSettingsCodeOptions {
    Set = 0,
    NoSuchGroupExists = 1,
    NameAlreadyExists = 2,
    UnknownError = 3
}

export enum UserGroupDeleteCodeOptions {
    Removed = 0,
    NoSuchGroupExists = 1,
    DefaultGroupNotFound = 2,
    UnknownError = 3,
}

// ====================
// MEMBERS
// ===================

export enum UserGroupAddMemberCodeOptions {
    Added = 0,
    UserGroupNotFound = 1,
    UserGroupIsDefault = 2,
    UserNotFound = 3,
    UserAlreadyAMember = 4,
    UnknownError = 5
}


export enum UserGroupRemoveMemberCodeOptions {
    Removed = 0,
    UserGroupNotFound = 1,
    UserNotAMember = 2
}
