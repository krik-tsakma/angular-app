
// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { EditUserGroupSettingsComponent } from './edit/settings/edit-user-group-settings.component';
import { ViewUserGroupsComponent } from './view/view-user-groups.component';

const UserGroupsRoutes: Routes = [
    {
        path: '',
        component: ViewUserGroupsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditUserGroupSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-user-group.module').then((m) => m.EditUserGroupModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }   
];

@NgModule({
    imports: [
        RouterModule.forChild(UserGroupsRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class UserGroupsRoutingModule { }
