﻿export interface AdminOptionsCategories {
    categoryTitle: string;
    categoryOptions: AdminCategoryOptions[];
    visible: boolean;
}

export interface AdminCategoryOptions {
    option: string;
    params: string[];
    accessible: boolean;
}
