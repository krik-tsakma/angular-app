// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { ViewAssetLinksComponent } from './view/view-asset-links.component';

const AssetLinksRoutes: Routes = [
    {
        path: '',
        component: ViewAssetLinksComponent,
        canActivate: [AuthGuard]
    },
    
];

@NgModule({
    imports: [
        RouterModule.forChild(AssetLinksRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AssetLinksRoutingModule { }

