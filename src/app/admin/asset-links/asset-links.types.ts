import { PagerResults } from '../../common/pager';

export interface AssetLinksResult extends PagerResults {
    results: AssetLink[];
}

export interface AssetLinkRequest {
    trackingDeviceID: number;
    vehicleID: number;
    driverID?: number;
}

export interface AssetLink {
    trackingDeviceID: number;
    trackingDeviceLabel: string;
    vehicleID: number;
    vehicleLabel: string;
    driverID?: number;
    driverLabel?: string;
    allowedVehicle: boolean;
    allowedDriver: boolean;
}


export enum AssetLinkCodeOptions {
    Linked = 0,
    TrackingDeviceDoesNotExist = 1,
    VehicleDoesNotExist = 2,
    DriverDoesNotExist = 3,
    IssuerNotFound = 4,
    NotAllowedVeh = 5,
    NotAllowedHr = 6,
    UnknownError = 9
}

export enum AssetUnlinkCodeOptions {
    Unlinked = 0,
    NoSuchAssetLinkExists = 1,
    AssetLinkVehicleDoesNotExist = 2,
    AssetLinkDriverDoesNotExist = 3,
    UnknownError = 9
}
