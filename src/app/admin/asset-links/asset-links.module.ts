
// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { AssetLinksRoutingModule } from './asset-links-routing.module';

// COMPONENTS
import { ViewAssetLinksComponent } from './view/view-asset-links.component';
import { EditAssetLinksDialogComponent } from './edit-dialog/edit-asset-links-dialog.component';

// SERVICES
import { AssetLinksService } from './asset-links.service';
import { EditAssetLinksDialogService } from './edit-dialog/edit-asset-links-dialog.service';
import { AssetLinksWarningService } from '../../common/asset-link-warning/asset-link-warning.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService ) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/asset-links/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        AssetLinksRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewAssetLinksComponent,
        EditAssetLinksDialogComponent
    ],
    entryComponents: [
        EditAssetLinksDialogComponent
    ],
    providers: [
        AssetLinksService,
        EditAssetLinksDialogService,
        AssetLinksWarningService
    ]
})
export class AssetLinksModule { }
