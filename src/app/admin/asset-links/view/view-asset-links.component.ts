// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { StoreManagerService } from '../../../shared/store-manager/store-manager.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { AdminLabelService } from '../../admin-label.service';
import { AssetLinksService } from '../asset-links.service';
import { EditAssetLinksDialogService } from '../edit-dialog/edit-asset-links-dialog.service';
import { Config } from '../../../config/config';

// TYPES
import {
    TableColumnSetting, TableActionItem, TableColumnSortOrderOptions,
    TableActionOptions, TableCellFormatOptions, TableColumnSortOptions
} from '../../../shared/data-table/data-table.types';
import { StoreItems } from '../../../shared/store-manager/store-items';
import { SearchTermRequest, Pager } from '../../../common/pager';
import { AssetLink, AssetLinksResult, AssetUnlinkCodeOptions } from '../asset-links.types';

@Component({
    selector: 'view-asset-links',
    templateUrl: 'view-asset-links.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['view-asset-links.less']
})

export class ViewAssetLinksComponent extends AdminBaseComponent implements OnInit {
    public assetLinks: AssetLink[] = [];
    public searchTermControl = new FormControl();
    public request: SearchTermRequest = {} as SearchTermRequest;
    public loading: boolean;
    public tableSettings: TableColumnSetting[];
    public pager: Pager = new Pager();

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translate: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBarService: SnackBarService,
        protected configService: Config,
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,

        protected storeManager: StoreManagerService,
        private service: AssetLinksService,
        private editService: EditAssetLinksDialogService
    ) {
        super(translate, previousRouteService, router, unsubscriber, labelService, configService, snackBarService, storeManager,  confirmDialogService);

        this.tableSettings = this.getTableSettings();
    }

    // ----------------
    // LIFECYCLE
    // ----------------

    public ngOnInit(): void {
        super.ngOnInit();

        const savedData = this.storeManager.getOption(StoreItems.assetLinks) as SearchTermRequest;
        if (savedData) {
            this.request = savedData;
            this.searchTermControl.setValue(this.request.searchTerm);
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchTermControl.valueChanges
            .pipe(
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    this.request.searchTerm = term;
                    this.get(true);
                    return of([]);
                })
            )
            .subscribe();
        this.get(true);
    }

    // ----------------
    // EVENT HANDLERS
    // ----------------

    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.searchTermControl.setValue(null);
    }


    public addNew() {
        this.editService
        .openDialog('add', {} as AssetLink)
        .subscribe((response: boolean) => {
            if (response === true) {
                this.get(true);
            }
        });
    }



    // ----------------
    // TABLE
    // ----------------

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const record = item.record as AssetLink;
        switch (item.action) {
            case TableActionOptions.edit:
                this.editService
                    .openDialog('edit', record)
                    .subscribe((response: boolean) => {
                        if (response === true) {
                            this.get(true);
                        }
                    });
                break;
            case TableActionOptions.delete:
                this.openDialog()
                    .subscribe((response) => {
                        if (response === true) {
                            this.unlink(record.trackingDeviceID);
                        }
                    });
                break;
            default:
                break;

        }
    }

    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }


    // ----------------
    // PRIVATE
    // ----------------

    // Fetch vehicle types data
    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.assetLinks = new Array<AssetLink>();
        }

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        // first stop currently executing requests
        this.unsubscribeService();

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.assetLinks, this.request);

        // then start the new request
        this.serviceSubscriber = this.service
            .get(this.request)
            .subscribe(
                (response: AssetLinksResult) => {
                    this.loading = false;

                    this.assetLinks = this.assetLinks.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;

                    if (!this.assetLinks || this.assetLinks.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }


    // ----------------
    // DELETE
    // ----------------

    private unlink(id: number) {
        this.loading = true;

        // first stop currently executing requests
        this.unsubscribeService();

        this.serviceSubscriber = this.service
            .unlink(id)
            .subscribe((res: AssetUnlinkCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get(true);
                }  else {
                    const message = this.getUnlinkResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    // -----------
    // HELPERS
    // -----------

    private getTableSettings(): TableColumnSetting[] {
        return [{
                    primaryKey: 'trackingDeviceLabel',
                    header: 'assets.tracking_device',
                    sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
                },
                {
                    primaryKey: 'vehicleLabel',
                    header: 'assets.vehicle',
                    sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null),
                    format: TableCellFormatOptions.custom,
                    formatFunction: (record: AssetLink) => {
                        if (!record.vehicleID && record.allowedVehicle) {
                            return '-';
                        }
                        return record.vehicleLabel;
                        // return `<a class="primary-link" href="/#/admin/vehicles/edit/${record.vehicleID}" target="_blank">${record.vehicleLabel}</a>`;
                    }
                },
                {
                    primaryKey: 'driverLabel',
                    header: 'assets.driver',
                    sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null),
                    format: TableCellFormatOptions.custom,
                    formatFunction: (record: AssetLink) => {
                        if (!record.driverID && record.allowedDriver) {
                            return '-';
                        }
                        return record.driverLabel;
                        // return `<a class="primary-link" href="/#/admin/drivers/edit/${record.driverID}" target="_blank">${record.driverLabel}</a>`;
                    }
                },
                {
                    primaryKey: ' ',
                    header: ' ',
                    actions: [TableActionOptions.edit, TableActionOptions.delete],
                    actionsVisibilityFunction: (action: TableActionOptions, record: AssetLink): boolean => {
                        if (!record.allowedVehicle || !record.allowedDriver) {
                            return false;
                        }
                        // if no vehicle and driver, then there's no link to delete
                        if (action === TableActionOptions.delete && !record.vehicleID && !record.driverID) {
                            return false;
                        }

                        return true;
                    }
                }];
    }

    // =========================
    // RESULTS
    // =========================

    private getUnlinkResult(code: AssetUnlinkCodeOptions): string {
        let message = '';
        switch (code) {
            case AssetUnlinkCodeOptions.NoSuchAssetLinkExists:
                message = 'form.errors.not_found';
                break;
            case AssetUnlinkCodeOptions.AssetLinkVehicleDoesNotExist:
                message = 'assets.links.error.vehicle_not_found';
                break;
            case AssetUnlinkCodeOptions.AssetLinkDriverDoesNotExist:
                message = 'assets.links.error.driver_not_found';
                break;
            case AssetUnlinkCodeOptions.UnknownError:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

}
