// FRAMEWORK
import { 
    Component, OnDestroy, ViewEncapsulation, OnInit,
    ViewChild, ElementRef, Renderer 
} from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

// RxJS
import { Subscription, of } from 'rxjs';

// SERVICES
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { AssetLinksService } from '../asset-links.service';
import { AssetLinksWarningService } from './../../../common/asset-link-warning/asset-link-warning.service';

// TYPES 
import { AssetLink, AssetLinkCodeOptions } from '../asset-links.types';
import { SearchType } from '../../../shared/search-mechanism/search-mechanism-types';
import { KeyName } from '../../../common/key-name';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { AssetLinkWarningMessage } from '../../../common/asset-link-warning/asset-link-warning-types';

@Component({
    selector: 'edit-asset-links-dialog',
    templateUrl: 'edit-asset-links-dialog.html',
    encapsulation: ViewEncapsulation.None
})

export class EditAssetLinksDialogComponent implements OnInit, OnDestroy {    
    @ViewChild('vehTerm', { static: false }) public vehTerm: ElementRef;
    @ViewChild('hrTerm', { static: false }) public hrTerm: ElementRef;
    public loading: boolean;
    public searchTypes: any = SearchType;
    public trackingDeviceEnabled: boolean;
    public trackingDeviceLinkedWarningMessage: string;
    
    // vehicle search
    public vehSearchForLinked: boolean;
    public vehSearchTerm: string;
    public vehSearchTermControl = new FormControl();
    public vehSearchLoading: boolean; 
    public vehs: KeyName[];
    public vehicleLinkedWarningMessage: string;

    // hr search
    public hrSearchForLinked: boolean;
    // public hrSearchTerm: string;
    public hrSearchTermControl = new FormControl();
    public hrSearchLoading: boolean; 
    public hrs: KeyName[];
    public hrLinkedWarningMessage: string;

    public request: AssetLink = {} as AssetLink;
    public serviceSubscriber: Subscription;

    
    constructor(        
        private service: AssetLinksService, 
        private renderer: Renderer,
        private assetLinkWarningService: AssetLinksWarningService,
        private dialogRef: MatDialogRef<EditAssetLinksDialogComponent>,             
        private unsubscribe: UnsubscribeService,
        private snackBar: SnackBarService) {
            // foo
    }

    public ngOnInit() {
        if (this.request.vehicleLabel === ' ') {
            this.request.vehicleLabel = '';
        }
        if (this.request.driverLabel === ' ') {
            this.request.driverLabel = '';
        }

        this.vehSearchForLinked = false;
        if (this.request.vehicleID > 0) {
            this.vehSearchForLinked = true;
            this.vehSearchTermControl.setValue(this.request.vehicleLabel);
        }
        this.vehSearchTermControl.valueChanges    
        .pipe(        
            debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing             
            distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
            // switchMap operator automatically unsubscribes from previous subscriptions,
            // as soon as the outer Observable emits new values
            // so we will always search with last term
            switchMap((term) => {
                this.onVehicleSearch(term);
                return of([]);
            })
        )
        .subscribe();


        this.hrSearchForLinked = false;
        if (this.request.driverID > 0) {
            this.hrSearchForLinked = true;
            this.hrSearchTermControl.setValue(this.request.driverLabel);
        }

        this.hrSearchTermControl.valueChanges    
        .pipe(        
            debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing             
            distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
            // switchMap operator automatically unsubscribes from previous subscriptions,
            // as soon as the outer Observable emits new values
            // so we will always search with last term
            switchMap((term) => {
                this.onDriverSearch(term);
                return of([]);
            })
        )
        .subscribe();
    }

    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }


    public saveBtnDisabled() {
        return this.loading || this.vehSearchLoading 
                || this.hrSearchLoading || this.formInvalid();
    }


   /*
    * selected option from trackingDevices
    */
    public onTrackingDeviceSelect(device: KeyName) {
        this.trackingDeviceEnabled = true;
        this.request.trackingDeviceID = device
            ? device.id 
            : null;
        this.request.trackingDeviceLabel = device 
            ? device.name 
            : null;        

        this.trackingDeviceLinkedWarningMessage = null;
        if (this.request.trackingDeviceID) {
            this.assetLinkWarningService
                .GetForDevice(this.request.trackingDeviceID)
                .then((res: AssetLinkWarningMessage) => {
                    this.trackingDeviceLinkedWarningMessage = res.message; 
                });
        } 
    }
  
   /*
    * selected option from vehicle
    */
    public onVehicleSelect(vehicle: KeyName) {
        this.request.vehicleID = vehicle 
            ? vehicle.id 
            : null;
        this.request.vehicleLabel = vehicle 
            ? vehicle.name 
            : null;
        
        this.vehicleLinkedWarningMessage = null;
        if (this.request.vehicleID) {
            this.assetLinkWarningService
                .GetForVehicle(this.request.vehicleID)
                .then((res: AssetLinkWarningMessage) => {
                    this.vehicleLinkedWarningMessage = res.message ;
                });
        } 
    }


   /*
    * selected option from driver
    */
    public onDriverSelect(driver: KeyName) {
        this.request.driverID = driver 
            ? driver.id 
            : null;
        this.request.driverLabel = driver
            ? driver.name 
            : null;

        this.hrLinkedWarningMessage = null;
        if (this.request.driverID) {
            this.assetLinkWarningService
                .GetForDriver(this.request.driverID)
                .then((res: AssetLinkWarningMessage) => {
                    this.hrLinkedWarningMessage = res.message; 
                });
        } 
    }

   
   /*
    * Closes the dialog
    */
    public close(success?: boolean) {
        this.dialogRef.close(success);
    }    



   /*
    * Enable/Disable save button    
    */
    public formInvalid(): boolean {
        if (!this.request.trackingDeviceID || 
            !this.request.vehicleID) {
            return true;
        }
        return false;            
    }



   /*
    * Save or update
    */
   public onSubmit(value: AssetLink) {
    // check if model is valid

    if (this.formInvalid()) {
        return;
    }

    this.loading = true;

    if (this.request.driverID === 0) {
        this.request.driverID = null;
        this.request.driverLabel = null;
    }

    this.serviceSubscriber = this.service
        .link(this.request)
        .subscribe((res: AssetLinkCodeOptions) => {
            this.loading = false;
            if (res === null) {
                this.snackBar.open('form.save_success', 'form.actions.close');
                this.close(true);
            } else {
                const message = this.getLinkResult(res);
                this.snackBar.open(message, 'form.actions.close');
            }
        },
        (err) => {
            this.loading = false;
            if (err.status === 404) {
                this.snackBar.open('form.errors.not_found', 'form.actions.close');
            } else {
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            }
        });

    }


    public clearInput(e: Event, inputSelect: string) {
        // do not bubble event otherwise focus is on the textbox
        if (e) {
            e.stopPropagation();
        }

        if (inputSelect === 'vehicle') {
            this.request.vehicleID = null;
            this.request.vehicleLabel = null;    
            this.vehSearchTermControl.setValue(null);
            this.vehicleLinkedWarningMessage = null;
        } else {
            this.request.driverID = null;
            this.request.driverLabel = null;
            this.hrSearchTermControl.setValue(null);
            this.hrLinkedWarningMessage = null;
        }

        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        const fileInput = inputSelect === 'vehicle' ? this.vehTerm : this.hrTerm;
        this.renderer.invokeElementMethod(fileInput.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }


    /*
    * Get vehicles
    */
   private onVehicleSearch(term: string) {
    if (!term || term.length < 2) {
        return;
    }

    this.vehSearchLoading = true;
    
    this.serviceSubscriber = this.service
            .getVehicles(term, this.vehSearchForLinked)
            .subscribe((res: KeyName[]) => {
                this.vehSearchLoading = false;
                this.vehs = res;
        },
        (err) => {
            this.vehSearchLoading = false;
            this.snackBar.open('form.errors.unknown', 'form.actions.ok');
        });
    }


    /*
    * Get drivers
    */
   private onDriverSearch(term: string) {
    if (!term || term.length < 2) {
        return;
    }

    this.hrSearchLoading = true;
    
    this.serviceSubscriber = this.service
            .getDrivers(term, this.hrSearchForLinked)
            .subscribe((res: KeyName[]) => {
                this.hrSearchLoading = false;
                this.hrs = res;
        },
        (err) => {
            this.hrSearchLoading = false;
            this.snackBar.open('form.errors.unknown', 'form.actions.ok');
        });
    }

    // =========================
    // RESULTS
    // =========================

    private getLinkResult(code: AssetLinkCodeOptions): string {
        let message = '';
        switch (code) {
            case AssetLinkCodeOptions.TrackingDeviceDoesNotExist:
                message = 'assets.links.error.device_not_found';
                break;
            case AssetLinkCodeOptions.VehicleDoesNotExist:
                message = 'assets.links.error.vehicle_not_found';
                break;
            case AssetLinkCodeOptions.DriverDoesNotExist:
                message = 'assets.links.error.driver_not_found';
                break;
            case AssetLinkCodeOptions.UnknownError:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

}
