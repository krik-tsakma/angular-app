// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { EditAssetLinksDialogComponent } from './edit-asset-links-dialog.component';

// TYPES
import { AssetLink } from '../asset-links.types';

@Injectable()
export class EditAssetLinksDialogService {
   
    constructor(private dialog: MatDialog) { }       

    public openDialog(title: 'add' | 'edit', param?: AssetLink): Observable<boolean> {

        const dialogRef: MatDialogRef<EditAssetLinksDialogComponent> = this.dialog.open(EditAssetLinksDialogComponent);
       
        if (param) {
            dialogRef.componentInstance.request = Object.assign({}, param);
        } 
            
        return dialogRef.afterClosed();
    }
} 
