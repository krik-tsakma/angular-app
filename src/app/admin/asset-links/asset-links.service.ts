// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import { AssetLinksResult, AssetLinkRequest, AssetLinkCodeOptions, AssetUnlinkCodeOptions, } from './asset-links.types';
import { SearchTermRequest } from '../../common/pager';
import { KeyName } from './../../common/key-name';

@Injectable()
export class AssetLinksService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/AssetLinks';
        }

    // Get all asset links
    public get(request: SearchTermRequest): Observable<AssetLinksResult> {

        const obj = this.authHttp.removeObjNullKeys({
            SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
            PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
            PageSize: request.pageSize ? request.pageSize.toString() : '20',
            Sorting: request.sorting
        });

        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp.get(this.baseURL, { params });           
    }

    // create / update link
    public link(entity: AssetLinkRequest): Observable<AssetLinkCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/link', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as AssetLinkCodeOptions;
                    }
                })               
            );            
    }

    // delete link
    public unlink(trackingDeviceID: number): Observable<AssetUnlinkCodeOptions> {
        return this.authHttp
            .delete(this.baseURL + '/unlink/' + trackingDeviceID, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as AssetUnlinkCodeOptions;
                    }
                })        
            ); 
    }

    // Get vehicles for linking
    public getVehicles(term: string, linked: boolean): Observable<KeyName[]> {

        const params = new HttpParams({
            fromObject: {
                SearchTerm: term,
                Linked: linked.toString()
            }
        });

        return this.authHttp
                    .get(this.baseURL + '/GetVehicles', { params });           
    }

    // Get drivers for linking
    public getDrivers(term: string, linked: boolean): Observable<KeyName[]> {

        const params = new HttpParams({
            fromObject: {
                SearchTerm: term,
                Linked: linked.toString()
            }
        });

        return this.authHttp
                    .get(this.baseURL + '/GetDrivers', { params });           
    }
} 

