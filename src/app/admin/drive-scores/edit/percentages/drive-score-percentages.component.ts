// FRAMEWORK
import { 
    Component, 
    Input, 
    Output,
    EventEmitter, 
    AfterViewInit 
} from '@angular/core';

// SERVICES
import { CustomTranslateService } from './../../../../shared/custom-translate.service';

// TYPES
import { DriveScorePercentage } from './../../drive-score-types';

@Component({
    selector: 'drive-score-percentages',
    templateUrl: './drive-score-percentages.html',
    styleUrls: ['./drive-score-percentages.less'],
})

export class DriveScorePercentagesComponent implements AfterViewInit {
    @Input() public modelData?: DriveScorePercentage[];
    @Output() public modelDataChange: EventEmitter<DriveScorePercentage[]> =
        new EventEmitter<DriveScorePercentage[]>();

    @Input() public isValid?: boolean;
    @Output() public isValidChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    public letterMapping: { [key: number]: string } = {};
    public formErrors: string[] = [];

    constructor(public translate: CustomTranslateService) {
        this.letterMapping[0] = 'H';
        this.letterMapping[1] = 'G';
        this.letterMapping[2] = 'F';
        this.letterMapping[3] = 'E';
        this.letterMapping[4] = 'D';
        this.letterMapping[5] = 'C';
        this.letterMapping[6] = 'B';
        this.letterMapping[7] = 'A';
    }


    public ngAfterViewInit() {
        // add a small timeout to avoid ExpressionChangedAfterItHasBeenCheckedError error
        setTimeout(() => {
            // if no data available, populate with suggestions
            if (!this.modelData || this.modelData.length === 0) {
                this.initFormValues();
                this.modelDataChange.emit(this.modelData);
            }
            this.onPercentageChange(true);
        }, 1000);
    }

    // every percentage must be at least one number up from previous letter
    public calcMin(value: number): number {
        switch (value) {
            case 0:
                return 0;
            default:
                const prev = this.modelData[value - 1];
                return prev.percentage + 1;
        }
    }

    // every percentage must be at least one number down from next letter
    public calcMax(value: number): number {
        switch (value) {
            case 0:
                return 0;
            case 7:
                return 100;
            default:
                const next = this.modelData[value + 1];
                return next.percentage - 1;
        }
    }

    public onPercentageChange(inputIsValid: boolean) {
        this.isValid = inputIsValid;
        this.isValidChange.emit(this.isValid);
    }


    private initFormValues(): void {
        this.modelData = [];

        for (let index = 0; index < 8; index++) {
            const element = {
                percentage: null,
                value: index
            } as DriveScorePercentage;

            if (index === 0) {
                element.percentage = 0;
            } else if (index === 7) {
                element.percentage = 100;
            } else {
                element.percentage = this.modelData[index - 1].percentage + 10;
            }

            this.modelData.push(element);
        }
    }
}
