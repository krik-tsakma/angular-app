// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// TYPES
import { DriveScorePercentage } from '../../drive-score-types';

@Pipe({
    name: 'myDriveScorePercentagesSort',
    pure: false
})

export class DriveScorePercentagesSortPipe implements PipeTransform {
    constructor() {
        // foo
    }

    public transform(percentages: DriveScorePercentage[]): DriveScorePercentage[] {
        if (!percentages) {           
            return percentages;
        }
        return percentages.sort((a, b) => a.value - b.value);
    }
}
