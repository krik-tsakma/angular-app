
// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from './../../admin-base.component';

// SERVICES
import { DriveScoresService } from './../drive-score.service';
import { Config } from './../../../config/config';
import { AdminLabelService } from './../../admin-label.service';
import { SnackBarService } from './../../../shared/snackbar.service';
import { UnsubscribeService } from './../../../shared/unsubscribe.service';
import { CustomTranslateService } from './../../../shared/custom-translate.service';
import { PreviousRouteService } from './../../../core/previous-route/previous-route.service';
import { DriveCyclesService } from './../../drive-cycles/drive-cycles.service';

// TYPES
import {
    DriveScoreSettings,
    DriveScoreUpsertCodeOptions,
    DriveScoreTypes,
    WeightByOptions,
    DriveCycleParameterSetCustomByOpts,
    DeviceTypeGroupParameterSet,
    DriveCycleParameterSet,
    DataPointParameterSet
} from './../drive-score-types';
import { KeyName } from './../../../common/key-name';
import { DataPointsEventItem } from './datapoints-list/datapoints-list-types';
import { AssetGroupTypes } from './../../asset-groups/asset-groups.types';


@Component({
    selector: 'edit-drive-score',
    templateUrl: 'edit-drive-score.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./edit-drive-score.less'],
})

export class EditDriveScoreComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public score: DriveScoreSettings = {} as DriveScoreSettings;

    public typeOptions: KeyName[];
    public weightByOptions: KeyName[];

    // device type groups
    public deviceTypeGroups: KeyName[];

    // drive cycles
    public driveCycles: KeyName[];

    // customize by
    public customByOptions: KeyName[];
    public customByEnumOptions = DriveCycleParameterSetCustomByOpts;

    // asset group types enum
    public groupTypes = AssetGroupTypes;

    // parameter to hold validity of driver score percentages component
    public scorePercentagesValid: boolean = false;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: DriveScoresService,
        private driveCyclesService: DriveCyclesService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);

        this.typeOptions = this.getDriveScoreTypeOptions();
        this.weightByOptions = this.getWeightByOptions();
        this.customByOptions = this.getCustomByOptions();
    }

    // =========================
    // ANGULAR LIFECYCLE
    // =========================

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => {
                this.action = params.id
                            ? 'form.actions.edit'
                            : 'form.actions.create';

                Promise.all([
                    this.setDeviceTypeGroups(),
                    this.setDriveCycles()
                ]).then(() => {
                    this.initFormValues();
                    if (params.id) {
                        this.get(params.id);
                    }
                });
            });
    }

    // =========================
    // DEVICE TYPE GROUP
    // =========================

    public onDeviceTypeGroupAvailabilityChange(dtgroup: DeviceTypeGroupParameterSet): void {
        dtgroup.customBy = DriveCycleParameterSetCustomByOpts.None;
        dtgroup.dataPointIDs = [];
        dtgroup.dataPoints = [];
        dtgroup.driveCycleParameterSets = [];
    }

    // =========================
    // DRIVE CYCLE
    // =========================

    /**
     * Check whether or not a drive cycle is enabled
     * @param dcID The drive cycle's data id
     * @param dtgroup The device type group the drive cycle belongs to
     */
    public driveCycleEnabled(dcID: number, dtgroup: DeviceTypeGroupParameterSet): boolean {
        const hasSets = this.getParameterSetsForDriveCycle(dtgroup, dcID);
        return hasSets.length > 0 && dtgroup.dataPointIDs.length > 0 ? true : false;
    }

    /**
     * Enable or disable a drive cycle of a device type group.
     * Upon disable remove the entry from the device type group, upon enable add the default parameter set
     * @param dcID The drive cycle's data id
     * @param dtgroup The device type group the drive cycle belongs to
     */
    public onDriveCycleSelection(dcID: number, dtgroup: DeviceTypeGroupParameterSet): void {
        const parameterSets = this.getParameterSetsForDriveCycle(dtgroup, dcID);
        if (parameterSets.length === 0) {
            const dc = this.driveCycles.find((_) => _.id === dcID);
            this.addDefaultParameterSetToDriveCycle(dc.id, dc.name, dtgroup);
            dtgroup.dataPoints.forEach((dp) => this.onDataPointSelection(dp, dtgroup));
        } else {
            dtgroup.driveCycleParameterSets = dtgroup.driveCycleParameterSets.filter((_) => _.driveCycleID !== dcID);
        }
    }


    public getParameterSetName(parameterSet: DriveCycleParameterSet, dtgroup: DeviceTypeGroupParameterSet): string {
        if (!dtgroup || !dtgroup.customByAssetList) {
            return;
        }

        let name = '';
        dtgroup.customByAssetList.forEach((_) => {
            const item = Object.assign({}, _) as KeyName;
            item.enabled = true;
            // the asset of the parameter set to be copied, should be disabled in the list
            // (no use of copying to itself)
            if (parameterSet.vehicleGroupID && parameterSet.vehicleGroupID === _.id) {
                const exists = dtgroup.customByAssetList.find((ca) => ca.id === parameterSet.vehicleGroupID);
                if (exists) {
                    name = exists.name;
                }
            } else if (parameterSet.humanResourceGroupID && parameterSet.humanResourceGroupID === _.id) {
                const exists = dtgroup.customByAssetList.find((ca) => ca.id === parameterSet.humanResourceGroupID);
                if (exists) {
                    name = exists.name;
                }
            } else if (parameterSet.vehicleTypeID && parameterSet.vehicleTypeID === _.id) {
                const exists = dtgroup.customByAssetList.find((ca) => ca.id === parameterSet.vehicleTypeID);
                if (exists) {
                    name = exists.name;
                }
            }
        });
        return name;
    }



    // =========================
    // DATAPOINT HANDLERS
    // =========================

    /**
     * Upon data point load, sync the datapoint ids with their respective names, to show to the user
     * @param data The full list of datapoints
     * @param dtgroup The device type group the datapoints belongs to
     */
    public onDataPointLoad(data: DataPointsEventItem, dtgroup: DeviceTypeGroupParameterSet) {
        data.datapoints.forEach((dtp) => {
            if (dtgroup.dataPointIDs && dtgroup.dataPointIDs.find((x) => x === dtp.id)) {
                dtp.enabled = false;
                dtgroup.dataPoints.push(dtp);

                dtgroup.driveCycleParameterSets.forEach((dc) => {
                    const ps = dc.datapointParameterSets.find((x) => x.dataPointID === dtp.id);
                    if (ps) {
                        ps.name = dtp.name;
                    }
                });
            }
        });
    }


    /**
     * Add a datapoint to the list, and the respective records to the parameter sets
     * @param datapoint The datapoint to be added
     * @param dtgroup The device type group the datapoint belongs to
     */
    public onDataPointSelection(datapoint: KeyName, dtgroup: DeviceTypeGroupParameterSet) {
        if (!dtgroup.dataPointIDs.find((_) => _ === datapoint.id)) {
            // add datapoint to the list
            dtgroup.dataPointIDs.push(datapoint.id);
            dtgroup.dataPoints.push(datapoint);

            // disable datapoint option from the respective DDL
            datapoint.enabled = false;
        }

        // in case none drive cycle is enabled, enable the Default one and add the datapoint to it
        if (dtgroup.driveCycleParameterSets.length === 0) {
            const defaultDC = this.driveCycles[0];
            this.addDefaultParameterSetToDriveCycle(defaultDC.id, defaultDC.name, dtgroup);
        }

        // add data point to all drive cycles of the device type group
        dtgroup.driveCycleParameterSets.forEach((dc) => {
            if (dc.datapointParameterSets.find((_) => _.dataPointID === datapoint.id)) {
                return;
            }
            dc.datapointParameterSets.push({
                scoreID: this.score.id,
                dataPointID: datapoint.id,
                name: datapoint.name,
                driveCycleID: dc.driveCycleID,
                deviceTypeGroupID: dc.deviceTypeGroupID,
                driveCycleParameterSetID: 0,
                weight: 0,
                target: 0,
            } as DataPointParameterSet);
        });

        this.sortByPriority(dtgroup);
    }


    /**
     * Removes a datapoint from the list, and the respective records from the parameter set
     * @param datapoint The datapoint to be removed
     * @param dtgroup The device type group the datapoint belongs to
     */
    public onDataPointRemoval(datapoint: KeyName, dtgroup: DeviceTypeGroupParameterSet) {
        // remove datapoint from the list
        dtgroup.dataPointIDs = dtgroup.dataPointIDs.filter((x) => x !== datapoint.id);
        dtgroup.dataPoints = dtgroup.dataPoints.filter((x) => x.id !== datapoint.id);

        // enable datapoint option to the respective DDL
        datapoint.enabled = true;

        // remove parameter set records for this datapoint
        dtgroup.driveCycleParameterSets.forEach((dc) => {
            const prev = dc.datapointParameterSets.slice(0);
            const index: number = prev.findIndex((y) => y.dataPointID === datapoint.id);
            if (index > -1) {
                prev.splice(index, 1);
            }
            dc.datapointParameterSets = prev;
        });
    }


    // =========================
    // CUSTOM BY HANDLERS
    // =========================

    // upon custom by selection
    public onCustomBySelect(option: DriveCycleParameterSetCustomByOpts, dtgroup: DeviceTypeGroupParameterSet) {
        dtgroup.customBy = option;
        // remove any other custom by parameter sets apart from the default
        dtgroup.driveCycleParameterSets = dtgroup.driveCycleParameterSets
                                                 .filter((x) => !x.vehicleGroupID && !x.humanResourceGroupID && !x.vehicleTypeID);
    }


    // when driver groups finished loading
    public onLoadDriverGroups(groups: KeyName[], dtgroup: DeviceTypeGroupParameterSet, dcID: number) {
        this.addCustomByAssetListToParameterSet(groups, dtgroup, dcID);
    }
    // upon driver group selection
    public onSelectDriverGroup(hrgroup: KeyName, dtgroup: DeviceTypeGroupParameterSet, dcID: number) {
        this.onSelectCustomByAsset(hrgroup, dtgroup, dcID, DriveCycleParameterSetCustomByOpts.HRGroup);
    }

    // when vehicle groups finished loading
    public onLoadVehicleGroups(groups: KeyName[], dtgroup: DeviceTypeGroupParameterSet, dcID: number) {
        this.addCustomByAssetListToParameterSet(groups, dtgroup, dcID);
    }
    // upon vehicle group selection
    public onSelectVehicleGroup(vehgroup: KeyName, dtgroup: DeviceTypeGroupParameterSet, dcID: number) {
        this.onSelectCustomByAsset(vehgroup, dtgroup, dcID, DriveCycleParameterSetCustomByOpts.VehGroup);
    }


    // when vehicle types finished loading
    public onLoadVehicleTypes(vehtypes: KeyName[], dtgroup: DeviceTypeGroupParameterSet, dcID: number) {
        this.addCustomByAssetListToParameterSet(vehtypes, dtgroup, dcID);
    }
    // upon vehicle type selection
    public onSelectVehicleType(vehtype: KeyName, dtgroup: DeviceTypeGroupParameterSet, dcID: number) {
        this.onSelectCustomByAsset(vehtype, dtgroup, dcID, DriveCycleParameterSetCustomByOpts.VehType);
    }

    /**
     * Removes a custom parameter set from the drive cycle
     * @param parameterSet The parameter set in question
     * @param dtgroup The device type group the parameter set belongs to
     */
    public onDriveCycleParameterSetRemove(parameterSet: DriveCycleParameterSet, dtgroup: DeviceTypeGroupParameterSet) {
        dtgroup.driveCycleParameterSets = dtgroup.driveCycleParameterSets
                                                 .filter((dc) => JSON.stringify(dc) !== JSON.stringify(parameterSet));
        const parameterSetsOfDC = this.getParameterSetsForDriveCycle(dtgroup, parameterSet.driveCycleID);

        parameterSetsOfDC.forEach((dc, index) => {
                            dc.priority = index + 1;
                            const option = dtgroup.customByAssetList
                                            .find((_) =>
                                                    (_.id === parameterSet.vehicleGroupID && parameterSet.vehicleGroupID > 0) ||
                                                    (_.id === parameterSet.humanResourceGroupID && parameterSet.humanResourceGroupID > 0) ||
                                                    (_.id === parameterSet.vehicleTypeID && parameterSet.vehicleTypeID > 0)
                                            );
                            if (option) {
                                option.enabled = true;
                            }
                        });


    }

    /**
     * Increase priority of a parameter set
     * @param parameterSet The parameter set in question
     * @param dtgroup The device type group the parameter set belongs to
     */
    public onDriveCycleParameterSetPriorityUp(parameterSet: DriveCycleParameterSet, dtgroup: DeviceTypeGroupParameterSet) {
        const parameterSetsOfDC = this.getParameterSetsForDriveCycle(dtgroup, parameterSet.driveCycleID);
        const currentIndex =  parameterSetsOfDC.findIndex((dc) => JSON.stringify(dc) === JSON.stringify(parameterSet));
        const prevIndex = currentIndex - 1;
        if (!currentIndex || parameterSet.priority < 3 || !parameterSetsOfDC[prevIndex]) {
            return;
        }

        parameterSet.priority = parameterSet.priority - 1;
        parameterSetsOfDC[prevIndex].priority = parameterSetsOfDC[prevIndex].priority + 1;
        // trigger UI change (angular change detection)
        this.sortByPriority(dtgroup);
    }

    /**
     * Decrease priority of a parameter set
     * @param parameterSet The parameter set in question
     * @param dtgroup The device type group the parameter set belongs to
     */
    public onDriveCycleParameterSetPriorityDown(parameterSet: DriveCycleParameterSet, dtgroup: DeviceTypeGroupParameterSet) {
        const parameterSetsOfDC = this.getParameterSetsForDriveCycle(dtgroup, parameterSet.driveCycleID);

        const currentIndex =  parameterSetsOfDC.findIndex((dc) => JSON.stringify(dc) === JSON.stringify(parameterSet));
        const nextIndex = currentIndex + 1;
        if (!currentIndex || parameterSet.priority === 1 || !parameterSetsOfDC[nextIndex]) {
            return;
        }

        parameterSet.priority = parameterSet.priority + 1;
        parameterSetsOfDC[nextIndex].priority = parameterSetsOfDC[nextIndex].priority - 1;
        // trigger UI change (angular change detection)
        this.sortByPriority(dtgroup);
    }

    /**
     * Whether or not to show the descrease priority button in a parameter set
     * @param parameterSet The parameter set in question
     * @param dtgroup The device type group the parameter set belongs to
     */
    public hideDescreasePriorityBtn(parameterSet: DriveCycleParameterSet, dtgroup: DeviceTypeGroupParameterSet) {
        if (!dtgroup.driveCycleParameterSets) {
            return true;
        }
        const parameterSetsOfDC = this.getParameterSetsForDriveCycle(dtgroup, parameterSet.driveCycleID);
        return parameterSetsOfDC.length === parameterSet.priority;
    }


    // ===================================
    // COPY PARAMETER SET TO DRIVE CYCLE
    // ===================================

    /**
     * Gets the values of the DDL used to select a drive cycle to copy parameter set configuration
     * @param dcID The drive cycle's data id
     */
    public getCopyToDriveCycleOptions(dcID: number): KeyName[] {
        const options: KeyName[] = [{
            id: -1,
            name: 'None',
            term: 'form.actions.none',
            enabled: true,
        }];
        this.driveCycles.forEach((_) => {
            const item = Object.assign({}, _) as KeyName;
            item.enabled = true;
            if (item.id === dcID) {
                item.enabled = false;
            }
            options.push(item);
        });
        return options;
    }

    /**
     * Copies a parameter set's configuration to another drive cycle.
     * If the parameter set is found in the drive cycle, then its values will be updated, otherwise a new parameter set is added to the list
     * @param dcID The drive cycle's data id
     * @param parameterSet The parameter set to be copied
     * @param dtgroup The device type group the parameter set belong's to
     */
    public onCopyToDriveCycle(dcID: number, parameterSet: DriveCycleParameterSet, dtgroup: DeviceTypeGroupParameterSet) {
        let parameterSetsOfDC = this.getParameterSetsForDriveCycle(dtgroup, dcID);
        // if default parameter set is missing add it
        if (parameterSetsOfDC.length === 0) {
            const dc = this.driveCycles.find((_) => _.id === dcID);
            this.addDefaultParameterSetToDriveCycle(dc.id, dc.name, dtgroup);
            parameterSetsOfDC = this.getParameterSetsForDriveCycle(dtgroup, dcID);
        }

        // check if parameter set to copy exists
        const setExists = parameterSetsOfDC.find((_) =>
                        (!_.vehicleGroupID && !parameterSet.vehicleGroupID
                            && !_.humanResourceGroupID && !parameterSet.humanResourceGroupID
                            && !_.vehicleTypeID && !parameterSet.vehicleTypeID) ||
                        (_.vehicleGroupID && _.vehicleGroupID === parameterSet.vehicleGroupID) ||
                        (_.humanResourceGroupID && _.humanResourceGroupID === parameterSet.humanResourceGroupID) ||
                        (_.vehicleTypeID && _.vehicleTypeID === parameterSet.vehicleTypeID)
                );

        if (setExists) {
            setExists.maxValue = parameterSet.maxValue;
            setExists.datapointParameterSets = JSON.parse(JSON.stringify(parameterSet.datapointParameterSets));
        } else {
            const newSet = {
                id: 0,
                driveCycleID: dcID,
                scoreID: this.score.id,
                deviceTypeGroupID: parameterSet.deviceTypeGroupID,
                maxValue: parameterSet.maxValue,
                humanResourceGroupID: parameterSet.humanResourceGroupID,
                vehicleGroupID: parameterSet.vehicleGroupID,
                vehicleTypeID: parameterSet.vehicleTypeID,
                priority: parameterSetsOfDC.length + 1,
                datapointParameterSets: [],
                showStats: false
            } as DriveCycleParameterSet;


            parameterSet.datapointParameterSets.forEach((_) => {
                const dp =  {
                    scoreID: this.score.id,
                    dataPointID: _.dataPointID,
                    driveCycleID: dcID,
                    deviceTypeGroupID: _.deviceTypeGroupID,
                    driveCycleParameterSetID: 0,
                    weight: _.weight,
                    target: _.target,
                    name: _.name
                } as DataPointParameterSet;
                newSet.datapointParameterSets.push(dp);
            });
            dtgroup.driveCycleParameterSets.push(newSet);
        }

        this.sortByPriority(dtgroup);
    }

    // ============================
    // COPY PARAMETER SET TO ASSET
    // ============================

    public showCopyToAssetsDDL(parameterSet: DriveCycleParameterSet, dtgroup: DeviceTypeGroupParameterSet): boolean {
        if (dtgroup.customBy === DriveCycleParameterSetCustomByOpts.None) {
            return false;
        }
        return true;
    }

    /**
     * Gets the values of the DDL used to select an asset to copy parameter set configuration
     * @param parameterSet The parameter set for which we show the DDL
     */
    public getCopyToAssetOptions(parameterSet: DriveCycleParameterSet, dtgroup: DeviceTypeGroupParameterSet): KeyName[] {
        if (!dtgroup.customByAssetList) {
            return;
        }

        const options: KeyName[] = [];
        dtgroup.customByAssetList.forEach((_) => {
            const item = Object.assign({}, _) as KeyName;
            item.enabled = true;
            // the asset of the parameter set to be copied, should be disabled in the list
            // (no use of copying to itself)
            if (
                (parameterSet.vehicleGroupID && parameterSet.vehicleGroupID === _.id) ||
                (parameterSet.humanResourceGroupID && parameterSet.humanResourceGroupID === _.id) ||
                (parameterSet.vehicleTypeID && parameterSet.vehicleTypeID === _.id)
            ) {
                item.enabled = false;
            }
            options.push(item);
        });
        return options;
    }

    /**
     * Copies a parameter set's configuration to a another one for a specified asset (i.e. vehicle group).
     * If the parameter set's asset is found, then its values will be updated, otherwise a new parameter set is added to the list
     * @param assetID The data id of the asset used for copying
     * @param parameterSet The parameter set to be copied
     * @param dtgroup The device type group, the parameter set is attached to
     */
    public onCopyToAsset(assetID: number, parameterSet: DriveCycleParameterSet, dtgroup: DeviceTypeGroupParameterSet) {
        if (!assetID || assetID < 1) {
            return;
        }
        const parameterSetsOfDC = this.getParameterSetsForDriveCycle(dtgroup, parameterSet.driveCycleID);
        const setExists = parameterSetsOfDC.find((_) =>
                (_.vehicleGroupID && _.vehicleGroupID === assetID) ||
                (_.humanResourceGroupID && _.humanResourceGroupID === assetID) ||
                (_.vehicleTypeID && _.vehicleTypeID === assetID)
                );

        if (setExists) {
            setExists.maxValue = parameterSet.maxValue;
            setExists.datapointParameterSets = JSON.parse(JSON.stringify(parameterSet.datapointParameterSets));
        } else {
            const newSet = {
                id: 0,
                driveCycleID: parameterSet.driveCycleID,
                scoreID: this.score.id,
                deviceTypeGroupID: parameterSet.deviceTypeGroupID,
                maxValue: parameterSet.maxValue,
                humanResourceGroupID: null,
                vehicleGroupID: null,
                vehicleTypeID: null,
                priority: parameterSetsOfDC.length + 1,
                datapointParameterSets: [],
                showStats: false
            } as DriveCycleParameterSet;

            parameterSet.datapointParameterSets.forEach((_) => {
                const dp =  {
                    scoreID: this.score.id,
                    dataPointID: _.dataPointID,
                    driveCycleID: _.driveCycleID,
                    deviceTypeGroupID: _.deviceTypeGroupID,
                    driveCycleParameterSetID: 0,
                    weight: _.weight,
                    target: _.target,
                    name: _.name
                } as DataPointParameterSet;
                newSet.datapointParameterSets.push(dp);
            });

            switch (dtgroup.customBy) {
                case DriveCycleParameterSetCustomByOpts.VehGroup: {
                    newSet.vehicleGroupID = assetID;
                    break;
                }
                case DriveCycleParameterSetCustomByOpts.HRGroup: {
                    newSet.humanResourceGroupID = assetID;
                    break;
                }
                case DriveCycleParameterSetCustomByOpts.VehType: {
                    newSet.vehicleTypeID = assetID;
                    break;
                }
            }

            dtgroup.driveCycleParameterSets.push(newSet);
        }
        this.sortByPriority(dtgroup);

    }


    // =========================
    // STATS GRAPHS
    // =========================

    public onShowStatsChange(parameterSet: DriveCycleParameterSet) {
        this.score.deviceTypeGroupParameterSets.forEach((dtg) => {
            dtg.driveCycleParameterSets.forEach((dc) => {
                if (dc.driveCycleID === parameterSet.driveCycleID && dc.deviceTypeGroupID === parameterSet.deviceTypeGroupID) {
                    return;
                }

                dc.showStats = false;
            });
        });
    }


    // =========================
    // FORM FIELD DISCRIMINATOR
    // =========================

    /**
     * Construct html form field discriminator for parameter sets iteration (we need unique html ids)
     * @param parameterSet The parameter set in question
     */
    public getFormFiedDisciminator(parameterSet: DriveCycleParameterSet) {
        const discriminator = parameterSet.driveCycleID.toString() + parameterSet.deviceTypeGroupID.toString();
        if (parameterSet.vehicleGroupID) {
            return discriminator + '_ff_vg' + String(parameterSet.vehicleGroupID);
        }

        if (parameterSet.humanResourceGroupID) {
            return discriminator + '_ff_hg' + String(parameterSet.humanResourceGroupID);
        }

        if (parameterSet.vehicleTypeID) {
            return discriminator + '_ff_vt' + String(parameterSet.vehicleTypeID);
        }
        return discriminator;
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: DriveScoreSettings, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }

        const req = Object.assign({}, this.score);
        req.deviceTypeGroupParameterSets = req.deviceTypeGroupParameterSets
                                              .filter((_) => _.enabled === true && _.driveCycleParameterSets.length > 0);

        this.loading = true;
        this.unsubscribeService();

        // This is create
        if (!this.score.id || this.score.id === 0) {
            this.serviceSubscriber = this.service
                .create(req)
                .subscribe((res: DriveScoreUpsertCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const errorMessage = this.getUpsertResult(res);
                        this.snackBar.open(errorMessage, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else {
        // This is an update
        this.serviceSubscriber = this.service
            .update(req)
            .subscribe((res: DriveScoreUpsertCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    const errorMessage = this.getUpsertResult(res);
                    this.snackBar.open(errorMessage, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
        }
    }


    /**
     * Fetch the drive score definition
     * @param id The score id in request
     */
    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((res: DriveScoreSettings) => {
                this.loading = false;
                this.score.id = res.id;
                this.score.name = res.name;
                this.score.isDefault = res.isDefault;
                this.score.type = res.type;
                this.score.weightBy = res.weightBy;
                this.score.scorePercentages = res.scorePercentages;

                this.score.deviceTypeGroupParameterSets.forEach((dtg) => {
                    dtg.scoreID = res.id;

                    const dtgEnabled = res.deviceTypeGroupParameterSets.find((x) => x.deviceTypeGroupID === dtg.deviceTypeGroupID);
                    if (dtgEnabled) {
                        dtg.enabled = true;
                        dtg.customBy = dtgEnabled.customBy;
                        dtg.dataPointIDs = dtgEnabled.dataPointIDs;
                        dtg.driveCycleParameterSets = dtgEnabled.driveCycleParameterSets;
                    }
                });

                this.score.deviceTypeGroupParameterSets.forEach((dtgroup) => {
                    this.sortByPriority(dtgroup);
                });
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }

    // =========================
    // PRIVATE
    // =========================

    /**
     * Intantiate the score object, used in the form
     */
    private initFormValues(): void {
        this.score = {
            id: 0,
            name: null,
            isDefault: false,
            type: DriveScoreTypes.rewarding,
            weightBy: WeightByOptions.Distance,
            deviceTypeGroupParameterSets: []
        } as DriveScoreSettings;

        this.deviceTypeGroups.forEach((dtg) => {
            const item = {
                id: null,
                scoreID: this.score.id,
                deviceTypeGroupID: dtg.id,
                customBy: DriveCycleParameterSetCustomByOpts.None,
                dataPointIDs: [],
                dataPoints: [],
                driveCycleParameterSets: [],
                name: dtg.name,
                enabled: false,
            } as DeviceTypeGroupParameterSet;

            this.score.deviceTypeGroupParameterSets.push(item);
        });
    }

    /**
     * Creates a new parameter set for the selected asset, based on the default parameter set's configuration
     * @param option The asset to be used for customization
     * @param dtgroup The device type group
     * @param dcID The drive cycle's data id
     * @param customBy The selected custom by option
     */
    private onSelectCustomByAsset(option: KeyName,
                                  dtgroup: DeviceTypeGroupParameterSet,
                                  dcID: number,
                                  customBy: DriveCycleParameterSetCustomByOpts) {
        if (!option || !option.id || option.id < 1) {
            return;
        }

        // disable the option from the DDL
        option.enabled = false;

        // if default parameter set is missing add it
        let parameterSetsOfDC = this.getParameterSetsForDriveCycle(dtgroup, dcID);
        if (parameterSetsOfDC.length === 0) {
            const dc = this.driveCycles.find((_) => _.id === dcID);
            this.addDefaultParameterSetToDriveCycle(dc.id, dc.name, dtgroup);
            parameterSetsOfDC = this.getParameterSetsForDriveCycle(dtgroup, dcID);
        }

        // remove any other asset type customization different than the type of the selected
        switch (customBy) {
            case DriveCycleParameterSetCustomByOpts.VehGroup: {
                parameterSetsOfDC = parameterSetsOfDC.filter((y) => !y.vehicleTypeID && !y.humanResourceGroupID);
                break;
            }
            case DriveCycleParameterSetCustomByOpts.HRGroup: {
                parameterSetsOfDC = parameterSetsOfDC.filter((y) => !y.vehicleGroupID && !y.vehicleTypeID);
                break;
            }
            case DriveCycleParameterSetCustomByOpts.VehType: {
                parameterSetsOfDC = parameterSetsOfDC.filter((y) => !y.vehicleGroupID && !y.humanResourceGroupID);
                break;
            }
        }

        // add the new parameter set, by copying the default one and make the respective changes (priorioty, asset type)
        const priorityNew = parameterSetsOfDC.length + 1;
        const defaultSet = parameterSetsOfDC.find((x) => !x.vehicleGroupID && !x.humanResourceGroupID && !x.vehicleTypeID);

        const newSet = {
            id: 0,
            driveCycleID: defaultSet.driveCycleID,
            scoreID: defaultSet.scoreID,
            deviceTypeGroupID: defaultSet.deviceTypeGroupID,
            maxValue: defaultSet.maxValue,
            name: option.name,
            priority: priorityNew,
            datapointParameterSets: [],
            enabled: true,
            vehicleGroupID: null,
            humanResourceGroupID: null,
            vehicleTypeID: null
        } as DriveCycleParameterSet;

        defaultSet.datapointParameterSets.forEach((_) => {
            const dp =  {
                scoreID: this.score.id,
                dataPointID: _.dataPointID,
                driveCycleID: _.driveCycleID,
                deviceTypeGroupID: _.deviceTypeGroupID,
                driveCycleParameterSetID: 0,
                weight: 0,
                target: 0,
                name: _.name
            } as DataPointParameterSet;
            newSet.datapointParameterSets.push(dp);
        });

        switch (customBy) {
            case DriveCycleParameterSetCustomByOpts.VehGroup: {
                newSet.vehicleGroupID = option.id;
                break;
            }
            case DriveCycleParameterSetCustomByOpts.HRGroup: {
                newSet.humanResourceGroupID = option.id;
                break;
            }
            case DriveCycleParameterSetCustomByOpts.VehType: {
                newSet.vehicleTypeID = option.id;
                break;
            }
        }

        dtgroup.driveCycleParameterSets.push(newSet);

        // trigger change detection
        this.sortByPriority(dtgroup);
    }


    /**
     * Gets the list of parameter sets within a specified drive cycle
     * @param dtgroup The device type group
     * @param dcID The drive cycle's data id
     */
    private getParameterSetsForDriveCycle(dtgroup: DeviceTypeGroupParameterSet, dcID: number): DriveCycleParameterSet[] {
        return dtgroup.driveCycleParameterSets ? dtgroup.driveCycleParameterSets.filter((x) => x.driveCycleID === dcID) : [];
    }

    /**
     * Save the list of assets that are used for parameter set customization.
     * This is needed so that we can enable an item in the list after parameter set removal.
     * @param list The list of assets used for customization
     * @param dtgroup The device type group
     * @param dcID The drive cycle's data id
     */
    private addCustomByAssetListToParameterSet(list: KeyName[], dtgroup: DeviceTypeGroupParameterSet, dcID: number): void {
        const none = {
            id: null,
            name: 'None',
            term: 'score_definition.add.none',
            enabled: true,
        };

        dtgroup.customByAssetList = list;

        const parameterSetsOfDC = this.getParameterSetsForDriveCycle(dtgroup, dcID);
        parameterSetsOfDC.forEach((dc) => {
            if (dc.vehicleGroupID) {
                list.find((x) => x.id === dc.vehicleGroupID).enabled = false;
            } else if (dc.vehicleTypeID) {
                list.find((x) => x.id === dc.vehicleTypeID).enabled = false;
            } else if (dc.humanResourceGroupID) {
                list.find((x) => x.id === dc.humanResourceGroupID).enabled = false;
            }
        });
    }

    private addDefaultParameterSetToDriveCycle(dcID: number, dcName: string, dtgroup: DeviceTypeGroupParameterSet): void {
        const dcParam = {
            id: 0,
            driveCycleID: dcID,
            scoreID: this.score.id,
            deviceTypeGroupID: dtgroup.deviceTypeGroupID,
            maxValue: 0,
            humanResourceGroupID: null,
            vehicleGroupID: null,
            vehicleTypeID: null,
            priority: 1,
            name: dcName,
            enabled: true,
            datapointParameterSets: [],
        } as DriveCycleParameterSet;
        dtgroup.driveCycleParameterSets.push(dcParam);

        dtgroup.dataPoints.forEach((dp) => {
            this.onDataPointSelection(dp, dtgroup);
        });
    }

    /**
     * Sort parameter sets within a device type group, based on their priority
     * @param dtgroup The device type group
     */
    private sortByPriority(dtgroup: DeviceTypeGroupParameterSet): void {
        dtgroup.driveCycleParameterSets = dtgroup.driveCycleParameterSets.sort((x, y) => x.priority - y.priority);
    }


    // =========================
    // LISTS
    // =========================

    /**
     * Get the list of weight by options
     */
    private getWeightByOptions(): KeyName[] {
        return [{
            id: WeightByOptions.Distance,
            name: 'distance',
            term: 'score_definition.add.distance',
            enabled: true
        },
        {
            id: WeightByOptions.Duration,
            name: 'duration',
            term: 'score_definition.add.duration',
            enabled: true
        }];

    }


    /**
     * Get the list of drive score types
     */
    private getDriveScoreTypeOptions(): KeyName[]  {
        return [{
            id: DriveScoreTypes.penalizing,
            name: 'penalizing',
            term: 'score_definition.add.penalizing',
            enabled: true
        },
        {
            id: DriveScoreTypes.rewarding,
            name: 'rewarding',
            term: 'score_definition.add.rewarding',
            enabled: true
        }];
    }

    /**
     * Get the list of custom by options
     */
    private getCustomByOptions(): KeyName[]  {
        return [{
            id: DriveCycleParameterSetCustomByOpts.None,
            name: 'None',
            term: 'score_definition.add.none',
            enabled: true,
        },
        {
            id: DriveCycleParameterSetCustomByOpts.HRGroup,
            name: 'Driver group',
            term: 'score_definition.add.hr_group',
            enabled: true,
        },
        {
            id: DriveCycleParameterSetCustomByOpts.VehGroup,
            name: 'Vehicle group',
            term: 'score_definition.add.vehicle_group',
            enabled: true,
        },
        {
            id: DriveCycleParameterSetCustomByOpts.VehType,
            name: 'Vehicle type',
            term: 'score_definition.add.vehicle_type',
            enabled: true,
        }];
    }

    /**
     * Fetch and save the list of device type groups
     */
    private setDeviceTypeGroups(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.loading = true;
            this.service
                .getDeviceTypeGroups()
                .subscribe((res: KeyName[]) => {
                    this.loading = false;
                    this.deviceTypeGroups = res;
                    resolve();
                },
                (err) => {
                    this.loading = false;
                    this.snackBar.open('data.error', 'form.actions.ok');
                    reject();
                }
            );
        });
    }

    /**
     * Fetch and save the list of drive cycles
     */
    private setDriveCycles(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.loading = true;
            this.driveCyclesService
                .get()
                .subscribe((res: KeyName[]) => {
                    this.loading = false;
                    this.driveCycles = res;
                    resolve();
                },
                (err) => {
                    this.loading = false;
                    this.snackBar.open('data.error', 'form.actions.ok');
                    reject();
                }
            );
        });
    }

    // =========================
    // RESULTS
    // =========================

    private getUpsertResult(code: DriveScoreUpsertCodeOptions): string {
        let message = '';
        switch (code) {
            case DriveScoreUpsertCodeOptions.NameAlreadyExists:
                message = 'form.errors.name_exists';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
