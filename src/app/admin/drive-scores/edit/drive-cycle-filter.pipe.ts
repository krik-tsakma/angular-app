﻿// FRAMEWORK
import { Pipe, PipeTransform } from '@angular/core';

// TYPES
import { DriveCycleParameterSet } from './../drive-score-types';

@Pipe({
    name: 'myDriveCycleFilter',
    pure: false
})

export class DriveCycleFilterPipe implements PipeTransform {
    constructor() {
        // foo
    }

    public transform(driveCycleSets: DriveCycleParameterSet[], driveCycleID: number): DriveCycleParameterSet[] {
        if (!driveCycleSets) {           
            return driveCycleSets;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return driveCycleSets.filter((item) => item.driveCycleID === driveCycleID);
    }
}
