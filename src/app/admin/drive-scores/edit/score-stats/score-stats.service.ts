
// FRAMEWORK
import { Injectable } from '@angular/core';

// RxJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';

// SERVICES
import { Config } from './../../../../config/config';
import { AuthHttp } from './../../../../auth/authHttp';

// TYPES
import { ScoreStatsRequest } from './score-stats-types';
import { ScoreStatsResult } from './score-stats-types';

@Injectable()
export class DriveScoreStatsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/DriveScoreStats';
        }

    // Get  drive score statistics
    public get(request: ScoreStatsRequest): Observable<ScoreStatsResult> {
        return this.authHttp
            .post(this.baseURL, request);
    }
}
