import { DistributionTypes } from './../../../../common/distribution-types/distribution-types';
import { DriveScoreSettings } from './../../drive-score-types';
import { PeriodTypes } from './../../../../common/period-selector/period-selector-types';
import { AssetTypes } from './../../../../common/entities/entity-types';

export interface ScoreStatsRequest {
    assetType: AssetTypes;
    groupID: number;
    vehicleTypeID: number;
    periodType: PeriodTypes;
    minimum: number;
    score: DriveScoreSettings;
    distributeBy: DistributionTypes;
}

export interface ScoreStatsResult {
    totalDistance: number;
    totalDuration: number;
    trips: number;
    inDefaultRangePerc: number;
    minScore: number;
    maxScore: number;
    aggregations: ScoreValueAggregation[];
    slices: ScoreLetterSlice[];
}

export interface ScoreValueAggregation {
    value: number;
    aggregation: number;
}

export interface ScoreLetterSlice {
    letter: string;
    perc: number;
}
