// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from './../../../../shared/shared.module';
import { PeriodSelectorModule } from './../../../../common/period-selector/period-selector.module';
import { DistributionTypesModule } from './../../../../common/distribution-types/distribution-types.module';
import { AssetGroupsModule } from './../../../../common/asset-groups/asset-groups.module';
import { VehicleTypesListModule } from './../../../../common/vehicle-types-list/vehicle-types-list.module';

// SERVICES
import { DriveScoreStatsService } from './score-stats.service';

// COMPONENTS
import { DriveScoreStatsComponent } from './score-stats.component';


@NgModule({
    imports: [
        SharedModule,
        PeriodSelectorModule,
        AssetGroupsModule,
        DistributionTypesModule,
        VehicleTypesListModule,
    ],
    declarations: [
        DriveScoreStatsComponent,
    ],
    exports: [ 
        DriveScoreStatsComponent,
    ],
    providers: [
        DriveScoreStatsService,
    ]
})
export class DriveScoreStatsModule { }
