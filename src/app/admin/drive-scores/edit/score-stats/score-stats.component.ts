import { DeviceTypeGroupParameterSet, DriveCycleParameterSet } from './../../drive-score-types';
// FRAMEWORK
import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation, Input } from '@angular/core';
import { NgForm } from '@angular/forms';

// RxJS
import { Subscription } from 'rxjs';

// SERVICES
import { UnsubscribeService } from './../../../../shared/unsubscribe.service';
import { DriveScoreStatsService } from './score-stats.service';
import { CustomTranslateService } from './../../../../shared/custom-translate.service';
import { UserOptionsService } from './../../../../shared/user-options/user-options.service';

// TYPES
import { EntityTypes } from './../../../../shared/entity-types/entity-types';
import { DistributionTypes } from './../../../../common/distribution-types/distribution-types';
import { PeriodTypes } from './../../../../common/period-selector/period-selector-types';
import { AmChartTypes } from './../../../../amcharts/amchart-types';
import { AssetTypes } from './../../../../common/entities/entity-types';
import { AssetGroupItem } from './../../../asset-groups/asset-groups.types';
import { KeyName } from './../../../../common/key-name';
import { ScoreStatsRequest, ScoreStatsResult } from './score-stats-types';
import { DriveScoreSettings } from '../../drive-score-types';
import { PeriodTypeOption } from './../../../../common/period-selector/period-selector-types';
import { AssetGroupTypes } from './../../../asset-groups/asset-groups.types';
import { LocalOptions } from './../../../../shared/user-options/user-options-types';

@Component({
    selector: 'score-stats',
    templateUrl: './score-stats.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./score-stats.less'],
})
export class DriveScoreStatsComponent implements OnInit, OnDestroy {
    @ViewChild('statsForm', { static: true }) public statsForm: NgForm;
    @Input() public score: DriveScoreSettings;
    @Input() public dtgroup: DeviceTypeGroupParameterSet;
    @Input() public drivecycle: DriveCycleParameterSet;

    public request: ScoreStatsRequest = {} as ScoreStatsRequest;
    public stats: ScoreStatsResult = null;
    public loading: boolean;
    public subscriber: Subscription;
    public error: string;

    // DDL
    public periodSelectorOptions: PeriodTypes[] = [];

    public groupTypes: any = AssetGroupTypes;
    public distributionTypes: any = DistributionTypes;

    public entityTypeOptions: EntityTypes[] = [];
    public entityTypes: any = EntityTypes;
    public entityOption: EntityTypes;

    // CHART
    public chartTypes = AmChartTypes;

    public localOptions: LocalOptions;

    // i18n
    private minimumLabel: string;
    private scoreStatsTranslations: any;
    private hoursTranslation: string;

    constructor(private translate: CustomTranslateService,
                private unsubscriber: UnsubscribeService,
                private userOptions: UserOptionsService,
                private service: DriveScoreStatsService) {
        // get translations
        translate
            .get([
                'time.hours',
                'score_stats.occurrences_yaxis',
                'score_stats.duration_yaxis',
                'score_stats.distance_yaxis',
                'score_stats.distribution_chart',
                'score_stats.score_labels_chart',
            ])
            .subscribe((t: any) => {
                this.scoreStatsTranslations =  {
                    minOccurences:  t['score_stats.occurrences_yaxis'],
                    minDuration:  t['score_stats.duration_yaxis'],
                    minDistance:  t['score_stats.distance_yaxis'],
                    distributionChart:  t['score_stats.distribution_chart'],
                    scoreLabelsChart:  t['score_stats.score_labels_chart']
                };
                this.hoursTranslation = t['time.hours'];
                this.setMinimumLabel();
            });
    }

    public ngOnInit(): void {
        this.localOptions = this.userOptions.localOptions;
        this.periodSelectorOptions = this.getPeriodTypes();
        this.entityTypeOptions = this.getEntityTypeOptions();
        this.initFormValues();
    }

    public ngOnDestroy(): void {
        this.unsubscriber.removeSubscription(this.subscriber);
    }


    // =========================
    // PERIOD TYPS  CALLBACKS
    // =========================

    // On change period
    public onPeriodChange(pto: PeriodTypeOption): void {
        this.request.periodType = pto.id;
        this.get();
    }


    // =========================
    // ENTITY TYPES CALLBACKS
    // =========================

    public onChangeEntityTypeGroup(newType: EntityTypes): void {
        this.entityOption = Number(newType);
        this.request.groupID = null;
        this.request.vehicleTypeID = null;
        this.setEntityType();
        this.get();
    }


    // =========================
    // ASSET GROUP CALLBACKS
    // =========================

    // When the user selects a driver group from the DDL
    public onChangeGroup(newGroup?: AssetGroupItem): void {
        this.request.groupID = newGroup
            ? newGroup.id
            : null;

        this.request.vehicleTypeID = null;
        this.get();
    }


    public onChangeGroupVehicleTypes(vehType: KeyName): void {
        this.request.vehicleTypeID = vehType !== null && vehType.id
            ? Number(vehType.id)
            : null;

        this.request.groupID = null;
        this.get();
    }

    // =============================
    // DISTRIBUTION TYPES CALLBACK
    // =============================

    public onChangeDistributionType(newType: DistributionTypes): void {
        this.request.distributeBy = Number(newType);
        this.setMinimumLabel();
        this.get();
    }

    public setMinimumLabel() {
        switch (this.request.distributeBy) {
            case DistributionTypes.NumOfTrips: {
                this.minimumLabel = this.scoreStatsTranslations.minOccurences + ' (#)';
                break;
            }
            case DistributionTypes.Duration: {
                this.minimumLabel = this.scoreStatsTranslations.minDuration + ' (' + this.hoursTranslation + ')';
                break;
            }
            case DistributionTypes.Distance: {
                this.minimumLabel = this.scoreStatsTranslations.minDistance + ' (' + this.userOptions.metrics.distance + ')';
                break;
            }
            default: {
                this.minimumLabel = this.scoreStatsTranslations.minOccurences + ' (#)';
                break;
            }
        }
    }


    // =========================
    // DRAW GRAPHS
    // =========================

    public makeOccurencesChart() {
        return {
            type: 'serial',
            titles: [
                {
                    text: this.scoreStatsTranslations.distributionChart,
                    size: 15
                }
            ],
            dataProvider: this.stats.aggregations,
            valueAxes: [{
                gridColor: '#FFFFFF',
                gridAlpha: 0.2,
                dashLength: 0,
                title: this.minimumLabel
            }],
            gridAboveGraphs: true,
            startDuration: 0,
            graphs: [{
                balloonText: '[[category]]: <b>[[value]]</b>',
                fillAlphas: 0.8,
                lineAlpha: 0.2,
                type: 'column',
                valueField: 'aggregation',
                fillColors: '#67b7dc',
            }],
            chartCursor: {
                categoryBalloonEnabled: false,
                cursorAlpha: 0,
                zoomable: false
            },
            categoryField: 'value',
            responsive: {
                enabled: true
            }
        };
    }



    public makePieChart() {
        return {
            type: 'pie',
            titles: [
                {
                    text: this.scoreStatsTranslations.scoreLabelsChart,
                    size: 15
                }
            ],
            // Disable animation
            startDuration: 0,
            legend: {
                position: 'bottom',
                align: 'center',
                autoMargins: true,
                // Empty to hide the value of the pie piece
                valueText: '',
                markerLabelGap: -11,
                color: '#ffffff',
                switchable: false
            },
            // Hole on center of piece
            innerRadius: '0%',
            dataProvider: this.stats.slices,
            valueField: 'perc',
            titleField: 'letter',
            colors: ['#00a74e', '#4bba42', '#b3ca15', '#ffd80f', '#feba00', '#f57000', '#e00001', '#a10202'],
            balloon: {
                fixedPosition: true
            },
            balloonText: '[[title]]: [[percents]]%',
            decimalSeparator: this.localOptions.decimal_separator,
            responsive: {
                enabled: true
            }
        };
    }


    // =========================
    // PRIVATE
    // =========================

    private get(): void {
        if (!this.request || this.request.minimum < 2) {
            return;
        }
        this.loading = true;

        this.unsubscriber.removeSubscription(this.subscriber);

        const score = {
            id: 0,
            name: this.score.name,
            isDefault: this.score.isDefault,
            type: this.score.type,
            weightBy: this.score.weightBy,
            scorePercentages: this.score.scorePercentages,
            deviceTypeGroupParameterSets: [{
                id: 0,
                scoreID: 0,
                deviceTypeGroupID: this.dtgroup.deviceTypeGroupID,
                customBy: this.dtgroup.customBy,
                dataPointIDs: this.dtgroup.dataPointIDs,
                driveCycleParameterSets: [this.drivecycle]
            }]
        };

        this.request.score = score;

        this.subscriber = this.service
            .get(this.request)
            .subscribe((res: ScoreStatsResult) => {
                this.loading = false;
                if (res && (res.aggregations || res.slices)) {
                    this.stats = res;
                } else {
                    this.stats = null;
                    this.error = 'data.empty_records';
                }
            },
            (err) => {
                this.loading = false;
                this.stats = null;
                this.error = 'data.error';
            });
    }

    private initFormValues() {
        this.entityOption = EntityTypes.Vehicles;
        this.request = {
            assetType: AssetTypes.Vehicle,
            groupID: null,
            vehicleTypeID: null,
            periodType: PeriodTypes.YearToDate,
            minimum: 50,
            distributeBy: DistributionTypes.NumOfTrips
        } as ScoreStatsRequest;
    }

    private getPeriodTypes() {
        return [
            PeriodTypes.FiscalYearToDate,
            PeriodTypes.Last12Months,
            PeriodTypes.LastMonth,
            PeriodTypes.LastWeek,
            PeriodTypes.PreviousFiscalYear,
            PeriodTypes.ThisMonth,
            PeriodTypes.ThisWeek,
            PeriodTypes.YearToDate
        ];
    }

    private getEntityTypeOptions() {
        return [
            EntityTypes.Vehicles,
            EntityTypes.Drivers,
            EntityTypes.VehicleTypes
        ];
    }

    private setEntityType() {
        switch (this.entityOption) {
            case EntityTypes.Drivers:
                this.request.assetType = AssetTypes.HR;
                break;
            case EntityTypes.Vehicles:
                this.request.assetType = AssetTypes.Vehicle;
                break;
            case EntityTypes.VehicleTypes:
                this.request.assetType = AssetTypes.Vehicle;
                break;
            default:
                break;
        }
    }
}
