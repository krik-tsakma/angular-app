
// FRAMEWORK
import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

// SERVICES
import { DriveScoresService } from './../../drive-score.service';

// TYPES
import { DataPointsEventItem } from './datapoints-list-types';
import { KeyName } from '../../../../common/key-name';

@Component({
    selector: 'datapoints-list',
    templateUrl: './datapoints-list.html'
})

export class DataPointsListComponent implements OnChanges {
    @Input() public dtgroupid: number;
    @Input() public modelData?: number;
    @Output() public modelDataChange: EventEmitter<KeyName> =
        new EventEmitter<KeyName>();
    @Output() public modelDataOnLoad: EventEmitter<DataPointsEventItem> =
        new EventEmitter<DataPointsEventItem>();

    public data: KeyName[];
    public loading: boolean;

    constructor(private service: DriveScoresService) {
        this.initDataTable();
    }

    public ngOnChanges(changes: SimpleChanges): void {
        const devTypeGroupChanges = changes['dtgroupid'];
        if (devTypeGroupChanges) {
            if (devTypeGroupChanges.currentValue) {
                this.get();
            }
        }
    }

    public onChangeOption(id?: KeyName) {
        this.modelData = id ? Number(id) : null;

        const item = this.data.find((x) => x.id === id );
        this.modelDataChange.emit(item);
    }

    private get() {
        this.initDataTable();
        this.loading = true;
        this.service
            .getDataPointsForDeviceGroup(this.dtgroupid)
            .subscribe((res: KeyName[]) => {
                this.loading = false;
                res.forEach((d) => {
                    d.enabled = true;
                });
                this.data = this.data.concat(res);
               
                this.modelDataOnLoad.emit({ 
                    datapoints: this.data,
                    selected: null
                } as DataPointsEventItem);
            },
            (err) => {
                this.loading = false;
            });
    }

    private initDataTable() {
        this.data = [{
            id: null,
            name: 'Select data points',
            term: 'form.actions.select',
            enabled: false
        }];
    }
}
