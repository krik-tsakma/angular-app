import { KeyName } from '../../../../common/key-name';

export interface DataPointsEventItem {
    datapoints: KeyName[];
    selected: number;
}
