import { KeyName } from './../../common/key-name';
import { PagerResults } from './../../common/pager';

export interface DriveScoresResult extends PagerResults {
    results: DriveScoreResultItem[];
}

export interface DriveScoreResultItem {
    id: number;
    name: string;
    isDefault: boolean;
    type: DriveScoreTypes;
    weightBy: WeightByOptions;
}

export interface DriveScoreSettings extends DriveScoreResultItem {
    scorePercentages: DriveScorePercentage[];
    deviceTypeGroupParameterSets: DeviceTypeGroupParameterSet[];
}

export interface DriveScorePercentage {
    scoreID: number;
    percentage: number;
    value: number;
}

export interface DeviceTypeGroupParameterSet {
    scoreID: number;
    deviceTypeGroupID: number;
    customBy: DriveCycleParameterSetCustomByOpts;
    dataPointIDs: number[];
    driveCycleParameterSets: DriveCycleParameterSet[];

    // UI ONLY
    name?: string;
    enabled?: boolean;
    dataPoints?: KeyName[];
    customByAssetList?: KeyName[];
}

export interface DriveCycleParameterSet {
    id: number;
    driveCycleID: number;
    scoreID: number;
    deviceTypeGroupID: number;
    maxValue: number;
    humanResourceGroupID?: number;
    vehicleGroupID?: number;
    vehicleTypeID?: number;
    priority?: number;
    datapointParameterSets: DataPointParameterSet[];

    // UI ONLY
    // customByAssets?: KeyName[];
    showStats?: boolean;
}

export interface DataPointParameterSet {
    scoreID: number;
    dataPointID: number;
    driveCycleID: number;
    driveCycleParameterSetID: number;
    deviceTypeGroupID: number;
    weight: number;
    target: number;

    // UI only
    name?: string;
}


export enum DriveCycleParameterSetCustomByOpts {
    None = 0,
    HRGroup = 1,
    VehGroup = 2,
    VehType = 3
}


export enum WeightByOptions {
    Distance = 0,
    Duration = 1
}

export enum DriveScoreTypes {
    rewarding = 0,
    penalizing = 1
}

export enum DriveScoreUpsertCodeOptions {
    Ok = 0,
    NameAlreadyExists = 1
}
