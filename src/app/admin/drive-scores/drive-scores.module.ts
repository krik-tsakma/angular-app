// FRAMEWORK
import { NgModule } from '@angular/core';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { DriveScoresRoutingModule } from './drive-scores-routing.module';
import { AssetGroupsModule } from '../../common/asset-groups/asset-groups.module';
import { VehicleTypesListModule } from './../../common/vehicle-types-list/vehicle-types-list.module';
import { DriveScoreStatsModule } from './edit/score-stats/score-stats.module';

// SERVICES
import { DriveScoresService } from './drive-score.service';
import { DriveCyclesService } from './../drive-cycles/drive-cycles.service';
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';
import { TranslateStateService } from './../../core/translateStore.service';

// COMPONENTS
import { ViewDriveScoresComponent } from './view/view-drive-scores.component';
import { EditDriveScoreComponent } from './edit/edit-drive-score.component';
import { DataPointsListComponent } from './edit/datapoints-list/datapoints-list.component';
import { DriveScorePercentagesComponent } from './edit/percentages/drive-score-percentages.component';

// PIPES
import { DriveCycleFilterPipe } from './edit/drive-cycle-filter.pipe';
import { DriveScorePercentagesSortPipe } from './edit/percentages/drive-score-percentages-sort.pipe';
import { HttpClient } from '@angular/common/http';

export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/drive-scores/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        AssetGroupsModule,
        VehicleTypesListModule,
        DriveScoresRoutingModule,
        DriveScoreStatsModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewDriveScoresComponent,
        EditDriveScoreComponent,
        DataPointsListComponent,
        DriveScorePercentagesComponent,
        DriveCycleFilterPipe,
        DriveScorePercentagesSortPipe,

    ],
    providers: [
        DriveScoresService,
        DriveCyclesService
    ]
})
export class DriveScoresModule { }
