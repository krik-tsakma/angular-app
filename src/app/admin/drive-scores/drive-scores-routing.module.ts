
// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { ViewDriveScoresComponent } from './view/view-drive-scores.component';
import { EditDriveScoreComponent } from './edit/edit-drive-score.component';

const DriveScoreRoutes: Routes = [
    {
        path: '',
        component: ViewDriveScoresComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditDriveScoreComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        component: EditDriveScoreComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }   
];

@NgModule({
    imports: [
        RouterModule.forChild(DriveScoreRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class DriveScoresRoutingModule { }
