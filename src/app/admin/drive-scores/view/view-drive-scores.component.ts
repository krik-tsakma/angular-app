﻿// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

// SERVICES
import { Config } from './../../../config/config';
import { AdminLabelService } from './../../admin-label.service';
import { StoreManagerService } from './../../../shared/store-manager/store-manager.service';
import { ConfirmDialogService } from './../../../shared/confirm-dialog/confirm-dialog.service';
import { SnackBarService } from './../../../shared/snackbar.service';
import { UnsubscribeService } from './../../../shared/unsubscribe.service';
import { CustomTranslateService } from './../../../shared/custom-translate.service';
import { PreviousRouteService } from './../../../core/previous-route/previous-route.service';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// TYPES
import { 
    TableColumnSetting, TableActionItem, 
    TableActionOptions, TableCellFormatOptions, TableColumnSortOptions, TableColumnSortOrderOptions 
} from '../../../shared/data-table/data-table.types';
import { DriveScoreResultItem, DriveScoreTypes } from '../drive-score-types';
import { DriveScoresService } from '../drive-score.service';
import { Pager, SearchTermRequest } from './../../../common/pager';
import { DriveScoresResult } from './../drive-score-types';
import { StoreItems } from './../../../shared/store-manager/store-items';

@Component({
    selector: 'view-drive-scores',
    templateUrl: './view-drive-scores.html',
    encapsulation: ViewEncapsulation.None
})

export class ViewDriveScoresComponent extends AdminBaseComponent implements OnInit {    
    public loading: boolean;
    public request: SearchTermRequest = {} as SearchTermRequest;
    public tableSettings: TableColumnSetting[];    
    public data: DriveScoreResultItem[];
    public message: string;
    public pager: Pager = new Pager();
    public searchTermControl = new FormControl();     

    constructor(protected previousRouteService: PreviousRouteService,
                protected translator: CustomTranslateService,
                protected unsubscriber: UnsubscribeService,        
                protected snackBarService: SnackBarService,        
                protected router: Router,
                protected confirmDialogService: ConfirmDialogService,
                protected storeManager: StoreManagerService,
                protected labelService: AdminLabelService,
                protected configService: Config,
                private service: DriveScoresService) {
            
            super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBarService, storeManager, confirmDialogService);
            this.tableSettings =  this.getTableSettings();
    }

    
    public ngOnInit(): void {
        super.ngOnInit();

        const savedData = this.storeManager.getOption(StoreItems.scoreDefinition) as SearchTermRequest;
        if (savedData) {
            this.request = savedData;
            this.searchTermControl.setValue(this.request.searchTerm);
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting && x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchTermControl.valueChanges
            .pipe(            
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing             
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    this.request.searchTerm = term;
                    this.get(true);
                    return of([]);
                })
            )
            .subscribe(); 

        this.get(true);
    }


    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.searchTermControl.setValue(null);
    }


    // ----------------
    // TABLE
    // ----------------

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.edit: 
                this.labelService.set(item.record.name);
                this.manage(id);
                break;
            case TableActionOptions.delete: 
                this.openDialog().subscribe((response: boolean) => {
                    if (response === true) {
                        this.delete(id);
                    }
                });
                break;
            default: 
                break;
        }
    }

    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }


    // ----------------
    // PRIVATE
    // ----------------

      // Fetch drive score data
      private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.data = new Array<DriveScoreResultItem>();
        }   

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        // first stop currently executing requests
        this.unsubscribeService();
   
        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.scoreDefinition, this.request);

        // then start the new request
        this.serviceSubscriber = this.service
            .get(this.request)
            .subscribe(
                (response: DriveScoresResult) => {
                    this.loading = false;
                    this.data = this.data.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;                                          
                    
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }

    // ----------------
    // DELETE
    // ----------------
    private delete(id: number) {
        this.loading = true;
        
        // first stop currently executing requests
        this.unsubscribeService();
        
        this.serviceSubscriber = this.service
            .delete(id)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 200) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get(true);
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                primaryKey: 'isDefault',
                header: 'score_definition.view.default',
                format: TableCellFormatOptions.checkIcon,
            },
            {
                primaryKey: 'name',
                header: 'score_definition.view.name',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'type',
                header: 'score_definition.view.type',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: DriveScoreResultItem) => {
                    if (item === undefined || item === null) {
                        return ' ';
                    }
                    return this.translator.instant('score_definition.add.' + DriveScoreTypes[item.type]);
                },
            },
            {
                primaryKey: 'id',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete]
            }
        ];
    }
}
