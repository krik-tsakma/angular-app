import { KeyName } from './../../common/key-name';
// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RxJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { DriveScoresResult, DriveScoreSettings, DriveScoreUpsertCodeOptions } from './drive-score-types';
import { SearchTermRequest } from './../../common/pager';


@Injectable()
export class DriveScoresService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/DriveScoreAdmin';
        }

    // Get all drive scores
    public get(request: SearchTermRequest): Observable<DriveScoresResult> {
    
        const params = new HttpParams({
            fromObject: {
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Sorting: request.sorting
            }
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }

    public getByID(id: number): Observable<DriveScoreSettings> {
        return this.authHttp
            .get(this.baseURL + '/' + id);
    }


    public create(entity: DriveScoreSettings): Observable<DriveScoreUpsertCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as DriveScoreUpsertCodeOptions;
                    }
                })
            );
    }


    public update(entity: DriveScoreSettings): Observable<DriveScoreUpsertCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriveScoreUpsertCodeOptions;
                    }
                })
            );            
    }

    public delete(id: number): Observable<number> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((r) => r.status as number)                    
            ); 
    }

    // Get device type groups
    public getDeviceTypeGroups(): Observable<KeyName[]> {

        return this.authHttp
            .get(this.baseURL + '/GetDeviceTypeGroups' );
    }

    // Get data points for device type group
    public getDataPointsForDeviceGroup(dtgroupid): Observable<KeyName[]> {

        return this.authHttp
            .get(this.baseURL + '/GetDataPointsForDeviceGroup/' + dtgroupid );
    }
} 
