// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import {
    DriverGroupsResult, 
    DriverGroupCreateCodeOptions, 
    DriverGroupItem, 
    DriverGroupSettings,
    DriverGroupSettingsCodeOptions,
    DriverGroupAddMemberCodeOptions,
    DriverGroupRemoveMemberCodeOptions,
    DriverGroupAuthorizeUserCodeOptions,
    DriverGroupUnauthorizeUserCodeOptions,
    BulkOperationRequest,
    SetRoleForMembersRequest,
    AccResultCodeOptions,
    SetDefaultRoleCodeOptions
} from './driver-groups.types';
import { SearchTermRequest } from '../../common/pager';
import { AssetGroupMemberAddRemoveRequest, AssetGroupMembersRequest, AssetGroupMembersResult } from '../asset-groups/asset-groups.types';

@Injectable()
export class DriverGroupsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/DriverGroups';
        }

        // Get all company user roles
        public get(request: SearchTermRequest): Observable<DriverGroupsResult> {
        
            const params = new HttpParams({
                fromObject: {
                    SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                    PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                    PageSize: request.pageSize ? request.pageSize.toString() : '20',
                    Sorting: request.sorting
                }
            });

            return this.authHttp
                .get(this.baseURL, { params });
        }


        public getByID(id: number): Observable<DriverGroupSettings> {
            return this.authHttp
                .get(this.baseURL + '/' + id);
        }


        public create(entity: DriverGroupSettings): Observable<DriverGroupCreateCodeOptions>  {
            if (entity.dimensionID === -1) {
                entity.dimensionID = null;
            }
            return this.authHttp
                .post(this.baseURL, entity, { observe: 'response' })
                .pipe(
                    map((res) => {
                        if (res.status === 201) {
                            return null;
                        } else  {
                            return res.body as DriverGroupCreateCodeOptions;
                        }
                    })
                );
        }


        public update(entity: DriverGroupSettings): Observable<DriverGroupSettingsCodeOptions>  {
            if (entity.dimensionID === -1) {
                entity.dimensionID = null;
            }
            return this.authHttp
                .put(this.baseURL, entity, { observe: 'response' })
                .pipe(
                    map((res) => {
                        // this is means empty Ok response from the server
                        if (res.status === 200 && !res.body) {
                            return null;
                        } else  {
                            return res.body as DriverGroupSettingsCodeOptions;
                        }
                    })
                );            
        }

        public delete(id: number): Observable<number> {
            return this.authHttp
                .delete(this.baseURL + '/' + id, { observe: 'response' })
                .pipe(
                     map((r) => r.status as number)
                ); 
        }

    // ====================
    // MEMBERS
    // ====================

    public getMembers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetMembers', { params });
    }

    public getNonMembers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetNonMembers', { params });
    }


    public addMember(entity: AssetGroupMemberAddRemoveRequest): Observable<DriverGroupAddMemberCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/AddMember', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverGroupAddMemberCodeOptions;
                    }
                })
            );            
    }

    public removeMember(entity: AssetGroupMemberAddRemoveRequest): Observable<DriverGroupRemoveMemberCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/RemoveMember', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverGroupRemoveMemberCodeOptions;
                    }
                })
            );            
    }


    // ====================
    // AUTHORIZED USERS
    // ====================

    public getAuthorizedUsers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20'
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetAuthorizedUsers', { params });
    }


    public authorizeUser(entity: AssetGroupMemberAddRemoveRequest): Observable<DriverGroupAuthorizeUserCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/AuthorizeUser', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverGroupAuthorizeUserCodeOptions;
                    }
                })
            );            
    }

    public getUnauthorizedUsers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetUnauthorizedUsers', { params });
    }


    public unauthorizeUser(entity: AssetGroupMemberAddRemoveRequest): Observable<DriverGroupUnauthorizeUserCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/UnauthorizeUser', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverGroupUnauthorizeUserCodeOptions;
                    }
                })
            );            
    }

    // ====================
    // BULK OPERATIONS
    // ====================

    public setRoleForMembers(role: SetRoleForMembersRequest) {
        return this.authHttp
        .put(this.baseURL + '/SetRoleForMembers', role, { observe: 'response' })
        .pipe(
            map((res) => {
                // this is means empty Ok response from the server
                if (res.status === 200 && !res.body) {
                    return SetDefaultRoleCodeOptions.Set;
                } else  {
                    return res.body as SetDefaultRoleCodeOptions;
                }
            })
        );            
    }

    public sendACCForMembers(acc: BulkOperationRequest) {
        return this.authHttp
        .post(this.baseURL + '/SendACCForMembers', acc, { observe: 'response' })
        .pipe(
            map((res) => {
                // this is means empty Ok response from the server
                if (res.status === 200 && !res.body) {
                    return AccResultCodeOptions.Send;
                } else  {
                    return res.body as AccResultCodeOptions;
                }
            })
        );            
    }
} 
