// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { SettingsListModule } from '../settings-list/settings-list.module';
import { DriverGroupsRoutingModule } from './driver-groups-routing.module';
import { EditDriverGroupSettingsModule } from './edit/settings/edit-driver-group-settings.module';

// COMPONENTS
import { ViewDriverGroupsComponent } from './view/view-driver-groups.component';
import { SetDefaultRoleDialogComponent } from './view/set-default-role-dialog/set-default-role-dialog.component';

// SERVICES
import { DriverGroupsService } from './driver-groups.service';
import { SetDefaultRoleDialogService } from './view/set-default-role-dialog/set-default-role-dialog.service';
import { DriversService } from '../drivers/drivers.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/groups/drivers/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        DriverGroupsRoutingModule,
        EditDriverGroupSettingsModule,
        SettingsListModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewDriverGroupsComponent,
        SetDefaultRoleDialogComponent
    ],
    entryComponents: [
        SetDefaultRoleDialogComponent
    ],
    providers: [
        DriverGroupsService,
        SetDefaultRoleDialogService,
        DriversService
    ]
})
export class DriverGroupsModule { }
