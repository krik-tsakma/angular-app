// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { EditDriverGroupSettingsComponent } from './edit/settings/edit-driver-group-settings.component';
import { ViewDriverGroupsComponent } from './view/view-driver-groups.component';

const DriverGroupsRoutes: Routes = [
    {
        path: '',
        component: ViewDriverGroupsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditDriverGroupSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-driver-group.module').then((m) => m.EditDriverGroupModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }   
];

@NgModule({
    imports: [
        RouterModule.forChild(DriverGroupsRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class DriverGroupsRoutingModule { }
