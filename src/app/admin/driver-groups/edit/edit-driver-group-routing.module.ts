﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';
import { DriverGroupsService } from '../driver-groups.service';

// COMPONENTS
import { EditDriverGroupComponent } from './edit-driver-group.component';
import { EditDriverGroupSettingsComponent } from './settings/edit-driver-group-settings.component';
import { EditDriverGroupMembersComponent } from './members/edit-driver-group-members.component';
import { EditDriverGroupAuthorizedUsersComponent } from './authorized-users/edit-driver-group-authorized-users.component';

const EditDriverGroupRoutes: Routes = [
    {
        path: '',
        component: EditDriverGroupComponent,
        canActivate: [AuthGuard]
    },   
    {
        path: 'settings',
        component: EditDriverGroupSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.settings'
        }
    },
    {
        path: 'members',
        component: EditDriverGroupMembersComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.membership'
        }
    },
    {
        path: 'authorized-users',
        component: EditDriverGroupAuthorizedUsersComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.access'
        }
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(EditDriverGroupRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        DriverGroupsService
    ]
})
export class EditDriverGroupRoutingModule { }
