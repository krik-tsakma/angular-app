﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../../shared/shared.module';
import { AssetGroupDimensionsModule } from '../../../../common/asset-group-dimensions/asset-group-dimensions.module';

// COMPONENTS
import { EditDriverGroupSettingsComponent } from './edit-driver-group-settings.component';

@NgModule({
    imports: [
        SharedModule,
        AssetGroupDimensionsModule
    ],
    declarations: [
        EditDriverGroupSettingsComponent
    ],
    exports: [
        EditDriverGroupSettingsComponent
    ]
})

export class EditDriverGroupSettingsModule { }
