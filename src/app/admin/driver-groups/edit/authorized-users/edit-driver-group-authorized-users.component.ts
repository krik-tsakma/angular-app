// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, Renderer } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { AdminLabelService } from '../../../admin-label.service';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { DriverGroupsService } from '../../driver-groups.service';
import { Config } from '../../../../config/config';

// TYPES
import { KeyName } from '../../../../common/key-name';
import { TableColumnSetting, TableActionOptions, TableColumnSortOrderOptions, TableColumnSortOptions, TableActionItem } from '../../../../shared/data-table/data-table.types';
import { SearchType } from '../../../../shared/search-mechanism/search-mechanism-types';
import { AssetGroupMembersRequest, AssetGroupMembersResult, AssetGroupMemberAddRemoveRequest } from '../../../asset-groups/asset-groups.types';
import { ConfirmationDialogParams } from '../../../../shared/confirm-dialog/confirm-dialog-types';
import { 
    DriverGroupSessionName, 
    DriverGroupAuthorizeUserCodeOptions, 
    DriverGroupUnauthorizeUserCodeOptions 
} from '../../driver-groups.types';
import { Pager } from '../../../../common/pager';

@Component({
    selector: 'edit-driver-group-authorized-users',
    templateUrl: 'edit-driver-group-authorized-users.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditDriverGroupAuthorizedUsersComponent extends AdminBaseComponent implements OnInit {
    public loading: boolean;
    public loadingUnauthorizedUsers: boolean;
    public pager: Pager = new Pager();
    public authorizedUsers: KeyName[] = [];
    public unauthorizedUsers: KeyName[] = [];
    public request: AssetGroupMembersRequest = {} as AssetGroupMembersRequest;
    public tableSettings: TableColumnSetting[];
    public searchTypes: any = SearchType;

    @ViewChild('wrapper', { static: true }) public wrapperElm: ElementRef;
    
    constructor(        
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected confirmDialogService: ConfirmDialogService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: DriverGroupsService,
        private renderer: Renderer
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar, null, confirmDialogService);
        labelService.setSessionItemName(DriverGroupSessionName);

        this.tableSettings =  [
            {
                primaryKey: 'name',
                header: 'assets.group.name',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'id',
                header: ' ',
                actions: [TableActionOptions.delete]
            }
        ];
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
                    this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
                    this.request.id = params.id;
                    this.getAuthorizedUsers(true);
                }
            });
    }

    // ----------------
    // TABLE
    // ----------------

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }
        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.delete: 
                const req = {
                    title: 'assets.group.members.remove_member',
                    message: 'form.warnings.are_you_sure',
                    submitButtonTitle: 'form.actions.continue'
                } as ConfirmationDialogParams;
                this.openDialog(req).subscribe((response) => {
                    if (response === true) {
                        this.onRemoveMember(id);
                    }
                });
                break;
            default: 
                break;

        }
    }

    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.getAuthorizedUsers(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.getAuthorizedUsers(true);
    }

    public onAuthorizeUser(userID: number) {
        if (!userID) {
            return;
        }
        
        this.loading = true;
        this.unsubscribeService();

        const req = {
            memberID: userID,
            groupID: this.request.id
        } as AssetGroupMemberAddRemoveRequest;

        this.unauthorizedUsers = [];
        this.focusOutFromAutocomplete();

        this.serviceSubscriber = this.service
            .authorizeUser(req)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('assets.group.access.authorize.success', 'form.actions.close');
                    this.getAuthorizedUsers(true);
                } else {
                    const message = this.getAddMemberResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // Fetch unauthorized users
    public getUnauthorizedUsers(term: string) {
        if (!term || term.length < 2) {
            return;
        }

        this.message = '';
        this.loadingUnauthorizedUsers = true;
        this.unauthorizedUsers = new Array<KeyName>();

        // first stop currently executing requests
        this.unsubscribeService();

        const req = {
            id: this.request.id,
            sorting: this.request.sorting,
            searchTerm: term
        } as AssetGroupMembersRequest;
        this.serviceSubscriber = this.service
            .getUnauthorizedUsers(req)
            .subscribe(
                (response: AssetGroupMembersResult) => {
                    this.loadingUnauthorizedUsers = false;
                    this.unauthorizedUsers = response.results;

                    if (!this.unauthorizedUsers || this.unauthorizedUsers.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loadingUnauthorizedUsers = false;
                    this.message = 'data.error';
                }
            );
    }
   

    // ----------------
    // PRIVATE
    // ----------------

    private onRemoveMember(memberID: number) {
        this.loading = true;
        this.unsubscribeService();

        const req = {
            memberID: memberID,
            groupID: this.request.id
        } as AssetGroupMemberAddRemoveRequest;
        
        this.serviceSubscriber = this.service
            .unauthorizeUser(req)
            .subscribe((res: DriverGroupUnauthorizeUserCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('assets.group.access.unauthorize.success', 'form.actions.close');
                    this.getAuthorizedUsers(true);
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // Fetch authorized users
    private getAuthorizedUsers(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.authorizedUsers = new Array<KeyName>();
        }

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        // first stop currently executing requests
        this.unsubscribeService();

        // then start the new request
        this.serviceSubscriber = this.service
            .getAuthorizedUsers(this.request)
            .subscribe(
                (response: AssetGroupMembersResult) => {
                    this.loading = false;
                    this.authorizedUsers = this.authorizedUsers.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;
                    
                    if (!this.authorizedUsers || this.authorizedUsers.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }
    
    private focusOutFromAutocomplete() {
        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.wrapperElm.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }


    // =========================
    // RESULTS
    // =========================

    private getAddMemberResult(code: DriverGroupAuthorizeUserCodeOptions): string {
        let message = '';
        switch (code) {
            case DriverGroupAuthorizeUserCodeOptions.UserAuthorized:
                message = 'assets.group.access.authorize.error';
                break;
            case DriverGroupAuthorizeUserCodeOptions.DriverGroupNotFound:
            case DriverGroupAuthorizeUserCodeOptions.UserNotFound:
                message = 'form.errors.not_found';
                break;
            case DriverGroupAuthorizeUserCodeOptions.IssuerNotFound:
            case DriverGroupAuthorizeUserCodeOptions.NotAllowedDriverGroup:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getRemoveMemberResult(code: DriverGroupUnauthorizeUserCodeOptions): string {
        let message = '';
        switch (code) {
            case DriverGroupUnauthorizeUserCodeOptions.UserNotAuthorized:
                message = 'assets.group.access.unauthorize.error';
                break;
            case DriverGroupUnauthorizeUserCodeOptions.UserNotFound:
            case DriverGroupUnauthorizeUserCodeOptions.HRGroupNotFound:
                message = 'form.errors.not_found';
                break;
            case DriverGroupUnauthorizeUserCodeOptions.IssuerNotFound:
            case DriverGroupUnauthorizeUserCodeOptions.NotAllowedDriverGroup:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
