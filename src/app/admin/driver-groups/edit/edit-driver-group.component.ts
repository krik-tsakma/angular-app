// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// TYPES 
import { SettingList } from '../../settings-list/settings-list.types';
import { DriverGroupSessionName } from '../driver-groups.types';

// SERVICES
import { AdminLabelService } from '../../admin-label.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

@Component({
    selector: 'edit-driver-group',
    templateUrl: 'edit-driver-group.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditDriverGroupComponent {
    public options: SettingList[] = [
        {
            title: 'assets.group.category.basic',
            items: [
                {
                    option: 'assets.group.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
                {
                    option: 'assets.group.category.basic.membership',
                    params: ['members'],
                    icon: 'group'
                },
                {
                    option: 'assets.group.category.basic.access',
                    params: ['authorized-users'],
                    icon: 'person_outline'
                },
            ]
        }
    ];

    constructor(public labelService: AdminLabelService,
                private router: Router,
                private configService: Config,
                private previousRouteService: PreviousRouteService) {
            labelService.setSessionItemName(DriverGroupSessionName);
        }

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
