﻿
// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';
import { EditDriverGroupRoutingModule } from './edit-driver-group-routing.module';
import { EditDriverGroupSettingsModule } from './settings/edit-driver-group-settings.module';

// SERVICES
import { DriverGroupsService } from '../driver-groups.service';

// COMPONENTS
import { EditDriverGroupComponent } from './edit-driver-group.component';
import { EditDriverGroupMembersComponent } from './members/edit-driver-group-members.component';
import { EditDriverGroupAuthorizedUsersComponent } from './authorized-users/edit-driver-group-authorized-users.component';

@NgModule({
    imports: [
        SharedModule,
        EditDriverGroupRoutingModule,
        EditDriverGroupSettingsModule,
        SettingsListModule,
    ],
    declarations: [
        EditDriverGroupComponent,
        EditDriverGroupMembersComponent,
        EditDriverGroupAuthorizedUsersComponent
    ],
    providers: [
        DriverGroupsService
    ]
})
export class EditDriverGroupModule { }
