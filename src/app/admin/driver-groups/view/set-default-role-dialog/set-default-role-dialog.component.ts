// FRAMEWORK
import { 
    Component, OnDestroy, ViewEncapsulation, OnInit,
    ViewChild, ElementRef, Renderer 
} from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// RxJS
import { Subscription, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

// SERVICES
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { DriversService } from '../../../drivers/drivers.service';

// TYPES 
import { KeyName } from '../../../../common/key-name';


@Component({
    selector: 'set-default-role-dialog',
    templateUrl: 'set-default-role-dialog.html',
    encapsulation: ViewEncapsulation.None
})

export class SetDefaultRoleDialogComponent implements OnDestroy {    
    public loading: boolean;
    public driverRoles: KeyName[];
    public selectedRoleID: number;
    public serviceSubscriber: Subscription;
    public message: string;

    
    constructor(        
        private service: DriversService, 
        private translator: CustomTranslateService,
        private dialogRef: MatDialogRef<SetDefaultRoleDialogComponent>,             
        private unsubscribe: UnsubscribeService) {
            this.serviceSubscriber = this.service
                .getDriverRolesList()
                .subscribe((res: KeyName[]) => {
                        this.loading = false;
                        this.driverRoles = res;
                        this.driverRoles.forEach((role) => { 
                            role.enabled = true;
                        });

                        this.driverRoles.unshift({
                            id: null,
                            name: this.translator.instant('form.actions.select') + '...',
                            enabled: false,
                        });                    
                    },
                    (err) => {
                        this.loading = false;
                        this.message = 'data.error';
                    }
                );
    }

    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }   


    public selectedRole(event) {
        
        this.selectedRoleID = event;  
        console.log(this.selectedRoleID);
    }

    public submit() {
        console.log('submit');
        this.close(this.selectedRoleID);
    }

   
   /*
    * Closes the dialog
    */
    public close(id?: number) {
        this.dialogRef.close(id);
    }       

}
