// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { SetDefaultRoleDialogComponent } from './set-default-role-dialog.component';

// TYPES


@Injectable()
export class SetDefaultRoleDialogService {
   
    constructor(private dialog: MatDialog) { }       

    public openDialog(): Observable<number> {

        const dialogRef: MatDialogRef<SetDefaultRoleDialogComponent> = this.dialog.open(SetDefaultRoleDialogComponent);                   
        return dialogRef.afterClosed();
    }
} 
