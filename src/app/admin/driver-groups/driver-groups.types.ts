import { PagerResults } from '../../common/pager';

export const  DriverGroupSessionName: string = 'dgn';

export interface DriverGroupsResult extends PagerResults {
    results: DriverGroupItem[];
}

export interface DriverGroupItem {
    id: number;
    name: string;
    isDefault: boolean;
    isUnlinked: boolean;
    dimensionTag?: string;
}

// ====================
// BASIC INFO
// ====================

export interface DriverGroupSettings {
    id: number;
    name: string;
    description: string;
    dimensionID: number;
}

export enum DriverGroupCreateCodeOptions {
    Created = 0,
    NotFound = 1,
    NameAlreadyExists = 2,
    UnknownError = 10,
}

export enum DriverGroupSettingsCodeOptions {
    Set = 0,
    NoSuchGroupExists = 1,
    NameAlreadyExists = 2,
    IssuerNotFound = 3,
    NotAllowedDriverGroup = 4,
    UnknownError = 10,
}

// ====================
// MEMBERS
// ===================

export enum DriverGroupAddMemberCodeOptions {
    Added = 0,
    DriverGroupNotFound = 1,
    DriverGroupIsDefault = 2,
    DriverNotFound = 3,
    DriverAlreadyAMember = 4,
    IssuerNotFound = 5,
    NotAllowedDriver = 6,
    NotAllowedDriverGroup = 7,
    UnknownError = 20
}

export enum DriverGroupRemoveMemberCodeOptions {
    Removed = 0,
    DriverGroupNotFound = 1,
    DriverNotAMember = 2,
    IssuerNotFound = 3,
    NotAllowedDriver = 4,
    NotAllowedDriverGroup = 5,
    UnknownError = 20
}

// ====================
// AUTHORIZED USERS
// ===================

export enum DriverGroupAuthorizeUserCodeOptions {
    Added = 0,
    DriverGroupNotFound = 1,
    UserNotFound = 2,
    UserAuthorized = 3,
    IssuerNotFound = 4,
    NotAllowedDriverGroup = 5,
    UnknownError = 20
}

export enum DriverGroupUnauthorizeUserCodeOptions {
    Removed = 0,
    HRGroupNotFound = 1,
    UserNotFound = 2,
    UserNotAuthorized = 3,
    IssuerNotFound = 4,
    NotAllowedDriverGroup = 5,
    UnknownError = 9
}

export enum DriverGroupEditingOptions {
    acc = 1,
    defaultRole = 2,    
}

export interface DriverGroupEditingOptionsDialogCloseResult {
    action: DriverGroupEditingOptions;
    element: DriverGroupItem;
}

export interface BulkOperationRequest {
    id: number;    
}

export interface SetRoleForMembersRequest extends BulkOperationRequest {    
    roleID: number; 
}

export enum AccResultCodeOptions {
    Send = 0,
    IssuerNotFound = 1,
    DriverGroupNotFound = 2,
    NotAllowedDriverGroup = 3
}

export enum SetDefaultRoleCodeOptions {
    Set = 0,
    IssuerNotFound = 1,
    DriverGroupNotFound = 2,
    RoleNotFound = 3,
    NotAllowedDriverGroup = 4,
    NotDriverRole = 5
}
