// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { KeyName } from '../../common/key-name';
import { 
    VehiclesResult, 
    VehicleSettings, 
    VehicleCreateCodeOptions, 
    VehicleUpdateCodeOptions, 
    VehicleGroupMembershipUpdateRequest, 
    VehicleGroupMembershipCodeOptions, 
    VehicleInactivationPeriod,
    VehicleInactivationPeriodInsertCodeOptions,
    VehicleInactivationPeriodUpdateCodeOptions,
    VehiclesRequest
} from './vehicles.types';

@Injectable()
export class VehiclesService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/Vehicles';
        }

    // Get all company user roles
    public get(request: VehiclesRequest): Observable<VehiclesResult> {

        const params = new HttpParams({
            fromObject: {
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Sorting: request.sorting,
                GroupID: request.groupID ? request.groupID.toString() : '',
                VehicleTypeID: request.vehicleTypeID ? request.vehicleTypeID.toString() : ''
            }
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }


    // =========================
    // BASIC
    // =========================

    
    public getByID(id: number): Observable<VehicleSettings> {
        return this.authHttp
            .get(this.baseURL + '/' + id);
    }

    public create(entity: VehicleSettings): Observable<VehicleCreateCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as VehicleCreateCodeOptions;
                    }
                })
            );
    }


    public update(entity: VehicleSettings): Observable<VehicleUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleUpdateCodeOptions;
                    }
                })
            );            
    }


    // Delete driver 
    public delete(id: number): Observable<number> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((res) => res.status)
            ); 
    }


    // =========================
    // GROUP MEMBERSHIP
    // =========================

    public getGroupMembershipByID(id: number): Observable<number[]> {
        return this.authHttp
            .get(this.baseURL + '/GetGroupMembership/' + id);
    }

    
    public updateGroupMembership(entity: VehicleGroupMembershipUpdateRequest): Observable<VehicleGroupMembershipCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateGroupMembership/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleGroupMembershipCodeOptions;
                    }
                })
            );            
    }

    // =========================
    // BLACKOUT PERIODS
    // =========================

    public getInactivationPeriodsByID(id: number): Observable<VehicleInactivationPeriod[]> {
        return this.authHttp
            .get(this.baseURL + '/GetBlackoutPeriods/' + id);
    }

    // insert a new black out period
    public addInactivationPeriod(entity: VehicleInactivationPeriod): Observable<VehicleInactivationPeriodInsertCodeOptions>  {
        return this.authHttp
            .post(this.baseURL + '/AddBlackoutPeriod/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as VehicleInactivationPeriodInsertCodeOptions;
                    }
                })
            );
    }

    public updateInactivationPeriod(entity: VehicleInactivationPeriod): Observable<VehicleInactivationPeriodUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateBlackoutPeriod/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleInactivationPeriodUpdateCodeOptions;
                    }
                })
            );     
    }

    // Delete black out period 
    public removeInactivationPeriod(id: number): Observable<number> {
        return this.authHttp
            .delete(this.baseURL + '/RemoveBlackoutPeriod/' + id, { observe: 'response' })
            .pipe(
                map((res) => res.status)
            ); 
    }
} 
