import { KeyName } from '../../common/key-name';
import { PagerResults, SearchTermRequest } from '../../common/pager';

export const VehicleSessionName: string = 'vehn';

export interface VehiclesRequest extends SearchTermRequest {
    vehicleTypeID?: number;
    groupID?: number;
}

export interface VehiclesResult extends PagerResults {
    results: VehicleResultItem[];
}

export interface VehicleResultItem extends KeyName {
    groups: string;
    vehicleType: string;
}
// ====================
// BASIC INFO
// ====================

export interface VehicleSettings {
    id: number;
    ownID: string;
    licencePlate: string;
    description: string;
    vehicleTypeID: number;
    chargingID: string;
}

export enum VehicleCreateCodeOptions {
    Created = 0,
    IssuerNotFound = 1,
    OwnIDAlreadyExists = 2,
    LicencePlateEmpty = 3,
    LicencePlateAlreadyExists = 4,
    ChargingIDAlreadyExists = 5,
    UnknownError = 20,
}

export enum VehicleUpdateCodeOptions {
    Updated = 0,
    NotFound = 1,
    OwnIDAlreadyExists = 2,
    LicencePlateEmpty = 3,
    LicencePlateAlreadyExists = 4,
    ChargingIDAlreadyExists = 5,
    UnknownError = 20,
}

// ====================
// GROUP MEMBERSHIP
// ====================

export interface VehicleGroupMembershipUpdateRequest {
    id: number;
    groupIDs: number[];
}

export enum VehicleGroupMembershipCodeOptions {
    Updated = 0,
    NoSuchVehExists = 1,
    IssuerNotFound = 2,
    NotAllowedVeh = 3,
    UnknownError = 10
}

// ====================
// BLACKOUT PERIODS
// ====================

export interface VehicleInactivationPeriod {
    id: number;
    vehicleID: number;
    since: string;
    till: string;
    comments: string;
    user: string;
}

export enum VehicleInactivationPeriodInsertCodeOptions {
    Added = 0,
    NoSuchVehicle = 1,
    CommentsRequired = 2,
    StopTimestampBeforeStartTimestamp = 3,
    StopTimestampLessThanOneDayAfterStartTimestamp = 4,
    OverlappingPeriods = 5,
    UnknownError = 8,
}

export enum VehicleInactivationPeriodUpdateCodeOptions {
    Updated = 0,
    NoSuchPeriod = 1,
    CommentsRequired = 2,
    StopTimestampBeforeStartTimestamp = 3,
    StopTimestampLessThanOneDayAfterStartTimestamp = 4,
    OverlappingPeriods = 5,
    UnknownError = 8,
}
