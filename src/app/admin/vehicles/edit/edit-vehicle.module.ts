﻿
// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';
import { AssetGroupMembershipModule } from '../../../common/asset-groups-membership/asset-group-membership.module';
import { EditVehicleRoutingModule } from './edit-vehicle-routing.module';
import { EditVehicleSettingsModule } from './settings/edit-vehicle-settings.module';
import { DatepickerModule } from '../../../common/datepicker/datepicker.module';
import { EnergyCorrectionFactorsModule } from '../../../common/energy-correction-factors/energy-correction-factors.module';
import { EnergyTargetBaselinesModule } from '../../../common/energy-target-baselines/energy-target-baselines.module';

// SERVICES
import { VehiclesService } from '../vehicles.service';

// COMPONENTS
import { EditVehicleGroupMembershipComponent } from './group-membership/vehicle-group-membership.component';
import { EditVehicleComponent } from './edit-vehicle.component';
import { VehicleInactivationPeriodsComponent } from './inactivation-periods/inactivation-periods.component';
import { EditVehicleInactivationPeriodComponent } from './inactivation-periods/edit/edit-period.component';
import { EditVehicleCorrectionFactorsComponent } from './correction-factors/edit-correction-factors.component';
import { EditVehicleTargetBaselinesComponent } from './target-baselines/edit-target-baselines.component';

@NgModule({
    imports: [
        SharedModule,
        EditVehicleRoutingModule,
        EditVehicleSettingsModule,
        SettingsListModule,
        AssetGroupMembershipModule,
        DatepickerModule,
        EnergyCorrectionFactorsModule,
        EnergyTargetBaselinesModule
    ],
    declarations: [
        EditVehicleComponent,
        EditVehicleGroupMembershipComponent,
        VehicleInactivationPeriodsComponent,
        EditVehicleInactivationPeriodComponent,
        EditVehicleCorrectionFactorsComponent,
        EditVehicleTargetBaselinesComponent
    ],
    providers: [
        VehiclesService
    ],
    entryComponents: [
        EditVehicleInactivationPeriodComponent
    ]
})
export class EditVehicleModule { }
