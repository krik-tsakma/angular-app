// FRAMEWORK
import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { AdminLabelService } from '../../../admin-label.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { Config } from '../../../../config/config';

// TYPES
import { AdminBaseComponent } from '../../../admin-base.component';
import { VehicleSessionName } from '../../vehicles.types';
import { EnergyTargetBaselinesComponent } from '../../../../common/energy-target-baselines/energy-target-baselines.component';


@Component({
    selector: 'edit-vehicle-target-baselines',
    templateUrl: 'edit-target-baselines.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditVehicleTargetBaselinesComponent extends AdminBaseComponent implements OnInit {
    @ViewChild(EnergyTargetBaselinesComponent, { static: true }) public targetBaselinesComponent: EnergyTargetBaselinesComponent;

    public loading: boolean;
    public vehicleID: number;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translate: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected router: Router,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute
    ) {
        super(translate, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(VehicleSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.vehicleID = params.id;
                }
            });
    }

    public addFactor() {
        this.targetBaselinesComponent.onAdd();
    }
}
