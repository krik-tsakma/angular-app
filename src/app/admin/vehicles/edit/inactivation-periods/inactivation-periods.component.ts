
// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// PIPES
import { DateFormatPipe, DateFormatter, DateFormatterDisplayOptions } from '../../../../shared/pipes/dateFormat.pipe';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { StoreManagerService } from '../../../../shared/store-manager/store-manager.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { UserOptionsService } from '../../../../shared/user-options/user-options.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { AdminLabelService } from '../../../admin-label.service';
import { VehiclesService } from '../../vehicles.service';
import { Config } from '../../../../config/config';

// TYPES
import { 
    TableColumnSetting, TableActionItem, TableActionOptions, 
    TableCellFormatOptions, TableColumnSortOrderOptions, TableColumnSortOptions
} from '../../../../shared/data-table/data-table.types';
import { VehicleInactivationPeriod, VehicleSessionName } from '../../vehicles.types';
import { EditVehicleInactivationPeriodComponent } from './edit/edit-period.component';

@Component({
    selector: 'vehicle-inactivation-periods',
    templateUrl: 'inactivation-periods.html',
    encapsulation: ViewEncapsulation.None,
    providers: [DateFormatPipe]
})

export class VehicleInactivationPeriodsComponent extends AdminBaseComponent implements OnInit {
    public vehicleID: number;
    public periods: VehicleInactivationPeriod[] = [];
    public loading: boolean;
    public tableSettings: TableColumnSetting[];
    private editInactivationPeriodDialogRef: MatDialogRef<EditVehicleInactivationPeriodComponent>;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBarService: SnackBarService,
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,
        protected storeManager: StoreManagerService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private userOptions: UserOptionsService,
        private service: VehiclesService,
        private dialog: MatDialog,
        private dateFormatPipe: DateFormatPipe
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBarService, storeManager, confirmDialogService);
        labelService.setSessionItemName(VehicleSessionName);

        this.tableSettings =  [
            {
                primaryKey: 'since',
                header: 'vehicles.inactivation.since',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null),
                format: TableCellFormatOptions.date
            },
            {
                primaryKey: 'till',
                header: 'vehicles.inactivation.till',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null),
                format: TableCellFormatOptions.custom,
                formatFunction: (record: VehicleInactivationPeriod) => {
                    if (record.till) {
                        return this.dateFormatPipe.transform(record.till, { 
                            display: DateFormatterDisplayOptions.date, 
                            dateFormat: this.userOptions.localOptions.short_date 
                        } as DateFormatter);
                    } else {
                        return translator.instant('time.now');
                    }
                }
            },
            {
                primaryKey: 'user',
                header: 'vehicles.inactivation.recording_user',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'comments',
                header: 'vehicles.inactivation.comments',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete],
              //  actionsVisibilityFunction: this.showUDButtons
            }
        ];
    }


    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.vehicleID = params.id;
                    this.get();
                }
            });
        
    }

    public addInactivationPeriod() {
        this.openInactivationPeriodEditDialog({ 
            vehicleID: this.vehicleID 
        } as VehicleInactivationPeriod); 
    }

    // ----------------
    // TABLE CALLBACKS
    // ----------------

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const record = item.record as VehicleInactivationPeriod;
        switch (item.action) {
            case TableActionOptions.edit: 
                record.vehicleID = this.vehicleID;
                this.openInactivationPeriodEditDialog(record); 
                break;
            case TableActionOptions.delete: 
                this.openDialog().subscribe((response) => {
                    if (response === true) {
                       this.delete(record.id);
                    }
                });
                break;
            default: 
                break;

        }
    }


    // ----------------
    // PRIVATE
    // ----------------

    // Fetch events data
    private get() {
        this.message = '';
        this.loading = true;
    
        // first stop currently executing requests
        this.unsubscribeService();

        // then start the new request
        this.serviceSubscriber = this.service
            .getInactivationPeriodsByID(this.vehicleID)
            .subscribe(
                (response: VehicleInactivationPeriod[]) => {
                    this.loading = false;
                    this.periods = response;
                    
                    if (!this.periods || this.periods.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }

    private openInactivationPeriodEditDialog(item: VehicleInactivationPeriod) {
        this.editInactivationPeriodDialogRef = this.dialog.open(EditVehicleInactivationPeriodComponent, {
            data: item
        });
    
        this.editInactivationPeriodDialogRef.afterClosed().subscribe((result) => {
            this.editInactivationPeriodDialogRef = null;
            if (result) {
                this.get();
            }
        });
    }

    // ----------------
    // DELETE
    // ----------------

    private delete(id: number) {
        this.loading = true;
        
        // first stop currently executing requests
        this.unsubscribeService();
        
        this.serviceSubscriber = this.service
            .removeInactivationPeriod(id)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 200) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }
}
