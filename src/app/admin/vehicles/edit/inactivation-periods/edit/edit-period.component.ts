// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material';

// COMPONENTS
import { AdminBaseComponent } from '../../../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../../core/previous-route/previous-route.service';
import { AdminLabelService } from '../../../../admin-label.service';
import { VehiclesService } from '../../../vehicles.service';
import { Config } from '../../../../../config/config';

// TYPES
import {
    VehicleInactivationPeriod,
    VehicleInactivationPeriodInsertCodeOptions,
    VehicleInactivationPeriodUpdateCodeOptions
} from '../../../vehicles.types';

// PIPES
import { StringFormatPipe } from '../../../../../shared/pipes/stringFormat.pipe';

// MOMENT
import moment from 'moment';


// DIALOG
@Component({
    selector: 'edit-vehicle-inactivation-period',
    templateUrl: 'edit-period.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-period.less']
})

export class EditVehicleInactivationPeriodComponent extends AdminBaseComponent implements OnInit {
    public loading: boolean;
    public action: string;
    public period: VehicleInactivationPeriod = {} as VehicleInactivationPeriod;
    public sinceDate: moment.Moment;
    public tillDate: moment.Moment;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected router: Router,
        protected labelService: AdminLabelService,
        protected configService: Config,
        public dialogRef: MatDialogRef<EditVehicleInactivationPeriodComponent>,
        private service: VehiclesService,
        private stringFormatPipe: StringFormatPipe,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        this.period = Object.assign({}, data);
        if (this.period.id) {
            this.sinceDate = moment(this.period.since);
            if (this.period.till) {
                this.tillDate = moment(this.period.till);
            }
        } else {
            this.sinceDate = moment();
        }
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public closeDialog(result?: string) {
        this.dialogRef.close(result);
    }

    // =========================
    // DATA
    // =========================


    public onSubmit({ value, valid }: { value: VehicleInactivationPeriod, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        this.period.since = this.sinceDate.startOf('day').format('YYYY-MM-DD HH:mm');
        if (this.tillDate) {
            this.period.till = this.tillDate.startOf('day').format('YYYY-MM-DD HH:mm');
        } else {
            this.period.till = null;
        }

        // This is create
        if (typeof(this.period.id) === 'undefined' || this.period.id === 0) {
            this.serviceSubscriber = this.service
                .addInactivationPeriod(this.period)
                .subscribe((res: VehicleInactivationPeriodInsertCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.closeDialog('success');
                    } else {
                        const message = this.getCreateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else {
            this.serviceSubscriber = this.service
                .updateInactivationPeriod(this.period)
                .subscribe((res: VehicleInactivationPeriodUpdateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.closeDialog('success');
                    } else {
                        const message = this.getUpdateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        }

    }


    // =========================
    // RESULTS
    // =========================

    private getCreateResult(code: VehicleInactivationPeriodInsertCodeOptions): string {
        let message = '';
        switch (code) {
            case VehicleInactivationPeriodInsertCodeOptions.OverlappingPeriods:
                message = 'vehicles.inactivation.errors.period_overlapping';
                break;
            case VehicleInactivationPeriodInsertCodeOptions.StopTimestampBeforeStartTimestamp:
                message = 'vehicles.inactivation.errors.ststop_must_be_after_start';
                break;
            case VehicleInactivationPeriodInsertCodeOptions.StopTimestampLessThanOneDayAfterStartTimestamp:
                message = 'vehicles.inactivation.errors.stop_must_be_more_than_a_day_after';
                break;
            case VehicleInactivationPeriodInsertCodeOptions.CommentsRequired:
                const required = this.translator.instant('form.errors.required');
                const name = this.translator.instant('vehicles.inactivation.comments');
                message = this.stringFormatPipe.transform(required, [name]);
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUpdateResult(code: VehicleInactivationPeriodUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case VehicleInactivationPeriodUpdateCodeOptions.OverlappingPeriods:
                message = 'vehicles.inactivation.errors.period_overlapping';
                break;
            case VehicleInactivationPeriodUpdateCodeOptions.StopTimestampBeforeStartTimestamp:
                message = 'vehicles.inactivation.errors.ststop_must_be_after_start';
                break;
            case VehicleInactivationPeriodUpdateCodeOptions.StopTimestampLessThanOneDayAfterStartTimestamp:
                message = 'vehicles.inactivation.errors.stop_must_be_more_than_a_day_after';
                break;
            case VehicleInactivationPeriodUpdateCodeOptions.CommentsRequired:
                const required = this.translator.instant('form.errors.required');
                const name = this.translator.instant('vehicles.inactivation.comments');
                message = this.stringFormatPipe.transform(required, [name]);
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
