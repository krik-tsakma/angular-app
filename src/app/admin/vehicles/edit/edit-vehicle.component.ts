// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { AdminLabelService } from '../../admin-label.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES 
import { SettingList } from '../../settings-list/settings-list.types';
import { VehicleSessionName } from '../vehicles.types';

@Component({
    selector: 'edit-vehicle',
    templateUrl: 'edit-vehicle.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditVehicleComponent {
    public options: SettingList[] = [
        {
            title: 'assets.category.basic',
            items: [
                {
                    option: 'assets.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
                {
                    option: 'assets.category.basic.groups',
                    params: ['groups'],
                    icon: 'group'
                },
            ]
        },
        {
            title: 'energy_category',
            items: [
                {
                    option: 'energy_category.correction_factors',
                    params: ['correction-factors'],
                    icon: 'note_add'
                },
                {
                    option: 'energy_category.target_baselines',
                    params: ['target-baselines'],
                    icon: 'equalizer'
                },
            ]
        },
        // {
        //     title: 'vehicles.category.readings',
        //     items: [
        //         // {
        //         //     option: 'vehicles.category.readings.odometer',
        //         //     params: ['odometer'],
        //         //     icon: 'directions_car'
        //         // },
        //         {
        //             option: 'vehicles.category.readings.inactivation',
        //             params: ['inactivation'],
        //             icon: 'history'
        //         },
        //     ]
        // }
    ];

    constructor(public labelService: AdminLabelService,
                private previousRouteService: PreviousRouteService,
                private router: Router,
                private configService: Config) {
        labelService.setSessionItemName(VehicleSessionName);
    }

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
