﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../../shared/shared.module';
import { VehicleTypesListModule } from '../../../../common/vehicle-types-list/vehicle-types-list.module';

// COMPONENTS
import { EditVehicleSettingsComponent } from './edit-vehicle-settings.component';

@NgModule({
    imports: [
        SharedModule,
        VehicleTypesListModule
    ],
    declarations: [
        EditVehicleSettingsComponent
    ],
    exports: [
        EditVehicleSettingsComponent
    ]
})
export class EditVehicleSettingsModule { }
