import { KeyName } from './../../../../common/key-name';
// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { AdminLabelService } from '../../../admin-label.service';
import { VehiclesService } from '../../vehicles.service';
import { Config } from '../../../../config/config';

// TYPES
import { VehicleSettings, VehicleSessionName, VehicleCreateCodeOptions, VehicleUpdateCodeOptions } from '../../vehicles.types';

@Component({
    selector: 'edit-vehicle-settings',
    templateUrl: 'edit-vehicle-settings.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-vehicle-settings.less']
})

export class EditVehicleSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public action: string;
    public vehicle: VehicleSettings = {} as VehicleSettings;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected router: Router,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: VehiclesService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(VehicleSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => {
                this.action = params.id
                            ? 'form.actions.edit'
                            : 'form.actions.create';
                if (params.id) {
                    this.get(params.id);
                }
            });
    }

    // =========================
    // VEHICLE TYPE CALLBACKS
    // =========================

    // When the user selects a vehicle type from the DDL
    public onChangeVehicleType(vehicleType: KeyName): void {
        this.vehicle.vehicleTypeID = vehicleType && vehicleType.id ? Number(vehicleType.id) : null;
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: VehicleSettings, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is create
        if (typeof(this.vehicle.id) === 'undefined' || this.vehicle.id === 0) {
            this.serviceSubscriber = this.service
                .create(this.vehicle)
                .subscribe((res: VehicleCreateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const message = this.getCreateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else {
            // This is an update
            this.serviceSubscriber = this.service
                .update(this.vehicle)
                .subscribe((res: VehicleUpdateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.labelService.set(this.vehicle.licencePlate);
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const message = this.getUpdateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        }
    }

    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((res: VehicleSettings) => {
                this.loading = false;
                this.vehicle = res;
            },
            (err) => {
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }


    // =========================
    // RESULTS
    // =========================
    private getCreateResult(code: VehicleCreateCodeOptions): string {
        let message = '';
        switch (code) {
            case VehicleCreateCodeOptions.LicencePlateAlreadyExists:
                message = 'vehicles.errors.licence_plate_exists';
                break;
            case VehicleCreateCodeOptions.OwnIDAlreadyExists:
                message = 'vehicles.errors.id_exists';
                break;
            case VehicleCreateCodeOptions.ChargingIDAlreadyExists:
                message = 'vehicles.errors.chargingid_exists';
                break;
            case VehicleCreateCodeOptions.IssuerNotFound:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUpdateResult(code: VehicleUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case VehicleUpdateCodeOptions.LicencePlateAlreadyExists:
                message = 'vehicles.errors.licence_plate_exists';
                break;
            case VehicleUpdateCodeOptions.OwnIDAlreadyExists:
                message = 'vehicles.errors.id_exists';
                break;
            case VehicleUpdateCodeOptions.ChargingIDAlreadyExists:
                message = 'vehicles.errors.chargingid_exists';
                break;
            case VehicleUpdateCodeOptions.NotFound:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
