﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';
import { VehiclesService } from '../vehicles.service';

// COMPONENTS
import { EditVehicleComponent } from './edit-vehicle.component';
import { EditVehicleSettingsComponent } from './settings/edit-vehicle-settings.component';
import { EditVehicleGroupMembershipComponent } from './group-membership/vehicle-group-membership.component';
import { VehicleInactivationPeriodsComponent } from './inactivation-periods/inactivation-periods.component';
import { EditVehicleCorrectionFactorsComponent } from './correction-factors/edit-correction-factors.component';
import { EditVehicleTargetBaselinesComponent } from './target-baselines/edit-target-baselines.component';

const EditVehiclesRoutes: Routes = [
    {
        path: '',
        component: EditVehicleComponent,
        canActivate: [AuthGuard]
    },   
    {
        path: 'settings',
        component: EditVehicleSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.category.basic.settings'
        }
    },
    {
        path: 'groups',
        component: EditVehicleGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.category.basic.groups'
        }
    },
    {
        path: 'correction-factors',
        component: EditVehicleCorrectionFactorsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'energy_category.correction_factors'
        }
    },
    {
        path: 'target-baselines',
        component: EditVehicleTargetBaselinesComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'energy_category.target_baselines'
        }
    },
    {
        path: 'inactivation',
        component: VehicleInactivationPeriodsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'vehicles.category.readings.inactivation'
        }
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(EditVehiclesRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        VehiclesService
    ]
})
export class EditVehicleRoutingModule { }
