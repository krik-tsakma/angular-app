
// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { SettingsListModule } from '../settings-list/settings-list.module';
import { VehiclesRoutingModule } from './vehicles-routing.module';
import { EditVehicleSettingsModule } from './edit/settings/edit-vehicle-settings.module';
import { VehicleTypesListModule } from '../../common/vehicle-types-list/vehicle-types-list.module';
import { AssetGroupsModule } from '../../common/asset-groups/asset-groups.module';
import { ImportExportModule } from '../import-export/import-export.module';

// COMPONENTS
import { ViewVehiclesComponent } from './view/view-vehicles.component';

// SERVICES
import { VehiclesService } from './vehicles.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/vehicles/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        VehiclesRoutingModule,
        SettingsListModule,
        EditVehicleSettingsModule,
        VehicleTypesListModule,
        AssetGroupsModule,
        ImportExportModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewVehiclesComponent
    ],
    providers: [
        VehiclesService
    ]
})
export class VehiclesModule { }
