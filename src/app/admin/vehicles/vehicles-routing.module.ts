// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';
import { VehiclesService } from './vehicles.service';

// COMPONENTS
import { ViewVehiclesComponent } from './view/view-vehicles.component';
import { EditVehicleSettingsComponent } from './edit/settings/edit-vehicle-settings.component';

const VehiclesRoutes: Routes = [
    {
        path: '',
        component: ViewVehiclesComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditVehicleSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-vehicle.module').then((m) => m.EditVehicleModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }   
];

@NgModule({
    imports: [
        RouterModule.forChild(VehiclesRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        VehiclesService
    ]
})
export class VehiclesRoutingModule { }
