// FRAMEWORK
import { Component, OnDestroy, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// RxJS
import { Subscription } from 'rxjs/Subscription';

// SERVICES
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { ImportExportService } from './import-export.service';
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';

// TYPES 
import { 
    ImportRequest, 
    ExportRequest, 
    DiaIogImportExportRequest, 
    ImportExportMessage, 
    ExportTypeOptions 
} from './import-export.types';
import { FileUploadResult, FileUploadResultCodeOptions, FileUploadErrorMessages } from '../../common/file-upload-types';
import { FileExportNotificationService } from './file-export-notification.service';
import { KeyName } from '../../common/key-name';

@Component({
    selector: 'import-export',
    templateUrl: 'import-export.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['import-export.less'],
})

export class ImportExportComponent implements OnInit, OnDestroy {    

    @ViewChild('fileInput', { static: false }) public fileInput;
    public exportLoading: boolean;
    public importLoading: boolean;

    public message: ImportExportMessage = {} as ImportExportMessage;
    public request: DiaIogImportExportRequest = {} as DiaIogImportExportRequest;
    public exportOptions: KeyName[];
    public exportSelection: string;

    private serviceSubscriber: Subscription;
    private timeoutBeforeClosing = 3000;

    constructor(        
        public service: ImportExportService, 
        private dialogRef: MatDialogRef<ImportExportComponent>,             
        private unsubscribe: UnsubscribeService,
        private notificationService: FileExportNotificationService,
        private translate: CustomTranslateService,
        private userOptions: UserOptionsService) {
            // foo
    }

    public ngOnInit() {
        this.exportOptions = this.getExportOptions();
    }

    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }


    /*
    * Uploads a file to the server
    */
    public upload(): void {
        const fi = this.fileInput.nativeElement as HTMLInputElement;
        if (fi.files && fi.files[0]) {
            this.importLoading = true;
            const fileToUpload: ImportRequest = {
                uid: this.userOptions.getUser().uid,
                type: this.request.type,
                file: fi.files[0]
            }; 

            this.notificationService.Start()
                .then(() => {
                    this.unsubscribe.removeSubscription(this.serviceSubscriber);
                    this.serviceSubscriber = this.service
                        .import(fileToUpload)
                        .subscribe((res: FileUploadResult) => {
                            this.importLoading = false;
                            if (res.code === FileUploadResultCodeOptions.Ok) {
                                this.message.success = 'import_export.success';
                                setTimeout(() => this.close(), this.timeoutBeforeClosing);
                            } else {
                                this.message.error = 'form.errors.unknown';                        
                                const messageCode = FileUploadErrorMessages.find((x) => x.id === res.code);
                                if (messageCode) {
                                    this.message.error = messageCode.term;
                                }      
                                
                                setTimeout(() => this.close(), this.timeoutBeforeClosing);
                            }
                        },
                        (err) => {
                            this.message.error = 'form.errors.unknown';
                            this.importLoading = false;
                        });
                })
                .catch(() => {
                    this.message.error = 'form.errors.unknown';
                    this.exportSelection = null;
                    this.exportLoading = false;
                });
        } else {            
            this.message.error = 'file_upload.errors.null';
        }
    }


   /*
    * Sends the export request to the server
    */
    public export(selectedOption: ExportTypeOptions) {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);

        const request: ExportRequest = {
            type: this.request.type,
            exportType: selectedOption,
            uid: this.userOptions.getUser().uid
        };

        
        this.notificationService.Start()
            .then(() => {
                this.exportLoading = true;
                this.serviceSubscriber = this.service
                    .export(request)
                    .subscribe((status: number) => {
                        this.exportLoading = false;
                        this.exportSelection = null;
                        if (status === 200) {
                            this.message.success = 'import_export.success';
                            // start listening for file export notifications
                            
                            setTimeout(() => this.close(), this.timeoutBeforeClosing);
                        } else {
                            this.message.error = 'data.error';
                        }
                    }, (err) => {
                        this.message.error = 'data.unknown-error';
                        this.exportSelection = null;
                        this.exportLoading = false;
                    });
            })
            .catch(() => {
                this.message.error = 'form.errors.unknown';
                this.exportSelection = null;
                this.exportLoading = false;
            });
    }


   /*
    * Closes the dialog
    */
    public close() {
        this.dialogRef.close('no');
    }
    

   /*
    * Gets the possible export types
    */ 
   private getExportOptions(): KeyName[] {
        const selectTerm = this.translate.instant('form.actions.select');
        return [
            {
                id: null,
                name: selectTerm + '...',
                enabled: false,
            },
            {
                id: ExportTypeOptions.Export,
                name: 'Company data',
                term: 'import_export.export.data',
                enabled: true
            },
            {
                id: ExportTypeOptions.Template,
                name: 'Template',
                term: 'import_export.export.template',
                enabled: true
            }
        ];
    }
}
