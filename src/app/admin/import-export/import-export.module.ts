// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';

// COMPONENTS
import { ImportExportComponent } from './import-export.component';

// SERVICES
import { ImportExportService } from './import-export.service';
import { ImportExportDialogService } from './import-export-dialog.service';


@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        ImportExportComponent
    ],
    entryComponents: [
        ImportExportComponent
    ],
    exports: [
        ImportExportComponent
    ],
    providers: [
        ImportExportService,
        ImportExportDialogService
    ]   
})

export class ImportExportModule {}
