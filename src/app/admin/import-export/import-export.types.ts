import { KeyName } from '../../common/key-name';

// =====================
// COMMON
// =====================

export enum ImportExportTypeOptions {
    VehicleEnergyTargetsBaselines = 0,
    VehicleTypeEnergyTargetsBaselines = 1,
    VehicleEnergyCorrectionFactors = 2,
    VehicleTypeEnergyCorrectionFactors = 3,
    Vehicles = 4,
    HumanResources = 5
}

export interface DiaIogImportExportRequest {
    title: string;
    type: ImportExportTypeOptions;
}


export enum ExportTypeOptions {   
    Export = 0,
    Template = 1,
    Errors = 2
}

// =====================
// EXPORT
// =====================

export interface ExportRequest {
    type: ImportExportTypeOptions;
    exportType: ExportTypeOptions;
    uid: string;
}

export interface ExportFileNotification {
    uid: string;
    fileURL: string;
    fileName: string;
    type: ImportExportTypeOptions;
    exportType: ExportTypeOptions;
    timestamp: string;
    code: ExportFileCodeOptions;
    
}

export enum CodeType {   
    export = 0,
    import = 1
}

export enum ExportFileCodeOptions {   
    Exported = 0,
    ErrorRetrievingDataForExport = 1,
    ErrorWritingToFile = 2,
    ErrorUploadingFile = 3,
    UnknownError = 10
}

// =====================
// IMPORT
// =====================

export interface ImportRequest {
    uid: string;
    type: ImportExportTypeOptions;
    file: File;
}

export interface ImportFileNotification {
    uid: string;
    timestamp: string;
    code: ImportFileCodeOptions;
    type: ImportExportTypeOptions;
    errorFileURL: string;
}

export enum ImportFileCodeOptions {   
    Imported = 0,
    ErrorReadingFromFile = 1,
    ErrorWritingToErrorsFile = 2,
    ErrorUploadingErrorsFile = 3,
    ErrorDownloadingImportFile = 4,
    ImportCompletedWithErrors = 5,
    UnknownError = 10
}

// =====================
// IMPORT NOTIFICATION
// =====================



// =====================
// NOTIFICATION DIALOG
// =====================

export interface ExportFileView {
    name: string;
    typeName: string;
    fileURL: string;
    timestamp: string;    
    code: ExportFileCodeOptions | ImportFileCodeOptions;
    codeType: CodeType;
    message?: string;
    error?: boolean;
    download?: boolean;
    exportType?: string;
}
export interface ImportExportMessage {
    success: string;
    error: string;
}

export const ImportExportTypes: KeyName[] = [
    {
        id: ImportExportTypeOptions.VehicleEnergyTargetsBaselines,
        name: 'import_export.types.targets_baselines.vehicles' 
    },
    {
        id: ImportExportTypeOptions.VehicleEnergyCorrectionFactors,
        name: 'import_export.types.correction_factors.vehicles' 
    },
    {
        id: ImportExportTypeOptions.VehicleTypeEnergyTargetsBaselines,
        name: 'import_export.types.targets_baselines.vehicle_types' 
    },
    {
        id: ImportExportTypeOptions.VehicleTypeEnergyCorrectionFactors,
        name: 'import_export.types.correction_factors.vehicle_types' 
    },
    {
        id: ImportExportTypeOptions.Vehicles,
        name: 'assets.vehicles' 
    },
    {
        id: ImportExportTypeOptions.HumanResources,
        name: 'assets.drivers' 
    }
];
