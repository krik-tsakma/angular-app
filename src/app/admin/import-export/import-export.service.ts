// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { ImportRequest, ExportRequest } from './import-export.types';
import { FileUploadResult } from '../../common/file-upload-types';
import { ImportExportTypeOptions } from './import-export.types';

@Injectable()
export class ImportExportService {
    private baseURL: string;
   
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/';
        }
        
   
    public export(entity: ExportRequest): Observable<number>  {
        const basePath = this.getPath(entity.type);
        const params = new HttpParams({
            fromObject: {
                ExportType: entity.exportType.toString(),
                Type: entity.type.toString(),
                UID: entity.uid            
            }
        });

        return this.authHttp
            .get(this.baseURL + basePath + '/Export', { params, observe: 'response' })   
            .pipe(
                map((res) => {
                    return res.status;
                })                
            );   
    }



    public import(req: ImportRequest): Observable<FileUploadResult> {
        const basePath = this.getPath(req.type);
        const input = new FormData();
        input.append('File', req.file);
        input.append('Type', req.type.toString());
        input.append('UID', req.uid);

        return this.authHttp
            .post(this.baseURL + basePath + '/Import', input);
    }

    private getPath(type: ImportExportTypeOptions): string {
        
        switch (type) {
            case ImportExportTypeOptions.VehicleTypeEnergyTargetsBaselines:
            case ImportExportTypeOptions.VehicleTypeEnergyCorrectionFactors:
            case ImportExportTypeOptions.VehicleEnergyCorrectionFactors:
            case ImportExportTypeOptions.VehicleEnergyTargetsBaselines:
            case ImportExportTypeOptions.Vehicles:
                return 'VehiclesFile';
            case ImportExportTypeOptions.HumanResources:
                return 'DriversFile';
        }
    }
} 
