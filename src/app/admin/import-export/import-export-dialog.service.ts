// FRAMEWORK
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { ImportExportComponent } from './import-export.component';

// TYPES
import { DiaIogImportExportRequest } from './import-export.types';

@Injectable()
export class ImportExportDialogService {
   
    constructor(private dialog: MatDialog) { }       

    public openDialog(params: DiaIogImportExportRequest): Observable<string> {

        const dialogRef: MatDialogRef<ImportExportComponent> = this.dialog.open(ImportExportComponent);
        dialogRef.componentInstance.request.title = params.title;
        dialogRef.componentInstance.request.type = params.type;

        return dialogRef.afterClosed();
    }

} 
