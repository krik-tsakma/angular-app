// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Subject } from 'rxjs';

// SIGNALR
import * as signalR from '@aspnet/signalr';

// SERVICES
import { Config } from '../../config/config';

// TYPES
import { ExportFileNotification, ImportFileNotification } from './import-export.types';

@Injectable()
export class FileExportNotificationService {
    public exportNotificationReceived: Subject<ExportFileNotification> = new Subject<ExportFileNotification>();
    public importNotificationReceived: Subject<ImportFileNotification> = new Subject<ImportFileNotification>();

    private hubConnection: signalR.HubConnection = null;
    private hubEndPoint: string = this.config.get('importExportHub');
   
    constructor(private config: Config) {
        // foo
    }
        
    public Start(): Promise<void> {
        return new Promise((resolve, reject) => {

            if (this.hubConnection !== null) {
                resolve();
                return;
            }
            this.hubConnection = new signalR.HubConnectionBuilder()
                                .withUrl(this.hubEndPoint)
                                .build();
            
            this.hubConnection.on('OnFileExport', (efn: ExportFileNotification) => {
                // console.log('export notification:', efn);
                this.exportNotificationReceived.next(efn);
            });

            this.hubConnection.on('OnFileImport', (ifn: ImportFileNotification) => {
                // console.log('import notification:', ifn);
                this.importNotificationReceived.next(ifn);
            });

            this.hubConnection
                .start()
                .then(() => {
                    console.log('Connection with Import / Export Files Hub started!');
                    resolve();
                })
                .catch(() => {
                    console.log('Error while establishing connection with Import / Export Files Hub');
                    reject();
                });
        });
    }

    public Stop(): void {
        if (this.hubConnection === null) {
            return;
        }
        this.hubConnection
            .stop()
            .then(() => {
                this.hubConnection = null;
            })
            .catch(() => console.log('Error while terminating connection with Import / Export Hub'));
    }
} 
