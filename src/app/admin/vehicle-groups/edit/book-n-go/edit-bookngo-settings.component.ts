// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// RXJS
import { Observable } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { VehicleGroupsService } from '../../vehicle-groups.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { TariffsService } from '../../../book-n-go/tariffs/tariffs.service';
import { FairUsePoliciesService } from '../../../book-n-go/fair-use-policies/fair-use-policies.service';
import { AdminLabelService } from '../../../admin-label.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { WeeklyTimeScheduleDialogService } from '../../../../common/weekly-time-schedule/weekly-time-schedule.service';
import { Config } from '../../../../config/config';

// TYPES
import { VehicleGroupBookNGo, VehicleGroupSessionName } from '../../vehicle-groups.types';
import { SearchType } from '../../../../shared/search-mechanism/search-mechanism-types';
import { FairUsePolicy, FairUsePoliciesRequest } from '../../../book-n-go/fair-use-policies/fair-use-policies.types';
import { Tariff } from '../../../book-n-go/tariffs/tariffs.types';
import { KeyName } from '../../../../common/key-name';
import { BookNGoResults } from '../../../book-n-go/book-n-go.types';
import { TariffsRequest } from '../../../book-n-go/tariffs/tariffs.types';
import { TableColumnSetting, TableActionOptions, TableCellFormatOptions } from '../../../../shared/data-table/data-table.types';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { WeeklyTimeSchedule, DayOfWeek } from '../../../../common/weekly-time-schedule/weekly-time-schedule-types';

@Component({
    selector: 'edit-vehicle-group-bookngo-settings',
    templateUrl: 'edit-bookngo-settings.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditVehicleGroupBookNGoSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public group: VehicleGroupBookNGo = {} as VehicleGroupBookNGo;
    public groupUsersLoading: boolean;
    public groupUsersItems: Observable<KeyName[]>;
    public searchTypes: any = SearchType;
    public driverGroupsTableSettings: TableColumnSetting[];
    public driverAuthorizationEditMode: boolean = false;
    public tariffs: Tariff[] = [];
    public fairUsePolicies: FairUsePolicy[] = [];

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected router: Router,
        protected labelService: AdminLabelService,
        protected confirmDialogService: ConfirmDialogService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: VehicleGroupsService,
        private tariffsService: TariffsService,
        private fairUsePoliciesService: FairUsePoliciesService,
        private scheduleDialogService: WeeklyTimeScheduleDialogService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar, null, confirmDialogService);
        this.labelService.setSessionItemName(VehicleGroupSessionName);

        this.driverGroupsTableSettings =  [
            {
                primaryKey: 'driverGroupName',
                header: 'assets.group.name',
            },
            {
                primaryKey: 'fairUsePolicyName',
                header: 'book_n_go.fair_use_policy.title',
                format: TableCellFormatOptions.string,
            },
            {
                primaryKey: 'tariffName',
                header: 'vehicle.group.book_n_go.primary_tariff',
                format: TableCellFormatOptions.string,
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete],
            }
        ];
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.get(params.id);
                    this.getOptions();
                }
            });
    }

    // =========================
    // LOCATION SEARCH
    // =========================

    public onLocationSelect(item?: KeyName) {
        this.group.pickupPointID = item ? item.id : null;
        this.group.pickupPointName = item ? item.name : null;
    }

    // =========================
    // SCHEDULE
    // =========================

    public openSchedule() {
        
        const weekSchedule = {
            items: [
                {
                    day: DayOfWeek.Sunday,
                    hours: []
                },
                {
                    day: DayOfWeek.Monday,
                    hours: []
                },
                {
                    day: DayOfWeek.Tuesday,
                    hours: []
                },
                {
                    day: DayOfWeek.Wednesday,
                    hours: []
                },
                {
                    day: DayOfWeek.Thursday,
                    hours: []
                },
                {
                    day: DayOfWeek.Friday,
                    hours: []
                },
                {
                    day: DayOfWeek.Saturday,
                    hours: []
                }
            ]
        } as WeeklyTimeSchedule;

        if (!this.group.weeklySchedule || !this.group.weeklySchedule.items || this.group.weeklySchedule.items.length === 0) {
            this.group.weeklySchedule = {
                items: weekSchedule.items
            };
        }

        this.scheduleDialogService
            .open(this.group.weeklySchedule)
            .subscribe((result: WeeklyTimeSchedule) => {  
                if (result && result.items) {
                    this.group.weeklySchedule = {
                        items: result.items
                    };
                }
            });        
    }

    // =========================
    // DATA
    // =========================
    
    public onSubmit({ value, valid }: { value: VehicleGroupBookNGo, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();      

        this.serviceSubscriber = this.service
            .updateBookNGoSettings(this.group)
            .subscribe((errorMessage: string) => {
                this.loading = false;
                if (errorMessage === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    this.snackBar.open(errorMessage, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getBookNGoSettingsByID(id)
            .subscribe((res: VehicleGroupBookNGo) => {
                this.loading = false;
                this.group = res;
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }
    

    private getOptions() {
        const noneTariff = { id: null, name: '-' } as Tariff;
        this.tariffs.push(noneTariff);
        this.tariffsService
            .get({ searchTerm: '', sorting: '', pageSize: 100} as TariffsRequest)
            .subscribe((response: BookNGoResults<Tariff>) => {
                    this.loading = false;
                    this.tariffs = this.tariffs.concat(response.results);
                    this.tariffs.forEach((t) => t.enabled = true);
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
            });

        const noneFairUsePolicy =  { id: null, name: '-' } as FairUsePolicy;
        this.fairUsePolicies.push(noneFairUsePolicy);
        this.fairUsePoliciesService
            .get({ searchTerm: '', sorting: '', pageSize: 100 } as FairUsePoliciesRequest)
            .subscribe((response: BookNGoResults<FairUsePolicy>) => {
                    this.loading = false;
                    this.fairUsePolicies = this.fairUsePolicies.concat(response.results);
                    this.fairUsePolicies.forEach((t) => t.enabled = true);
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
            });
    }
}
