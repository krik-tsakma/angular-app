// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, Renderer } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { AdminLabelService } from '../../../admin-label.service';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { VehicleGroupsService } from '../../vehicle-groups.service';
import { Config } from '../../../../config/config';

// TYPES
import { KeyName } from '../../../../common/key-name';
import { TableColumnSetting, TableActionOptions, TableColumnSortOrderOptions, TableColumnSortOptions, TableActionItem } from '../../../../shared/data-table/data-table.types';
import { SearchType } from '../../../../shared/search-mechanism/search-mechanism-types';
import { 
    VehicleGroupSessionName, 
    AuthorizedDriverGroupsRequest,
    UnauthorizedDriverGroupsRequest,
    VehicleGroupAuthorizeDriverCodeOptions,
    VehicleGroupUnauthorizeDriverCodeOptions,
    AuthorizeDriverGroupRequest
} from '../../vehicle-groups.types';
import { ConfirmationDialogParams } from '../../../../shared/confirm-dialog/confirm-dialog-types';

@Component({
    selector: 'edit-vehicle-group-authorized-driver-groups',
    templateUrl: 'edit-vehicle-group-authorized-driver-groups.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditVehicleGroupAuthorizedDriverGroupsComponent extends AdminBaseComponent implements OnInit {
    public loading: boolean;
    public loadingUnauthorizedGroups: boolean;
    public authorizedGroups: KeyName[] = [];
    public unauthorizedGroups: KeyName[] = [];
    public request: AuthorizedDriverGroupsRequest = {} as AuthorizedDriverGroupsRequest;
    public tableSettings: TableColumnSetting[];
    public searchTypes: any = SearchType;
    public memberToAdd: string;
    @ViewChild('wrapper', { static: true }) public wrapperElm: ElementRef;
    
    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected confirmDialogService: ConfirmDialogService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: VehicleGroupsService,
        private renderer: Renderer
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar, null, confirmDialogService);
        labelService.setSessionItemName(VehicleGroupSessionName);

        this.tableSettings =  [
            {
                primaryKey: 'name',
                header: 'assets.group.name',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'id',
                header: ' ',
                actions: [TableActionOptions.delete]
            }
        ];
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
                    this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
                    this.request.id = params.id;
                    this.getAuthorizedDriverGroups(true);
                }
            });
    }

    // ----------------
    // TABLE
    // ----------------

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }
        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.delete: 
                const req = {
                    title: 'vehicle.group.authorized_drivers.unauthorize.title',
                    message: 'form.warnings.are_you_sure',
                    submitButtonTitle: 'form.actions.continue'
                } as ConfirmationDialogParams;
                this.openDialog(req).subscribe((response) => {
                    if (response === true) {
                        this.onUnauthorizeDriverGroup(id);
                    }
                });
                break;
            default: 
                break;

        }
    }

    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.getAuthorizedDriverGroups(true);
    }


    // Fetch unauthorized users
    public getUnauthorizedDriverGroups(term: string) {
        this.message = '';
        this.loadingUnauthorizedGroups = true;
        this.unauthorizedGroups = new Array<KeyName>();

        // first stop currently executing requests
        this.unsubscribeService();

        const req = {
            id: this.request.id,
            sorting: 'Name asc',
            searchTerm: term
         } as UnauthorizedDriverGroupsRequest;

        // then start the new request
        this.serviceSubscriber = this.service
            .getUnauthorizedDriverGroups(req)
            .subscribe(
                (response: KeyName[]) => {
                    this.loadingUnauthorizedGroups = false;
                    this.unauthorizedGroups = response;

                    if (!this.unauthorizedGroups || this.unauthorizedGroups.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loadingUnauthorizedGroups = false;
                    this.message = 'data.error';
                }
            );
    }

    public onAuthorizeDriverGroup(driverGroupID: number) {
        if (!driverGroupID) {
            return;
        }
        
        this.loading = true;
        this.unsubscribeService();

        const req = {
            driverGroupID: driverGroupID,
            vehicleGroupID: this.request.id
        } as AuthorizeDriverGroupRequest;

        this.unauthorizedGroups = [];
        this.focusOutFromAutocomplete();

        this.serviceSubscriber = this.service
            .authorizeDriverGroup(req)
            .subscribe((res: VehicleGroupAuthorizeDriverCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('vehicle.group.authorized_drivers.authorize.success', 'form.actions.close');
                    this.getAuthorizedDriverGroups(true);
                } else {
                    const message = this.getAuthorizeResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }
    
    // ----------------
    // PRIVATE
    // ----------------

    private onUnauthorizeDriverGroup(driverGroupID: number) {
        this.loading = true;
        this.unsubscribeService();

        const req = {
            driverGroupID: driverGroupID,
            vehicleGroupID: this.request.id
        } as AuthorizeDriverGroupRequest;
        
        this.serviceSubscriber = this.service
            .unauthorizeDriverGroup(req)
            .subscribe((res: VehicleGroupUnauthorizeDriverCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('vehicle.group.authorized_drivers.unauthorize.success', 'form.actions.close');
                    this.getAuthorizedDriverGroups(true);
                } else {
                    const message = this.getUnauthorizeResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // Fetch authorized users
    private getAuthorizedDriverGroups(reset: boolean) {
        this.message = '';
        this.loading = true;
        if (reset) {
            this.authorizedGroups = new Array<KeyName>();
        }   

        // first stop currently executing requests
        this.unsubscribeService();

        // then start the new request
        this.serviceSubscriber = this.service
            .getAuthorizedDriverGroups(this.request)
            .subscribe(
                (response: KeyName[]) => {
                    this.loading = false;
                    this.authorizedGroups = response;

                    if (!this.authorizedGroups || this.authorizedGroups.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }

    private focusOutFromAutocomplete() {
        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.wrapperElm.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }


    // =========================
    // RESULTS
    // =========================

    private getAuthorizeResult(code: VehicleGroupAuthorizeDriverCodeOptions): string {
        let message = '';
        switch (code) {
            case VehicleGroupAuthorizeDriverCodeOptions.HRGroupAuthorized:
                message = 'vehicle.group.authorized_drivers.authorize.error';
                break;
            case VehicleGroupAuthorizeDriverCodeOptions.VehicleGroupNotFound:
            case VehicleGroupAuthorizeDriverCodeOptions.HRGroupNotFound:
                message = 'form.errors.not_found';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUnauthorizeResult(code: VehicleGroupUnauthorizeDriverCodeOptions): string {
        let message = '';
        switch (code) {
            case VehicleGroupUnauthorizeDriverCodeOptions.HRGroupNotAuthorized:
                message = 'vehicle.group.authorized_drivers.unauthorize.error';
                break;
            case VehicleGroupUnauthorizeDriverCodeOptions.VehicleGroupNotFound:
            case VehicleGroupUnauthorizeDriverCodeOptions.HRGroupNotFound:
                message = 'form.errors.not_found';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
