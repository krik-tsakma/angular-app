
// FRAMEWORK
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';
import { EditVehicleGroupRoutingModule } from './edit-vehicle-group-routing.module';
import { EditVehicleGroupSettingsModule } from './settings/edit-vehicle-group-settings.module';
import { AssetGroupsModule } from '../../../common/asset-groups/asset-groups.module';
import { WeeklyTimeScheduleModule } from '../../../common/weekly-time-schedule/weekly-time-schedule.module';

// COMPONENTS
import { EditVehicleGroupComponent } from './edit-vehicle-group.component';
import { EditVehicleGroupBookNGoSettingsComponent } from './book-n-go/edit-bookngo-settings.component';
import { EditVehicleGroupMembersComponent } from './members/edit-vehicle-group-members.component';
import { EditVehicleGroupAuthorizedUsersComponent } from './authorized-users/edit-vehicle-group-authorized-users.component';
import { EditVehicleGroupAuthorizedDriverGroupsComponent } from './authorized-driver-groups/edit-vehicle-group-authorized-driver-groups.component';

// SERVICES
import { VehicleGroupsService } from '../vehicle-groups.service';

@NgModule({
    imports: [
        SharedModule,
        EditVehicleGroupRoutingModule,
        EditVehicleGroupSettingsModule,
        SettingsListModule,
        AssetGroupsModule,
        WeeklyTimeScheduleModule
    ],
    declarations: [
        EditVehicleGroupComponent,
        EditVehicleGroupMembersComponent,
        EditVehicleGroupAuthorizedUsersComponent,
        EditVehicleGroupBookNGoSettingsComponent,
        EditVehicleGroupAuthorizedDriverGroupsComponent,
    ],  
    providers: [
        VehicleGroupsService,    
    ]
})
export class EditVehicleGroupModule { }
