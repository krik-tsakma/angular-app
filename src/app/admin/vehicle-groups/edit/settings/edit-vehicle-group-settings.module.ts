﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../../shared/shared.module';

// COMPONENTS
import { EditVehicleGroupSettingsComponent } from './edit-vehicle-group-settings.component';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        EditVehicleGroupSettingsComponent
    ],
    exports: [
        EditVehicleGroupSettingsComponent
    ]
})

export class EditVehicleGroupSettingsModule { }
