// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';
import { VehicleGroupsService } from '../vehicle-groups.service';

// COMPONENTS
import { EditVehicleGroupComponent } from './edit-vehicle-group.component';
import { EditVehicleGroupSettingsComponent } from './settings/edit-vehicle-group-settings.component';
import { EditVehicleGroupBookNGoSettingsComponent } from './book-n-go/edit-bookngo-settings.component';
import { EditVehicleGroupMembersComponent } from './members/edit-vehicle-group-members.component';
import { EditVehicleGroupAuthorizedUsersComponent } from './authorized-users/edit-vehicle-group-authorized-users.component';
import { EditVehicleGroupAuthorizedDriverGroupsComponent } from './authorized-driver-groups/edit-vehicle-group-authorized-driver-groups.component';

const EditVehicleGroupRoutes: Routes = [
    {
        path: '',
        component: EditVehicleGroupComponent,
        canActivate: [AuthGuard]
    },   
    {
        path: 'settings',
        component: EditVehicleGroupSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.settings'
        }
    },
    {
        path: 'members',
        component: EditVehicleGroupMembersComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.membership'
        }
    },
    {
        path: 'authorized-users',
        component: EditVehicleGroupAuthorizedUsersComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.access'
        }
    },
    {
        path: 'drivers',
        component: EditVehicleGroupAuthorizedDriverGroupsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'vehicle.group.category.authorized_drivers'
        }
    },
    {
        path: 'bookngo',
        component: EditVehicleGroupBookNGoSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'admin.categories.book_n_go'
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(EditVehicleGroupRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        VehicleGroupsService
    ]
})
export class EditVehicleGroupRoutingModule { }
