// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { AdminLabelService } from '../../admin-label.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { AuthService } from '../../../auth/authService';
import { Config } from '../../../config/config';

// TYPES 
import { SettingList } from '../../settings-list/settings-list.types';
import { VehicleGroupSessionName } from '../vehicle-groups.types';
import { AdminPropertyOptions, ModuleOptions } from '../../../auth/acl.types';

@Component({
    selector: 'edit-vehicle-group',
    templateUrl: 'edit-vehicle-group.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditVehicleGroupComponent {
    public options: SettingList[] = [
        {
            title: 'assets.group.category.basic',
            items: [
                {
                    option: 'assets.group.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
                {
                    option: 'assets.group.category.basic.membership',
                    params: ['members'],
                    icon: 'directions_car'
                },
                {
                    option: 'assets.group.category.basic.access',
                    params: ['authorized-users'],
                    icon: 'person_outline'
                }
            ]
        },
        {
            title: 'vehicle.group.category.authorized_drivers',
            items: [
                {
                    option: 'vehicle.group.category.authorized_drivers.set',
                    params: ['drivers'],
                    icon: 'vpn_key'
                }
            ]
        },
        {
            title: 'vehicle.group.category.book_n_go',
            items: [
                {
                    option: 'vehicle.group.category.book_n_go.settings',
                    params: ['bookngo'],
                    icon: 'timeline',
                    allowed: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.BookNGo)
                }
            ]
        }
    ];

    constructor(public labelService: AdminLabelService,
                private previousRouteService: PreviousRouteService,
                private router: Router,
                private authService: AuthService,
                private configService: Config) {
            labelService.setSessionItemName(VehicleGroupSessionName);
    }

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
