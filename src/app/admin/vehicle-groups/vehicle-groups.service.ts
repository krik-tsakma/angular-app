
// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import { 
    VehicleGroupsResult, 
    VehicleGroupCreateCodeOptions, 
    VehicleGroupSettings, 
    VehicleGroupSettingsCodeOptions,
    VehicleGroupBookNGo,
    VehicleGroupAddMemberCodeOptions,
    VehicleGroupRemoveMemberCodeOptions,
    VehicleGroupUnauthorizeUserCodeOptions,
    VehicleGroupAuthorizeUserCodeOptions,
    AuthorizedDriverGroupsRequest,
    UnauthorizedDriverGroupsRequest,
    VehicleGroupAuthorizeDriverCodeOptions,
    VehicleGroupUnauthorizeDriverCodeOptions,
    AuthorizeDriverGroupRequest
} from './vehicle-groups.types';
import { SearchTermRequest } from '../../common/pager';
import { AssetGroupItem, AssetGroupMembersRequest, AssetGroupMembersResult, AssetGroupMemberAddRemoveRequest } from '../asset-groups/asset-groups.types';
import { BookNGoDeleteRequest, BookNGoCrudResult, BookNGoUpsertRequest, BookNGoCrudCodeOptions } from '../book-n-go/book-n-go.types';
import { KeyName } from '../../common/key-name';

@Injectable()
export class VehicleGroupsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/VehicleGroups';
        }

        // Get all vehicle groups
        public get(request: SearchTermRequest): Observable<VehicleGroupsResult> {

            const obj = this.authHttp.removeObjNullKeys({
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Sorting: request.sorting
            });

            const params = new HttpParams({
                fromObject: obj
            });

            return this.authHttp
                .get(this.baseURL, { params });
        }


        public getByID(id: number): Observable<VehicleGroupSettings> {
            return this.authHttp
                .get(this.baseURL + '/' + id);
        }

        public create(entity: VehicleGroupSettings): Observable<VehicleGroupCreateCodeOptions>  {
            return this.authHttp
                .post(this.baseURL, entity, { observe: 'response' })
                .pipe(
                    map((res) => {
                        if (res.status === 201) {
                            return null;
                        } else  {
                            return res.body as VehicleGroupCreateCodeOptions;
                        }
                    })
                );
        }


        public update(entity: VehicleGroupSettings): Observable<VehicleGroupSettingsCodeOptions>  {
            return this.authHttp
                .put(this.baseURL, entity, { observe: 'response' })
                .pipe(
                    map((res) => {
                        // this is means empty Ok response from the server
                        if (res.status === 200 && !res.body) {
                            return null;
                        } else  {
                            return res.body as VehicleGroupSettingsCodeOptions;
                        }
                    })
                );            
        }

        public delete(id: number): Observable<number> {
            return this.authHttp
                .delete(this.baseURL + '/' + id, { observe: 'response' })
                .pipe(
                     map((res) => res.status as number)
                ); 
        }


    // ====================
    // MEMBERS
    // ====================

    public getMembers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetMembers', { params });
    }

    public getNonMembers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetNonMembers', { params });
    }

    public addMember(entity: AssetGroupMemberAddRemoveRequest): Observable<VehicleGroupAddMemberCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/AddMember', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleGroupAddMemberCodeOptions;
                    }
                })
            );            
    }

    public removeMember(entity: AssetGroupMemberAddRemoveRequest): Observable<VehicleGroupRemoveMemberCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/RemoveMember', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleGroupRemoveMemberCodeOptions;
                    }
                })
            );            
    }


    // ====================
    // AUTHORIZED USERS
    // ====================

    public getAuthorizedUsers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20'
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetAuthorizedUsers', { params });
    }


    public authorizeUser(entity: AssetGroupMemberAddRemoveRequest): Observable<VehicleGroupAuthorizeUserCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/AuthorizeUser', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleGroupAuthorizeUserCodeOptions;
                    }
                })
            );            
    }

    public getUnauthorizedUsers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetUnauthorizedUsers', { params });
    }

    public unauthorizeUser(entity: AssetGroupMemberAddRemoveRequest): Observable<VehicleGroupUnauthorizeUserCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/UnauthorizeUser', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleGroupUnauthorizeUserCodeOptions;
                    }
                })
            );            
    }


    // =========================
    // AUTHORIZED DRIVER GROUPS
    // =========================

    public getAuthorizedDriverGroups(request: AuthorizedDriverGroupsRequest): Observable<KeyName[]> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                Sorting: request.sorting,
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetAuthorizedDriverGroups/', { params });
    }


    public authorizeDriverGroup(entity: AuthorizeDriverGroupRequest): Observable<VehicleGroupAuthorizeDriverCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/AuthorizeDriverGroup', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleGroupAuthorizeDriverCodeOptions;
                    }
                })
            );            
    }

    public getUnauthorizedDriverGroups(request: UnauthorizedDriverGroupsRequest): Observable<KeyName[]> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetUnauthorizedDriverGroups', { params });
    }

    public unauthorizeDriverGroup(entity: AuthorizeDriverGroupRequest): Observable<VehicleGroupUnauthorizeDriverCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/UnauthorizeDriverGroup', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as VehicleGroupUnauthorizeDriverCodeOptions;
                    }
                })
            );            
    }

    // ====================
    // BOOK N GO
    // ====================

    public getBookNGoSettingsByID(id: number): Observable<VehicleGroupBookNGo> {
        return this.authHttp
            .get(this.baseURL + '/GetBookNGoSettings/' + id);
    }


    public updateBookNGoSettings(entity: VehicleGroupBookNGo): Observable<string>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateBookNGoSettings/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as string;
                    }
                })
            );            
    }
} 
