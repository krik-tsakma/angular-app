// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { SettingsListModule } from '../settings-list/settings-list.module';
import { EditVehicleGroupSettingsModule } from './edit/settings/edit-vehicle-group-settings.module';
import { VehicleGroupsRoutingModule } from './vehicle-groups-routing.module';

// COMPONENTS
import { ViewVehicleGroupsComponent } from './view/view-vehicle-groups.component';

// SERVICES
import { VehicleGroupsService } from './vehicle-groups.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/groups/vehicles/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        VehicleGroupsRoutingModule,
        EditVehicleGroupSettingsModule,
        SettingsListModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewVehicleGroupsComponent
    ],
    providers: [
        VehicleGroupsService
    ]
})
export class VehicleGroupsModule { }
