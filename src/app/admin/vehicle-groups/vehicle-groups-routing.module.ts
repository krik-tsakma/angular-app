
// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { EditVehicleGroupSettingsComponent } from './edit/settings/edit-vehicle-group-settings.component';
import { ViewVehicleGroupsComponent } from './view/view-vehicle-groups.component';

const VehicleGroupsRoutes: Routes = [
    {
        path: '',
        component: ViewVehicleGroupsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditVehicleGroupSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-vehicle-group.module').then((m) => m.EditVehicleGroupModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }   
];

@NgModule({
    imports: [
        RouterModule.forChild(VehicleGroupsRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class VehicleGroupsRoutingModule { }
