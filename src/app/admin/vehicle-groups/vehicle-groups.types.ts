import { PagerResults } from '../../common/pager';
import { WeeklyTimeSchedule } from '../../common/weekly-time-schedule/weekly-time-schedule-types';

export const VehicleGroupSessionName: string = 'vgn';

export interface VehicleGroupsResult extends PagerResults {
    results: VehicleGroupResultItem[];
}

export interface VehicleGroupResultItem {
    id: number;
    name: string;
    isDefault: boolean;
    isUnlinked: boolean;
}

// ====================
// BASIC INFO
// ====================

export interface VehicleGroupSettings {
    id: number;
    name: string;
    description: string;
}

export enum VehicleGroupCreateCodeOptions {
    Created = 0,
    NameAlreadyExists = 1,
    IssuerNotFound = 2,
    UnknownError = 10,
}

export enum VehicleGroupSettingsCodeOptions {
    Set = 0,
    NoSuchGroupExists = 1,
    NameAlreadyExists = 2,
    IssuerNotFound = 4,
    NotAllowedVehicleGroup = 5,
    UnknownError = 20,
}

// ====================
// BOOK N' GO
// ====================

export interface VehicleGroupBookNGo {
    id: number;
    isEnabledForBookNGo: boolean;
    pickupPointID?: number;
    pickupPointName?: string;
    showSoCBeforeBookingHours?: number;
    tariffID?: number;
    fairUsePolicyID?: number;
    weeklySchedule?: WeeklyTimeSchedule;
}

// ====================
// MEMBERS
// ===================

export enum VehicleGroupAddMemberCodeOptions {
    Added = 0,
    VehicleGroupNotFound = 1,
    VehicleGroupIsDefault = 2,
    VehicleNotFound = 3,
    VehicleAlreadyAMember = 4,
    IssuerNotFound = 5,
    NotAllowedVehicle = 6,
    NotAllowedVehicleGroup = 7,
    UnknownError = 20
}


export enum VehicleGroupRemoveMemberCodeOptions {
    Removed = 0,
    VehicleGroupNotFound = 1,
    VehicleNotAMember = 2,
    IssuerNotFound = 4,
    NotAllowedVehicle = 5,
    NotAllowedVehicleGroup = 6,
    UnknownError = 20
}

// ====================
// AUTHORIZED USERS
// ===================

export enum VehicleGroupAuthorizeUserCodeOptions {
    Added = 0,
    VehicleGroupNotFound = 1,
    UserNotFound = 2,
    UserAuthorized = 3,
    IssuerNotFound = 4,
    NotAllowedVehicleGroup = 5,
    UnknownError = 20
}

export enum VehicleGroupUnauthorizeUserCodeOptions {
    Authorized = 0,
    VehicleGroupNotFound = 1,
    UserNotFound = 2,
    UserNotAuthorized = 3,
    UnknownError = 9
}

// ====================
// AUTHORIZED DRIVERS
// ===================

export interface AuthorizedDriverGroupsRequest {
    id: number;
    sorting: string;
}

export interface UnauthorizedDriverGroupsRequest extends AuthorizedDriverGroupsRequest {
    searchTerm: string;
}

export interface AuthorizeDriverGroupRequest {
    vehicleGroupID: number;
    driverGroupID: number;
}

export enum VehicleGroupAuthorizeDriverCodeOptions {
    Authorized = 0,
    VehicleGroupNotFound = 1,
    HRGroupNotFound = 2,
    HRGroupAuthorized = 3
}

export enum VehicleGroupUnauthorizeDriverCodeOptions {
    Unauthorized = 0,
    VehicleGroupNotFound = 1,
    HRGroupNotFound = 2,
    HRGroupNotAuthorized = 3,
}
