// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { AdminLabelService } from '../../admin-label.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES 
import { SettingList } from '../../settings-list/settings-list.types';
import { DriverSessionName } from '../drivers.types';

@Component({
    selector: 'edit-driver',
    templateUrl: 'edit-driver.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditDriverComponent {
    public options: SettingList[] = [
        {
            title: 'assets.category.basic',
            items: [
                {
                    option: 'assets.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
                {
                    option: 'assets.category.basic.groups',
                    params: ['groups'],
                    icon: 'group'
                },
            ]
        },
        {
            title: 'drivers.category.user',
            items: [
                {
                    option: 'drivers.category.user.settings',
                    params: ['account'],
                    icon: 'account_box'
                }
            ]
        }
    ];

    constructor(public labelService: AdminLabelService,
                private configService: Config,
                private previousRouteService: PreviousRouteService,
                private router: Router) {
        labelService.setSessionItemName(DriverSessionName);
    }

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
