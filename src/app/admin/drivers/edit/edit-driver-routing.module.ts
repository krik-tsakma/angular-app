﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';
import { DriversService } from '../drivers.service';

// COMPONENTS
import { EditDriverComponent } from './edit-driver.component';
import { EditDriverSettingsComponent } from './settings/edit-driver-settings.component';
import { EditDriverGroupMembershipComponent } from './group-membership/driver-group-membership.component';
import { EditDriverAccountSettingsComponent } from './account/driver-account-settings.component';

const EditDriversRoutes: Routes = [
    {
        path: '',
        component: EditDriverComponent,
        canActivate: [AuthGuard]
    },   
    {
        path: 'settings',
        component: EditDriverSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.category.basic.settings'
        }
    },
    {
        path: 'groups',
        component: EditDriverGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.category.basic.groups'
        }
    },
    {
        path: 'account',
        component: EditDriverAccountSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'drivers.category.user.settings'
        }
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(EditDriversRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        DriversService
    ]
})
export class EditDriverRoutingModule { }
