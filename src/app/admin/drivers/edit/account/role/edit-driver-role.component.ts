// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, Input, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

// RXJS
import { Subscription } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../../../admin-base.component';

// SERVICES
import { DriversService } from '../../../drivers.service';
import { CustomTranslateService } from '../../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../../core/previous-route/previous-route.service';
import { Config } from '../../../../../config/config';

// TYPES
import { KeyName } from '../../../../../common/key-name';
import { 
    DriverRoleUpdateRequest, 
    DriverRoleAccountCreationUpdateCodeOptions, 
    DriverRoleUpdateCodeOptions 
} from './edit-driver-role.types';

@Component({
    selector: 'edit-driver-role',
    templateUrl: 'edit-driver-role.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditDriverRoleComponent extends AdminBaseComponent implements OnInit, AfterViewInit {
    @Input() public driverID: number;
    public hasAccount: boolean;
    public loading: boolean = true;
    public loadingDriverRoles: boolean;
    public driverRoles: KeyName[];
    public driver: DriverRoleUpdateRequest = {} as DriverRoleUpdateRequest;

    private userRoleSubscriber: Subscription;
    constructor(        
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,      
        protected configService: Config,  
        private service: DriversService        
    ) {
        super(translator, previousRouteService, router, unsubscriber, null, configService);

        this.getDriverRoles();
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterViewInit(): void {
        this.driver.id = this.driverID;
        this.get(this.driverID);
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: DriverRoleUpdateRequest, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        if (this.hasAccount === true) {
            this.serviceSubscriber = this.service
                .setRole(this.driver)
                .subscribe((res?: DriverRoleUpdateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const message  = this.getUpdateCodeResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        } else {
             this.serviceSubscriber = this.service
                .setAccountCreationRole(this.driver)
                .subscribe((res?: DriverRoleAccountCreationUpdateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const message  = this.getUpdateAccountCreationCodeResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        }
    }


    private getDriverRoles() {
        this.loadingDriverRoles = true;
        this.service
            .getDriverRolesList()
            .subscribe((res: KeyName[]) => {
                this.loadingDriverRoles = false;
                this.driverRoles = res;
                this.driverRoles.forEach((x) => { 
                    x.enabled = true;
                });

                if (!this.hasAccount) {
                    const selectOption = {
                        id: null,
                        name: this.translator.instant('form.actions.select') + '...',
                        enabled: false,
                    } as KeyName;
                    this.driverRoles.unshift(selectOption);
                }
            },
            (err) => {
                this.loadingDriverRoles = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }

    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();

        this.serviceSubscriber = this.service
            .getRole(id)
            .subscribe((res: number) => {
                this.loading = false;
                this.driver.roleID = res;
                if (res == null) {
                    this.hasAccount = false;
                    this.getAccountCreationRole(id);
                } else {
                    this.hasAccount = true;
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            }
        );
    }
    
    private getAccountCreationRole(id: number) {
        this.serviceSubscriber = this.service
            .getAccountCreationRole(id)
            .subscribe((res: number) => {
                this.loading = false;
                this.driver.roleID = res;
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            }
        );
    }

    // =========================
    // RESULTS
    // =========================

    private getUpdateCodeResult(code: DriverRoleUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case DriverRoleUpdateCodeOptions.NoAccountForThisHR:
                message = 'drivers.errors.no_user_account';
                break;
            case DriverRoleUpdateCodeOptions.NoSuchHRExists:
                message = 'form.errors.not_found';
                break;
            case DriverRoleUpdateCodeOptions.IssuerNotFound:
            case DriverRoleUpdateCodeOptions.NotAllowedHr:
            case DriverRoleUpdateCodeOptions.UnknownError:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUpdateAccountCreationCodeResult(code: DriverRoleAccountCreationUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case DriverRoleAccountCreationUpdateCodeOptions.NoSuchRoleExists:
                message = 'form.errors.not_found';
                break;
            case DriverRoleAccountCreationUpdateCodeOptions.NoSuchHRExists:
                message = 'form.errors.not_found';
                break;
            case DriverRoleAccountCreationUpdateCodeOptions.IssuerNotFound:
            case DriverRoleAccountCreationUpdateCodeOptions.NotAllowedHr:
            case DriverRoleAccountCreationUpdateCodeOptions.UnknownError:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
