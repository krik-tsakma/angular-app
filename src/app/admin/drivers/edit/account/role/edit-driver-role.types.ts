
export interface DriverRoleUpdateRequest {
    id: number;
    roleID: number;
}

export enum DriverRoleUpdateCodeOptions {
    Changed = 0,
    NoAccountForThisHR = 1,
    NoSuchHRExists = 2,
    NoSuchRoleExists = 3,
    IssuerNotFound = 4,
    NotAllowedHr = 5,
    UnknownError = 9
}

export enum DriverRoleAccountCreationUpdateCodeOptions {
    Changed = 0,
    NoSuchHRExists = 1,
    NoSuchRoleExists = 2,
    IssuerNotFound = 3,
    NotAllowedHr = 4,
    UnknownError = 9
}
