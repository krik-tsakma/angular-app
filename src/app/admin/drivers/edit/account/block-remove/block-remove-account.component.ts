// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, Input, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../../../admin-base.component';

// SERVICES
import { DriversService } from '../../../drivers.service';
import { CustomTranslateService } from '../../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../../core/previous-route/previous-route.service';
import { Config } from '../../../../../config/config';

// TYPES
import { 
    DriverBlockStatusResult, 
    DriverBlockAccountRequest, 
    DriverBlockAccountCodeOptions, 
    DriverRemoveAccountCodeOptions 
} from './block-remove-account.types';


@Component({
    selector: 'block-remove-driver-account',
    templateUrl: 'block-remove-account.html',
    encapsulation: ViewEncapsulation.None,
})

export class BlockRemoveDriverAccountComponent extends AdminBaseComponent implements OnInit, AfterViewInit {
    @Input() public driverID: number;
    public loading: boolean = true;
    public driver: DriverBlockStatusResult = {} as DriverBlockStatusResult;

    constructor(        
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,    
        protected configService: Config,   
        private service: DriversService) {
            super(translator, previousRouteService, router, unsubscriber, null, configService);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }
    
    public ngAfterViewInit(): void {
        this.get(this.driverID);
    }
    
    // =========================
    // DATA
    // =========================

    public onSubmitBlockAccount() {
        this.loading = true;
        this.unsubscribeService();

        const req = {
            id: this.driverID,
            block: !this.driver.accountBlocked
        } as DriverBlockAccountRequest;
        
        this.serviceSubscriber = this.service
            .blockAccount(req)
            .subscribe((res: DriverBlockAccountCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    let message = 'form.errors.unknown';
                    if (res === DriverBlockAccountCodeOptions.UserAccountNotFound) {
                        message = 'drivers.errors.no_user_account';
                    }
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    public onSubmitRemoveAccount() {
        this.loading = true;
        this.unsubscribeService();

        this.serviceSubscriber = this.service
            .removeAccount(this.driverID)
            .subscribe((res: DriverRemoveAccountCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    let message = 'form.errors.unknown';
                    if (res === DriverRemoveAccountCodeOptions.NotFound) {
                        message = 'drivers.errors.no_user_account';
                    }
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getAccountBlockSettingsByID(id)
            .subscribe((res: DriverBlockStatusResult) => {
                this.loading = false;
                this.driver = res;
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }
}


