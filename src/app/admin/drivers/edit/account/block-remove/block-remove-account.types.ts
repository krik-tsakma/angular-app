// ====================
// USER ACCOUNT BLOCK
// ====================

export interface DriverBlockStatusResult {
    hasAccount: boolean;
    accountBlocked: boolean;
    username: string;
}

export interface DriverBlockAccountRequest {
    id: number;
    block: boolean;
}

export enum DriverBlockAccountCodeOptions {
    HRBlockedLifted = 1,
    HRBlockedRaised = 2,
    HrNotFound = 3,
    UserAccountNotFound = 4,
    NoStateChange = 5,
    UnknownError = 10
}


// ====================
// USER ACCOUNT REMOVE
// ====================

export enum DriverRemoveAccountCodeOptions {
    Removed = 0,
    NotFound = 1,
    UnknownError = 2,
}
