// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { AdminLabelService } from '../../../admin-label.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { Config } from '../../../../config/config';

// TYPES
import { DriverSessionName } from '../../drivers.types';

@Component({
    selector: 'driver-account-settings',
    templateUrl: 'driver-account-settings.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['driver-account-settings.less']
})

export class EditDriverAccountSettingsComponent extends AdminBaseComponent implements OnInit {
    public action: string;
    public driverID: number;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute        
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(DriverSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.driverID = params.id;
                }
            });
    }
} 
