export interface DriverAccountSettingsResult {
    allowAccountCreation: boolean;
    accountCreationCode: string;
    accountCreationCodeLastSent: string;
    companyAccountCreationCode: string;
}

export interface DriverAccountSettingsRequest {
    id: number;
    allowAccountCreation: boolean;
}

export enum DriverAccountSettingsCodeOptions {
    Updated = 0,
    NoSuchHRExists = 1,
    AccountCreationCodeInvalid = 2,
    AccountCreationCodeAlreadyExists = 3,
    AccountCreationCodeNeeded = 4,
}

// ===========================
// SENT ACCOUNT CREATION CODE
// ===========================

export enum DriverAccountCodeSendOptions {
    Sent = 0,
    HumanResourceHasNoEMailAddress = 1,
    HumanResourceNotFound = 2,
    AccountCreationNotAllowed = 3
}
