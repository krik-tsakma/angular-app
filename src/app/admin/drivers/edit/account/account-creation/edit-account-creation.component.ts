
// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../../../admin-base.component';

// SERVICES
import { DriversService } from '../../../drivers.service';
import { CustomTranslateService } from '../../../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../../../shared/snackbar.service';
import { Config } from '../../../../../config/config';
import { UserOptionsService } from './../../../../../shared/user-options/user-options.service';

// TYPES
import { 
    DriverAccountSettingsRequest, 
    DriverAccountSettingsCodeOptions, 
    DriverAccountCodeSendOptions, 
    DriverAccountSettingsResult 
} from './edit-account-creation.types';
import { DateFormatterDisplayOptions } from './../../../../../shared/pipes/dateFormat.pipe';

@Component({
    selector: 'edit-driver-account-creation',
    templateUrl: 'edit-account-creation.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditDriverAccountCreationComponent extends AdminBaseComponent implements OnInit, OnChanges {
    @Input() public driverID: number;
    public loading: boolean = true;
    public driver: DriverAccountSettingsResult = {} as DriverAccountSettingsResult;
    public dateDisplayOptions: any = DateFormatterDisplayOptions;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,   
        protected configService: Config,     
        public userOptions: UserOptionsService,
        private service: DriversService
        
    ) {
        super(translator, previousRouteService, router, unsubscriber, null, configService);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }
    
    public ngOnChanges(changes: SimpleChanges): void {
        const driverChange = changes['driverID'];
        if (driverChange.currentValue) {
            this.get(this.driverID);
        }
    }
    
    // =========================
    // ACCOUNT SETTINGS
    // =========================

    public onSubmitAccountSettings() {
        this.loading = true;
        this.unsubscribeService();

        const req = {
            id: this.driverID,
            allowAccountCreation: !this.driver.allowAccountCreation
        } as DriverAccountSettingsRequest;
        
        // This is an update
        this.serviceSubscriber = this.service
            .updateAccountSettings(req)
            .subscribe((res: DriverAccountSettingsCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    const message = this.getAccountSettingsCodeResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // =========================
    // ACCOUNT CREATION CODE
    // =========================

    public onSendAccountCreationCode() {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .sendAccountCreationCode(this.driverID)
            .subscribe((res: DriverAccountCodeSendOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('drivers.account.creation.code.send_to_driver.success', 'form.actions.close');
                    this.back();
                } else {
                    const message = this.getSendAccountCreationCodeResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // =========================
    // DATA
    // =========================

    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getAccountSettingsByID(id)
            .subscribe((res: DriverAccountSettingsResult) => {
                this.loading = false;
                this.driver = res;
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }

    // =========================
    // RESULTS
    // =========================

    private getAccountSettingsCodeResult(code: DriverAccountSettingsCodeOptions): string {
        let message = '';
        switch (code) {
            case DriverAccountSettingsCodeOptions.NoSuchHRExists:
                message = 'form.errors.not_found';
                break;
            case DriverAccountSettingsCodeOptions.AccountCreationCodeNeeded:
                message = 'drivers.errors.creation.code';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getSendAccountCreationCodeResult(code: DriverAccountCodeSendOptions): string {
        let message = '';
        switch (code) {
            case DriverAccountCodeSendOptions.HumanResourceHasNoEMailAddress:
                message = 'drivers.errors.send_code.email_missing';
                break;
            case DriverAccountCodeSendOptions.AccountCreationNotAllowed:
                message = 'drivers.errors.send_code.account_creation_not_allowed';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}


