﻿
// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { EditDriverRoutingModule } from './edit-driver-routing.module';
import { EditDriverSettingsModule } from './settings/edit-driver-settings.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';
import { AssetGroupMembershipModule } from '../../../common/asset-groups-membership/asset-group-membership.module';

// SERVICES
import { DriversService } from '../drivers.service';

// COMPONENTS
import { EditDriverComponent } from './edit-driver.component';
import { EditDriverGroupMembershipComponent } from './group-membership/driver-group-membership.component';
import { EditDriverAccountSettingsComponent } from './account/driver-account-settings.component';
import { EditDriverRoleComponent } from './account/role/edit-driver-role.component';
import { EditDriverAccountCreationComponent } from './account/account-creation/edit-account-creation.component';
import { BlockRemoveDriverAccountComponent } from './account/block-remove/block-remove-account.component';

@NgModule({
    imports: [
        SharedModule,
        EditDriverRoutingModule,
        EditDriverSettingsModule,
        SettingsListModule,
        AssetGroupMembershipModule,
    ],
    declarations: [
        EditDriverComponent,
        EditDriverGroupMembershipComponent,
        EditDriverAccountSettingsComponent,
        EditDriverRoleComponent,
        EditDriverAccountCreationComponent,
        BlockRemoveDriverAccountComponent
    ],
    providers: [
        DriversService
    ]
})
export class EditDriverModule {

}
