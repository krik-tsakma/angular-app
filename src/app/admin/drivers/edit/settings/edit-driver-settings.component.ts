// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// RXJS
import { Subscription } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { DriversService } from '../../drivers.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import { DriverSettings, DriverCreateCodeOptions, DriverUpdateCodeOptions, PINTypes, DriverSessionName } from '../../drivers.types';
import { KeyName } from '../../../../common/key-name';

@Component({
    selector: 'edit-driver-settings',
    templateUrl: 'edit-driver-settings.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-driver-settings.less']
})

export class EditDriverSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public action: string;
    public driver: DriverSettings = {} as DriverSettings;
    public pinTypeOptions: KeyName[] = [
        {
            id: PINTypes.iButton,
            name: 'iButton',
            term: 'drivers.pin.type.iButton',
            enabled: true,
        },
        {
            id: PINTypes.MYFARE_UID,
            name: 'MIFAREUID',
            term: 'drivers.pin.type.mifare_uid',
            enabled: true,
        },
        {
            id: PINTypes.DTCO_CARD,
            name: 'DTCOCard',
            term: 'drivers.pin.type.dtco_card',
            enabled: true,
        }
    ];

    constructor(        
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected router: Router,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: DriversService
        
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(DriverSessionName);

        this.driver.pinType = PINTypes.iButton;
        this.pinTypeOptions.forEach((w) => {
            this.translator
                .get(w.term)
                .subscribe((t) => {
                    w.name = t;
                });
        });
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                this.action = params.id
                            ? 'form.actions.edit'
                            : 'form.actions.create';
                if (params.id) {
                    this.get(params.id);
                }
            });
    }


    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: DriverSettings, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is create
        if (typeof(this.driver.id) === 'undefined' || this.driver.id === 0) {
            this.serviceSubscriber = this.service
                .create(this.driver)
                .subscribe((res: DriverCreateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {                        
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {                        
                        const message = this.getCreateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else { 
            // This is an update
            this.serviceSubscriber = this.service
                .update(this.driver)
                .subscribe((res: DriverUpdateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.labelService.set(`${this.driver.firstname} ${this.driver.lastname}`);
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const message = this.getUpdateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        }
    }

    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((res: DriverSettings) => {
                this.loading = false;
                this.driver = res;
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }


    // =========================
    // RESULTS
    // =========================
    private getCreateResult(code: DriverCreateCodeOptions): string {
        let message = '';
        switch (code) {
            case DriverCreateCodeOptions.FullNameAlreadyExists:
                message = 'drivers.errors.fullname_exists';
                break;
            case DriverCreateCodeOptions.EmailAddressAlreadyExists:
                message = 'drivers.errors.email_exists';
                break;
            case DriverCreateCodeOptions.EmailAddressInvalid:
                message = 'form.errors.email_invalid';
                break;
            case DriverCreateCodeOptions.MobilePhoneNumberInvalid:
                message = 'form.errors.phone_number_invalid';
                break;
            case DriverCreateCodeOptions.MobilePhoneNumberAlreadyExists:
                message = 'drivers.errors.phone_number_exists';
                break;
            case DriverCreateCodeOptions.PinAlreadyExists:
                message = 'drivers.errors.pin_exists';
                break;
            case DriverCreateCodeOptions.OwnIDAlreadyExists:
                message = 'drivers.errors.id_exists';
                break;
            case DriverCreateCodeOptions.NotFound:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUpdateResult(code: DriverUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case DriverUpdateCodeOptions.FullNameAlreadyExists:
                message = 'drivers.errors.fullname_exists';
                break;
            case DriverUpdateCodeOptions.EmailAddressAlreadyExists:
                message = 'drivers.errors.email_exists';
                break;
            case DriverUpdateCodeOptions.EmailAddressInvalid:
                message = 'form.errors.email_invalid';
                break;
            case DriverUpdateCodeOptions.MobilePhoneNumberInvalid:
                message = 'form.errors.phone_number_invalid';
                break;
            case DriverUpdateCodeOptions.MobilePhoneNumberAlreadyExists:
                message = 'drivers.errors.phone_number_exists';
                break;
            case DriverUpdateCodeOptions.PinAlreadyExists:
                message = 'drivers.errors.pin_exists';
                break;
            case DriverUpdateCodeOptions.OwnIDAlreadyExists:
                message = 'drivers.errors.id_exists';
                break;
            case DriverUpdateCodeOptions.NotFound:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
