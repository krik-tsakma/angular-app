﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../../shared/shared.module';

// COMPONENTS
import { EditDriverSettingsComponent } from './edit-driver-settings.component';


@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        EditDriverSettingsComponent
    ],
    exports: [
        EditDriverSettingsComponent
    ]
})
export class EditDriverSettingsModule { }
