// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { 
    DriversResult, 
    DriverSettings, 
    DriverUpdateCodeOptions, 
    DriverGroupMembershipUpdateRequest, 
    DriverGroupMembershipCodeOptions, 
    DriverCreateCodeOptions,
    DriversRequest, 
} from './drivers.types';
import { KeyName } from '../../common/key-name';
import { 
    DriverBlockStatusResult, 
    DriverBlockAccountRequest, 
    DriverBlockAccountCodeOptions,
    DriverRemoveAccountCodeOptions
 } from './edit/account/block-remove/block-remove-account.types';
import { 
    DriverAccountSettingsRequest, 
    DriverAccountSettingsCodeOptions, 
    DriverAccountSettingsResult, 
    DriverAccountCodeSendOptions
} from './edit/account/account-creation/edit-account-creation.types';
import { 
    DriverRoleAccountCreationUpdateCodeOptions, 
    DriverRoleUpdateRequest, 
    DriverRoleUpdateCodeOptions 
} from './edit/account/role/edit-driver-role.types';

@Injectable()
export class DriversService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/DriversAdmin';
        }

    // Get all company user roles
    public get(request: DriversRequest): Observable<DriversResult> {

        const params = new HttpParams({
            fromObject: {
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Sorting: request.sorting,
                GroupID: request.groupID ? request.groupID.toString() : ''
            }
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }


    // =========================
    // BASIC
    // =========================

    
    public getByID(id: number): Observable<DriverSettings> {
        return this.authHttp
            .get(this.baseURL + '/' + id);
    }

    public create(entity: DriverSettings): Observable<DriverCreateCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as DriverCreateCodeOptions;
                    }
                })
            );
    }


    public update(entity: DriverSettings): Observable<DriverUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverUpdateCodeOptions;
                    }
                })
            );            
    }


    // Delete driver 
    public delete(id: number): Observable<number> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((res) => res.status)
            ); 
    }


    // =========================
    // GROUP MEMBERSHIP
    // =========================

    public getGroupMembershipByID(id: number): Observable<number[]> {
        return this.authHttp
            .get(this.baseURL + '/GetGroupMembership/' + id);
    }

    
    public updateGroupMembership(entity: DriverGroupMembershipUpdateRequest): Observable<DriverGroupMembershipCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateGroupMembership/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverGroupMembershipCodeOptions;
                    }
                })
            );            
    }

    // =========================
    // USER ACCOUNT
    // =========================

    public getAccountSettingsByID(id: number): Observable<DriverAccountSettingsResult> {
        return this.authHttp
            .get(this.baseURL + '/GetAccountSettings/' + id);
    }

    public updateAccountSettings(entity: DriverAccountSettingsRequest): Observable<DriverAccountSettingsCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateAccountSettings/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverAccountSettingsCodeOptions;
                    }
                })
            );            
    }

    public sendAccountCreationCode(id: number): Observable<DriverAccountCodeSendOptions> {
        return this.authHttp
            .get(this.baseURL + '/SendAccountCreationCode/' + id, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverAccountCodeSendOptions;
                    }
                })
            );
    }

    // =========================
    // BLOCK/REMOVE USER ACCOUNT
    // =========================

    public getAccountBlockSettingsByID(id: number): Observable<DriverBlockStatusResult> {
        return this.authHttp
            .get(this.baseURL + '/GetAccountBlockSettings/' + id);
    }

    // block user account
    public blockAccount(entity: DriverBlockAccountRequest): Observable<DriverBlockAccountCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/BlockAccount/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverBlockAccountCodeOptions;
                    }
                })
            );            
    }

      // remove user account
      public removeAccount(id: number): Observable<DriverRemoveAccountCodeOptions>  {
        return this.authHttp
            .delete(this.baseURL + '/RemoveAccount/' + id, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverRemoveAccountCodeOptions;
                    }
                })
            );            
    }

    // =========================
    // DRIVER ROLE
    // =========================

    public getDriverRolesList(): Observable<KeyName[]> {
        return this.authHttp
            .get(this.baseURL + '/GetDriverRoles/');
    }

    public getRole(id: number): Observable<number> {
        return this.authHttp
            .get(this.baseURL + '/GetDriverRole/' + id);
    }

    public setRole(entity: DriverRoleUpdateRequest): Observable<DriverRoleUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateDriverRole', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverRoleUpdateCodeOptions;
                    }
                })
            );            
    }

    public getAccountCreationRole(id: number): Observable<number> {
        return this.authHttp
            .get(this.baseURL + '/GetAccountCreationDriverRole/' + id);
    }

    public setAccountCreationRole(entity: DriverRoleUpdateRequest): Observable<DriverRoleAccountCreationUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateAccountCreationDriverRole', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as DriverRoleAccountCreationUpdateCodeOptions;
                    }
                })
            );            
    }
} 

