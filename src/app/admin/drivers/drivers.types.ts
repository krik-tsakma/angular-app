import { KeyName } from '../../common/key-name';
import { PagerResults, SearchTermRequest } from '../../common/pager';

export const DriverSessionName: string = 'dn';

export interface DriversRequest extends SearchTermRequest {
    groupID?: number;
}

export interface DriversResult extends PagerResults {
    results: DriverResultItem[];
}

export interface DriverResultItem extends KeyName {
    ownID: string;
    groups: string;
    email: string;
    userAccountBlocked: boolean;
}
// ====================
// BASIC INFO
// ====================

export interface DriverSettings {
    id: number;
    ownID: string;
    firstname: string;
    lastname: string;
    description: string;
    email: string;
    phone: string;
    pin: string;
    pinType: PINTypes;
    useAlias: boolean;
    alias: string;
}

export enum PINTypes {
    iButton = 0,
    MYFARE_UID = 1,
    DTCO_CARD = 2
}

export enum DriverCreateCodeOptions {
    Created = 0,
    NotFound = 1,
    FullNameAlreadyExists = 2,
    OwnIDAlreadyExists = 3,
    PinAlreadyExists = 4,
    EmailAddressInvalid = 5,
    MobilePhoneNumberInvalid = 6,
    EmailAddressAlreadyExists = 7,
    MobilePhoneNumberAlreadyExists = 8,
    UnknownError = 20,
}

export enum DriverUpdateCodeOptions {
    Updated = 0,
    NotFound = 1,
    FullNameAlreadyExists = 2,
    OwnIDAlreadyExists = 3,
    PinAlreadyExists = 4,
    EmailAddressInvalid = 5,
    MobilePhoneNumberInvalid = 6,
    EmailAddressAlreadyExists = 7,
    MobilePhoneNumberAlreadyExists = 8,
    UnknownError = 20,
}

// ====================
// GROUP MEMBERSHIP
// ====================

export interface DriverGroupMembershipUpdateRequest {
    id: number;
    groupIDs: number[];
}

export enum DriverGroupAddMemberCodeOptions {
    Updated = 0,
    NoSuchHRExists = 1,
    UnknownError = 2
}

// ====================
// USER ACCESS
// ====================

export interface DriverGroupMembershipUpdateRequest {
    id: number;
    groupIDs: number[];
}

export enum DriverGroupMembershipCodeOptions {
    Updated = 0,
    NoSuchHRExists = 1,
    IssuerNotFound = 2,
    NotAllowedHr = 3,
    UnknownError = 20
}
