// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';
import { DriversService } from './drivers.service';

// COMPONENTS
import { ViewDriversComponent } from './view/view-drivers.component';
import { EditDriverSettingsComponent } from './edit/settings/edit-driver-settings.component';

const DriversRoutes: Routes = [
    {
        path: '',
        component: ViewDriversComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditDriverSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-driver.module').then((m) => m.EditDriverModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }   
];

@NgModule({
    imports: [
        RouterModule.forChild(DriversRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        DriversService
    ]
})
export class DriversRoutingModule { }
