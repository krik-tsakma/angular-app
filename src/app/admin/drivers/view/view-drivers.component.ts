// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { StoreManagerService } from '../../../shared/store-manager/store-manager.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { DriversService } from '../drivers.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { AdminLabelService } from '../../admin-label.service';
import { AssetLinksWarningService } from '../../../common/asset-link-warning/asset-link-warning.service';
import { Config } from '../../../config/config';

// TYPES
import { Pager } from '../../../common/pager';
import { StoreItems } from '../../../shared/store-manager/store-items';
import { DriversResult, DriverResultItem, DriverSessionName, DriversRequest } from '../drivers.types';
import {
    TableColumnSetting, TableActionItem, TableActionOptions,
    TableCellFormatOptions, TableColumnSortOrderOptions, TableColumnSortOptions
} from '../../../shared/data-table/data-table.types';
import { ConfirmationDialogParams } from '../../../shared/confirm-dialog/confirm-dialog-types';
import { AssetGroupItem, AssetGroupTypes } from '../../asset-groups/asset-groups.types';
import { EntityTypes } from '../../../shared/entity-types/entity-types';
import { AssetLinkWarningMessage } from '../../../common/asset-link-warning/asset-link-warning-types';
import { ImportExportTypeOptions } from './../../import-export/import-export.types';
import {
    MoreOptionsMenuOptions,
    MoreOptionsMenuTypes,
    MoreOptionsMenuOption
} from './../../../shared/more-options-menu/more-options-menu.types';
import { ImportExportDialogService } from '../../import-export/import-export-dialog.service';

@Component({
    selector: 'view-drivers',
    templateUrl: 'view-drivers.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['view-drivers.less'],
    providers: [AssetLinksWarningService]
})

export class ViewDriversComponent extends AdminBaseComponent implements OnInit {
    public drivers: DriverResultItem[] = [];
    public searchTermControl = new FormControl();
    public request: DriversRequest = {} as DriversRequest;
    public pager: Pager = new Pager();
    public loading: boolean;
    public tableSettings: TableColumnSetting[];
    public pageOptions: MoreOptionsMenuOptions[];

    // asset filters related
    public groupTypes = AssetGroupTypes;
    public entityTypes: any = EntityTypes;
    public entityTypesData: EntityTypes[] = [
        EntityTypes.Drivers
    ];
    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBarService: SnackBarService,
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,
        protected storeManager: StoreManagerService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private service: DriversService,
        private assetLinkWarningService: AssetLinksWarningService,
        private importExportDialog: ImportExportDialogService
    ) {
        super(
            translator,
            previousRouteService,
            router,
            unsubscriber,
            labelService,
            configService,
            snackBarService,
            storeManager,
            confirmDialogService
        );
        labelService.setSessionItemName(DriverSessionName);

        this.tableSettings =  this.getTableSettings();
        this.pageOptions = this.getPageMenuOptions();
    }


    public ngOnInit(): void {
        super.ngOnInit();

        const savedData = this.storeManager.getOption(StoreItems.drivers) as DriversRequest;
        if (savedData) {
            this.request = savedData;
            this.searchTermControl.setValue(this.request.searchTerm);
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchTermControl.valueChanges
            .pipe(
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    this.request.searchTerm = term;
                    this.get(true);
                    return of([]);
                })
            )
            .subscribe();

        this.get(true);
    }

    // ===================
    // EVENT HANDLERS
    // ===================

    public onSelectPageOption(item: MoreOptionsMenuOption): void {
        if (item.id === 'add') {
            this.manage();
        } else {
            this.importExportDialog
                .openDialog({
                    title: item.term,
                    type: item.id
                });
        }
    }

    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.searchTermControl.setValue(null);
    }

    // =========================
    // DRIVER GROUP CALLBACKS
    // =========================

    // When the user selects a driver group from the DDL
    public onChangeGroup(newGroup?: AssetGroupItem): void {
        this.request.groupID = newGroup
            ? newGroup.id
            : null;
        this.get(true);
    }

    // =========================
    // TABLE CALLBACKS
    // =========================

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const record = item.record as DriverResultItem;
        switch (item.action) {
            case TableActionOptions.edit:
                this.manage(record.id, item.record.name);
                break;
            case TableActionOptions.delete:
                this.getAssetLink(record.id).then((linkDescription: string) => {
                    const confMsg = this.translator.instant('form.warnings.are_you_sure');
                    let req = {
                        title: 'form.actions.delete',
                        message: linkDescription + ' <br/><br/>' + confMsg,
                        submitButtonTitle: 'form.actions.delete'
                    } as ConfirmationDialogParams;
                    if (linkDescription == null) {
                        req = null;
                    }
                    this.openDialog(req).subscribe((response) => {
                        if (response === true) {
                            this.delete(record.id);
                        }
                    });
                });
                break;
            default:
                break;
        }
    }


    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }

    public showUDButtons(action: TableActionOptions, record: DriverResultItem): boolean {
        if (!record) {
            return false;
        }
        return true;
    }

    // =========================
    // PRIVATE
    // =========================

    // Fetch events data
    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.drivers = new Array<DriverResultItem>();
        }

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        // first stop currently executing requests
        this.unsubscribeService();

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.drivers, this.request);

        // then start the new request
        this.serviceSubscriber = this.service
            .get(this.request)
            .subscribe(
                (response: DriversResult) => {
                    this.loading = false;

                    this.drivers = this.drivers.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;

                    if (!this.drivers || this.drivers.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                }
            );
    }


    // =========================
    // DELETE
    // =========================

    private delete(id: number) {
        this.loading = true;

        // first stop currently executing requests
        this.unsubscribeService();

        this.serviceSubscriber = this.service
            .delete(id)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 200) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get(true);
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private getAssetLink(id: number): Promise<string> {
        return new Promise((resolve, reject) => {
            this.assetLinkWarningService
                .GetForVehicle(id)
                .then((res: AssetLinkWarningMessage) => {
                    resolve(res.message);
                });
        });
    }

    // =================
    // HELPERS
    // =================

    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                primaryKey: 'name',
                header: 'drivers.fullname',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null),
                customClass: (record: DriverResultItem) => {
                    return record.userAccountBlocked ? 'blocked-account' : '';
                }
            },
            {
                primaryKey: 'ownID',
                header: 'drivers.ID',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null),
                customClass: (record: DriverResultItem) => {
                    return record.userAccountBlocked ? 'blocked-account' : '';
                }
            },
            {
                primaryKey: 'groups',
                header: 'assets.groups',
                customClass: (record: DriverResultItem) => {
                    return record.userAccountBlocked ? 'blocked-account' : '';
                }
            },
            {
                primaryKey: 'email',
                header: 'drivers.email',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null),
            },
            {
                primaryKey: 'userAccountBlocked',
                header: 'drivers.account.user.blocked',
                format: TableCellFormatOptions.custom,
                formatFunction: (record: DriverResultItem) => {
                    if (record.userAccountBlocked === true) {
                        return `<div class="blocked-account-icon-wrapper"><div class="blocked-account-icon"></div></div>`;
                    } else {
                        return ' ';
                    }
                }
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete],
                actionsVisibilityFunction: this.showUDButtons
            }
        ];
    }

    private getPageMenuOptions(): MoreOptionsMenuOptions[] {
        return [
            {
                term: 'assets.driver',
                options: [
                    {
                        id: 'add',
                        term: 'form.actions.create',
                        type: MoreOptionsMenuTypes.none,
                        enabled: true
                    }
                ]
            },
            {
                term: 'import_export.title',
                options: [
                    {
                        id: ImportExportTypeOptions.HumanResources,
                        term: 'assets.drivers',
                        type: MoreOptionsMenuTypes.none,
                        enabled: true
                    }
                ]
            }
        ];
    }
}
