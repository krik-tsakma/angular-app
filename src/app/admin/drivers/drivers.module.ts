
// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { DriversRoutingModule } from './drivers-routing.module';
import { EditDriverSettingsModule } from './edit/settings/edit-driver-settings.module';
import { SettingsListModule } from '../settings-list/settings-list.module';
import { AssetGroupsModule } from '../../common/asset-groups/asset-groups.module';
import { ImportExportModule } from '../import-export/import-export.module';

// COMPONENTS
import { ViewDriversComponent } from './view/view-drivers.component';

// SERVICES
import { DriversService } from './drivers.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/drivers/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        DriversRoutingModule,
        EditDriverSettingsModule,
        SettingsListModule,
        AssetGroupsModule,
        ImportExportModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewDriversComponent
    ],
    providers: [
        DriversService
    ]
})
export class DriversModule { }
