export interface SettingList {
    title: string;
    items: SettingListItem[];
    visible?: boolean;
}

export interface SettingListItem {
    option: string;
    params: string[];
    icon?: string;
    description?: string;
    allowed?: boolean;
}
