
// FRAMEWORK
import {
    Component,
    ViewEncapsulation,
    Input,
    OnChanges,
    SimpleChanges
} from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { Config } from '../../config/config';
import { CustomTranslateService } from './../../shared/custom-translate.service';

// TYPES
import { SettingList } from './settings-list.types';

@Component({
    selector: 'settings-list',
    templateUrl: 'settings-list.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['settings-list.less']
})

export class SettingsListComponent implements OnChanges {
    @Input() public options: SettingList[];
    public skipLocChange = !this.configService.get('router');

    constructor(
        private translate: CustomTranslateService,
        private router: Router,
        private configService: Config) {
            // FOO
        }

    public ngOnChanges(changes: SimpleChanges) {
        const changeType = changes['options'];
        if (!changeType) {
            return;
        }

        changeType.currentValue.forEach((x) => {
            x.visible = false;
            if (x.items.find((y) => (y.allowed === undefined || y.allowed === true))) {
                x.visible = true;
            }
        });
    }

    public chooseOption(params: string[]): void {
        this.router.navigate(params, { skipLocationChange: this.skipLocChange });
    }
}
