// FRAMEWORK
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';

// COMPONENTS
import { SettingsListComponent } from './settings-list.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MatListModule,
        MatIconModule,
        TranslateModule.forChild({
            isolate: false
        }),
    ],
    declarations: [
        SettingsListComponent
    ],
    exports: [
        SettingsListComponent
    ]
})
export class SettingsListModule { }
