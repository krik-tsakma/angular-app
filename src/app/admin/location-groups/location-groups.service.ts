// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RxJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { 
    LocationGroupsResult, 
    LocationGroupSettings, 
    LocationGroupCreateCodeOptions, 
    LocationGroupSettingsCodeOptions, 
    LocationGroupAddMemberCodeOptions, 
    LocationGroupRemoveMemberCodeOptions, 
    LocationGroupAuthorizeUserCodeOptions, 
    LocationGroupUnauthorizeUserCodeOptions 
} from './location-groups.types';
import { SearchTermRequest } from '../../common/pager';
import { AssetGroupMembersRequest, AssetGroupMembersResult, AssetGroupMemberAddRemoveRequest } from '../asset-groups/asset-groups.types';

@Injectable()
export class LocationGroupsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/LocationGroups';
        }

        // Get all location groups
        public get(request: SearchTermRequest): Observable<LocationGroupsResult> {
        
            const params = new HttpParams({
                fromObject: {
                    SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                    PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                    PageSize: request.pageSize ? request.pageSize.toString() : '20',
                    Sorting: request.sorting
                }
            });

            return this.authHttp
                .get(this.baseURL, { params });
        }


        public getByID(id: number): Observable<LocationGroupSettings> {
            return this.authHttp
                .get(this.baseURL + '/' + id);
        }


        public create(entity: LocationGroupSettings): Observable<LocationGroupCreateCodeOptions>  {
            return this.authHttp
                .post(this.baseURL, entity, { observe: 'response' })
                .pipe(
                    map((res) => {
                        if (res.status === 201) {
                            return null;
                        } else  {
                            return res.body as LocationGroupCreateCodeOptions;
                        }
                    })
                );
        }


        public update(entity: LocationGroupSettings): Observable<LocationGroupSettingsCodeOptions>  {
            return this.authHttp
                .put(this.baseURL, entity, { observe: 'response' })
                .pipe(
                    map((res) => {
                        // this is means empty Ok response from the server
                        if (res.status === 200 && !res.body) {
                            return null;
                        } else  {
                            return res.body as LocationGroupSettingsCodeOptions;
                        }
                    })
                );            
        }

        public delete(id: number): Observable<number> {
            return this.authHttp
                .delete(this.baseURL + '/' + id, { observe: 'response' })
                .pipe(
                     map((r) => r.status as number)
                ); 
        }

    // ====================
    // MEMBERS
    // ====================

    public getMembers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20'
            }
        });

        return this.authHttp.get(this.baseURL + '/GetMembers', { params });
    }

    public getNonMembers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20'
            }
        });

        return this.authHttp.get(this.baseURL + '/GetNonMembers', { params });
    }


    public addMember(entity: AssetGroupMemberAddRemoveRequest): Observable<LocationGroupAddMemberCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/AddMember', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as LocationGroupAddMemberCodeOptions;
                    }
                })
            );            
    }

    public removeMember(entity: AssetGroupMemberAddRemoveRequest): Observable<LocationGroupRemoveMemberCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/RemoveMember', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as LocationGroupRemoveMemberCodeOptions;
                    }
                })
            );            
    }


    // ====================
    // AUTHORIZED USERS
    // ====================

    public getAuthorizedUsers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20'
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetAuthorizedUsers', { params });
    }


    public authorizeUser(entity: AssetGroupMemberAddRemoveRequest): Observable<LocationGroupAuthorizeUserCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/AuthorizeUser', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as LocationGroupAuthorizeUserCodeOptions;
                    }
                })
            );            
    }

    public getUnauthorizedUsers(request: AssetGroupMembersRequest): Observable<AssetGroupMembersResult> {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting,
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetUnauthorizedUsers', { params });
    }


    public unauthorizeUser(entity: AssetGroupMemberAddRemoveRequest): Observable<LocationGroupUnauthorizeUserCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/UnauthorizeUser', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as LocationGroupUnauthorizeUserCodeOptions;
                    }
                })
            );            
    }
} 
