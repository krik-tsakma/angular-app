// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { SettingsListModule } from '../settings-list/settings-list.module';
import { EditLocationGroupSettingsModule } from './edit/settings/edit-location-group-settings.module';
import { LocationGroupsRoutingModule } from './location-groups-routing.module';

// SERVICES
import { AdminLabelService } from '../admin-label.service';
import { LocationGroupsService } from './location-groups.service';

// COMPONENTS
import { ViewLocationGroupsComponent } from './view/view-location-groups.component';

@NgModule({
    imports: [
        SharedModule,
        LocationGroupsRoutingModule,
        EditLocationGroupSettingsModule,
        SettingsListModule,
    ],
    declarations: [
        ViewLocationGroupsComponent
    ],      
    providers: [
        LocationGroupsService
    ]
})
export class LocationGroupsModule { }
