﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../../shared/shared.module';

// COMPONENTS
import { EditLocationGroupSettingsComponent } from './edit-location-group-settings.component';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        EditLocationGroupSettingsComponent
    ],
    exports: [
        EditLocationGroupSettingsComponent
    ]
})

export class EditLocationGroupSettingsModule { }
