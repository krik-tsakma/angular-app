// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { AdminLabelService } from '../../admin-label.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES 
import { SettingList } from '../../settings-list/settings-list.types';
import { LocationGroupSessionName } from '../location-groups.types';

@Component({
    selector: 'edit-location-group',
    templateUrl: 'edit-location-group.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditLocationGroupComponent {
    public options: SettingList[] = [
        {
            title: 'assets.group.category.basic',
            items: [
                {
                    option: 'assets.group.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
                {
                    option: 'assets.group.category.basic.membership',
                    params: ['members'],
                    icon: 'place'
                },
                {
                    option: 'assets.group.category.basic.access',
                    params: ['authorized-users'],
                    icon: 'person_outline'
                }
            ]
        }
    ];

    constructor(public labelService: AdminLabelService,
                private configService: Config,
                private previousRouteService: PreviousRouteService,
                private router: Router) {
            labelService.setSessionItemName(LocationGroupSessionName);
    }

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
