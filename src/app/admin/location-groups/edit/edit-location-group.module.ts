
// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';
import { EditLocationGroupRoutingModule } from './edit-location-group-routing.module';
import { EditLocationGroupSettingsModule } from './settings/edit-location-group-settings.module';
import { AssetGroupsModule } from '../../../common/asset-groups/asset-groups.module';

// COMPONENTS
import { EditLocationGroupComponent } from './edit-location-group.component';
import { EditLocationGroupMembersComponent } from './members/edit-location-group-members.component';
import { EditLocationGroupAuthorizedUsersComponent } from './authorized-users/edit-location-group-authorized-users.component';

// SERVICES
import { LocationGroupsService } from '../location-groups.service';

@NgModule({
    imports: [
        SharedModule,
        EditLocationGroupRoutingModule,
        EditLocationGroupSettingsModule,
        SettingsListModule,
        AssetGroupsModule,
    ],
    declarations: [
        EditLocationGroupComponent,
        EditLocationGroupMembersComponent,
        EditLocationGroupAuthorizedUsersComponent
    ],
    providers: [
        LocationGroupsService
    ]
})
export class EditLocationGroupModule { }
