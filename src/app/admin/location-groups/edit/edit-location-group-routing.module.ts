﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';

// COMPONENTS
import { EditLocationGroupComponent } from './edit-location-group.component';
import { EditLocationGroupSettingsComponent } from './settings/edit-location-group-settings.component';
import { EditLocationGroupMembersComponent } from './members/edit-location-group-members.component';
import { EditLocationGroupAuthorizedUsersComponent } from './authorized-users/edit-location-group-authorized-users.component';

const EditLocationGroupRoutes: Routes = [
    {
        path: '',
        component: EditLocationGroupComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'settings',
        component: EditLocationGroupSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.settings'
        }
    },
    {
        path: 'members',
        component: EditLocationGroupMembersComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.membership'
        }
    },
    {
        path: 'authorized-users',
        component: EditLocationGroupAuthorizedUsersComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.access'
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(EditLocationGroupRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class EditLocationGroupRoutingModule { }
