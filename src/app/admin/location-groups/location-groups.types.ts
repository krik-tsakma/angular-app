import { KeyName } from './../../common/key-name';
import { PagerResults } from '../../common/pager';

export const LocationGroupSessionName: string = 'lgn';

export interface LocationGroupsResult extends PagerResults {
    results: LocationGroupResultItem[];
}

export interface LocationGroupResultItem extends KeyName {
    isDefault?: boolean;
    isUnlinked?: boolean;
}

// ====================
// BASIC INFO
// ====================

export interface LocationGroupSettings {
    id: number;
    name: string;
    description: string;
}

export enum LocationGroupCreateCodeOptions {
    Created = 0,
    NameAlreadyExists = 1,
    IssuerNotFound = 2,
    UnknownError = 10,
}

export enum LocationGroupSettingsCodeOptions {
    Set = 0,
    NoSuchGroupExists = 1,
    NameAlreadyExists = 2,
    IssuerNotFound = 3,
    NotAllowedLocationGroup = 4,
    UnknownError = 10,
}

// ====================
// MEMBERS
// ===================

export enum LocationGroupAddMemberCodeOptions {
    Added = 0,
    LocationGroupNotFound = 1,
    LocationGroupIsDefault = 2,
    LocationNotFound = 3,
    LocationAlreadyAMember = 4,
    IssuerNotFound = 5,
    NotAllowedLocation = 6,
    NotAllowedLocationGroup = 7,
    UnknownError = 20
}


export enum LocationGroupRemoveMemberCodeOptions {
    Removed = 0,
    LocationGroupNotFound = 1,
    LocationNotAMember = 2,
    IssuerNotFound = 3,
    NotAllowedLocation = 4,
    NotAllowedLocationGroup = 5,
    UnknownError = 10
}

// ====================
// AUTHORIZED USERS
// ===================

export enum LocationGroupAuthorizeUserCodeOptions {
    Added = 0,
    LocationGroupNotFound = 1,
    UserNotFound = 2,
    UserAuthorized = 3,
    IssuerNotFound = 4,
    NotAllowedLocation = 5,
    NotAllowedLocationGroup = 6,
    UnknownError = 10
}

export enum LocationGroupUnauthorizeUserCodeOptions {
    Removed = 0,
    LocationGroupNotFound = 1,
    UserNotFound = 2,
    UserNotAuthorized = 3,
    IssuerNotFound = 4,
    NotAllowedLocationGroup = 5,
    UnknownError = 9
}
