
// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { EditLocationGroupSettingsComponent } from './edit/settings/edit-location-group-settings.component';
import { ViewLocationGroupsComponent } from './view/view-location-groups.component';

const LocationGroupsRoutes: Routes = [
    {
        path: '',
        component: ViewLocationGroupsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditLocationGroupSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-location-group.module').then((m) => m.EditLocationGroupModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }   
];

@NgModule({
    imports: [
        RouterModule.forChild(LocationGroupsRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class LocationGroupsRoutingModule { }
