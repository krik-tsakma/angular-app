// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';

// COMPONENTS
import { AdminCoreComponent } from './admin-core/admin-core.component';

// SERVICES
import { AdminLabelService } from './admin-label.service';
import { TariffsService } from './book-n-go/tariffs/tariffs.service';
import { FairUsePoliciesService } from './book-n-go/fair-use-policies/fair-use-policies.service';
import { TranslateStateService } from '../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../multiTranslateHttpLoader';

export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}


@NgModule({
    imports: [
        SharedModule,
        AdminRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        AdminCoreComponent
    ],
    providers: [
        AdminLabelService,
        TariffsService,
        FairUsePoliciesService
    ]
})

export class AdminModule { }
