// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../auth/authService';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { AdminOptionsCategories } from '../admin-types';
import { Config } from '../../config/config';

// TYPES
import { AdminPropertyOptions, ModuleOptions } from '../../auth/acl.types';

@Component({
    selector: 'admin-core',
    templateUrl: './admin-core.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./admin-core.less']
})

export class AdminCoreComponent {
    public skipLocChange = !this.configService.get('router');
    public adminOptions: AdminOptionsCategories[] = [
        {
            categoryTitle: 'admin.categories.company',
            visible: false,
            categoryOptions: [
                {
                    option: 'admin.categories.company.manage',
                    params: ['company'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Company)
                }
            ]
        },
        {
            categoryTitle: 'admin.categories.scores',
            visible: false,
            categoryOptions: [
                {
                    option: 'admin.categories.scores.energy_score',
                    params: ['energy-score'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Scores)
                },
                {
                    option: 'admin.categories.scores.score_definition',
                    params: ['score-definition'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Scores)
                }
            ]
        },
        {
            categoryTitle: 'admin.categories.users',
            visible: false,
            categoryOptions: [
                {
                    option: 'admin.categories.users.manage',
                    params: ['users'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Users)

                },
                {
                    option: 'admin.categories.users.roles',
                    params: ['user-roles'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Users)

                },
                {
                    option: 'admin.categories.users.groups',
                    params: ['groups', 'users'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Users)
                }
            ]
        },
        {
            categoryTitle: 'admin.categories.vehicles',
            visible: false,
            categoryOptions: [
                {
                    option: 'admin.categories.vehicles.manage',
                    params: ['vehicles'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Vehicles)
                },
                {
                    option: 'admin.categories.vehicles.groups',
                    params: ['groups', 'vehicles'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Vehicles)
                },
                {
                    option: 'admin.categories.vehicles.types',
                    params: ['vehicle-types'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Vehicles)
                },
            ]
        },
        {
            categoryTitle: 'admin.categories.drivers',
            visible: false,
            categoryOptions: [
                {
                    option: 'admin.categories.drivers.manage',
                    params: ['drivers'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Drivers)
                },
                {
                    option: 'admin.categories.drivers.groups',
                    params: ['groups', 'drivers'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Drivers)
                }
            ]
        },
        {
            categoryTitle: 'admin.categories.locations',
            visible: false,
            categoryOptions: [
                // {
                //     option: 'admin.categories.locations.manage',
                //     params: ['locations'],
                //     accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Locations)
                // },
                {
                    option: 'admin.categories.locations.groups',
                    params: ['groups', 'locations'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.Locations)
                }
            ]
        },
        {
            categoryTitle: 'admin.categories.devices',
            visible: false,
            categoryOptions: [
                {
                    option: 'admin.categories.devices.links',
                    params: ['asset-links'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.AssetLinks)
                }
            ]
        },
        {
            categoryTitle: 'admin.categories.book_n_go',
            visible: false,
            categoryOptions: [
                {
                    option: 'admin.categories.book_n_go.fair_use_policies',
                    params: ['fair-use-policies'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.BookNGo)
                },
                {
                    option: 'admin.categories.book_n_go.tariffs',
                    params: ['tariffs'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.BookNGo)
                }
            ]
        },
        {
            categoryTitle: 'admin.categories.charge_points',
            visible: false,
            categoryOptions: [
                {
                    option: 'admin.categories.charge_points.manage',
                    params: ['charge-points'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.ChargePoints)
                },
                {
                    option: 'admin.categories.charge_points.groups',
                    params: ['groups', 'charge-points'],
                    accessible: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.ChargePoints)
                }
            ]
        }
    ];
    constructor(
        private router: Router,
        private previousRouteService: PreviousRouteService,
        private configService: Config,
        public translate: TranslateService,
        public userOptions: UserOptionsService,
        public authService: AuthService) {
            this.adminOptions.forEach((x) => {
                if (x.categoryOptions.find((y) => y.accessible === true)) {
                    x.visible = true;
                }
            });
        }
}
