﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { EditUserRoutingModule } from './edit-user-routing.module';
import { EditUserSettingsModule } from './settings/edit-user-settings.module';
import { AssetGroupMembershipModule } from '../../../common/asset-groups-membership/asset-group-membership.module';
import { AccessRightsModule } from '../../../common/access-rights/access-rights.module';
import { TimezonesListModule } from '../../../common/timezones-list/timezones-list.module';
import { UserRolesListModule } from '../../../common/user-roles-list/roles-list.module';

// COMPONENTS
import { EditUserComponent } from './edit-user.component';
import { EditUserRightsComponent } from './access-rights/edit-user-rights.component';
import { EditUserGroupMembershipComponent } from './group-membership/user-group-membership.component';
import { EditUserRoleComponent } from './role/edit-user-role.component';
import { SettingsListModule } from '../../settings-list/settings-list.module';

// SERVICES
import { UsersService } from '../users.service';
import { UserRolesService } from '../../user-roles/user-roles.service';

@NgModule({
    imports: [
        SharedModule,
        AssetGroupMembershipModule,
        EditUserRoutingModule,
        EditUserSettingsModule,
        AccessRightsModule,
        SettingsListModule,
        UserRolesListModule
    ],
    declarations: [
        EditUserComponent,
        EditUserGroupMembershipComponent,
        EditUserRightsComponent,
        EditUserRoleComponent
    ],
    providers: [
        UsersService,
        UserRolesService
    ]
})
export class EditUserModule { }
