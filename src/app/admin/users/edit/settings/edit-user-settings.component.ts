// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { UsersService } from '../../users.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import { UserAccount, UserCreateCodeOptions, UserUpdateCodeOptions, UsersSessionName } from '../../users.types';
import { MeasurementSystems, VehicleLabels, DriverLabels } from '../../../../common/entities/entity-types';
import { PasswordRulesFromOptions } from '../../../../shared/password-rules/password-rules-types';
import { UserDefaultCompanySettings } from './../../users.types';

@Component({
    selector: 'edit-user-settings',
    templateUrl: 'edit-user-settings.html',
    encapsulation: ViewEncapsulation.None
})

export class EditUserSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: false }) public editForm: NgForm;
    public action: string;
    public loading: boolean;
    public vehicleLabels = VehicleLabels;
    public driverLabels = DriverLabels;
    public measurementSystems = MeasurementSystems;    
    public user: UserAccount = new UserAccount();
    public passwordRulesOptions: any = PasswordRulesFromOptions;
    public isPasswordValid: boolean;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected router: Router,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,        
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: UsersService,
        
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(UsersSessionName);

        this.vehicleLabels.forEach((x) => {
            translator.get(x.term).subscribe((t) => {
                x.name = t;
            });
        });
        this.driverLabels.forEach((x) => {
            translator.get(x.term).subscribe((t) => {
                x.name = t;
            });
        });
        this.measurementSystems.forEach((x) => {
            translator.get(x.term).subscribe((t) => {
                x.name = t;
            });
        });
    }

    // =========================
    // LIFECYCLE
    // =========================

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                id: string;
            }) => { 
                this.action = params.id 
                            ? 'form.actions.edit'
                            : 'form.actions.create';

                if (params.id) {
                    this.get(Number(params.id));
                } else {
                    this.getDefaultCompanySettings()
                        .then((def: UserDefaultCompanySettings) => {
                            this.user.vehicleLabel = def.vehicleLabel;
                            this.user.driverLabel = def.driverLabel;
                            this.user.culture = def.culture;
                            this.user.timeZoneIndex = def.timezoneIndex;
                            this.user.measurementSystem = def.measurementSystem;
                        });
                }
            });
    }

    // =========================
    // EVENT HANDLERS
    // =========================

    public onPasswordValidation(isValid: boolean): void {
        this.isPasswordValid = isValid;
    }


    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: UserAccount, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is create
        if (typeof(this.user.id) === 'undefined' || this.user.id === 0) {
            this.serviceSubscriber = this.service
                .create(this.user)
                .subscribe((res: UserCreateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const message = this.getCreateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else { 
            // This is an update
            this.serviceSubscriber = this.service
                .update(this.user)
                .subscribe((res: UserUpdateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.labelService.set(`${this.user.firstname} ${this.user.lastname}`);
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const message = this.getUpdateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        }
    }

    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((res: UserAccount) => {
                this.loading = false;
                this.user = res;
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }


    private getDefaultCompanySettings(): Promise<UserDefaultCompanySettings> {
        return new Promise((resolve, reject) => {
            this.loading = true;
            this.unsubscribeService();
            this.serviceSubscriber = this.service
                .getDefaultCompanySettings()
                .subscribe((res: UserDefaultCompanySettings) => {
                    this.loading = false;
                    resolve(res);
                    
                },
                (err) => {
                    this.loading = false;
                    reject(null);
                }
            );
        });
    }


    // =========================
    // RESULTS
    // =========================

    private getCreateResult(code: UserCreateCodeOptions): string {
        let message = '';
        switch (code) {
            case UserCreateCodeOptions.UsernameAlreadyExists:
                message = 'users.errors.username_exists';
                break;
            case UserCreateCodeOptions.FirstLastNameCombinationExists:
                message = 'users.errors.fullname_exists';
                break;
            case UserCreateCodeOptions.EmailAddressAlreadyExists:
                message = 'users.errors.email_exists';
                break;
            case UserCreateCodeOptions.EmailAddressInvalid:
                message = 'form.errors.email_invalid';
                break;
            case UserCreateCodeOptions.MobilePhoneNumberInvalid:
                message = 'form.errors.phone_number_invalid';
                break;
            case UserCreateCodeOptions.SycadaSuperAdminCreationNotAllowed:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUpdateResult(code: UserUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case UserUpdateCodeOptions.UsernameAlreadyExists:
                message = 'users.errors.username_exists';
                break;
            case UserUpdateCodeOptions.FirstLastNameCombinationExists:
                message = 'users.errors.fullname_exists';
                break;
            case UserUpdateCodeOptions.EmailAddressAlreadyExists:
                message = 'users.errors.email_exists';
                break;
            case UserUpdateCodeOptions.EmailAddressInvalid:
                message = 'form.errors.email_invalid';
                break;
            case UserUpdateCodeOptions.MobilePhoneNumberInvalid:
                message = 'form.errors.phone_number_invalid';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
