﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../../shared/shared.module';
import { CulturesListModule } from '../../../../common/cultures-list/cultures-list.module';
import { TimezonesListModule } from '../../../../common/timezones-list/timezones-list.module';
import { UserRolesListModule } from '../../../../common/user-roles-list/roles-list.module';

// COMPONENTS
import { EditUserSettingsComponent } from './edit-user-settings.component';


@NgModule({
    imports: [
        SharedModule,
        UserRolesListModule,
        CulturesListModule,
        TimezonesListModule
    ],
    declarations: [
        EditUserSettingsComponent
    ],
    exports: [
        EditUserSettingsComponent
    ]
})
export class EditUserSettingsModule {}
