// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { UserRolesService } from '../../../user-roles/user-roles.service';
import { UsersService } from '../../users.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import { UserRolesResultItem } from '../../../user-roles/user-roles.types';
import { UserRoleUpdateRequest, UserRoleUpdateCodeOptions, UsersSessionName } from '../../users.types';

@Component({
    selector: 'edit-user-role',
    templateUrl: 'edit-user-role.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditUserRoleComponent extends AdminBaseComponent implements OnInit {
    public loading: boolean;
    public userRoles: UserRolesResultItem[];
    
    public user: UserRoleUpdateRequest = {} as UserRoleUpdateRequest;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,  
        protected labelService: AdminLabelService,
        protected router: Router,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: UsersService,
        private userRolesService: UserRolesService,
        
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(UsersSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.user.userID = params.id;
                    this.get(params.id);
                }
            });
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: UserRoleUpdateRequest, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is an update
        this.serviceSubscriber = this.service
            .updateRole(this.user)
            .subscribe((res?: UserRoleUpdateCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    const message = this.getChangeResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getRoleByID(id)
            .subscribe((res: number) => {
                this.loading = false;
                this.user.roleID = res;
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }

     // =========================
    // RESULTS
    // =========================

    private getChangeResult(code: UserRoleUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case UserRoleUpdateCodeOptions.UserNotFound:
                message = 'form.errors.not_found';
                break;
            case UserRoleUpdateCodeOptions.UserRoleNotFound:
                message = 'users.errors.role_not_found';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
