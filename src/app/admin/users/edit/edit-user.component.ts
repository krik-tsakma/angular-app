// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { AdminLabelService } from '../../admin-label.service';
import { AuthService } from '../../../auth/authService';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES 
import { SettingList } from '../../settings-list/settings-list.types';
import { UsersSessionName } from '../users.types';
import { ModuleOptions, AdminPropertyOptions } from '../../../auth/acl.types';

@Component({
    selector: 'edit-user',
    templateUrl: 'edit-user.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditUserComponent {
    public options: SettingList[] = [
        {
            title: 'users.category.basic',
            items: [
                {
                    option: 'users.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
                {
                    option: 'users.category.basic.role',
                    params: ['role'],
                    icon: 'assignment_ind'
                },
                {
                    option: 'users.category.basic.rights',
                    params: ['rights'],
                    icon: 'lock'
                }
            ]
        },
        {
            title: 'users.category.access',
            items: [
                {
                    option: 'assets.groups.vehicle',
                    params: ['groups', 'vehicle'],
                    icon: 'directions_car'
                },
                {
                    option: 'assets.groups.driver',
                    params: ['groups', 'driver'],
                    icon: 'person'
                },
                {
                    option: 'assets.groups.location',
                    params: ['groups', 'location'],
                    icon: 'place'
                },            
                {
                    option: 'assets.groups.charge_point',
                    params: ['groups', 'charge-point'],
                    icon: 'power',
                    allowed: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.ChargePoints)
                }
            ]
        }
    ];

    constructor(public labelService: AdminLabelService,
                private router: Router,
                private authService: AuthService,
                private previousRouteService: PreviousRouteService,
                private configService: Config) {
        labelService.setSessionItemName(UsersSessionName);
    }

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
