// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { UsersService } from '../../users.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import { AssetGroupTypes } from '../../../asset-groups/asset-groups.types';
import { 
    UserGroupMembershipUpdateRequest, 
    UserGroupMembershipResult,
    UserGroupMembershipCodeOptions, 
    UsersSessionName
} from '../../users.types';

@Component({
    selector: 'edit-user-role-group-membership',
    templateUrl: 'user-group-membership.html',
    encapsulation: ViewEncapsulation.None
})

export class EditUserGroupMembershipComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public groupTypes: any = AssetGroupTypes;
    public user: UserGroupMembershipUpdateRequest = {} as UserGroupMembershipUpdateRequest;
    public userRoleID: number;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: UsersService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar);
        labelService.setSessionItemName(UsersSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
                groupType: AssetGroupTypes;
            }) => { 
                if (params.id) {
                    this.activatedRoute
                    .data
                    .subscribe((data) => {
                        this.user.userID = params.id;
                        this.user.groupType = data.groupType;
                        this.get(params.id, data.groupType);
                    });
                }
            });
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: UserGroupMembershipUpdateRequest, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

     
        // This is an update
        this.serviceSubscriber = this.service
            .updateGroupMembership(this.user)
            .subscribe((res?: UserGroupMembershipCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    private get(id: number, groupType: AssetGroupTypes) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getGroupMembershipByID(id, groupType)
            .subscribe((res: UserGroupMembershipResult) => {
                this.loading = false;
                this.user.groupIDs = res.groupIDs;
                this.userRoleID = res.roleID;
            },
            (err) => {
                this.loading = false;
                this.message = 'data.error';
            }
        );
    }
}
