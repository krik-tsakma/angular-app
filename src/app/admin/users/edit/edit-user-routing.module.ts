﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';
import { UsersService } from '../users.service';

// COMPONENTS
import { EditUserComponent } from './edit-user.component';
import { EditUserSettingsComponent } from './settings/edit-user-settings.component';
import { EditUserGroupMembershipComponent } from './group-membership/user-group-membership.component';
import { EditUserRightsComponent } from './access-rights/edit-user-rights.component';
import { EditUserRoleComponent } from './role/edit-user-role.component';
import { AssetGroupTypes } from '../../asset-groups/asset-groups.types';

const EditUserRoutes: Routes = [
    {
        path: '',
        component: EditUserComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'settings',
        component: EditUserSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'users.category.basic.settings'
        }
    },
    {
        path: 'role',
        component: EditUserRoleComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'users.category.basic.role'
        }
    },
    {
        path: 'rights',
        component: EditUserRightsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'users.category.basic.rights'
        }
    },
    {
        path: 'groups/vehicle',
        component: EditUserGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.groups.vehicle',
            groupType: AssetGroupTypes.Vehicle,
        }
    },
    {
        path: 'groups/driver',
        component: EditUserGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.groups.driver',
            groupType: AssetGroupTypes.Driver,
        }
    },
    {
        path: 'groups/location',
        component: EditUserGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.groups.location',
            groupType: AssetGroupTypes.Location,
        }
    },
    {
        path: 'groups/charge-point',
        component: EditUserGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.groups.charge_point',
            groupType: AssetGroupTypes.ChargePoint
        }
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(EditUserRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        UsersService
    ]
})
export class EditUserRoutingModule { }
