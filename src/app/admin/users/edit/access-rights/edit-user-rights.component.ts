// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { UsersService } from '../../users.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import { UserAccessRightsRequest, UserAccessRightsUpdateCodeOptions, UserRightsResult, UsersSessionName } from '../../users.types';
import { Module } from '../../../../auth/acl.types';

@Component({
    selector: 'edit-user-rights',
    templateUrl: 'edit-user-rights.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditUserRightsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public action: string;
    public loading: boolean;
    public user: UserAccessRightsRequest = {} as UserAccessRightsRequest;
    public roleID: number;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: UsersService
        
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(UsersSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.user.userID = params.id;
                    this.get(params.id);
                }
            });
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: Module[], valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is an update
        this.serviceSubscriber = this.service
            .updateAccessRights(this.user)
            .subscribe((res: UserAccessRightsUpdateCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    const message = this.getUpdateResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getRightsByID(id)
            .subscribe((res: UserRightsResult) => {
                this.loading = false;
                this.user.rights = res.rights;
                this.roleID = res.roleID;
            },
            (err) => {
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }

    // =========================
    // RESULTS
    // =========================

    private getUpdateResult(code: UserAccessRightsUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case UserAccessRightsUpdateCodeOptions.AccessRightsExceedingThoseOfRole:
                message = 'users.errors.rights_exceeding_role';
                break;
            case UserAccessRightsUpdateCodeOptions.UserNotFound:
                message = 'form.errors.not_found';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
