// FRAMEWORK
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { StoreManagerService } from '../../../shared/store-manager/store-manager.service';
import { UsersService } from '../users.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { AdminLabelService } from '../../admin-label.service';
import { Config } from '../../../config/config';

// TYPES
import { Pager } from '../../../common/pager';
import { StoreItems } from '../../../shared/store-manager/store-items';
import { UserAccountResultItem, UsersRequest, UsersResult, UsersSessionName, UserRemoveCodeOptions } from '../users.types';
import { 
    TableColumnSetting, TableActionItem, TableActionOptions, 
    TableColumnSortOrderOptions, TableColumnSortOptions
} from '../../../shared/data-table/data-table.types';

@Component({
    selector: 'view-users',
    templateUrl: 'view-users.html'
})

export class ViewUsersComponent extends AdminBaseComponent implements OnInit {
    public users: UserAccountResultItem[] = [];
    public searchTermControl = new FormControl();     
    public request: UsersRequest = {} as UsersRequest;
    public pager: Pager = new Pager();
    public loading: boolean;
    public tableSettings: TableColumnSetting[];

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBarService: SnackBarService,
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,
        protected storeManager: StoreManagerService,
        protected configService: Config,
        private service: UsersService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBarService, storeManager, confirmDialogService);
        labelService.setSessionItemName(UsersSessionName);

        this.tableSettings =  [
            {
                primaryKey: 'name',
                header: 'users.fullname',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'role',
                header: 'users.role',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete]
            }
        ];
    }


    public ngOnInit(): void {
        super.ngOnInit();
       
        const savedData = this.storeManager.getOption(StoreItems.users) as UsersRequest;
        if (savedData) {
            this.request = savedData;
            this.searchTermControl.setValue(this.request.searchTerm);
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchTermControl.valueChanges 
            .pipe(           
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing             
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    this.request.searchTerm = term;
                    this.get(true);
                    return of([]);
                })
            )
            .subscribe(); 
        
        this.get(true);
    }



    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.searchTermControl.setValue(null);
    }

    // ----------------
    // TABLE CALLBACKS
    // ----------------

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.edit: 
                this.manage(id, item.record.name);
                break;
            case TableActionOptions.delete: 
                this.openDialog().subscribe((response) => {
                    if (response === true) {
                        this.delete(id);
                    }
                });
                break;
            default: 
                break;

        }
    }


    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }

    // Fetch events data
    public get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.users = new Array<UserAccountResultItem>();
        }   

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;
   
        // first stop currently executing requests
        this.unsubscribeService();

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.users, this.request);
            
        // then start the new request
        this.serviceSubscriber = this.service
            .get(this.request)
            .subscribe(
                (response: UsersResult) => {
                    this.loading = false;

                    this.users = this.users.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;                                          
                    
                    if (!this.users || this.users.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                () => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }


    // ----------------
    // DELETE
    // ----------------
    private delete(id: number) {
        this.loading = true;
        
        // first stop currently executing requests
        this.unsubscribeService();
        
        this.serviceSubscriber = this.service
            .delete(id)
            .subscribe((res: UserRemoveCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get(true);
                } else {
                    const message = this.getDeleteResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // =========================
    // RESULTS
    // =========================

    private getDeleteResult(code: UserRemoveCodeOptions): string {
        let message = '';
        switch (code) {
            case UserRemoveCodeOptions.NoSuchUserExists:
                message = 'form.errors.not_found';
                break;
            case UserRemoveCodeOptions.LastSycadaAdmin:
                message = 'users.errors.last_sycada_admin';
                break;
            case UserRemoveCodeOptions.UnknownError:
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
