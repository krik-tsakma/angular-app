// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { EditUserSettingsModule } from './edit/settings/edit-user-settings.module';
import { AssetGroupMembershipModule } from '../../common/asset-groups-membership/asset-group-membership.module';
import { AccessRightsModule } from '../../common/access-rights/access-rights.module';
import { CulturesListModule } from '../../common/cultures-list/cultures-list.module';
import { SettingsListModule } from '../settings-list/settings-list.module';
import { UserRolesListModule } from '../../common/user-roles-list/roles-list.module';

// COMPONENTS
import { ViewUsersComponent } from './view/view-users.component';

// SERVICES
import { UsersService } from './users.service';
import { UserRolesService } from '../user-roles/user-roles.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/users/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        AssetGroupMembershipModule,
        UsersRoutingModule,
        EditUserSettingsModule,
        AccessRightsModule,
        CulturesListModule,
        SettingsListModule,
        UserRolesListModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewUsersComponent
    ],
    providers: [
        UsersService,
        UserRolesService
    ]
})
export class UsersModule { }
