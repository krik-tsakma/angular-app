
// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { 
    UsersResult, 
    UserAccount, 
    UserGroupMembershipUpdateRequest, 
    UserGroupMembershipResult, 
    UserAccessRightsRequest, 
    UserRoleUpdateRequest, 
    UserRoleUpdateCodeOptions,
    UserAccessRightsUpdateCodeOptions,
    UserCreateCodeOptions,
    UserUpdateCodeOptions,
    UserGroupMembershipCodeOptions,
    UserRightsResult,
    UsersRequest,
    UserRemoveCodeOptions,
    UserDefaultCompanySettings
} from './users.types';
import { AssetGroupTypes } from '../asset-groups/asset-groups.types';

@Injectable()
export class UsersService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/UsersAdmin';
        }

    // Get all company user roles
    public get(request: UsersRequest): Observable<UsersResult> {
 
        const obj = this.authHttp.removeObjNullKeys({
            RoleID: request.userRoleID ? request.userRoleID.toString() : null,
            SearchTerm: request.searchTerm ? request.searchTerm.toString() : null,
            PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
            PageSize: request.pageSize ? request.pageSize.toString() : '20',
            Sorting: request.sorting
        });

        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }

    public getDefaultCompanySettings(): Observable<UserDefaultCompanySettings> {
        return this.authHttp
            .get(this.baseURL + '/DefaultSettings');
    }

    
    public getByID(id: number): Observable<UserAccount> {
        return this.authHttp
            .get(this.baseURL + '/' + id);
    }

    public getRightsByID(id: number): Observable<UserRightsResult> {
        return this.authHttp
            .get(this.baseURL + '/GetAccessRights/' + id);
    }

    public getRoleByID(id: number): Observable<number> {
        return this.authHttp
            .get(this.baseURL + '/GetRole/' + id);
    }

    public getGroupMembershipByID(id: number, groupType: AssetGroupTypes): Observable<UserGroupMembershipResult> {

        const params = new HttpParams({
            fromObject: {
                UserID: String(id),
                GroupType: String(groupType)
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetGroupMembership/', { params });
    }


    public create(entity: UserAccount): Observable<UserCreateCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as UserCreateCodeOptions;
                    }
                })
            );
    }


    public update(entity: UserAccount): Observable<UserUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserUpdateCodeOptions;
                    }
                })
            );            
    }
    

    public updateAccessRights(entity: UserAccessRightsRequest): Observable<UserAccessRightsUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateAccessRights/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserAccessRightsUpdateCodeOptions;
                    }
                })
            );            
    }

    public updateGroupMembership(entity: UserGroupMembershipUpdateRequest): Observable<UserGroupMembershipCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateGroupMembership/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserGroupMembershipCodeOptions;
                    }
                })
            );            
    }

    public updateRole(entity: UserRoleUpdateRequest): Observable<UserRoleUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateRole/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserRoleUpdateCodeOptions;
                    }
                })
            );            
    }


    // Delete user 
    public delete(id: number): Observable<UserRemoveCodeOptions> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserRemoveCodeOptions;
                    }
                })
            ); 
    }
} 
