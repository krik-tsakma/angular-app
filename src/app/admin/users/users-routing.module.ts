// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';
import { UsersService } from './users.service';

// COMPONENTS
import { ViewUsersComponent } from './view/view-users.component';
import { EditUserSettingsComponent } from './edit/settings/edit-user-settings.component';


const UserAccountsRoutes: Routes = [
    {
        path: '',
        component: ViewUsersComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditUserSettingsComponent,
        data: {
            breadcrumb: 'form.actions.create'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-user.module').then((m) => m.EditUserModule),
        data: {
            breadcrumb: 'form.actions.edit'
        },
        canActivate: [AuthGuard]
    }    
];

@NgModule({
    imports: [
        RouterModule.forChild(UserAccountsRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        UsersService
    ]
})
export class UsersRoutingModule { }
