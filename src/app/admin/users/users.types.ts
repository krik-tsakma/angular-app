import { MeasurementSystemOptions, VehicleLabelOptions, DriverLabelOptions } from '../../common/entities/entity-types';
import { AssetGroupTypes } from '../asset-groups/asset-groups.types';
import { Module } from '../../auth/acl.types';
import { KeyName } from '../../common/key-name';
import { PagerRequest, PagerResults } from '../../common/pager';

export const UsersSessionName: string = 'un';

export interface UsersRequest extends PagerRequest {
    userRoleID?: number;
    searchTerm: string;
}

export interface UsersResult extends PagerResults {
    results: UserAccountResultItem[];
}

export interface UserAccountResultItem extends KeyName {
    role: string;
}

export class UserAccount {
    public id: number;
    public firstname: string;
    public lastname: string;
    public email: string;
    public phone: string;
    public username: string;
    public roleID: number;
    public password: string;
    public confirmPassword: string;
    public timeZoneIndex: number;
    public measurementSystem: MeasurementSystemOptions;
    public culture: string;
    public vehicleLabel: VehicleLabelOptions;
    public driverLabel: DriverLabelOptions;
    public refreshInterval: number;

    constructor() {
        this.measurementSystem = MeasurementSystemOptions.Metric;
        this.vehicleLabel = VehicleLabelOptions.LicencePlate;
        this.driverLabel = DriverLabelOptions.Name;
    }
}

// ==========================
// DEFAULT COMPANY SETTINGS
// ==========================

export interface UserDefaultCompanySettings {
    timezoneIndex: number;
    culture: string;
    driverLabel: DriverLabelOptions;
    vehicleLabel: VehicleLabelOptions;
    measurementSystem: MeasurementSystemOptions;
}

// ====================
// USER ROLE
// ====================

export interface UserRoleUpdateRequest {
    userID: number;
    roleID: number;
}

export enum UserRoleUpdateCodeOptions {
    Changed = 0,
    UserNotFound = -1,
    UserRoleNotFound = -2
}

// ====================
// GROUP MEMBERSHIP
// ====================

export interface UserGroupMembershipRequest {
    userID: number;
    groupType: AssetGroupTypes;
}
export interface UserGroupMembershipUpdateRequest extends UserGroupMembershipRequest {
    groupIDs: number[];
}

export interface UserGroupMembershipResult {
    roleID: number;
    groupIDs: number[];
}

export enum UserGroupMembershipCodeOptions {
    Set = 0,
    UserNotFound = 1,
}

// ====================
// ACCESS RIGHTS
// ====================

export interface UserRightsResult {
    roleID: number;
    rights: Module[];
}

export interface UserAccessRightsRequest {
    userID: number;
    rights: Module[];
}

export enum UserAccessRightsUpdateCodeOptions {
    Set = 0,
    UserNotFound = 1,
    AccessRightsExceedingThoseOfRole = 2,
    UnknownError = 10
}

// ====================
// RESULT CODES
// ====================
export enum UserCreateCodeOptions {
    Created = 0,
    FirstLastNameCombinationExists = 1,
    EmailAddressInvalid = 2,
    MobilePhoneNumberInvalid = 3,
    PasswordsDoNotMatch = 4,
    UsernameAlreadyExists = 10,
    EmailAddressAlreadyExists = 11,
    SycadaSuperAdminCreationNotAllowed = 12,
    UnknownError = 13,
}

export enum UserUpdateCodeOptions {
    Set = 0,
    UserNotFound = 1,
    UsernameAlreadyExists = 2,
    FirstLastNameCombinationExists = 3,
    EmailAddressAlreadyExists = 4,
    UnknownError = 5,
    EmailAddressInvalid = 6,
    MobilePhoneNumberInvalid = 7
}

export enum UserRemoveCodeOptions {
    Removed = 0,
    NoSuchUserExists = 1,
    LastSycadaAdmin = 2,
    UnknownError = 3
}
