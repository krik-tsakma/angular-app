

// FRAMEWORK
import { Injectable } from '@angular/core';

@Injectable()
export class AdminLabelService {
    private sessionItemName: string;
    constructor() {
        // foo
    }

    public setSessionItemName(name: string) {
        this.sessionItemName = name;
    }

    public set(name: string) {
        sessionStorage.setItem(this.sessionItemName, name);
    }

    public get() {
        return sessionStorage.getItem(this.sessionItemName);
    }

    public remove() {
        return sessionStorage.removeItem(this.sessionItemName);
    }
}
