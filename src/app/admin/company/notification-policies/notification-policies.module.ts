// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { NotificationPoliciesRoutingModule } from './notification-policies-routing.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';

// SERVICES
import { NotificationPoliciesService } from './notification-policies.service';

// COMPONENTS
import { ViewNotificationPoliciesComponent } from './view/view-notification-policies.component';
import { EditNotificationPoliciesComponent } from './edit/edit-notification-policies.component';
import { AddToListComponent } from './edit/add-to-list/add-to-list.component';

@NgModule({
    imports: [
        SharedModule,
        NotificationPoliciesRoutingModule,
        SettingsListModule,
    ],
    declarations: [
        ViewNotificationPoliciesComponent,
        EditNotificationPoliciesComponent,
        AddToListComponent
    ],      
    providers: [
        NotificationPoliciesService
    ]
})
export class NotificationPoliciesModule { }
