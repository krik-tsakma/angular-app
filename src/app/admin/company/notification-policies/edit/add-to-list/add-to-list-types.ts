﻿import { PagerResults } from '../../../../../common/pager';

export enum ValidationType {
    email = 1,
    phone = 2
}

export interface AddToListResults extends PagerResults {
    results: string[];
}
