// FRAMEWORK
import {
    Component, OnChanges, OnInit, Input, Output, ViewChild, ElementRef,
    Renderer, EventEmitter, ViewEncapsulation, SimpleChanges 
} from '@angular/core';

// RXJS
import { Observable, Subject, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

// SERVICES
import { NotificationPoliciesService } from '../../notification-policies.service';

// TYPES
import { ValidationType } from './add-to-list-types';
import { ValidationRegexPatterns } from '../../../../../shared/form-validation-directives/validation-regex-patterns';

@Component({
    selector: 'add-to-list',
    templateUrl: './add-to-list.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: []
})

export class AddToListComponent implements OnInit, OnChanges {
    @ViewChild('term', { static: false }) public fileInput: ElementRef;
    @Input() public chosenOption: ValidationType;
    @Output() public onSelect = new EventEmitter<string>();
    @Output() public onAdd = new EventEmitter<string>();
    public loading: boolean = false;
    public inputSelected: any;
    public searchTermStream: Subject<string> = new Subject();
    public pattern: RegExp;
   
    public items: Observable<string[]> = this.searchTermStream
        .pipe(
            debounceTime(500),
            distinctUntilChanged(),
            switchMap((term) => {
                // use this to empty result set in case of change            
                if (term === null) {
                    this.loading = false;
                    return of([]);
                } else {
                    this.loading = false;
                    return this.service.getlist(this.chosenOption, term);
                }
            })
        );

    constructor(private service: NotificationPoliciesService ,
                private renderer: Renderer) {
        // foo
    }

    public ngOnInit() {
        this.pattern = this.chosenOption === ValidationType.email
            ? new RegExp(ValidationRegexPatterns.email)
            : new RegExp(ValidationRegexPatterns.phoneNumber);        
    }

    public ngOnChanges(changes: SimpleChanges) {
        const changeTerm = changes['term'];
        if (changeTerm) {            
            this.inputSelected = changeTerm.currentValue;
        }
        const changeName = changes['name'];
        if (changeName) {            
            this.inputSelected = changeName.currentValue;
        }
    }

    // Method that pass
    public search(term: any) {   
        this.loading = true;                 
        if (term && term.length > 1) {
            this.searchTermStream.next(term);
        } else {
            this.loading = false;     
        }
    }

    public selected(term?: string) {
        this.onSelect.emit(term);
        this.inputSelected = null;
    }


    public optionValid(term) {
        return this.pattern.test(term);
    }


    public onAddNew(term) {        
        if (this.pattern.test(term)) {
            this.onAdd.emit(term);
            this.clearInput();
        }
    }


    public clearInput(e?: Event) {
        // do not bubble event otherwise focus is on the textbox
        if (e) {
            e.stopPropagation();
        }

        // clear selection
        this.inputSelected = null;
        this.selected(null);

        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.fileInput.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }
}
