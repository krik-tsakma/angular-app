﻿// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';
import { SearchMechanismComponent } from '../../../../shared/search-mechanism/search-mechanism.component';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { StoreManagerService } from '../../../../shared/store-manager/store-manager.service';
import { AdminLabelService } from '../../../admin-label.service';
import { NotificationPoliciesService } from '../notification-policies.service';
import { Config } from '../../../../config/config';

// TYPES
import { SearchType } from '../../../../shared/search-mechanism/search-mechanism-types';
import { NotificationPolicySessionName, NotificationPolicy, NotificationPolicySendOptions } from '../notification-policies.types';
import { ValidationType } from './add-to-list/add-to-list-types';
import { KeyName } from '../../../../common/key-name';

@Component({
    selector: 'edit-notification-policies',
    templateUrl: 'edit-notification-policies.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-notification-policies.less'],
})

export class EditNotificationPoliciesComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    @ViewChild(SearchMechanismComponent, { static: true }) public searchAsset: SearchMechanismComponent;
    public notificationPolicy: NotificationPolicy = {} as NotificationPolicy;
    public loading: boolean;
    public sendToDriver: KeyName[] = [
        {
            id: NotificationPolicySendOptions.sms,
            name: 'SMS',
            enabled: false
        },
        {
            id: NotificationPolicySendOptions.email,
            name: 'E-mail',
            enabled: false
        }
    ];
    public sendToFleetManager: KeyName[] = [
        {
            id: NotificationPolicySendOptions.sms,
            name: 'SMS',
            enabled: false
        },
        {
            id: NotificationPolicySendOptions.email,
            name: 'E-mail',
            enabled: false
        }
    ];
    public assetTypes: any = SearchType;
    public validationType: any = ValidationType;
   
    constructor(        
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBarService: SnackBarService,
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,
        protected storeManager: StoreManagerService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private service: NotificationPoliciesService,
        private activatedRoute: ActivatedRoute
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBarService, storeManager, confirmDialogService);
        this.labelService.setSessionItemName(NotificationPolicySessionName);
        
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => {
                this.action = params.id
                    ? 'form.actions.edit'
                    : 'form.actions.create';
                if (params.id) {
                    this.get(params.id);
                }
            });
    }

    // Emits every time a search option is selected from search inputbox's available options
    public onAssetSelect(item: KeyName) {
        if (!item) {
            return;
        }

        if (!this.notificationPolicy.users) {
            this.notificationPolicy.users = [];
        }

        if (this.notificationPolicy.users.find((user) => user.id === item.id) === undefined) {
            this.notificationPolicy.users.push(item);
        }

        if (this.searchAsset) {
            this.searchAsset.clearInput();
        }
    }


    public removeAsset(id) {
        if (!id) {
            return;
        }
        
        this.notificationPolicy.users = this.notificationPolicy.users.filter((user) => {
            return user.id !== id;
        });

    }


    public onListItemSelect(item: string, type: ValidationType) {
        if (!item) {
            return;
        }
        const sendOption = type === ValidationType.email
            ? 'emailAddresses'
            : 'mobilePhoneNumbers';

        if (!this.notificationPolicy[sendOption]) {
            this.notificationPolicy[sendOption] = [];
        } 

        if (this.notificationPolicy[sendOption].find((option) => option === item) === undefined) {
            this.notificationPolicy[sendOption].push(item);
        }

        if (this.searchAsset) {
            this.searchAsset.clearInput();
        }
    }

    public removeListItem(item: string, type: ValidationType) {
        if (!item) {
            return;
        }

        const sendOption = type === ValidationType.email
            ? 'emailAddresses'
            : 'mobilePhoneNumbers';

        this.notificationPolicy[sendOption] = this.notificationPolicy[sendOption].filter((option) => {
            return option !== item;
        });

    }


    public onSubmit(editForm) {
        if (editForm.valid) {
            this.notificationPolicy.name = editForm.value.name;
            this.notificationPolicy.driverSend = [];
            this.sendToDriver.forEach((opt) => {
                if (opt.enabled) {
                    this.notificationPolicy.driverSend.push(opt.id);
                }
            });
            this.notificationPolicy.fleetManagerSend = [];
            this.sendToFleetManager.forEach((opt) => {
                if (opt.enabled) {
                    this.notificationPolicy.fleetManagerSend.push(opt.id);
                }
            });

            const snackBarResults = (code) => {
                if (code === 200) {
                    this.snackBar.open('form.save_success', 'form.actions.ok');
                } else if (code === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            };

            if (this.notificationPolicy.id) {
                // Update an existing notification policy
                this.service.update(this.notificationPolicy)
                    .subscribe((res) => snackBarResults(res));
            } else {
                // Create a new notification policy
                this.service.create(this.notificationPolicy)
                    .subscribe((res) => snackBarResults(res));
            }
        }
    }


    // =========================
    // DATA
    // =========================
    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((res: NotificationPolicy) => {
                this.loading = false;
                this.notificationPolicy = res;
                // this.notificationPolicy.driverSend.forEach((drSend) => {
                //     if (drSend) {
                //         this.sendToDriver.find((option) => option.id === drSend).enabled = true;
                //     }
                // });
                // this.notificationPolicy.fleetManagerSend.forEach((FMSend) => {
                //     if (FMSend) {
                //         this.sendToFleetManager.find((option) => option.id === FMSend).enabled = true;
                //     }
                // });
                console.log('notificationPolicy: ', res);
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            });
    }
}
