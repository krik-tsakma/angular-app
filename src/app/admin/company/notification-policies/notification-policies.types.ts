import { KeyName } from '../../../common/key-name';
import { PagerResults } from '../../../common/pager';

export const NotificationPolicySessionName: string = 'notpoln';

export interface NotificationPoliciesResult extends PagerResults {
    results: NotificationPolicyItem[];
}

export interface NotificationPolicyItem {
    id: number;
    name: string;
    isDefault: boolean;
}

export enum NotificationPolicySendOptions {
    email = 1,
    sms = 2
}

export interface NotificationPolicy {
    id: number;
    name: string;
    driverSend: NotificationPolicySendOptions[];
    fleetManagerSend: NotificationPolicySendOptions[];
    users: KeyName[];
    emailAddresses: string[];
    mobilePhoneNumbers: string[];
}
