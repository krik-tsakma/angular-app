// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RxJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../../auth/authHttp';
import { Config } from '../../../config/config';

// TYPES
import { SearchTermRequest } from '../../../common/pager';
import { NotificationPolicy, NotificationPoliciesResult } from './notification-policies.types';
import { ValidationType } from './edit/add-to-list/add-to-list-types';

@Injectable()
export class NotificationPoliciesService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/NotificationPoliciesAdmin';
        }


    // Get all notification policies
    public get(request: SearchTermRequest): Observable<NotificationPoliciesResult> {
    
        const params = new HttpParams({
            fromObject: {
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                Sorting: request.sorting
            }
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }



    // Get specific notification policy via ID
    public getByID(id: number): Observable<NotificationPolicy> {
         return this.authHttp
             .get(this.baseURL + '/' + id);
    }



    // Get list option for Emails or Mobile phones (depends on term option)
    public getlist(typeOflist: ValidationType, term: string): Observable<string[]> {
        if (!term || term.length < 2) {
            return;
        }

        const type = typeOflist === ValidationType.email
            ? 'GetEmailAddresses'
            : 'GetPhoneNumbers';

        const params = new HttpParams({
            fromObject: {
                SearchTerm: term,                               
            }
        }); 

        return this.authHttp
            .get(this.baseURL + '/' + type , { params })
            .pipe(
                map((res) => {
                    return res;
                })
            );
    }



    // Create new notification policy
    public create(notPolicy: NotificationPolicy): Observable<number>  {
        return this.authHttp
            .post(this.baseURL, notPolicy, { observe: 'response' })
            .pipe(
                map((r) => r.status as number)
             );
    }



    // Update a notification policy
    public update(notPolicy: NotificationPolicy): Observable<number>  {
        return this.authHttp
            .put(this.baseURL, notPolicy, { observe: 'response' })
            .pipe(
                map((r) => r.status as number)
            );     
    }



    // Delete a notification policy
    public delete(id: number): Observable<number> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((r) => r.status as number)
            ); 
    }

} 
