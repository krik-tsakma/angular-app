
// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';

// COMPONENTS
import { ViewNotificationPoliciesComponent } from './view/view-notification-policies.component';
import { EditNotificationPoliciesComponent } from './edit/edit-notification-policies.component';

const NotificationPoliciesRoutes: Routes = [
    {
        path: '',
        component: ViewNotificationPoliciesComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditNotificationPoliciesComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        component: EditNotificationPoliciesComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }   
];

@NgModule({
    imports: [
        RouterModule.forChild(NotificationPoliciesRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class NotificationPoliciesRoutingModule { }
