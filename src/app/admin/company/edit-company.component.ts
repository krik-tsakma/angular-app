
// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { AdminLabelService } from '../admin-label.service';
import { UserOptionsService } from '../../shared/user-options/user-options.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { Config } from '../../config/config';

// TYPES 
import { SettingList } from '../settings-list/settings-list.types';
import { CompanySessionName } from './company.types';

@Component({
    selector: 'edit-company',
    templateUrl: 'edit-company.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditCompanyComponent {
    public options: SettingList[] = [
        {
            title: 'company.category.basic',
            items: [
                {
                    option: 'company.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
                {
                    option: 'company.category.basic.settings.map',
                    params: ['map'],
                    icon: 'map'
                },
                {
                    option: 'company.category.basic.settings.password_policy',
                    params: ['password-policy'],
                    icon: 'lock'
                }
            ]
        },
        {
            title: 'company.category.appearance',
            items: [
                {
                    option: 'company.category.appearance.customize',
                    params: ['appearance'],
                    icon: 'format_color_fill'
                }
            ]
        },
        {
            title: 'company.category.notifications',
            items: [
                {
                    option: 'company.category.notifications.event_properties',
                    params: ['events'],
                    icon: 'report'
                },
                // {
                //     option: 'company.category.notifications.policies',
                //     params: ['notification-policies'],
                //     icon: 'report'
                // }
            ]
        },
        // {
        //     title: 'company.category.access_restrictions',
        //     items: [
        //         {
        //             option: 'company.category.access_restrictions.ip_restrictions',
        //             params: ['ip-restrictions'],
        //             icon: 'remove_circle'
        //         }
        //     ]
        // }
    ];


    constructor(public labelService: AdminLabelService,
                private configService: Config,
                private router: Router,
                private previousRouteService: PreviousRouteService,
                private userOptions: UserOptionsService) {
        this.labelService.setSessionItemName(CompanySessionName);
        this.labelService.set(this.userOptions.getUser().companyName);
    }
    
    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
