import { MeasurementSystemOptions, VehicleLabelOptions, DriverLabelOptions } from '../../common/entities/entity-types';

export const CompanySessionName: string = 'cn';

// ====================
// COMPANY SETTINGS
// ====================

export interface CompanySettings {
    name: string;
    description: string;
    accountCreationCodePrefix: string;
    timeZoneIndex: number;
    measurementSystem: MeasurementSystemOptions;
    culture: string;
    vehicleLabel: VehicleLabelOptions;
    driverLabel: DriverLabelOptions;
    allowAliasesUse: boolean;
}

export enum CompanySettingsUpdateCodeOptions {
    Updated = 0,
    NoSuchCompanyExists = 1,
    NameAlreadyExists = 2,
    NameRequired = 3,
    InvalidAccountCreationCodePrefix = 4,
    UnknownError = 4
}

// ====================
// MAP
// ====================

export interface CompanyMapSettings {
    refreshIntervalMinutes: number;
    singleMapPointScale: number;
    showVehiclesOfflineAfterHours: number;
}

export enum CompanyMapSettingsUpdateCodeOptions {
    Set = 0,
    NoSuchCompanyExists = 1,
}

// ====================
// APPEARANCE
// ====================

export class CompanyAppearenceSettings {
    public theme: CompanyTheme;
    public logoURL?: string;
    
    constructor() {
        this.theme = new CompanyTheme();
    }
}

export class CompanyTheme {
    public primaryColor: string;
    public secondaryColor: string;

    constructor() {
        this.primaryColor = 'blue';
        this.secondaryColor = 'green';
    }
}
