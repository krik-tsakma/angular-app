// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpResponseBase } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { Module } from '../../auth/acl.types';
import { 
    CompanySettings, 
    CompanySettingsUpdateCodeOptions, 
    CompanyAppearenceSettings, 
    CompanyTheme, 
    CompanyMapSettings, 
    CompanyMapSettingsUpdateCodeOptions
} from './company.types';
import { FileUploadResult } from '../../common/file-upload-types';

@Injectable()
export class CompanyService {
    private baseURL: string;
   
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/Company';
        }

    // ====================
    // BASIC SETTINGS
    // ====================
    
    public get(): Observable<CompanySettings> {
        return this.authHttp
            .get(this.baseURL);
    }
   
    public update(entity: CompanySettings): Observable<CompanySettingsUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as CompanySettingsUpdateCodeOptions;
                    }
                })
            );            
    }

    // ====================
    // MAP
    // ====================

    public getMapSettings(): Observable<CompanyMapSettings> {
        return this.authHttp
            .get(this.baseURL + '/GetMapSettings');
    }

    public updateMapSettings(entity: CompanyMapSettings): Observable<CompanyMapSettingsUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateMapSettings', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.json() as CompanyMapSettingsUpdateCodeOptions;
                    }
                })
            );            
    }

    // ====================
    // LOGO
    // ====================

    public getAppearanceSetting(): Observable<CompanyAppearenceSettings> {
        return this.authHttp
            .get(this.baseURL + '/GetAppearanceSettings');
    }

    // Upload log method 
    public uploadLogo(fileToUpload: File): Observable<FileUploadResult> {
        const input = new FormData();
        input.append('File', fileToUpload);
        
        return this.authHttp
            .post(this.baseURL + '/UploadLogo', input);
    }

    // Remove logo method 
    public removeLogo(): Observable<number>  {
        return this.authHttp
            .delete(this.baseURL + '/RemoveLogo', { observe: 'response' })
            .pipe(
                map((res) => {
                    return res.status;
                })                
            );
    }

    // ====================
    // LOGO
    // ====================

    public setCustomTheme(theme: CompanyTheme): Observable<number>  {
        return this.authHttp
            .put(this.baseURL + '/SetCustomTheme', theme, { observe: 'response' })
            .pipe(
                map((res: HttpResponseBase) => res.status)                
            );
    }

    public setDefaultTheme(): Observable<number>  {
        return this.authHttp
            .delete(this.baseURL + '/SetDefaultTheme', { observe: 'response' })
            .pipe(
                map((res: HttpResponseBase) => res.status)                
            );
    }
} 
