// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { EditCompanyComponent } from './edit-company.component';
import { EditCompanySettingsComponent } from './settings/edit-company-settings.component';
import { EditCompanyAppearenceSettingsComponent } from './appearance/appearance-settings.component';
import { EditCompanyMapSettingsComponent } from './map-settings/edit-company-map-settings.component';
import { EditCompanyEventPropertiesComponent } from './events/edit-events-properties.component';
import { IpRestrictionsComponent } from './ip-restrictions/ip-restrictions.component';

const CompanyRoutes: Routes = [
    {
        path: '',
        component: EditCompanyComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'settings',
        component: EditCompanySettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'company.category.basic.settings'
        }
    },
    {
        path: 'map',
        component: EditCompanyMapSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'company.category.basic.settings.map'
        }
    },
    {
        path: 'appearance',
        component: EditCompanyAppearenceSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'company.category.appearance'
        }
    },
    {
        path: 'password-policy',
        loadChildren: () => import('./password-policies/password-policies.module').then((m) => m.PasswordPoliciesModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'company.category.basic.settings.password_policy'
        }
    },
    {
        path: 'events',
        component: EditCompanyEventPropertiesComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'company.category.notifications.event_properties'
        }
    },
    {
        path: 'notification-policies',
        loadChildren: () => import('./notification-policies/notification-policies.module').then((m) => m.NotificationPoliciesModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'company.category.notifications.policies'
        }
    },
    {
        path: 'ip-restrictions',
        component: IpRestrictionsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'company.category.access_restrictions.ip_restrictions'
        }
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(CompanyRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class CompanyRoutingModule { }
