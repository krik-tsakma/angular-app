// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// SERVICES
import { CompanyService } from '../company.service';
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { AdminLabelService } from '../../admin-label.service';

// TYPES
import { FileUploadErrorMessages, FileUploadResult, FileUploadResultCodeOptions } from '../../../common/file-upload-types';
import { CompanyAppearenceSettings, CompanyTheme, CompanySessionName } from '../company.types';
import { Config } from '../../../config/config';

@Component({
    selector: 'company-appearance-settings',
    templateUrl: 'appearance-settings.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['appearance-settings.less']
})

export class EditCompanyAppearenceSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('fileLogoInput', { static: false }) public fileLogoInput;
    @ViewChild('logoImage', { static: false }) public logoImage;
    public loading: boolean;
    public company: CompanyAppearenceSettings = new CompanyAppearenceSettings();
    public theme: CompanyTheme = new CompanyTheme();

    public colorPickerPrimaryOptions = [];
    public colorPickerSecondaryOptions = [];

    /** These are the keys used to define the widget types. */
    private customThemePrimaryOptions: { [id: string]: string } = {
        ['blue']: '#1C5074',
        ['tirquaz']: '#0ea4b5',
        ['purple']: '#472f91',
    };
    private customThemeSecondaryOptions: { [id: string]: string } = {
        ['green']: '#65B254',
        ['lgreen']: '#c7d527',
        ['orange']: '#f47920',
        ['red']: '#e40520',
    };

    private imageEmpty: string = this.config.get('companyLogoUrl') + '/SycadaLogo.png';


    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected router: Router,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        private service: CompanyService,
        private config: Config,
        private userOptions: UserOptionsService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, config);
        labelService.setSessionItemName(CompanySessionName);

        Object.keys(this.customThemePrimaryOptions).forEach((key) => { 
            this.colorPickerPrimaryOptions.push(this.customThemePrimaryOptions[key]);
        });
        Object.keys(this.customThemeSecondaryOptions).forEach((key) => { 
            this.colorPickerSecondaryOptions.push(this.customThemeSecondaryOptions[key]);
        });
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.get();

    }

    
    // =========================
    // EVENT HANDLERS
    // =========================

    /**
     * upload ev image file to server
     */
    public uploadLogo(): void {
        const fi = this.fileLogoInput.nativeElement as HTMLInputElement;
        if (fi.files && fi.files[0]) {
            this.loading = true;
            const fileToUpload = fi.files[0];
            this.service
                .uploadLogo(fileToUpload)
                .subscribe((res: FileUploadResult) => {
                    this.loading = false;
                    if (res.code === FileUploadResultCodeOptions.Ok) {
                        this.company.logoURL = '';
                        this.company.logoURL = res.url;
                        fi.value = '';
                    } else {
                        let message = 'form.errors.unknown';
                        const messageCode = FileUploadErrorMessages.find((x) => x.id === res.code);
                        if (messageCode) {
                            message = messageCode.name;
                        } 
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                    this.loading = false;
                });
        } else {
            this.snackBar.open('file_upload.errors.null', 'form.actions.close');
        }
    }
    
    /**
     * remove logo image file from server
     */
    public removeLogo(): void {
        this.loading = true;
        this.service
            .removeLogo()
            .subscribe((status: number) => {
                this.loading = false;
                if (status === 200) {
                    const fi = this.fileLogoInput.nativeElement as HTMLInputElement;
                    fi.value = '';
                    this.company.logoURL = '';
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
                this.loading = false;
            });
    }

    /**
     * show an empty image if none was found in the server
     */
    public imgErrorHandler(event) {
        event.target.src = this.imageEmpty;
    }
   
    // =========================
    // DATA
    // =========================

    public setTheme() {
        this.loading = true;
        this.unsubscribeService();

        // switch from hex code to string notation
        Object.keys(this.customThemePrimaryOptions).forEach((key) => { 
            if (this.customThemePrimaryOptions[key] === this.theme.primaryColor) {
                this.company.theme.primaryColor = key;
            }
        });
        Object.keys(this.customThemeSecondaryOptions).forEach((key) => { 
            if (this.customThemeSecondaryOptions[key] === this.theme.secondaryColor) {
                this.company.theme.secondaryColor = key;
            }
        });

        this.serviceSubscriber = this.service
            .setCustomTheme(this.company.theme)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 200) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.userOptions.setTheme(this.company.theme);
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }

    public setDefaultTheme() {
        this.loading = true;
        this.unsubscribeService();

        this.serviceSubscriber = this.service
            .setDefaultTheme()
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 200) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.userOptions.setTheme(null);
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }

    private get() {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getAppearanceSetting()
            .subscribe((res: CompanyAppearenceSettings) => {
                this.loading = false;
                this.company.logoURL = res.logoURL;
                if (res.theme) {
                    // get hex code from string notation
                    this.theme.primaryColor = this.customThemePrimaryOptions[res.theme.primaryColor];
                    this.theme.secondaryColor = this.customThemeSecondaryOptions[res.theme.secondaryColor];
                } else {
                    this.theme.primaryColor = this.customThemePrimaryOptions['blue'];
                    this.theme.secondaryColor = this.customThemeSecondaryOptions['green'];
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }
}
