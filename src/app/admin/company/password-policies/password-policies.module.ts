// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';
import { PasswordPoliciesRoutingModule } from './password-policies-routing.module';

// SERVICES
import { PasswordPoliciesService } from './password-policies.service';

// COMPONENTS
import { EditPasswordPoliciesComponent } from './edit-password-policy.component';

@NgModule({
    imports: [
        SharedModule,
        SettingsListModule,
        PasswordPoliciesRoutingModule
    ],
    declarations: [
        EditPasswordPoliciesComponent,
    ],      
    providers: [
        PasswordPoliciesService
    ]
})
export class PasswordPoliciesModule { }
