// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { PasswordPoliciesService } from './password-policies.service';
import { AdminLabelService } from '../../admin-label.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES
import { PasswordPolicyItem, PasswordPolicyUpsertCodeOptions } from './password-policies-types';
import { CompanySessionName } from '../company.types';

@Component({
    selector: 'edit-password-policy',
    templateUrl: 'edit-password-policy.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-password-policy.less'],
})

export class EditPasswordPoliciesComponent extends AdminBaseComponent implements OnInit {
    public action: string;
    public loading: boolean;
    public policy: PasswordPolicyItem = {} as PasswordPolicyItem;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        public userOptions: UserOptionsService,
        private service: PasswordPoliciesService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        this.labelService.setSessionItemName(CompanySessionName);
    }

    // =========================
    // LIFECYCLE
    // =========================

    public ngOnInit(): void {
        super.ngOnInit();
        this.get();
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: {}, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();
     
        this.serviceSubscriber = this.service
            .upsert(this.policy)
            .subscribe((res: PasswordPolicyUpsertCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    public restoreDefaults() {
        this.loading = true;
        this.unsubscribeService();
     
        this.serviceSubscriber = this.service
            .reset()
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 200) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }

    private get() {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .get()
            .subscribe((res: PasswordPolicyItem) => {
                this.loading = false;
                this.policy = res;
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }
}
