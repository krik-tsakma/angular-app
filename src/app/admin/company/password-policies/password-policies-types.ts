export interface PasswordPolicyItem {
    isDefault: boolean;
    minPasswordLength: number;
    minLowerCaseChars: number;
    minUpperCaseChars: number;
    minDigits: number;
    minSpecialChars: number;
}

export enum PasswordPolicyUpsertCodeOptions {
    Found = 0,
    IssuerNotFound = -1,
    NotFound = -2
}
