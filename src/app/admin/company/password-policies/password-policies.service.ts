// FRAMEWORK
import { Injectable } from '@angular/core';

// RxJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../../auth/authHttp';
import { Config } from '../../../config/config';

// TYPES
import { PasswordPolicyItem, PasswordPolicyUpsertCodeOptions } from './password-policies-types';

@Injectable()
export class PasswordPoliciesService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/PasswordPolicies';
        }


    // Get the password policy for the company
    public get(): Observable<PasswordPolicyItem> {
         return this.authHttp
             .get(this.baseURL);
    }


    // Update a password policy
    public upsert(policy: PasswordPolicyItem): Observable<PasswordPolicyUpsertCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, policy, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.json() as PasswordPolicyUpsertCodeOptions;
                    }
                })
            );     
    }


    // Reset company's password policy to the default values
    public reset(): Observable<number> {
        return this.authHttp
            .delete(this.baseURL, { observe: 'response' })
            .pipe(
                map((r) => r.status as number)                    
            ); 
    }

} 
