
// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';

// COMPONENTS
import { EditPasswordPoliciesComponent } from './edit-password-policy.component';

const PasswordPoliciesRoutes: Routes = [
    {
        path: '',
        component: EditPasswordPoliciesComponent,
        canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(PasswordPoliciesRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class PasswordPoliciesRoutingModule { }
