// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { AdminLabelService } from '../../../admin-label.service';
import { EventPropertiesService } from '../event-properties.service';
import { Config } from '../../../../config/config';

// TYPES
import { 
    EventPropertyUpdateRequest, 
    EventClassUpdateCodeOptions, 
    EventStatusUpdateCodeOptions, 
    EventPriorityUpdateCodeOptions 
} from '../event-properties-types';

// DIALOG
@Component({
    selector: 'edit-company-event-property',
    templateUrl: 'edit-event-property.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-event-property.less']
})

export class EditCompanyEventPropertyComponent extends AdminBaseComponent implements OnInit {
    public loading: boolean;
    public action: string;
    public record: EventPropertyUpdateRequest = {} as EventPropertyUpdateRequest;

    constructor(
        
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected router: Router,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        public dialogRef: MatDialogRef<EditCompanyEventPropertyComponent>,
        private activatedRoute: ActivatedRoute,
        private service: EventPropertiesService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        this.record = Object.assign({}, data);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public closeDialog(type?: string) {
        this.dialogRef.close(type);
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: EventPropertyUpdateRequest, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        switch (this.record.type) {
            case 'priority':
                this.updateEventPriority(this.record);
                break;
            case 'status':
                this.updateEventStatus(this.record);
                break;
            case 'class':
                this.updateEventClass(this.record);
                break;
        }

    }

    private updateEventPriority(req: EventPropertyUpdateRequest) {
        this.loading = true;
        req.reset = false;

        this.service
            .updateEventPriority(req)
            .subscribe((res: EventPriorityUpdateCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.closeDialog(this.record.type);
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private updateEventClass(req: EventPropertyUpdateRequest) {
        this.loading = true;
        req.reset = false;

        this.service
            .updateEventClass(req)
            .subscribe((res: EventClassUpdateCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.closeDialog(this.record.type);
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private updateEventStatus(req: EventPropertyUpdateRequest) {
        this.loading = true;
        req.reset = false;

        this.service
            .updateEventStatus(req)
            .subscribe((res: EventStatusUpdateCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.closeDialog(this.record.type);
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }
   
}
