// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../../auth/authHttp';
import { Config } from '../../../config/config';

// TYPES
import { EventPropertyUpdateRequest, EventPriorityUpdateCodeOptions, EventStatusUpdateCodeOptions, EventClassUpdateCodeOptions } from './event-properties-types';

@Injectable()
export class EventPropertiesService {
    private baseURL: string;
   
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/EventPropertiesAdmin';
        }

    // ====================
    // EVENT CLASSES
    // ====================
    
    public updateEventClass(entity: EventPropertyUpdateRequest): Observable<EventClassUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/SetEventClass', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as EventClassUpdateCodeOptions;
                    }
                })
            );            
    }

    // ====================
    // EVENT STATUSES
    // ====================

    public updateEventStatus(entity: EventPropertyUpdateRequest): Observable<EventStatusUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/SetEventStatus', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as EventStatusUpdateCodeOptions;
                    }
                })
            );            
    }

    // ====================
    // EVENT PRIORITIES
    // ====================

    public updateEventPriority(entity: EventPropertyUpdateRequest): Observable<EventPriorityUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/SetEventPriority', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as EventPriorityUpdateCodeOptions;
                    }
                })
            );            
    }
} 
