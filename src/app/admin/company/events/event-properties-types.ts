import { KeyName } from '../../../common/key-name';

export interface EventPropertyUpdateRequest extends KeyName {
    reset: boolean;
    type: string;
}

export enum EventClassUpdateCodeOptions {
    Updated = 0,
    NoSuchCompanyExists = 1,
    NoSuchEventClassExists = 2,
    UnknownError = 3
}

export enum EventStatusUpdateCodeOptions {
    Updated = 0,
    NoSuchCompanyExists = 1,
    NoSuchEventStatusExists = 2,
    UnknownError = 3
}

export enum EventPriorityUpdateCodeOptions {
    Updated = 0,
    NoSuchCompanyExists = 1,
    NoSuchEventPriorityExists = 2,
    UnknownError = 3
}
