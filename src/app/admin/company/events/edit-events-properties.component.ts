// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';
import { EditCompanyEventPropertyComponent } from './edit/edit-event-property.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { AdminLabelService } from '../../admin-label.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { EventPropertiesService } from './event-properties.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { Config } from '../../../config/config';
import { EventPrioritiesService } from './../../../common/events-priorities-list/events-priorities.service';
import { EventClassesService } from './../../../common/events-classes-list/events-classes.service';
import { EventStatusesService } from './../../../common/events-statuses-list/events-statuses.service';

// TYPES
import { KeyName } from '../../../common/key-name';
import { CompanySessionName } from '../company.types';
import { TableColumnSetting, TableActionOptions, TableCellFormatOptions, TableActionItem } from '../../../shared/data-table/data-table.types';
import { EventPropertyUpdateRequest, EventPriorityUpdateCodeOptions, EventStatusUpdateCodeOptions, EventClassUpdateCodeOptions } from './event-properties-types';

@Component({
    selector: 'edit-company-event-properties',
    templateUrl: 'edit-events-properties.html',
    encapsulation: ViewEncapsulation.None
})

export class EditCompanyEventPropertiesComponent extends AdminBaseComponent implements OnInit {
    public loadingEventClasses: boolean;
    public eventClasses: KeyName[] = [];
    public tableSettingsClasses: TableColumnSetting[];

    public loadingEventStatuses: boolean;
    public eventStatuses: KeyName[] = [];
    public tableSettingsStatuses: TableColumnSetting[];

    public loadingEventPriorities: boolean;
    public eventPriorities: KeyName[] = [];
    public tableSettingsPriorities: TableColumnSetting[];

    private editEventPropertyDialogRef: MatDialogRef<EditCompanyEventPropertyComponent>;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected confirmDialogService: ConfirmDialogService,
        protected configService: Config,
        public userOptions: UserOptionsService,
        private service: EventPropertiesService,
        private eventClassesService: EventClassesService,
        private eventPrioritiesService: EventPrioritiesService,
        private eventStatusesService: EventStatusesService,
        private dialog: MatDialog
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar, null, confirmDialogService);
        this.labelService.setSessionItemName(CompanySessionName);

        this.tableSettingsClasses = this.getTableSettings();
        this.tableSettingsStatuses = this.getTableSettings();
        this.tableSettingsPriorities = this.getTableSettings();
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.getEventClasses();
        this.getEventStatuses();
        this.getEventPriorities();
    }

    // =========================
    // TABLE CALLBACKS
    // =========================

    // Navigate to edit page
    public onManage(item: TableActionItem, type: string) {
        if (!item) {
            return;
        }

        const record = item.record as EventPropertyUpdateRequest;
        switch (item.action) {
            case TableActionOptions.edit: 
                this.openEditDialog(record, type); 
                break;
            case TableActionOptions.delete: 
                this.openDialog().subscribe((response) => {
                    if (response === true) {
                        switch (type) {
                            case 'priority':
                                this.deleteEventPriority(record.id);
                                break;
                            case 'status':
                                this.deleteEventStatus(record.id);
                                break;
                            case 'class':
                                this.deleteEventClass(record.id);
                                break;
                        }
                    }
                });
                break;
            default: 
                break;

        }
    }


    // =========================
    // DATA
    // =========================

    private getEventClasses() {
        this.loadingEventClasses = true;
        this.eventClassesService
            .get()
            .subscribe((res: KeyName[]) => {
                this.loadingEventClasses = false;
                this.eventClasses = res;
            },
            (err) => {
                this.loadingEventClasses = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }

    private deleteEventClass(id: number) {
        this.loadingEventClasses = true;
        const req = {
            id: id,
            reset: true
        } as EventPropertyUpdateRequest;

        this.service
            .updateEventClass(req)
            .subscribe((res: EventClassUpdateCodeOptions) => {
                this.loadingEventClasses = false;
                if (res === null) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.getEventClasses();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loadingEventClasses = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private getEventStatuses() {
        this.loadingEventStatuses = true;
        this.eventStatusesService
            .get()
            .subscribe((res: KeyName[]) => {
                this.loadingEventStatuses = false;
                this.eventStatuses = res;
            },
            (err) => {
                this.loadingEventStatuses = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }

    private deleteEventStatus(id: number) {
        this.loadingEventStatuses = true;
        const req = {
            id: id,
            reset: true
        } as EventPropertyUpdateRequest;

        this.service
            .updateEventStatus(req)
            .subscribe((res: EventStatusUpdateCodeOptions) => {
                this.loadingEventStatuses = false;
                if (res === null) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.getEventStatuses();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loadingEventStatuses = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private getEventPriorities() {
        this.loadingEventPriorities = true;
        this.eventPrioritiesService
            .get()
            .subscribe((res: KeyName[]) => {
                this.loadingEventPriorities = false;
                this.eventPriorities = res;
            },
            (err) => {
                this.loadingEventPriorities = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }

    private deleteEventPriority(id: number) {
        this.loadingEventPriorities = true;
        const req = {
            id: id,
            reset: true
        } as EventPropertyUpdateRequest;

        this.service
            .updateEventPriority(req)
            .subscribe((res: EventPriorityUpdateCodeOptions) => {
                this.loadingEventPriorities = false;
                if (res === null) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.getEventPriorities();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loadingEventPriorities = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // =========================
    // HELPERS
    // =========================

    private getTableSettings(): TableColumnSetting[] {
        return [
            {
                primaryKey: 'id',
                header: 'event_properties.index',
                format: TableCellFormatOptions.string
            },
            {
                primaryKey: 'name',
                header: 'event_properties.label'
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete]
            }
        ];
    }

    private openEditDialog(item: EventPropertyUpdateRequest, type: string) {
        item.type = type;
        this.editEventPropertyDialogRef = this.dialog.open(EditCompanyEventPropertyComponent, {
            data: item
        });
    
        this.editEventPropertyDialogRef.afterClosed().subscribe((res) => {
            this.editEventPropertyDialogRef = null;
            if (res) {
                switch (res) {
                    case 'priority':
                        this.getEventPriorities();
                        break;
                    case 'status':
                        this.getEventStatuses();
                        break;
                    case 'class':
                        this.getEventClasses();
                        break;
                }
            }
        });
    }
}
