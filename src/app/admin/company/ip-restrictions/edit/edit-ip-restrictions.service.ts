﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// COMPONENTS
import { EditIpRestrictionsComponent } from './edit-ip-restrictions.component';

// SERVICES
import { AuthHttp } from '../../../../auth/authHttp';
import { Config } from '../../../../config/config';

// TYPES
import { IpRestriction } from '../ip-restrictions-types';

@Injectable()
export class EditIpRestrictionsService {
    private baseURL: string;

    constructor(
        private authHttp: AuthHttp,
        private config: Config,
        private dialog: MatDialog) {
        this.baseURL = this.config.get('apiUrl') + '/api/IpRestriction';
    }   


    public saveIpRestriction(ipRestricition: IpRestriction): Observable<number> {
        return this.authHttp
            .post(this.baseURL, ipRestricition, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return res.status;
                    } else {
                        return res.body as number;
                    }
                })
            );
    }



    public updateIpRestriction(ipRestricition: IpRestriction): Observable<number> {
        return this.authHttp
            .put(this.baseURL, ipRestricition, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server                    
                    if (res.status === 200 && !res.body) {
                        return res.status;
                    } else {
                        return res.body as number;
                    }
                })
            );
    }    


}
