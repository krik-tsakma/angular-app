﻿// FRAMEWORK
import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// SERVICES
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { EditIpRestrictionsService } from './edit-ip-restrictions.service';

// PIPES
import { FormValidationMessagePipe } from '../../../../shared/pipes/form-validation-message.pipe';

// TYPES
import { IpRestriction } from '../ip-restrictions-types';

@Component({
    selector: 'edit-ip-restrictions',
    templateUrl: 'edit-ip-restrictions.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-ip-restrictions.less'],
    providers: [FormValidationMessagePipe]
})

export class EditIpRestrictionsComponent implements OnDestroy {

    public loading: boolean;
    public request: IpRestriction = {} as IpRestriction;
    private serviceSubscriber: any;

    constructor(        
        private dialogRef: MatDialogRef<EditIpRestrictionsComponent>,      
        private snackBar: SnackBarService,
        private unsubscribe: UnsubscribeService,
        private service: EditIpRestrictionsService) {
        // foo
    }

   

    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }

    public closeDialog() {
        this.dialogRef.close();
    }

    public onSaveIP({ value, valid }: { value: any, valid: boolean }) {
        const subManage = (res) => {
            this.loading = false;
            // this is means empty Ok response from the server
            if (res === 200) {
                this.snackBar.open('reset_password.success', 'form.actions.ok');
                this.closeDialog();
            } else {
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            }
        };


        // check if model is valid
        if (!valid) {
            return;
        }               

        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        if (!this.request.id) {
            this.serviceSubscriber = this.service
                .saveIpRestriction(this.request)
                .subscribe((res) => {
                    subManage(res);
                },
                (err) => {
                    this.loading = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                });
        } else {                          
            this.serviceSubscriber = this.service
                .updateIpRestriction(this.request)
                .subscribe((res) => {                    
                    subManage(res);
                },
                (err) => {
                    this.loading = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                });
        }
    }
}
