﻿// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// COMPONENTS
import { EditIpRestrictionsComponent } from './edit/edit-ip-restrictions.component';

// SERVICES
import { AuthHttp } from '../../../auth/authHttp';
import { Config } from '../../../config/config';

// TYPES
import { IpRestriction, IpRestrictionsResults, IpRestrictionsRequest } from './ip-restrictions-types';

@Injectable()
export class IpRestrictionsService {
    private baseURL: string;

    constructor(
        private authHttp: AuthHttp,
        private config: Config,
        private dialog: MatDialog) {
        this.baseURL = this.config.get('apiUrl') + '/api/IpRestriction';
    }

    public getIpRestrictions(req: IpRestrictionsRequest): Observable<IpRestrictionsResults> {
        const obj = this.authHttp.removeObjNullKeys({           
            PageNumber: req.pageNumber.toString(),
            PageSize: req.pageSize.toString(),
            Sorting: req.sorting
        });

        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }   



    public deleteIpRestriction(id: number): Observable<number> {

        // const obj = this.authHttp.removeObjNullKeys({
        //    ID: id.toString(),            
        // })
        // const params = new HttpParams({
        //    fromObject: obj
        // }); 
      
        return this.authHttp
            .delete(this.baseURL + '/' + id.toString(), { observe: 'response' })
            .pipe(
                map((res) => {                   
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return res.status;
                    } else {
                        return res.body as number;
                    }
                })
            );
    }


    public OpenDialog(params: IpRestriction): Observable<string> {
        const dialogRef: MatDialogRef<EditIpRestrictionsComponent> = this.dialog.open(EditIpRestrictionsComponent);
        dialogRef.componentInstance.request.id = params.id;
        dialogRef.componentInstance.request.fromIP = params.fromIP;
        dialogRef.componentInstance.request.toIP = params.toIP;

        return dialogRef.afterClosed();
    }


}
