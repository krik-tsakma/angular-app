﻿import { PagerResults, PagerRequest } from '../../../common/pager';

export interface IpRestriction {
    id: number;
    fromIP: string;
    toIP: string;
}

export interface IpRestrictionsResults extends PagerResults {
    results: IpRestriction[];
}

export type IpRestrictionsRequest = PagerRequest;
