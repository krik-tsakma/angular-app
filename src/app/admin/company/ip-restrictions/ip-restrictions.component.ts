﻿// FRAMEWORK
import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChildren } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { IpRestrictionsService } from './ip-restrictions.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';

// TYPES
import { IpRestriction, IpRestrictionsRequest } from './ip-restrictions-types';
import { Pager } from '../../../common/pager';
import { ConfirmationDialogParams } from '../../../shared/confirm-dialog/confirm-dialog-types';
import { TableColumnSetting, TableActionItem, TableActionOptions, TableCellFormatOptions } from '../../../shared/data-table/data-table.types';


@Component({
    selector: 'ip-restrictions',
    templateUrl: 'ip-restrictions.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['ip-restrictions.less']
})


export class IpRestrictionsComponent implements OnInit, OnDestroy {

    public ips: IpRestriction[];

    public loading: boolean;
    public ipsData: IpRestriction[] = [] as IpRestriction[];
    public ipRestrictionsListTableSettings: TableColumnSetting[];
    public request: IpRestrictionsRequest = {} as IpRestrictionsRequest;
    public pager: Pager = new Pager();
    public message: string;
    public ipRestrictionsTotalPages: number;

    private serviceSubscriber: any;

    constructor(
        private service: IpRestrictionsService,
        private confirmDialogService: ConfirmDialogService,
        private unsubscribe: UnsubscribeService) {

        this.ipRestrictionsListTableSettings = [           
            {
                primaryKey: 'fromIP',
                header: 'company.ip_restrictions.from_ip',
                format: TableCellFormatOptions.string,
            },
            {
                primaryKey: 'toIP',
                header: 'company.ip_restrictions.to_ip',
                format: TableCellFormatOptions.string,
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete],
            }
        ];

    }

    public ngOnInit() {
        this.get(true);
    }  


    public ngOnDestroy() {
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
    }


    // =========================
    // TABLE
    // =========================

    // fetch data
    public loadMore() {
        if (this.pager.pageNumber < this.pager.totalPages && this.loading === false) {
            this.pager.addPage();
            this.get(false);
        }
    }


    public deleteDialog(req?: ConfirmationDialogParams): Observable<boolean> {
        if (!req) {
            req = {
                title: 'form.actions.delete',
                message: 'form.warnings.are_you_sure',
                submitButtonTitle: 'form.actions.delete'
            } as ConfirmationDialogParams;
        }

        return this.confirmDialogService
            .confirm(req)
            .pipe(
                map((result) => {
                    if (result === 'yes') {
                        return true;
                    }
                    return false;
                })
            );
    }


    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }
        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.edit:
                this.service.OpenDialog(item.record);
                break;
            case TableActionOptions.delete:
                this.deleteDialog().subscribe((response) => {
                    if (response === true) {                       
                        this.service.deleteIpRestriction(id).subscribe((res) => {
                            this.get(true);
                        });                        
                    }
                });
                break;
            default:
                break;
        }
    }


    // Add new restriction
    public addNew() {
        const restriction: IpRestriction = {} as IpRestriction;

        this.service.OpenDialog(restriction);
    }


    // =========================
    // PRIVATE
    // =========================

    // Fetch driver summary data
    private get(resetPageNumber: boolean) {
        if (this.loading) {
            return;
        }

        this.message = '';
        this.loading = true;
        this.request.pageSize = this.pager.pageSize;

        if (resetPageNumber) {
            this.request.pageSize = this.ipsData.length === 0
                ? 20
                : this.ipsData.length;
            this.pager.pageNumber = 1;
        }

        this.request.pageNumber = this.pager.pageNumber;

        // first stop currently executing requests
        this.unsubscribe.removeSubscription(this.serviceSubscriber);

       
        // then start the new request
        this.serviceSubscriber = this.service
            .getIpRestrictions(this.request)
            .subscribe(
                (response) => {
                    this.loading = false;
                    if (resetPageNumber === true) {
                        this.ipsData = response && response.results
                            ? response.results
                            : [];
                    } else {
                        this.ipsData = this.ipsData.concat(response.results);                          
                    }

                    if (!this.ipsData || this.ipsData.length === 0) {
                        this.message = 'data.empty_records';
                        return;
                    }

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;

                    if (!this.ipRestrictionsTotalPages) {
                        this.ipRestrictionsTotalPages = this.pager.totalPages;
                    }
                },
                (err) => {
                    this.loading = false;
                    this.ipsData = [];
                    this.message = 'data.error';
                }
            );
        return;
    }

}
