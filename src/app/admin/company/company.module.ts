// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { CompanyRoutingModule } from './company-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { SettingsListModule } from '../settings-list/settings-list.module';
import { CulturesListModule } from '../../common/cultures-list/cultures-list.module';
import { AccessRightsModule } from '../../common/access-rights/access-rights.module';
import { TimezonesListModule } from '../../common/timezones-list/timezones-list.module';
import { ColorPickerModule } from '../../common/color-picker/color-picker.module';
import { NotificationPoliciesModule } from './notification-policies/notification-policies.module';
import { PasswordPoliciesModule } from './password-policies/password-policies.module';

// COMPONENTS
import { EditCompanyComponent } from './edit-company.component';
import { EditCompanySettingsComponent } from './settings/edit-company-settings.component';
import { EditCompanyAppearenceSettingsComponent } from './appearance/appearance-settings.component';
import { EditCompanyMapSettingsComponent } from './map-settings/edit-company-map-settings.component';
import { EditCompanyEventPropertiesComponent } from './events/edit-events-properties.component';
import { EditCompanyEventPropertyComponent } from './events/edit/edit-event-property.component';
import { IpRestrictionsComponent } from './ip-restrictions/ip-restrictions.component';
import { EditIpRestrictionsComponent } from './ip-restrictions/edit/edit-ip-restrictions.component';

// SERVICES
import { CompanyService } from './company.service';
import { EventPropertiesService } from './events/event-properties.service';
import { IpRestrictionsService } from './ip-restrictions/ip-restrictions.service';
import { EditIpRestrictionsService } from './ip-restrictions/edit/edit-ip-restrictions.service';
import { EventStatusesService } from '../../common/events-statuses-list/events-statuses.service';
import { EventClassesService } from '../../common/events-classes-list/events-classes.service';
import { EventPrioritiesService } from '../../common/events-priorities-list/events-priorities.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/company/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        CompanyRoutingModule,
        AccessRightsModule,
        CulturesListModule,
        TimezonesListModule,
        SettingsListModule,
        ColorPickerModule,
        NotificationPoliciesModule,
        PasswordPoliciesModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        EditCompanyComponent,
        EditCompanySettingsComponent,
        EditCompanyAppearenceSettingsComponent,
        EditCompanyMapSettingsComponent,
        EditCompanyEventPropertiesComponent,
        EditCompanyEventPropertyComponent,
        IpRestrictionsComponent,
        EditIpRestrictionsComponent
    ],
    providers: [
        CompanyService,
        EventPropertiesService,
        EventStatusesService,
        EventClassesService,
        EventPrioritiesService,
        IpRestrictionsService,
        EditIpRestrictionsService
    ],
    entryComponents: [
        EditCompanyEventPropertyComponent,
        EditIpRestrictionsComponent
    ]
})
export class CompanyModule { }
