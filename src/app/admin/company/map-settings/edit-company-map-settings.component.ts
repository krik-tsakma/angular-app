// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { CompanyService } from '../company.service';
import { AdminLabelService } from '../../admin-label.service';
import { UserOptionsService } from '../../../shared/user-options/user-options.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES
import { CompanyMapSettings, CompanyMapSettingsUpdateCodeOptions, CompanySessionName } from '../company.types';

@Component({
    selector: 'edit-company-map-settings',
    templateUrl: 'edit-company-map-settings.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-company-map-settings.less'],
})

export class EditCompanyMapSettingsComponent extends AdminBaseComponent implements OnInit {
    public action: string;
    public loading: boolean;
    public company: CompanyMapSettings = {} as CompanyMapSettings;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        public userOptions: UserOptionsService,
        private service: CompanyService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        this.labelService.setSessionItemName(CompanySessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.get();
    }


    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: {}, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

     
        this.serviceSubscriber = this.service
            .updateMapSettings(this.company)
            .subscribe((res: CompanyMapSettingsUpdateCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private get() {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getMapSettings()
            .subscribe((res: CompanyMapSettings) => {
                this.loading = false;
                this.company = res;
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }
}
