
// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

// PIPES 
import { StringFormatPipe } from '../../../shared/pipes/stringFormat.pipe';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { CompanyService } from '../company.service';
import { AdminLabelService } from '../../admin-label.service';
import { Config } from '../../../config/config';

// TYPES
import { MeasurementSystems, VehicleLabels, DriverLabels } from '../../../common/entities/entity-types';
import { CompanySettings, CompanySettingsUpdateCodeOptions, CompanySessionName } from '../company.types';

@Component({
    selector: 'edit-company-settings',
    templateUrl: 'edit-company-settings.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-company-settings.less'],
})

export class EditCompanySettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public action: string;
    public loading: boolean;
    public loadingUserRoles: boolean;
    public vehicleLabels = VehicleLabels;
    public driverLabels = DriverLabels;
    public measurementSystems = MeasurementSystems;
    
    public company: CompanySettings = {} as CompanySettings;

    constructor(
        protected previousRouteService: PreviousRouteService,        
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private service: CompanyService,
        private stringFormatPipe: StringFormatPipe
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        this.labelService.setSessionItemName(CompanySessionName);

        this.vehicleLabels.forEach((x) => {
            translator.get(x.term).subscribe((t) => {
                x.name = t;
            });
        });
        this.driverLabels.forEach((x) => {
            translator.get(x.term).subscribe((t) => {
                x.name = t;
            });
        });
        this.measurementSystems.forEach((x) => {
            translator.get(x.term).subscribe((t) => {
                x.name = t;
            });
        });
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.get();
    }


    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: {}, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

     
        this.serviceSubscriber = this.service
            .update(this.company)
            .subscribe((res: CompanySettingsUpdateCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.labelService.set(this.company.name);
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    const message = this.getUpdateResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private get() {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .get()
            .subscribe((res: CompanySettings) => {
                this.loading = false;
                this.company = res;
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }


    // =========================
    // RESULTS
    // =========================

    private getUpdateResult(code: CompanySettingsUpdateCodeOptions): string {
        let message = '';
        
        switch (code) {
            case CompanySettingsUpdateCodeOptions.NameRequired:
                const required = this.translator.instant('form.errors.required');
                const name = this.translator.instant('company.name');
                message = this.stringFormatPipe.transform(required, [name]);
                break;
            case CompanySettingsUpdateCodeOptions.NameAlreadyExists:
                message = 'company.errors.name_exists';
                break;
            case CompanySettingsUpdateCodeOptions.InvalidAccountCreationCodePrefix:
                message = 'company.errors.account_creation_code_prefix';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
