// FRAMEWORK
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';

// RXJS
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { CustomTranslateService } from '../shared/custom-translate.service';
import { SnackBarService } from '../shared/snackbar.service';
import { StoreManagerService } from '../shared/store-manager/store-manager.service';
import { UnsubscribeService } from '../shared/unsubscribe.service';
import { AdminLabelService } from './admin-label.service';
import { ConfirmDialogService } from '../shared/confirm-dialog/confirm-dialog.service';
import { PreviousRouteService } from '../core/previous-route/previous-route.service';
import { Config } from '../config/config';

// TYPES
import { ConfirmationDialogParams } from '../shared/confirm-dialog/confirm-dialog-types';


export class AdminBaseComponent implements OnInit {
    public skipLocChange = !this.configService.get('router');
    public serviceSubscriber: Subscription;
    public action: string;
    public message: string;

    constructor(                        
        protected translator: CustomTranslateService,
        protected previousRouteService: PreviousRouteService, 
        protected router: Router,             
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,    
        protected configService: Config,    
        protected snackBar?: SnackBarService,        
        protected storeManager?: StoreManagerService,        
        protected confirmDialogService?: ConfirmDialogService,                                     
    ) {
        // foo
    }
    

    public ngOnInit(): void {
        // foo        
    }
    
        
    // Click back arrow
    public back() {        
        this.router.navigate(this.previousRouteService.getPreviousUrl().split('/'), { skipLocationChange: this.skipLocChange });
    }


    public unsubscribeService(): void {
        this.unsubscriber.removeSubscription(this.serviceSubscriber);        
    }


    // Redirect to page for editing an existing entity, or adding a new one
    public manage(id?: number, labelName?: string) {
        if (id) {
            if (labelName) {
                this.labelService.set(labelName);
            }
            this.router.navigate([this.router.url , 'edit', id], { skipLocationChange: this.skipLocChange });
        } else {
            this.labelService.remove();
            this.router.navigate([this.router.url, 'add'], { skipLocationChange: this.skipLocChange });
        }
    }

    public openDialog(req?: ConfirmationDialogParams): Observable<boolean> {
        if (!req) {
            req = {
                title: 'form.actions.delete',
                message: 'form.warnings.are_you_sure',
                submitButtonTitle: 'form.actions.delete'
            } as ConfirmationDialogParams;
        }

        return this.confirmDialogService
                    .confirm(req)
                    .pipe(
                        map((result) => {
                            if (result === 'yes') {
                                return true;
                            } 
                            return false;
                        })
                    );
    }

    public getLabelEntity() {
        return this.labelService.get();
    }
}
