﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EnergyScoreComponent } from './energy-score.component';

import { AuthGuard } from '../../auth/authGuard';

const EnergyScoreRoutes: Routes = [
    {
        path: '',
        component: EnergyScoreComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(EnergyScoreRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class EnergyScoreRoutingModule { }
