// FRAMEWORK
import { Component, AfterViewInit, ViewEncapsulation, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

// SERVICES
import { CustomTranslateService } from '../../shared/custom-translate.service';
import { SnackBarService } from '../../shared/snackbar.service';
import { PreviousRouteService } from '../../core/previous-route/previous-route.service';
import { EnergyScoreService } from './energy-score.service';
import { UnsubscribeService } from '../../shared/unsubscribe.service';
import { Config } from '../../config/config';


// TYPES
import { EnergyScoreDef, EnergyScoreDefs, EnergyScoreResult, EnergyScorePercentage } from './energy-score-types';


// Transform translates with arguments function
const transform = (text: string, args: string[]): string => {
    if (!text) {
        return text;
    }
    args.forEach((arg, i) => {
        const pattern = new RegExp('\\{' + (i) + '\\}', 'g');
        text = text.replace(pattern, args[i]);
    });
    return text;
};


@Component({
    selector: 'energy-score',
    templateUrl: './energy-score.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./energy-score.less']
})

export class EnergyScoreComponent implements OnDestroy, AfterViewInit {
    public energyScoreDef: EnergyScoreDef[] = EnergyScoreDefs;
    public energyScoreForm: FormGroup;
    public formErrors: any = {
        scores: []
    };
    public loading: boolean = false;
    public isDefault: boolean;
    private energyScoreDefinition: EnergyScorePercentage[];
    private serviceSubscriber: any;
    private formSubscriber: any;
    private changeScoresSubscriber: any;
    private validationMessages: any;

    constructor(
        public router: Router,
        public snackBar: SnackBarService,
        public fb: FormBuilder,
        private configService: Config,
        private previousRouteService: PreviousRouteService,
        private translate: CustomTranslateService,
        private service: EnergyScoreService,
        private changeDetectionRef: ChangeDetectorRef,
        private unsubscribe: UnsubscribeService) {

            this.buildForm();

            this.energyScoreDef.forEach((score) => {
                this.addScores(score.value);
            });

    }



    public ngAfterViewInit() {

        this.translate.get([
            'energy_score.errors.scores.validateEachScoreA',
            'energy_score.errors.scores.validateEachScoreG',
            'energy_score.errors.scores.validateEachScore',
            'energy_score.errors.scores.required',
        ]).subscribe((res) => {
            this.validationMessages = {
                scores: []
            };
            this.energyScoreDef.forEach((score, index) => {
                let eachScoreTranslate: any;
                if (score.value === 7) {
                    eachScoreTranslate = transform(res['energy_score.errors.scores.validateEachScoreA'], [score.letter]);
                } else if (score.value === 0) {
                    eachScoreTranslate = null;
                } else if (score.value === 1) {
                    eachScoreTranslate = transform(res['energy_score.errors.scores.validateEachScoreG'], [
                        score.letter,
                        this.energyScoreDef[index + 1].letter
                    ]);
                } else {
                    eachScoreTranslate = transform(res['energy_score.errors.scores.validateEachScore'], [
                        score.letter,
                        this.energyScoreDef[index - 1].letter,
                        this.energyScoreDef[index + 1].letter,
                    ]);
                }
                this.validationMessages.scores.push({
                    percentage: {
                        required: transform(res['energy_score.errors.scores.required'], [score.letter]),
                        validateEachScore: eachScoreTranslate
                    }
                });
                this.formErrors.scores.push({ percentage: '' });
            });
        });


        const changeScores: any = this.energyScoreForm.get('scores');
        // ------------- check all percentages on every change in the array ------------- //
        this.changeScoresSubscriber =
            changeScores
                .valueChanges
                .pipe(
                    debounceTime(500),
                    distinctUntilChanged()
                )
                .subscribe((scores: EnergyScoreDef[]) => {
                    // Check if all controller are set
                    if (this.energyScoreDef.length === scores.length) {
                        scores.forEach((sc, index) => {
                            if (index > 0 && sc.percentage !== undefined && scores[index - 1].percentage !== undefined) {
                                if (scores[index - 1].percentage > sc.percentage) {
                                    if (!changeScores.at(index - 1).get('percentage').valid) {
                                        changeScores.at(index - 1).get('percentage').updateValueAndValidity();
                                    }
                                    if (!changeScores.at(index).get('percentage').valid) {
                                        changeScores.at(index).get('percentage').updateValueAndValidity();
                                    }
                                }
                            }
                        });
                    }
                });
        // ------------- END - check all percentages on every change in the array ------------- //

        this.get();

        // manually trigger change detection for the current component.
        // otherwise "Expression ___ has changed after it was checked" error
        this.changeDetectionRef.detectChanges();
    }


    public ngOnDestroy(): void {
        this.unsubscribe.removeSubscription([
            this.serviceSubscriber,
            this.formSubscriber,
            this.changeScoresSubscriber
        ]);
    }


    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }


    // ---- SAVE FUNCTION ---- //
    public save(event: Event) {
        event.preventDefault();
        this.loading = true;

        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.updateEnergyScoreDefinition()
            .then((response: EnergyScorePercentage[]) => {
                this.serviceSubscriber = this.service
                    .update(response)
                    .subscribe((res) => {
                        this.snackBar.open('form.save_success', 'form.actions.ok');
                        this.get();
                    },
                    (err) => {
                        this.loading = false;
                        if (err.ok === false || (err.status && err.status !== 200)) {
                            this.snackBar.open('form.errors.unknown', 'form.actions.close');
                        }
                    });
            })
            .catch((err) => {
                this.loading = false;
                if (err) {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            });
    }


    public restoreDefaults() {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.service
            .delete()
            .subscribe((result: any) => {
                this.loading = false;
                if (!result || result.length === 0) {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
                this.get();
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.close');
            });
    }

    // FormGroup Score Custom Validator
    public validateEachScore(c: FormControl) {
        if (c.parent) {
            const parentControlValue = c.parent.value.value;
            const score = this.energyScoreDef.find((energyScore) => {
                return energyScore.value === parentControlValue;
            });

            if (parentControlValue === 1) {
                if (c.value || c.value === 0) {
                    if ((c.root.value.scores[score.value + 1].percentage === '' ||
                            c.root.value.scores[score.value + 1].percentage > c.value)) {
                        return null;
                    }
                    return {
                        validateEachScore: {
                            valid: false
                        }
                    };
                }
            }

            if (parentControlValue > 1 && parentControlValue < 7) {
                if (c.value || c.value === 0) {
                    if ((c.root.value.scores[score.value - 1].percentage === '' ||
                        c.root.value.scores[score.value - 1].percentage < c.value) &&
                        (c.root.value.scores[score.value + 1].percentage === '' ||
                        c.root.value.scores[score.value + 1].percentage > c.value)) {
                        return null;
                    }
                    return {
                        validateEachScore: {
                            valid: false
                        }
                    };
                }
                return {
                    validateEachScore: {
                        valid: false
                    }
                };
            }
            if (parentControlValue === 7) {

                if (c.value || c.value === 0) {
                    if (c.root.value.scores[score.value - 1].percentage === '' ||
                        c.root.value.scores[score.value - 1].percentage < c.value) {
                        return null;
                    }
                    return {
                        validateEachScore: {
                            valid: false
                        }
                    };
                }
                return {
                    validateEachScore: {
                        valid: false
                    }
                };
            }

        }
    }

    private get() {
        this.loading = true;
        this.unsubscribe.removeSubscription(this.serviceSubscriber);
        this.serviceSubscriber = this.service
            .get()
            .subscribe((result: EnergyScoreResult) => {
                this.loading = false;
                if (!result || result.percentages.length === 0) {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
                this.isDefault = result.isDefault;
                const scores: any = this.energyScoreForm.get('scores');
                result.percentages.forEach((res: EnergyScoreDef) => {
                    if (res.value === 0) {
                        return;
                    }
                    scores.at(res.value).get('percentage').setValue(res.percentage);
                    scores.at(res.value).get('percentage').markAsTouched();
                    scores.at(res.value).get('percentage').markAsDirty();
                });
            },
            (err) => {
                this.loading = false;
                if (err) {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            });
    }


    // Update energy score
    private updateEnergyScoreDefinition(): Promise<EnergyScorePercentage[]> {
        return new Promise((resolve, reject) => {
            this.energyScoreDefinition = this.energyScoreForm.value.scores;

            this.energyScoreDefinition.forEach((score, index) => {
                if (score.value > 1 && score.percentage) {
                    if (score.percentage <= this.energyScoreDefinition[index - 1].percentage) {
                        reject('score error');
                    }
                }
            });
            resolve(this.energyScoreDefinition);
        });
    }


    // Everytime a form's element changes, onValueChanged function check validation options
    private onValueChanged(data?: any) {
        if (!this.energyScoreForm) {
            return;
        }
        const form: FormGroup = this.energyScoreForm;

        const check = (control, field, index?: number) => {
            if (control && control.dirty && !control.valid) {
                if (index) {
                    const messages = this.validationMessages[field][index];
                    for (const insideCTR in control.controls) {
                        if (control.controls[insideCTR].errors) {
                            for (const key in control.controls[insideCTR].errors) {
                                if (control.controls[insideCTR].errors.hasOwnProperty(key)) {
                                    this.formErrors[field][index][insideCTR] +=
                                        messages[insideCTR][key] + ' <br />';
                                }
                            }
                        }
                    }
                }
            }
        };

        for (const field in this.formErrors) {
            if (this.formErrors.hasOwnProperty(field)) {
                // clear previous error message (if any)
                let control: any;
                if (Array.isArray(this.formErrors[field])) {
                    this.formErrors[field].forEach((insideField, index: number) => {
                        for (const inField in insideField) {
                            if (insideField.hasOwnProperty(inField) && form.get(field)) {
                                control = form.get(field);
                                if (control.length) {
                                    control.controls.forEach((ctr, i: number) => {
                                        this.formErrors[field][i][inField] = '';
                                        check(ctr, field, i);
                                    });
                                }
                            }
                        }
                    });
                }
            }
        }
    }



    // Initialize Form and subscribe in valueChange stream
    // which calls onValueChanged method
    private buildForm(): void {

        this.energyScoreForm = this.fb.group({
            scores: this.fb.array([])
        });

        this.formSubscriber = this.energyScoreForm.valueChanges
            .subscribe((data: any) => {
                this.onValueChanged(data);
            });

        this.onValueChanged();
    }


    // SCORES ARRAY
    private initScores(val) {
        return this.fb.group({
            value: [val],
            percentage: [
                {
                    value: val === 0 ? null : '',
                    disabled: val === 0 ? true : false
                },
                Validators.compose([
                    Validators.required,
                    this.validateEachScore.bind(this)
                ])
            ]
        });
    }

    private addScores(value: number) {
        const inner: any = this.energyScoreForm.get('scores');
        inner.push(this.initScores(value));
    }
    // END - SCORELEVEL ARRAY
}
