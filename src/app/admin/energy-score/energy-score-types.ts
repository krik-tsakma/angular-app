﻿
export let EnergyScoreDefs: EnergyScoreDef[] = [
    {
        letter: 'H',
        formName: 'scoreH',
        value: 0
    },
    {
        letter: 'G',
        formName: 'scoreG',
        value: 1
    },
    {
        letter: 'F',
        formName: 'scoreF',
        value: 2
    },
    {
        letter: 'E',
        formName: 'scoreE',
        value: 3
    },
    {
        letter: 'D',
        formName: 'scoreD',
        value: 4
    },
    {
        letter: 'C',
        formName: 'scoreC',
        value: 5
    },
    {
        letter: 'Β',
        formName: 'scoreB',
        value: 6
    },
    {
        letter: 'Α',
        formName: 'scoreA',
        value: 7
    }
];

export interface EnergyScorePercentage {
    value: number;
    percentage?: number;
}

export interface EnergyScoreDef extends EnergyScorePercentage {
    letter: string;
    formName: string;
    max?: number;
    min?: number;
}

export interface EnergyScoreResult {
    isDefault: boolean;
    percentages: EnergyScorePercentage[];
}
