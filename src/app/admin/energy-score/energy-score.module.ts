﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { EnergyScoreRoutingModule } from './energy-score-routing.module';

// COMPONENTS
import { EnergyScoreComponent } from './energy-score.component';

// SERVICES
import { EnergyScoreService } from './energy-score.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/energy-score/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        EnergyScoreRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        EnergyScoreComponent
    ],
    providers: [
        EnergyScoreService
    ]
})
export class EnergyScoreModule { }
