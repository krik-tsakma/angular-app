﻿// FRAMEWORK
import { Injectable } from '@angular/core';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { EnergyScoreResult, EnergyScorePercentage } from './energy-score-types';

@Injectable()
export class EnergyScoreService {
    private baseURL: string;

    constructor(
        private authHttp: AuthHttp,
        private config: Config
    ) {
        this.baseURL = this.config.get('apiUrl') + '/api/EnergyScoreAdmin';
    }

    // Get energy score definition
    public get(): Observable<EnergyScoreResult> {
        return this.authHttp
                    .get(this.baseURL);
    }


    public update(energyScoreDefinition: EnergyScorePercentage[]): Observable<{}> {
        return this.authHttp
                    .put(this.baseURL, energyScoreDefinition);
    }

    public delete(): Observable<{}> {
        return this.authHttp
                    .delete(this.baseURL);
    }
} 

