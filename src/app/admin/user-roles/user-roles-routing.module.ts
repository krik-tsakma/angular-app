// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { ViewUserRolesComponent } from './view/view-user-roles.component';
import { EditUserRoleSettingsComponent } from './edit/settings/settings.component';

const UserRolesRoutes: Routes = [
    {
        path: '',
        component: ViewUserRolesComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditUserRoleSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-user-role.module').then((m) => m.EditUserRolesModule),
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }     
];

@NgModule({
    imports: [
        RouterModule.forChild(UserRolesRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [ ]
})
export class UserRolesRoutingModule { }
