import { UserRolesResult } from './../user-roles.types';
// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { StoreManagerService } from '../../../shared/store-manager/store-manager.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { UserRolesService } from '../user-roles.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { AdminLabelService } from '../../admin-label.service';
import { Config } from '../../../config/config';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// TYPES
import { SearchTermRequest, Pager } from '../../../common/pager';
import { StoreItems } from '../../../shared/store-manager/store-items';
import { 
    TableColumnSetting, TableActionItem, TableActionOptions, 
    TableColumnSortOrderOptions, TableColumnSortOptions
} from '../../../shared/data-table/data-table.types';
import { UserRolesResultItem, UserRoleDeleteOptions, UserRoleSessionName } from '../user-roles.types';

@Component({
    selector: 'view-user-roles',
    templateUrl: 'view-user-roles.html',
    encapsulation: ViewEncapsulation.None
})

export class ViewUserRolesComponent extends AdminBaseComponent implements OnInit {
    public roles: UserRolesResultItem[] = [];
    public searchTermControl = new FormControl();     
    public request: SearchTermRequest = {} as SearchTermRequest;
    public loading: boolean;
    public tableSettings: TableColumnSetting[];
    public pager: Pager = new Pager();
    
    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBarService: SnackBarService,
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,
        protected storeManager: StoreManagerService,
        protected configService: Config,
        private service: UserRolesService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBarService, storeManager, confirmDialogService);
        labelService.setSessionItemName(UserRoleSessionName);
        
        this.tableSettings =  [
            {
                primaryKey: 'name',
                header: 'user_roles.name',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete],
                actionsVisibilityFunction: this.showUDButtons
            }
        ];
    }


    public ngOnInit(): void {
        super.ngOnInit();
       
        const savedData = this.storeManager.getOption(StoreItems.userRoles) as SearchTermRequest;
        if (savedData) {
            this.request = savedData;
            this.searchTermControl.setValue(this.request.searchTerm);
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchTermControl.valueChanges
            .pipe(         
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing             
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    this.request.searchTerm = term;
                    this.get(true);
                    return of([]);
                })
            )
            .subscribe(); 

        this.get(true);
    }



    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.searchTermControl.setValue(null);
    }

    
    // ----------------
    // TABLE CALLBACKS
    // ----------------


    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }


    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const record = item.record as UserRolesResultItem;
        switch (item.action) {
            case TableActionOptions.edit: 
                this.manage(record.id, record.name);
                break;
            case TableActionOptions.delete: 
                this.openDialog()
                    .subscribe((response) => {
                        if (response === true) {
                            this.delete(record.id);
                        }
                    });
                break;
            default: 
                break;

        }
    }


    public showUDButtons(action: TableActionOptions, record: UserRolesResultItem): boolean {
        if (!record) {
            return false;
        }
        if (record.isDefaultSycadaAdmin === true) {
            return false;
        } else if (record.isDefaultDriver === true || record.isDefaultAdmin === true || record.isDefaultUser === true) {
            switch (action) {
                case TableActionOptions.edit:
                    return true;
                case TableActionOptions.delete:
                    return false;
            }
            return false;
        } 

        return true;
    }


    // ----------------
    // PRIVATE
    // ----------------

    // Fetch events data
    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;

        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.roles = new Array<UserRolesResultItem>();
        }   

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;

        // first stop currently executing requests
        this.unsubscribeService();

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.userRoles, this.request);
            
        // then start the new request
        this.serviceSubscriber = this.service
            .get(this.request)
            .subscribe((response: UserRolesResult) => {
                    this.loading = false;
                    this.roles = this.roles.concat(response.results);

                   
                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;  
                    
                    if (!this.roles || this.roles.length === 0) {
                        this.message = 'data.empty_records';
                    }

                },
                () => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }


    // ----------------
    // DELETE
    // ----------------
    private delete(id: number) {
        this.loading = true;

        // first stop currently executing requests
        this.unsubscribeService();
        
        this.serviceSubscriber = this.service
            .delete(id)
            .subscribe((res: UserRoleDeleteOptions) => {
                this.loading = false;
                if (res !== null) {
                    switch (res) {
                        case UserRoleDeleteOptions.DefaultRoleCannotBeRemoved:
                            this.snackBar.open('user_roles.errors.delete.default_role', 'form.actions.close');
                            break;
                        case UserRoleDeleteOptions.RoleAssignedToUsers:
                            this.snackBar.open('user_roles.errors.delete.role_assigned_to_users', 'form.actions.close');
                            break;
                    }
                } else {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get(true);
                }
            },
            () => {
                    this.loading = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                });
    }
}
