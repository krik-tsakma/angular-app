// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { UserRolesRoutingModule } from './user-roles-routing.module';
import { EditUserRoleSettingsModule } from './edit/settings/settings.module';
import { AccessRightsModule } from '../../common/access-rights/access-rights.module';
import { AssetGroupMembershipModule } from '../../common/asset-groups-membership/asset-group-membership.module';
import { QuickFilterColumnsModule } from '../../common/quick-filters/columns/quick-filter-columns.module';
import { EditDashboardWidgetsModule } from '../../dashboard/edit/edit-widget.module';
import { SettingsListModule } from '../settings-list/settings-list.module';

// COMPONENTS
import { ViewUserRolesComponent } from './view/view-user-roles.component';

// SERVICES
import { UserRolesService } from './user-roles.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(
        http,
        [
            './assets/i18n/admin/user-roles/',
            './assets/i18n/dashboard/'
        ],
        '.json',
        translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        AssetGroupMembershipModule,
        UserRolesRoutingModule,
        EditUserRoleSettingsModule,
        AccessRightsModule,
        QuickFilterColumnsModule,
        EditDashboardWidgetsModule,
        SettingsListModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewUserRolesComponent
    ],
    providers: [
        UserRolesService,
    ]
})
export class UserRolesModule { }
