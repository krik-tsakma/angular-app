import { PagerResults } from './../../common/pager';
import { UserRoleDashboard } from './user-roles.types';
import { WidgetTypes } from '../../dashboard/widgets/widget-types';
import { QuickFilter } from '../../common/quick-filters/quick-filters-types';
import { Module } from '../../auth/acl.types';
import { KeyName } from '../../common/key-name';
import { AssetGroupTypes } from '../asset-groups/asset-groups.types';

export const UserRoleSessionName: string = 'urn';


export interface UserRolesWidgetResultItem {
    id: number;
    name: string;
    type: WidgetTypes;
    allowUserToRemove: boolean;

}

export interface UserRolesResult extends PagerResults {
    results: UserRolesResultItem[];
}

export interface UserRolesResultItem extends KeyName {
    id: number;
    isDefaultRole: boolean;
    isDefaultDriver: boolean;
    isDefaultAdmin: boolean;
    isDefaultSycadaAdmin: boolean;
    isDefaultUser: boolean;
}

// ====================
// MAIN SETTINGS
// ====================
export interface UserRoleSettingsResult {
    id: number;
    name: string;
    isDriver: boolean;
    isAdmin: boolean;
}

export interface UserRoleCreateRequest {
    name: string;
    isDriver: boolean;
    isAdmin: boolean;
}

export enum UserRoleCreateCodeOptions {
    Created = 0,
    NameExists = 1,
    NameNotSpecified = 2,
}

// ====================
// SET NAME
// ====================

export interface UserRoleSetNameRequest {
    id: number;
    name: string;
}

export enum UserRoleSetNameCodeOptions {
    Set = 0,
    RoleNotFound = 1,
    NameExists = 2,
    NameNotSpecified = 3
}

// ====================
// GROUP MEMBERSHIP
// ====================

export interface UserRoleGroupMembership {
    roleID: number;
    groupIDs: number[];
    groupType: AssetGroupTypes;
}

export enum UserRoleGroupMembershipSetCodeOptions {
    Set = 0,
    RoleNotFound = 1,
}

// ====================
// ACCESS RIGHTS
// ====================

export interface UserRoleAccessRightsRequest {
    id: number;
    rights: Module[];
}

export enum UserRoleAccessRightsUpdateCodeOptions {
    Set = 0,
    RoleNotFound = 1,
    AccessRightsExceedingThoseOfCompany = 2,
}

// ====================
// DASHBOARD
// ====================
export interface UserRoleDashboardWidgetTypes extends KeyName {
    checked: boolean;
}

export interface UserRoleDashboard extends UserRoleDashboardResult {
    id: number;
}

export interface UserRoleDashboardResult {
    allowedWidgetTypes: number[];
    isDriverRole: boolean;
}

// ====================
// QUICK FILTERS
// ====================

export interface UserRoleQuickFilter {
    roleID: number;
    quickFilter: QuickFilter;
}

export enum UserRoleDeleteOptions {
    Removed,
    DefaultRoleCannotBeRemoved,
    RoleAssignedToUsers
}

// ====================
// DATA ACCESS
// ====================

export interface UserRoleDataAccessRequest {
    id: number;
    allowAccessToCompanyStats: boolean;
    allowAccessToVehicleTypeStats: boolean;
    hideTripsAndPositionsOlderThan: number;
}
