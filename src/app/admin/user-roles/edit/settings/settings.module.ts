﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';

// COMPONENTS
import { EditUserRoleSettingsComponent } from './settings.component';


@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        EditUserRoleSettingsComponent
    ],
    exports: [
        EditUserRoleSettingsComponent
    ]
})
export class EditUserRoleSettingsModule {}
