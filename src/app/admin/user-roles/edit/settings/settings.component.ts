// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { UserRolesService } from '../../user-roles.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// PIPES
import { StringFormatPipe } from '../../../../shared/pipes/stringFormat.pipe';

// TYPES
import { AssetGroupTypes } from '../../../asset-groups/asset-groups.types';
import {  
    UserRoleSetNameRequest, 
    UserRoleCreateRequest, 
    UserRoleSettingsResult, 
    UserRolesResultItem,
    UserRoleSetNameCodeOptions,
    UserRoleCreateCodeOptions,
    UserRoleSessionName
} from '../../user-roles.types';
import { config } from 'rxjs';

@Component({
    selector: 'edit-user-role-settings',
    templateUrl: 'settings.html',
    encapsulation: ViewEncapsulation.None
})

export class EditUserRoleSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;

    public action: string;
    public loading: boolean;
    public groupTypes: any = AssetGroupTypes;
    public role: UserRoleSettingsResult = {} as UserRoleSettingsResult;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected router: Router,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: UserRolesService,
        private stringFormatPipe: StringFormatPipe
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar);
        labelService.setSessionItemName(UserRoleSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                id: number;
            }) => { 
                this.action = params.id
                            ? 'form.actions.edit'
                            : 'form.actions.create';
               
                if (params.id) {
                    this.get(params.id);
                }
            });
    }

    // =========================
    // EVENTS
    // =========================

    public onIsDriverChange() {
        if (this.role.isDriver === true) {
            this.role.isAdmin = false;
        }
    }

    public onIsAdminChange() {
        if (this.role.isAdmin === true) {
            this.role.isDriver = false;
        }
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: UserRolesResultItem, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is create
        if (typeof(this.role.id) === 'undefined' || this.role.id === 0) {
            const req = {
                name: this.role.name,
                isDriver: this.role.isDriver,
                isAdmin: this.role.isAdmin
            } as UserRoleCreateRequest;
            this.serviceSubscriber = this.service
                .create(this.role)
                .subscribe((res?: UserRoleCreateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {                        
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const message = this.getCreateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else { 
            // This is an update
            const req = {
                id: this.role.id,
                name: this.role.name
            } as UserRoleSetNameRequest;
            this.serviceSubscriber = this.service
                .updateSettings(req)
                .subscribe((res?: UserRoleSetNameCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.labelService.set(this.role.name);
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const message = this.getUpdateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        }
    }


    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getSettingsByID(id)
            .subscribe((res: UserRoleSettingsResult) => {
                this.loading = false;
                this.role = res;
            },
            (err) => {
                this.loading = false;
                this.message = 'data.error';
            }
        );
    }

    // =========================
    // RESULTS
    // =========================
    private getCreateResult(code: UserRoleCreateCodeOptions): string {
        let message = '';
        switch (code) {
            case UserRoleCreateCodeOptions.NameExists:
                message = 'form.errors.name_exists';
                break;
            case UserRoleCreateCodeOptions.NameNotSpecified:
                const required = this.translator.instant('form.errors.required');
                const name = this.translator.instant('user_roles.name');
                message = this.stringFormatPipe.transform(required, [name]);
                break;
        }
        return message;
    }

    private getUpdateResult(code: UserRoleSetNameCodeOptions): string {
        let message = '';
        switch (code) {
            case UserRoleSetNameCodeOptions.NameExists:
                message = 'form.errors.name_exists';
                break;
            case UserRoleSetNameCodeOptions.NameNotSpecified:
                const required = this.translator.instant('form.errors.required');
                const name = this.translator.instant('user_roles.name');
                message = this.stringFormatPipe.transform(required, [name]);
                break;
            case UserRoleSetNameCodeOptions.RoleNotFound:
                message = 'form.errors.not_found';
                break;
        }
        return message;
    }
}
