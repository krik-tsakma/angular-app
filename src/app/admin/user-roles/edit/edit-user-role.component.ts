// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { AdminLabelService } from '../../admin-label.service';
import { AuthService } from '../../../auth/authService';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES 
import { SettingList } from '../../settings-list/settings-list.types';
import { UserRoleSessionName } from '../user-roles.types';
import { ModuleOptions, AdminPropertyOptions } from '../../../auth/acl.types';

@Component({
    selector: 'edit-user-role',
    templateUrl: 'edit-user-role.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditUserRoleComponent {
   public options: SettingList[] = [
        {
            title: 'user_roles.category.basic',
            items: [
                {
                    option: 'user_roles.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
                {
                    option: 'user_roles.category.basic.rights',
                    params: ['rights'],
                    icon: 'lock'
                }
            ]
        },
        {
            title: 'user_roles.category.group_membership',
            items: [
                {
                    option: 'assets.groups.vehicle',
                    params: ['groups', 'vehicle'],
                    icon: 'directions_car'
                },
                {
                    option: 'assets.groups.driver',
                    params: ['groups', 'driver'],
                    icon: 'person'
                },
                {
                    option: 'assets.groups.location',
                    params: ['groups', 'location'],
                    icon: 'place'
                },
                {
                    option: 'assets.groups.charge_point',
                    params: ['groups', 'charge-point'],
                    icon: 'power',
                    allowed: this.authService.userHasAccess(ModuleOptions.Admin, AdminPropertyOptions.ChargePoints)
                }
            ]
        },
        {
            title: 'user_roles.category.dashboard',
            items: [
                {
                    option: 'user_roles.category.dashboard.settings',
                    params: ['dashboard'],
                    icon: 'dashboard'
                }
            ]
        },
        {
            title: 'user_roles.category.quick_filters',
            items: [
                {
                    option: 'user_roles.category.quick_filters.trips',
                    params: ['quick-filters', 'trips'],
                    icon: 'filter_list'
                },
                {
                    option: 'user_roles.category.quick_filters.followup',
                    params: ['quick-filters', 'followup'],
                    icon: 'filter_list'
                },
            ]
        },
        // {
        //     title: 'user_roles.category.data_access',
        //     items: [
        //         {
        //             option: 'user_roles.category.data_access.customize',
        //             params: ['data'],
        //             icon: 'visibility'
        //         }
        //     ]
        // },
    ];

    constructor(public labelService: AdminLabelService,
                private router: Router,
                private previousRouteService: PreviousRouteService,
                private authService: AuthService,
                private configService: Config) {
        labelService.setSessionItemName(UserRoleSessionName);
    }

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
