// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { SnackBarService } from '../../../../shared/snackbar.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { UserRolesService } from '../../user-roles.service';
import { PreviewWidgetService } from '../../../../dashboard/edit/preview-widget.service';
import { WidgetsService } from '../../../../dashboard/dashboard.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import {
    UserRoleDashboard,
    UserRolesWidgetResultItem,
    UserRoleDashboardWidgetTypes,
    UserRoleSessionName,
    UserRoleDashboardResult
} from '../../user-roles.types';
import { BaseWidget, WidgetTypes, WidgetsResults, WidgetTypesAclMapping } from '../../../../dashboard/widgets/widget-types';
import {
    TableColumnSetting,
    TableCellFormatOptions,
    TableActionOptions,
    TableActionItem
} from '../../../../shared/data-table/data-table.types';

@Component({
    selector: 'edit-user-role-dashboard',
    templateUrl: 'dashboard.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['dashboard.less']
})

export class EditUserRoleDashboardComponent extends AdminBaseComponent implements OnInit {
    public action: string;
    public loading: boolean;
    public role: UserRoleDashboard = {} as UserRoleDashboard;

    // dashboard
    public loadingWidgets: boolean;
    public demoWidget: any = {} as BaseWidget;
    public dashboardWidgets: UserRolesWidgetResultItem[] = [];
    public dashboardWidgetsTableSettings: TableColumnSetting[];
    public dashboardWidgetsEditMode: boolean = false;
    public selectedWidgetType = -1;
    public selectedWidgetID?: number;
    public widgetTypes = WidgetTypes;
    public dashboardWidgetOptions: UserRoleDashboardWidgetTypes[] = [
        {
            id: -1,
            name: 'select',
            term: 'form.actions.select',
            enabled: true,
            checked: false,
        },
        {
            id: WidgetTypes.Chart,
            name: 'chart',
            term: 'dashboard.edit_widget.chart',
            enabled: true,
            checked: false,
        },
        {
            id: WidgetTypes.Summary,
            name: 'summary',
            term: 'dashboard.edit_widget.summary',
            enabled: true,
            checked: false,
        },
        {
            id: WidgetTypes.Ranking,
            name: 'ranking',
            term: 'dashboard.edit_widget.ranking',
            enabled: true,
            checked: false,
        },
        {
            id: WidgetTypes.Events,
            name: 'events',
            term: 'dashboard.edit_widget.events',
            enabled: true,
            checked: false,
        }
    ];

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected confirmDialogService: ConfirmDialogService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: UserRolesService,
        private widgetsService: WidgetsService,
        private previewWidget: PreviewWidgetService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar, null, confirmDialogService);
        labelService.setSessionItemName(UserRoleSessionName);

        this.dashboardWidgetsTableSettings =  [
            {
                primaryKey: 'name',
                header: 'dashboard.edit_widget.widget_title',
            },
            {
                primaryKey: 'type',
                header: 'dashboard.edit_widget.widget_type',
                format: TableCellFormatOptions.custom,
                formatFunction: (item: UserRolesWidgetResultItem) => {
                    switch (item.type) {
                        case WidgetTypes.Chart: {
                            return this.translator.instant('dashboard.edit_widget.chart');
                        }
                        case WidgetTypes.Summary: {
                            return this.translator.instant('dashboard.edit_widget.summary');
                        }
                        case WidgetTypes.Ranking: {
                            return this.translator.instant('dashboard.edit_widget.ranking');
                        }
                        case WidgetTypes.Events: {
                            return this.translator.instant('dashboard.edit_widget.events');
                        }
                    }
                },
            },
            {
                primaryKey: 'allowUserToRemove',
                header: 'dashboard.edit_widget.allow_edit_remove',
                format: TableCellFormatOptions.checkIcon,
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete]
            }
        ];
        this.dashboardWidgetOptions.forEach((w) => {
            this.translator
                .get(w.term)
                .subscribe((t) => {
                    w.name = t;
                });
        });

        // subscribe to widget changes
        this.previewWidget
            .changedWidget$
            .subscribe((item: any) => {
                if (item != null) {
                    this.demoWidget =  Object.assign({}, item);
                }
            });
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => {
                this.action = params.editMode === 'edit'
                            ? 'form.actions.edit'
                            : 'form.actions.create';

                if (params.id) {
                    this.role.id = params.id;
                    this.get(params.id);
                }
            });
    }

    // =========================
    // EVENTS
    // =========================

    public onToggleEditingDashboardWidgetView() {
        this.dashboardWidgetsEditMode = !this.dashboardWidgetsEditMode;
        if (this.dashboardWidgetsEditMode === false) {
            this.selectedWidgetID = null;
            this.selectedWidgetType = -1;
            this.getWidgets();
        }
    }

    public onManageDashboardWidget(item: TableActionItem) {
        if (!item) {
            return;
        }
        const record = item.record as UserRolesWidgetResultItem;
        switch (item.action) {
            case TableActionOptions.edit:
                this.selectedWidgetID = record.id;
                this.selectedWidgetType = record.type;
                this.onToggleEditingDashboardWidgetView();
                break;
            case TableActionOptions.delete:
                this.openDialog()
                    .subscribe(() => {
                        this.deleteWidget(record.id);
                    });
                break;
        }
    }

    // =========================
    // DATA
    // =========================

    public setAllowedWidgetTypes() {

        this.loading = true;
        this.unsubscribeService();

        this.role.allowedWidgetTypes = [];
        this.dashboardWidgetOptions.forEach((x) => {
            if (x.checked === true) {
                const flagValue = WidgetTypesAclMapping.GetForWidgetType(x.id);
                this.role.allowedWidgetTypes.push(flagValue);
            }
        });

        // This is an update
        this.serviceSubscriber = this.service
            .updateDashboard(this.role)
            .subscribe((res?: number) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
    }

    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getDashboardByID(id)
            .subscribe((res: UserRoleDashboardResult) => {
                this.loading = false;
                this.role.isDriverRole = res.isDriverRole;
                this.role.allowedWidgetTypes = res.allowedWidgetTypes;
                this.role.allowedWidgetTypes.forEach((w) => {
                    this.dashboardWidgetOptions.forEach((y) => {
                        const flagValue = WidgetTypesAclMapping.GetForWidgetType(y.id);
                        if (w === flagValue) {
                            y.checked = true;
                        }
                    });
                });
                this.getWidgets();
            },
            (err) => {
                this.loading = false;
                this.message = 'data.error';
            }
        );
    }

    private getWidgets() {
        this.loadingWidgets = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.widgetsService
            .get(this.role.id)
            .subscribe((data: WidgetsResults) => {
                this.loadingWidgets = false;
                this.dashboardWidgets = data.items.map((w) => {
                    return {
                        id: w.instance.id,
                        name: w.instance.title,
                        allowUserToRemove: w.instance.allowUserToRemove,
                        type: w.type
                    } as UserRolesWidgetResultItem;
                });
            },
            (error) => {
                this.loadingWidgets = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
        });
    }

    private deleteWidget(id: number) {
        this.loadingWidgets = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.widgetsService
            .delete(id, this.role.id)
            .subscribe((status: any) => {
                this.loadingWidgets = false;
                if (status === 200) {
                    this.getWidgets();
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (error) => {
                this.loadingWidgets = false;
                if (error.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
        });
    }

}
