// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { UserRolesService } from '../../user-roles.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import { QuickFiltersEntities, QuickFilter } from '../../../../common/quick-filters/quick-filters-types';
import { QuickFiltersColumnsChangeItem } from '../../../../common/quick-filters/columns/quick-filter-column.types';
import { UserRoleQuickFilter, UserRoleSessionName } from '../../user-roles.types';
import { EditQuickFilterResults } from '../../../../common/quick-filters/quick-filters-types';

@Component({
    selector: 'edit-user-role-quick-filters',
    templateUrl: 'quick-filters.html',
    encapsulation: ViewEncapsulation.None
})

export class EditUserRoleQuickFiltersComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public role: UserRoleQuickFilter = {} as UserRoleQuickFilter;
    public quickFilterEntities = QuickFiltersEntities;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: UserRolesService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar);
        labelService.setSessionItemName(UserRoleSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
                entityID: number;
            }) => { 
                if (params.id) {
                    this.activatedRoute
                        .data
                        .subscribe((data) => {
                            this.role.roleID = params.id;
                            this.get(params.id, data.entityID);
                        });
                }
            });
    }


    // ===============================
    // QUICK FILTER COLUMNS CALLBACKS
    // ===============================

    public onColumnSelection(item: QuickFiltersColumnsChangeItem) {
        this.role.quickFilter.scoreIDs = item.scoreIDs;
        this.role.quickFilter.propertyIDs = item.propertyIDs;
    }


    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: UserRoleQuickFilter, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is an update
        this.serviceSubscriber = this.service
            .updateQuickFilters(this.role)
            .subscribe((res?: EditQuickFilterResults) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    private get(id: number, entitID: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getQuickFiltersByID(id, entitID)
            .subscribe((res: QuickFilter) => {
                this.loading = false;
                this.role.quickFilter = res;
            },
            (err) => {
                this.loading = false;
                this.message = 'data.error';
            }
        );
    }
}
