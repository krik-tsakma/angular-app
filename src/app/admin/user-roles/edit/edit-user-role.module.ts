﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { EditUserRolesRoutingModule } from './edit-user-role-routing.module';
import { EditUserRoleSettingsModule } from './settings/settings.module';
import { AccessRightsModule } from '../../../common/access-rights/access-rights.module';
import { AssetGroupMembershipModule } from '../../../common/asset-groups-membership/asset-group-membership.module';
import { QuickFilterColumnsModule } from '../../../common/quick-filters/columns/quick-filter-columns.module';
import { EditDashboardWidgetsModule } from '../../../dashboard/edit/edit-widget.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';

// COMPONENTS
import { EditUserRoleComponent } from './edit-user-role.component';
import { EditUserRoleDashboardComponent } from './dashboard/dashboard.component';
import { EditUserRoleGroupMembershipComponent } from './group-membership/group-membership.component';
import { EditUserRoleQuickFiltersComponent } from './quick-filters/quick-filters.component';
import { EditUserRoleRightsComponent } from './access-rights/edit-rights.component';
import { EditUserRoleDataAccessComponent } from './data-access/data-access.component';

// SERVICES
import { UserRolesService } from '../user-roles.service';

@NgModule({
    imports: [
        SharedModule,
        AssetGroupMembershipModule,
        EditUserRolesRoutingModule,
        EditUserRoleSettingsModule,
        AccessRightsModule,
        QuickFilterColumnsModule,
        EditDashboardWidgetsModule,
        SettingsListModule,
    ],
    declarations: [
        EditUserRoleComponent,
        EditUserRoleDashboardComponent,
        EditUserRoleGroupMembershipComponent,
        EditUserRoleQuickFiltersComponent,
        EditUserRoleRightsComponent,
        EditUserRoleDataAccessComponent
    ],
    providers: [
        UserRolesService,
    ]
})
export class EditUserRolesModule { }
