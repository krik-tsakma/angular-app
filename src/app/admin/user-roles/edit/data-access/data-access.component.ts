// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { UserRolesService } from '../../user-roles.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import { UserRoleDataAccessRequest, UserRoleSessionName } from '../../user-roles.types';
import { Module } from '../../../../auth/acl.types';
import { config } from 'rxjs';

@Component({
    selector: 'edit-user-role-data-access',
    templateUrl: 'data-access.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditUserRoleDataAccessComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public action: string;
    public loading: boolean;
    public role: UserRoleDataAccessRequest = {} as UserRoleDataAccessRequest;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected router: Router,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: UserRolesService
        
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(UserRoleSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
            if (params.id) {
                    this.role.id = params.id;
                    this.get(params.id);
                }
            });
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: Module[], valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is an update
        this.serviceSubscriber = this.service
            .updateDataAccess(this.role)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getDataAccessByID(id)
            .subscribe((res: UserRoleDataAccessRequest) => {
                this.loading = false;
                this.role = res;
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }

    // =========================
    // RESULTS
    // =========================
    // private getCreateResult(code: UserRoleAccessRightsUpdateCodeOptions): string {
    //     let message = '';
    //     switch (code) {
    //         case UserRoleAccessRightsUpdateCodeOptions.RoleNotFound:
    //             message = 'form.errors.not_found';
    //             break;
    //         case UserRoleAccessRightsUpdateCodeOptions.AccessRightsExceedingThoseOfCompany:
    //             message = 'user_roles.errors.rights_exceeding_role';
    //             break;
    //     }
    //     return message;
    // }
}
