﻿
// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';

// COMPONENTS
import { EditUserRoleComponent } from './edit-user-role.component';
import { EditUserRoleSettingsComponent } from './settings/settings.component';
import { EditUserRoleDashboardComponent } from './dashboard/dashboard.component';
import { EditUserRoleGroupMembershipComponent } from './group-membership/group-membership.component';
import { EditUserRoleQuickFiltersComponent } from './quick-filters/quick-filters.component';
import { EditUserRoleRightsComponent } from './access-rights/edit-rights.component';
import { EditUserRoleDataAccessComponent } from './data-access/data-access.component';

// TYPES
import { AssetGroupTypes } from '../../asset-groups/asset-groups.types';
import { QuickFiltersEntities } from '../../../common/quick-filters/quick-filters-types';

const EditUserRolesRoutes: Routes = [
    {
        path: '',
        component: EditUserRoleComponent,
        canActivate: [AuthGuard]
    },       
    {
        path: 'settings',
        component: EditUserRoleSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'user_roles.category.basic.settings'
        }
    },
    {
        path: 'rights',
        component: EditUserRoleRightsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'user_roles.category.basic.rights'
        }
    },
    {
        path: 'dashboard',
        component: EditUserRoleDashboardComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'user_roles.category.dashboard.settings'
        }
    },
    {
        path: 'groups/vehicle',
        component: EditUserRoleGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.groups.vehicle',
            groupType: AssetGroupTypes.Vehicle
        }
    },
    {
        path: 'groups/driver',
        component: EditUserRoleGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.groups.driver',
            groupType: AssetGroupTypes.Driver
        }
    },
    {
        path: 'groups/location',
        component: EditUserRoleGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.groups.location',
            groupType: AssetGroupTypes.Location
        }
    },
    {
        path: 'groups/charge-point',
        component: EditUserRoleGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.groups.charge_point',
            groupType: AssetGroupTypes.ChargePoint
        }
    },
    {
        path: 'quick-filters/followup',
        component: EditUserRoleQuickFiltersComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'user_roles.category.quick_filters.followup',
            entityID: QuickFiltersEntities.FollowUp
        }
    },
    {
        path: 'quick-filters/trips',
        component: EditUserRoleQuickFiltersComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'user_roles.category.quick_filters.trips',
            entityID: QuickFiltersEntities.Trips
        }
    },
    {
        path: 'data',
        component: EditUserRoleDataAccessComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'user_roles.category.data_access.customize',
        }
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(EditUserRolesRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [ ]
})
export class EditUserRolesRoutingModule { }
