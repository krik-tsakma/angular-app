// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';
import { ConfirmDialogService } from './../../../../shared/confirm-dialog/confirm-dialog.service';

// SERVICES
import { UserRolesService } from '../../user-roles.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import { UserRoleAccessRightsRequest, UserRoleAccessRightsUpdateCodeOptions, UserRoleSessionName } from '../../user-roles.types';
import { Module } from '../../../../auth/acl.types';
import { ConfirmationDialogParams } from './../../../../shared/confirm-dialog/confirm-dialog-types';

@Component({
    selector: 'edit-user-role-rights',
    templateUrl: 'edit-rights.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditUserRoleRightsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public action: string;
    public loading: boolean;
    public role: UserRoleAccessRightsRequest = {} as UserRoleAccessRightsRequest;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected confirmDialogService: ConfirmDialogService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: UserRolesService
        
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(UserRoleSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
            if (params.id) {
                    this.role.id = params.id;
                    this.get(params.id);
                }
            });
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: Module[], valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }

        this.openDialog({
                title: 'form.actions.save',
                message: 'user_roles.rights.warning',
                submitButtonTitle: 'form.actions.continue'
            } as ConfirmationDialogParams)
            .subscribe((response) => {
                if (response === true) {
                    this.save();
                }
            });
    }

    private save() {
        this.loading = true;
        this.unsubscribeService();

        // This is an update
        this.serviceSubscriber = this.service
            .updateAccessRights(this.role)
            .subscribe((res: UserRoleAccessRightsUpdateCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getRightsByID(id)
            .subscribe((res: Module[]) => {
                this.loading = false;
                this.role.rights = res;
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('data.error', 'form.actions.ok');
            }
        );
    }

}
