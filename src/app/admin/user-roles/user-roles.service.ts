// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { 
    UserRoleDashboard, 
    UserRoleSettingsResult,
    UserRoleDeleteOptions, 
    UserRoleQuickFilter, 
    UserRoleGroupMembership, 
    UserRoleCreateRequest,
    UserRoleSetNameRequest, 
    UserRolesResult,
    UserRoleSetNameCodeOptions,
    UserRoleCreateCodeOptions,
    UserRoleGroupMembershipSetCodeOptions,
    UserRoleAccessRightsRequest,
    UserRoleAccessRightsUpdateCodeOptions,
    UserRoleDataAccessRequest,
    UserRoleDashboardResult
} from './user-roles.types';
import { QuickFilter, EditQuickFilterResults } from '../../common/quick-filters/quick-filters-types';
import { SearchTermRequest } from '../../common/pager';
import { Module } from '../../auth/acl.types';
import { AssetGroupTypes } from '../asset-groups/asset-groups.types';

@Injectable()
export class UserRolesService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/UserRolesAdmin';
        }

    // Get all company user roles
    public get(request: SearchTermRequest): Observable<UserRolesResult> {

        const obj = this.authHttp.removeObjNullKeys({
            SearchTerm: request.searchTerm ? request.searchTerm.toString() : null,
            Sorting: request.sorting
        });

        const params = new HttpParams({
            fromObject: obj
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }


    // =========================
    // BASIC SETTINGS
    // =========================

    public getSettingsByID(id: number): Observable<UserRoleSettingsResult> {
        return this.authHttp
            .get(this.baseURL + '/GetSettings/' + id);
    }


    public create(entity: UserRoleCreateRequest): Observable<UserRoleCreateCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as UserRoleCreateCodeOptions;
                    }
                })
            );
    }


    public updateSettings(entity: UserRoleSetNameRequest): Observable<UserRoleSetNameCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserRoleSetNameCodeOptions;
                    }
                })
            );
    }

    // Delete user role
    public delete(id: number): Observable<UserRoleDeleteOptions> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserRoleDeleteOptions;
                    }
                })
            ); 
    }


    // =========================
    // DASHBOARD
    // =========================

    public getDashboardByID(id: number): Observable<UserRoleDashboardResult> {
        return this.authHttp
            .get(this.baseURL + '/GetDashboard/' + id);
    }


    public updateDashboard(entity: UserRoleDashboard): Observable<number>  {
        return this.authHttp
            .put(this.baseURL + '/PutDashboard/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as number;
                    }
                })
            );            
    }


    // =========================
    // QUICK FILTERS
    // =========================

    public getQuickFiltersByID(id: number, entityID): Observable<QuickFilter> {

        const params = new HttpParams({
            fromObject: {
                ID: String(id),
                EntityID: String(entityID)
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetQuickFilters/', { params });
    }


    public updateQuickFilters(entity: UserRoleQuickFilter): Observable<number>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateQuickFilters', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return EditQuickFilterResults.Unknown;
                    }
                })
            );            
    }


    // =========================
    // GROUP MEMBERSHIP
    // =========================

    public getGroupMembershipByID(id: number, groupType: AssetGroupTypes): Observable<number[]> {

        const params = new HttpParams({
            fromObject: {
                ID: String(id),
                groupType: String(groupType)
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetGroupMembership/', { params });
    }

    public updateGroupMembership(entity: UserRoleGroupMembership): Observable<UserRoleGroupMembershipSetCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateGroupMembership', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserRoleGroupMembershipSetCodeOptions;
                    }
                })
            );            
    }

    // =========================
    // ACCESS RIGHTS
    // =========================

    public getRightsByID(id: number): Observable<Module[]> {
        return this.authHttp
            .get(this.baseURL + '/GetAccessRights/' + id);
    }

    public updateAccessRights(entity: UserRoleAccessRightsRequest): Observable<UserRoleAccessRightsUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateAccessRights/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserRoleAccessRightsUpdateCodeOptions;
                    }
                })
            );            
    }

    // =========================
    // DATA RIGHTS
    // =========================

    public getDataAccessByID(id: number): Observable<UserRoleDataAccessRequest> {
        return this.authHttp
            .get(this.baseURL + '/GetDataAccess/' + id);
    }

    public updateDataAccess(entity: UserRoleDataAccessRequest): Observable<number>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateDataAccess/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as UserRoleAccessRightsUpdateCodeOptions;
                    }
                })
            );            
    }
} 
