// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { AdminLabelService } from '../../../admin-label.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { ChargePointsService } from '../../charge-points.service';
import { Config } from '../../../../config/config';

// TYPES
import {
    ChargePointSettings,
    ChargePointSessionName,
    ChargePointCreateCodeOptions,
    ChargePointSettingsCodeOptions
} from '../../charge-points.types';
import { SearchType } from '../../../../shared/search-mechanism/search-mechanism-types';
import { KeyName } from '../../../../common/key-name';

@Component({
    selector: 'edit-charge-point-settings',
    templateUrl: 'edit-charge-point-settings.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-charge-point.less']
})

export class EditChargePointSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: false }) public editForm: NgForm;

    public loading: boolean;
    public loadingUnlinkedChargePoints: boolean;
    public chargePoint: ChargePointSettings = {} as ChargePointSettings;
    public unlinkedChargePoints: KeyName[] = [];
    public searchTypes: any = SearchType;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: ChargePointsService

    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(ChargePointSessionName);
    }

    // =========================
    // LIFECYCLE
    // =========================

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => {
                this.action = params.id
                            ? 'form.actions.edit'
                            : 'form.actions.create';
                if (params.id) {
                    this.get(params.id);
                }
            });
    }

    // =========================
    // LOCATION SEARCH
    // =========================

    public onLocationSelect(item?: KeyName) {
        this.chargePoint.locationID = item ? item.id : null;
        this.chargePoint.locationName = item ? item.name : null;
    }


    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: ChargePointSettings, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is create
        if (typeof(this.chargePoint.id) === 'undefined' || this.chargePoint.id === 0) {
            this.serviceSubscriber = this.service
                .create(this.chargePoint)
                .subscribe((res: ChargePointCreateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const errorMessage = this.getCreateResult(res);
                        this.snackBar.open(errorMessage, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else {
        // This is an update
        this.serviceSubscriber = this.service
            .update(this.chargePoint)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === null) {
                    this.labelService.set(this.chargePoint.name);
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    const errorMessage = this.getUpdateResult(res);
                    this.snackBar.open(errorMessage, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
        }
    }


    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((res: ChargePointSettings) => {
                this.loading = false;
                this.chargePoint = res;
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }


    // =========================
    // RESULTS
    // =========================
    private getCreateResult(code: ChargePointCreateCodeOptions): string {
        let message = '';
        switch (code) {
            case ChargePointCreateCodeOptions.NameAlreadyExists:
                message = 'form.errors.name_exists';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUpdateResult(code: ChargePointSettingsCodeOptions): string {
        let message = '';
        switch (code) {
            case ChargePointSettingsCodeOptions.NameAlreadyExists:
                message = 'form.errors.name_exists';
                break;
            case ChargePointSettingsCodeOptions.NotFound:
                message = 'form.errors.not_found';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
