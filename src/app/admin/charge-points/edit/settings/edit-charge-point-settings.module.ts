﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../../shared/shared.module';

// COMPONENTS
import { EditChargePointSettingsComponent } from './edit-charge-point-settings.component';
import { NotificationPoliciesListModule } from '../../../../common/notification-policies-list/notification-policies-list.module';

@NgModule({
    imports: [
        SharedModule,
        NotificationPoliciesListModule
    ],
    declarations: [
        EditChargePointSettingsComponent
    ],
    exports: [
        EditChargePointSettingsComponent
    ]
})

export class EditChargePointSettingsModule { }
