// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, Renderer } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { AdminLabelService } from '../../../admin-label.service';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { Config } from '../../../../config/config';

// TYPES
import { KeyName } from '../../../../common/key-name';
import { 
    TableColumnSetting, 
    TableActionOptions, 
    TableColumnSortOrderOptions, 
    TableColumnSortOptions,
    TableActionItem } from '../../../../shared/data-table/data-table.types';
import { ConfirmationDialogParams } from '../../../../shared/confirm-dialog/confirm-dialog-types';
import { ChargePointLinkUnlinkRequest, ChargePointSessionName, ChargePointLinkCodeOptions, ChargePointUnlinkCodeOptions } from '../../charge-points.types';
import { ChargePointsService } from '../../charge-points.service';

@Component({
    selector: 'edit-linked-cps',
    templateUrl: 'edit-linked-cps.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditLinkedChargePoinsComponent extends AdminBaseComponent implements OnInit {
    public loading: boolean;
    public loadingUnlinkedCps: boolean;
    public linked: KeyName[] = [];
    public unlinked: KeyName[] = [];
    public chargePointID: number;
    public tableSettings: TableColumnSetting[];

    @ViewChild('wrapper', { static: true }) public wrapperElm: ElementRef;
    
    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected confirmDialogService: ConfirmDialogService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: ChargePointsService,
        private renderer: Renderer
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar, null, confirmDialogService);
        labelService.setSessionItemName(ChargePointSessionName);

        this.tableSettings =  [
            {
                primaryKey: 'name',
                header: 'charge_points.name',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, false, null)
            },
            {
                primaryKey: 'id',
                header: ' ',
                actions: [TableActionOptions.delete]
            }
        ];
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.chargePointID = params.id;
                    this.getLinkedChargePoints();
                }
            });
    }

    // ----------------
    // TABLE
    // ----------------

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }
        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.delete: 
                const req = {
                    title: 'charge_points.manage_links.unlink',
                    message: 'form.warnings.are_you_sure',
                    submitButtonTitle: 'form.actions.continue'
                } as ConfirmationDialogParams;
                this.openDialog(req).subscribe((response) => {
                    if (response === true) {
                        this.onUnlinkChargePoint(id);
                    }
                });
                break;
            default: 
                break;

        }
    }

    public onSort(column: TableColumnSetting) {
        this.getLinkedChargePoints();
    }

    // =========================
    // CHARGE POINT SEARCH
    // =========================
    public onLinkChargePoint(cpID: number) {
        if (!cpID) {
            return;
        }
        
        this.loading = true;
        this.unsubscribeService();

        const req = {
            chargePointLinkID: cpID,
            chargePointID: this.chargePointID
        } as ChargePointLinkUnlinkRequest;

        this.unlinked = [];
        this.focusOutFromAutocomplete();

        this.serviceSubscriber = this.service
            .linkChargePoint(req)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('charge_points.manage_links.link.success', 'form.actions.close');
                    this.getLinkedChargePoints();
                } else {
                    const message = this.getAddLinkResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    
    // Fetch unlinked cps
    public getUnlinkedChargePoints(term: string) {
        if (!term || term.length < 2) {
            return;
        }

        this.message = '';
        this.loadingUnlinkedCps = true;
        this.unlinked = new Array<KeyName>();

        // first stop currently executing requests
        this.unsubscribeService();

        this.serviceSubscriber = this.service
            .getUnlinkedChargePoints(this.chargePointID, term)
            .subscribe((response: KeyName[]) => {
                    this.loadingUnlinkedCps = false;
                    this.unlinked = response;
                },
                (err) => {
                    this.loadingUnlinkedCps = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            );
    }
   


    // ----------------
    // PRIVATE
    // ----------------

    private onUnlinkChargePoint(cpID: number) {
        if (!cpID) {
            return;
        }
        
        this.loading = true;
        this.unsubscribeService();

        const req = {
            chargePointLinkID: cpID,
            chargePointID: this.chargePointID
        } as ChargePointLinkUnlinkRequest;

        this.unlinked = [];
        this.focusOutFromAutocomplete();

        this.serviceSubscriber = this.service
            .unlinkChargePoint(req)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('charge_points.manage_links.unlink.success', 'form.actions.close');
                    this.getLinkedChargePoints();
                } else {
                    const message = this.getRemoveLinkResult(res);
                    this.snackBar.open(message, 'form.actions.close');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

    // Fetch authorized users
    private getLinkedChargePoints() {
        this.message = '';
        this.loading = true;

        // first stop currently executing requests
        this.unsubscribeService();

        // then start the new request
        this.serviceSubscriber = this.service
            .getLinkedChargePoints(this.chargePointID)
            .subscribe((response: KeyName[]) => {
                    this.loading = false;
                    this.linked = response;
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }
    
    private focusOutFromAutocomplete() {
        // dispatch focus out event, by clicking anywhere else in the document
        const clickEvent = new MouseEvent('click', { bubbles: true });
        this.renderer.invokeElementMethod(this.wrapperElm.nativeElement.ownerDocument, 'dispatchEvent', [clickEvent]);
    }


    // =========================
    // RESULTS
    // =========================

    private getAddLinkResult(code: ChargePointLinkCodeOptions): string {
        let message = '';
        switch (code) {
            case ChargePointLinkCodeOptions.ChargePointNotFound:
            case ChargePointLinkCodeOptions.ChargePointToLinkNotFound:
                message = 'form.errors.not_found';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getRemoveLinkResult(code: ChargePointUnlinkCodeOptions): string {
        let message = '';
        switch (code) {
            case ChargePointUnlinkCodeOptions.ChargePointNotFound:
            case ChargePointUnlinkCodeOptions.ChargePointToLinkNotFound:
                message = 'form.errors.not_found';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
