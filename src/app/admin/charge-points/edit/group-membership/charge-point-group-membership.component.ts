
// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { AdminLabelService } from '../../../admin-label.service';
import { ChargePointsService } from './../../charge-points.service';
import { Config } from '../../../../config/config';

// TYPES
import { AssetGroupTypes } from '../../../asset-groups/asset-groups.types';
import { ChargePointGroupMembershipUpdateRequest, ChargePointSessionName, ChargePointGroupMembershipCodeOptions } from './../../charge-points.types';

@Component({
    selector: 'edit-charge-point-group-membership',
    templateUrl: 'charge-point-group-membership.html',
    encapsulation: ViewEncapsulation.None
})

export class EditChargePointGroupMembershipComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public groupTypes: any = AssetGroupTypes;
    public cp: ChargePointGroupMembershipUpdateRequest = {} as ChargePointGroupMembershipUpdateRequest;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected router: Router,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: ChargePointsService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar);
        labelService.setSessionItemName(ChargePointSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.cp.id = params.id;
                    this.get(params.id);
                }
            });
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: ChargePointGroupMembershipUpdateRequest, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

     
        // This is an update
        this.serviceSubscriber = this.service
            .updateGroupMembership(this.cp)
            .subscribe((res?: ChargePointGroupMembershipCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getGroupMembershipByID(id)
            .subscribe((res: number[]) => {
                this.loading = false;
                this.cp.groupIDs = res;
            },
            (err) => {
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }
}
