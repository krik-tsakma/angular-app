// FRAMEWORK
import { Component, ViewEncapsulation, ViewChildren } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// SERVICES
import { AdminLabelService } from '../../admin-label.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES 
import { SettingList } from '../../settings-list/settings-list.types';
import { ChargePointSessionName } from '../charge-points.types';

@Component({
    selector: 'edit-charge-point',
    templateUrl: 'edit-charge-point.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditChargePointComponent {
    public options: SettingList[] = [
        {
            title: 'assets.category.basic',
            items: [
                {
                    option: 'assets.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
                {
                    option: 'assets.category.basic.groups',
                    params: ['groups'],
                    icon: 'group'
                }
            ]
        },
        {
            title: 'charge_points.linked_with',
            items: [
                {
                    option: 'charge_points.manage_links',
                    params: ['linked'],
                    icon: 'group_work'
                }
            ]
        },
    ];

    constructor(public labelService: AdminLabelService,
                private configService: Config,
                private router: Router,
                private previousRouteService: PreviousRouteService) {
            labelService.setSessionItemName(ChargePointSessionName);
    }

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
