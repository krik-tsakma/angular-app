// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';
import { AssetGroupsModule } from '../../../common/asset-groups/asset-groups.module';
import { EditChargePointRoutingModule } from './edit-charge-point-routing.module';
import { EditChargePointSettingsModule } from './settings/edit-charge-point-settings.module';
import { AssetGroupMembershipModule } from './../../../common/asset-groups-membership/asset-group-membership.module';

// COMPONENTS
import { EditChargePointComponent } from './edit-charge-point.component';
import { EditLinkedChargePoinsComponent } from './links/edit-linked-cps.component';
import { EditChargePointGroupMembershipComponent } from './group-membership/charge-point-group-membership.component';

// SERVICES
import { ChargePointsService } from '../charge-points.service';

@NgModule({
    imports: [
        SharedModule,
        EditChargePointRoutingModule,
        EditChargePointSettingsModule,
        SettingsListModule,
        AssetGroupMembershipModule,
        AssetGroupsModule
    ],
    declarations: [
        EditChargePointComponent,
        EditLinkedChargePoinsComponent,
        EditChargePointGroupMembershipComponent,
    ],
    providers: [
        ChargePointsService
    ]
})
export class EditChargePointModule { }
