﻿
// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';

// COMPONENTS
import { EditChargePointSettingsComponent } from './settings/edit-charge-point-settings.component';
import { EditChargePointComponent } from './edit-charge-point.component';
import { EditLinkedChargePoinsComponent } from './links/edit-linked-cps.component';
import { EditChargePointGroupMembershipComponent } from './group-membership/charge-point-group-membership.component';

const EditChargePointRoutes: Routes = [
    {
        path: '',
        component: EditChargePointComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'settings',
        component: EditChargePointSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.settings'
        }
    },
    {
        path: 'groups',
        component: EditChargePointGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.group.category.basic.membership'
        }
    },
    {
        path: 'linked',
        component: EditLinkedChargePoinsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'charge_points.manage_links'
        }
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(EditChargePointRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class EditChargePointRoutingModule { }
