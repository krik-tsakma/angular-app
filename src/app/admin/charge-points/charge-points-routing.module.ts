
// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';

// COMPONENTS
import { ViewChargePointsComponent } from './view/view-charge-points.component';
import { EditChargePointSettingsComponent } from './edit/settings/edit-charge-point-settings.component';

const ChargePointRoutes: Routes = [
    {
        path: '',
        component: ViewChargePointsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditChargePointSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-charge-point.module').then((m) => m.EditChargePointModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ChargePointRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class ChargePointRoutingModule { }
