import { PagerResults } from '../../common/pager';

export const ChargePointSessionName: string = 'cpn';

export interface ChargePointsResult extends PagerResults {
    results: ChargePointResultItem[];
}

export interface ChargePointResultItem {
    id: number;
    name: string;
    locationName: string;
    groups: string;
}

// ====================
// BASIC INFO
// ====================

export interface ChargePointSettings {
    id: number;
    name: string;
    locationID: number;
    locationName: string;
    power: number;
    ocppid: string;
    latitude: number;
    longitude: number;
    notificationPolicyID?: number;
}

export enum ChargePointCreateCodeOptions {
    Created = 0,
    NameAlreadyExists = 1,
    AssociatedLocationNotFound = 3,
    UnknownError = 10,
}

export enum ChargePointSettingsCodeOptions {
    Updated = 0,
    NotFound = 1,
    NameAlreadyExists = 2,
    AssociatedLocationNotFound = 3,
    UnknownError = 10,
}

// ====================
// LINK / UNLINK
// ====================

export interface ChargePointLinkUnlinkRequest {
    chargePointID: number;
    chargePointLinkID: number;
}

export enum ChargePointLinkCodeOptions {
    Added = 0,
    ChargePointNotFound = 1,
    ChargePointToLinkNotFound = 2,
    LinkAlreadyExists = 3,
    IssuerNotFound = 4,
    ChargePointNotAllowed = 5,
    ChargePointToLinkNotAllowed = 6
}

export enum ChargePointUnlinkCodeOptions {
    Removed = 0,
    ChargePointNotFound = 1,
    ChargePointToLinkNotFound = 2,
    LinkDoesNotExist = 3,
    IssuerNotFound = 4,
    ChargePointNotAllowed = 5,
    ChargePointToLinkNotAllowed = 6
}

// ====================
// GROUP MEMBERSHIP
// ====================

export interface ChargePointGroupMembershipUpdateRequest {
    id: number;
    groupIDs: number[];
}

export enum ChargePointGroupMembershipCodeOptions {
    Updated = 0,
    NoSuchChargePointExists = 1,
    IssuerNotFound = 2,
    NotAllowedChargePoint = 3,
    UnknownError = 10
}
