// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RxJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { KeyName } from '../../common/key-name';
import { SearchTermRequest } from '../../common/pager';
import { 
    ChargePointsResult, 
    ChargePointSettings, 
    ChargePointCreateCodeOptions, 
    ChargePointSettingsCodeOptions, 
    ChargePointLinkUnlinkRequest, 
    ChargePointUnlinkCodeOptions, 
    ChargePointLinkCodeOptions, 
    ChargePointGroupMembershipUpdateRequest,
    ChargePointGroupMembershipCodeOptions
} from './charge-points.types';

@Injectable()
export class ChargePointsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/ChargePoints';
        }

    // Get all charge groups
    public get(request: SearchTermRequest): Observable<ChargePointsResult> {
    
        const params = new HttpParams({
            fromObject: {
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Sorting: request.sorting
            }
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }


    public getByID(id: number): Observable<ChargePointSettings> {
        return this.authHttp
            .get(this.baseURL + '/' + id);
    }


    public create(entity: ChargePointSettings): Observable<ChargePointCreateCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as ChargePointCreateCodeOptions;
                    }
                })
            );
    }


    public update(entity: ChargePointSettings): Observable<ChargePointSettingsCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ChargePointSettingsCodeOptions;
                    }
                })
            );            
    }

    public delete(id: number): Observable<number> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((r) => r.status as number)                    
            ); 
    }

    // ====================
    // LINK CHARGE POINT
    // ====================

    public getLinkedChargePoints(cpID: number): Observable<KeyName[]> {
        return this.authHttp
            .get(this.baseURL + '/GetLinkedChargePoints/' + cpID);
    }


    public linkChargePoint(entity: ChargePointLinkUnlinkRequest): Observable<ChargePointLinkCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/LinkChargePoint', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ChargePointLinkCodeOptions;
                    }
                })
            );            
    }

    // ====================
    // UNLINK CHARGE POINT
    // ====================

    public getUnlinkedChargePoints(cpID: number, searchTerm: string): Observable<KeyName[]> {

        const params = new HttpParams({
            fromObject: {
                SearchTerm: searchTerm ? searchTerm.toString() : '',
                ChargePointID: cpID ? cpID.toString() : null,
            }
        });

        return this.authHttp
            .get(this.baseURL + '/GetUnlinkedChargePoints', { params });
    }


    public unlinkChargePoint(entity: ChargePointLinkUnlinkRequest): Observable<ChargePointUnlinkCodeOptions> {
        return this.authHttp
            .put(this.baseURL + '/UnlinkChargePoint', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ChargePointUnlinkCodeOptions;
                    }
                })
            );            
    }

     // =========================
    // GROUP MEMBERSHIP
    // =========================

    public getGroupMembershipByID(id: number): Observable<number[]> {
        return this.authHttp
            .get(this.baseURL + '/GetGroupMembership/' + id);
    }

    
    public updateGroupMembership(entity: ChargePointGroupMembershipUpdateRequest): Observable<ChargePointGroupMembershipCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateGroupMembership/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as ChargePointGroupMembershipCodeOptions;
                    }
                })
            );            
    }

} 
