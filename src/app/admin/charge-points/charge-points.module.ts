// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler  } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { SettingsListModule } from '../settings-list/settings-list.module';
import { EditChargePointSettingsModule } from './edit/settings/edit-charge-point-settings.module';
import { ChargePointRoutingModule } from './charge-points-routing.module';

// COMPONENTS
import { ViewChargePointsComponent } from './view/view-charge-points.component';

// SERVICES
import { ChargePointsService } from './charge-points.service';
import { TranslateStateService } from '../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../multiTranslateHttpLoader';


export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService ) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/charge-points/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        SharedModule,
        ChargePointRoutingModule,
        EditChargePointSettingsModule,
        SettingsListModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        })
    ],
    declarations: [
        ViewChargePointsComponent
    ],
    providers: [
        ChargePointsService
    ]
})
export class ChargePointsModule { }
