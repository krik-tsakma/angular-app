// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';

// COMPONENTS
import { ViewFairUsePoliciesComponent } from './view/view-fair-use-policies.component';
import { EditFairUsePolicyComponent } from './edit/edit-fair-use-policy.component';

const FairUsePoliciesRoutes: Routes = [
    {
        path: '',
        component: ViewFairUsePoliciesComponent,
        canActivate: [AuthGuard]
    },
    {
        path: ':editMode/:id',
        component: EditFairUsePolicyComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    },
    {
        path: ':editMode',
        component: EditFairUsePolicyComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(FairUsePoliciesRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class FairUsePoliciesRoutingModule { }
