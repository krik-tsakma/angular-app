import { FairUsePolicy } from './fair-use-policies.types';
import { SearchTermRequest } from '../../../common/pager';
import { KeyName } from '../../../common/key-name';

export type FairUsePoliciesRequest = SearchTermRequest;

export interface FairUsePolicy extends KeyName {
    maxBookingsOverPeriod: number;
    maxBookingsOverPeriodType: FairUsePolicyPeriodTypes;
    maxBookingLengthHours: number;
    cancelationFeeHours: number;
}

export enum FairUsePolicyPeriodTypes {
    Day = 0,
    RollingWeek,
    RollingMonth,
    RollingYear,
    CalendarWeek,
    CalendarMonth,
    CalendarYear
}

export let FairUsePolicyPeriodTypesOptions: KeyName[] = [
    {
        id: FairUsePolicyPeriodTypes.Day,
        name: 'FairUsePolicyPeriodTypes.Day',
        term: 'book_n_go.fair_use_policies.max_future_booking.type.day',
    },
    {
        id: FairUsePolicyPeriodTypes.RollingWeek,
        name: 'FairUsePolicyPeriodTypes.RollingWeek',
        term: 'book_n_go.fair_use_policies.max_future_booking.type.rolling_week',
    },
    {
        id: FairUsePolicyPeriodTypes.RollingMonth,
        name: 'FairUsePolicyPeriodTypes.RollingMonth',
        term: 'book_n_go.fair_use_policies.max_future_booking.type.rolling_month',
    },
    {
        id: FairUsePolicyPeriodTypes.RollingYear,
        name: 'FairUsePolicyPeriodTypes.RollingYear',
        term: 'book_n_go.fair_use_policies.max_future_booking.type.rolling_year',
    },
    {
        id: FairUsePolicyPeriodTypes.CalendarWeek,
        name: 'FairUsePolicyPeriodTypes.CalendarWeek',
        term: 'book_n_go.fair_use_policies.max_future_booking.type.calendar_week',
    },
    {
        id: FairUsePolicyPeriodTypes.CalendarMonth,
        name: 'FairUsePolicyPeriodTypes.CalendarMonth',
        term: 'book_n_go.fair_use_policies.max_future_booking.type.calendar_month',
    },
    {
        id: FairUsePolicyPeriodTypes.CalendarYear,
        name: 'FairUsePolicyPeriodTypes.CalendarYear',
        term: 'book_n_go.fair_use_policies.max_future_booking.type.calendar_year',
    },
];
