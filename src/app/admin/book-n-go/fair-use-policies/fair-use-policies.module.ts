// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { FairUsePoliciesRoutingModule } from './fair-use-policies-routing.module';

// SERVICES
import { FairUsePoliciesService } from './fair-use-policies.service';
import { TranslateStateService } from '../../../core/translateStore.service';

// COMPONENTS
import { ViewFairUsePoliciesComponent } from './view/view-fair-use-policies.component';
import { EditFairUsePolicyComponent } from './edit/edit-fair-use-policy.component';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../../multiTranslateHttpLoader';

export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/book-n-go/fair-use-policies/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        HttpClientJsonpModule,
        SharedModule,
        FairUsePoliciesRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        }),
    ],
    declarations: [
        ViewFairUsePoliciesComponent,
        EditFairUsePolicyComponent,
    ],
    entryComponents: [
    ],
    providers: [
        FairUsePoliciesService
    ]
})

export class FairUsePoliciesModule {

}
