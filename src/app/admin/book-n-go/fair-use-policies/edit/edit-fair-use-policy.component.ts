// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { FairUsePoliciesService } from '../fair-use-policies.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UserOptionsService } from '../../../../shared/user-options/user-options.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import { KeyName } from '../../../../common/key-name';
import { 
    FairUsePolicy, FairUsePolicyPeriodTypes, 
    FairUsePolicyPeriodTypesOptions, 
} from '../fair-use-policies.types';
import { BookNGoUpsertRequest, BookNGoCrudCodeOptions, BookNGoCrudResult } from '../../book-n-go.types';

@Component({
    selector: 'edit-fair-use-policy',
    templateUrl: 'edit-fair-use-policy.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-fair-use-policy.less']
})

export class EditFairUsePolicyComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: false }) public editForm: NgForm;
    public action: string;
    public loading: boolean;
    public policy: FairUsePolicy = {} as FairUsePolicy;
    public maxBookingsOverPeriodTypes: KeyName[] = FairUsePolicyPeriodTypesOptions;
    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,    
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private userOptions: UserOptionsService,
        private service: FairUsePoliciesService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);

        this.policy.maxBookingsOverPeriodType = FairUsePolicyPeriodTypes.CalendarMonth;
        this.maxBookingsOverPeriodTypes.forEach((x) => {
            x.name = this.translator.instant(x.term);
            x.enabled = true;
        });
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: string;
            }) => { 
                this.action = params.editMode === 'edit' 
                            ? 'form.actions.edit'
                            : 'form.actions.create';

                if (params.id) {
                    this.get(params.id);
                }
            });
    }

    public get(id: string) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service.getByID(id)
        .subscribe((data) => {
            this.loading = false;
            this.policy = data;
        },
        (err) => {
            this.loading = false;
            this.message = 'data.error';
        });
    }

    public onSubmit({ value, valid }: { value: FairUsePolicy, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }

        this.loading = true;
        this.unsubscribeService();
        
        const req = { entity: this.policy, culture: this.userOptions.getCulture() } as BookNGoUpsertRequest<FairUsePolicy>;
        // This is create
        if (typeof(this.policy.id) === 'undefined' || this.policy.id === 0) {
            this.serviceSubscriber = this.service
                .create(req)
                .subscribe((res: BookNGoCrudResult) => {
                    this.loading = false;
                    if (res.code !== BookNGoCrudCodeOptions.None) {
                        const errorMessage = this.getCrudResult(res.code, res.localizedErrorMessage);
                        this.snackBar.open(errorMessage, 'form.actions.close');
                    } else {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else { 
            // This is an update
            this.serviceSubscriber = this.service
            .update(req)
            .subscribe((res: BookNGoCrudResult) => {
                this.loading = false;
                if (res.code !== BookNGoCrudCodeOptions.None) {
                    const errorMessage = this.getCrudResult(res.code, res.localizedErrorMessage);
                    this.snackBar.open(errorMessage, 'form.actions.close');
                } else {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        }
    }

    public onChangePeriodType(type: FairUsePolicyPeriodTypes) {
        this.policy.maxBookingsOverPeriodType = type;
    }

    
    // =========================
    // RESULTS
    // =========================

    private getCrudResult(code: BookNGoCrudCodeOptions, message: string): string {
        switch (code) {
            case BookNGoCrudCodeOptions.NotFound:
                message = 'form.errors.not_found';
                break;
            case BookNGoCrudCodeOptions.Unknown:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

}
