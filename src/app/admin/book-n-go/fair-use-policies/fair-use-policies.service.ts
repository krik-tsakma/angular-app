// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../../auth/authHttp';
import { Config } from '../../../config/config';

// TYPES
import { FairUsePolicy, FairUsePoliciesRequest } from './fair-use-policies.types';
import { 
    BookNGoDeleteRequest, BookNGoUpsertRequest, BookNGoResults,
    BookNGoCrudResult, BookNGoCrudCodeOptions 
} from '../book-n-go.types';

@Injectable()
export class FairUsePoliciesService {
    private baseApiURL: string;
    constructor(private authHttp: AuthHttp,
                private config: Config) { 
        
        this.baseApiURL = config.get('apiUrl') + '/api/FairUsePolicies';
    }


    // Get 
    public get(request: FairUsePoliciesRequest): Observable<BookNGoResults<FairUsePolicy>> {             

        const params = new HttpParams({            
            fromObject: {
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Sorting: request.sorting
            }
        });  

        return this.authHttp
            .get(this.baseApiURL, { params });
    }

    public getByID(id: string): Observable<FairUsePolicy> {
        return this.authHttp
            .get(this.baseApiURL + '/' + id);
    }


    public create(entity: BookNGoUpsertRequest<FairUsePolicy>): Observable<BookNGoCrudResult>  {
        return this.authHttp
            .post(this.baseApiURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        const createdEntity = res.body as BookNGoCrudResult;
                        return { id: createdEntity.id, code: BookNGoCrudCodeOptions.None } as BookNGoCrudResult;
                    } else  {
                        return res.body as BookNGoCrudResult;
                    }                    
                })               
            );
    }

    public update(entity: BookNGoUpsertRequest<FairUsePolicy>): Observable<BookNGoCrudResult>  {
        return this.authHttp
            .put(this.baseApiURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return { code: BookNGoCrudCodeOptions.None } as BookNGoCrudResult;
                    } else  {
                        return res.body as BookNGoCrudResult;
                    }
                })
            );            
    }

    // Delete method 
    public delete(request: BookNGoDeleteRequest): Observable<BookNGoCrudResult>  {
        const params = new HttpParams({
            fromObject: {
                ID: request.id.toString(),
                Culture: request.culture
            }
        });

        return this.authHttp
            .delete(this.baseApiURL, { observe: 'response', params: params })
            .pipe(
                map((res) => {
                    if (res.status === 200 && !res.body) {
                        return { code: BookNGoCrudCodeOptions.None } as BookNGoCrudResult;
                    } else  {
                        return res.body as BookNGoCrudResult;
                    }
                })
            );
    }

}
