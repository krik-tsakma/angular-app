// FRAMEWORK
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { TariffsService } from '../tariffs.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { StoreManagerService } from '../../../../shared/store-manager/store-manager.service';
import { UserOptionsService } from '../../../../shared/user-options/user-options.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import { Pager } from '../../../../common/pager';
import { StoreItems } from '../../../../shared/store-manager/store-items';
import { Tariff, TariffsRequest } from '../tariffs.types';
import { 
    TableColumnSetting, TableActionItem, TableActionOptions, 
    TableColumnSortOrderOptions, TableColumnSortOptions
} from '../../../../shared/data-table/data-table.types';
import { 
    BookNGoDeleteRequest, BookNGoResults,
    BookNGoCrudCodeOptions, BookNGoCrudResult, TariffsSessionName 
} from '../../book-n-go.types';

@Component({
    selector: 'view-tariffs',
    templateUrl: 'view-tariffs.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: []
})


export class ViewTariffsComponent extends AdminBaseComponent implements OnInit {
    public tariffs: Tariff[] = [];
    public searchTermControl = new FormControl();     
    public request: TariffsRequest = {} as TariffsRequest;
    public pager: Pager = new Pager();
    public loading: boolean;
    public tableSettings: TableColumnSetting[];

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBarService: SnackBarService,
        protected labelService: AdminLabelService,
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,
        protected storeManager: StoreManagerService,
        protected configService: Config,
        private userOptions: UserOptionsService,
        private service: TariffsService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBarService, storeManager, confirmDialogService);
        labelService.setSessionItemName(TariffsSessionName);

        this.tableSettings =  [
            {
                primaryKey: 'name',
                header: 'book_n_go.tariffs.name',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete]
            }
        ];
    }


    public ngOnInit(): void {
        super.ngOnInit();
       
        const savedData = this.storeManager.getOption(StoreItems.tariffs) as TariffsRequest;
        if (savedData) {
            this.request = savedData;
            this.searchTermControl.setValue(this.request.searchTerm);
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchTermControl.valueChanges    
            .pipe(        
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing             
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    this.request.searchTerm = term;
                    this.get(true);
                    return of([]);
                })
            )
            .subscribe(); 
        this.get(true);
    }



    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.searchTermControl.setValue(null);
    }

    
    // ----------------
    // TABLE CALLBACKS
    // ----------------

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const id = item.record.id;
        switch (item.action) {
            case TableActionOptions.edit: 
                this.manage(id, item.record.name);
                break;
            case TableActionOptions.delete: 
                this.openDialog().subscribe((response) => {
                    if (response === true) {
                        this.delete(id);
                    }
                });
                break;
            default: 
                break;

        }
    }


    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }


    // ----------------
    // PRIVATE
    // ----------------

    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.tariffs = new Array<Tariff>();
        }   

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;
   
        // first stop currently executing requests
        this.unsubscribeService();

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.tariffs, this.request);
            
        // then start the new request
        this.serviceSubscriber = this.service
            .get(this.request)
            .subscribe(
                (response: BookNGoResults<Tariff>) => {
                    this.loading = false;

                    this.tariffs = this.tariffs.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;                                          
                    
                    if (!this.tariffs || this.tariffs.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                () => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }


    // ----------------
    // DELETE
    // ----------------
    private delete(id: number) {
        // first stop currently executing requests
        this.unsubscribeService();
        
        const req = { id: id, culture: this.userOptions.getCulture() } as BookNGoDeleteRequest;
        this.serviceSubscriber = this.service
            .delete(req)
            .subscribe((res: BookNGoCrudResult) => {
                if (res.code !== BookNGoCrudCodeOptions.None) {
                    switch (res.code) {
                        case BookNGoCrudCodeOptions.NotFound:
                            this.snackBar.open('form.errors.not_found', 'form.actions.close');
                            break;
                        case BookNGoCrudCodeOptions.Unknown:
                            this.snackBar.open('form.errors.unknown', 'form.actions.close');
                            break;
                        default:
                            this.snackBar.open(res.localizedErrorMessage, 'form.actions.close');
                            break;
                    }
                } else {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get(true);
                }
            },
            () => {
                    this.loading = false;
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                });
    }
}
