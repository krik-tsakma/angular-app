// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { TariffsService } from '../tariffs.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UserOptionsService } from '../../../../shared/user-options/user-options.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { AdminLabelService } from '../../../admin-label.service';
import { Config } from '../../../../config/config';

// TYPES
import { Tariff } from '../tariffs.types';
import { 
    BookNGoUpsertRequest, BookNGoCrudCodeOptions, 
    BookNGoCrudResult, TariffsSessionName 
} from '../../book-n-go.types';

@Component({
    selector: 'edit-tariff',
    templateUrl: 'edit-tariff.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['edit-tariff.less']
})

export class EditTariffComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: false }) public editForm: NgForm;
    public action: string;
    public loading: boolean;
    public tariff: Tariff = {} as Tariff;
    
    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,      
        protected router: Router,
        protected unsubscriber: UnsubscribeService,    
        protected snackBar: SnackBarService,
        protected configService: Config,
        public labelService: AdminLabelService,
        public userOptions: UserOptionsService,
        private activatedRoute: ActivatedRoute,
        private service: TariffsService,        
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(TariffsSessionName);

    }

    public ngOnInit(): void {
        super.ngOnInit();

       // this.setUpForm();
        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: string;
            }) => { 
                this.action = params.editMode === 'edit' 
                            ? 'form.actions.edit'
                            : 'form.actions.create';

                if (params.id) {
                    this.get(params.id);
                }
            });
        
    }

    public get(id: string) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((data) => {
                this.loading = false;
                this.tariff = data;
            },
            (err) => {
                this.loading = false;
                this.message = 'data.error';
            }
        );
    }

    public onSubmit({ value, valid }: { value: Tariff, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        const req = { entity: this.tariff, culture: this.userOptions.getCulture() } as BookNGoUpsertRequest<Tariff>;
        // This is create
        if (typeof(this.tariff.id) === 'undefined' || this.tariff.id === 0) {
            this.serviceSubscriber = this.service
                .create(req)
                .subscribe((res: BookNGoCrudResult) => {
                    this.loading = false;
                    if (res.code !== BookNGoCrudCodeOptions.None) {
                        const errorMessage = this.getCrudResult(res.code, res.localizedErrorMessage);
                        this.snackBar.open(errorMessage, 'form.actions.close');
                    } else {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else { 
            // This is an update
            this.serviceSubscriber = this.service
            .update(req)
            .subscribe((res: BookNGoCrudResult) => {
                this.loading = false;
                if (res.code !== BookNGoCrudCodeOptions.None) {
                    const errorMessage = this.getCrudResult(res.code, res.localizedErrorMessage);
                    this.snackBar.open(errorMessage, 'form.actions.close');
                } else {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        }
    }

    
     // =========================
    // RESULTS
    // =========================

    private getCrudResult(code: BookNGoCrudCodeOptions, message: string): string {
        switch (code) {
            case BookNGoCrudCodeOptions.NotFound:
                message = 'form.errors.not_found';
                break;
            case BookNGoCrudCodeOptions.Unknown:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
