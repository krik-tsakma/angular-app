// FRAMEWORK
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientJsonpModule } from '@angular/common/http';

// NODE_MODULES
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { TariffsRoutingModule } from './tariffs-routing.module';

// COMPONENTS
import { ViewTariffsComponent } from './view/view-tariffs.component';
import { EditTariffComponent } from './edit/edit-tariff.component';

// SERVICES
import { TranslateStateService } from '../../../core/translateStore.service';

// CUSTOM TRANSLATE LOADER
import { MultiTranslateHttpLoader, MultiMissingTranslationHandler } from './../../../multiTranslateHttpLoader';

export function myTranslateLoader(http: HttpClient, translateState: TranslateStateService) {
    return new MultiTranslateHttpLoader(http, ['./assets/i18n/admin/book-n-go/tariffs/'], '.json', translateState);
}

export function myMissingLoader(translateState: TranslateStateService) {
    return new MultiMissingTranslationHandler(translateState);
}

@NgModule({
    imports: [
        HttpClientJsonpModule,
        SharedModule,
        TariffsRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (myTranslateLoader),
                deps: [HttpClient, TranslateStateService]
            },
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useFactory: (myMissingLoader),
                deps: [TranslateStateService]
            },
            useDefaultLang: false,
            isolate: true
        }),
    ],
    declarations: [
        ViewTariffsComponent,
        EditTariffComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ]
})

export class TariffsModule {

}
