import { SearchTermRequest } from '../../../common/pager';
import { KeyName } from '../../../common/key-name';

export type TariffsRequest = SearchTermRequest;

export interface Tariff extends KeyName {
    distanceBased: boolean;
    distancePrice: number;
    timeBased: boolean;
    timePer15Minutes: number;
    timePerHour: number;
    timePer24Hours: number;
    startFee: boolean;
    startFeePrice: number;
}
