// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';
import { TariffsService } from './tariffs.service';

// COMPONENTS
import { ViewTariffsComponent } from './view/view-tariffs.component';
import { EditTariffComponent } from './edit/edit-tariff.component';

const TariffsRoutes: Routes = [
    {
        path: '',
        component: ViewTariffsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: ':editMode/:id',
        component: EditTariffComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    },
    {
        path: ':editMode',
        component: EditTariffComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(TariffsRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        TariffsService
    ]
})
export class TariffsRoutingModule { }
