import { UserLang } from '../../common/user-lang';
import { PagerRequest, PagerResults } from '../../common/pager';

export const FairUserPoliciesSessionName: string = 'fupn';
export const TariffsSessionName: string = 'tn';

export interface BookNGoResults<T> extends PagerResults {
    results: T[];
}

export interface BookNGoUpsertRequest<T> {
    entity: T;
}

export interface BookNGoDeleteRequest extends UserLang {
    id: number;
}

export enum BookNGoCrudCodeOptions {
    None = 0,
    ServerError,
    NotFound,
    InvalidOperation,
    ValidationError,
    PersistenceFailure,
    Unknown
}

export interface BookNGoCrudResult {
    id?: number;
    code: BookNGoCrudCodeOptions;
    localizedErrorMessage: string;
}
