
// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../shared/shared.module';
import { LocationsRoutingModule } from './locations-routing.module';
import { SettingsListModule } from '../settings-list/settings-list.module';
import { EditLocationSettingsModule } from './edit/settings/edit-location-settings.module';
import { LocationGroupsModule } from '../../common/location-groups/location-groups.module';

// SERVICES
import { LocationsService } from './locations.service';

// COMPONENTS
import { ViewLocationsComponent } from './view/view-locations.component';

@NgModule({
    imports: [
        SharedModule,
        LocationsRoutingModule,
        SettingsListModule,
        EditLocationSettingsModule,
        LocationGroupsModule,
    ],
    declarations: [
        ViewLocationsComponent
    ],   
    providers: [
        LocationsService
    ]
})
export class LocationsModule { }
