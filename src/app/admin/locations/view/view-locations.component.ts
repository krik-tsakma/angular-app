// FRAMEWORK
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

// RXJS
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

// COMPONENTS
import { AdminBaseComponent } from '../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../shared/custom-translate.service';
import { SnackBarService } from '../../../shared/snackbar.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { StoreManagerService } from '../../../shared/store-manager/store-manager.service';
import { UnsubscribeService } from '../../../shared/unsubscribe.service';
import { AdminLabelService } from '../../admin-label.service';
import { LocationsService } from '../locations.service';
import { AssetLinksService } from '../../asset-links/asset-links.service';
import { Config } from '../../../config/config';

// TYPES
import { Pager } from '../../../common/pager';
import { StoreItems } from '../../../shared/store-manager/store-items';
import { 
    TableColumnSetting, TableActionItem, TableActionOptions, 
    TableColumnSortOrderOptions, TableColumnSortOptions
} from '../../../shared/data-table/data-table.types';
import { LocationResultItem, LocationSessionName, LocationsResult, LocationsRequest } from '../locations.types';
import { AssetGroupItem } from '../../asset-groups/asset-groups.types';
import { MoreOptionsMenuOption, MoreOptionsMenuTypes, MoreOptionsMenuOptions } from '../../../shared/more-options-menu/more-options-menu.types';
import { EntityTypes } from '../../../shared/entity-types/entity-types';
import { KeyName } from '../../../common/key-name';

@Component({
    selector: 'view-locations',
    templateUrl: 'view-locations.html',
    providers: [AssetLinksService]
})

export class ViewLocationsComponent extends AdminBaseComponent implements OnInit {
    public vehicles: LocationResultItem[] = [];
    public searchTermControl = new FormControl();     
    public request: LocationsRequest = {} as LocationsRequest;
    public pager: Pager = new Pager();
    public loading: boolean;
    public tableSettings: TableColumnSetting[];
    public groupSelected = null;
    public pageOptions: MoreOptionsMenuOptions[] = [
        {
            term: 'assets.locations',
            options: [
                {
                    id: 'add',
                    term: 'form.actions.create',
                    type: MoreOptionsMenuTypes.none,
                    enabled: true                  
                }
            ]
        }
    ];


    constructor(
        protected previousRouteService: PreviousRouteService,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBarService: SnackBarService,
        protected router: Router,
        protected confirmDialogService: ConfirmDialogService,
        protected storeManager: StoreManagerService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private service: LocationsService,
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBarService, storeManager, confirmDialogService);
        labelService.setSessionItemName(LocationSessionName);

        this.tableSettings =  [
            {
                primaryKey: 'name',
                header: 'location.data_table.name',
                sorting:  new TableColumnSortOptions(TableColumnSortOrderOptions.asc, true, null)
            },
            {
                primaryKey: 'groups',
                header: 'assets.groups',
            },         
            {
                primaryKey: ' ',
                header: ' ',
                actions: [TableActionOptions.edit, TableActionOptions.delete],
                actionsVisibilityFunction: this.showUDButtons
            }
        ];
    }


    public ngOnInit(): void {
        super.ngOnInit();
       
        const savedData = this.storeManager.getOption(StoreItems.locations) as LocationsRequest;
        if (savedData) {
            this.request = savedData;
            this.searchTermControl.setValue(this.request.searchTerm);
        } else {
            const defaultSortColumn = this.tableSettings.find((x) => x.sorting.selected === true);
            this.request.sorting = defaultSortColumn.sorting.stringFormat(defaultSortColumn.primaryKey);
        }

        this.searchTermControl.valueChanges     
            .pipe(       
                debounceTime(500), // Debounce 500 waits for 500ms until user stops pushing             
                distinctUntilChanged(), // use this to handle scenarios like hitting backspace and retyping the same letter
                // switchMap operator automatically unsubscribes from previous subscriptions,
                // as soon as the outer Observable emits new values
                // so we will always search with last term
                switchMap((term) => {
                    this.request.searchTerm = term;
                    this.get(true);
                    return of([]);
                })
            )
            .subscribe(); 
        
        this.get(true);
    }


    public onSelectPageOption(item: MoreOptionsMenuOption): void {
        if (item.id === 'add') {
            this.manage();
        } 
    }



    public clearInput(event: Event) {
        if (event) {
            // do not bubble event otherwise focus is on the textbox
            event.stopPropagation();
        }
        // clear selection
        this.searchTermControl.setValue(null);
    }

    
    public onChangeLocalGroups(event: AssetGroupItem) {
        this.request.groupID = event.id;
        this.get(true);
    }

    // =========================
    // ENTITY TYPES CALLBACKS
    // =========================

    public onChangeEntityType(newType: EntityTypes): void {
        this.request.groupID = null;
        this.get(true);
    }

    // =========================
    // VEHICLE TYPE CALLBACKS
    // =========================

    // When the user selects a vehicle type from the DDL
    public onChangeVehicleType(vehicleType: KeyName): void {
        this.request.groupID = null;
        this.get(true);
    }

    // =========================
    // TABLE CALLBACKS
    // =========================

    // Navigate to edit page
    public onManage(item: TableActionItem) {
        if (!item) {
            return;
        }

        const record = item.record as LocationResultItem;
        switch (item.action) {
            case TableActionOptions.edit: 
                this.manage(record.id, item.record.name);
                break;
            case TableActionOptions.delete: 
                this.openDialog().subscribe((response) => {
                    if (response === true) {
                        this.delete(record.id);
                    }
                });
                break;
            default: 
                break;

        }
    }


    // On table scroll down set pager settings and fetch next data
    public onScrollDown() {
        if (this.pager.pageNumber === this.pager.totalPages) {
            return;
        }

        this.pager.addPage();
        this.get(false);
    }


    public onSort(column: TableColumnSetting) {
        this.request.sorting = column.sorting.stringFormat(column.primaryKey);
        this.get(true);
    }

    public showUDButtons(action: TableActionOptions, record: LocationResultItem): boolean {
        if (!record) {
            return false;
        }
       
        return true;
    }


    // =========================
    // PRIVATE
    // =========================

    // Fetch events data
    private get(resetPageNumber: boolean) {
        this.message = '';
        this.loading = true;
        if (resetPageNumber) {
            this.pager.pageNumber = 1;
            this.vehicles = new Array<LocationResultItem>();
        }   

        this.request.pageNumber = this.pager.pageNumber;
        this.request.pageSize = this.pager.pageSize;
   
        // first stop currently executing requests
        this.unsubscribeService();

        // the save the request's data for future user
        this.storeManager.saveOption(StoreItems.locations, this.request);
            
        // then start the new request
        this.serviceSubscriber = this.service
            .get(this.request)
            .subscribe(
                (response: LocationsResult) => {
                    this.loading = false;

                    this.vehicles = this.vehicles.concat(response.results);

                    // Get the paging info
                    this.pager.pageNumber = response.pageNumber;
                    this.pager.pageSize = response.pageSize;
                    this.pager.totalPages = response.totalPages;
                    this.pager.totalRecords = response.totalRecords;                                          
                    
                    if (!this.vehicles || this.vehicles.length === 0) {
                        this.message = 'data.empty_records';
                    }
                },
                (err) => {
                    this.loading = false;
                    this.message = 'data.error';
                }
            );
    }


    // =========================
    // DELETE
    // =========================

    private delete(id: number) {
        this.loading = true;
        
        // first stop currently executing requests
        this.unsubscribeService();
        
        this.serviceSubscriber = this.service
            .delete(id)
            .subscribe((res: number) => {
                this.loading = false;
                if (res === 200) {
                    this.snackBar.open('form.delete_success', 'form.actions.ok');
                    this.get(true);
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            },
            (err) => {
                this.loading = false;
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }

}
