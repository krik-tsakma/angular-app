import { KeyName } from '../../common/key-name';
import { PagerResults, SearchTermRequest } from '../../common/pager';

export const LocationSessionName: string = 'locn';

export interface LocationsRequest extends SearchTermRequest {    
    groupID?: number;
}

export interface LocationsResult extends PagerResults {
    results: LocationResultItem[];
}

export interface LocationResultItem extends KeyName {
    groups: string;    
}
// ====================
// BASIC INFO
// ====================

export interface LocationSettings {
    id: number;
    ownID: string;
    licencePlate: string;
    description: string;
    vehicleTypeID: number;
    chargingID: string;
}

export enum LocationCreateCodeOptions {
    Created = 0,
    OwnIDAlreadyExists = 1,
    LicencePlateAlreadyExists = 2,
    ChargingIDAlreadyExists = 3,
    UnknownError = 8,
}

export enum LocationUpdateCodeOptions {
    Updated = 0,
    NoSuchVehExists = 1,
    OwnIDAlreadyExists = 3,
    LicencePlateEmpty = 4,
    LicencePlateAlreadyExists = 5,
    ChargingIDAlreadyExists = 6,
    UnknownError = 9,
}

// ====================
// GROUP MEMBERSHIP
// ====================

export interface LocationGroupMembershipUpdateRequest {
    id: number;
    groupIDs: number[];
}

export enum LocationGroupMembershipCodeOptions {
    Updated = 0,
    NoSuchHRExists = 1,
    UnknownError = 2
}
