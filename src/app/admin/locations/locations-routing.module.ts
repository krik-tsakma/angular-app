// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../auth/authGuard';
import { LocationsService } from './locations.service';

// COMPONENTS
import { ViewLocationsComponent } from './view/view-locations.component';
import { EditLocationSettingsComponent } from './edit/settings/edit-location-settings.component';

const LocationsRoutes: Routes = [
    {
        path: '',
        component: ViewLocationsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: EditLocationSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.create'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit-location.module').then((m) => m.EditLocationModule),
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'form.actions.edit'
        }
    }   
];

@NgModule({
    imports: [
        RouterModule.forChild(LocationsRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        LocationsService
    ]
})
export class LocationsRoutingModule { }
