// FRAMEWORK
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { AdminLabelService } from '../../admin-label.service';
import { PreviousRouteService } from '../../../core/previous-route/previous-route.service';
import { Config } from '../../../config/config';

// TYPES 
import { SettingList } from '../../settings-list/settings-list.types';
import { LocationSessionName } from '../locations.types';

@Component({
    selector: 'edit-location',
    templateUrl: 'edit-location.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditLocationComponent {
    public options: SettingList[] = [
        {
            title: 'assets.category.basic',
            items: [
                {
                    option: 'assets.category.basic.settings',
                    params: ['settings'],
                    icon: 'settings'
                },
                {
                    option: 'assets.category.basic.groups',
                    params: ['groups'],
                    icon: 'group'
                },
            ]
        }
    ];

    constructor(public labelService: AdminLabelService,
                private configService: Config,    
                private previousRouteService: PreviousRouteService,
                private router: Router) {
        labelService.setSessionItemName(LocationSessionName);
    }

    // Click back arrow
    public back() {
        this.router.navigate([this.previousRouteService.getPreviousUrl()], { skipLocationChange: !this.configService.get('router') });
    }
}
