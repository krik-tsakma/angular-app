﻿// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../../shared/shared.module';

// COMPONENTS
import { EditLocationSettingsComponent } from './edit-location-settings.component';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        EditLocationSettingsComponent
    ],
    exports: [
        EditLocationSettingsComponent
    ]
})
export class EditLocationSettingsModule { }
