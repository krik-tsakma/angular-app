// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { SnackBarService } from '../../../../shared/snackbar.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { AdminLabelService } from '../../../admin-label.service';
import { LocationsService } from '../../locations.service';
import { Config } from '../../../../config/config';

// TYPES
import { LocationSettings, LocationSessionName, LocationCreateCodeOptions, LocationUpdateCodeOptions } from '../../locations.types';

@Component({
    selector: 'edit-location-settings',
    templateUrl: 'edit-location-settings.html',
    encapsulation: ViewEncapsulation.None,
})

export class EditLocationSettingsComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public action: string;
    public vehicle: LocationSettings = {} as LocationSettings;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected snackBar: SnackBarService,
        protected labelService: AdminLabelService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: LocationsService
        
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService);
        labelService.setSessionItemName(LocationSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                this.action = params.id
                            ? 'form.actions.edit'
                            : 'form.actions.create';
                if (params.id) {
                    this.get(params.id);
                }
            });
    }


    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: LocationSettings, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

        // This is create
        if (typeof(this.vehicle.id) === 'undefined' || this.vehicle.id === 0) {
            this.serviceSubscriber = this.service
                .create(this.vehicle)
                .subscribe((res: LocationCreateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const message = this.getCreateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
            },
            (err) => {
                this.loading = false;
                this.snackBar.open('form.errors.unknown', 'form.actions.ok');
            });
        } else { 
            // This is an update
            this.serviceSubscriber = this.service
                .update(this.vehicle)
                .subscribe((res: LocationUpdateCodeOptions) => {
                    this.loading = false;
                    if (res === null) {
                        this.labelService.set(this.vehicle.licencePlate);
                        this.snackBar.open('form.save_success', 'form.actions.close');
                        this.back();
                    } else {
                        const message = this.getUpdateResult(res);
                        this.snackBar.open(message, 'form.actions.close');
                    }
                },
                (err) => {
                    this.loading = false;
                    if (err.status === 404) {
                        this.snackBar.open('form.errors.not_found', 'form.actions.close');
                    } else {
                        this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                    }
                });
        }
    }

    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getByID(id)
            .subscribe((res: LocationSettings) => {
                this.loading = false;
                this.vehicle = res;
            },
            (err) => {
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }


    // =========================
    // RESULTS
    // =========================
    private getCreateResult(code: LocationCreateCodeOptions): string {
        let message = '';
        switch (code) {
            case LocationCreateCodeOptions.LicencePlateAlreadyExists:
                message = 'vehicles.errors.licence_plate_exists';
                break;
            case LocationCreateCodeOptions.OwnIDAlreadyExists:
                message = 'vehicles.errors.id_exists';
                break;
            case LocationCreateCodeOptions.ChargingIDAlreadyExists:
                message = 'vehicles.errors.chargingid_exists';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }

    private getUpdateResult(code: LocationUpdateCodeOptions): string {
        let message = '';
        switch (code) {
            case LocationUpdateCodeOptions.LicencePlateAlreadyExists:
                message = 'vehicles.errors.licence_plate_exists';
                break;
            case LocationUpdateCodeOptions.OwnIDAlreadyExists:
                message = 'vehicles.errors.id_exists';
                break;
            case LocationUpdateCodeOptions.ChargingIDAlreadyExists:
                message = 'vehicles.errors.chargingid_exists';
                break;
            default:
                message = 'form.errors.unknown';
                break;
        }
        return message;
    }
}
