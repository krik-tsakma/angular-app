// FRAMEWORK
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// COMPONENTS
import { AdminBaseComponent } from '../../../admin-base.component';

// SERVICES
import { CustomTranslateService } from '../../../../shared/custom-translate.service';
import { UnsubscribeService } from '../../../../shared/unsubscribe.service';
import { SnackBarService } from '../../../../shared/snackbar.service';
import { PreviousRouteService } from '../../../../core/previous-route/previous-route.service';
import { AdminLabelService } from '../../../admin-label.service';
import { LocationsService } from '../../locations.service';
import { Config } from '../../../../config/config';

// TYPES
import { AssetGroupTypes } from '../../../asset-groups/asset-groups.types';
import { LocationSessionName, LocationGroupMembershipUpdateRequest, LocationGroupMembershipCodeOptions } from '../../locations.types';

@Component({
    selector: 'location-group-membership',
    templateUrl: 'location-group-membership.html',
    encapsulation: ViewEncapsulation.None
})

export class EditLocationGroupMembershipComponent extends AdminBaseComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public loading: boolean;
    public groupTypes: any = AssetGroupTypes;
    public loc: LocationGroupMembershipUpdateRequest = {} as LocationGroupMembershipUpdateRequest;

    constructor(
        protected previousRouteService: PreviousRouteService,
        protected router: Router,
        protected translator: CustomTranslateService,
        protected unsubscriber: UnsubscribeService,
        protected labelService: AdminLabelService,
        protected snackBar: SnackBarService,
        protected configService: Config,
        private activatedRoute: ActivatedRoute,
        private service: LocationsService
    ) {
        super(translator, previousRouteService, router, unsubscriber, labelService, configService, snackBar);
        labelService.setSessionItemName(LocationSessionName);
    }

    public ngOnInit(): void {
        super.ngOnInit();

        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: number;
            }) => { 
                if (params.id) {
                    this.loc.id = params.id;
                    this.get(params.id);
                }
            });
    }

    // =========================
    // DATA
    // =========================

    public onSubmit({ value, valid }: { value: LocationGroupMembershipUpdateRequest, valid: boolean }) {
        // check if model is valid
        if (!valid) {
            return;
        }
        this.loading = true;
        this.unsubscribeService();

     
        // This is an update
        this.serviceSubscriber = this.service
            .updateGroupMembership(this.loc)
            .subscribe((res?: LocationGroupMembershipCodeOptions) => {
                this.loading = false;
                if (res === null) {
                    this.snackBar.open('form.save_success', 'form.actions.close');
                    this.back();
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.close');
                }
            },
            (err) => {
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('form.errors.unknown', 'form.actions.ok');
                }
            });
    }


    private get(id: number) {
        this.loading = true;
        this.unsubscribeService();
        this.serviceSubscriber = this.service
            .getGroupMembershipByID(id)
            .subscribe((res: number[]) => {
                this.loading = false;
                this.loc.groupIDs = res;
            },
            (err) => {
                if (err.status === 404) {
                    this.snackBar.open('form.errors.not_found', 'form.actions.close');
                } else {
                    this.snackBar.open('data.error', 'form.actions.ok');
                }
            }
        );
    }
}
