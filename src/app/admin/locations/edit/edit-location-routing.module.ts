﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES
import { AuthGuard } from '../../../auth/authGuard';
import { LocationsService } from '../locations.service';

// COMPONENTS
import { EditLocationComponent } from './edit-location.component';
import { EditLocationSettingsComponent } from './settings/edit-location-settings.component';
import { EditLocationGroupMembershipComponent } from './group-membership/location-group-membership.component';

const EditLocationsRoutes: Routes = [
    {
        path: '',
        component: EditLocationComponent,
        canActivate: [AuthGuard]
    },   
    {
        path: 'settings',
        component: EditLocationSettingsComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.category.basic.settings'
        }
    },
    {
        path: 'groups',
        component: EditLocationGroupMembershipComponent,
        canActivate: [AuthGuard],
        data: {
            breadcrumb: 'assets.category.basic.groups'
        }
    },    
];

@NgModule({
    imports: [
        RouterModule.forChild(EditLocationsRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        LocationsService
    ]
})
export class EditLocationRoutingModule { }
