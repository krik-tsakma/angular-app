﻿
// FRAMEWORK
import { NgModule } from '@angular/core';

// MODULES
import { SharedModule } from '../../../shared/shared.module';
import { SettingsListModule } from '../../settings-list/settings-list.module';
import { EditLocationRoutingModule } from './edit-location-routing.module';
import { EditLocationSettingsModule } from './settings/edit-location-settings.module';

// SERVICES
import { LocationsService } from '../locations.service';

// COMPONENTS
import { EditLocationGroupMembershipComponent } from './group-membership/location-group-membership.component';
import { EditLocationComponent } from './edit-location.component';
import { AssetGroupMembershipModule } from '../../../common/asset-groups-membership/asset-group-membership.module';

@NgModule({
    imports: [
        SharedModule,
        EditLocationRoutingModule,
        EditLocationSettingsModule,
        SettingsListModule,
        AssetGroupMembershipModule
    ],
    declarations: [
        EditLocationComponent,
        EditLocationGroupMembershipComponent    
    ],
    providers: [
        LocationsService
    ]
})
export class EditLocationModule { }
