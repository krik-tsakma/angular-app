// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// SERVICES
import { AuthHttp } from '../../auth/authHttp';
import { Config } from '../../config/config';

// TYPES
import { KeyName } from '../../common/key-name';
import { 
    LocationsRequest,
    LocationsResult, 
    LocationSettings, 
    LocationCreateCodeOptions, 
    LocationUpdateCodeOptions, 
    LocationGroupMembershipUpdateRequest, 
    LocationGroupMembershipCodeOptions
} from './locations.types';

@Injectable()
export class LocationsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/Locations';
        }

    // Get all Locations
    public get(request: LocationsRequest): Observable<LocationsResult> {

        const params = new HttpParams({
            fromObject: {
                SearchTerm: request.searchTerm ? request.searchTerm.toString() : '',
                PageNumber: request.pageNumber ? request.pageNumber.toString() : '1',
                PageSize: request.pageSize ? request.pageSize.toString() : '20',
                Sorting: request.sorting,
                GroupID: request.groupID ? request.groupID.toString() : '',                
            }
        });

        return this.authHttp
            .get(this.baseURL, { params });
    }


    // =========================
    // BASIC
    // =========================

    // Get Single Location's details 
    public getByID(id: number): Observable<LocationSettings> {
        return this.authHttp
            .get(this.baseURL + '/' + id);
    }

    // Create New Location 
    public create(entity: LocationSettings): Observable<LocationCreateCodeOptions>  {
        return this.authHttp
            .post(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    if (res.status === 201) {
                        return null;
                    } else  {
                        return res.body as LocationCreateCodeOptions;
                    }
                })
            );
    }

    // Update Location 
    public update(entity: LocationSettings): Observable<LocationUpdateCodeOptions>  {
        return this.authHttp
            .put(this.baseURL, entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as LocationUpdateCodeOptions;
                    }
                })
            );            
    }

    // Delete Location 
    public delete(id: number): Observable<number> {
        return this.authHttp
            .delete(this.baseURL + '/' + id, { observe: 'response' })
            .pipe(
                map((res) => res.status)
            ); 
    }






// ===================================================== //
//                        TODO                           //


    // =========================
    // GROUP MEMBERSHIP
    // =========================

    public getGroupMembershipByID(id: number): Observable<number[]> {
        return this.authHttp
            .get(this.baseURL + '/GetGroupMembership/' + id);
    }

    
    public updateGroupMembership(entity: LocationGroupMembershipUpdateRequest): Observable<LocationGroupMembershipCodeOptions>  {
        return this.authHttp
            .put(this.baseURL + '/UpdateGroupMembership/', entity, { observe: 'response' })
            .pipe(
                map((res) => {
                    // this is means empty Ok response from the server
                    if (res.status === 200 && !res.body) {
                        return null;
                    } else  {
                        return res.body as LocationGroupMembershipCodeOptions;
                    }
                })
            );            
    }

} 
