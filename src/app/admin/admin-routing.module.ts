﻿
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminCoreComponent } from './admin-core/admin-core.component';
import { AuthGuard } from '../auth/authGuard';

const AdminRoutes: Routes = [
    {
        path: '',
        component: AdminCoreComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'company',
        loadChildren: () => import('./company/company.module').then((m) => m.CompanyModule),
        data: {
            breadcrumb: 'admin.categories.company.manage'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'score-definition',
        loadChildren:  () => import('./drive-scores/drive-scores.module').then((m) => m.DriveScoresModule),
        data: {
            breadcrumb: 'admin.categories.scores.score_definition'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'energy-score',
        loadChildren: () => import('./energy-score/energy-score.module').then((m) => m.EnergyScoreModule),
        data: {
            breadcrumb: 'admin.categories.scores.energy_score'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'users',
        loadChildren: () => import('./users/users.module').then((m) => m.UsersModule),
        data: {
            breadcrumb: 'admin.categories.users.manage'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'user-roles',
        loadChildren: () => import('./user-roles/user-roles.module').then((m) => m.UserRolesModule),
        data: {
            breadcrumb: 'admin.categories.users.roles'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'drivers',
        loadChildren: () => import('./drivers/drivers.module').then((m) => m.DriversModule),
        data: {
            breadcrumb: 'admin.categories.drivers.manage'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'groups/users',
        loadChildren: () => import('./user-groups/user-groups.module').then((m) => m.UserGroupsModule),
        data: {
            breadcrumb: 'admin.categories.users.groups'
        }
    },
    {
        path: 'groups/vehicles',
        loadChildren: () => import('./vehicle-groups/vehicle-groups.module').then((m) => m.VehicleGroupsModule),
        data: {
            breadcrumb: 'admin.categories.vehicles.groups'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'groups/drivers',
        loadChildren: () => import('./driver-groups/driver-groups.module').then((m) => m.DriverGroupsModule),
        data: {
            breadcrumb: 'admin.categories.drivers.groups'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'locations',
        loadChildren: () => import('./locations/locations.module').then((m) => m.LocationsModule),
        data: {
            breadcrumb: 'admin.categories.locations.manage'
        }
    },
    {
        path: 'groups/locations',
        loadChildren: () => import('./location-groups/location-groups.module').then((m) => m.LocationGroupsModule),
        data: {
            breadcrumb: 'admin.categories.locations.groups'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'vehicles',
        loadChildren: () => import('./vehicles/vehicles.module').then((m) => m.VehiclesModule),
        data: {
            breadcrumb: 'admin.categories.vehicles.manage'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'vehicle-types',
        loadChildren: () => import('./vehicle-types/vehicle-types.module').then((m) => m.VehicleTypesModule),
        data: {
            breadcrumb: 'admin.categories.vehicles.types'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'fair-use-policies',
        loadChildren: () => import('./book-n-go/fair-use-policies/fair-use-policies.module').then((m) => m.FairUsePoliciesModule),
        data: {
            breadcrumb: 'book_n_go.fair_use_policies.title'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'tariffs',
        loadChildren: () => import('./book-n-go/tariffs/tariffs.module').then((m) => m.TariffsModule),
        data: {
            breadcrumb: 'book_n_go.tariffs.title'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'charge-points',
        loadChildren: () => import('./charge-points/charge-points.module').then((m) => m.ChargePointsModule),
        data: {
            breadcrumb: 'admin.categories.charge_points'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'groups/charge-points',
        loadChildren: () => import('./charge-points-groups/charge-points-groups.module').then((m) => m.ChargePointsGroupsModule),
        data: {
            breadcrumb: 'admin.categories.charge_points.groups'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'asset-links',
        loadChildren: () => import('./asset-links/asset-links.module').then((m) => m.AssetLinksModule),
        data: {
            breadcrumb: 'admin.categories.devices.links'
        },
        canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(AdminRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AdminRoutingModule { }
