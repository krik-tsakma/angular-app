// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from '../../config/config';
import { AuthHttp } from '../../auth/authHttp';

// TYPES
import { AssetGroupsRequest, AssetGroupItem } from './asset-groups.types';

@Injectable()
export class AssetGroupsService {
    private baseURL: string;
    constructor(
        private authHttp: AuthHttp,
        private config: Config) {
            this.baseURL = this.config.get('apiUrl') + '/api/AssetGroups';
        }

    // Get all company user roles
    public get(request: AssetGroupsRequest): Observable<AssetGroupItem[]> {
        const objs = this.authHttp.removeObjNullKeys({
            SearchTerm: request.searchTerm ? request.searchTerm.toString() : null,
            Sorting: request.sorting,
            Type: request.type.toString(),
            IncludeUngrouped : String(request.includeUngrouped),
            RoleID: request.roleID ? request.roleID.toString() : ''
        });

        const params = new HttpParams({
            fromObject: objs
        });

        return this.authHttp.get(this.baseURL, { params });            
    }

} 

