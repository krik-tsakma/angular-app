import { KeyName } from '../../common/key-name';
import { EnumTranslation } from '../../common/enum-translation';
import { PagerResults, SearchTermRequest } from '../../common/pager';

export interface AssetGroupsRequest {
    searchTerm: string;
    type: AssetGroupTypes;
    includeUngrouped: boolean;
    sorting: string;
    roleID?: number;
}

export interface AssetGroupItem extends KeyName {
    isDefault: boolean;
    membersCount?: number;
    dimensionTag?: string;
}

export enum AssetGroupTypes {
    Vehicle = 0,
    Driver = 1,
    Location = 2,
    Route = 3,
    DeviceType = 4,
    User = 5,
    ChargePoint = 6
}

export interface AssetGroupMembersRequest extends SearchTermRequest {
    id: number;
}
export interface AssetGroupMembersResult extends PagerResults {
    results: KeyName[];
}

export interface AssetGroupMemberAddRemoveRequest {
    groupID: number;
    memberID: number;
}
