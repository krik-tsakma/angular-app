function ScoreBuildUpGraph(htmlID, decimalSeparator, thousandsSeparator, chartTitle, goodOnTargetLbl, highOnTargetLbl, lowOnTargetLbl, referenceAssetsTitle) // Constructor
{
    this.id = htmlID;
    this.decimalSeparator = decimalSeparator ? decimalSeparator : '.';
    this.thousandsSeparator = thousandsSeparator ? thousandsSeparator : ',';
    this.chartTitle = chartTitle;

    this.referenceAssetsTitle = referenceAssetsTitle;

    this.goodOnTargetLbl = goodOnTargetLbl ? goodOnTargetLbl : '< 5% below target or over target';
    this.highOnTargetLbl = highOnTargetLbl ? highOnTargetLbl : '5-20% below target';
    this.lowOnTargetLbl = lowOnTargetLbl ? lowOnTargetLbl : '> 20% below target';
    this._goodColorCode = '#00a74e';
    this._highColorCode = '#ffd80f';
    this._lowColorCode = '#c34706';
}

ScoreBuildUpGraph.prototype.get = function (dataProvider, printable) {
    let data = this._formatData(dataProvider);
    let legends = [];
    data.forEach((dp, index) => {
        legends.push({ code: 'DP' + (index + 1), name: dp.datapointName });
    });

    const that = this;
    const cfg = {
        type: 'serial',
        theme: 'default',
        dataProvider: data,
        gridAboveGraphs: true,
        startDuration: 0,
        addClassNames: true,
        valueAxes: [{
            title: this.chartTitle + ' %',
            gridThickness: 0
        }],
        numberFormatter: {
            decimalSeparator: this.decimalSeparator,
            thousandsSeparator: this.thousandsSeparator
        },
        graphs: [{
            balloonText: '[[category]]: <b>[[value]]</b>',
            fillAlphas: 1,
            lineThickness: 0,
            type: 'column',
            valueField: 'targetDeviation',
            colorField: 'color',
            showAllValueLabels: true,
            labelText: printable ? "[[value]]" : null,
        },
        {
            id: 'ReferenceTargetDeviationLine',
            balloonText: ' <b>[[title]]</b><br/>[[category]]: <b>[[value]]</b>',
            bullet: 'round',
            lineThickness: 0,
            bulletSize: 12,
            bulletBorderAlpha: 1,
            colorField: 'referenceTargetDeviationColor',
            bulletBorderColor: '#000000',
            useLineColorForBulletBorder: false,
            bulletBorderThickness: 2,
            fillAlphas: 0,
            lineAlpha: 1,
            title: this.referenceAssetsTitle,
            valueField: 'referenceTargetDeviation',
            dashLength: 5
        }],
        categoryField: 'datapointName',
        categoryAxis: {
            autoWrap: true,
            gridPosition: 'start',
            widthField: 'weight',
            tickLength: 0,
            gridThickness: 1,
            labelFunction: (label, categoryAxis, valueAxis) => {
                const lbl = legends.find((_) => _.name === label);
                return lbl ? lbl.code : label;
            },
            listeners: [{
                event: 'rollOverItem',
                method: (e) => {
                    e.target.setAttr('cursor', 'pointer');
                    const div = document.createElement('div');
                    div.setAttribute('id', 'score-build-up-tooltip');
                    div.innerHTML = e.serialDataItem.category;
                    div.style.position = 'absolute';
                    div.style.top = e.target.y + 20 + 'px';
                    div.style.left = e.target.x - 20 + 'px';
                    div.style.padding = '5px';
                    div.style.backgroundColor = '#000';
                    div.style.color = '#fff';
                    div.style.borderRadius = '2px';
                    div.style.fontSize = '12px';
                    div.style.textAlign = 'center';
                    const amchartElm = that.closest(e.target.node, 'amchart');
                    if (amchartElm) {
                        amchartElm.appendChild(div);
                    }
                }
            },
            {
                event: 'rollOutItem',
                method: (e) => {
                    e.target.setAttr('cursor', 'default');
                    const tooltip = document.getElementById('score-build-up-tooltip');
                    if (tooltip) {
                        tooltip.parentNode.removeChild(tooltip);
                    }
                }
            }]
        },
        responsive: {
            enabled: true
        },
        export: {
            enabled: true,
            menu: []
        },
        listeners: [{
            event: "init",
            method: function(e) {
                if (!data || data.length === 0) {
                    return;
                }
                // get legend object
                const legID = "custom-legenddiv" + that.id;
                const legend = document.getElementById(legID);

                if (legend.innerHTML !== "") {
                    return;
                }

                that._addToLegend(legend, that._goodColorCode, that.goodOnTargetLbl, true);
                that._addToLegend(legend, that._highColorCode, that.highOnTargetLbl, true);
                that._addToLegend(legend, that._lowColorCode, that.lowOnTargetLbl, true);

                legends.forEach((dp) => {
                    that._addToLegend(legend, dp.code, dp.name)
                });
            }
        }]
    };

    // Use the map method to extract the values
    const targetDeviationValues = data.map((o) => {
         return o.targetDeviation;
    });
    const referenceTargetDeviationValues = data.map((o) => {
         return o.referenceTargetDeviation;
    });
    // use the array in the max method to get the max value
    const targetDeviationMax = Math.max.apply(Math, targetDeviationValues);
    const referenceTargetDeviationMax = Math.max.apply(Math, referenceTargetDeviationValues);
    // use the array in the min method to get the max value
    const targetDeviationMin = Math.min.apply(Math, targetDeviationValues);
    const referenceTargetDeviationMin = Math.min.apply(Math, referenceTargetDeviationValues);

    if (targetDeviationMin > -100 || referenceTargetDeviationMin > -100) {
        cfg['valueAxes'][0]['minimum'] = -100;
    }
    if (targetDeviationMax < 100 || referenceTargetDeviationMax < 100) {
        cfg['valueAxes'][0]['maximum'] = 100;
    }
    return cfg;
}

ScoreBuildUpGraph.prototype.getColor = function (targetDeviation) {
    let color = '';
    if (targetDeviation >= -5) {
        color = this._goodColorCode; // good
    } else if (targetDeviation > -20) {
        color = this._highColorCode; // high
    } else {
        color = this._lowColorCode; // low
    }
    return color;
}
ScoreBuildUpGraph.prototype.closest = function(el, id) {
    while (String(el.nodeName).toLowerCase() !== String(id).toLowerCase()) {
        el = el.parentNode;
        if (!el) {
            return null;
        }
    }
    return el;
}

ScoreBuildUpGraph.prototype._formatData = function (result) {
    const finalDataProvider = [];
    result.items.forEach((element) => {
        const item = element;
        item.color = this.getColor(item.targetDeviation);
        if (element.weight > 0) {
            if (result.referenceItems && result.referenceItems.length > 0) {
                const referenceItem = result.referenceItems.filter((x) => x.datapointID === item.datapointID);
                if (referenceItem && referenceItem[0].weight > 0) {
                    item.referenceTargetDeviation = referenceItem[0].targetDeviation;
                    item.referenceTargetDeviationColor = sbc.getColor(item.referenceTargetDeviation);
                }
            }
            finalDataProvider.push(item);
        }
    });

    return finalDataProvider;
}

ScoreBuildUpGraph.prototype._addToLegend = function (legend, title, value, titleIsColor) {
    // create a legend item holder
    var item = document.createElement("div");

    // create title
    var titleElm = document.createElement("span");
    titleElm.className = "title";

    if (titleIsColor) {
        var squareElm = document.createElement("span");
        squareElm.className = "square";
        squareElm.style.backgroundColor = title;
        titleElm.appendChild(squareElm);

    } else {
        titleElm.className = "title";
        titleElm.innerHTML = title;
    }
    item.appendChild(titleElm);

    // create  value
    var valueElm = document.createElement("span");
    valueElm.className = "value";

    valueElm.innerHTML = value;
    item.appendChild(valueElm);

    // add item to legend
    legend.appendChild(item);
}
