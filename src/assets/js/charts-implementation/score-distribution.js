function ScoreDistributionGraph(htmlID, decimalSeparator, thousandsSeparator, dateFormat, targetTitle, targetValue, referenceAssetsSelected) // Constructor
{
    this.chartCfg = {};
    this.id = htmlID;
    this.decimalSeparator = decimalSeparator ? decimalSeparator : '.';
    this.thousandsSeparator = thousandsSeparator ? thousandsSeparator : ',';
    this.targetValue = targetValue;
    this.targetTitle = targetTitle;
    this.referenceAssetsSelected = referenceAssetsSelected ? referenceAssetsSelected : false;
    this.dateFormat = dateFormat;
}

ScoreDistributionGraph.prototype.get = function (dataProvider) {
    let data = this._formatData(dataProvider);
    this.chartCfg = {
        dataProvider: data,
        type: 'serial',
        startDuration: 0,
        theme: 'default',
        numberFormatter: {
            decimalSeparator: this.decimalSeparator,
            thousandsSeparator: this.thousandsSeparator
        },
        categoryField: 'timestamp',
        categoryAxis: {
            gridPosition: 'start',
             // small angle to labels on x axis to avoid label overlapping
            labelRotation: 15,
            axisAlpha: 0,
            gridAlpha: 0,
            position: 'left',
            categoryFunction:
            // TODO(GT): duplicate code
            (category, dataItem, categoryAxis) => {
                if(!dataItem.timestamp)
                    return ' ';
                
                return this.getDateFormatted(dataItem.timestamp);
            },

        },

        graphs: [
            this.getScoreLetterGraph('A', 'v1'),
            this.getScoreLetterGraph('B', 'v1'),
            this.getScoreLetterGraph('C', 'v1'),
            this.getScoreLetterGraph('D', 'v1'),
            this.getScoreLetterGraph('E', 'v1'),
            this.getScoreLetterGraph('F', 'v1'),
            this.getScoreLetterGraph('G', 'v1'),
            this.getScoreLetterGraph('H', 'v1')
        ],
         // Custom legend, otherwise with reference assets, letters appear twice
        legend: {
            position: 'bottom',
            align: 'center',
            autoMargins: true,
            color: '#ffffff',
            markerLabelGap: -11,
            // Empty to hide the value of the pie piece
            valueText: '',
            switchable: false,
            data: [
                {
                    title: 'A',
                    color: this.getForLetter('A')
                },
                {
                    title: 'B',
                    color: this.getForLetter('B')
                },
                {
                    title: 'C',
                    color: this.getForLetter('C')
                },
                {
                    title: 'D',
                    color: this.getForLetter('D')
                },
                {
                    title: 'E',
                    color: this.getForLetter('E')
                },
                {
                    title: 'F',
                    color: this.getForLetter('F')
                },
                {
                    title: 'G',
                    color: this.getForLetter('G')
                },
                {
                    title: 'H',
                    color: this.getForLetter('H')
                }
            ]
        },
       valueAxes: [{
            id: 'v1',
            stackType: '100%',
            axisAlpha: 1,
            gridAlpha: 0.2,
            title: '%'
        }],
        responsive: {
            enabled: true
        },
        export: {
            enabled: true, 
            menu: []
        }
    };

    if (this.targetValue) {
        // THE TARGET LINE
        this.chartCfg['guides'] = [{
            fillAlpha: 0,
            lineAlpha: 1,
            lineThickness: 3,
            value: this.targetValue,
            above: true,
            label: this.targetTitle + ': ' + this.targetValue,
            boldLabel: true,
            balloonText: this.targetTitle + ': ' + this.targetValue,
            inside: true
        }];
    }

    // when we have reference items, then add a new axis and attach the items
    if (this.referenceAssetsSelected === true) {
        const axes = this.chartCfg.valueAxes;
        axes.push({
            id: 'v2',
            stackType: '100%'
        });

        const graphs = this.chartCfg.graphs;
        graphs.push(this.getScoreLetterGraph('referenceA', 'v2'));
        graphs.push(this.getScoreLetterGraph('referenceB', 'v2'));
        graphs.push(this.getScoreLetterGraph('referenceC', 'v2'));
        graphs.push(this.getScoreLetterGraph('referenceD', 'v2'));
        graphs.push(this.getScoreLetterGraph('referenceE', 'v2'));
        graphs.push(this.getScoreLetterGraph('referenceF', 'v2'));
        graphs.push(this.getScoreLetterGraph('referenceG', 'v2'));
        graphs.push(this.getScoreLetterGraph('referenceH', 'v2'));
    }
    return this.chartCfg;
}


ScoreDistributionGraph.prototype.getScoreLetterGraph = function (letter, axis) {
    const isReferenceItem = letter.indexOf('reference') > -1;
    const l = letter.replace('reference', '');
    const letterColor = this.getForLetter(l);
    return {
        valueAxis: axis,
        balloonFunction: (item, graph) => {
            return '<b>' + (isReferenceItem ? 'Reference ' : '') + l + ': ' + item.values.value.toFixed(2) + '</b>';
        },
        fillAlphas: isReferenceItem ? 0.6 : 1,
        lineAlpha: 1,
        labelText: '[[value]]',
        labelFunction: (graphDataItem, graph) => {
            return graphDataItem.values.value.toFixed(2);
        },
        title: l,
        type: 'column',
        color: '#FFFFFF',
        fillColors: letterColor,
        lineColor: letterColor,
        valueField: letter
    };
}

ScoreDistributionGraph.prototype.getForLetter = function(letter) {

    switch (letter) {
        case 'A' :
            return '#00A74E';
        case 'B' :
            return '#4BBA42';
        case 'C' :
            return '#B3CA15';
        case 'D' :
            return '#FFD80F';
        case 'E' :
            return '#FEBA00';
        case 'F' :
            return '#F57000';
        case 'G' :
            return '#E00001';
        case 'H' :
            return '#A10202';     
        default:
            return '';       
    }
}
ScoreDistributionGraph.prototype.getDateFormatted = function(dateIso) {
    const date = new Date(dateIso)
    const day = date.getDate();
    const monthIndex = date.getMonth();
    const year = date.getFullYear();

    var dt = this.dateFormat;
    dt = dt.replace('DD', this._ensure2Digits(day));
    dt = dt.replace('MM', this._ensure2Digits(monthIndex+ 1));
    dt = dt.replace('YYYY', this._ensure2Digits(year));

    return dt;

}

ScoreDistributionGraph.prototype._ensure2Digits = function (num) {
	if (num >= 10)
		return num.toString();
	return "0" + num.toString();
}

ScoreDistributionGraph.prototype._formatData = function(dataProvider) {
    if (!dataProvider) {
        return;
    }
    const newDataProvider = [];

    dataProvider.items.forEach((element) => {
        const newItem = {};
        newItem.timestamp = element.timestamp;
        let referenceItems = [];
        if (dataProvider.referenceItems && dataProvider.referenceItems.length > 0) {
            referenceItems = dataProvider.referenceItems.filter((x) => x.timestamp === element.timestamp);
        }
        element.items.forEach((item) => {
            newItem[item.letter] = item.perc;

            if (referenceItems && referenceItems.length > 0) {
                const referenceItem = referenceItems[0].items.filter((x) => x.letter == item.letter);
                if (referenceItem && referenceItem.length > 0) {
                    newItem['reference' + item.letter] = referenceItem[0].perc;
                }
            }
        });

        newDataProvider.push(newItem);
    });

    return newDataProvider;
}