if (location.hostname === 'gfm3-acc.sycada.com' || location.hostname === 'gfm3.sycada.com') {

    const hostName = location.hostname === 'gfm3.sycada.com' ? '1' : '2';

    const script = document.createElement('script');
    script.async = true;
    script.src = `https://www.googletagmanager.com/gtag/js?id=UA-137497398-${hostName}`;

    document.head.prepend(script);

    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    // gtag('config', `UA-137497398-${hostName}`);
}
