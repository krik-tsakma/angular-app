window.isUpdateAvailable = new Promise((resolve, reject) => {
    if ('serviceWorker' in navigator) {                
        navigator
            .serviceWorker
            .register('/service-worker.js')
            .then((reg) => {
                // check every minute for new service-worker
                setInterval(() => {                     
                    reg.update();                                                        
                }, 60000);

                reg.onupdatefound = () => {
                    // An updated service worker has appeared in reg.installing!
                    console.log('U.F');
                    const newWorker = reg.installing;                          
                    newWorker.onstatechange = () => {                                   
                        // Has service worker state changed?                       
                        switch (newWorker.state) {
                            case 'installed':                                
                                // If There is a new service worker available (aka true), show the notification                                                                                
                                resolve(navigator.serviceWorker.controller ? true : false);                                           
                                break;
                        }
                    };
                };                            
            }).catch((err)=> {
                console.log('Service Worker: Registration Failed', err);
            });
    } 
});