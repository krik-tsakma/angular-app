// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:49267',
  maxServerID: 0,
  companyLogoUrl: 'https://apps.sycada.com/content/companies_logos',
  evImageUrl: 'https://apps.sycada.com/content/vehicle_types',
  chargePointsHub: 'http://localhost:49267/hubs/chargepoint',
  importExportHub: 'http://localhost:49267/hubs/importexport',
  fiscalTripsReviewHub: 'http://localhost:49267/hubs/fiscaltripsreview',
  superAdminUrl: 'http://localhost:60630/Account/Login',
  router: true,
  analytics: '',
  announcementEnabled: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
