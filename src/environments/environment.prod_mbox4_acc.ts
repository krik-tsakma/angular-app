export const environment = {
    production: true,
    apiUrl: 'https://apps.sycada.com/mbox4_acc',
    maxServerID: 4,
    companyLogoUrl: 'https://apps.sycada.com/content/companies_logos',
    evImageUrl: 'https://apps.sycada.com/content/vehicle_types',
    chargePointsHub: 'https://apps.sycada.com/mbox4/hubs/chargepoint',
    importExportHub: 'https://apps.sycada.com/mbox4/hubs/importexport',
    fiscalTripsReviewHub: 'https://apps.sycada.com/mbox4/hubs/fiscaltripsreview',
    superAdminUrl: 'https://admin.mysycada.com/Account/Login',
    router: false,
    analytics: 'https://www.google-analytics.com/analytics.js',
    announcementEnabled: true
  };
