export const environment = {
    production: true,
    apiUrl: 'http://localhost:49267',
    maxServerID: 4,
    companyLogoUrl: 'https://apps.sycada.com/content/companies_logos',
    evImageUrl: 'https://apps.sycada.com/content/vehicle_types',
    chargePointsHub: 'http://localhost:49267/hubs/chargepoint',
    importExportHub: 'http://localhost:49267/hubs/importexport',
    fiscalTripsReviewHub: 'http://localhost:49267/hubs/fiscaltripsreview',
    superAdminUrl: 'http://localhost:60630/Account/Login',
    router: false,
    analytics: '',
    announcementEnabled: true
  };
