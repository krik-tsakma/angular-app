﻿module.exports = {
    staticFileGlobs: [
      'dist/main-es2015.**.js',
      'dist/common-es2015.**.js',
      'dist/polyfills-es2015.**.js',
      'dist/runtime-es2015.**js',
      'dist/scripts.**.js',
      'dist/**.css',
      'dist/index.html',
      'dist/assets/images/**/**.*',
      'dist/assets/marker-icons/**/**.*'
    ],
    verbose: true,
    root: 'dist',
    stripPrefix: 'dist/',
    navigateFallback: '/index.html',
    runtimeCaching: [
        {
            urlPattern: /\/assets\//,
            handler: 'cacheFirst'
        },
        {
            urlPattern: /^http:\/\/localhost:8080\/#\//,
            handler: 'cacheFirst'
        },
        {
          urlPattern: /^https:\/\/gfm3.sycada.com\/#\//,
          handler: 'cacheFirst'
        },
        {
          urlPattern: /^https:\/\/gfm3-acc.sycada.com\/#\//,
          handler: 'cacheFirst'
        }

    ]
};
